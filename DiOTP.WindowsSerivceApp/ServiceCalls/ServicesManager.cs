﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.OracleDTOs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;

namespace DiOTP.WindowsSerivceApp.ServiceCalls
{
    public class ServicesManager
    {
        public static Response responseCustom = new Response();

        public static Response GetMaHolderRegByAccountNo(string accNo)
        {
            MaHolderReg maHolderReg = new MaHolderReg();
            try
            {
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    StringBuilder filter = new StringBuilder();
                    filter.Append(" where 1=1");
                    filter.Append(" and holder_no = '" + accNo + "'");
                    var response = client.GetAsync("api/MaHolderRegApi/Get?filter=" + filter.ToString()).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        Response responseFromAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                        if (responseFromAPI.IsSuccess)
                        {
                            List<MaHolderReg> maHolderRegs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MaHolderReg>>(responseFromAPI.Data.ToString());
                            maHolderReg = maHolderRegs.FirstOrDefault();
                            responseCustom.IsSuccess = true;
                            responseCustom.Data = maHolderReg;
                        }
                        else
                        {
                            responseCustom = responseFromAPI;
                        }
                    }
                    else
                    {
                        responseCustom.IsSuccess = false;
                        responseCustom.Message = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static Response GetMaHolderRegsByIdNo(string IdNo)
        {
            List<MaHolderReg> maHolderRegs = new List<MaHolderReg>();
            try
            {
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    StringBuilder filter = new StringBuilder();
                    filter.Append(" where 1=1");
                    //filter.Append(" and id_no_2 not in ('STA', 'STA  2', 'STA 2', 'STA 2 (10JF)', 'STA 3', 'STA2', 'STA3')");
                    //filter.Append(" and id_no_ol not in ('STA2', '10 JF', 'STA2')");
                    //filter.Append(" and id_no_3 not in ('10JF', 'STA 3', 'STA3')");
                    filter.Append(" and id_no = '" + IdNo + "'");
                    filter.Append(" or id_no_old = '" + IdNo + "'");
                    filter.Append(" or id_no_2 = '" + IdNo + "'");
                    filter.Append(" or id_no_old_2 = '" + IdNo + "'");
                    filter.Append(" or id_no_3 = '" + IdNo + "'");
                    filter.Append(" or id_no_old_3 = '" + IdNo + "'");
                    filter.Append(" or id_no_4 = '" + IdNo + "'");
                    filter.Append(" or id_no_old_4 = '" + IdNo + "'");
                    filter.Append(" or id_no_5 = '" + IdNo + "'");
                    filter.Append(" or id_no_old_5 = '" + IdNo + "'");
                    var response = client.GetAsync("api/MaHolderRegApi/Get?filter=" + filter.ToString()).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        Response responseFromAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                        if (responseFromAPI.IsSuccess)
                        {
                            maHolderRegs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MaHolderReg>>(responseFromAPI.Data.ToString());
                            responseCustom.IsSuccess = true;
                            responseCustom.Data = maHolderRegs;
                        }
                        else
                        {
                            responseCustom = responseFromAPI;
                        }
                        //Newtonsoft.Json.Linq.JObject json = Newtonsoft.Json.Linq.JObject.Parse(responseString);
                    }
                    else
                    {
                        responseCustom.IsSuccess = false;
                        responseCustom.Message = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        public static List<UtmcDetailedMemberInvestment> GetMAAccountSummary(string holderNo)
        {
            List<UtmcDetailedMemberInvestment> objs = new List<UtmcDetailedMemberInvestment>();

            DateTime currentDate = DateTime.Now;
            //DateTime yearsBefore = currentDate.AddYears(-10);
            DateTime yearsBefore = new DateTime(2000, 1, 1);

            List<HolderInv> holderInvs = GetHolderInvByHolderNo(holderNo);
            if (holderInvs.Count != 0)
            {
                List<HolderLedger> holderLedgers = GetHolderLedgerByHolderNo(" HOLDER_NO='" + holderNo + "' and FUND_ID in (" + String.Join(",", holderInvs.Select(x => "'" + x.FundId + "'").ToArray()) + ") ");

                List<UtmcMemberInvestment> utmcMemberInvestments = GetMemberInvestmentsByHolderNo(holderNo, yearsBefore.ToString("yyyy-MM-dd"), currentDate.ToString("yyyy-MM-dd"), String.Join(",", holderInvs.Select(x => "'" + x.FundId + "'").ToArray()));

                List<UtmcCompositionalTransaction> utmcCompositionalTransactions = GetCompositionalTransactionByHolderNo(holderNo, yearsBefore.ToString("yyyy-MM-dd"), currentDate.ToString("yyyy-MM-dd"));

                foreach (string fundCode in holderLedgers.Select(x => x.FundID).Distinct().ToList())
                {
                    UtmcDetailedMemberInvestment utmcDetailedMemberInvestment = new UtmcDetailedMemberInvestment
                    {
                        holderInv = holderInvs.Where(x => x.FundId == fundCode).FirstOrDefault(),
                        //utmcMemberInvestments = utmcMemberInvestments.Where(x => x.IpdFundCode == fundCode).OrderByDescending(x => x.EffectiveDate).ToList(),
                        utmcCompositionalTransactions = utmcCompositionalTransactions.Where(x => x.IpdFundCode == fundCode).OrderByDescending(x => x.DateOfTransaction).ToList(),
                        utmcMemberInvestments = utmcMemberInvestments.Where(x => x.ReportDate == utmcMemberInvestments.Max(y => y.ReportDate)).Where(x=>x.IpdFundCode == fundCode).ToList(),
                        fundDetailedInformation = GlobalProperties.FundDetailedInformations.Where(x => x.UtmcFundInformation.IpdFundCode == fundCode).FirstOrDefault(),
                        holderLedgers = holderLedgers.Where(x => x.FundID == fundCode).ToList()
                    };
                    objs.Add(utmcDetailedMemberInvestment);
                }
            }
            return objs;
        }

        public static List<HolderInv> GetHolderInvByHolderNo(string holderNo)
        {
            List<HolderInv> holderInvs = new List<HolderInv>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/HolderInvApi?holderNo=" + holderNo).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    holderInvs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<HolderInv>>(responseFromApi.Data.ToString());
                }
            }
            return holderInvs;
        }

        public static List<UtmcMemberInvestment> GetMemberInvestmentsByHolderNo(string holderNo, string startDate, string endDate, string fundIds)
        {
            List<UtmcMemberInvestment> utmcMemberInvestments = new List<UtmcMemberInvestment>();
            if (fundIds != "")
            {
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync("api/UtmcMemberInvestmentApi?holderNo=" + holderNo + "&startDate=" + startDate + "&endDate=" + endDate + "&fundIds=" + fundIds).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                        utmcMemberInvestments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcMemberInvestment>>(responseString);
                    }
                }
            }
            return utmcMemberInvestments;
        }

        public static List<UtmcCompositionalTransaction> GetCompositionalTransactionByHolderNo(string holderNo)
        {
            List<UtmcCompositionalTransaction> utmcCompositionalTransactions = new List<UtmcCompositionalTransaction>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/UtmcCompositionalTransactionApi?holderNo=" + holderNo).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    utmcCompositionalTransactions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcCompositionalTransaction>>(responseString);
                }
            }
            return utmcCompositionalTransactions;
        }

        public static List<UtmcCompositionalTransaction> GetCompositionalTransactionByHolderNo(string holderNo, string startDate, string endDate)
        {
            List<UtmcCompositionalTransaction> utmcCompositionalTransactions = new List<UtmcCompositionalTransaction>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/UtmcCompositionalTransactionApi?holderNo=" + holderNo + "&startDate=" + startDate + "&endDate=" + endDate).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    utmcCompositionalTransactions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcCompositionalTransaction>>(responseString);
                }
            }
            return utmcCompositionalTransactions;
        }

        public static Response GetAdditionalInfoByHolderNo(string holderNo)
        {
            Response responseAddInfo = new Response();
            try
            {
                List<AddInfo> addInfos = new List<AddInfo>();
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync("api/AddInfoApi?holderNo=" + holderNo).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                        Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                        addInfos = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AddInfo>>(responseFromApi.Data.ToString());
                        responseAddInfo.IsSuccess = true;
                        responseAddInfo.Data = addInfos;
                    }
                }
            }
            catch (Exception ex)
            {
                responseAddInfo.IsSuccess = false;
                responseAddInfo.Message = ex.Message;
            }
            return responseAddInfo;
        }

        public static List<HolderLedger> GetHolderLedgerByHolderNo(string filter)
        {
            List<HolderLedger> holderLedgers = new List<HolderLedger>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/HolderLedgerApi?filter=" + filter).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    holderLedgers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<HolderLedger>>(responseFromApi.Data.ToString());
                }
            }
            return holderLedgers;
        }
    }
}