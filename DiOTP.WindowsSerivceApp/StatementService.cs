﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WindowsSerivceApp.ServiceCalls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.WindowsSerivceApp
{
    public class StatementService
    {
        private static readonly Lazy<IUserStatementService> lazyIUserStatementService = new Lazy<IUserStatementService>(() => new UserStatementService());
        public static IUserStatementService IUserStatementService { get { return lazyIUserStatementService.Value; } }

        private static readonly Lazy<IUtmcDailyNavFundService> lazyUtmcDailyNavFundObj = new Lazy<IUtmcDailyNavFundService>(() => new UtmcDailyNavFundService());
        public static IUtmcDailyNavFundService IUtmcDailyNavFundService { get { return lazyUtmcDailyNavFundObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        public static List<UtmcFundInformation> utmcFundInformations { get; set; }

        public static void GetFunds()
        {
            Response responseUFIs = IUtmcFundInformationService.GetData(0, 0, true);
            if (responseUFIs.IsSuccess)
            {
                utmcFundInformations = (List<UtmcFundInformation>)responseUFIs.Data;
            }
        }
        public static void GenerateMonthlyReport(DateTime firstdForMonth, DateTime lastdForMonth)
        {
            GetFunds();
            //Response responseUAs = GenericService.PullData<UserAccount>(" account_no in ('36708', '23351', '36707', '23354') ", 0, 0, false, null);
            //Response responseUAs = GenericService.PullData<UserAccount>(" account_no in ('36981') ", 0, 0, false, null);
            List<UserAccount> allUA = UserAccountService.GetAll();
            //List<UserAccount> allUA = (List<UserAccount>)responseUAs.Data;

            foreach (UserAccount ua in allUA)
                GenerateReport(ua.AccountNo, firstdForMonth, lastdForMonth, 1);
        }

        public static void GenerateYearlyReport(DateTime firstdForMonth, DateTime lastdForMonth)
        {
            GetFunds();
            List<UserAccount> allUA = UserAccountService.GetAll();
            foreach (UserAccount ua in allUA)
                GenerateReport(ua.AccountNo, firstdForMonth, lastdForMonth, 3);
        }


        public static void GenerateReport(string accountNo, DateTime f, DateTime l, int statementCategory)
        {

            UserStatements us = new UserStatements();
            UserAccount ua = new UserAccount();
            ua = UserAccountService.GetByid(accountNo);

            string time1 = DateTime.Now.ToString("dd-MM-yyyy");
            DateTime currenttime1 = DateTime.Parse(time1);

            string time2 = DateTime.Now.ToString("dd-MM-yyyy");
            DateTime currenttime2 = DateTime.Parse(time2);

            int userAID = ua.Id;
            int userID = ua.UserId;

            List<UserStatement> us1 = new List<UserStatement>();
            //us1 = UserStatementService.GetAllUS();
            if (statementCategory == 1)
            {
                Response response = IUserStatementService.GetDataByFilter(" user_account_id=" + ua.Id + " and MONTH(created_date)=" + currenttime1.Month + " and user_statement_category_id=" + statementCategory + " ", 0, 0, false);
                us1 = (List<UserStatement>)response.Data;
            }
            else if (statementCategory == 3)
            {
                Response response = IUserStatementService.GetDataByFilter(" user_account_id=" + ua.Id + " and YEAR(created_date)=" + currenttime1.Year + " and user_statement_category_id=" + statementCategory + " ", 0, 0, false);
                us1 = (List<UserStatement>)response.Data;
            }
            string pdfDate = DateTime.Now.ToString("ddMMyyyyHHmmssfff");
            try
            {
                if (us1.Count == 0 && (statementCategory == 1 || statementCategory == 3))
                {
                    string StatementsPath = ConfigurationManager.AppSettings["StatementsPath"].ToString();
                    string path = "";
                    //Monthly
                    if (statementCategory == 1)
                        path = Path.GetFullPath(StatementsPath) + "/" + l.ToString("MM-yyyy") + "/" + accountNo + "_" + pdfDate + ".pdf";
                    //path = Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy") + "/" + accountNo + "_" + pdfDate + ".pdf";
                    //Yearly
                    else if (statementCategory == 3)
                        path = Path.GetFullPath(StatementsPath) + "/" + l.ToString("MM-yyyy") + "_" + "yearlyReport" + "/" + accountNo + "_" + pdfDate + ".pdf";
                    //path = Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy") + "_" + "yearlyReport" + "/" + accountNo + "_" + pdfDate + ".pdf";

                    if (!Directory.Exists(Path.GetFullPath(StatementsPath) + "/" + l.ToString("MM-yyyy")))
                        Directory.CreateDirectory(Path.GetFullPath(StatementsPath) + "/" + l.ToString("MM-yyyy"));

                    if (!Directory.Exists(Path.GetFullPath(StatementsPath) + "/" + l.ToString("MM-yyyy") + "_" + "yearlyReport"))
                        Directory.CreateDirectory(Path.GetFullPath(StatementsPath) + "/" + l.ToString("MM-yyyy") + "_" + "yearlyReport");

                    //if (!Directory.Exists(Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy")))
                    //{
                    //    Directory.CreateDirectory(Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy"));
                    //}
                    //if (!Directory.Exists(Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy") + "_" + "yearlyReport"))
                    //{
                    //    Directory.CreateDirectory(Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy") + "_" + "yearlyReport");
                    //}
                    FileStream fs = new FileStream(path, FileMode.CreateNew);
                    Document doc = new Document(PageSize.A4, 36, 36, 36, 135);
                    PdfWriter pdfWriter = PdfWriter.GetInstance(doc, fs);

                    doc.Open();

                    Response maHG = ServicesManager.GetMaHolderRegByAccountNo(accountNo);

                    PageEvent e = new PageEvent();
                    e.AddLogo(pdfWriter, doc);
                    e.AddHeader(pdfWriter, doc, maHG);
                    pdfWriter.PageEvent = e;

                    //table style
                    Font fontText = FontFactory.GetFont("ARIAL", 9);
                    Font fontTitle = FontFactory.GetFont("ARIAL", 9, BaseColor.WHITE);
                    BaseColor headBackgroundColor = BaseColor.DARK_GRAY;

                    // table title - Summary Portfolio
                    PdfPTable tableSummaryPortfolio = new PdfPTable(5);
                    tableSummaryPortfolio.WidthPercentage = 100;
                    int[] intTblWidth = { 30, 10, 20, 15, 15 };
                    tableSummaryPortfolio.SetWidths(intTblWidth);

                    tableSummaryPortfolio.Rows.Add(new PdfPRow(new PdfPCell[] {
                new PdfPCell(new Phrase("Trust Name", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                new PdfPCell(new Phrase("Trust Code", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                new PdfPCell(new Phrase("Units Held", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                new PdfPCell(new Phrase("Redemption Price (RM)", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                new PdfPCell(new Phrase("Current Market Value (RM)", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER}
            }));

                    //table content -  Summary Portfolio 
                    decimal totalCurrentMarketValue = 0;
                    int i = 0;
                    List<SummaryPortfolio> summaryPortfolioList = new List<SummaryPortfolio>();
                    //summaryPortfolioList = GenerateStatementService.GetAll(l, accountNo);

                    List<HolderLedger> holderLedgers = ServicesManager.GetHolderLedgerByHolderNo(" holder_no = '" + accountNo + "' and fund_id in ('01', '02', '03', '04', '05', '06', '07', '08', '09') ");

                    List<HolderLedger> holderLedgersFiltered = holderLedgers;
                    holderLedgers.ForEach(checkX =>
                    {
                        if (checkX.TransNO.Contains('X'))
                        {
                            string transNoX = checkX.TransNO;
                            string transNo = checkX.TransNO.TrimEnd('X');
                            holderLedgersFiltered = holderLedgersFiltered.Where(y => y.TransNO != transNo && y.TransNO != transNoX).ToList();
                        }
                    });
                    holderLedgersFiltered = holderLedgersFiltered.OrderBy(y => y.TransDt).ThenBy(y => Convert.ToInt32(y.TransNO)).ToList();
                    holderLedgersFiltered.Where(y => y.TransDt <= l).GroupBy(y => y.FundID).Select(grp => grp.ToList()).ToList().ForEach(grpByFund =>
                    {
                        UtmcFundInformation utmcFundInformation = utmcFundInformations.FirstOrDefault(y => y.IpdFundCode == grpByFund.FirstOrDefault().FundID);
                        DateTime l7 = l.AddDays(-7);
                        Response responseDNP = IUtmcDailyNavFundService.GetDataByFilter(" IPD_Fund_Code='" + grpByFund.FirstOrDefault().FundID + "' and Daily_NAV_Date > '" + l7.ToString("yyyy-MM-dd") + "' and Daily_NAV_Date <='" + l.ToString("yyyy-MM-dd") + "' ", 0, 0, true);
                        UtmcDailyNavFund utmcDailyNavFund = new UtmcDailyNavFund();
                        if (responseDNP.IsSuccess)
                        {
                            List<UtmcDailyNavFund> utmcDailyNavFunds = (List<UtmcDailyNavFund>)responseDNP.Data;
                            if(utmcDailyNavFunds.Count > 0)
                            {
                                utmcDailyNavFund = utmcDailyNavFunds.FirstOrDefault();
                            }
                        }
                        summaryPortfolioList.Add(new SummaryPortfolio
                        {
                            Fund_Name = utmcFundInformation.FundName,
                            Fund_Code = utmcFundInformation.FundCode,
                            Net_Cumulative_Closing_Balance_Units = grpByFund.Sum(y => y.TransUnits),
                            Market_Price_NAV = utmcDailyNavFund.DailyUnitPrice,
                            Current_Market_Value = grpByFund.Sum(z => z.TransUnits) * utmcDailyNavFund.DailyUnitPrice,
                        });
                    });
                    holderLedgersFiltered = holderLedgersFiltered.Where(y => y.TransDt <= l).ToList();
                    //List<UtmcCompositionalTransaction> utmcCompositionalTransactions = ServicesManager.GetCompositionalTransactionByHolderNo(accountNo, f.ToString("yyyy-MM-dd"), l.ToString("yyyy-MM-dd"));
                    //utmcCompositionalTransactions = utmcCompositionalTransactions.Where(y => y.DateOfTransaction.Value <= l).OrderBy(y => y.DateOfTransaction.Value).ThenBy(y => y.IpdUniqueTransactionId).ToList();
                    //utmcCompositionalTransactions.Where(y => y.ReportDate.Value <= l).GroupBy(y => y.IpdFundCode).Select(grp => grp.ToList()).ToList().ForEach(y =>
                    //{
                    //    summaryPortfolioList.Add(new SummaryPortfolio
                    //    {
                    //        Fund_Name = "",
                    //        Fund_Code = y.FirstOrDefault().IpdFundCode,
                    //        Net_Cumulative_Closing_Balance_Units = y.Sum(z => z.Units.Value),
                    //        Market_Price_NAV = y.Sum(z => z.Units.Value * z.TransPR),
                    //        Current_Market_Value = y.Sum(z => z.Units.Value * z.TransPR),
                    //    });
                    //});

                    List<string> fundCodeList = new List<string>();

                    foreach (SummaryPortfolio summaryPortfolio in summaryPortfolioList)
                    {
                        BaseColor contentBackgroundColor = i % 2 == 0 ? new BaseColor(236, 236, 236) : BaseColor.WHITE;
                        PdfPCell[] cells = new PdfPCell[] {
                    new PdfPCell(new Phrase(summaryPortfolio.Fund_Name.Capitalize(), fontText)){ BackgroundColor = contentBackgroundColor },
                    new PdfPCell(new Phrase(summaryPortfolio.Fund_Code, fontText)){ BackgroundColor = contentBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                    new PdfPCell(new Phrase(summaryPortfolio.Net_Cumulative_Closing_Balance_Units.ToString("#,##0.0000"), fontText)){ BackgroundColor = contentBackgroundColor, HorizontalAlignment = Element.ALIGN_RIGHT},
                    new PdfPCell(new Phrase(summaryPortfolio.Market_Price_NAV.ToString("#,##0.000000000"), fontText)){ BackgroundColor = contentBackgroundColor, HorizontalAlignment = Element.ALIGN_RIGHT},
                    new PdfPCell(new Phrase(summaryPortfolio.Current_Market_Value.ToString("#,##0.00"), fontText)){ BackgroundColor = contentBackgroundColor, HorizontalAlignment = Element.ALIGN_RIGHT},


                };
                        fundCodeList.Add(summaryPortfolio.Fund_Code);
                        tableSummaryPortfolio.Rows.Add(new PdfPRow(cells));
                        i++;
                        totalCurrentMarketValue += summaryPortfolio.Current_Market_Value;
                    }

                    Font fontTextBold = FontFactory.GetFont("ARIAL", 9);
                    BaseColor contentBackgroundColor2 = i % 2 == 0 ? new BaseColor(236, 236, 236) : BaseColor.WHITE;
                    tableSummaryPortfolio.Rows.Add(new PdfPRow(new PdfPCell[] {
                new PdfPCell(new Phrase("Total Portfolio Value (RM)", fontTextBold)) { BackgroundColor = contentBackgroundColor2, Colspan=4},
                new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColor2},
                new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColor2},
                new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColor2},
                new PdfPCell(new Phrase(totalCurrentMarketValue.ToString("#,##0.00"), fontTextBold)) { BackgroundColor = contentBackgroundColor2, HorizontalAlignment = Element.ALIGN_RIGHT}

        }));

                    // table title - Transaction
                    PdfPTable tableTransaction = new PdfPTable(8);
                    tableTransaction.WidthPercentage = 100;
                    int[] intTblWidth2 = { 12, 12, 10, 13, 14, 14, 12, 11 };
                    tableTransaction.SetWidths(intTblWidth2);
                    tableTransaction.Rows.Add(new PdfPRow(new PdfPCell[] {
                new PdfPCell(new Phrase("Trans Date", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                new PdfPCell(new Phrase("Processed Date", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                new PdfPCell(new Phrase("Trust Code", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                new PdfPCell(new Phrase("Trans No", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                new PdfPCell(new Phrase("Number of units", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                new PdfPCell(new Phrase("Unit Cost (RM)", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                new PdfPCell(new Phrase("Trans Amount (RM)", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                new PdfPCell(new Phrase("Servicing Agent", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER }
            }));

                    //table content - Transaction
                    int x = 0;
                    int count = 1;
                    decimal totalUnits = 0;
                    int countTransaction = 0;

                    List<TransactionByDate> balanceUnitsList = new List<TransactionByDate>();
                    //balanceUnitsList = GenerateStatementService.GetLastMonthSum(l, accountNo);

                    List<TransactionByDate> transactionByDateList = new List<TransactionByDate>();
                    //transactionByDateList = GenerateStatementService.GetAllTransaction(f, l, accountNo);

                    holderLedgersFiltered
                        //.Where(y => y.ReportDate.Value.ToString("yyyy-MM-dd") == l.ToString("yyyy-MM-dd"))
                        .GroupBy(y => y.FundID)
                        .Select(grp => grp.ToList())
                        .ToList().ForEach(y =>
                        {
                            balanceUnitsList.Add(new TransactionByDate
                            {
                                Sum_Units = y.Sum(z => z.TransUnits),
                                Report_Date = y.FirstOrDefault().TransDt,
                                Fund_Code = y.FirstOrDefault().FundID
                            });
                        });
                    holderLedgersFiltered
                        .ToList().ForEach(y =>
                        {
                            string[] transTypes = new string[] { "TR", "RD" };
                            transactionByDateList.Add(new TransactionByDate
                            {
                                Date_Of_Transaction = y.TransDt,
                                Date_Of_Settlement = y.EnterDt,
                                Fund_Code = y.FundID,
                                Fund_Name = "",
                                Transaction_Code = y.TransType,
                                Transaction_Name = "",
                                IPD_Unique_Transaction_ID = y.TransNO,
                                Units = y.TransUnits,
                                Unit_Cost_RM = Math.Round(transTypes.Contains(y.TransType) ? y.TransPr : y.TransAmt / y.TransUnits, 4),
                                Gross_Amount_RM = y.TransAmt,
                                Report_Date = y.TransDt,
                                Agent_Code = y.AgentCode
                            });
                        });

                    foreach (TransactionByDate balanceUnits in balanceUnitsList)
                    {
                        if (balanceUnits != null)
                        {
                            BaseColor contentBackgroundColorLightGrey = x % 2 == 0 ? new BaseColor(236, 236, 236) : BaseColor.WHITE;
                            PdfPCell[] cells = new PdfPCell[] {
                        new PdfPCell(new Phrase(balanceUnits.Report_Date.ToString("dd/MM/yyyy"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey, HorizontalAlignment = Element.ALIGN_CENTER },
                        new PdfPCell(new Phrase("B/f Balance", fontText)){ BackgroundColor = contentBackgroundColorLightGrey, HorizontalAlignment = Element.ALIGN_CENTER },
                        new PdfPCell(new Phrase(balanceUnits.Fund_Code.ToString(), fontText)){ BackgroundColor = contentBackgroundColorLightGrey, HorizontalAlignment = Element.ALIGN_CENTER },
                        new PdfPCell(new Phrase(" ", fontText)){ BackgroundColor = contentBackgroundColorLightGrey},
                        new PdfPCell(new Phrase(balanceUnits.Sum_Units.ToString("#,##0.0000"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey, HorizontalAlignment = Element.ALIGN_RIGHT },
                        new PdfPCell(new Phrase(" ", fontText)){ BackgroundColor = contentBackgroundColorLightGrey },
                        new PdfPCell(new Phrase(" ", fontText)){ BackgroundColor = contentBackgroundColorLightGrey },
                        new PdfPCell(new Phrase(" ", fontText)){ BackgroundColor = contentBackgroundColorLightGrey }
                    };
                            //totalUnits = balanceUnits.Sum_Units;


                            List<TransactionByDate> sorted = new List<TransactionByDate>();
                            sorted = transactionByDateList.Where(X => X.Fund_Code == balanceUnits.Fund_Code).ToList();

                            foreach (TransactionByDate transactionByDate in sorted)
                            {
                                if (transactionByDate != null)
                                {
                                    BaseColor contentBackgroundColorLightGrey2 = x % 2 == 0 ? new BaseColor(236, 236, 236) : BaseColor.WHITE;
                                    countTransaction++;
                                    PdfPCell[] cells2 = new PdfPCell[] {
                        new PdfPCell(new Phrase(transactionByDate.Date_Of_Transaction.ToString("dd/MM/yyyy"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_LEFT },
                        new PdfPCell(new Phrase(transactionByDate.Date_Of_Settlement.ToString("dd/MM/yyyy"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_LEFT },
                        new PdfPCell(new Phrase(transactionByDate.Fund_Code.ToString(), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_LEFT },
                        new PdfPCell(new Phrase((transactionByDate.Transaction_Code + transactionByDate.IPD_Unique_Transaction_ID), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_LEFT },
                        new PdfPCell(new Phrase(transactionByDate.Units.ToString("#,##0.0000"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_RIGHT },
                        new PdfPCell(new Phrase(transactionByDate.Unit_Cost_RM.ToString("#,##0.000000000"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_RIGHT  },
                        new PdfPCell(new Phrase(transactionByDate.Gross_Amount_RM.ToString("#,##0.00"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_RIGHT  },
                        new PdfPCell(new Phrase(transactionByDate.Agent_Code, fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_LEFT }
                            };
                                    tableTransaction.Rows.Add(new PdfPRow(cells2));
                                    totalUnits += transactionByDate.Units;
                                    x++;
                                }
                            }
                            //tableTransaction.Rows.Add(new PdfPRow(cells));
                            //x++;
                            if (sorted.Count != 0)
                            {
                                BaseColor contentBackgroundColorGrey = new BaseColor(220, 220, 220);
                                BaseColor contentBackgroundColor = BaseColor.WHITE;
                                tableTransaction.Rows.Add(new PdfPRow(new PdfPCell[] {
                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, BorderWidthTop=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, BorderWidthTop=0, BorderWidthRight=0, BorderWidthLeft=0},
                        new PdfPCell(new Phrase("No of transaction: " + countTransaction, fontTextBold)) { BackgroundColor = contentBackgroundColorGrey, Colspan=2, BorderWidthRight=0, BorderWidthTop=0, BorderWidthLeft=0},
                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, Border=0, BorderWidthTop=0},
                        new PdfPCell(new Phrase(totalUnits.ToString("#,##0.0000"), fontTextBold)) { BackgroundColor = contentBackgroundColorGrey, BorderWidthTop=0, HorizontalAlignment = Element.ALIGN_RIGHT},
                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, BorderWidthLeft=0,  BorderWidthTop=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, BorderWidthLeft=0,  BorderWidthTop=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, BorderWidthLeft=0,  BorderWidthTop=0}

                    }));

                                BaseColor contentBackgroundColor3 = BaseColor.WHITE;
                                tableTransaction.Rows.Add(new PdfPRow(new PdfPCell[] {
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0}
                     }));

                            }



                            count++;
                            countTransaction = 0;
                            totalUnits = 0;
                        }
                    }


                    tableTransaction.DeleteLastRow();

                    tableSummaryPortfolio.SpacingBefore = 5f;
                    tableSummaryPortfolio.SpacingAfter = 12.5f;
                    tableTransaction.SpacingBefore = 5f;
                    tableTransaction.SpacingAfter = 12.5f;

                    doc.Add(tableSummaryPortfolio);
                    e.AddHeader2(pdfWriter, doc, f, l);
                    doc.Add(tableTransaction);
                    e.AddInformation(doc);
                    e.OnEndPage(pdfWriter, doc);
                    pdfWriter.CloseStream = true;
                    doc.Close();
                    e.AddFooter(path, doc);

                    fs.Close();

                    us.user_id = userID;
                    us.user_account_id = Convert.ToInt32(userAID);
                    us.user_statment_category_id = statementCategory;
                    us.name = accountNo + "_" + pdfDate + ".pdf";

                    if (statementCategory == 1)
                        us.url = "/Statements/" + l.ToString("MM-yyyy") + "/" + accountNo + "_" + pdfDate + ".pdf";
                    else if (statementCategory == 3)
                        us.url = "/Statements/" + l.ToString("MM-yyyy") + "_" + "yearlyReport" + "/" + accountNo + "_" + pdfDate + ".pdf";
                    //us.url = "/" + DateTime.Now.ToString("MM-yyyy") + "/" + accountNo + "_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".pdf";
                    us.createdate = currenttime2;
                    us.statementdate = l;
                    us.status = 1;
                    us = UserStatementService.Insert(us);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("GenerateReport: Exception - " + ex.Message);
            }
        }

    }
}
