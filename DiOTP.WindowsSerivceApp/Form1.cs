﻿using DiOTP.OracleData;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiOTP.WindowsSerivceApp
{
    public partial class Form1 : Form
    {

        public bool isProcessingDaily;
        public bool isProcessedDaily;
        public DateTime processedDate;
        private BackgroundWorker backgroundWorkerProcessDaily;
        string DailyProcessSchedule = ConfigurationManager.AppSettings["DailyProcessSchedule"].ToString();

        string Day = ConfigurationManager.AppSettings["Day"].ToString();
        //string Time = ConfigurationManager.AppSettings["Time"].ToString();
        string YearlyMonth = ConfigurationManager.AppSettings["Month"].ToString();
        string EveryDayTime = ConfigurationManager.AppSettings["EveryDayTime"].ToString();
        public string ApiBaseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
        public string FPXPaymentStatusUrl = ConfigurationManager.AppSettings["FPXPaymentStatusUrl"].ToString();
        public string FPXkey = ConfigurationManager.AppSettings["FPXkey"].ToString();
        public string FundsUpdatingTime = ConfigurationManager.AppSettings["FundsUpdatingTime"].ToString();

        int Year = DateTime.Now.Year;
        private System.Timers.Timer timer;
        private System.Timers.Timer updatetimer;

        public bool iscallCompleted = true;

        private static readonly Lazy<IUserOrderService> lazyIUserService = new Lazy<IUserOrderService>(() => new UserOrderService());
        public static IUserOrderService IUserOrderService { get { return lazyIUserService.Value; } }

        private static readonly Lazy<IUserService> lazyObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyObj.Value; } }

        public Form1()
        {

            InitializeComponent();

            InitializeBackgroundWorker();

            label1.Text = "Time Scheduler Runing";
            timer1.Enabled = true;
            DateTime fromDate = new DateTime(2017, 12, 31);
            DateTime toDate = new DateTime(2019, 1, 1);
            //DateTime toDate = DateTime.Now;
            StatementService.GenerateYearlyReport(fromDate, toDate);
            fromDate = new DateTime(2018, 11, 30);
            //toDate = new DateTime(2019, 1, 1);
            StatementService.GenerateMonthlyReport(fromDate, toDate);
            //// this is system timer
            this.timer = new System.Timers.Timer(3600000D);  // 3600000D milliseconds = 1 hour
            //this.timer = new System.Timers.Timer(10000);  // 3600000D milliseconds = 1 hour
            this.timer.AutoReset = true;
            this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.DaysTimeScheduler);
            this.timer.Start();

            this.updatetimer = new System.Timers.Timer(10000);  // 10000 milliseconds = 1 min
            this.updatetimer.AutoReset = true;
            this.updatetimer.Elapsed += new System.Timers.ElapsedEventHandler(this.UpdateBackendOrderStatus);
            this.updatetimer.Start();

            //// this one Threading timer
            //TimeScheduler();
            label2.Text = "Started On :" + DateTime.Now.ToString();
        }

        private void InitializeBackgroundWorker()
        {
            backgroundWorkerProcessDaily = new BackgroundWorker();
            backgroundWorkerProcessDaily.DoWork += new DoWorkEventHandler(backgroundWorkerProcessDaily_DoWork);
            backgroundWorkerProcessDaily.WorkerReportsProgress = false;
            backgroundWorkerProcessDaily.WorkerSupportsCancellation = true;
            backgroundWorkerProcessDaily.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorkerProcessDaily_RunWorkerCompleted);
            backgroundWorkerProcessDaily.RunWorkerAsync();
        }

        private void backgroundWorkerProcessDaily_DoWork(object sender, DoWorkEventArgs e)
        {
            isProcessingDaily = true;
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = DailyProcess(worker, e);
        }
        private void backgroundWorkerProcessDaily_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                //MessageBox.Show(e.Error.Message);
            }
            else if (e.Cancelled)
            {
            }
            else
            {
                isProcessingDaily = false;
                backgroundWorkerProcessDaily.RunWorkerAsync();
            }
        }

        public object DailyProcess(BackgroundWorker worker, DoWorkEventArgs e)
        {
            string currentTime = DateTime.Now.ToString("HH:mm");
            if (currentTime == DailyProcessSchedule && DateTime.Now.ToString("dd/MM/yyyy") != processedDate.ToString("dd/MM/yyyy"))
            {
                Logger.WriteLog("DailyProcess runing");
                //Code
                processedDate = DateTime.Now;
                isProcessedDaily = true;
                Logger.WriteLog("DailyProcess end");
            }
            else
            {
                //Do Nothing
            }
            return "";
        }


        public void TimeScheduler()
        {
            Logger.WriteLog("Time Scheduler started");

            //// For Interval in Seconds 
            //// This Scheduler will start at 10:10 and call after every 60 Seconds
            //// IntervalInSeconds(start_hour, start_minute, seconds)
            SchedulerModel.IntervalInSeconds(DateTime.Now.Hour, DateTime.Now.Minute + 1, 60,
            () =>
            {
                // SecondsScheduler();
                DaysScheduler();
            });

            //// For Interval in Minutes 
            //// This Scheduler will start at 10:10 and call after every 5 Minutes
            //// IntervalInMinutes(start_hour, start_minute, seconds)
            //SchedulerModel.IntervalInMinutes(10, 10, 5,
            //() =>
            //{
            //   // MinutesScheduler();
            //});

            //// For Interval in Hours 
            //// This Scheduler will start at 10:10 and call after every 1 Hour
            //// IntervalInHours(start_hour, start_minute, seconds)
            //SchedulerModel.IntervalInHours(10, 10, 1,
            //() =>
            //{
            //  //  HoursScheduler();
            //});

            //// For Interval in days 
            //// This Scheduler will start at 10:10 and call after every 1 day
            //// IntervalInDays(start_hour, start_minute, seconds)
            //SchedulerModel.IntervalInDays(10,10, 1,
            //() =>
            //{
            //   // DaysScheduler();
            //});

            //// For Interval in Hours 
            //// This Scheduler will start at 10:10 and call after every 1 Hour
            //// IntervalInHours(start_hour, start_minute, seconds)
            SchedulerModel.IntervalInHours(DateTime.Now.Hour, DateTime.Now.Minute + 1, 1,
            () =>
            {
                //DaysScheduler();
            });


        }
        bool isMonthlyStarted = false;
        public void DaysScheduler()
        {
            Logger.WriteLog("DaysScheduler runing");
            // Every day runing this method.
            if (EveryDayTime == DateTime.Now.Hour.ToString())
            {
                // write method
            }
            if (Day == DateTime.Now.Day.ToString())
            {

            }
            if (Day == DateTime.Now.Day.ToString() && YearlyMonth == DateTime.Now.Month.ToString())
            {
                DateTime now = DateTime.Now;
                var startDate = new DateTime(now.Year, now.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);
                Logger.WriteLog("GenerateMonthlyReport startDate : " + startDate + " endDate : " + endDate);
                if (!isMonthlyStarted)
                {
                    isMonthlyStarted = true;
                    StatementService.GenerateMonthlyReport(startDate, endDate);
                }
            }
            if (YearlyMonth == DateTime.Now.Month.ToString() && Year == DateTime.Now.Year)
            {
                DateTime now = DateTime.Now;
                var startDate = new DateTime(now.Year, now.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);
                if (!string.IsNullOrEmpty(YearlyMonth) && int.TryParse(YearlyMonth, out int n))
                {
                    endDate = new DateTime(now.Year, Convert.ToInt32(YearlyMonth), 1);
                    if (YearlyMonth == "12")
                        startDate = new DateTime(now.Year, 1, 1);
                    else
                        startDate = new DateTime(now.Year - 1, Convert.ToInt32(YearlyMonth) + 1, 1);
                    Logger.WriteLog("GenerateYearlyReport startDate : " + startDate + " endDate : " + endDate);
                    Year++;
                    StatementService.GenerateYearlyReport(startDate, endDate);
                }

            }
        }

        public void DaysTimeScheduler(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //Logger.WriteLog("DaysScheduler runing");
                if (Day == DateTime.Now.Day.ToString() && !isMonthlyStarted)
                {
                    isMonthlyStarted = true;
                    DateTime now = DateTime.Now;
                    var startDate = new DateTime(now.Year, now.Month, 1);
                    var endDate = startDate.AddMonths(1).AddDays(-1);
                    Logger.WriteLog("GenerateMonthlyReport startDate : " + startDate + " endDate : " + endDate);
                    GenerateStatementService.GenerateMonthlyReport(startDate, endDate);
                    isMonthlyStarted = false;
                }
                if (YearlyMonth == DateTime.Now.Month.ToString() && Year == DateTime.Now.Year)
                {
                    DateTime now = DateTime.Now;
                    var startDate = new DateTime(now.Year, now.Month, 1);
                    var endDate = startDate.AddMonths(1).AddDays(-1);
                    if (!string.IsNullOrEmpty(YearlyMonth) && int.TryParse(YearlyMonth, out int n))
                    {
                        endDate = new DateTime(now.Year, Convert.ToInt32(YearlyMonth), 1);
                        if (YearlyMonth == "12")
                            startDate = new DateTime(now.Year, 1, 1);
                        else
                            startDate = new DateTime(now.Year - 1, Convert.ToInt32(YearlyMonth) + 1, 1);
                        Logger.WriteLog("GenerateYearlyReport startDate : " + startDate + " endDate : " + endDate);
                        Year++;
                        GenerateStatementService.GenerateYearlyReport(startDate, endDate);
                    }

                }
                if (DateTime.Now.Hour.ToString() == FundsUpdatingTime)
                {
                    Inser_Fund_Nav();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("DaysTimeScheduler exception :" + ex.Message);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToString();
            timer1.Stop();

        }

        public void UpdateBackendOrderStatus(object sender, System.Timers.ElapsedEventArgs e)
        {
            //Logger.WriteLog("Call Status: " + iscallCompleted);
            try
            {
                if (iscallCompleted)
                {
                    iscallCompleted = false;
                    // Get the user success orders
                    var userOrders = IUserOrderService.GetDataByFilter(" order_status=2 and fpx_status=99 ", 0, 0, false);
                    if (userOrders.IsSuccess && userOrders.Data != null)
                    {
                        List<UserOrder> list = (List<UserOrder>)userOrders.Data;
                        //string refNos = String.Join(",", list.Select(a => "'" + a.RejectReason + "'").ToArray());
                        //refNos = "'224446'";
                        if (list != null && list.Count != 0)
                        {
                            bool ispaymentcallCompleted = true;
                            foreach (UserOrder order in list)
                            {
                                if (ispaymentcallCompleted)
                                {
                                    ispaymentcallCompleted = false;
                                    Response responseUList = IUserService.GetDataByPropertyName(nameof(Utility.User.Id), order.UserId.ToString(), true, 0, 0, false);
                                    if (responseUList.IsSuccess && responseUList.Data != null)
                                    {
                                        User user = ((List<User>)responseUList.Data).FirstOrDefault();
                                        FPXPaymentH(order.OrderNo, order.TransNo, Math.Round(order.Amount, 2).ToString(), user.EmailId, user.Username, "0", "0", "APEX", "0", order.BankCode);
                                    }
                                }
                                ispaymentcallCompleted = true;
                            }
                        }
                        // oracle databse statu updating
                        //// Get data from oracle Db
                        //List<HolderLedger> holderLedgerList = GetHolderLedger(refNos);
                        ////var data = NewRepoClass.GetDataByFilter<HolderLedger>(" trans_no in (" + refNos + ")", 0, 0, false, true, null, false);
                        ////var holderData = (List<HolderLedger>)data.Data;
                        //Logger.WriteLog("Db Holder Ledger count : " + holderLedgerList.Count);

                        //if (holderLedgerList != null && holderLedgerList.Count != 0)
                        //{
                        //    // Status Update function 
                        //}
                    }
                    iscallCompleted = true;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UpdateBackendOrderStatus : " + ex.Message);
            }
        }
        public List<HolderLedger> GetHolderLedger(string refNos)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ApiBaseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    StringBuilder filter = new StringBuilder();
                    var response = client.GetAsync("api/HolderLedger?filter=" + refNos).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        Response responseFromAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                        if (responseFromAPI.IsSuccess)
                        {
                            List<HolderLedger> holderLedger = Newtonsoft.Json.JsonConvert.DeserializeObject<List<HolderLedger>>(responseFromAPI.Data.ToString());
                            return holderLedger;
                        }
                    }
                    else
                    {
                        //responseCustom.IsSuccess = false;
                        //responseCustom.Message = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                //responseCustom.IsSuccess = false;
                //responseCustom.Message = ex.Message;
            }
            return new List<HolderLedger>();
        }

        public string UpdatePaymentStatus(string orderNo)
        {
            try
            {
                string baseURL = ApiBaseURL;
                string destinationUrl = baseURL + "?orderNo=" + orderNo + "&transDate=test&txn_amt=100&buyer_mail_id=test@gmail.com&buyerName=test&buyerBankBranch=sc01&buyerAccNo=ABC123&makerName=test&buyerIBAN=test&buyerBankId=20";
                HttpWebRequest request = (System.Net.HttpWebRequest)WebRequest.Create(destinationUrl);
                request.ContentType = "text/xml; encoding='utf-8'";
                request.Method = "POST";
                request.Timeout = Timeout.Infinite;

            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ((HttpWebResponse)ex.Response);
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
            }
            return "";
        }

        public string Inser_Fund_Nav()
        {
            try
            {
                string baseURL = "http://localhost:1205/api/";
                string destinationUrl = baseURL + "api/PaymentApi/InsertFundInfo";
                HttpWebRequest request = (System.Net.HttpWebRequest)WebRequest.Create(destinationUrl);
                request.Method = "GET";
                request.Timeout = Timeout.Infinite;
                using (var response = request.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        using (var sr = new StreamReader(responseStream))
                        {
                            string responseStr = sr.ReadToEnd();
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ((HttpWebResponse)ex.Response);
                    Logger.WriteLog("Inser_Fund_Nav WebException :" + response.StatusDescription);
                }
                else
                {
                    Logger.WriteLog("Inser_Fund_Nav Exception :" + ex.Message);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Inser_Fund_Nav Exception :" + ex.Message);
            }
            return "";
        }
        public string FPXPaymentH(string orderNo, string transDate, string txn_amt,
          string buyer_mail_id, string buyerName, string buyerBankBranch,
          string buyerAccNo, string makerName, string buyerIBAN, string buyerBankId)
        {

            Controller c = new Controller();
            String checksum = "";
            Random RandomNumber = new Random();
            String fpx_msgType = "AE";
            String fpx_msgToken = "01";
            String fpx_sellerExId = "EX00008813";
            String fpx_sellerExOrderNo = orderNo;
            String fpx_sellerOrderNo = orderNo;
            String fpx_sellerTxnTime = transDate;
            String fpx_sellerId = "SE00010202";
            String fpx_sellerBankCode = "01";
            String fpx_txnCurrency = "MYR";
            String fpx_txnAmount = txn_amt;
            String fpx_buyerEmail = buyer_mail_id;
            String fpx_buyerId = buyer_mail_id;
            String fpx_buyerName = buyerName;
            String fpx_buyerBankId = buyerBankId;
            String fpx_buyerBankBranch = buyerBankBranch;
            String fpx_buyerAccNo = buyerAccNo;
            String fpx_makerName = makerName;
            String fpx_buyerIban = buyerIBAN;
            String fpx_productDesc = "PD";
            String fpx_version = "6.0";
            String fpx_checkSum = "";
            fpx_checkSum = fpx_buyerAccNo + "|" + fpx_buyerBankBranch + "|" + fpx_buyerBankId + "|" + fpx_buyerEmail + "|" + fpx_buyerIban + "|" + fpx_buyerId + "|" + fpx_buyerName + "|";
            fpx_checkSum += fpx_makerName + "|" + fpx_msgToken + "|" + fpx_msgType + "|" + fpx_productDesc + "|" + fpx_sellerBankCode + "|" + fpx_sellerExId + "|";
            fpx_checkSum += fpx_sellerExOrderNo + "|" + fpx_sellerId + "|" + fpx_sellerOrderNo + "|" + fpx_sellerTxnTime + "|" + fpx_txnAmount + "|" + fpx_txnCurrency + "|" + fpx_version;
            fpx_checkSum = fpx_checkSum.Trim();

            checksum = c.RSASign(fpx_checkSum, FPXkey + "EX00008813.key"); //Exchange Key name 
            String finalMsg = fpx_checkSum;

            string rawString = "txn_amt=" + fpx_txnAmount + "&";
            rawString += "fpx_msgType=" + fpx_msgType + "&";
            rawString += "fpx_msgToken=" + fpx_msgToken + "&";
            rawString += "fpx_sellerExId=" + fpx_sellerExId + "&";
            rawString += "fpx_sellerExOrderNo=" + fpx_sellerExOrderNo + "&";
            rawString += "fpx_sellerTxnTime=" + fpx_sellerTxnTime + "&";
            rawString += "fpx_sellerOrderNo=" + fpx_sellerOrderNo + "&";
            rawString += "fpx_sellerBankCode=" + fpx_sellerBankCode + "&";
            rawString += "fpx_txnCurrency=" + fpx_txnCurrency + "&";
            rawString += "fpx_txnAmount=" + fpx_txnAmount + "&";
            rawString += "fpx_buyerEmail=&";
            rawString += "fpx_checkSum=" + checksum + "&";
            rawString += "fpx_buyerName=" + buyerName + "&";
            rawString += "fpx_buyerBankId=" + buyerBankId + "&";
            rawString += "fpx_buyerBankBranch=" + buyerBankBranch + "&";
            rawString += "fpx_buyerAccNo=" + buyerAccNo + "&";
            rawString += "fpx_buyerId=" + fpx_buyerId + "&";
            rawString += "fpx_makerName=" + makerName + "&";
            rawString += "fpx_buyerIban=" + fpx_buyerIban + "&";
            rawString += "fpx_productDesc=" + fpx_productDesc + "&";
            rawString += "fpx_version=" + fpx_version + "&";
            rawString += "fpx_sellerId=" + fpx_sellerId + "&";
            rawString += "checkSum_String=" + checksum;


            WebRequest request = WebRequest.Create(FPXPaymentStatusUrl);
            //WebRequest request = WebRequest.Create("https://uat.mepsfpx.com.my/FPXMain/sellerNVPTxnStatus.jsp");
            request.Method = "POST";
            //byte[] byteArray = Encoding.UTF8.GetBytes("txn_amt=3450.00&fpx_msgType=AE&fpx_msgToken=01&fpx_sellerExId=EX00008813&fpx_sellerExOrderNo=SA1800000010&fpx_sellerTxnTime=20190514175842&fpx_sellerOrderNo=SA1800000010&fpx_sellerBankCode=01&fpx_txnCurrency=MYR&fpx_txnAmount=3450.00&fpx_buyerEmail=&fpx_checkSum=B2313F3B1A1075581B13880707A074B8CB8E380D6C4FD651281468B18E034E63E7CE40440E56B76D86368340FFE38B723394BF91DE7DE8D3ACDEDEA77FE52A51B0F5A5292A35C7870A66E1949386B551CB338C3CAA3217FACE2CCC5C3EEAD83FF4085B07E828AB78E429BAC0C55D3180DC999EF84892493E84FC9A6D94165A431C66DF02A420D75448D97E2017E93C787630274DA114FF567B5F667802F297D3D852A0F6C075D59484ECF2A47BF54932082FA3DEF695ECF4A0A99E959391396DAD360C8C101233E7ACF97106CFC2E479A6084715944B7B21B824EF9D6E37EA09A44669BC3861421C5F52CA7827F508E862517DDA87B45DB875C4104A8C550994&fpx_buyerName=&fpx_buyerBankId=TEST0021&fpx_buyerBankBranch=0&fpx_buyerAccNo=0&fpx_buyerId=&fpx_makerName=APEX&fpx_buyerIban=0&fpx_productDesc=PD&fpx_version=6.0&fpx_sellerId=SE00010202&checkSum_String=B2313F3B1A1075581B13880707A074B8CB8E380D6C4FD651281468B18E034E63E7CE40440E56B76D86368340FFE38B723394BF91DE7DE8D3ACDEDEA77FE52A51B0F5A5292A35C7870A66E1949386B551CB338C3CAA3217FACE2CCC5C3EEAD83FF4085B07E828AB78E429BAC0C55D3180DC999EF84892493E84FC9A6D94165A431C66DF02A420D75448D97E2017E93C787630274DA114FF567B5F667802F297D3D852A0F6C075D59484ECF2A47BF54932082FA3DEF695ECF4A0A99E959391396DAD360C8C101233E7ACF97106CFC2E479A6084715944B7B21B824EF9D6E37EA09A44669BC3861421C5F52CA7827F508E862517DDA87B45DB875C4104A8C550994");
            byte[] byteArray = Encoding.UTF8.GetBytes(rawString);

            request.ContentLength = byteArray.Length;

            request.ContentType = "application/x-www-form-urlencoded";

            Stream dataStream = request.GetRequestStream();

            dataStream.Write(byteArray, 0, byteArray.Length);

            dataStream.Close();

            WebResponse response = request.GetResponse();

            Console.WriteLine(((HttpWebResponse)response).StatusDescription);

            Stream dataStreamRes = response.GetResponseStream();

            using (dataStream = response.GetResponseStream())
            {
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                string responseFromServer = reader.ReadToEnd();
                // Display the content.  
                Console.WriteLine(responseFromServer);
                if (!string.IsNullOrEmpty(responseFromServer))
                {
                    string[] responseList = responseFromServer.Split('&');
                    if (responseList != null && responseList.Count() != 0)
                    {
                        string[] statusList = responseList[0].Split('=');
                        return statusList[2];
                    }
                }
            }
            // Close the response.  
            response.Close();
            return "";
        }



    }
}
