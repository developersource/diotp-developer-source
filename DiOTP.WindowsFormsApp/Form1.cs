﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.Caching;
using System.Web;
using System.Threading;
using System.Net.Mail;
using System.Configuration;
using DiOTP.Service.IService;
using DiOTP.Service;
using DiOTP.Utility.Helper;
using System.Net;
using System.IO;

namespace DiOTP.WindowsFormsApp
{
    public partial class Form1 : Form
    {
        string Day = ConfigurationManager.AppSettings["Day"].ToString();
        //string Time = ConfigurationManager.AppSettings["Time"].ToString();
        string YearlyMonth = ConfigurationManager.AppSettings["Month"].ToString();
        string EveryDayTime = ConfigurationManager.AppSettings["EveryDayTime"].ToString();
        public string ApiBaseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
        public string FundsUpdatingTime = ConfigurationManager.AppSettings["FundsUpdatingTime"].ToString();

        int Year = DateTime.Now.Year;
        private System.Timers.Timer timer;
        int Month = DateTime.Now.Month;
        public bool iscallCompleted = true;

        private static readonly Lazy<IUserOrderService> lazyIUserService = new Lazy<IUserOrderService>(() => new UserOrderService());
        public static IUserOrderService IUserOrderService { get { return lazyIUserService.Value; } }

        private static readonly Lazy<IUserService> lazyObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyObj.Value; } }
        public Form1()
        {
            InitializeComponent();
            txtEmail.Visible = false;
            btnSend.Visible = false;
            this.timer = new System.Timers.Timer(50000);  // 3600000D milliseconds = 1 hour
            this.timer.AutoReset = true;
            this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.DaysTimeScheduler);
            this.timer.Start();
        }


        private void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtEmail.Text.Trim() != "")
                {
                    MailMessage message = new MailMessage("apex-noreply@apexis.com.my", txtEmail.Text.Trim(), "Hi", "Welcome");
                    message.IsBodyHtml = true;
                    SmtpClient emailClient = new SmtpClient("192.168.3.13");
                    emailClient.Send(message);
                    lblMsg.Text = "Success. Please check your email.";
                    MessageBox.Show("Success");
                }
                else
                {
                    MessageBox.Show("Please enter email.");
                }
            }
            catch (Exception ex)
            {
                lblMsg.Text = ex.Message;
                MessageBox.Show(ex.Message);
            }
        }
        bool isMonthlyStarted = false;
        public void DaysTimeScheduler(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //Logger.WriteLog("DaysScheduler runing");
                if (Day == DateTime.Now.Day.ToString() && Month == DateTime.Now.Month && !isMonthlyStarted)
                {
                    isMonthlyStarted = true;
                    DateTime now = DateTime.Now;
                    var startDate = new DateTime(now.Year, now.Month, 1);
                    var endDate = startDate.AddMonths(1).AddDays(-1);
                    Logger.WriteLog("GenerateMonthlyReport startDate : " + startDate + " endDate : " + endDate);
                    GenerateStatementService.GenerateMonthlyReport(startDate, endDate);
                    isMonthlyStarted = false;
                    Month++;
                }
                if (YearlyMonth == DateTime.Now.Month.ToString() && Day == DateTime.Now.Day.ToString() && DateTime.Now.Year == Year)
                {
                    DateTime now = DateTime.Now;
                    var startDate = new DateTime(now.Year, now.Month, 1);
                    var endDate = startDate.AddMonths(1).AddDays(-1);
                    if (!string.IsNullOrEmpty(YearlyMonth) && int.TryParse(YearlyMonth, out int n))
                    {
                        endDate = new DateTime(now.Year, Convert.ToInt32(YearlyMonth), 1);
                        if (YearlyMonth == "12")
                            startDate = new DateTime(now.Year, 1, 1);
                        else
                            startDate = new DateTime(now.Year - 1, Convert.ToInt32(YearlyMonth) + 1, 1);
                        Logger.WriteLog("GenerateYearlyReport startDate : " + startDate + " endDate : " + endDate);
                        Year++;
                        GenerateStatementService.GenerateYearlyReport(startDate, endDate);
                    }
                    Year++;
                }
                if (DateTime.Now.Hour.ToString() == FundsUpdatingTime)
                {
                    Inser_Fund_Nav();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("DaysTimeScheduler exception :" + ex.Message);
            }
        }
        public string Inser_Fund_Nav()
        {
            try
            {
                string baseURL = "http://localhost:1205/api/";
                string destinationUrl = baseURL + "api/PaymentApi/InsertFundInfo";
                HttpWebRequest request = (System.Net.HttpWebRequest)WebRequest.Create(destinationUrl);
                request.Method = "GET";
                request.Timeout = Timeout.Infinite;
                using (var response = request.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        using (var sr = new StreamReader(responseStream))
                        {
                            string responseStr = sr.ReadToEnd();
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ((HttpWebResponse)ex.Response);
                    Logger.WriteLog("Inser_Fund_Nav WebException :" + response.StatusDescription);
                }
                else
                {
                    Logger.WriteLog("Inser_Fund_Nav Exception :" + ex.Message);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Inser_Fund_Nav Exception :" + ex.Message);
            }
            return "";
        }
    }
}
