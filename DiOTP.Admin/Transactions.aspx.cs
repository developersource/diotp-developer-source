﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using OfficeOpenXml;
using MoreLinq;
using DiOTP.Utility.CustomClasses;
using System.Globalization;
using static DiOTP.Utility.CustomClasses.CustomStatus;
using Newtonsoft.Json;
using Admin.ServiceCalls;
using DiOTP.Utility.OracleDTOs;
using System.Configuration;

namespace Admin
{
    public partial class Transactions : System.Web.UI.Page
    {
        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());
        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserOrderFileService> lazyUserOrderFileObj = new Lazy<IUserOrderFileService>(() => new UserOrderFileService());
        public static IUserOrderFileService IUserOrderFileService { get { return lazyUserOrderFileObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder filter = new StringBuilder();
            string mainQ = (@"select uo.order_status, uo.ID, uo.order_type, uo.user_id, u.username, uo.user_account_id, uo.settlement_date, ua.account_no, uo.order_no, uo.amount, uo.units, uo.payment_method, uo.created_date, uo.updated_date, group_concat(uo.reject_reason ORDER BY uo.id) as reject_reason, sum(uo.amount) as SumAmount, sum(uo.units) as SumUnits, uo.fpx_bank_code, count(uo.order_no ) as no_of_items, if(uo.order_type = 3, group_concat(ufi.Fund_Name , ',', ufi2.Fund_Name ORDER BY uo.id), group_concat(ufi.Fund_Name ORDER BY uo.id)) as fund_names, group_concat(uo.consultantID ORDER BY uo.id) as ConsultantId from ");
            string mainQCount = (@"select count(*) from ( ");
            filter.Append(@" user_orders uo
                            join users u on u.id = uo.user_id
                            join user_accounts ua on ua.id = uo.user_account_id
                            join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            where uo.order_type not in (4, 6) ");

            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["pageLength"]))
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["pageLength"]))
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FromDate"]))
                {
                    FromDate.Value = Request.QueryString["FromDate"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ToDate"]))
                {
                    ToDate.Value = Request.QueryString["ToDate"].ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    if (Search.Value.ToLower() == "buy")
                    {
                        filter.Append(" and uo.order_type = 1 ");
                    }
                    else if (Search.Value.ToLower() == "sell")
                    {
                        filter.Append(" and uo.order_type = 2 ");
                    }
                    else if (Search.Value.ToLower() == "switch")
                    {
                        filter.Append(" and uo.order_type in ('3', '4') ");
                    }
                    else
                    {
                        filter.Append(" and (u.username like '%" + Search.Value.Trim() + "%'");
                        filter.Append(" or u.id_no like '%" + Search.Value.Trim() + "%' ");
                        filter.Append(" or uo.order_no like '%" + Search.Value.Trim() + "%' ");
                        filter.Append(" or uo.fpx_bank_code like '%" + Search.Value.Trim() + "%' ");
                        filter.Append(" or uo.reject_reason like '%" + Search.Value.Trim() + "%' ");
                        filter.Append(" or ua.account_no like '%" + Search.Value.Trim() + "%') ");
                    }

                }

                if (IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    if (FilterValue.Value != "All")
                        filter.Append(" and uo.order_status in ('" + FilterValue.Value + "') ");
                    else
                        filter.Append(" and uo.order_status IN('2', '3', '39') ");
                }
                else
                {
                    filter.Append(" and uo.order_status IN ('2') ");
                }
                if (!string.IsNullOrEmpty(FromDate.Value) && !string.IsNullOrEmpty(ToDate.Value))
                {
                    DateTime from = DateTime.ParseExact(FromDate.Value.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime to = DateTime.ParseExact(ToDate.Value.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    if (FromDate.Value.Trim() == ToDate.Value.Trim())
                    {
                        to = to.AddDays(1);
                    }
                    else if (from > to)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                                "alert('From date cannot be greater than To date.');", true);
                    }
                    filter.Append(" and uo.created_date between '" + from.ToString("yyyy-MM-dd") + "' and '" + to.ToString("yyyy-MM-dd") + "'");
                }
                filter.Append(" group by uo.order_no ");
                filter.Append(" order by FIELD(uo.order_status, '2', '3', '39'), uo.confirmation_date desc ");
                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + mainQ + filter.ToString() + ") countIn ").Data.ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseUOList = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, false);
                if (responseUOList.IsSuccess)
                {
                    var uoDyn = responseUOList.Data;
                    var responseJSON = JsonConvert.SerializeObject(uoDyn);
                    List<UserOrderCustom> userOrders = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserOrderCustom>>(responseJSON);

                    string directoryPath = ConfigurationManager.AppSettings["siteURL"].ToString();

                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    userOrders.ForEach(u =>
                    {
                        string checkBox = string.Empty;

                        DateTime dateTime = u.CreatedDate;
                        if (u.OrderStatus == (int)Order_Status.Order_Approved || u.OrderStatus == (int)Order_Status.Order_Rejected || u.OrderStatus == (int)Order_Status.Payment_Rejected)
                        {
                            checkBox = @"<label>" + index + @"</label><br/>";
                        }
                        else
                        {
                            checkBox = @"<div class='square single-row'>
                                                    <div class='checkbox'>
                                                        <input type='checkbox' name='checkRow' class='checkRow' data-type='" + u.OrderType + "' data-itemcount='" + u.NoOfItems + @"' data-items='" + u.FundNames.Capitalize() + "' value='" + u.Id + @"' /> <label>" + index + @"</label><br/>
                                                    </div>
                                                </div>";
                        }

                        string url = "";
                        Response responseUOFList = IUserOrderFileService.GetDataByPropertyName(nameof(UserOrderFile.OrderNo), u.OrderNo, true, 0, 0, true);
                        if (responseUOFList.IsSuccess)
                        {
                            UserOrderFile uof = ((List<UserOrderFile>)responseUOFList.Data).FirstOrDefault();
                            if (uof != null)
                                url = uof.Url;
                        }

                        string[] reasons = u.RejectReason.Split(',');

                        reasons = reasons.Distinct().ToArray();

                        string rejectReason = String.Join(",", reasons);
                        Type t = GetType("FPXBank");
                        string BankName = u.FpxBankCode;
                        FPXBank b = FPXEnumeration.GetByBankId(t, BankName);
                        asb.Append(@"<tr>
                                            <td class='icheck'>
                                                " + checkBox + @"
                                                <span class='row-status'>
                                                    " + (u.OrderStatus == (int)Order_Status.Order_Approved ? "<span class='label label-success'>OA</span>" :
                                     u.OrderStatus == (int)Order_Status.Order_Rejected ? "<span class='label label-danger'>OR</span>" :
                                     u.OrderStatus == (int)Order_Status.Order_Verified ? "<span class='label label-warning'>OP</span>" : "")
                                  + @"
                                                </span>
                                            </td>
                                            <td>" + dateTime.ToString("dd/MM/yyyy HH:mm:ss") + @"</td>
                                            <td>" + (u.OrderType == 1 ? "Buy" : (u.OrderType == 2 ? "Sell" : (u.OrderType == 3 ? "Switch" : (u.OrderType == 4 ? "Switch" : "")))) + @"</td>
                                            <td>" + u.Username + @"</td>
                                            <td>" + u.AccountNo + @"</td>
                                            <td><a href='javascript:;' data-id='" + u.OrderNo + @"' data-original-title='View Detail' data-trigger='hover' data-placement='bottom' class='popovers action-button text-info' data-url='ViewTransactionDetail.aspx' data-title='Transaction Detail - " + u.OrderNo + @"' data-action='View'>" + u.OrderNo + @"</a></td>
                                            <td class='text-right'>" + (u.OrderType == (int)OrderType.Buy ? u.SumAmount.ToString("N2") : "-") + @"</td>
                                            <td class='text-right'>" + (u.OrderType == (int)OrderType.Buy ? "-" : u.SumUnits.ToString("N4")) + @"</td>
                                            <td>" + (u.PaymentMethod == "1" ? "FPX PAYMENT (" + (b == null ? BankName : b.DisplayName) + @")" : (u.PaymentMethod == "2" ? "<a data-original-title='Click to view' data-trigger='hover' data-placement='bottom' class='popovers text-info' href=\"" + directoryPath + url + "\" target='_blank'><i class='fa fa-eye'></i></a>" : "-")) + @"</td>               
                                            <td>" + (u.SettlementDate.HasValue && (u.OrderStatus == 3 || u.OrderStatus == 39) ? u.SettlementDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : "-") + @"</td>
                                            <td>" + (rejectReason) + @"</td>
                                            <td>" + (u.ConsultantId == null ? "" : u.ConsultantId) + @"</td>
                                            </tr>
                
                                            ");
                        index++;
                    });
                    transactionsTbody.InnerHtml = asb.ToString();
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action, String rejectReason = "")
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response response1 = new Response();
            List<string> responseMsgs = new List<string>();
            try
            {
                string idString = String.Join(",", ids);
                StringBuilder filter = new StringBuilder();
                string mainQ = (@"select uo.order_status, uo.ID, uo.order_type, uo.user_id, u.username, uo.settlement_date, uo.user_account_id, ua.account_no, uo.order_no, uo.amount, uo.units, uo.payment_method, uo.updated_date, uo.reject_reason, sum(uo.amount) as SumAmount, sum(uo.units) as SumUnits, uo.fpx_bank_code, count(uo.order_no ) as no_of_items, if(uo.order_type = 3, group_concat(ufi.Fund_Name , ',', ufi2.Fund_Name), group_concat(ufi.Fund_Name)) as fund_names, uo.consultantID as ConsultantId from ");
                filter.Append(@" user_orders uo
                            join users u on u.id = uo.user_id
                            join user_accounts ua on ua.id = uo.user_account_id
                            join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            where uo.order_type != 6 ");

                Response responseUOList = GenericService.GetDataByQuery(mainQ + filter.ToString() + " and uo.ID in (" + idString + ") ", 0, 0, false, null, false, null, false);

                if (responseUOList.IsSuccess)
                {

                    var uoDyn = responseUOList.Data;
                    var responseJSON = JsonConvert.SerializeObject(uoDyn);
                    List<UserOrderCustom> order = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserOrderCustom>>(responseJSON);


                    List<UserOrder> multipleOrders = new List<UserOrder>();
                    List<UserOrder> sent = new List<UserOrder>();

                    order.ForEach(x =>
                    {

                        Response response = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), x.OrderNo, true, 0, 0, false);
                        if (response.IsSuccess)
                        {
                            List<UserOrder> ol = (List<UserOrder>)response.Data;
                            if (ol.Count > 0)
                            {
                                multipleOrders.AddRange(ol);
                            }
                        }
                    });

                    multipleOrders = multipleOrders.OrderBy(x => x.Id).ToList();
                    List<HolderLedger> AllHolderLedgers = new List<HolderLedger>();
                    if (action == "Approve")
                    {

                        List<UtmcFundInformation> utmcfundinfos = new List<UtmcFundInformation>();
                        Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                        if (responseUFIList.IsSuccess)
                        {
                            utmcfundinfos = (List<UtmcFundInformation>)responseUFIList.Data;
                        }

                        string[] trans_nos = rejectReason.Split(',');

                        string query = @" select h.* from uts.holder_ledger h
                                        where 
                                        h.TRANS_NO in (" + (String.Join(", ", trans_nos.Select(x => "'" + x + "'").ToArray())) + @") ";

                        List<HolderLedger> holderLedgersO = ServicesManager.GetHolderLedgerByHolderNo(query);
                        holderLedgersO = holderLedgersO.Where(hl => utmcfundinfos.Select(ut => ut.IpdFundCode).Contains(hl.FundID)).ToList();
                        if (holderLedgersO.Count != trans_nos.Length)
                        {
                            Logger.WriteLog("holderLedgers.Count != trans_nos.Length");
                            response1.IsSuccess = false;
                            response1.Message = "Invalid Transaction no!";
                            return response1;
                        }

                        List<HolderLedger> holderLedgers = new List<HolderLedger>();
                        trans_nos.ToList().ForEach(tn => {
                            HolderLedger holderLedger = holderLedgersO.Where(h => h.TransNO == tn).FirstOrDefault();
                            holderLedgers.Add(holderLedger);
                        });


                        int idx = 0;
                        bool isValidTransNo = true;
                        multipleOrders.ForEach(x =>
                        {
                            string trans_no = trans_nos[idx];
                            idx++;

                            string ot = (x.OrderType == 1 ? "SA" : (x.OrderType == 2 ? "RD" : (x.OrderType == 3 ? "RD" : (x.OrderType == 4 ? "SA" : ""))));
                            string at = (x.OrderType == 3 || x.OrderType == 4 ? "SW" : "");
                            DateTime xDT = new DateTime(x.CreatedDate.Year, x.CreatedDate.Month, x.CreatedDate.Day);

                            List<HolderLedger> hls = holderLedgers
                            .Where(h =>
                                h.TransType == ot &&
                                h.AccType == (at == "" ? h.AccType : at) &&
                                h.TransDt >= xDT &&
                                h.HolderNo.ToString() == order.FirstOrDefault(z => z.OrderNo == x.OrderNo).AccountNo &&
                                h.TransNO == trans_no
                            ).ToList();



                            if (hls.Count > 0)
                            {
                                AllHolderLedgers.AddRange(hls);
                                HolderLedger holderLedger = hls.FirstOrDefault();
                                var jsonString = JsonConvert.SerializeObject(holderLedger);
                                Logger.WriteLog(jsonString);
                                if (x.OrderType == 1 && x.Amount != holderLedger.TransAmt.Value)
                                {
                                    Logger.WriteLog("x.OrderType == 1 && x.Amount != holderLedger.TransAmt.Value");
                                    response1.IsSuccess = false;
                                    response1.Message = "Invalid Transaction no!";
                                }
                                else if (x.OrderType == 2 && x.Units != holderLedger.TransUnits.Value)
                                {
                                    Logger.WriteLog("x.OrderType == 2 && x.Units != holderLedger.TransUnits.Value");
                                    response1.IsSuccess = false;
                                    response1.Message = "Invalid Transaction no!";
                                    isValidTransNo = false;
                                }
                                if (isValidTransNo)
                                {
                                    if (x.OrderStatus == 2)
                                    {
                                        x.UpdatedDate = DateTime.Now;
                                        x.OrderStatus = 3;

                                        x.Status = 1;
                                        x.RejectReason = trans_no;
                                        x.SettlementDate = holderLedger.TransDt.HasValue ? holderLedger.TransDt.Value : default(DateTime);

                                        x.TransAmt = holderLedger.TransAmt.HasValue ? holderLedger.TransAmt.Value : 0;
                                        x.TransUnits = holderLedger.TransUnits.HasValue ? holderLedger.TransUnits.Value : 0;
                                        x.AppFee = holderLedger.AppFee.HasValue ? holderLedger.AppFee.Value : 0;
                                        x.ExitFee = holderLedger.ExitFee.HasValue ? holderLedger.ExitFee.Value : 0;
                                        x.FeeAmt = holderLedger.FeeAMT.HasValue ? holderLedger.FeeAMT.Value : 0;
                                        x.TransPr = holderLedger.TransPr.HasValue ? holderLedger.TransPr.Value : 0;

                                    }
                                }
                            }
                            else
                            {
                                Logger.WriteLog("ot: " + ot);
                                Logger.WriteLog("at: " + at);
                                Logger.WriteLog("xDT: " + xDT);
                                Logger.WriteLog("AccountNo: " + order.FirstOrDefault(z => z.OrderNo == x.OrderNo).AccountNo);
                                Logger.WriteLog("trans_no: " + trans_no);

                                Logger.WriteLog("isValidTransNo = false");
                                isValidTransNo = false;
                            }
                        });
                        if (!isValidTransNo)
                        {
                            Logger.WriteLog("isValidTransNo = false");
                            response1.IsSuccess = false;
                            response1.Message = "Invalid Transaction no!";
                            return response1;
                        }
                        order.ForEach(y =>
                        {
                            if (y.OrderStatus == 3)
                            {
                                response1.IsSuccess = false;
                                responseMsgs.Add("[" + y.OrderNo + "] has been approved");
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Order number " + y.OrderNo + " succesfully approved",
                                    TableName = "user_orders",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "order_status",
                                    ValueOld = "2",
                                    ValueNew = "3",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);
                                //Audit Log Ends here
                            }
                            else if (y.OrderStatus == 39)
                            {
                                response1.IsSuccess = false;
                                responseMsgs.Add("[" + y.OrderNo + "] has been rejected");
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Order number " + y.OrderNo + " has been rejected",
                                    TableName = "user_orders",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                alm = (AdminLogMain)responseLog.Data;
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "order_status",
                                    ValueOld = "2",
                                    ValueNew = "39",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);
                                //Audit Log Ends here
                            }
                            else
                            {
                                response1.IsSuccess = true;
                                responseMsgs.Add("[" + y.OrderNo + "] is approved");
                                Response responseOrder = IUserOrderService.GetDataByFilter("order_no = '" + y.OrderNo + "' group by order_no", 0, 10, false);
                                if (responseOrder.IsSuccess)
                                {
                                    List<UserOrder> orders = (List<UserOrder>)responseOrder.Data;
                                    sent.AddRange(orders);
                                }
                            }
                        });
                        IUserOrderService.UpdateBulkData(multipleOrders);
                    }
                    if (action == "Reject")
                    {
                        multipleOrders.ForEach(x =>
                        {
                            if (x.OrderStatus == 2)
                            {
                                x.UpdatedDate = DateTime.Now;
                                x.RejectReason = rejectReason;
                                x.OrderStatus = 39;
                                x.Status = 1;
                                x.SettlementDate = DateTime.Now;
                            }
                        });

                        order.ForEach(y =>
                        {
                            if (y.OrderStatus == 3)
                            {
                                response1.IsSuccess = false;
                                responseMsgs.Add("[" + y.OrderNo + "] has been approved");
                            }
                            else if (y.OrderStatus == 39)
                            {
                                response1.IsSuccess = false;
                                responseMsgs.Add("[" + y.OrderNo + "] has been rejected");
                            }
                            else
                            {
                                response1.IsSuccess = true;
                                responseMsgs.Add("[" + y.OrderNo + "] is rejected");
                                Response responseOrder = IUserOrderService.GetDataByFilter("order_no = '" + y.OrderNo + "' group by order_no", 0, 10, false);
                                if (responseOrder.IsSuccess)
                                {
                                    List<UserOrder> orders = (List<UserOrder>)responseOrder.Data;
                                    sent.AddRange(orders);
                                }
                            }
                        });
                        IUserOrderService.UpdateBulkData(multipleOrders);
                    }

                    sent.ForEach(x =>
                    {
                        Response response = IUserService.GetSingle(x.UserId);
                        if (response.IsSuccess)
                        {
                            string fundName = "";
                            string fundName2 = "";
                            List<UtmcFundInformation> funds = new List<UtmcFundInformation>();
                            List<UtmcFundInformation> funds2 = new List<UtmcFundInformation>();
                            List<UserOrder> allOrders = multipleOrders.Where(y => y.OrderNo == x.OrderNo).ToList();
                            User user = (User)response.Data;
                            UserAccount primaryAccount = user.UserIdUserAccounts.FirstOrDefault(w => w.Id == x.UserAccountId);
                            Email email = new Email();
                            email.user = user;
                            email.rejectReason = rejectReason;
                            allOrders.GroupBy(z => z.FundId).Select(grp => grp.FirstOrDefault()).ToList().ForEach(z =>
                            {
                                Response responseF = IUtmcFundInformationService.GetSingle(z.FundId);
                                if (responseF.IsSuccess)
                                {
                                    UtmcFundInformation utmcFundInformation = (UtmcFundInformation)responseF.Data;
                                    fundName = utmcFundInformation.FundName.Capitalize();
                                    funds.Add(utmcFundInformation);

                                }
                                if (z.ToFundId != 0)
                                {
                                    Response responseF2 = IUtmcFundInformationService.GetSingle(z.ToFundId);
                                    if (responseF2.IsSuccess)
                                    {
                                        UtmcFundInformation utmcFundInformation = (UtmcFundInformation)responseF2.Data;
                                        fundName2 = utmcFundInformation.FundName.Capitalize();
                                        funds2.Add(utmcFundInformation);
                                    }
                                }
                            });
                            EmailService.SendOrderEmail(allOrders, email, funds, funds2, primaryAccount.AccountNo, AllHolderLedgers);
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                response1.IsSuccess = false;
                responseMsgs.Add("Action: " + ex.Message + " - Exception");
                Console.WriteLine("Transactions action: " + ex.Message);
            }
            response1.Message = String.Join(",<br/>", responseMsgs.ToArray());
            return response1;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object ActionSimulation(Int32[] ids, String action, String rejectReason = "")
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response response1 = new Response();
            List<string> responseMsgs = new List<string>();
            try
            {
                string idString = String.Join(",", ids);
                StringBuilder filter = new StringBuilder();
                string mainQ = (@"select uo.order_status, uo.ID, uo.order_type, uo.user_id, u.username, u.id_no, uo.settlement_date, uo.user_account_id, ua.account_no, uo.order_no, uo.amount, uo.units, uo.payment_method, uo.updated_date, uo.reject_reason, sum(uo.amount) as SumAmount, sum(uo.units) as SumUnits, uo.fpx_bank_code, count(uo.order_no ) as no_of_items, if(uo.order_type = 3, group_concat(ufi.Fund_Name , ',', ufi2.Fund_Name), group_concat(ufi.Fund_Name)) as fund_names, uo.consultantID as ConsultantId from ");
                filter.Append(@" user_orders uo
                            join users u on u.id = uo.user_id
                            join user_accounts ua on ua.id = uo.user_account_id
                            join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            where uo.order_type != 6 ");

                Response responseUOList = GenericService.GetDataByQuery(mainQ + filter.ToString() + " and uo.ID in (" + idString + ") ", 0, 0, false, null, false, null, false);

                if (responseUOList.IsSuccess)
                {

                    var uoDyn = responseUOList.Data;
                    var responseJSON = JsonConvert.SerializeObject(uoDyn);
                    List<UserOrderCustom> order = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserOrderCustom>>(responseJSON);


                    List<UserOrder> multipleOrders = new List<UserOrder>();
                    List<UserOrder> sent = new List<UserOrder>();

                    order.ForEach(x =>
                    {

                        Response response = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), x.OrderNo, true, 0, 0, false);
                        if (response.IsSuccess)
                        {
                            List<UserOrder> ol = (List<UserOrder>)response.Data;
                            if (ol.Count > 0)
                            {
                                multipleOrders.AddRange(ol);
                            }
                        }
                    });

                    multipleOrders = multipleOrders.OrderBy(x => x.Id).ToList();
                    List<HolderLedger> AllHolderLedgers = new List<HolderLedger>();
                    if (action == "Approve")
                    {

                        List<UtmcFundInformation> utmcfundinfos = new List<UtmcFundInformation>();
                        Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                        if (responseUFIList.IsSuccess)
                        {
                            utmcfundinfos = (List<UtmcFundInformation>)responseUFIList.Data;
                        }
                        List<HolderInv> holderInvs = ServicesManager.GetHolderInvByHolderNo(order.FirstOrDefault().AccountNo);


                        //string[] trans_nos = rejectReason.Split(',');


                        //string query = @" select h.* from uts.holder_ledger h
                        //where
                        //                h.TRANS_NO in (" + (String.Join(", ", trans_nos.Select(x => "'" + x + "'").ToArray())) + @") ";

                        //List<HolderLedger> holderLedgersO = ServicesManager.GetHolderLedgerByHolderNo(query);
                        //holderLedgersO = holderLedgersO.Where(hl => utmcfundinfos.Select(ut => ut.IpdFundCode).Contains(hl.FundID)).ToList();
                        //if (holderLedgersO.Count != trans_nos.Length)
                        //{
                        //    Logger.WriteLog("holderLedgers.Count != trans_nos.Length");
                        //    response1.IsSuccess = false;
                        //    response1.Message = "Invalid Transaction no!";
                        //    return response1;
                        //}

                        //List<HolderLedger> holderLedgers = new List<HolderLedger>();
                        //trans_nos.ToList().ForEach(tn =>
                        //{
                        //    HolderLedger holderLedger = holderLedgersO.Where(h => h.TransNO == tn).FirstOrDefault();
                        //    holderLedgers.Add(holderLedger);
                        //});


                        int idx = 0;
                        bool isValidTransNo = true;
                        multipleOrders.ForEach(x =>
                        {
                            //string trans_no = trans_nos[idx];
                            idx++;
                            UtmcFundInformation utmcFundInformation = utmcfundinfos.FirstOrDefault(f => f.Id == x.FundId);
                            string ot = (x.OrderType == 1 ? "SA" : (x.OrderType == 2 ? "RD" : (x.OrderType == 3 ? "RD" : (x.OrderType == 4 ? "SA" : ""))));
                            string at = (x.OrderType == 3 || x.OrderType == 4 ? "SW" : "");
                            DateTime xDT = new DateTime(x.CreatedDate.Year, x.CreatedDate.Month, x.CreatedDate.Day);
                            Response responseTransNo = ServicesManager.GetNewTransNo();
                            if (responseTransNo.IsSuccess)
                            {
                                string transNo = responseTransNo.Data.ToString();
                                Decimal amount = 0;
                                Decimal units = 0;
                                if (ot == "SA")
                                {
                                    amount = x.Amount;
                                    units = (x.Amount / utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice);
                                }
                                else if (ot == "RD")
                                {
                                    amount = (x.Units * utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice);
                                    units = x.Units;
                                }

                                HolderInv holderInv = holderInvs.FirstOrDefault(hi => hi.FundId == utmcFundInformation.IpdFundCode);
                                Decimal currUnitHldgs = 0;
                                if (holderInv == null)
                                {
                                    currUnitHldgs = units;
                                    string insertQueryHolderInv = @"Insert into UTS.HOLDER_INV (HOLDER_NO, FUND_ID, ID_NO, ID_NO_OLD, SALE_VAL_CUM, SALE_UNIT_CUM, RDM_VAL_CUM, RDM_UNIT_CUM, BONUS_VAL_CUM, BONUS_UNIT_CUM, COL_VAL_CUM, COL_UNIT_CUM, TAX_WITHELD, DIV_ACC, CURR_UNIT_HLDG, CURR_UNIT_HLDG_XD, NO_ACTIVE_CERT, NO_CANCEL_CERT, STMT_UNIT_CUM, TRF_UNIT_CUM, TOT_SP, BI_VAL_CUM, BI_UNIT_CUM, HLDR_AVE_COST, HLDR_AVE_PRICE, HLDR_AVE_FEE, LAST_AVE_COST, LAST_AVE_PRICE, LAST_AVE_FEE, CUM_VALUE_HLDG, CUM_FEE, DOWNLOAD_IND)Values(" + order.FirstOrDefault(z => z.OrderNo == x.OrderNo).AccountNo + @", '" + utmcFundInformation.IpdFundCode + "', '" + order.FirstOrDefault(z => z.OrderNo == x.OrderNo).IdNo + @"', 'A1234567', 0, 1010.101, 0, 0, 0, 24424.922, 0, 0, 341.88, 5643.87, " + units + @", 0, 2, 0, 42323.1342, 0, 0, 0, 0, 0.254505435, 0.254505435, 0, 0.25564838, 0.25564838, 0, 10694.38, 0, 'N')";
                                    Response responseInsertHI = ServicesManager.InsertRecord(insertQueryHolderInv);
                                    if (responseInsertHI.IsSuccess)
                                    {

                                    }
                                    else
                                    {
                                        isValidTransNo = false;
                                    }
                                }
                                else
                                {
                                    currUnitHldgs = holderInv.CurrUnitHldg;
                                    if (ot == "SA")
                                    {
                                        currUnitHldgs += units;
                                    }
                                    else if (ot == "RD")
                                    {
                                        currUnitHldgs -= units;
                                    }
                                    string updateQueryHolderInv = @"update UTS.HOLDER_INV set CURR_UNIT_HLDG=" + currUnitHldgs + @" where HOLDER_NO=" + order.FirstOrDefault(z => z.OrderNo == x.OrderNo).AccountNo + @" and FUND_ID='" + utmcFundInformation.IpdFundCode + "' and  ID_NO='" + order.FirstOrDefault(z => z.OrderNo == x.OrderNo).IdNo + @"'";
                                    Response responseUpdateHI = ServicesManager.InsertRecord(updateQueryHolderInv);
                                    if (responseUpdateHI.IsSuccess)
                                    {

                                    }
                                    else
                                    {
                                        isValidTransNo = false;
                                    }
                                }



                                string insertQuery = @"INSERT INTO UTS.HOLDER_LEDGER (HOLDER_NO, ID_NO, FUND_ID, TRANS_NO, TRANS_TYPE, DIV_IND, ENTER_DT, ENTER_TIME, ENTRY_DT, ENTRY_TIME, ENTRY_BY, ENTRY_TERM, TRANS_DT, TRANS_UNITS, TRANS_AMT, TRANS_PR, CUR_UNIT_HLDG, CUM_VALUE_HLDG, CUM_FEE, OTHER_CHARGES, BANK_CHARGES, STAMP_DUTY, CERT_FEE, IS_IND, OPT_IND, AGENT_ID, AGENT_TYPE, AGENT_CODE, ACC_TYPE, ACC_TYPE_NO, APP_FEE, BROKER_FEE, EXIT_FEE, COM_FEE, MGR_AVE_COST_PU, HLDR_AVE_COST, HLDR_AVE_PRICE, HLDR_AVE_FEE, LAST_AVE_PRICE, LAST_AVE_FEE, BROKER_DISC_RATE, COM_DISC_RATE, SORT_SEQ, KWSP_STATUS, DISBURSE_DT, GAIN_LOSS, DOWNLOAD_IND, FEE_AMT, UNIT_VALUE, GST_AMT, FUND_GST_AMT, DOC_TYPE, DOC_NO, PRINT_IND) VALUES (" + order.FirstOrDefault(z => z.OrderNo == x.OrderNo).AccountNo + @", '" + order.FirstOrDefault(z => z.OrderNo == x.OrderNo).IdNo + @"', '" + utmcFundInformation.IpdFundCode + @"', '" + transNo + @"', '" + ot + @"',  'S', TO_DATE('" + xDT.ToString("dd/MM/yyyy") + @"', 'DD/MM/YYYY'), TO_DATE('" + DateTime.Now.ToString("dd/MM/yyyy") + @"', 'DD/MM/YYYY'), TO_DATE('" + DateTime.Now.ToString("dd/MM/yyyy") + @"', 'DD/MM/YYYY'), TO_DATE('" + DateTime.Now.ToString("dd/MM/yyyy") + @"', 'DD/MM/YYYY'),  'dya1', 'XYLOG', TO_DATE('" + DateTime.Now.ToString("dd/MM/yyyy") + @"', 'DD/MM/YYYY'), " + units + @", " + amount + @", " + utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice + @", " + currUnitHldgs + @", " + (currUnitHldgs * utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice) + @", 0, 0,  0, 0, 0, 'I', 'N',  '" + x.ConsultantId + @"', 'A', '33003022', '" + at + @"', 1992017, " + (x.OrderType == 1 ? utmcFundInformation.UtmcFundInformationIdUtmcFundCharges.FirstOrDefault().InitialSalesChargesPercent : 0) + @", 0, 0, " + (x.OrderType == 1 ? utmcFundInformation.UtmcFundInformationIdUtmcFundCharges.FirstOrDefault().InitialSalesChargesPercent : 0) + @", 1,  1, 1, 0, 1, 0,  0, 0, 3370873, ' ', TO_DATE('" + DateTime.Now.ToString("dd/MM/yyyy") + @"', 'DD/MM/YYYY'),  0, 'Y', " + (x.OrderType == 1 ? amount * utmcFundInformation.UtmcFundInformationIdUtmcFundCharges.FirstOrDefault().InitialSalesChargesPercent / 100 : 0) + @", " + (x.OrderType == 1 ? amount - (amount * utmcFundInformation.UtmcFundInformationIdUtmcFundCharges.FirstOrDefault().InitialSalesChargesPercent / 100) : amount) + @", 0,  0, 'CS', 'CS1800110248', 'N')";

                                Response responseInsert = ServicesManager.InsertRecord(insertQuery);

                                if (responseInsert.IsSuccess)
                                {

                                    string query = @" select h.* from uts.holder_ledger h
                                        where 
                                        h.TRANS_NO in ('" + transNo + @"') ";

                                    List<HolderLedger> holderLedgers = ServicesManager.GetHolderLedgerByHolderNo(query);
                                    holderLedgers = holderLedgers.Where(hl => utmcfundinfos.Select(ut => ut.IpdFundCode).Contains(hl.FundID)).ToList();
                                    List<HolderLedger> hls = holderLedgers
                                    .Where(h =>
                                        h.TransType == ot &&
                                        h.AccType == (at == "" ? h.AccType : at) &&
                                        h.TransDt >= xDT &&
                                        h.HolderNo.ToString() == order.FirstOrDefault(z => z.OrderNo == x.OrderNo).AccountNo &&
                                        h.TransNO == transNo
                                    ).ToList();

                                    if (hls.Count > 0)
                                    {
                                        AllHolderLedgers.AddRange(hls);
                                        HolderLedger holderLedger = hls.FirstOrDefault();
                                        var jsonString = JsonConvert.SerializeObject(holderLedger);
                                        Logger.WriteLog(jsonString);
                                        if (x.OrderType == 1 && x.Amount != holderLedger.TransAmt.Value)
                                        {
                                            Logger.WriteLog("x.OrderType == 1 && x.Amount != holderLedger.TransAmt.Value");
                                            response1.IsSuccess = false;
                                            response1.Message = "Invalid Transaction no!";
                                        }
                                        else if (x.OrderType == 2 && x.Units != holderLedger.TransUnits.Value)
                                        {
                                            Logger.WriteLog("x.OrderType == 2 && x.Units != holderLedger.TransUnits.Value");
                                            response1.IsSuccess = false;
                                            response1.Message = "Invalid Transaction no!";
                                            isValidTransNo = false;
                                        }
                                        if (isValidTransNo)
                                        {
                                            if (x.OrderStatus == 2)
                                            {
                                                x.UpdatedDate = DateTime.Now;
                                                x.OrderStatus = 3;

                                                x.Status = 1;
                                                x.RejectReason = transNo;
                                                x.SettlementDate = holderLedger.TransDt.HasValue ? holderLedger.TransDt.Value : default(DateTime);

                                                x.TransAmt = holderLedger.TransAmt.HasValue ? holderLedger.TransAmt.Value : 0;
                                                x.TransUnits = holderLedger.TransUnits.HasValue ? holderLedger.TransUnits.Value : 0;
                                                x.AppFee = holderLedger.AppFee.HasValue ? holderLedger.AppFee.Value : 0;
                                                x.ExitFee = holderLedger.ExitFee.HasValue ? holderLedger.ExitFee.Value : 0;
                                                x.FeeAmt = holderLedger.FeeAMT.HasValue ? holderLedger.FeeAMT.Value : 0;
                                                x.TransPr = holderLedger.TransPr.HasValue ? holderLedger.TransPr.Value : 0;

                                            }
                                        }
                                    }
                                    else
                                    {
                                        Logger.WriteLog("ot: " + ot);
                                        Logger.WriteLog("at: " + at);
                                        Logger.WriteLog("xDT: " + xDT);
                                        Logger.WriteLog("AccountNo: " + order.FirstOrDefault(z => z.OrderNo == x.OrderNo).AccountNo);
                                        Logger.WriteLog("trans_no: " + transNo);

                                        Logger.WriteLog("isValidTransNo = false");
                                        isValidTransNo = false;
                                    }
                                }
                                else
                                {
                                    isValidTransNo = false;
                                }
                            }
                            else
                            {
                                isValidTransNo = false;
                            }
                        });
                        if (!isValidTransNo)
                        {
                            Logger.WriteLog("isValidTransNo = false");
                            response1.IsSuccess = false;
                            response1.Message = "Invalid Transaction no!";
                            return response1;
                        }
                        order.ForEach(y =>
                        {
                            if (y.OrderStatus == 3)
                            {
                                response1.IsSuccess = false;
                                responseMsgs.Add("[" + y.OrderNo + "] has been approved");
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Order number " + y.OrderNo + " succesfully approved",
                                    TableName = "user_orders",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "order_status",
                                    ValueOld = "2",
                                    ValueNew = "3",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);
                                //Audit Log Ends here
                            }
                            else if (y.OrderStatus == 39)
                            {
                                response1.IsSuccess = false;
                                responseMsgs.Add("[" + y.OrderNo + "] has been rejected");
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Order number " + y.OrderNo + " has been rejected",
                                    TableName = "user_orders",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                alm = (AdminLogMain)responseLog.Data;
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "order_status",
                                    ValueOld = "2",
                                    ValueNew = "39",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);
                                //Audit Log Ends here
                            }
                            else
                            {
                                response1.IsSuccess = true;
                                responseMsgs.Add("[" + y.OrderNo + "] is approved");
                                Response responseOrder = IUserOrderService.GetDataByFilter("order_no = '" + y.OrderNo + "' group by order_no", 0, 10, false);
                                if (responseOrder.IsSuccess)
                                {
                                    List<UserOrder> orders = (List<UserOrder>)responseOrder.Data;
                                    sent.AddRange(orders);
                                }
                            }
                        });
                        IUserOrderService.UpdateBulkData(multipleOrders);
                    }
                    if (action == "Reject")
                    {
                        multipleOrders.ForEach(x =>
                        {
                            if (x.OrderStatus == 2)
                            {
                                x.UpdatedDate = DateTime.Now;
                                x.RejectReason = rejectReason;
                                x.OrderStatus = 39;
                                x.Status = 1;
                                x.SettlementDate = DateTime.Now;
                            }
                        });

                        order.ForEach(y =>
                        {
                            if (y.OrderStatus == 3)
                            {
                                response1.IsSuccess = false;
                                responseMsgs.Add("[" + y.OrderNo + "] has been approved");
                            }
                            else if (y.OrderStatus == 39)
                            {
                                response1.IsSuccess = false;
                                responseMsgs.Add("[" + y.OrderNo + "] has been rejected");
                            }
                            else
                            {
                                response1.IsSuccess = true;
                                responseMsgs.Add("[" + y.OrderNo + "] is rejected");
                                Response responseOrder = IUserOrderService.GetDataByFilter("order_no = '" + y.OrderNo + "' group by order_no", 0, 10, false);
                                if (responseOrder.IsSuccess)
                                {
                                    List<UserOrder> orders = (List<UserOrder>)responseOrder.Data;
                                    sent.AddRange(orders);
                                }
                            }
                        });
                        IUserOrderService.UpdateBulkData(multipleOrders);
                    }

                    sent.ForEach(x =>
                    {
                        Response response = IUserService.GetSingle(x.UserId);
                        if (response.IsSuccess)
                        {
                            string fundName = "";
                            string fundName2 = "";
                            List<UtmcFundInformation> funds = new List<UtmcFundInformation>();
                            List<UtmcFundInformation> funds2 = new List<UtmcFundInformation>();
                            List<UserOrder> allOrders = multipleOrders.Where(y => y.OrderNo == x.OrderNo).ToList();
                            User user = (User)response.Data;
                            UserAccount primaryAccount = user.UserIdUserAccounts.FirstOrDefault(w => w.Id == x.UserAccountId);
                            Email email = new Email();
                            email.user = user;
                            email.rejectReason = rejectReason;
                            allOrders.GroupBy(z => z.FundId).Select(grp => grp.FirstOrDefault()).ToList().ForEach(z =>
                            {
                                Response responseF = IUtmcFundInformationService.GetSingle(z.FundId);
                                if (responseF.IsSuccess)
                                {
                                    UtmcFundInformation utmcFundInformation = (UtmcFundInformation)responseF.Data;
                                    fundName = utmcFundInformation.FundName.Capitalize();
                                    funds.Add(utmcFundInformation);

                                }
                                if (z.ToFundId != 0)
                                {
                                    Response responseF2 = IUtmcFundInformationService.GetSingle(z.ToFundId);
                                    if (responseF2.IsSuccess)
                                    {
                                        UtmcFundInformation utmcFundInformation = (UtmcFundInformation)responseF2.Data;
                                        fundName2 = utmcFundInformation.FundName.Capitalize();
                                        funds2.Add(utmcFundInformation);
                                    }
                                }
                            });
                            EmailService.SendOrderEmail(allOrders, email, funds, funds2, primaryAccount.AccountNo, AllHolderLedgers);
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                response1.IsSuccess = false;
                responseMsgs.Add("Action: " + ex.Message + " - Exception");
                Console.WriteLine("Transactions action: " + ex.Message);
            }
            response1.Message = String.Join(",<br/>", responseMsgs.ToArray());
            return response1;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Download(string filter)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            string message = string.Empty;
            try
            {
                string tempPath = Path.GetTempPath() + "Transactions_" + DateTime.Now.ToString("yyyyMMddhhmmssfff");
                FileInfo excelFile = new FileInfo(tempPath);
                message = excelFile.FullName;
                ExcelPackage excel = new ExcelPackage();
                excel.Workbook.Worksheets.Add("Transactions");

                var headerRow = new List<string[]>()
            {
                new string[] { "S.No", "Order Date", "Order Status", "Trans Type", "Username", "MA Account", "Order No.", "Funds", "Amounts(MYR)/Units", "Total Amount(MYR)", "Total Units", "Payment", "Processed Date", "Agent Code", "Distribution Instruction", "Remarks / Trans no", "Credit bank (Account No)" }
            };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A2:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "2";

                var worksheet = excel.Workbook.Worksheets["Transactions"];



                try
                {
                    // ------------------------------------------------
                    // Creation of header cells
                    // ------------------------------------------------
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    string docDetails = "Transactions";

                    // ------------------------------------------------
                    // Populate sheet with some real data from list
                    // ------------------------------------------------
                    int row = 3; // start row (in row 1 are header cells)
                    StringBuilder filterQuery = new StringBuilder();
                    filterQuery.Append(@"SELECT group_concat(bd.name SEPARATOR ',') as bank_name, bd.name as bank_name1, group_concat(mhb.bank_account_no SEPARATOR ',') as bank_account_no, uo.ID, uo.user_id, u.username, uo.user_account_id, ua.account_no, uo.order_no, uo.order_type, uo.created_date, 
                            uo.amount, uo.units, ufi.Fund_Name, 
                            if(uo.order_type = 3 or uo.order_type = 4, group_concat(ufi.Fund_Name , ',\r\n', ufi2.Fund_Name), group_concat(ufi.Fund_Name SEPARATOR ',\r\n')) as allfunds1, group_concat(uo.amount SEPARATOR ',\r\n') as allamounts, group_concat(uo.units SEPARATOR ',\r\n') as allunits, 
                            ufi2.Fund_Name as Fund_Name2, uo.payment_method, uo.payment_date, 
                            uo.settlement_date, ufc.INITIAL_SALES_CHARGES_PERCENT, ufc2.INITIAL_SALES_CHARGES_PERCENT, uo.order_status, 
                            uo.fpx_bank_code, group_concat(uo.reject_reason SEPARATOR ',') as reject_reason, ufd.LATEST_NAV_PRICE as LATEST_NAV_PRICE1, 
                            ufd2.LATEST_NAV_PRICE as LATEST_NAV_PRICE2, sum(uo.amount) as SumAmount, sum(uo.units) as SumUnits,
                            group_concat(uo.distribution_instruction SEPARATOR ',') as distribution_instruction,
                            group_concat(uo.consultantId SEPARATOR ',\r\n') as consultantId
                            FROM user_orders uo
                            left join users u on u.id = uo.user_id
                            left join user_accounts ua on ua.ID = uo.user_account_id
                            left join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            left join utmc_fund_charges ufc on ufc.utmc_fund_information_id = uo.fund_id
                            left join utmc_fund_charges ufc2 on ufc2.utmc_fund_information_id = uo.to_fund_id 
                            left join utmc_fund_details ufd on ufd.utmc_fund_information_id = ufi.ID
                            left join utmc_fund_details ufd2 on ufd2.utmc_fund_information_id = ufi2.ID 
                            left join ma_holder_bank mhb on mhb.id = uo.bank_id
                            left join banks_def bd on bd.id = mhb.bank_def_id
                            where uo.order_type not in (4, 6) ");
                    string orderType = "";
                    string fromDate = filter.Split(',')[1];
                    string toDate = filter.Split(',')[2];
                    filter = filter.Split(',')[0];
                    if (!string.IsNullOrEmpty(filter) && filter != "")
                    {
                        if (filter != "All")
                        {
                            docDetails += " | Status: " + (filter == "2" ? "Order Pending" : (filter == "3" ? "Order Approved" : (filter == "39" ? "Order Rejected" : "")));
                            filterQuery.Append(" and uo.order_status in ('" + filter + "') ");
                        }
                        else
                        {
                            docDetails += " | Status: All (Order Pending, Order Approved, Order Rejected) ";
                            filterQuery.Append(" and uo.order_status IN('2', '3', '39') ");
                        }
                    }
                    else
                    {
                        docDetails += " | Status: Order Pending ";
                        filterQuery.Append(" and uo.order_status IN ('2') ");
                    }

                    if (fromDate != "" && toDate != "")
                    {
                        DateTime from = DateTime.ParseExact(fromDate.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        DateTime to = DateTime.ParseExact(toDate.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        docDetails += " | Date Range: " + from + " - " + to;
                        filterQuery.Append(" and uo.created_date between '" + from.ToString("yyyy-MM-dd") + " 00:00:00' and '" + to.ToString("yyyy-MM-dd") + " 23:59:59' ");
                    }
                    filterQuery.Append(@"group by uo.order_no ");
                    filterQuery.Append(@"order by FIELD(uo.order_status, '2', '3', '39'), uo.created_date desc ");

                    string docDetailsRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    worksheet.Cells[docDetailsRange].Merge = true;
                    worksheet.Cells[docDetailsRange].Value = docDetails;
                    worksheet.Cells[docDetailsRange].Style.Font.Bold = true;


                    Response responseTransactions = GenericService.GetDataByQuery(filterQuery.ToString(), 0, 0, false, null, false, null, true);

                    if (responseTransactions.IsSuccess)
                    {
                        var transactionsDyn = responseTransactions.Data;
                        var responseJSON = JsonConvert.SerializeObject(transactionsDyn);
                        List<DiOTP.Utility.Payments> transactions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.Payments>>(responseJSON);

                        
                        string bankCode = "";
                        int index = 1;
                        transactions.ForEach(x =>
                        {
                            if (x.fpx_bank_code == "" || x.fpx_bank_code == null)
                            {
                                bankCode = "";
                            }
                            else
                            {
                                bankCode = x.fpx_bank_code;
                            }
                            string switchFunds = "";
                            string switchUnits = "";
                            if (x.order_type == 3 || x.order_type == 4)
                            {
                                string[] funds = x.allfunds1.Split(',');
                                string[] units = x.allunits.Split(',');
                                for (int i = 0; i < funds.Length; i++)
                                {
                                    funds[i] = funds[i].Trim(Environment.NewLine.ToCharArray());
                                    funds[i + 1] = funds[i + 1].Trim(Environment.NewLine.ToCharArray());
                                    switchFunds += funds[i] + "(Out)," + funds[i + 1] + "(In),\r\n";
                                    i++;
                                }
                                for (int i = 0; i < units.Length; i++)
                                {
                                    units[i] = units[i].Trim(Environment.NewLine.ToCharArray());
                                    switchUnits += units[i] + ",\r\n";
                                }
                                switchFunds = switchFunds.Trim(Environment.NewLine.ToCharArray());
                                switchUnits = switchUnits.Trim(Environment.NewLine.ToCharArray());
                                switchFunds = switchFunds.Trim(',');
                                switchUnits = switchUnits.Trim(',');
                            }
                            string[] reasons = x.reject_reason.Split(',');
                            reasons = reasons.Distinct().ToArray();

                            string rejectReason = String.Join(",\r\n", reasons);

                            string[] distributionInstructions = x.distribution_instruction.Split(',');
                            List<string> disInss = new List<string>();
                            distributionInstructions.ForEach(d =>
                            {
                                disInss.Add((d == "1" ? "Payout" : (d == "3" ? "Reinvest" : "-")));
                            });
                            string distributionInstruction = String.Join(",\r\n", disInss.ToArray());
                            
                            worksheet.Cells[row, 1].Value = index;
                            worksheet.Cells[row, 2].Value = x.created_date.ToString("dd/MM/yyyy HH:mm:ss");
                            worksheet.Cells[row, 3].Value = x.order_status == 2 ? "Order Pending" : x.order_status == 3 ? "Order Approved" : x.order_status == 39 ? "Order Rejected" : "";
                            worksheet.Cells[row, 4].Value = x.order_type == 1 ? "Buy" : x.order_type == 2 ? "Sell" : x.order_type == 3 || x.order_type == 4 ? "Switch" : "";
                            worksheet.Cells[row, 5].Value = x.username.ToString();
                            worksheet.Cells[row, 6].Value = x.account_no;
                            worksheet.Cells[row, 7].Value = x.order_no;
                            worksheet.Cells[row, 8].Value = (x.order_type == 3 || x.order_type == 4 ? switchFunds : x.allfunds1);
                            worksheet.Cells[row, 9].Value = x.order_type == 1 ? x.allamounts : (x.order_type == 2 ? x.allunits : (x.order_type == 3 || x.order_type == 4 ? switchUnits : ""));
                            worksheet.Cells[row, 10].Value = x.SumAmount == 0 ? "-" : x.SumAmount.ToString();
                            worksheet.Cells[row, 11].Value = x.SumUnits == 0 ? "-" : x.SumUnits.ToString();
                            worksheet.Cells[row, 12].Value = x.payment_method == "1" ? "FPX PAYMENT (" + bankCode + @")" : (x.payment_method == "2" ? "PROOF OF PAYMENT" : "-");
                            worksheet.Cells[row, 13].Value = (x.settlement_date.HasValue && (x.order_status == 3 || x.order_status == 39) ? x.settlement_date.Value.ToString("dd/MM/yyyy HH:mm:ss") : "-");
                            worksheet.Cells[row, 14].Value = x.consultantId;
                            worksheet.Cells[row, 15].Value = distributionInstruction;
                            worksheet.Cells[row, 16].Value = rejectReason;
                            worksheet.Cells[row, 17].Value = x.bank_name + (x.bank_account_no != null && x.bank_account_no != "" ? "(" + x.bank_account_no + ")" : "");



                            
                            row++;
                            index++;
                        });
                    }

                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    // Apply some predefined styles for data to look nicely :)
                    worksheet.Cells[headerRange].Style.Font.Bold = true;

                    // Save this data as a file
                    excel.SaveAs(excelFile);

                    // Display SUCCESS message
                    response.IsSuccess = true;
                    response.Message = "Downloading...";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "Payments List successfully downloaded",
                        TableName = "user_orders",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);
                    if (!responseLog.IsSuccess)
                    {
                        //Audit log failed
                    }
                    else
                    {

                    }

                    //Audit Log Ends here
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                }
                finally
                {
                    
                    //Empty variables
                    excel = null;
                    worksheet = null;

                    // Force garbage collector cleaning
                    GC.Collect();

                    //change to byte[]
                    byte[] result = File.ReadAllBytes(excelFile.ToString());
                    String file = Convert.ToBase64String(result);
                    //delete temporary file
                    File.Delete(excelFile.ToString());
                    response.Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + file;
                    response.Message = "Transactions" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    //response.Data = "data:application/vnd.ms-excel;base64," + file;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + message;
            }
            return response;
        }

        public static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType("DiOTP.Utility.CustomClasses." + typeName);
                if (type != null)
                    return type;
            }
            return null;
        }
    }
}