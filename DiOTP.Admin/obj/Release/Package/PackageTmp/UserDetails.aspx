﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserDetails.aspx.cs" Inherits="Admin.UserDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <table class="display responsive nowrap table table-bordered mb-30" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Email Id</th>
                                <th>Mobile Number</th>
                                <th>Status</th>
                                <th>Last login IP</th>
                                <th>Register IP</th>
                            </tr>
                        </thead>
                        <tbody id="userTbody" runat="server">
                            <tr>
                                <td>Username</td>
                                <td>Email Id</td>
                                <td>Mobile Number</td>
                                <td>Status</td>
                                <td>Last login IP</td>
                                <td>Register IP</td>
                            </tr>
                        </tbody>

                    </table>
                </div>

                <div class="col-md-6">
                    <h3 class="table-heading">Portfolio <small class="pull-right"><strong id="asOnDate" runat="server"></strong></small></h3>
                    <table class="table userDetailsTable">
                        <tr>
                            <th>No. of MA Accounts</th>
                            <td id="noOfMAAccounts" runat="server">-</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-right">
                                <a id="impersonateUserLink" runat="server" href='javascript:;' target='_blank' class='btn btn-sm btn-primary'>Impersonate User</a>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="col-md-6">
                    <h3 class="table-heading">Security Bindings</h3>
                    <table class="table userDetailsTable">
                        <tr>
                            <th>Email Id verification</th>
                            <td id="emailSecurity" runat="server">Binded</td>
                        </tr>
                        <tr>
                            <th>SMS Authentication Binding</th>
                            <td id="mobileSecurity" runat="server">Binded</td>
                        </tr>
                        <tr>
                            <th>Google Authentication Binding</th>
                            <td id="googleSecurity" runat="server">Binded</td>
                        </tr>
                    </table>
                </div>

                <div class="col-md-12">
                    <h3 class="table-heading">MA Accounts</h3>
                    <table class="display responsive nowrap table table-bordered mb-30 dataTable maAccounts">
                        <thead>
                            <tr>
                                <th>MA Number</th>
                                <th>Account plan</th>
                                <th>SAT Score</th>
                                <th>SAT (updated on)</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Birth date</th>
                                <th>Telephone</th>
                                <th>Handphone</th>
                                <th>Occupation</th>
                                <th>Nationality</th>
                                <th>No of dependents</th>
                                <th>Residential Address</th>
                                <th>Permanent Address</th>
                            </tr>
                        </thead>
                        <tbody id="userAccountsTbody" runat="server">
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    $('.dataTable.maAccounts').DataTable({
                        dom: ''
                    });
                }, 100);
            });
        </script>
    </form>
</body>
</html>
