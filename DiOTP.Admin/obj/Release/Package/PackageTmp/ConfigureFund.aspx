﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfigureFund.aspx.cs" Inherits="Admin.ConfigureFund" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="configureFundForm" runat="server">
        <div id="FormId" data-value="configureFundForm">
            <div class="row">
                <div class="col-md-12 fundvalidation">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">IPD Fund Code:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="fundId" id="fundId" runat="server" clientidmode="static" class="form-control" placeholder="Enter IPD Fund Code"  onkeypress="return (event.charCode >= 48 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122)" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
