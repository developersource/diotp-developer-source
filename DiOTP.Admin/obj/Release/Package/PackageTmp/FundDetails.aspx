﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FundDetails.aspx.cs" Inherits="Admin.FundDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="display responsive nowrap table table-bordered mb-20" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Fundname</th>
                                        <th>Unit Price</th>
                                        <th>Change</th>
                                        <th>Change %</th>
                                        <th>Currency</th>
                                        <th>Category</th>
                                        <th>Nav Date</th>
                                        <th class="hide">Fund Class</th>
                                        <th class="hide">IsRetail </th>
                                    </tr>
                                </thead>
                                <tbody id="fundTableBody" runat="server">
                                    <tr>
                                        <td>Fundname</td>
                                        <td>Unit Price</td>
                                        <td>Change</td>
                                        <td>Change %</td>
                                        <td>Category</td>
                                        <td>Nav Date</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <h3 class="table-heading">Fund Information
                            <%--<small class="pull-right">
                                <a href="javascript:;" id="editFundInfo" runat="server" class="tab-menu-item btn btn-success btn-sm" data-ismodal="1" data-showmodal="1" data-modalsize="M" data-url='EditFundInfo.aspx' data-title='Edit Fund Information' data-action="EditFI"><i class="fa fa-edit"></i></a>
                            </small>--%>
                            </h3>
                            <table class="table userDetailsTable">
                                <tr>
                                    <th>IPD Fund Code:</th>
                                    <td id="fundIpdCode" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Category</th>
                                    <td id="fundCategory" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Conventional Status:</th>
                                    <td id="fundStatus" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Fund Zone:</th>
                                    <td id="fundZone" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Is Emis?:</th>
                                    <td id="IsEmis" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Fund Class:</th>
                                    <td id="fundCls" runat="server"></td>
                                </tr>
                                 <tr>
                                    <th>Is Retail?:</th>
                                    <td id="IsRetail" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Sat Group:</th>
                                    <td id="SatGrp" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>RSP Applicable:</th>
                                    <td id="RspApplicable" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Launch Date</th>
                                    <td id="fundLaunchDate" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Launch Price</th>
                                    <td id="fundLaunchPrice" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Relaunch Date</th>
                                    <td id="fundRelaunchDate" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Pricing Basis</th>
                                    <td id="fundPricingBasis" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Latest NAV Price</th>
                                    <td id="fundLatestNavPrice" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Historical Income Distribution</th>
                                    <td id="fundHistoricalIncomeDistribution" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Approved by EPF</th>
                                    <td id="fundApprovedByEPF" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Shariah Complaint</th>
                                    <td id="fundShariahComplaint" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Risk Rating</th>
                                    <td id="fundRiskRating" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Fund Size</th>
                                    <td id="fundSize" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Minimum Initial Investment (CASH/EPF)</th>
                                    <td id="fundMinimumInitialInvestment" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Minimum Subsequent Investment (CASH/EPF)</th>
                                    <td id="fundMinimumSubsequentInvestment" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Minimum RSP Investment (Initial/Additional)</th>
                                    <td id="fundMinimumRSPInvestment" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Minimum Redemption Amount</th>
                                    <td id="fundMinimumRedemptionAmount" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Minimum Holding</th>
                                    <td id="fundMinimumHolding" runat="server"></td>
                                </tr>
                            </table>
                        </div>

                        <div class="col-md-6">
                            <h3 class="table-heading">Fund Downloads 
                            <%--<small class="pull-right">
                                <a href="javascript:;" id="editFundDownloads" runat="server" class="tab-menu-item btn btn-success btn-sm hide" data-ismodal="1" data-showmodal="1" data-modalsize="M" data-url='EditFundDownloads.aspx' data-title='Add Fund Downloads' data-action="EditFD"><i class="fa fa-edit"></i></a>
                            </small>--%>
                            </h3>
                            <div class="panel-group" id="downloadFileLinks" runat="server">
                            </div>

                            <h3 class="table-heading">Fund Charges 
                            <%--<small class="pull-right">
                                <a href="javascript:;" id="editFundCharges" runat="server" class="tab-menu-item btn btn-success btn-sm" data-ismodal="1" data-showmodal="1" data-modalsize="M" data-url='EditFundCharges.aspx' data-title='Edit Fund Charges' data-action="EditFC"><i class="fa fa-edit"></i></a>
                            </small>--%>
                            </h3>
                            <table class="table userDetailsTable">
                                <tr>
                                    <th>Discounted Initial Sales Charge*</th>
                                    <td id="fundDiscountedInitialSalesCharge" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Annual Management Charge*</th>
                                    <td id="fundAnnualManagementCharge" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>EPF Sales Charge*</th>
                                    <td id="fundEPFSalesCharge" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Trustee Fee*</th>
                                    <td id="fundTrusteeFee" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Switching Fee*</th>
                                    <td id="fundSwitchingFee" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Redemption Fee*</th>
                                    <td id="fundRedemptionFee" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Transfer Fee*</th>
                                    <td id="fundTransferFee" runat="server"></td>
                                </tr>
                                <tr>
                                    <th>Other Significant Fees*</th>
                                    <td id="fundOtherSignificantFee" runat="server"></td>
                                </tr>
                                <tr>
                                    <td id="fundGSTFee" runat="server" colspan="2"></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="table-heading">Fund Terms
                            <%--<small class="pull-right">
                                <a href="javascript:;" id="editFundTerms" runat="server" class="tab-menu-item btn btn-success btn-sm" data-ismodal="1" data-showmodal="1" data-modalsize="M" data-url='EditFundTerms.aspx' data-title='Edit Fund Terms' data-action="EditFT"><i class="fa fa-edit"></i></a>
                            </small>--%>
                            </h3>
                            <table class="table userDetailsTable">
                                <tr>
                                    <th colspan="2">
                                        <h5>Cooling-off Period</h5>
                                        <p class="text-justify" id="fundCoolingOffPeriod" style="white-space: pre-line" runat="server"></p>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="2">
                                        <h5>Distribution Policy</h5>
                                        <p class="text-justify" id="fundDistributionPolicy" style="white-space: pre-line" runat="server"></p>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="2">
                                        <h5>Investment Objective</h5>
                                        <p class="text-justify" id="fundInvestmentObjective" style="white-space: pre-line" runat="server"></p>
                                    </th>
                                </tr>
                                <tr id="fundInvestmentStrategyAndPolicyTD" runat="server" visible="false">
                                    <th colspan="2">
                                        <h5>Investment Strategy and Policy</h5>
                                        <p class="text-justify" id="fundInvestmentStrategyAndPolicy" style="white-space: pre-line" runat="server"></p>
                                    </th>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </form>
</body>
</html>

    
    <script src="/Content/js/jquery.dataTables.min.js"></script>
    <script src="/Content/js/dataTables.bootstrap.min.js"></script>
    <script src="/Content/js/dataTables.responsive.min.js"></script>
    <script src="/Content/js/responsive.bootstrap.min.js"></script>
    <script src="/Content/js/highcharts.js"></script>