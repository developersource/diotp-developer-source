﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewDetailRSP.aspx.cs" Inherits="Admin.ViewDetailRSP" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <table class="display responsive nowrap table table-bordered mb-30" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Fund Name</th>
                                <th>Subscription Amount</th>
                                <th>Ref No / Remarks</th>
                            </tr>
                        </thead>
                        <tbody id="userTbody" runat="server">
                            <tr>
                                <td>No.</td>
                                <td>Fund Name</td>
                                <td>RSP Amount (MYR)</td>

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
