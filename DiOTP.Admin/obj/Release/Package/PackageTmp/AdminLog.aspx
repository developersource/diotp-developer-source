﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminLog.aspx.cs" Inherits="Admin.AdminLog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="adminLogForm" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="content">
                        <div class="mb-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="search-section">
                                                <input type="text" id="Search" name="Search" runat="server" class="form-control" placeholder="Search here" />
                                                <input type="hidden" id="IsNewSearch" name="IsNewSearch" value="0" runat="server" clientidmode="static" />
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <select id="Module" name="Module" runat="server" class="form-control">
                                                <option value="">All Modules</option>
                                                <option value="1">Funds List</option>
                                                <option value="2">Update Statement links</option>
                                                <option value="3">Bank List</option>
                                                <option value="4">Banner Listing</option>
                                                <option value="5">Announcement Listing</option>
                                                <option value="6">Admin List</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="input-group input-large">
                                                <%--<input type="text" class="form-control dpd1" name="from" />--%>
                                                <input type="text" id="FromDate" name="FromDate" runat="server" class="form-control" placeholder="From Date" />
                                                <span class="input-group-addon">To</span>
                                                <input type="text" id="ToDate" name="ToDate" runat="server" class="form-control" placeholder="To Date" />
                                                <%--<input type="text" class="form-control dpd2" name="to" />--%>
                                            </div>
                                            <%--<div class="action-section">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button data-message="Successfully rejected" data-status="Rejected" data-original-title="Reject" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Reject" data-isconfirm="1"><i class="fa fa-ban"></i></button>
                                                        <button data-message="Successfully approved" data-status="Approved" data-original-title="Approve" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Activate" data-isconfirm="0"><i class="fa fa-check"></i></button>
                                                        <span class="common-divider pull-right"></span>
                                                    </div>
                                                </div>
                                            </div>--%>
                                        </div>
                                        <div class="col-md-1">
                                            <%--<button data-message="Successfully approved" data-status="Approved" data-original-title="Approve" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Approve" data-isconfirm="0"><i class="fa fa-search"></i></button>--%>
                                            <button type="button" class="popovers btn action-button pull-right btn-block" data-action="Search"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <%--<div class="action-section">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button data-message="Successfully rejected" data-status="Rejected" data-original-title="Reject" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Reject" data-isconfirm="1"><i class="fa fa-ban"></i></button>
                                                <button data-message="Successfully approved" data-status="Approved" data-original-title="Approve" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Activate" data-isconfirm="0"><i class="fa fa-check"></i></button>
                                                <span class="common-divider pull-right"></span>
                                            </div>
                                        </div>
                                    </div>--%>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong> 
                                        <span>You can search here with Username, MA Acc no, and Description</span>
                                    </small>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="filter-section mt-10">
                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <select id="pageLength" name="pageLength" runat="server" clientidmode="static" class="form-control pull-right page-length-selection">
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="display responsive nowrap table table-bordered dataTable" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Username</th>
                                            <th>Description</th>
                                            <th>Date</th>
                                            <th>View Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody id="changeLogTbody" runat="server"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnNumberPerPage" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnTotalRecordsCount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCurrentPageNo" runat="server" ClientIDMode="Static" />
    </form>
    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
    <%--Date Picker--%>
    <script>
        $ui(document).ready(function () {
            $ui('#FromDate, #ToDate').datepicker({
                format: 'dd/mm/yyyy',
                pickTime: false,
                autoclose: true
            });

        });
    </script>
</body>
</html>
