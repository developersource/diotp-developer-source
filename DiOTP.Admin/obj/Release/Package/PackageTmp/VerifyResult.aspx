﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VerifyResult.aspx.cs" Inherits="Admin.VerifyResult" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" id="VerifyResultJson" name="VerifyResultJson" runat="server" clientidmode="static" />
                    <button type="button" id="downloadCSV" class="btn btn-primary pull-right"><i class="fa fa-download"></i></button>
                    <h5>Input</h5>
                    <table class="table table-condensed" id="inputFields">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Field Name</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody id="tbodyInput" runat="server">
                        </tbody>
                    </table>
                    <h5 id="verifyStatus" runat="server"></h5>
                    <table class="table table-condensed" id="inputFieldsResult">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Data Source</th>
                                <th>Field Name</th>
                                <th>Field Status</th>
                            </tr>
                        </thead>
                        <tbody id="tbodyResult" runat="server">
                        </tbody>
                    </table>
                    <h5 class="hide">Raw result</h5>
                    <pre id="jsonResult" runat="server" class="hide"></pre>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                function downloadCSV(csv, filename) {
                    var csvFile;
                    var downloadLink;

                    // CSV file
                    csvFile = new Blob([csv], { type: "text/csv" });

                    // Download link
                    downloadLink = document.createElement("a");

                    // File name
                    downloadLink.download = filename;

                    // Create a link to the file
                    downloadLink.href = window.URL.createObjectURL(csvFile);

                    // Hide download link
                    downloadLink.style.display = "none";

                    // Add the link to DOM
                    document.body.appendChild(downloadLink);

                    // Click download link
                    downloadLink.click();
                }
                function exportTableToCSV(filename) {
                    var csv = [];
                    var rows = $('#inputFields tr');

                    for (var i = 0; i < rows.length; i++) {
                        var row = [], cols = rows[i].querySelectorAll("td, th");

                        for (var j = 0; j < cols.length; j++)
                            row.push(cols[j].innerText + "\t");

                        csv.push(row.join(","));
                    }
                    csv.push('');
                    rows = $('#inputFieldsResult tr');

                    for (var i = 0; i < rows.length; i++) {
                        var row = [], cols = rows[i].querySelectorAll("td, th");

                        for (var j = 0; j < cols.length; j++)
                            row.push(cols[j].innerText);

                        csv.push(row.join(","));
                    }

                    // Download CSV file
                    downloadCSV(csv.join("\n"), filename);
                }
                $('#downloadCSV').click(function () {
                    exportTableToCSV('Result.csv');
                });
            });
        </script>
    </form>
</body>
</html>
