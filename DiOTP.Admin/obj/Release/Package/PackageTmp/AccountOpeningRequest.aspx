﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccountOpeningRequest.aspx.cs" Inherits="Admin.AccountOpeningRequest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="accountOpeningsForm" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="content">
                        <div class="mb-10">
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="search-section">
                                        <input type="text" id="Search" name="Search" runat="server" class="form-control" placeholder="Search here" />
                                        <input type="hidden" id="IsNewSearch" name="IsNewSearch" value="0" runat="server" clientidmode="static" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <select id="DateType" name="DateType" runat="server" class="form-control">
                                        <option value="">Select</option>
                                        <option value="1">Submitted Date</option>
                                        <option value="2">Settlement Date</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                                        <input type="text" id="FromDate" name="FromDate" runat="server" class="form-control" placeholder="From Date" />
                                        <span class="input-group-addon">To</span>
                                        <input type="text" id="ToDate" name="ToDate" runat="server" class="form-control" placeholder="To Date" />
                                    </div>
                                </div>

                                <div class="col-md-1">
                                    <div class="search-section">
                                        <button type="button" class="button-common-label action-button" data-action="Search"><i class="fa fa-search"></i></button>
                                        <span class="common-divider pull-right"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="action-section">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button data-message="Successfully rejected" data-status="Reject" data-original-title="Reject" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Reject" data-isconfirm="1" data-isreason="0"><i class="fa fa-ban"></i></button>
                                                <%--<button id="rejectDoc" data-message="Rejected" data-status="Active" data-original-title="Reject" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="RejectDoc" data-isconfirm="1" data-isreason="0"><i class="fa fa-times"></i></button>--%>
                                                <button data-message="Successfully approved" data-status="Approve" data-original-title="Approve" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Approve" data-isconfirm="0"><i class="fa fa-check"></i></button>
                                                <span class="common-divider pull-right"></span>
                                                <button id="download" data-message="Success" data-status="Active" data-original-title="Download as excel" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Download" data-isconfirm="0"><i class="fa fa-download"></i></button>
                                                <button id="approveDoc" data-message="Documents Verified" data-status="Pending" data-original-title="Verify Documents" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="ApproveDoc" data-isconfirm="1" data-isreason="0"><i class="fa fa-check-square"></i></button>
                                                <button id="approveID" data-message="Identity Verified" data-status="Pending" data-original-title="Verify Identity" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="VerifyID" data-isconfirm="1" data-isreason="0"><i class="fa fa-user"></i></button>
                                                <button id="request" data-message="Additional files Requested" data-status="Pending" data-original-title="Request additional file" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="RequestFile" data-isconfirm="1" data-isreason="0"><i class="fa fa-reply"></i></button>
                                                <button id="requestUnclear" data-message="Unclear files Requested" data-status="Pending" data-original-title="Request unclear files" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="RequestUnclearFile" data-isconfirm="1" data-isreason="1"><i class="fa fa-repeat"></i></button>
                                                
                                                <%--<button id="verifyID" data-message="Identity Verified" data-status="Active" data-original-title="Verify Identity" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="VerifyID" data-isconfirm="1" data-isreason="0"><i class="fa fa-check-square"></i></button>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong>
                                        <span>You can search here with Username or MA Acc No</span>
                                    </small>
                                </div>
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong>
                                        <span>You can Request Unclear or Additional Files/Verify Documents/Download/Activate/Deactivate on Individual Accounts</span>
                                    </small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="filter-section mt-10">
                                        <input type="hidden" id="FilterValue" name="FilterValue" runat="server" class="form-control" />
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="All">All</button>
                                        <button type="button" class="btn btn-primary action-button first-load" data-action="Filter" data-filter="0">Pending</button>
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="1">Approved</button>
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="2">Rejected</button>
                                        <small>
                                            <br />
                                            <strong>Note:</strong>
                                            <span>Status Filtering on Individual Accounts</span>
                                        </small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <select id="pageLength" name="pageLength" runat="server" clientidmode="static" class="form-control pull-right page-length-selection">
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="display responsive nowrap table table-bordered dataTable userAccounts" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th class='icheck'>
                                                <div class='square single-row'>
                                                    <div class='checkbox'>
                                                        <input type='checkbox' name='checkRowAll' class='checkRowAll' value='All' /><br />
                                                    </div>
                                                </div>
                                                S.No
                                            </th>
                                            <th class="br-0" data-column-name="username">User details</th>
                                            <th class="bl-0"></th>

                                        </tr>
                                    </thead>
                                    <tbody id="usersTbody" runat="server">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnNumberPerPage" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnTotalRecordsCount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCurrentPageNo" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnOrderByColumn" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnOrderType" runat="server" ClientIDMode="Static" Value="0" />
    </form>
    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-print-json@0.2/dist/pretty-print-json.css" />
    <script src="https://cdn.jsdelivr.net/npm/pretty-print-json@0.2/dist/pretty-print-json.min.js"></script>
    <script>
        $ui(document).ready(function () {
            $ui('#FromDate, #ToDate').datepicker({
                format: 'dd/mm/yyyy',
                pickTime: false,
                autoclose: true
            });

            //$ui('#ToDate').datepicker({
            //    format: 'mm/dd/yyyy',
            //    pickTime: false,
            //    autoclose: true
            //});
        });
    </script>
</body>
</html>
