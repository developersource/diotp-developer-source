﻿var $ui = $;
$(document).ready(function () {
        $(document).on('hidden.bs.modal', function (event) {
            if ($('.modal:visible').length) {
                $('body').addClass('modal-open');
            }
            $('.colorpicker').remove();

        });
                $('body').on('click', '.tab-menu-item', function () {
                    var title = $(this).attr('data-title');
                    var url = $(this).attr('data-url');
                    var closable = $(this).attr('data-closable');
                    var showModal = $(this).attr('data-showModal');
                    if (showModal != '1') {
                        if ($('#tt').tabs('exists', title + '<span class="data-url hide">' + url + '</span>')) {
        $('#tt').tabs('select', title + '<span class="data-url hide">' + url + '</span>');
                        } else {
                            if (closable == "false")
                                closable = false;
                            else
                                closable = true;
                            $.get(url, function (content) {
        $('#tt').tabs('add', {
            title: title + '<span class="data-url hide">' + url + '</span>',
            content: content,
            closable: closable
        });
                            }, 'html');
                        }
                    }
                    else {
                        var modalSize = $(this).attr('data-modalSize');
                        var dataId = $(this).attr('data-id');
                        var isModal = $(this).attr('data-isModal');
                        var dataAction = $(this).attr("data-action");
                        $('body').removeClass('EditFI');
                        $('body').removeClass('EditFC');
                        $('body').removeClass('EditFT');
                        $('body').removeClass('Upload');
                        $('body').addClass(dataAction);
                        if (isModal != '1') {
        $('#largeModalContent').html("");
                            if (modalSize == 'L') {
        $('#largeModal').modal('toggle');
                                $('#largeModalTitle').html(title);
                                $('#largeModalContent').load(url + "?id=" + dataId);
                            }
                        }
                        else {
        $('#innerLargeModalContent').html("");
                            if (modalSize == 'L') {
        $('#innerLargeModal').modal('toggle');
                                $('#innerLargeModalTitle').html(title);
                                $('#innerLargeModalContent').load(url + "?id=" + dataId);
                            }
                            if (modalSize == 'M') {
                                var dataInnerId = $(this).attr('data-innerId');
                                var fileTypeId = $(this).attr('data-fileTypeId');
                                $('#innerMediumModal').modal('toggle');
                                $('#innerMediumModalTitle').html(title);
                                var url = url + "?id=" + dataId + "&innerId=" + dataInnerId + "&fileTypeId=" + fileTypeId + "&titleIn=" + title;
                                var replacedurl = url.split(' ').join('%20');
                                $('#innerMediumModalContent').load(replacedurl);
                            }
                        }
                    }
                });
                $('body').on('click', '.tabs-first', function () {
        //$.get("Dashboard.aspx", function (content) {
        //    var selectedTab = $('#tt').tabs("getTab", 0);
        //    $('#tt').tabs('update', {
        //        tab: selectedTab,
        //        options: {
        //            content: content
        //        }
        //    });
        //}, 'html');
    });
                var isAjaxActive = 0;
                var ajaxRequest;
                var refreshDashboard = function () {
                    if (isAjaxActive == 0) {
        $('#IsNewSearch').val('1');
                        var data = $('#dashboardForm').serializeArray();
                        data = data.filter(function (item) {
                            return item.name.indexOf('_') === -1;
                        });
                        var url = "Dashboard.aspx?" + $.param(data);
                        var tab = $('#tt').tabs('getSelected');
                        var index = $('#tt').tabs('getTabIndex', tab);
                        if (index == 0) {
        ajaxRequest = $.post(url, function (content) {
            isAjaxActive = 0;
            var selectedTab = $('#tt').tabs("getTab", 0);
            $('#tt').tabs('update', {
                tab: selectedTab,
                options: {
                    content: content
                }
            });
            // setTimeout(refreshDashboard, 2000);
        }, 'html');
                        }
                        else {
        // setTimeout(refreshDashboard, 2000);
    }
                    }
                    else {
        isAjaxActive = 1;
                        //  setTimeout(refreshDashboard, 2000);
                    }
                }
                // setTimeout(refreshDashboard, 2000);
            });
        function showSnackBar(message) {
                var x = $("#snackbar");
                x.html(message);
                x.addClass('show');
                setTimeout(function () {
            x.className = x.removeClass('show');
                }, 6000); // 3000 = 3 seconds
            }
