﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="Admin.Profile" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="ProfileForm" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <table class="display responsive nowrap table table-bordered " cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th colspan="2">Details</th>
                            </tr>
                        </thead>
                        <tbody id="usersTbody" runat="server"></tbody>
                    </table>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnNumberPerPage" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnTotalRecordsCount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCurrentPageNo" runat="server" ClientIDMode="Static" />
    </form>
    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
</body>
</html>
