﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditFundTerms.aspx.cs" Inherits="Admin.EditFundTerms" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <label>Cooling-off Period</label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox TextMode="MultiLine" Rows="5" ID="txtCoolingoffPeroid" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label>Distribution Policy</label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox TextMode="MultiLine" Rows="5" ID="txtDistributionPolicy" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label>Investment Objective</label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox TextMode="MultiLine" Rows="5" ID="txtInvestmentObjective" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label>Investment Strategy and Policy</label>
                    </div>
                    <div class="col-md-9">
                        <asp:TextBox TextMode="MultiLine" Rows="5" ID="txtInvestmentStrategyAndPolicy" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
