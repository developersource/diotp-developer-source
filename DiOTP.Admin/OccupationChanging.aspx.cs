﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.OracleDTOs;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using Admin.ServiceCalls;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class OccupationChanging : System.Web.UI.Page
    {
        private static readonly Lazy<IMaHolderRegService> lazyMaHolderRegServiceObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());
        public static IMaHolderRegService IMaHolderRegService { get { return lazyMaHolderRegServiceObj.Value; } }

        private static readonly Lazy<IStatesDefService> lazyStatesDefServiceObj = new Lazy<IStatesDefService>(() => new StatesDefService());
        public static IStatesDefService IStatesDefService { get { return lazyStatesDefServiceObj.Value; } }

        private static readonly Lazy<ICountriesDefService> lazyCountriesDefServiceObj = new Lazy<ICountriesDefService>(() => new CountriesDefService());
        public static ICountriesDefService ICountriesDefService { get { return lazyCountriesDefServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserTypeService> lazyUserTypeServiceObj = new Lazy<IUserTypeService>(() => new UserTypeService());

        public static IUserTypeService IUserTypeService { get { return lazyUserTypeServiceObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get data query
            StringBuilder filter = new StringBuilder();
            string mainQ = (@"SELECT mhr.id, ors.*,mhr.OCC_CODE, mhr.NAME_1, mhr.OTP_ENT_BY, mhr.OTP_VERSION, mhr.OTP_TAC_CD, mhr.ID_NO FROM");
            string mainQCount = (@"select count(*) from ");
            filter.Append(@" ma_holder_reg mhr 
                            join occupation_requests ors on mhr.holder_no = ors.holder_no 
                            where mhr.id in (select max(id) from ma_holder_reg group by holder_no) and 1=1 ");
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                User user = (User)Session["admin"];
                Response response = IUserTypeService.GetDataByFilter("user_id = '" + user.Id + "'", 0, 0, false);
                UserType loginUserType = new UserType();
                if (response.IsSuccess)
                {
                    loginUserType = ((List<UserType>)response.Data).FirstOrDefault();
                }

                //page number filter
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }

                //search function

                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and (mhr.NAME_1 like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or mhr.HOLDER_NO like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or mhr.ID_NO like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or mhr.ADDR_1 like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or mhr.ADDR_2 like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or mhr.ADDR_3 like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or mhr.ADDR_4 like '%" + Search.Value.Trim() + "%')");
                }

                //process status filter

                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    if (FilterValue.Value != "All")
                        filter.Append(" and mhr.OTP_VERSION in ('" + FilterValue.Value + "')");
                    else
                        filter.Append(@" and mhr.OTP_VERSION in ('0','1','9') ");
                }
                else
                {
                    filter.Append(@"and mhr.OTP_VERSION in ('0') ");
                }
                filter.Append(@"group by mhr.holder_no ");
                
                // filter order

                if (Request.QueryString["hdnOrderByColumn"] != "" && Request.QueryString["hdnOrderByColumn"] != null && Request.QueryString["hdnOrderByColumn"].Trim() != "-")
                {
                    hdnOrderByColumn.Value = Request.QueryString["hdnOrderByColumn"].ToString();
                    hdnOrderType.Value = Request.QueryString["hdnOrderType"].ToString();
                    filter.Append(" order by " + hdnOrderByColumn.Value + " " + hdnOrderType.Value + " ");
                }
                else
                {
                    if (FilterValue.Value == "0" || string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                    {
                        filter.Append("order by mhr.OTP_ACT_ST, mhr.OTP_ENT_BY desc");
                    }
                    else
                    {
                        filter.Append("order by mhr.OTP_ACT_ST, mhr.OTP_TAC_CD desc ");
                    }
                }


                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);



                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + filter.ToString()).Data.ToString();

                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                // database connection

                Response responseMAHolderRegList = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, false);
                if (responseMAHolderRegList.IsSuccess)
                {
                    var OccupationDyn = responseMAHolderRegList.Data;
                    var responseJSON = JsonConvert.SerializeObject(OccupationDyn);
                    List<OccupationChangeRequest> occupation = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OccupationChangeRequest>>(responseJSON);


                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    foreach (OccupationChangeRequest a in occupation)
                    {
                        string checkBox = string.Empty;
                        if (a.OtpVersion == "0")
                        {
                            checkBox = @"<div class='square single-row'>
                                                <div class='checkbox'>
                                                    <input type = 'checkbox' name='checkRow' class='checkRow' value='" + a.Id + @"' /> <label>" + index + @"</label><br/>
                                                </div>
                                            </div>";
                        }
                        else
                        {
                            checkBox = @"<label>" + index + @"</label><br/>";
                        }

                        String occdesc = "";
                        string incomedesc = "";
                        if (a.Isemployed == 0) {
                            occdesc = a.OccCode;
                            incomedesc = String.IsNullOrEmpty(a.IncomeCode) ? "-" : a.IncomeCode;
                        }
                        else
                        {
                            Response responseOccValue = ServiceCalls.ServicesManager.GetOccupationDescData(a.OccCode);
                            if (responseOccValue.IsSuccess)
                            {
                                OccupationDTO occupationDTO = (OccupationDTO)responseOccValue.Data;
                                occdesc = occupationDTO.OCCCODE;
                            }
                            else {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseOccValue.Message + "\");", true);

                            }
                            Response responseMonthlyIncome = ServiceCalls.ServicesManager.GetMonthlyIncomeDataByCode(a.IncomeCode);
                            if (responseMonthlyIncome.IsSuccess)
                            {
                                IncomeDTO incomeDTO = (IncomeDTO)responseMonthlyIncome.Data;
                                incomedesc = incomeDTO.SDESC;
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseMonthlyIncome.Message + "\");", true);
                            }
                        }

                        //Income
                        
                        
                        //else
                        //{
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "('Alert', \"" + responseMonthlyIncome.Message + "\", '');", true);
                        //}


                        //Response responseOccupationRequestList = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, false);
                        //if (responseMAHolderRegList.IsSuccess)
                        //{



                        //}
                        asb.Append(@"<tr>
                                                <td class='icheck'>
                                                    " + checkBox + @"
                                                    <span class='row-status'>" + (a.OtpVersion == "1" ? "<span class='label label-success'>Approved</span>" :
                                                                            a.OtpVersion == "0" ? "<span class='label label-warning'>Pending</span>" : "<span class='label label-danger'>Rejected</span>") + @"
                                                    </span>
                                                </td>
                                                <td>" + a.OtpEntBy.Value.ToString("dd/MM/yyyy") + @"</td>
                                                <td>" + (a.OtpVersion == "0" ? "-" : a.OtpTacCd.Value.ToString("dd/MM/yyyy")) + @"</td>
                                                <td>" + a.Name1 + @"</td>
                                                <td>" + a.HolderNo + @"</td>
                                                <td>" + a.IdNo + @"</td>
                                                <td>" + (a.Isemployed == 0 ? "Unemployed" : "Employed") + @"</td>
                                                <td>" + occdesc + @"</td>
                                                <td>" + (String.IsNullOrEmpty(a.NameOfEmployer) ? "-" : a.NameOfEmployer) + @"</td>
                                                <td>" + (String.IsNullOrEmpty(a.Nob) ? "-" : a.Nob) + @"</td>
                                                <td>" + incomedesc + @"</td>
                                                <td>" + (String.IsNullOrEmpty(a.Addr1) ? "-" : a.Addr1)+ @"</td>
                                                <td>" + (String.IsNullOrEmpty(a.Addr2) ? "-" : a.Addr2) + @"</td>
                                                <td>" + (String.IsNullOrEmpty(a.Addr3) ? "-" : a.Addr3) + @"</td>
                                                <td>" + (String.IsNullOrEmpty(a.PostCode) ? "-" : a.PostCode) + @"</td>
                                                <td>" + (String.IsNullOrEmpty(a.City) ? "-" : a.City) + @"</td>
                                                <td>" + (String.IsNullOrEmpty(a.State) ? "-" : a.State) + @"</td>
                                                <td>" + (String.IsNullOrEmpty(a.Country) ? "-" : a.Country) + @"</td>
                                                <td>" + (String.IsNullOrEmpty(a.OfficeTelNo2) ? "-" : a.OfficeTelNo2) + @"</td>
                                                </tr>"
                                    );
                        index++;
                    }
                    // apend data to html
                    usersTbody.InnerHtml = asb.ToString();

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                        "alert('" + responseMAHolderRegList.Message + "');", true);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            string message = "";
            try
            {
                String title = "Occupation Information Update";
                List<string> content1s = new List<string>();
                List<string> content2s = new List<string>();
                List<Email> emails = new List<Email>();
                List<String> dates = new List<String>();
                string idString = String.Join(",", ids);
                Response responseMAHolderRegList = IMaHolderRegService.GetDataByFilter(" ID in (" + idString + ")", 0, 0, false);
                if (responseMAHolderRegList.IsSuccess)
                {
                    List<MaHolderReg> mhrs = (List<MaHolderReg>)responseMAHolderRegList.Data;

                    if (action == "Activate")
                    {
                        mhrs.ForEach(x =>
                        {
                            if (x.OtpVersion == "0")
                            {
                                Response responseUAList = IUserAccountService.GetDataByFilter("account_no = '" + x.HolderNo + "' ", 0, 0, false);
                                UserAccount ua = new UserAccount();
                                if (responseUAList.IsSuccess)
                                {
                                    ua = ((List<UserAccount>)responseUAList.Data).FirstOrDefault();
                                    //Response responseUList = IUserService.GetDataByFilter("ID = '" + ua.UserId + "' ", 0, 0, false);
                                    //if (responseUList.IsSuccess)
                                    //{
                                    //User u = ((List<User>)responseUList.Data).FirstOrDefault();

                                    Email email = new Email();
                                    Response rU = IUserService.GetSingle(ua.UserId);
                                    if (rU.IsSuccess)
                                    {
                                        email.user = (User)rU.Data;
                                        ua.UserIdUser = email.user;
                                    }
                                    content1s.Add("Occupation change request has been approved by Apex admin on ");
                                    dates.Add(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                                    content2s.Add("");
                                    emails.Add(email);



                                    //}
                                }
                                Response responseOccupationChangeReqquest = GenericService.GetDataByQuery(@"SELECT mhr.id, ors.* FROM ma_holder_reg mhr 
                                                                             join occupation_requests ors on mhr.holder_no = ors.holder_no
                                                                            where mhr.OTP_VERSION in ('0', '1', '9') and mhr.id in
                                                                            (select max(id) from ma_holder_reg group by holder_no) and mhr.Id in (" + x.Id + ")", 0, 20, false, null, false, null, false);
                                if (responseMAHolderRegList.IsSuccess)
                                {
                                    var OccupationDyn = responseOccupationChangeReqquest.Data;
                                    var responseJSON = JsonConvert.SerializeObject(OccupationDyn);
                                    List<OccupationChangeRequest> occupation = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OccupationChangeRequest>>(responseJSON);

                                    foreach (OccupationChangeRequest a in occupation)
                                    {
                                        x.OtpTacCd = DateTime.Now;
                                        x.OtpVersion = "1";
                                        x.OccCode = a.OccCode;
                                        x.AccountType = a.IncomeCode;
                                        x.Nob = a.Nob;
                                        x.OfficeAddress1 = a.Addr1; //
                                        x.OfficeAddress2 = a.Addr2; //
                                        x.OfficeAddress3 = a.Addr3; //
                                        x.NameOfEmployer = a.NameOfEmployer;   
                                        x.OfficePostCode = a.PostCode; //    
                                        x.OfficeCity = a.City; //
                                        x.OfficeState = a.State; //
                                        x.OfficeCountry = a.Country; //
                                        x.OfficeTelNo = a.OfficeTelNo2; //
                                    }
                                }
                                message += "Occupation change request of " + x.Name1 + " is successfully Approved.<br/>";
                                UserLogMain z = new UserLogMain()
                                {
                                    TableName = "ma_holder_reg",
                                    Description = "Admin rejected user request for occupation change",
                                    UpdatedDate = DateTime.Now,
                                    UserId = ua.UserId,
                                    RefId = ua.Id,
                                    RefValue = ua.AccountNo,
                                    StatusType = 0
                                };
                                IUserLogMainService.PostData(z);

                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "User (" + ua.UserIdUser.Username + ") Occupation Change Request Successfully approved",
                                    TableName = "ma_holder_reg",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                alm = (AdminLogMain)responseLog.Data;
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "OTP_VERSION",
                                    ValueOld = "0",
                                    ValueNew = "1",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);

                                //Audit Log Ends here
                            }
                            else if (x.OtpVersion == "9")
                            {
                                message += "Occupation of " + x.Name1 + " is already rejected.<br/>";
                            }
                            else if (x.OtpVersion == "1")
                            {
                                message += "Occupation of " + x.Name1 + " is already approved.<br/>";
                            }
                        });
                        IMaHolderRegService.UpdateBulkData(mhrs);
                        
                    }
                    if (action == "Reject")
                    {
                        mhrs.ForEach(x =>
                        {
                            if (x.OtpVersion == "0")
                            {
                                Response responseUAList = IUserAccountService.GetDataByFilter("account_no = '" + x.HolderNo + "' ", 0, 0, false);
                                UserAccount ua = new UserAccount();
                                if (responseUAList.IsSuccess)
                                {
                                    ua = ((List<UserAccount>)responseUAList.Data).FirstOrDefault();

                                    Email email = new Email();
                                    Response rU = IUserService.GetSingle(ua.UserId);
                                    if (rU.IsSuccess)
                                    {
                                        email.user = (User)rU.Data;
                                        ua.UserIdUser = email.user;
                                    }
                                    content1s.Add("Occupation change request has been rejected by Apex admin on ");
                                    dates.Add(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                                    content2s.Add("");
                                    emails.Add(email);

                                }
                                x.OtpVersion = "9";
                                x.OtpTacCd = DateTime.Now;
                                UserLogMain z = new UserLogMain()
                                {
                                    TableName = "ma_holder_reg",
                                    Description = "Admin rejected user request for occupation change",
                                    UpdatedDate = DateTime.Now,
                                    UserId = ua.UserId,
                                    RefId = ua.Id,
                                    RefValue = ua.AccountNo,
                                    StatusType = 0
                                };
                                IUserLogMainService.PostData(z);
                                message += "Occupation change request of " + x.Name1 + " is Rejected.<br/>";
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "User (" + ua.UserIdUser.Username + ") Occupation Change Request rejected",
                                    TableName = "ma_holder_reg",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                alm = (AdminLogMain)responseLog.Data;
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "OTP_VERSION",
                                    ValueOld = "1",
                                    ValueNew = "0",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);

                                //Audit Log Ends here
                            }
                            else if (x.OtpVersion == "9")
                            {
                                message += "Occupation change request of " + x.Name1 + " is already rejected.<br/>";
                            }
                            else if (x.OtpVersion == "1")
                            {
                                message += "Occupation change reuest of " + x.Name1 + " is already approved.<br/>";
                            }
                        });
                        IMaHolderRegService.UpdateBulkData(mhrs);
                    }
                }
                emails.ForEach(x =>
                {
                    EmailService.SendUpdateMail(x, title, content1s[emails.IndexOf(x)], dates[emails.IndexOf(x)], content2s[emails.IndexOf(x)]);
                });
                response.IsSuccess = true;
                response.Message = message;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.WriteLine("User accounts action: " + ex.Message);
            }
            return response;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Download(string filter)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response response = new Response();
            string message = string.Empty;
            try
            {

                string tempPath = Path.GetTempPath() + "Address_" + DateTime.Now.ToString("yyyyMMddhhmmssfff");
                FileInfo excelFile = new FileInfo(tempPath);
                message = excelFile.FullName;
                ExcelPackage excel = new ExcelPackage();
                excel.Workbook.Worksheets.Add("Occupation Change Requests");

                var headerRow = new List<string[]>()
            {
                new string[] { "S.No", "Status", "Requested date", "Appv/Rej date", "Username", "MA No.", "Id No.","Employment Status", "Occupation", "Name of Employer", "Nature of Business", "Monthly Income", "Address Line 1", "Address Line 2", "Address Line 3", "Post Code", "City", "State", "Country", "Office Tel No" }
            };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A2:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "2";

                var worksheet = excel.Workbook.Worksheets["Occupation Change Requests"];

                string docDetails = "Occupation Change Requests";

                try
                {
                    // ------------------------------------------------
                    // Creation of header cells
                    // ------------------------------------------------
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    // ------------------------------------------------
                    // Populate sheet with some real data from list
                    // ------------------------------------------------
                    int row = 3; // start row (in row 1 are header cells)
                    StringBuilder filterQuery = new StringBuilder();
                    StringBuilder jfilter = new StringBuilder();


                    filterQuery.Append(@"Select mhr.id, ors.*,mhr.OCC_CODE, mhr.NAME_1, mhr.OTP_ENT_BY, mhr.OTP_VERSION, mhr.OTP_TAC_CD, mhr.ID_NO
                                        from ma_holder_reg mhr join occupation_requests ors on mhr.holder_no = ors.holder_no 
                                        where mhr.id in (select max(id) from ma_holder_reg group by holder_no) ");

                    if (!string.IsNullOrEmpty(filter) && filter != "")
                    {

                        docDetails += " | Status: " + (filter == "0" ? "Pending" : (filter == "1" ? "Approved" : (filter == "9" ? "Rejected" : (filter == "All" ? "All" : "-")))) + " ";
                        if (filter != "All")
                            filterQuery.Append(" and OTP_VERSION in ('" + filter + "')");
                        else
                            filterQuery.Append(@" and OTP_VERSION in ('0','1','9') ");
                    }
                    else
                    {
                        docDetails += " | Status: Pending";
                        filterQuery.Append(@" and OTP_VERSION in ('0') ");
                    }
                    int skip = 0, take = 0;
                    filterQuery.Append(@"order by OTP_VERSION desc, OTP_ENT_BY desc");

                    string docDetailsRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    worksheet.Cells[docDetailsRange].Merge = true;
                    worksheet.Cells[docDetailsRange].Value = docDetails;
                    worksheet.Cells[docDetailsRange].Style.Font.Bold = true;

                    Response responseAddress = GenericService.GetDataByQuery(filterQuery.ToString(), skip, take, false, null, false, null, false);
                    if (responseAddress.IsSuccess)
                    {
                        var OccupationDyn = responseAddress.Data;
                        var responseJSON = JsonConvert.SerializeObject(OccupationDyn);
                        List<DiOTP.Utility.OccupationChangeRequest> Occupation = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.OccupationChangeRequest>>(responseJSON);

                        StringBuilder asb = new StringBuilder();

                        int index = 1;

                        foreach (OccupationChangeRequest ocr in Occupation)
                        {
                            String occdesc = "";
                            string incomedesc = "";
                            if (ocr.Isemployed == 0)
                            {
                                occdesc = ocr.OccCode;
                            }
                            else
                            {
                                Response responseOccValue = ServiceCalls.ServicesManager.GetOccupationDescData(ocr.OccCode);
                                if (responseOccValue.IsSuccess)
                                {
                                    OccupationDTO occupationDTO = (OccupationDTO)responseOccValue.Data;
                                    occdesc = occupationDTO.OCCCODE;
                                }
                                else
                                {
                                    response = responseOccValue;

                                }
                                Response responseMonthlyIncome = ServiceCalls.ServicesManager.GetMonthlyIncomeDataByCode(ocr.IncomeCode);
                                if (responseMonthlyIncome.IsSuccess)
                                {
                                    IncomeDTO incomeDTO = (IncomeDTO)responseMonthlyIncome.Data;
                                    incomedesc = incomeDTO.SDESC;
                                }
                                else
                                {
                                    response = responseMonthlyIncome;
                                }
                            }

                            worksheet.Cells[row, 1].Value = index;
                            worksheet.Cells[row, 2].Value = (ocr.OtpVersion == "0" ? "Pending" : (ocr.OtpVersion == "1" ? "Approved" : (ocr.OtpVersion == "9" ? "Rejected" : "-")));
                            worksheet.Cells[row, 3].Value = ocr.OtpEntBy.Value.ToString("dd/MM/yyyy");
                            worksheet.Cells[row, 4].Value = ocr.OtpVersion == "0" ? "-" : ocr.OtpTacCd.Value.ToString("dd/MM/yyyy");
                            worksheet.Cells[row, 5].Value = ocr.Name1;
                            worksheet.Cells[row, 6].Value = ocr.HolderNo;
                            worksheet.Cells[row, 7].Value = ocr.IdNo;
                            worksheet.Cells[row, 8].Value = (ocr.Isemployed == 0 ? "Unemployed" : "Employed");
                            worksheet.Cells[row, 9].Value = occdesc;
                            worksheet.Cells[row, 10].Value = (String.IsNullOrEmpty(ocr.NameOfEmployer) ? "-" : ocr.NameOfEmployer);
                            worksheet.Cells[row, 11].Value = (String.IsNullOrEmpty(ocr.Nob) ? "-" : ocr.Nob);
                            worksheet.Cells[row, 12].Value = incomedesc;
                            worksheet.Cells[row, 13].Value = (String.IsNullOrEmpty(ocr.Addr1) ? "-" : ocr.Addr1);
                            worksheet.Cells[row, 14].Value = (String.IsNullOrEmpty(ocr.Addr2) ? "-" : ocr.Addr2);
                            worksheet.Cells[row, 15].Value = (String.IsNullOrEmpty(ocr.Addr3) ? "-" : ocr.Addr3);
                            worksheet.Cells[row, 16].Value = (String.IsNullOrEmpty(ocr.PostCode) ? "-" : ocr.PostCode);
                            worksheet.Cells[row, 17].Value = (String.IsNullOrEmpty(ocr.City) ? "-" : ocr.City);
                            worksheet.Cells[row, 18].Value = (String.IsNullOrEmpty(ocr.State) ? "-" : ocr.State);
                            worksheet.Cells[row, 19].Value = (String.IsNullOrEmpty(ocr.Country) ? "-" : ocr.Country);
                            worksheet.Cells[row, 20].Value = (String.IsNullOrEmpty(ocr.OfficeTelNo2) ? "-" : ocr.OfficeTelNo2);
                            row++;
                            index++;

                        }

                    }
                    else {
                        response = responseAddress;
                    }
                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    // Apply some predefined styles for data to look nicely :)
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    // Save this data as a file
                    excel.SaveAs(excelFile);
                    // Display SUCCESS message
                    response.IsSuccess = true;
                    response.Message = "Downloading...";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "Occupation Change Request List downloaded",
                        TableName = "ma_holder_reg",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);


                    //Audit Log ends here
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                }
                finally
                {
                    // Release COM objects (very important!)

                    //Empty variables
                    excel = null;
                    worksheet = null;

                    // Force garbage collector cleaning
                    GC.Collect();

                    //change to byte[]
                    byte[] result = File.ReadAllBytes(excelFile.ToString());
                    String file = Convert.ToBase64String(result);
                    //delete temporary file
                    File.Delete(excelFile.ToString());
                    response.Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + file;
                    response.Message = "eApexIs_occupation_change_request_list" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    //response.Data = "data:application/vnd.ms-excel;base64," + file;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + message;
            }
            return response;
        }
    }
}