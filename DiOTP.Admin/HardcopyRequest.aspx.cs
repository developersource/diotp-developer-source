﻿using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class HardcopyRequest : System.Web.UI.Page
    {
        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                StringBuilder filter = new StringBuilder();
                filter.Append(" user_role_id='3'");

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    string columnNameUsername = Converter.GetColumnNameByPropertyName<User>(nameof(DiOTP.Utility.User.Username));
                    filter.Append(" and (" + columnNameUsername + " like '%" + Search.Value.Trim() + "%'");
                    string columnNameEmail = Converter.GetColumnNameByPropertyName<User>(nameof(DiOTP.Utility.User.EmailId));
                    filter.Append(" or " + columnNameEmail + " like '%" + Search.Value.Trim() + "%'");
                    string columnNameIdNo = Converter.GetColumnNameByPropertyName<User>(nameof(DiOTP.Utility.User.IdNo));
                    filter.Append(" or " + columnNameIdNo + " like '%" + Search.Value.Trim() + "%')");
                }

                if (IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    filter.Append(" and is_hard_copy in ('" + FilterValue.Value + "')");
                }
                else
                {
                    filter.Append(" and is_hard_copy in ('0', '1')");
                }
                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = IUserService.GetCountByFilter(filter.ToString()).ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }
                filter.Append(" order by hardcopy_updated_date desc ");
                Response responseUAList = IUserService.GetDataByFilter(filter.ToString(), skip, take, false);
                if (responseUAList.IsSuccess)
                {
                    List<User> users = (List<User>)responseUAList.Data;

                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    foreach (User user in users)
                    {
                        bool isShowUser = true;
                        
                        if (isShowUser)
                        {
                            asb.Append(@"<tr>
                                <td class='icheck'>
                                    <div class='square single-row'>
                                        <div class='checkbox'>
                                            <input type='checkbox' name='checkRow' class='checkRow' value='" + user.Id + @"' /> <label>" + index + @"</label><br/>
                                        </div>
                                    </div>
                                    <span class='row-status'>" + (user.IsHardCopy == 1 ?
                                                                        "<span class='label label-success'>Hardcopy</span>" :
                                                                        "<span class='label label-danger'>Softcopy</span>")
                                                                        + @"</span>
                                </td>
                                <td>" + user.Username + @"</a>
                                <td>" + user.IdNo + @"</a>
                                <td>" + user.EmailId + @"</td>
                                <td>" + user.HardcopyUpdatedDate.Value.ToString("dd/MM/yyyy HH:mm:ss") + @"</td>
                            </tr>");
                            index++;
                        }
                    }
                    hardcopyTbody.InnerHtml = asb.ToString();
                }
            }
        }
        
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            try
            {
                string idString = String.Join(",", ids);
                Response responseUList = IUserService.GetDataByFilter(" ID in (" + idString + ")", 0, 0, false);
                if (responseUList.IsSuccess)
                {
                    List<User> users = (List<User>)responseUList.Data;
                    if (action == "Deactivate")
                    {
                        users.ForEach(x =>
                        {
                            Response responseUList1 = IUserService.GetSingle(x.Id);
                            if (responseUList1.IsSuccess)
                            {
                                User userX = (User)responseUList1.Data;


                                MaHolderReg maHolderReg = new MaHolderReg();
                                Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(userX.UserIdUserAccounts.Where(y => y.IsPrimary == 1).FirstOrDefault().AccountNo);

                                if (responseMHR.IsSuccess)
                                {
                                    maHolderReg = (MaHolderReg)responseMHR.Data;
                                    Email email = new Email();
                                    email.user = userX;
                                    email.user.Username = maHolderReg.Name1;
                                    EmailService.SendUpdateMail(email, "User Information Update", "Your Hardcopy has been deactivated successfully on", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");
                                }


                            }
                            x.IsHardCopy = 0;
                            x.HardcopyUpdatedDate = DateTime.Now;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = x.Username + " (" + x.IdNo + ") Hardcopy deactivated",
                                TableName = "users",
                                UpdatedDate = DateTime.Now,
                                UserId = x.Id
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "is_hard_copy",
                                ValueOld = "1",
                                ValueNew = "0",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);

                            //Audit Log Ends here
                        });
                        IUserService.UpdateBulkData(users);
                        
                    }
                    if (action == "Activate")
                    {
                        users.ForEach(x =>
                        {
                            Response responseUList1 = IUserService.GetSingle(x.Id);
                            if (responseUList1.IsSuccess)
                            {
                                User userX = (User)responseUList1.Data;
                                MaHolderReg maHolderReg = new MaHolderReg();
                                Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(userX.UserIdUserAccounts.Where(y => y.IsPrimary == 1).FirstOrDefault().AccountNo);

                                if (responseMHR.IsSuccess)
                                {
                                    maHolderReg = (MaHolderReg)responseMHR.Data;
                                    Email email = new Email();
                                    email.user = userX;
                                    email.user.Username = maHolderReg.Name1;
                                    EmailService.SendUpdateMail(email, "User Information Update", "Your Hardcopy has been activated successfully", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");
                                }
                            }
                            x.IsHardCopy = 1;
                            x.HardcopyUpdatedDate = DateTime.Now;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = x.Username + " (" + x.IdNo + ") Hardcopy activated",
                                TableName = "users",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "is_hard_copy",
                                ValueOld = "0",
                                ValueNew = "1",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);

                            //Audit Log Ends here
                        });
                        IUserService.UpdateBulkData(users);
                    }
                    if(action == "Toggle")
                    {
                        users.ForEach(x =>
                        {
                            Response responseUList1 = IUserService.GetSingle(x.Id);
                            if (responseUList1.IsSuccess)
                            {
                                User userX = (User)responseUList1.Data;
                                MaHolderReg maHolderReg = new MaHolderReg();
                                Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(userX.UserIdUserAccounts.Where(y => y.IsPrimary == 1).FirstOrDefault().AccountNo);

                                if (responseMHR.IsSuccess)
                                {
                                    maHolderReg = (MaHolderReg)responseMHR.Data;
                                    Email email = new Email();
                                    email.user = userX;
                                    email.user.Username = maHolderReg.Name1;
                                    EmailService.SendUpdateMail(email, "User Information Update", "Your Hardcopy has been " + (x.IsHardCopy == 0 ? "activated" : "de-activated") + " successfully", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");
                                }
                            }
                            //change hardcopy status
                            x.IsHardCopy = (x.IsHardCopy == 0 ? 1 : 0);
                            x.HardcopyUpdatedDate = DateTime.Now;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = x.Username + " (" + x.IdNo + ((x.IsHardCopy == 0 ? ") Hardcopy deactivated" : ") Hardcopy activated") ),
                                TableName = "users",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "is_hard_copy",
                                ValueOld = (x.IsHardCopy == 0 ? "1" : "0"),
                                ValueNew = (x.IsHardCopy == 0 ? "0" : "1"),
                            };
                            Response response2 = IAdminLogSubService.PostData(als);

                            //Audit Log Ends here
                        });
                        IUserService.UpdateBulkData(users);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("User hardcopy action: " + ex.Message);
                return false;
            }
        }

        

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Download(string filter)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response response = new Response();
            string message = string.Empty;
            try
            {
                
                string tempPath = Path.GetTempPath() + "Hardcopy_Requests_" + DateTime.Now.ToString("yyyyMMddhhmmssfff");
                FileInfo excelFile = new FileInfo(tempPath);
                message = excelFile.FullName;
                ExcelPackage excel = new ExcelPackage();
                excel.Workbook.Worksheets.Add("Hardcopy Requests");

                var headerRow = new List<string[]>()
            {
                new string[] { "S.No", "Status", "Username", "ID No", "Email", "Updated Date" }
            };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A2:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "2";

                var worksheet = excel.Workbook.Worksheets["Hardcopy Requests"];

                string docDetails = "Hardcopy Requests";

                try
                {
                    // ------------------------------------------------
                    // Creation of header cells
                    // ------------------------------------------------
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    // ------------------------------------------------
                    // Populate sheet with some real data from list
                    // ------------------------------------------------
                    int row = 3; // start row (in row 1 are header cells)
                    StringBuilder filterQuery = new StringBuilder();
                    if (filter == "" || filter == null)
                    {
                        docDetails += " | Status: All (Hardcopy, Softcopy)";
                        filterQuery.Append(" user_role_id = '3' ");
                    }
                    else
                    {
                        docDetails += " | Status: " + (filter == "1" ? "Hardcopy" : (filter == "0" ? "Softcopy" : "-")) + " ";
                        filterQuery.Append(" user_role_id = '3' and is_hard_copy = '"+ filter + "' ");
                    }

                    string docDetailsRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    worksheet.Cells[docDetailsRange].Merge = true;
                    worksheet.Cells[docDetailsRange].Value = docDetails;
                    worksheet.Cells[docDetailsRange].Style.Font.Bold = true;

                    Response responseUAList = IUserService.GetDataByFilter(filterQuery.ToString(), 0, 0, true);
                    if (responseUAList.IsSuccess)
                    {
                        List<User> users = (List<User>)responseUAList.Data;
                        int index = 1;
                        foreach (User user in users)
                        {
                            worksheet.Cells[row, 1].Value = index;
                            if (user.IsHardCopy == 1)
                            {
                                worksheet.Cells[row, 2].Value = "Hardcopy";
                                worksheet.Cells[row, 2].Style.Font.Color.SetColor(System.Drawing.Color.White);
                                worksheet.Cells[row, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[row, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Green);
                            }
                            else if (user.IsHardCopy == 0)
                            {
                                worksheet.Cells[row, 2].Value = "Softcopy";
                                worksheet.Cells[row, 2].Style.Font.Color.SetColor(System.Drawing.Color.White);
                                worksheet.Cells[row, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[row, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Red);
                            }
                            worksheet.Cells[row, 3].Value = user.Username;
                            worksheet.Cells[row, 4].Value = user.IdNo;
                            worksheet.Cells[row, 5].Value = user.EmailId;
                            worksheet.Cells[row, 6].Value = (user.HardcopyUpdatedDate.Value == default(DateTime) ? " - " : user.HardcopyUpdatedDate.Value.ToString("dd/MM/yyyy hh:mm:ss tt"));
                            row++;
                            index++;
                        }
                    }

                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    // Apply some predefined styles for data to look nicely :)
                    worksheet.Cells[headerRange].Style.Font.Bold = true;

                    // Save this data as a file
                    excel.SaveAs(excelFile);

                    // Display SUCCESS message
                    response.IsSuccess = true;
                    response.Message = "Downloading...";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "User Hardcopy Request List downloaded",
                        TableName = "users",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);


                    //Audit Log ends here
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                }
                finally
                {
                    
                    //Empty variables
                    excel = null;
                    worksheet = null;

                    // Force garbage collector cleaning
                    GC.Collect();

                    //change to byte[]
                    byte[] result = File.ReadAllBytes(excelFile.ToString());
                    String file = Convert.ToBase64String(result);
                    //delete temporary file
                    File.Delete(excelFile.ToString());
                    response.Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + file;
                    response.Message = "Hardcopy_Request" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    
                }
            }
            catch(Exception ex)
            {
                response.Message = ex.Message + message;
            }
            return response;
        }
    }
}