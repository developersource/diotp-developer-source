﻿using Admin.FPXLibary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class FPXTransactionStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FPXPayment(
                "SA2100000015", "20210106121553", "30000.00", 
                "kaixuan@petraware.com", "kaixuan@petraware.com", "Lim Soon Jin", "0", 
                "0", "APEX", "0", 
                "TEST0021", "AE");
        }

        public void FPXPayment(
            string orderNo, string transDate, string txn_amt,
            string buyer_mail_id, string buyer_id, string buyerName, string buyerBankBranch,
            string buyerAccNo, string makerName, string buyerIBAN,
            string buyerBankId, string msgType)
        {
            Controller c = new Controller();
            String checksum = "";
            Random RandomNumber = new Random();
            String fpx_msgType = msgType;
            String fpx_msgToken = "01";
            String fpx_sellerExId = "EX00008813";
            String fpx_sellerExOrderNo = orderNo;
            String fpx_sellerOrderNo = orderNo;
            String fpx_sellerTxnTime = transDate;
            String fpx_sellerId = "SE00010202";
            String fpx_sellerBankCode = "01";
            String fpx_txnCurrency = "MYR";
            String fpx_txnAmount = txn_amt;
            String fpx_buyerEmail = buyer_mail_id;
            String fpx_buyerId = buyer_id;
            String fpx_buyerName = buyerName;
            String fpx_buyerBankId = buyerBankId;
            String fpx_buyerBankBranch = buyerBankBranch;
            String fpx_buyerAccNo = buyerAccNo;
            String fpx_makerName = makerName;
            String fpx_buyerIban = buyerIBAN;
            String fpx_productDesc = "PD";
            String fpx_version = "6.0";
            String fpx_checkSum = "";
            fpx_checkSum = fpx_buyerAccNo + "|" + fpx_buyerBankBranch + "|" + fpx_buyerBankId + "|" + fpx_buyerEmail + "|" + fpx_buyerIban + "|" + fpx_buyerId + "|" + fpx_buyerName + "|";
            fpx_checkSum += fpx_makerName + "|" + fpx_msgToken + "|" + fpx_msgType + "|" + fpx_productDesc + "|" + fpx_sellerBankCode + "|" + fpx_sellerExId + "|";
            fpx_checkSum += fpx_sellerExOrderNo + "|" + fpx_sellerId + "|" + fpx_sellerOrderNo + "|" + fpx_sellerTxnTime + "|" + fpx_txnAmount + "|" + fpx_txnCurrency + "|" + fpx_version;
            fpx_checkSum = fpx_checkSum.Trim();

            checksum = c.RSASign(fpx_checkSum, Request.PhysicalApplicationPath + "EX00008813.key"); //Exchange Key name 
            String finalMsg = fpx_checkSum;
            string FPXPostRequestUrl = ConfigurationManager.AppSettings["FPXPostRequestUrl"].ToString();
            var formPostText = @"<html><body><div>
                    <form method='POST' action='" + FPXPostRequestUrl + @"' name='FPXPaymentForm'>
                      <input type='hidden' name='txn_amt' value='" + fpx_txnAmount + @"' /> 
                      <input type='hidden' value='" + fpx_msgType + @"' name='fpx_msgType'>

                      <input type='hidden' value='" + fpx_msgToken + @"' name='fpx_msgToken'>
                      <input type='hidden' value='" + fpx_sellerExId + @"' name='fpx_sellerExId'>
                      <input type='hidden' value='" + fpx_sellerExOrderNo + @"' name='fpx_sellerExOrderNo'>
                      <input type='hidden' value='" + fpx_sellerTxnTime + @"' name='fpx_sellerTxnTime'>
                      <input type='hidden' value='" + fpx_sellerOrderNo + @"' name='fpx_sellerOrderNo'>
                      <input type='hidden' value='" + fpx_sellerBankCode + @"' name='fpx_sellerBankCode'>
                      <input type='hidden' value='" + fpx_txnCurrency + @"' name='fpx_txnCurrency'>
                      <input type='hidden' value='" + fpx_txnAmount + @"' name='fpx_txnAmount'>
                      <input type='hidden' value='" + fpx_buyerEmail + @"' name='fpx_buyerEmail'>
                      <input type='hidden' value='" + checksum + @"' name='fpx_checkSum'>
                      <input type='hidden' value='" + fpx_buyerName + @"' name='fpx_buyerName'>
                      <input type='hidden' value='" + fpx_buyerBankId + @"' name='fpx_buyerBankId'>
                      <input type='hidden' value='" + fpx_buyerBankBranch + @"' name='fpx_buyerBankBranch'>
                      <input type='hidden' value='" + fpx_buyerAccNo + @"' name='fpx_buyerAccNo'>
                      <input type='hidden' value='" + fpx_buyerId + @"' name='fpx_buyerId'>
                      <input type='hidden' value='" + fpx_makerName + @"' name='fpx_makerName'>
                      <input type='hidden' value='" + fpx_buyerIban + @"' name='fpx_buyerIban'>
                      <input type='hidden' value='" + fpx_productDesc + @"' name='fpx_productDesc'>
                      <input type='hidden' value='" + fpx_version + @"' name='fpx_version'>
                      <input type='hidden' value='" + fpx_sellerId + @"' name='fpx_sellerId'>
                      <input type='hidden' value='" + checksum + @"' name='checkSum_String'>
                      <input type='hidden' value='" + Request.PhysicalApplicationPath + @"'>
                    </form></div><script type='text/javascript'>document.FPXPaymentForm.submit();</script></body></html>
                    ";
            Response.Write(formPostText);
        }
    }
}