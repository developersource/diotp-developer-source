﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AddressChanging : System.Web.UI.Page
    {
        private static readonly Lazy<IMaHolderRegService> lazyMaHolderRegServiceObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());
        public static IMaHolderRegService IMaHolderRegService { get { return lazyMaHolderRegServiceObj.Value; } }

        private static readonly Lazy<IStatesDefService> lazyStatesDefServiceObj = new Lazy<IStatesDefService>(() => new StatesDefService());
        public static IStatesDefService IStatesDefService { get { return lazyStatesDefServiceObj.Value; } }

        private static readonly Lazy<ICountriesDefService> lazyCountriesDefServiceObj = new Lazy<ICountriesDefService>(() => new CountriesDefService());
        public static ICountriesDefService ICountriesDefService { get { return lazyCountriesDefServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserTypeService> lazyUserTypeServiceObj = new Lazy<IUserTypeService>(() => new UserTypeService());

        public static IUserTypeService IUserTypeService { get { return lazyUserTypeServiceObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {

            StringBuilder filter = new StringBuilder();
            string mainQ = (@"SELECT * FROM");
            string mainQCount = (@"select count(*) from ");
            filter.Append(@" ma_holder_reg
                             where 1=1 ");
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                User user = (User)Session["admin"];
                Response response = IUserTypeService.GetDataByFilter("user_id = '" + user.Id + "'", 0, 0, false);
                UserType loginUserType = new UserType();
                if (response.IsSuccess)
                {
                    loginUserType = ((List<UserType>)response.Data).FirstOrDefault();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and (NAME_1 like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or HOLDER_NO like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ID_NO like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ADDR_1 like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ADDR_2 like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ADDR_3 like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ADDR_4 like '%" + Search.Value.Trim() + "%')");
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    if (FilterValue.Value != "All")
                        filter.Append(" and OTP_ACT_ST in ('" + FilterValue.Value + "')");
                    else
                        filter.Append(@" and OTP_ACT_ST in ('0','1','9') ");
                }
                else
                {
                    filter.Append(@"and OTP_ACT_ST in ('0') ");
                }

                if (Request.QueryString["hdnOrderByColumn"] != "" && Request.QueryString["hdnOrderByColumn"] != null && Request.QueryString["hdnOrderByColumn"].Trim() != "-")
                {
                    hdnOrderByColumn.Value = Request.QueryString["hdnOrderByColumn"].ToString();
                    hdnOrderType.Value = Request.QueryString["hdnOrderType"].ToString();
                    filter.Append(" order by " + hdnOrderByColumn.Value + " " + hdnOrderType.Value + " ");
                }
                else
                {
                    if (FilterValue.Value == "0" || string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                    {
                        filter.Append("order by OTP_ACT_ST, OTP_EXPIRY_DATE desc");
                    }
                    else
                    {
                        filter.Append("order by OTP_ACT_ST, OTP_ENT_DT desc ");
                    }
                }


                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);



                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + filter.ToString()).Data.ToString();

                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseMAHolderRegList = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, false);
                if (responseMAHolderRegList.IsSuccess)
                {
                    var AddressDyn = responseMAHolderRegList.Data;
                    var responseJSON = JsonConvert.SerializeObject(AddressDyn);
                    List<MaHolderReg> address = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MaHolderReg>>(responseJSON);


                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    foreach (MaHolderReg a in address)
                    {

                        Response responseStateList = IStatesDefService.GetDataByFilter("(select trim(LEADING '0' from code)) = '" + a.StateCode + "' ", 0, 0, false);
                        if (responseStateList.IsSuccess)
                        {
                            StatesDef statesDef = ((List<StatesDef>)responseStateList.Data).FirstOrDefault();
                            Response responseCountryList = ICountriesDefService.GetDataByFilter("code = '" + a.CountryRes + "'", 0, 0, false);
                            if (responseCountryList.IsSuccess)
                            {
                                CountriesDef countriesDef = ((List<CountriesDef>)responseCountryList.Data).FirstOrDefault();

                                string checkBox = string.Empty;
                                if (a.OtpActSt == "0")
                                {
                                    checkBox = @"<div class='square single-row'>
                                                <div class='checkbox'>
                                                    <input type = 'checkbox' name='checkRow' class='checkRow' value='" + a.Id + @"' /> <label>" + index + @"</label><br/>
                                                </div>
                                            </div>";
                                }
                                else
                                {
                                    checkBox = @"<label>" + index + @"</label><br/>";
                                }

                                asb.Append(@"<tr>
                                                <td class='icheck'>
                                                    " + checkBox + @"
                                                    <span class='row-status'>" + (a.OtpActSt == "1" ? "<span class='label label-success'>Approved</span>" :
                                                                                    a.OtpActSt == "0" ? "<span class='label label-warning'>Pending</span>" : "<span class='label label-danger'>Rejected</span>") + @"
                                                    </span>
                                                </td>
                                                <td>" + a.OtpExpiryDate.Value.ToString("dd/MM/yyyy") + @"</td>
                                                <td>" + (a.OtpActSt == "0" ? "-" : a.OtpEntDt.Value.ToString("dd/MM/yyyy")) + @"</td>
                                                <td>" + a.Name1 + @"</td>
                                                <td>" + a.HolderNo + @"</td>
                                                <td>" + a.IdNo + @"</td>
                                                <td>" + a.Addr1 + @"</td>
                                                <td>" + a.Addr2 + @"</td>
                                                <td>" + a.Addr3 + @"</td>
                                                <td>" + a.Addr4 + @"</td>
                                                <td>" + a.Postcode + @"</td>
                                                <td>" + statesDef.Name + @"</td>
                                                <td>" + countriesDef.Name + @"</td>
                                                </tr>"
                                            );
                                index++;
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                            "alert('" + responseCountryList.Message + "');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                        "alert('" + responseStateList.Message + "');", true);
                        }
                    }
                    usersTbody.InnerHtml = asb.ToString();

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                "alert('" + responseMAHolderRegList.Message + "');", true);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            string message = "";
            try
            {
                String title = "Address Information Update";
                List<string> content1s = new List<string>();
                List<string> content2s = new List<string>();
                List<Email> emails = new List<Email>();
                List<String> dates = new List<String>();
                string idString = String.Join(",", ids);
                Response responseMAHolderRegList = IMaHolderRegService.GetDataByFilter(" ID in (" + idString + ")", 0, 0, false);
                if (responseMAHolderRegList.IsSuccess)
                {
                    List<MaHolderReg> mhrs = (List<MaHolderReg>)responseMAHolderRegList.Data;

                    if (action == "Activate")
                    {
                        mhrs.ForEach(x =>
                        {
                            if (x.OtpActSt == "0")
                            {
                                Response responseUAList = IUserAccountService.GetDataByFilter("account_no = '" + x.HolderNo + "' ", 0, 0, false);
                                UserAccount ua = new UserAccount();
                                if (responseUAList.IsSuccess)
                                {
                                    ua = ((List<UserAccount>)responseUAList.Data).FirstOrDefault();
                                    //Response responseUList = IUserService.GetDataByFilter("ID = '" + ua.UserId + "' ", 0, 0, false);
                                    //if (responseUList.IsSuccess)
                                    //{
                                    //User u = ((List<User>)responseUList.Data).FirstOrDefault();

                                    Email email = new Email();
                                    Response rU = IUserService.GetSingle(ua.UserId);
                                    if (rU.IsSuccess)
                                    {
                                        email.user = (User)rU.Data;
                                        ua.UserIdUser = email.user;
                                    }
                                    content1s.Add("Address change request has been approved by Apex admin on ");
                                    dates.Add(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                                    content2s.Add("");
                                    emails.Add(email);



                                    //}
                                }
                                x.OtpEntDt = DateTime.Now;
                                x.OtpActSt = "1";
                                message += "Address change request of " + x.Name1 + " is successfully Approved.<br/>";
                                UserLogMain z = new UserLogMain()
                                {
                                    TableName = "ma_holder_reg",
                                    Description = "Admin approved user request for MA address change",
                                    UpdatedDate = DateTime.Now,
                                    UserId = ua.UserId,
                                    RefId = ua.Id,
                                    RefValue = ua.AccountNo,
                                    StatusType = 0
                                };
                                IUserLogMainService.PostData(z);
                                
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "User (" + ua.UserIdUser.Username + ") Address Change Request Successfully approved",
                                    TableName = "ma_holder_reg",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                alm = (AdminLogMain)responseLog.Data;
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "OTP_ACT_ST",
                                    ValueOld = "0",
                                    ValueNew = "1",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);

                                //Audit Log Ends here
                            }
                            else if (x.OtpActSt == "9")
                            {
                                message += "Address of " + x.Name1 + " is already rejected.<br/>";
                            }
                            else if (x.OtpActSt == "1")
                            {
                                message += "Address of " + x.Name1 + " is already approved.<br/>";
                            }
                        });
                        IMaHolderRegService.UpdateBulkData(mhrs);
                    }
                    if (action == "Reject")
                    {
                        mhrs.ForEach(x =>
                        {
                            if (x.OtpActSt == "0")
                            {
                                Response responseUAList = IUserAccountService.GetDataByFilter("account_no = '" + x.HolderNo + "' ", 0, 0, false);
                                UserAccount ua = new UserAccount();
                                if (responseUAList.IsSuccess)
                                {
                                    ua = ((List<UserAccount>)responseUAList.Data).FirstOrDefault();

                                    Email email = new Email();
                                    Response rU = IUserService.GetSingle(ua.UserId);
                                    if (rU.IsSuccess)
                                    {
                                        email.user = (User)rU.Data;
                                        ua.UserIdUser = email.user;
                                    }
                                    content1s.Add("Address change request has been rejected by Apex admin on ");
                                    dates.Add(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                                    content2s.Add("");
                                    emails.Add(email);

                                }
                                x.OtpActSt = "9";
                                x.OtpEntDt = DateTime.Now;
                                UserLogMain z = new UserLogMain()
                                {
                                    TableName = "ma_holder_reg",
                                    Description = "Admin rejected user request for MA address change",
                                    UpdatedDate = DateTime.Now,
                                    UserId = ua.UserId,
                                    RefId = ua.Id,
                                    RefValue = ua.AccountNo,
                                    StatusType = 0
                                };
                                IUserLogMainService.PostData(z);
                                message += "Address of " + x.Name1 + " is Rejected.<br/>";
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "User (" + ua.UserIdUser.Username + ")  Address Change Request rejected",
                                    TableName = "ma_holder_reg",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                alm = (AdminLogMain)responseLog.Data;
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "OTP_ACT_ST",
                                    ValueOld = "1",
                                    ValueNew = "0",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);

                                //Audit Log Ends here
                            }
                            else if (x.OtpActSt == "9")
                            {
                                message += "Address of " + x.Name1 + " is already rejected.<br/>";
                            }
                            else if (x.OtpActSt == "1")
                            {
                                message += "Address of " + x.Name1 + " is already approved.<br/>";
                            }
                        });
                        IMaHolderRegService.UpdateBulkData(mhrs);
                    }
                }
                emails.ForEach(x =>
                {
                    EmailService.SendUpdateMail(x, title, content1s[emails.IndexOf(x)], dates[emails.IndexOf(x)], content2s[emails.IndexOf(x)]);
                });
                response.IsSuccess = true;
                response.Message = message;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.WriteLine("User accounts action: " + ex.Message);
            }
            return response;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Download(string filter)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response response = new Response();
            string message = string.Empty;
            try
            {

                string tempPath = Path.GetTempPath() + "Address_" + DateTime.Now.ToString("yyyyMMddhhmmssfff");
                FileInfo excelFile = new FileInfo(tempPath);
                message = excelFile.FullName;
                ExcelPackage excel = new ExcelPackage();
                excel.Workbook.Worksheets.Add("Address Change Requests");

                var headerRow = new List<string[]>()
            {
                new string[] { "S.No", "Status", "Requested date", "Appv/Rej date", "Username", "MA No.", "Id No.", "Address line 1", "Address line 2", "Address line 3", "Address line 4", "Postcode", "State", "Country" }
            };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A2:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "2";

                var worksheet = excel.Workbook.Worksheets["Address Change Requests"];

                string docDetails = "Address Change Requests";

                try
                {
                    // ------------------------------------------------
                    // Creation of header cells
                    // ------------------------------------------------
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    // ------------------------------------------------
                    // Populate sheet with some real data from list
                    // ------------------------------------------------
                    int row = 3; // start row (in row 1 are header cells)
                    StringBuilder filterQuery = new StringBuilder();
                    StringBuilder jfilter = new StringBuilder();


                    filterQuery.Append(@"select * from ma_holder_reg where 1=1 ");

                    if (!string.IsNullOrEmpty(filter) && filter != "")
                    {

                        docDetails += " | Status: " + (filter == "0" ? "Pending" : (filter == "1" ? "Approved" : (filter == "9" ? "Rejected" : (filter == "All" ? "All" : "-")))) + " ";
                        if (filter != "All")
                            filterQuery.Append(" and OTP_ACT_ST in ('" + filter + "')");
                        else
                            filterQuery.Append(@" and OTP_ACT_ST in ('0','1','9') ");
                    }
                    else
                    {
                        docDetails += " | Status: Pending";
                        filterQuery.Append(@" and OTP_ACT_ST in ('0') ");
                    }
                    int skip = 0, take = 20;
                    filterQuery.Append(@"order by OTP_ACT_ST desc, OTP_EXPIRY_DATE desc");

                    string docDetailsRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    worksheet.Cells[docDetailsRange].Merge = true;
                    worksheet.Cells[docDetailsRange].Value = docDetails;
                    worksheet.Cells[docDetailsRange].Style.Font.Bold = true;

                    Response responseAddress = GenericService.GetDataByQuery(filterQuery.ToString(), skip, take, false, null, false, null, false);
                    if (responseAddress.IsSuccess)
                    {
                        var AddressDyn = responseAddress.Data;
                        var responseJSON = JsonConvert.SerializeObject(AddressDyn);
                        List<DiOTP.Utility.MaHolderReg> Address = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.MaHolderReg>>(responseJSON);

                        StringBuilder asb = new StringBuilder();

                        int index = 1;

                        foreach (MaHolderReg mhr in Address)
                        {
                            Response responseStateList = IStatesDefService.GetDataByFilter("(select trim(LEADING '0' from code)) = '" + mhr.StateCode + "' ", 0, 0, false);
                            if (responseStateList.IsSuccess)
                            {
                                StatesDef statesDef = ((List<StatesDef>)responseStateList.Data).FirstOrDefault();
                                Response responseCountryList = ICountriesDefService.GetDataByFilter("code = '" + mhr.CountryRes + "'", 0, 0, false);
                                if (responseCountryList.IsSuccess)
                                {
                                    CountriesDef countriesDef = ((List<CountriesDef>)responseCountryList.Data).FirstOrDefault();
                                    worksheet.Cells[row, 1].Value = index;
                                    worksheet.Cells[row, 2].Value = (mhr.OtpActSt == "0" ? "Pending" : (mhr.OtpActSt == "1" ? "Approved" : (mhr.OtpActSt == "9" ? "Rejected" : "-")));
                                    worksheet.Cells[row, 3].Value = mhr.OtpExpiryDate.Value.ToString("dd/MM/yyyy");
                                    worksheet.Cells[row, 4].Value = mhr.OtpActSt == "0" ? "-" : mhr.OtpEntDt.Value.ToString("dd/MM/yyyy");
                                    worksheet.Cells[row, 5].Value = mhr.Name1;
                                    worksheet.Cells[row, 6].Value = mhr.HolderNo;
                                    worksheet.Cells[row, 7].Value = mhr.IdNo;
                                    worksheet.Cells[row, 8].Value = mhr.Addr1;
                                    worksheet.Cells[row, 9].Value = mhr.Addr2;
                                    worksheet.Cells[row, 10].Value = mhr.Addr3;
                                    worksheet.Cells[row, 11].Value = mhr.Addr4;
                                    worksheet.Cells[row, 12].Value = mhr.Postcode;
                                    worksheet.Cells[row, 13].Value = statesDef.Name;
                                    worksheet.Cells[row, 14].Value = countriesDef.Name;
                                    row++;
                                    index++;
                                }
                            }
                        }

                    }

                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    // Apply some predefined styles for data to look nicely :)
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    // Save this data as a file
                    excel.SaveAs(excelFile);
                    // Display SUCCESS message
                    response.IsSuccess = true;
                    response.Message = "Downloading...";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "Address Change Request List downloaded",
                        TableName = "ma_holder_reg",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);


                    //Audit Log ends here
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                }
                finally
                {
                    // Release COM objects (very important!)

                    //Empty variables
                    excel = null;
                    worksheet = null;

                    // Force garbage collector cleaning
                    GC.Collect();

                    //change to byte[]
                    byte[] result = File.ReadAllBytes(excelFile.ToString());
                    String file = Convert.ToBase64String(result);
                    //delete temporary file
                    File.Delete(excelFile.ToString());
                    response.Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + file;
                    response.Message = "eApexIs_address_list" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    //response.Data = "data:application/vnd.ms-excel;base64," + file;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + message;
            }
            return response;
        }

    }
}