﻿    function numberWithCommas(number) {
            var parts = number.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }

        //var MaCountDiOTP = parseInt($('#MaCountDiOTP').val());
        //var MaCountOracle = parseInt($('#MaCountOracle').val());
        //var MaAssetDiOTP = parseFloat(parseFloat($('#MaAssetDiOTP').val()).toFixed(2));
        //var MaAssetOracle = parseFloat(parseFloat($('#MaAssetOracle').val()).toFixed(2));


        var machart = Highcharts.chart('ma-chart', {
        chart: {
        type: 'column'
            },
            title: {
        text: ''
                //text: 'Monthly MA Accounts'
            },
            //subtitle: {
        //    text: 'Source: '
        //},
        credits: {
        href: 'javascript:;',
                text: '',
                position: {
        align: 'right',
                    verticalAlign: 'bottom',
                }
            },
            xAxis: {
        categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
        min: 0,
                title: {
        text: 'MA Count'
                }
            },
            tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                formatter: function () {
                    var s = [];
                    s.push('<span style="font-size:10px">' + this.points[0].key + '</span><table>');
                    $.each(this.points, function (i, point) {
                        var seriesName = point.series.name;
                        var y = point.y;
                        var x = point.x;

                        var str = y.toString().split('.');
                        if (str[0].length >= 4) {
                str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
                        }
                        //s.push('<span style="text-decoration:underline;">Date: ' + Highcharts.dateFormat('%d/%m/%Y', new Date(x)) + '</span><br /><span style="color:' + point.color + '">Realized Profit: ' + (str.join('.')) + ' </span>');
                        s.push('<tr><td style="color:' + point.color + ';padding:0">' + seriesName + ': </td>' +
                            '<td style="padding:0"><b>' + (str.join('.')) + '</b></td></tr>');
                    });
                    s.push('</table>');

                    return s.join('');
                },
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
        column: {
        //grouping: false,
        shadow: false,
                    //pointPadding: 0.2,
                    borderWidth: 0,
                    showInLegend: true,
                    dataLabels: {
        enabled: true,
                        //formatter: function () {
        //    var y = this.y;
        //    var max = this.series.yAxis.max,
        //        color = this.y / max < 0.05 ? 'black' : 'white'; // 5% width
        //    return '<span style="color: ' + color + '">' + y + ' </span>';
        //},
        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        y: 10, // 10 pixels down from the top
                        style: {
        fontSize: '10px',
                            fontFamily: 'helvetica, arial, sans-serif',
                            textShadow: false,
                            fontWeight: 'bold',
                            textOutline: false
                        }
                    }
                },
                point: {
        events: {
        legendItemClick: function () {
        this.slice(null);
                            return false;
                        }
                    }
                }
            },
            series: []
        });
        var aumchart = Highcharts.chart('aum-chart', {
        chart: {
        type: 'column'
            },
            title: {
        text: ''
                //text: 'Monthly Investment'
            },
            //subtitle: {
        //    text: 'Source: '
        //},
        credits: {
        href: 'javascript:;',
                text: '',
                position: {
        align: 'right',
                    verticalAlign: 'bottom',
                }
            },
            xAxis: {
        categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
        min: 0,
                title: {
        text: 'Investment (MYR)'
                }
            },
            tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                formatter: function () {
                    var s = [];
                    s.push('<span style="font-size:10px">' + this.points[0].key + '</span><table>');
                    $.each(this.points, function (i, point) {
                        var seriesName = point.series.name;
                        var y = point.y;
                        var x = point.x;

                        var str = y.toString().split('.');
                        if (str[0].length >= 4) {
                str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
                        }
                        //s.push('<span style="text-decoration:underline;">Date: ' + Highcharts.dateFormat('%d/%m/%Y', new Date(x)) + '</span><br /><span style="color:' + point.color + '">Realized Profit: ' + (str.join('.')) + ' </span>');
                        s.push('<tr><td style="color:' + point.color + ';padding:0">' + seriesName + ': </td>' +
                            '<td style="padding:0"><b>MYR ' + (str.join('.')) + '</b></td></tr>');
                    });
                    s.push('</table>');

                    return s.join('');
                },
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
        column: {
        //grouping: false,
        shadow: false,
                    pointPadding: 0.2,
                    borderWidth: 0
                },
                point: {
        events: {
        legendItemClick: function () {
        this.slice(null);
                            return false;
                        }
                    }
                }
            },
        });
        $(document).ready(function () {

        $.ajax({
            url: "Dashboard.aspx/GetMACountStats",
            async: false,
            contentType: 'application/json; charset=utf-8',
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                var json = data.d;
                var MaCountDiOTP = parseInt(json.MaCountDiOTP);
                var MaCountOracle = parseInt(json.MaCountOracle);

                var donutChartMACount = Highcharts.chart('ma-count-chart', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: 0,
                        plotShadow: false,
                    },
                    title: {
                        text: numberWithCommas(MaCountDiOTP) + ' <small>vs</small> ' + numberWithCommas(MaCountOracle),
                        align: 'center',
                        verticalAlign: 'middle',
                        y: 80,
                        useHTML: true,
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>',
                        followPointer: true,
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    credits: {
                        href: 'javascript:;',
                        text: '',
                        position: {
                            align: 'right',
                            verticalAlign: 'bottom',
                        }
                    },
                    plotOptions: {
                        pie: {
                            shadow: false,
                            showInLegend: true,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}: {point.percentage:.2f} %',
                                style: {
                                    fontWeight: 'normal'
                                }
                            },
                            startAngle: -90,
                            endAngle: 90,
                            center: ['50%', '75%'],
                            size: '100%',
                            point: {
                                events: {
                                    legendItemClick: function () {
                                        this.slice(null);
                                        return false;
                                    }
                                }
                            }
                        },
                    },
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal'
                                },
                                yAxis: {
                                    labels: {
                                        align: 'left',
                                        x: 0,
                                        y: -5
                                    },
                                    title: {
                                        text: null
                                    }
                                },
                                subtitle: {
                                    text: null
                                },
                                credits: {
                                    enabled: false
                                }
                            }
                        }],
                    }
                });

                var donutChartMACountSeriesData = [];
                donutChartMACountSeriesData.push({ className: "highcharts-color-green", name: "Registered", y: MaCountDiOTP });
                donutChartMACountSeriesData.push({ className: "highcharts-color-blue", name: "Unregistered", y: MaCountOracle - MaCountDiOTP });
                //donutChartMACountSeriesData.push(["Unregistered", MaCountOracle - MaCountDiOTP]);
                donutChartMACount.addSeries({
                    type: 'pie',
                    name: 'Percent',
                    innerSize: '50%',
                    data: donutChartMACountSeriesData,
                    type: 'pie',
                    useHTML: true
                });

            }
        });

            $.ajax({
        url: "Dashboard.aspx/GetMAAssetStats",
                async: false,
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    var json = data.d;
                    var MaAssetDiOTP = parseInt(json.MaAssetDiOTP);
                    var MaAssetOracle = parseInt(json.MaAssetOracle);

                    var donutChartMAAsset = Highcharts.chart('ma-asset-chart', {
        chart: {
        plotBackgroundColor: null,
                            plotBorderWidth: 0,
                            plotShadow: false,
                        },
                        title: {
        text: numberWithCommas(MaAssetDiOTP) + ' <small>vs</small> ' + numberWithCommas(MaAssetOracle),
                            align: 'center',
                            verticalAlign: 'middle',
                            y: 80,
                            useHTML: true,
                        },
                        tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>',
                            followPointer: true,
                        },
                        accessibility: {
        point: {
        valueSuffix: '%'
                            }
                        },
                        credits: {
        href: 'javascript:;',
                            text: '',
                            position: {
        align: 'right',
                                verticalAlign: 'bottom',
                            }
                        },
                        plotOptions: {
        pie: {
        shadow: false,
                                showInLegend: true,
                                dataLabels: {
        enabled: true,
                                    format: '{point.name}: {point.percentage:.2f} %',
                                    style: {
        fontWeight: 'normal'
                                    }
                                },
                                startAngle: -90,
                                endAngle: 90,
                                center: ['50%', '75%'],
                                size: '100%',
                                point: {
        events: {
        legendItemClick: function () {
        this.slice(null);
                                            return false;
                                        }
                                    }
                                }
                            }
                        },
                        responsive: {
        rules: [{
        condition: {
        maxWidth: 500
                                },
                                chartOptions: {
        legend: {
        align: 'center',
                                        verticalAlign: 'bottom',
                                        layout: 'horizontal'
                                    },
                                    yAxis: {
        labels: {
        align: 'left',
                                            x: 0,
                                            y: -5
                                        },
                                        title: {
        text: null
                                        }
                                    },
                                    subtitle: {
        text: null
                                    },
                                    credits: {
        enabled: false
                                    }
                                }
                            }],
                        }
                    });
                    var donutChartMAAssetSeriesData = [];

                    donutChartMAAssetSeriesData.push({className: "highcharts-color-green", name: "eApexIs Asset", y: MaAssetDiOTP });
                    donutChartMAAssetSeriesData.push({className: "highcharts-color-blue", name: "Oracle Asset", y: MaAssetOracle - MaAssetDiOTP });
                    donutChartMAAsset.addSeries({
        type: 'pie',
                        name: 'Percent',
                        innerSize: '50%',
                        data: donutChartMAAssetSeriesData,
                        type: 'pie',
                        useHTML: true
                    });

                }
            });


            var colorStrings = ['blue', 'green'];
            var theMonths = ["Jan", "Feb", "Mar", "Apr", "May",
                "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var YTDCDATA;
            $('#MaChartPlotType').change(function () {
                if ($(this).val() == 'MTD') {
        $.ajax({
            url: "Dashboard.aspx/GetMACountSeries",
            async: false,
            contentType: 'application/json; charset=utf-8',
            type: "GET",
            dataType: "JSON",
            data: { 'PlotType': JSON.stringify($(this).val()) },
            success: function (data) {
                var json = data.d;
                YTDCDATA = json;
                while (machart.series.length > 0) machart.series[0].remove(true);
                machart.xAxis[0].visible = true;
                machart.xAxis[0].setCategories(theMonths);
                var html = "";
                $.each(json, function (idx, y) {
                    var series = [];
                    for (i = 0; i < 12; i++) {
                        var rec = $.grep(y.mACountByMonths, function (obj, index) {
                            return theMonths[i] == obj.Month;
                        });
                        if (rec.length == 1)
                            series.push(parseInt(rec[0].MACOUNT));
                        else
                            series.push(0);
                    }
                    machart.addSeries({
                        className: "highcharts-color-" + colorStrings[idx],
                        data: series,
                        name: y.YEAR,
                        useHTML: true
                    });
                });
            }
        });
                }
                else if ($(this).val() == 'YTD') {
                    var theYears = $.map(YTDCDATA, function (obj, idx) {
                        return obj.YEAR;
                    });
                    while (machart.series.length > 0) machart.series[0].remove(true);
                    machart.xAxis[0].visible = false;
                    var json = YTDCDATA;
                    var html = "";
                    console.log(json);
                    $.each(json, function (idx, y) {
                        var series = [];
                        var yearCount = y.mACountByMonths.map(function (obj) { return parseInt(obj.MACOUNT); }).reduce(function (a, b) {
                            return a + b;
                        });
                        console.log(yearCount);
                        series.push(parseInt(yearCount));
                        machart.addSeries({
        className: "highcharts-color-" + colorStrings[idx],
                            data: series,
                            name: y.YEAR,
                            useHTML: true
                        });
                    });

                }
            });
            $('#MaChartPlotType').change();

            var YTDADATA;
            $('#MaAssetPlotType').change(function () {
                if ($(this).val() == 'MTD') {
        $.ajax({
            url: "Dashboard.aspx/GetMAAssetSeries",
            async: false,
            contentType: 'application/json; charset=utf-8',
            type: "GET",
            dataType: "JSON",
            data: { 'PlotType': JSON.stringify($(this).val()) },
            success: function (data) {
                var json = data.d;
                YTDADATA = json;
                while (aumchart.series.length > 0) aumchart.series[0].remove(true);
                aumchart.xAxis[0].visible = true;
                aumchart.xAxis[0].setCategories(theMonths);
                var html = "";
                $.each(json, function (idx, y) {
                    var series = [];
                    for (i = 0; i < 12; i++) {
                        var rec = $.grep(y.mAAssetByMonths, function (obj, index) {
                            return theMonths[i] == obj.Month;
                        });
                        if (rec.length == 1)
                            series.push(parseInt(rec[0].TRANSAMT));
                        else
                            series.push(0);
                    }
                    aumchart.addSeries({
                        className: "highcharts-color-" + colorStrings[idx],
                        data: series,
                        name: y.YEAR,
                        useHTML: true
                    });
                });
            }
        });
                }
                else if ($(this).val() == 'YTD') {
                    var theYears = $.map(YTDADATA, function (obj, idx) {
                        return obj.YEAR;
                    });
                    while (aumchart.series.length > 0) aumchart.series[0].remove(true);
                    aumchart.xAxis[0].visible = false;
                    var json = YTDADATA;
                    var html = "";
                    $.each(json, function (idx, y) {
                        var series = [];
                        var yearCount = y.mAAssetByMonths.map(function (obj) { return parseInt(obj.TRANSAMT); }).reduce(function (a, b) {
                            return a + b;
                        });
                        series.push(parseInt(yearCount));
                        aumchart.addSeries({
        className: "highcharts-color-" + colorStrings[idx],
                            data: series,
                            name: y.YEAR,
                            useHTML: true
                        });
                    });

                }
            });
            $('#MaAssetPlotType').change();

        });

