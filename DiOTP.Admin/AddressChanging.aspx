﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddressChanging.aspx.cs" Inherits="Admin.AddressChanging" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="AddressChangingForm" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="content">
                        <div class="mb-10">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="search-section">
                                        <button type="button" class="button-common-label action-button" data-action="Search"><i class="fa fa-search"></i></button>
                                        <input type="text" id="Search" name="Search" runat="server" class="form-control" placeholder="Search here" />
                                        <input type="hidden" id="IsNewSearch" name="IsNewSearch" value="0" runat="server" clientidmode="static" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="action-section">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button data-message="Successfully rejected" data-status="Rejected" data-original-title="Reject" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Reject" data-isconfirm="1"><i class="fa fa-ban"></i></button>
                                                <button data-message="Successfully approved" data-status="Approved" data-original-title="Approve" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Activate" data-isconfirm="0"><i class="fa fa-check"></i></button>
                                                <button id="download" data-message="Success" data-status="Active" data-original-title="Download as excel" data-trigger="hover" data-placement="bottom" type="button" class="popovers btn action-button pull-right" data-action="Download" data-isconfirm="0"><i class="fa fa-download"></i></button>
                                                <span class="common-divider pull-right"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong>
                                        <span>You can search here with Username or MA No</span>
                                    </small>
                                </div>
                                <div class="col-md-6">
                                    <small>
                                        <strong>Note:</strong>
                                        <span>You can Download/Approve/Reject on Address Changing</span>
                                    </small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="filter-section mt-10">
                                        <input type="hidden" id="FilterValue" name="FilterValue" runat="server" class="form-control" />
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="All">All</button>
                                        <button type="button" class="btn btn-primary action-button first-load" data-action="Filter" data-filter="0">Pending</button>
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="1">Approved</button>
                                        <button type="button" class="btn btn-primary action-button" data-action="Filter" data-filter="9">Rejected</button>
                                        <small>
                                            <br />
                                            <strong>Note:</strong>
                                            <span>Filter by status</span>
                                        </small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <select id="pageLength" name="pageLength" runat="server" clientidmode="static" class="form-control pull-right page-length-selection">
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="display responsive nowrap table table-bordered dataTable" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th class='icheck'>
                                                <div class='square single-row'>
                                                    <div class='checkbox'>
                                                        <input type='checkbox' name='checkRowAll' class='checkRowAll' value='All' /><br />
                                                    </div>
                                                </div>
                                                S.No
                                            </th>
                                            <th data-column-name="OTP_EXPIRY_DATE">Requested date</th> 
                                            <th data-column-name="OTP_ENT_DT">Appv/Rej date</th>
                                            <th data-column-name="NAME_1">Username</th>
                                            <th data-column-name="HOLDER_NO">MA No</th>
                                            <th data-column-name="ID_NO">ID No</th>
                                            <th data-column-name="ADDR_1">Address line 1</th>
                                            <th data-column-name="ADDR_2">Address line 2</th>
                                            <th data-column-name="ADDR_3">Address line 3</th>
                                            <th data-column-name="ADDR_4">Address line 4</th>
                                            <th data-column-name="">Postcode</th>
                                            <th data-column-name="">State</th>
                                            <th data-column-name="">Country</th>
                                        </tr>
                                    </thead>
                                    <tbody id="usersTbody" runat="server">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnNumberPerPage" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnTotalRecordsCount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCurrentPageNo" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnOrderByColumn" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnOrderType" runat="server" ClientIDMode="Static" Value="0" />
    </form>
    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
</body>
</html>
