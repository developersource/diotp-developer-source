﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class BannerListing : System.Web.UI.Page
    {
        private static readonly Lazy<ISiteContentService> lazyUserServiceObj = new Lazy<ISiteContentService>(() => new SiteContentService());

        public static ISiteContentService ISiteContentService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder filter = new StringBuilder();
            string mainQ = (@"SELECT * FROM");
            string mainQCount = (@"select count(*) from ");
            filter.Append(@" site_content
                             where 1=1 ");
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isAccountAdmin"] != null && Session["isAccountAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }


                string siteContentTypeId = Converter.GetColumnNameByPropertyName<SiteContent>(nameof(SiteContent.SiteContentTypeId));
                filter.Append("and " + siteContentTypeId + "='2'");

                if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    string columnNameTitle = Converter.GetColumnNameByPropertyName<SiteContent>(nameof(SiteContent.Title));
                    filter.Append(" and " + columnNameTitle + " like '%" + Search.Value.Trim() + "%'");
                }



                filter.Append("order by status desc, priority");


                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = ISiteContentService.GetCountByFilter(filter.ToString()).ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseSCList = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, false);


                if (responseSCList.IsSuccess)
                {
                    var BannerDyn = responseSCList.Data;
                    var responseJSON = JsonConvert.SerializeObject(BannerDyn);
                    List<SiteContent> sitecontent = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SiteContent>>(responseJSON);
                     

                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    foreach (SiteContent s in sitecontent)
                    {
                        asb.Append(@"<tr 
                    data-Id='" + s.Id + @"' 
                    data-Title='" + s.Title + @"' 
                    data-TitleColor='" + s.TitleColor + @"' 
                    data-SubTitle='" + s.SubTitle + @"'
                    data-PublishDate='" + s.PublishDate + @"'
                    data-ShortDisplayContent='" + s.ShortDisplayContent + @"'
                    data-Content='" + s.Content + @"'
                    data-ContentColor='" + s.ContentColor + @"'
                    data-ImageUrl='" + s.ImageUrl + @"'
                    data-Priority='" + s.Priority + @"'
                    data-Status='" + s.Status + @"'
                    >
                <td class='icheck'>
                    <div class='square single-row'>
                         <div class='checkbox'>
                            <input type='checkbox' name='checkRow' class='checkRow' value='" + s.Id + @"' /> <label>" + index + @"</label><br/>
                         </div>
                      </div>
                     <span class='row-status'>" + (s.Status == 1 ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Inactive</span>") + @"</span>
                 </td>
                <td>" + s.Title + @"</td>
                <td>" + s.TitleColor + @"</td>
                <td>" + s.SubTitle + @"</td>
                <td>" + s.PublishDate + @"</td>
                <td class='td-limit'>" + s.ShortDisplayContent + @"</td>
                <td class='td-limit'>" + s.Content + @"</td>
                <td>" + s.ContentColor + @"</td>
                <td><img src='" + s.ImageUrl + @"' alt='" + s.Title + @"' class='img-responsive' /></td>
                <td>" + s.Priority + @"</td>
            </tr>
            ");
                        index++;

                    }
                    bannerTbody.InnerHtml = asb.ToString();
                }
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response responseMsg = new Response();
            try
            {
                string idString = String.Join(",", ids);
                Response responseSCList = ISiteContentService.GetDataByFilter(" ID in (" + idString + ")", 0, 0, false);
                if (responseSCList.IsSuccess)
                {
                    List<SiteContent> siteContents = (List<SiteContent>)responseSCList.Data;
                    foreach (SiteContent siteContent in siteContents)
                    {
                        Response response = ISiteContentService.GetDataByFilter("site_content_type_id = 2 and priority=" + siteContent.Priority, 0, 10, false);
                        if (response.IsSuccess)
                        {
                            List<SiteContent> sc = ((List<SiteContent>)response.Data).Where(z => z.Status == 0 && z.Id == siteContent.Id).ToList();
                            List<SiteContent> sc1 = ((List<SiteContent>)response.Data).Where(z => z.Status == 1).ToList();
                            if (action == "Activate")
                            {
                                sc.ForEach(x =>
                                {
                                    x.Status = 1;
                                    //Audit Log starts here
                                    AdminLogMain alm = new AdminLogMain()
                                    {
                                        Description = "Banner with " + x.Title + " has been activated",
                                        TableName = "site_content",
                                        UpdatedDate = DateTime.Now,
                                        UserId = loginUser.Id,
                                    };
                                    Response responseLog = IAdminLogMainService.PostData(alm);
                                    if (!responseLog.IsSuccess)
                                    {
                                        //Audit log failed
                                    }
                                    else
                                    {

                                    }
                                    alm = (AdminLogMain)responseLog.Data;
                                    AdminLogSub als = new AdminLogSub()
                                    {
                                        AdminLogMainId = alm.Id,
                                        ColumnName = "status",
                                        ValueOld = "0",
                                        ValueNew = "1",
                                    };
                                    Response response2 = IAdminLogSubService.PostData(als);
                                    //Audit Log Ends here
                                });
                                sc1.ForEach(x =>
                                {
                                    x.Status = 0;
                                    //Audit Log starts here
                                    AdminLogMain alm = new AdminLogMain()
                                    {
                                        Description = "Banner with " + x.Title + " has been deactivated",
                                        TableName = "site_content",
                                        UpdatedDate = DateTime.Now,
                                        UserId = loginUser.Id,
                                    };
                                    Response responseLog = IAdminLogMainService.PostData(alm);
                                    if (!responseLog.IsSuccess)
                                    {
                                        //Audit log failed
                                    }
                                    else
                                    {

                                    }
                                    alm = (AdminLogMain)responseLog.Data;
                                    AdminLogSub als = new AdminLogSub()
                                    {
                                        AdminLogMainId = alm.Id,
                                        ColumnName = "status",
                                        ValueOld = "1",
                                        ValueNew = "0",
                                    };
                                    Response response2 = IAdminLogSubService.PostData(als);
                                    //Audit Log Ends here
                                });
                                responseMsg.IsSuccess = true;
                                responseMsg.Message = "Success<br/>NOTE: Only allow one banner with one priority - Banner with " + String.Join(", ", sc1.Select(x => x.Title).ToArray()) + " has been deactivated";
                                ISiteContentService.UpdateBulkData(sc);
                                ISiteContentService.UpdateBulkData(sc1);
                            }
                        }
                    }
                    if (action == "Deactivate")
                    {
                        siteContents.ForEach(x =>
                        {
                            x.Status = 0;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = "Banner with " + x.Title + " has been deactivated",
                                TableName = "site_content",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "status",
                                ValueOld = "1",
                                ValueNew = "0",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                            //Audit Log Ends here
                        });
                        responseMsg.IsSuccess = true;
                        responseMsg.Message = "Success";
                        ISiteContentService.UpdateBulkData(siteContents);
                    }
                }
                return responseMsg;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Site contents action: " + ex.Message);
                responseMsg.IsSuccess = false;
                responseMsg.Message = ex.Message;
                return responseMsg;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(SiteContent obj)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            if (obj.Title == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter the Title";
                return response;
            }
            if (obj.SubTitle == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter the Short Title";
                return response;
            }
            if (obj.ShortDisplayContent == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter the Short Display Content";
                return response;
            }
            if (obj.Content == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter the Content";
                return response;
            }
            if (obj.Priority == 0)
            {
                response.IsSuccess = false;
                response.Message = "Select the Priority No";
                return response;
            }
            if (obj.ImageUrl == "")
            {
                response.IsSuccess = false;
                response.Message = "Select the Image";
                return response;
            }
            if (obj.PublishDate.ToString() == "1/1/0001 12:00:00 AM")
            {
                response.IsSuccess = false;
                response.Message = "Choose the Publish Date";
                return response;
            }

            try
            {
                if (obj.Id == 0)
                {
                    Response responseSCList = ISiteContentService.GetDataByPropertyName(nameof(SiteContent.Priority), obj.Priority.ToString(), true, 0, 0, false);
                    if (responseSCList.IsSuccess)
                    {
                        List<SiteContent> siteContents = (List<SiteContent>)responseSCList.Data;
                        if (siteContents != null)
                        {
                            foreach (SiteContent sc in siteContents)
                            {
                                sc.Status = 0;
                                ISiteContentService.UpdateData(sc);
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Banner has successfully added",
                                    TableName = "site_content",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }

                                //Audit Log Ends here
                            }

                        }
                        obj.Status = 1;
                        ISiteContentService.PostData(obj);
                    }
                    response.IsSuccess = true;
                    response.Message = "Success";

                }
                else
                {
                    Response responseSCList = ISiteContentService.GetDataByPropertyName(nameof(SiteContent.Priority), obj.Priority.ToString(), true, 0, 0, false);
                    if (responseSCList.IsSuccess)
                    {
                        List<SiteContent> siteContents = (List<SiteContent>)responseSCList.Data;
                        if (siteContents != null)
                        {
                            foreach (SiteContent sc in siteContents)
                            {
                                sc.Status = 0;
                                ISiteContentService.UpdateData(sc);
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Banner" + sc.Id + " has successfully updated",
                                    TableName = "site_content",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }

                                //Audit Log Ends here
                            }
                        }
                        obj.Status = 1;
                        ISiteContentService.UpdateData(obj);
                    }
                    response.IsSuccess = true;
                    response.Message = "Success";
                }
                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Add language dir action: " + ex.Message);
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }
    }
}