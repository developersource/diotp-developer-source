﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserObj.Value; } }
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else
            {
                User sessionUser = (User)Session["admin"];
                Response response = IUserService.GetSingle(sessionUser.Id);
                if (response.IsSuccess)
                {
                    User user = (User)response.Data;
                    Username.Value = user.Username;
                    CreatedBy.Value = user.CreatedBy.ToString();
                    ModifiedBy.Value = user.ModifiedBy.ToString();
                    CreatedDate.Value = user.CreatedDate.ToString("MM/dd/yyyy");
                    ModifiedDate.Value = user.ModifiedDate == null ? default(DateTime).ToString("MM/dd/yyyy") : user.ModifiedDate.Value.ToString("MM/dd/yyyy");
                }
            }
        }

        protected void btnSetPassword_Click(object sender, EventArgs e)
        {
            try
            {
                User sessionUser = (User)Session["admin"];
                Response response = IUserService.GetSingle(sessionUser.Id);
                if (response.IsSuccess)
                {
                    User user = (User)response.Data;
                    user = ((List<User>)response.Data).FirstOrDefault();
                    if (user != null)
                    {
                            user.IsActive = 1;
                            user.Status = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Setpassword Page btnSetPassword_Click: " + ex.Message);
            }
        }
    }
}