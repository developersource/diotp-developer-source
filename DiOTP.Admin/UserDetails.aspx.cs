﻿using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static Admin.ServiceCalls.ServicesManager;

namespace Admin
{
    public partial class UserDetails : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IOccupationCodesDefService> lazyOccupationCodesDefServiceObj = new Lazy<IOccupationCodesDefService>(() => new OccupationCodesDefService());

        public static IOccupationCodesDefService IOccupationCodesDefService { get { return lazyOccupationCodesDefServiceObj.Value; } }

        private static readonly Lazy<IRaceDefService> lazyIRaceDefServiceObj = new Lazy<IRaceDefService>(() => new RaceDefService());

        public static IRaceDefService IRaceDefService { get { return lazyIRaceDefServiceObj.Value; } }

        private static readonly Lazy<INationalityDefService> lazyINationalityDefServiceObj = new Lazy<INationalityDefService>(() => new NationalityDefService());

        public static INationalityDefService INationalityDefService { get { return lazyINationalityDefServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            else
            {
                String idString = Request.QueryString["id"];

                if (!string.IsNullOrEmpty(idString))
                {
                    int id = Convert.ToInt32(idString);

                    Response response = IUserService.GetSingle(id);
                    if (response.IsSuccess)
                    {
                        User user = (User)response.Data;
                        StringBuilder asb = new StringBuilder();
                        asb.Append(@"<tr>
                                <td>" + user.Username + @"</td>
                                <td>" + user.EmailId + @"</td>
                                <td>" + user.MobileNumber + @"</td>
                                <td>" + user.Status + @"</td>
                                <td>" + user.LastLoginIp + @"</td>
                                <td>" + user.RegisterIp + @"</td>
                            </tr>");
                        userTbody.InnerHtml = asb.ToString();

                        string siteURL = ConfigurationManager.AppSettings["siteURL"];
                        string impersonateURL = ConfigurationManager.AppSettings["impersonateURL"];
                        impersonateUserLink.Attributes.Remove("href");
                        impersonateUserLink.Attributes.Add("href", impersonateURL + "ImpersonateUser.aspx?id=" + CustomEncryptorDecryptor.EncryptText(user.Id.ToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff")));

                        noOfMAAccounts.InnerHtml = user.UserIdUserAccounts.Count.ToString();
                        
                        asOnDate.InnerHtml = DateTime.Now.ToString("dd/MM/yyyy");

                        UserSecurity usEmail = user.UserIdUserSecurities.Where(x => x.UserSecurityTypeId == 1).FirstOrDefault();
                        emailSecurity.InnerHtml = "";
                        if (usEmail != null)
                        {
                            if (usEmail.IsVerified == 1)
                            {
                                emailSecurity.InnerHtml = "Binded";
                            }
                            else
                            {
                                
                            }
                        }
                        else
                        {
                            emailSecurity.InnerHtml = "Not binded";
                        }

                        UserSecurity usMobile = user.UserIdUserSecurities.Where(x => x.UserSecurityTypeId == 2).FirstOrDefault();
                        mobileSecurity.InnerHtml = "";
                        if (usMobile != null)
                        {
                            if (usMobile.IsVerified == 1)
                            {
                                mobileSecurity.InnerHtml = "Binded";
                            }
                            else
                            {
                                mobileSecurity.InnerHtml = "Not binded";
                            }
                        }
                        else
                        {
                            mobileSecurity.InnerHtml = "Not binded";
                        }
                        UserSecurity usGoogle = user.UserIdUserSecurities.Where(x => x.UserSecurityTypeId == 3).FirstOrDefault();
                        googleSecurity.InnerHtml = "";
                        if (usGoogle != null)
                        {
                            if (usGoogle.IsVerified == 1)
                            {
                                googleSecurity.InnerHtml = "Binded";
                            }
                            else
                            {
                                googleSecurity.InnerHtml = "Not binded";
                            }
                        }
                        else
                        {
                            googleSecurity.InnerHtml = "Not binded";
                        }

                        asb = new StringBuilder();

                        List<UserAccount> userAccounts = user.UserIdUserAccounts;
                        foreach (UserAccount uA in userAccounts)
                        {
                            Response responseMA = ServicesManager.GetMaHolderRegByAccountNo(uA.AccountNo);
                            if (responseMA.IsSuccess)
                            {
                                MaHolderReg maHolderReg = (MaHolderReg)responseMA.Data;
                                if (maHolderReg != null)
                                {
                                    string accPlan = CustomValues.GetAccounPlan(maHolderReg.HolderCls);
                                    String name = maHolderReg.Name1;
                                    name += (maHolderReg.Name2 != null && maHolderReg.Name2.Trim() != "" ? " " + maHolderReg.Name2 : "");
                                    name += (maHolderReg.Name3 != null && maHolderReg.Name3.Trim() != "" ? " " + maHolderReg.Name3 : "");
                                    name += (maHolderReg.Name4 != null && maHolderReg.Name4.Trim() != "" ? " " + maHolderReg.Name4 : "");
                                    name += (maHolderReg.Name5 != null && maHolderReg.Name5.Trim() != "" ? " " + maHolderReg.Name5 : "");

                                    if (accPlan == "JOINT")
                                        name = maHolderReg.Name2;

                                    String gender = maHolderReg.Sex == "M" ? "Male" : maHolderReg.Sex == "F" ? "Female" : "Others";

                                    string bdd = DateTime.Now.ToString("MM/dd/yyyy");
                                    DateTime BDT = DateTime.ParseExact(maHolderReg.BirthDt, "M/d/yyyy", CultureInfo.InvariantCulture); 
                                    String birthDate = BDT.ToString("dd/MM/yyyy");

                                    String teleNo = maHolderReg.TelNo;
                                    String handPhone = maHolderReg.HandPhoneNo;

                                    if (accPlan == "JOINT")
                                        handPhone = maHolderReg.JointTelNo;

                                    String occupation = maHolderReg.OccCode;

                                    Response responseOCDList = IOccupationCodesDefService.GetDataByPropertyName(nameof(OccupationCodesDef.Code), maHolderReg.OccCode, true, 0, 0, true);
                                    if (responseOCDList.IsSuccess)
                                    {
                                        OccupationCodesDef occupationCodes = ((List<OccupationCodesDef>)responseOCDList.Data).FirstOrDefault();
                                        if (occupationCodes != null)
                                        {
                                            occupation = occupationCodes.Name;
                                            occupation = occupation.Replace("L-", "").Replace("H-", "");
                                        }
                                    }
                                    String nationality = maHolderReg.Nationality;

                                    Response responseNDList = INationalityDefService.GetDataByPropertyName(nameof(NationalityDef.Code), maHolderReg.Nationality, true, 0, 0, true);
                                    if (responseNDList.IsSuccess)
                                    {
                                        NationalityDef nationalityCode = ((List<NationalityDef>)responseNDList.Data).FirstOrDefault();
                                        if (nationality != null)
                                            nationality = nationalityCode.Name;
                                    }

                                    String noOfDependents = maHolderReg.NoOfDpndnt.ToString();

                                    String rAddress = maHolderReg.Addr1;
                                    rAddress += (maHolderReg.Addr2 != null && maHolderReg.Addr2.Trim() != "" ? ", " + maHolderReg.Addr2 : "");
                                    rAddress += (maHolderReg.Addr3 != null && maHolderReg.Addr3.Trim() != "" ? ", " + maHolderReg.Addr3 : "");
                                    rAddress += (maHolderReg.Addr4 != null && maHolderReg.Addr4.Trim() != "" ? ", " + maHolderReg.Addr4 : "");

                                    String pAddress = maHolderReg.PrmntAddr1;
                                    pAddress += (maHolderReg.PrmntAddr2 != null && maHolderReg.PrmntAddr2.Trim() != "" ? ", " + maHolderReg.PrmntAddr2 : "");
                                    pAddress += (maHolderReg.PrmntAddr3 != null && maHolderReg.PrmntAddr3.Trim() != "" ? ", " + maHolderReg.PrmntAddr3 : "");
                                    pAddress += (maHolderReg.PrmntAddr4 != null && maHolderReg.PrmntAddr4.Trim() != "" ? ", " + maHolderReg.PrmntAddr4 : "");


                                    asb.Append(@"<tr>
                                    <td>" + uA.AccountNo + @"</td>
                                    <td>" + accPlan + @"</td>
                                    <td>" + uA.SatScore + @"</td>
                                    <td>" + (uA.SatUpdatedDate != null ? uA.SatUpdatedDate.Value.ToString("dd/MM/yyyy") : "-") + @"</td>
                                    <td>" + name + @"</td>
                                    <td>" + gender + @"</td>
                                    <td>" + birthDate + @"</td>
                                    <td>" + teleNo + @"</td>
                                    <td>" + handPhone + @"</td>
                                    <td>" + occupation + @"</td>
                                    <td>" + nationality + @"</td>
                                    <td>" + noOfDependents + @"</td>
                                    <td>" + rAddress + @"</td>
                                    <td>" + pAddress + @"</td>
                                </tr>");
                                }
                            }
                        }
                        
                        userAccountsTbody.InnerHtml = asb.ToString();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                            "alert('" + response.Message + "');", true);
                    }
                }
            }

        }
    }
}