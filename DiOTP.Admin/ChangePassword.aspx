﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="Admin.ChangePassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div id="changePasswordForm" runat="server">
        <div class="row" id="FormId" data-value="changePasswordForm">
            <div class="col-md-12">
                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Username:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
                        <input type="hidden" name="Status" value="1" id="Status" runat="server" clientidmode="static" />
                        <input type="hidden" name="CreatedBy" value="0" id="CreatedBy" runat="server" clientidmode="static" />
                        <input type="hidden" name="CreatedDate" value="0" id="CreatedDate" runat="server" clientidmode="static" />
                        <input type="hidden" name="ModifiedBy" value="0" id="ModifiedBy" runat="server" clientidmode="static" />
                        <input type="hidden" name="ModifiedDate" value="0" id="ModifiedDate" runat="server" clientidmode="static" />
                        <input type="hidden" name="UserRoleId" value="2" id="UserRoleId" runat="server" clientidmode="static" />

                        <input type="hidden" name="UniqueKey" id="UniqueKey" runat="server" clientidmode="static" class="form-control" placeholder="Enter Name" />
                        <input type="hidden" name="IsOnline" value="0" id="IsOnline" runat="server" clientidmode="static" />
                        <input type="hidden" name="IsActive" value="0" id="IsActive" runat="server" clientidmode="static" />
                        <input type="hidden" name="IsPrimary" value="0" id="IsPrimary" runat="server" clientidmode="static" />
                        <input type="hidden" name="LastLoginDate" value="" id="LastLoginOn" runat="server" clientidmode="static" />
                        <input type="hidden" name="IsSatChecked" value="0" id="IsSatChecked" runat="server" clientidmode="static" />
                        <input type="hidden" name="VerificationCode" value="0" id="VerificationCode" runat="server" clientidmode="static" />
                        <input type="hidden" name="LastLoginIp" id="LastLoginIp" runat="server" clientidmode="static" class="form-control" placeholder="Enter MA No" />
                        <input type="hidden" name="EmailId" id="EmailId" runat="server" clientidmode="static" class="form-control" placeholder="Enter Email" />
                        <input type="hidden" name="RegisterIp" id="RegisterIp" runat="server" clientidmode="static" class="form-control" placeholder="Enter Company ID" />

                        <input type="text" name="Username" id="Username" runat="server" clientidmode="static" class="form-control" placeholder="Enter Username" readonly="readonly" />
                    </div>
                </div>

                 <div class="row mb-6" id="MADiv" runat="server">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Old password:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="password" name="Password" id="Password" runat="server" clientidmode="static" class="form-control" placeholder="Enter Password" />
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">New Password:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="password" name="TransPwd" id="TransPwd" runat="server" clientidmode="static" class="form-control" placeholder="Enter New Password" />
                    </div>
                </div>

                <div class="row mb-6">
                    <div class="col-md-3">
                        <label class="fs-14 mt-2">Confirm Password:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="password" name="MobileNumber" id="MobileNumber" runat="server" clientidmode="static" class="form-control" placeholder="Renter New Password" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
