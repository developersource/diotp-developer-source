﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddBanner.aspx.cs" Inherits="Admin.AddBanner" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="addBannerForm" runat="server">
        <div id="FormId" data-value="addBannerForm">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Select Image:</label>
                        </div>  
                        <div class="col-md-8">
                            <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
                            <input type="hidden" name="SiteContentTypeId" value="2" id="SiteContentTypeId" runat="server" clientidmode="static" />
                            <input type="hidden" name="IsDisplay" value="1" id="IsDisplay" runat="server" clientidmode="static" />
                            <input type="hidden" name="IsContentDisplay" value="1" id="IsContentDisplay" runat="server" clientidmode="static" />
                            <input type="hidden" name="Status" value="1" id="Status" runat="server" clientidmode="static" />
                            <input type="hidden" name="CreatedBy" value="0" id="CreatedBy" runat="server" clientidmode="static" />
                            <input type="hidden" name="CreatedDate" value="0" id="CreatedDate" runat="server" clientidmode="static" />
                            <input type="hidden" name="UpdatedBy" value="0" id="UpdatedBy" runat="server" clientidmode="static" />
                            <input type="hidden" name="UpdatedDate" value="0" id="UpdatedDate" runat="server" clientidmode="static" />
                            <input type="hidden" name="ImageUrl" id="ImageUrl" runat="server" clientidmode="static" />
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div id="divImg" class="fileupload-new thumbnail" style="width: 200px; height: 150px;" runat="server"  clientidmode="Static">
                                    <img id="bannerImage" runat="server" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                </div>
                                <%--<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>--%>
                                <div>
                                    <span class="btn btn-white btn-file">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Select image</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i>Change</span>
                                        <asp:FileUpload ID="fuBannerImage" runat="server" ClientIDMode="Static" CssClass="default filePicker" />
                                    </span>
                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i>Remove</a>
                                </div>
                                <div id="uploadStatus">0%</div>
                            </div>
                            <span class="label label-danger">NOTE!</span>
                            <span>Attached image thumbnail is
                                    supported in Latest Firefox, Chrome, Opera,
                                    Safari and Internet Explorer 10 only
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Title:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="Title" id="Title" runat="server" clientidmode="static" class="form-control" placeholder="Enter Title" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Title Color:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="TitleColor" id="TitleColor" runat="server" clientidmode="static" class="form-control default-color-picker" placeholder="Select Color" data-format="hex" value="#5a3d3d" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Short Title:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="SubTitle" id="SubTitle" runat="server" clientidmode="static" class="form-control" placeholder="Enter Short Title" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Date of Publish:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="PublishDate" id="PublishDate" runat="server" clientidmode="static" class="form-control default-date-picker" placeholder="Select Date" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Short Display Content:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="ShortDisplayContent" id="ShortDisplayContent" runat="server" clientidmode="static" class="form-control" placeholder="Enter Short Display Content" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Select Priority:</label>
                        </div>
                        <div class="col-md-8">
                            <select name="Priority" id="Priority" runat="server" clientidmode="static" class="form-control">
                                <option value="0">Select</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Content Color:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="ContentColor" id="ContentColor" runat="server" clientidmode="static" class="form-control default-color-picker" placeholder="Select Color" data-format="hex" value="#5a3d3d" />
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row mb-6">
                        <div class="col-md-2">
                            <label class="fs-14 mt-2">Content:</label>
                        </div>
                        <div class="col-md-10">
                            <textarea placeholder="Enter Content" rows="5" class="form-control" name="Content" id="Content" runat="server"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $ui(document).ready(function () {

                $ui('.default-date-picker').datepicker({
                    format: 'mm/dd/yyyy',
                    pickTime: false,
                    autoclose: true
                });

                function progressHandlingFunction(e) {
                    if (e.lengthComputable) {
                        var percentage = Math.floor((e.loaded / e.total) * 100);
                        //update progressbar percent complete
                        $ui('#uploadStatus').html(percentage + '%');
                        console.log("Value = " + e.loaded + " :: Max =" + e.total);
                    }
                }

                function sendFile(file) {
                    var formData = new FormData();
                    formData.append('file', file);
                    $ui.ajax({
                        type: 'post',
                        url: 'fileUploader.ashx',
                        data: formData,
                        xhr: function () {  // Custom XMLHttpRequest
                            var myXhr = $ui.ajaxSettings.xhr();
                            if (myXhr.upload) { // Check if upload property exists
                                //update progressbar percent complete
                                $ui('#uploadStatus').html('0%');
                                // For handling the progress of the upload
                                myXhr.upload.addEventListener('progress', progressHandlingFunction, false);

                            }
                            return myXhr;
                        },
                        success: function (status) {
                            console.log(status);
                            if (status != 'error') {
                                $ui("#bannerImage").attr("src", status);
                                $ui('#ImageUrl').val(status);
                                
                            }
                        },
                        processData: false,
                        contentType: false,
                        error: function () {
                            alert("Whoops something went wrong!");
                        }
                    });
                }

                var _URL = window.URL || window.webkitURL;
                $ui('.filePicker').unbind('change');
                $ui('.filePicker').change(function () {
                    var file, img;
                    if ((file = this.files[0])) {
                        img = new Image();
                        img.onload = function () {
                            sendFile(file);
                        };
                        img.onerror = function () {
                            alert("Not a valid file:" + file.type);
                        };
                        img.src = _URL.createObjectURL(file);
                    }
                });

                $ui('.default-color-picker').colorpicker();

            });
        </script>
    </form>
</body>
</html>
