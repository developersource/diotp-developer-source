﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class ViewDetailRSP : System.Web.UI.Page
    {
        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserOrderFileService> lazyUserOrderFileObj = new Lazy<IUserOrderFileService>(() => new UserOrderFileService());

        public static IUserOrderFileService IUserOrderFileService { get { return lazyUserOrderFileObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            else
            {
                String orderNo = Request.QueryString["id"];
                int index = 0;
                Response responseUOList = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), orderNo, true, 0, 0, true);
                if (responseUOList.IsSuccess)
                {
                    List<UserOrder> x = (List<UserOrder>)responseUOList.Data;
                    StringBuilder asb = new StringBuilder();
                    if (x.Count() != 0)
                    {
                        foreach (UserOrder i in x)
                        {
                            Response response = IUtmcFundInformationService.GetSingle(i.FundId);
                            if (response.IsSuccess)
                            {
                                UtmcFundInformation fundDetail = (UtmcFundInformation)response.Data;
                                index++;
                                asb.Append(@"<tr>
                                <td>" + index + @"</td>
                                <td>" + fundDetail.FundName.Capitalize() + @"</td>
                                <td>" + i.Amount.ToString("N2") + @"</td>
                                <th>" + i.RejectReason + @"</th>
                            </tr>");
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                    "alert('" + response.Message + "');", true);
                            }
                        }
                        userTbody.InnerHtml = asb.ToString();
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                            "alert('" + responseUOList.Message + "');", true);
                }
            }
        }
    }
}