﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class ViewTransactionDetail : System.Web.UI.Page
    {
        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserOrderFileService> lazyUserOrderFileObj = new Lazy<IUserOrderFileService>(() => new UserOrderFileService());

        public static IUserOrderFileService IUserOrderFileService { get { return lazyUserOrderFileObj.Value; } }


        private static readonly Lazy<IMaHolderBankService> lazyMaHolderBankObj = new Lazy<IMaHolderBankService>(() => new MaHolderBankService());
        public static IMaHolderBankService IMaHolderBankService { get { return lazyMaHolderBankObj.Value; } }

        private static readonly Lazy<IBanksDefService> lazyBanksDefObj = new Lazy<IBanksDefService>(() => new BanksDefService());
        public static IBanksDefService IBanksDefService { get { return lazyBanksDefObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            else
            {
                String orderNo = Request.QueryString["id"];
                int index = 0;

                Response responseMO = GenericService.GetDataByQuery(@"SELECT uo.ID, uo.user_id, uo.user_account_id, ua.account_no, uo.order_no, uo.order_type, uo.created_date, uo.amount, uo.units, 
                            ufi.Fund_Name, ufi2.Fund_Name as Fund_Name2, uo.payment_method, uo.payment_date, uo.settlement_date, ufc.INITIAL_SALES_CHARGES_PERCENT, ufc2.INITIAL_SALES_CHARGES_PERCENT, 
                            uo.order_status, uo.fpx_bank_code, uo.reject_reason as trans_no, uo.reject_reason, ufd.LATEST_NAV_PRICE as LATEST_NAV_PRICE1, ufd2.LATEST_NAV_PRICE as LATEST_NAV_PRICE2, 
                            uo.distribution_instruction, uo.bank_id, uo.fpx_bank_code, uo.consultantID as consultantid, uo.trans_amt, uo.trans_units
                            FROM user_orders uo
                            left join user_accounts ua on ua.ID = uo.user_account_id
                            left join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            left join utmc_fund_charges ufc on ufc.utmc_fund_information_id = uo.fund_id
                            left join utmc_fund_charges ufc2 on ufc2.utmc_fund_information_id = uo.to_fund_id 
                            left join utmc_fund_details ufd on ufd.utmc_fund_information_id = ufi.ID
                            left join utmc_fund_details ufd2 on ufd2.utmc_fund_information_id = ufi2.ID
                            where uo.order_no='" + orderNo + @"'  order by uo.ID ", 0, 0, false, null, false, null, true);
                if (responseMO.IsSuccess)
                {
                    var rawDynMOData = responseMO.Data;
                    var responseMOJSON = JsonConvert.SerializeObject(rawDynMOData);
                    List<OrderListDTO> UserOrderMOList = JsonConvert.DeserializeObject<List<OrderListDTO>>(responseMOJSON);
                    string totalAmt = UserOrderMOList.Sum(y => y.amount).ToString("N2", new CultureInfo("en-US"));
                    string totalUnits = UserOrderMOList.Sum(y => y.units).ToString("N4", new CultureInfo("en-US"));
                    decimal totalAmtEst = 0;
                    decimal totalUnitsEst = 0;

                    StringBuilder asb = new StringBuilder();
                    StringBuilder tblHeader = new StringBuilder();

                    if (UserOrderMOList.FirstOrDefault().order_type == 1)
                    {
                        tblHeader.Append(@"<tr>
                                                <th>No.</th>
                                                <th>Fund Name</th>
                                                <th>Distribution Instruction</th>
                                                " + (UserOrderMOList.FirstOrDefault().order_status == 3 ? "<th class='text-right'>Unit Price (MYR)</th>" : "") + @"
                                                <th class='text-right'>Units</th>
                                                <th class='text-right'>Amout(MYR)</th>
                                                <th>Ref No / Remarks</th>
                                                <th>Agent Code</th>
                                               </tr>");

                        UserOrderMOList.ForEach(y =>
                        {
                            string transno = y.trans_no == "" ? "-" : y.trans_no;
                            OrderListDTO uo = y;
                            int i = 0;
                            decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);
                            totalAmtEst += UnitsValueMYR;
                            decimal UnitsValue = (y.amount / y.LATEST_NAV_PRICE1);
                            totalUnitsEst += UnitsValue;

                            index++;
                            MaHolderBank maHolderBank = null;
                            BanksDef banksDef = null;
                            if (y.distribution_instruction == 1)
                            {
                                if (y.bank_id != 0 && y.fpx_bank_code != null)
                                {
                                    Response responseBankSubmit = IMaHolderBankService.GetDataByFilter("ID = '" + y.bank_id + "' ", 0, 0, false);
                                    if (responseBankSubmit.IsSuccess)
                                    {
                                        List<MaHolderBank> maholderbanks = (List<MaHolderBank>)responseBankSubmit.Data;
                                        maHolderBank = maholderbanks.FirstOrDefault();
                                        Response responseBankDef = IBanksDefService.GetDataByFilter("ID = '" + maHolderBank.BankDefId + "' ", 0, 0, false);
                                        if (responseBankDef.IsSuccess)
                                        {
                                            List<BanksDef> banks = (List<BanksDef>)responseBankDef.Data;
                                            banksDef = banks.FirstOrDefault();
                                        }
                                    }
                                }
                            }

                            asb.Append(@"<tr>
                                    <td>" + index + @"</td>
                                    <td>" + y.Fund_Name.Capitalize() + @"</td>
                                    <td> " + (y.distribution_instruction == 3 ? "Reinvest" : (y.distribution_instruction == 1 ? "Payout " + (maHolderBank != null ? "(Credit Bank: "+ banksDef.Name + " - " + maHolderBank.BankAccountNo + @")" : "") + "" : "-")) + @"</td>
                                    " + (y.order_status == 3 ? "<td class='text-right'>" + (y.order_status == 3 ? y.LATEST_NAV_PRICE1 + "" : "-") + @"</td>" : "") + @"
                                    <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4") : "(Est.) " + totalUnitsEst.ToString("N4")) + @"</td>
                                    <td class='text-right'>" + (y.order_status == 3 && y.trans_amt.HasValue ? y.trans_amt.Value.ToString("N2") : y.amount.ToString("N2")) + @"</td>
                                    <td>" + y.reject_reason + @"</td>
                                    <td>" + y.consultantid + @"</td>
                                </tr>");

                        });

                        viewTransHead.InnerHtml = tblHeader.ToString();
                        userTbody.InnerHtml = asb.ToString();

                    }
                    
                    else if (UserOrderMOList.FirstOrDefault().order_type == 2)
                    {
                        tblHeader.Append(@"<tr>
                                                <th>No.</th>
                                                <th>Fund Name</th>
                                                " + (UserOrderMOList.FirstOrDefault().fpx_bank_code != "" && UserOrderMOList.FirstOrDefault().fpx_bank_code != null ? "<th>Credit Bank (Account No)</th>" : "") + @"
                                                " + (UserOrderMOList.FirstOrDefault().order_status == 3 ? "<th class='text-right'>Unit Price (MYR)</th>" : "") + @"
                                                <th class='text-right'>Units</th>
                                                <th class='text-right'>Amount(MYR)</th>
                                                <th>Ref No / Remarks</th>
                                               </tr>");
                        UserOrderMOList.ForEach(y =>
                        {
                            string transno = y.trans_no == "" ? "-" : y.trans_no;
                            OrderListDTO uo = y;
                            decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);
                            totalAmtEst += UnitsValueMYR;
                            decimal UnitsValue = (y.amount * y.LATEST_NAV_PRICE1);
                            totalUnitsEst += UnitsValue;
                            index++;

                            BanksDef banksDef = null;
                            MaHolderBank maHolderBank = null;
                            if (y.fpx_bank_code != "" && y.fpx_bank_code != null)
                            {
                                Response responseBankSubmit = IMaHolderBankService.GetDataByFilter("ID = '" + y.fpx_bank_code + "' ", 0, 0, false);
                                if (responseBankSubmit.IsSuccess)
                                {
                                    List<MaHolderBank> maholderbanks = (List<MaHolderBank>)responseBankSubmit.Data;
                                    maHolderBank = maholderbanks.FirstOrDefault();
                                    Response responseBankDef = IBanksDefService.GetDataByFilter("ID = '" + maHolderBank.BankDefId + "' ", 0, 0, false);
                                    if (responseBankDef.IsSuccess)
                                    {
                                        List<BanksDef> banks = (List<BanksDef>)responseBankDef.Data;
                                        banksDef = banks.FirstOrDefault();
                                    }
                                }
                            }
                            asb.Append(@"<tr>
                                            <td>" + index + @"</td>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            " + (maHolderBank != null ? "<td>" + banksDef.Name + " (" + maHolderBank.BankAccountNo + ")</td>" : "") + @"
                                            " + (y.order_status == 3 ? "<td class='text-right'>" + (y.order_status == 3 ? y.LATEST_NAV_PRICE1 + "" : "-") + @"</td>" : "") + @"
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4") : y.units.ToString("N4")) + @" </td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_amt.HasValue ? y.trans_amt.Value.ToString("N2") : "(Est.) " + UnitsValueMYR.ToString("N2")) + @" </td>
                                            <td>" + y.reject_reason + @"</td>
                                        </tr>");

                        });

                        viewTransHead.InnerHtml = tblHeader.ToString();
                        userTbody.InnerHtml = asb.ToString();

                    }
                    else if (UserOrderMOList.FirstOrDefault().order_type == 3 || UserOrderMOList.FirstOrDefault().order_type == 4)
                    {

                        tblHeader.Append(@"<tr>
                                                <th>No.</th>
                                                <th>From Fund</th>
                                                <th>Type</th>
                                                " + (UserOrderMOList.FirstOrDefault().order_status == 3 ? "<th class='text-right'>Unit Price (MYR)</th>" : "") + @"
                                                <th class='text-right'>Units</th>
                                                <th class='text-right'>Amount(MYR)</th>
                                                <th>Ref No / Remarks</th>
                                               </tr>");

                        List<OrderListDTO> ys = UserOrderMOList.Where(z => z.order_type == 4).ToList();
                        UserOrderMOList = UserOrderMOList.Where(z => z.order_type == 3).ToList();
                        UserOrderMOList.ForEach(y =>
                        {
                            decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);

                            index++;
                            asb.Append(@"<tr>
                                                        <td>" + index + @"</td>
                                                        <td>" + y.Fund_Name.Capitalize() + @"</td>
                                                        <td>Switch Sell</td>
                                                        " + (y.order_status == 3 ? "<td class='text-right'>" + (y.order_status == 3 ? y.LATEST_NAV_PRICE1 + "" : "-") + @"</td>" : "") + @"
                                                        <td class='text-right'>" + y.units.ToString("N4") + @"</td>
                                                        <td class='text-right'>" + (y.order_status == 3 ? y.amount.ToString("N2") : "(Est.) " + UnitsValueMYR.ToString("N2", new CultureInfo("en-US"))) + @"</td>
                                                        <td>" + y.reject_reason + @"</td>
                                                    </tr>");
                            index++;
                            Decimal totalUnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);
                            string totalUnitsInValue = (totalUnitsValueMYR * y.LATEST_NAV_PRICE2.Value).ToString("N4", new CultureInfo("en-US"));
                            string UnitsInValue = (UnitsValueMYR * y.LATEST_NAV_PRICE2.Value).ToString("N4", new CultureInfo("en-US"));
                            OrderListDTO yy = ys.FirstOrDefault(z => z.order_no == y.order_no && z.order_type == 4 && z.Fund_Name == y.Fund_Name && z.Fund_Name2 == y.Fund_Name2);
                            asb.Append(@"<tr>
                                                        <td>" + index + @"</td>
                                                        <td>" + yy.Fund_Name2.Capitalize() + @"</td>
                                                        <td>Switch Buy</td>
                                                        " + (y.order_status == 3 ? "<td class='text-right'>" + (y.order_status == 3 ? y.LATEST_NAV_PRICE2 + "" : "-") + @"</td>" : "") + @"
                                                        <td class='text-right'>" + (yy.order_status == 3 && yy.trans_units.HasValue ? yy.trans_units.Value.ToString("N4") : "(Est.) " + totalUnitsInValue) + @"</td>
                                                        <td class='text-right'>" + (yy.order_status == 3 && yy.trans_amt.HasValue ? yy.trans_amt.Value.ToString("N2") : "(Est.) " + totalUnitsValueMYR.ToString("N2")) + @"</td>
                                                        <td>" + yy.reject_reason + @"</td>
                                                    </tr>");
                        });

                        viewTransHead.InnerHtml = tblHeader.ToString();
                        userTbody.InnerHtml = asb.ToString();

                    }

                }
                
            }
        }
    }
}