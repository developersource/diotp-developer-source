﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Admin.Dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .highcharts-color-green, .highcharts-color-green [class*=highcharts-color-], .highcharts-color-green .highcharts-point {
            fill: rgb(54, 179, 126);
        }

        .highcharts-color-blue, .highcharts-color-blue [class*=highcharts-color-], .highcharts-color-blue .highcharts-point {
            fill: rgb(0, 82, 204);
        }

        .link-container a {
            color: rgb(0, 82, 204);
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <form id="dashboardForm" runat="server">
        <div class="container-fluid">
            <strong class="pull-right text-danger" id="AsOfDate1" runat="server"></strong>
            <div class="row">
                <div class="col-md-6">
                    <div class="back-board clearfix">
                        <h4 class="text-bold text-center"><strong>MA REGISTRATIONS MILESTONE</strong></h4>
                        <figure class="highcharts-figure">
                            <div id="ma-count-chart"></div>
                        </figure>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="back-board clearfix">
                        <h4 class="text-bold text-center"><strong>AUM MILESTONE</strong></h4>
                        <figure class="highcharts-figure">
                            <div id="ma-asset-chart"></div>
                        </figure>
                    </div>
                </div>
            </div>

            <hr />

            <div class="row">
                <div class="col-md-6">
                    <div class="back-board clearfix">
                        <h4 class="text-bold text-center"><strong>MA REGISTRATIONS CHART</strong></h4>
                        <figure class="highcharts-figure">
                            <select name="MaChartPlotType" id="MaChartPlotType" runat="server" clientidmode="static" style="position: absolute; margin-top: -30px; z-index: 9; right: 25px;">
                                <option value="MTD">MTD</option>
                                <option value="YTD">YTD</option>
                            </select>
                            <div id="ma-chart"></div>
                            <%--<p class="highcharts-description">
                            A basic column chart compares rainfall values between four cities.
        Tokyo has the overall highest amount of rainfall, followed by New York.
        The chart is making use of the axis crosshair feature, to highlight
        months as they are hovered over.
                        </p>--%>
                        </figure>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="back-board clearfix">
                        <h4 class="text-bold text-center"><strong>MA INVESTMENTS CHART</strong></h4>
                        <figure class="highcharts-figure">
                            <select name="MaAssetPlotType" id="MaAssetPlotType" runat="server" clientidmode="static" style="position: absolute; margin-top: -30px; z-index: 9; right: 25px;">
                                <option value="MTD">MTD</option>
                                <option value="YTD">YTD</option>
                            </select>
                            <div id="aum-chart"></div>
                            <%--<p class="highcharts-description">
                            A basic column chart compares rainfall values between four cities.
        Tokyo has the overall highest amount of rainfall, followed by New York.
        The chart is making use of the axis crosshair feature, to highlight
        months as they are hovered over.
                        </p>--%>
                        </figure>
                    </div>
                </div>
            </div>

            <hr />

            <div class="row link-container">
                <div class="col-md-6">
                    <div class="back-board clearfix">
                        <h4 class="text-bold text-center"><strong>TO-DO LIST</strong></h4>
                        <table class="table table-bordered">
                            <tr>
                                <th>Address verification pending count</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AddressChanging.aspx' data-title='MA Address Verification'>
                                        <span id="addrVerCount" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Bank verification pending count</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='BankDetailsVerification.aspx' data-title='Bank Details Verification'>
                                        <span id="bankVerCount" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Payment pending count</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Payments.aspx' data-title='Payments'>
                                        <span id="paymentPendingCount" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Order pending count</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='Transactions.aspx' data-title='Transactions'>
                                        <span id="orderPendingCount" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>RSP enrollment pending count</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='RegularSavingPlans.aspx' data-title='Regular Saving Enrollment'>
                                        <span id="rspPendingCount" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Account Opening</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='AccountOpeningRequest.aspx' data-title='AccountOpeningRequest'>
                                        <span id="AccountOpeningCount" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="back-board clearfix">
                        <h4 class="text-bold text-center">
                            <strong>QUICK VIEW</strong>
                        </h4>
                        <table class="table table-bordered">
                            <tr>
                                <th>Hardcopy vs Softcopy requests</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='HardcopyRequest.aspx' data-title='Hardcopy Request'>
                                        <span id="newHardcopyRequestsCount" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>MA Bank bindings count</th>
                                <td>
                                    <a href='javascript:;' class='easyui-linkbutton tab-menu-item' data-url='MABanks.aspx' data-title='MA Bank Binding List'>
                                        <span id="newMABankBindingList" runat="server"></span>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" id="MaCountDiOTP" runat="server" clientidmode="static" />
        <input type="hidden" id="MaCountOracle" runat="server" clientidmode="static" />
        <input type="hidden" id="MaAssetDiOTP" runat="server" clientidmode="static" />
        <input type="hidden" id="MaAssetOracle" runat="server" clientidmode="static" />

    </form>
    <script src="Content/mycj/common-scripts.js?v=1.0"></script>
    <script src="/Content/js/highcharts.js"></script>
    <script src="Content/mycj/dashboard-scripts.js"></script>

</body>
</html>
