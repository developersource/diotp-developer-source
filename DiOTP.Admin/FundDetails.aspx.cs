﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class FundDetails : System.Web.UI.Page
    {
        private static readonly Lazy<IFundInfoService> lazyFundInfoObj = new Lazy<IFundInfoService>(() => new FundInfoService());

        public static IFundInfoService IFundInfoService { get { return lazyFundInfoObj.Value; } }

        private static readonly Lazy<IUtmcDailyNavFundService> lazyUtmcDailyNavFundObj = new Lazy<IUtmcDailyNavFundService>(() => new UtmcDailyNavFundService());

        public static IUtmcDailyNavFundService IUtmcDailyNavFundService { get { return lazyUtmcDailyNavFundObj.Value; } }

        private static readonly Lazy<IUtmcFundCorporateActionService> lazyUtmcFundCorporateActionObj = new Lazy<IUtmcFundCorporateActionService>(() => new UtmcFundCorporateActionService());

        public static IUtmcFundCorporateActionService IUtmcFundCorporateActionService { get { return lazyUtmcFundCorporateActionObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        private static readonly Lazy<IFundReturnService> lazyIFundReturnServiceObj = new Lazy<IFundReturnService>(() => new FundReturnService());

        public static IFundReturnService IFundReturnService { get { return lazyIFundReturnServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundFileService> IutmcFundFileService = new Lazy<IUtmcFundFileService>(() => new UtmcFundFileService());

        public static IUtmcFundFileService IutmcFundFile { get { return IutmcFundFileService.Value; } }

        private static readonly Lazy<IUtmcFundDetailService> lazyfdObj = new Lazy<IUtmcFundDetailService>(() => new UtmcFundDetailService());

        public static IUtmcFundDetailService IUtmcFundDetailService { get { return lazyfdObj.Value; } }

        public static string Prospectus = "";
        public static string Annual_Report = "";
        public static string Product_Highlight_Sheet = "";
        public static string Factsheet = "";
        public static string Others = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isAccountAdmin"] != null && Session["isAccountAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                String idString = Request.QueryString["id"];
                if (!string.IsNullOrEmpty(idString))
                {
                    int id = Convert.ToInt32(idString);
                    Response response = IUtmcFundInformationService.GetSingle(id);
                    if (response.IsSuccess)
                    {
                        UtmcFundInformation utmcFundInformation = (UtmcFundInformation)response.Data;

                        Response responseUFDList = IUtmcFundDetailService.GetDataByPropertyName(nameof(UtmcFundDetail.UtmcFundInformationId), utmcFundInformation.Id.ToString(), true, 0, 0, false);
                        if (responseUFDList.IsSuccess)
                        {
                            UtmcFundDetail ufd = ((List<UtmcFundDetail>)responseUFDList.Data).FirstOrDefault();
                            String propName = nameof(UtmcFundInformation.FundCode);
                            propName = nameof(FundInfo.FundCode);

                            Response responseFIList = IFundInfoService.GetDataByPropertyName(propName, utmcFundInformation.IpdFundCode, true, 0, 0, false);
                            if (responseFIList.IsSuccess) {
                                FundInfo fundInfo = ((List<FundInfo>)responseFIList.Data).FirstOrDefault();
                                StringBuilder sb = new StringBuilder();
                                sb.Append(@"<tr class='fs-12'>
                                    <th><span class='fundName'>" + utmcFundInformation.FundCode + @"</span> <span class='ml-10'>" + (utmcFundInformation.IsRetail == 0 ? "<img src='/Content/MyImage/12.png' height='16' style='margin-top:-5px;' />" : "") + @"</span><span class='fundType'>" + (ufd.IsEpfApproved == 1 ? "EPF-MIS" : "") + @"</span> <br><span class='fundFullname'>" + utmcFundInformation.FundName.Capitalize() + @"</span></th>
                                    <td>" + String.Format("{0:0.0000}", fundInfo.CurrentUnitPrice) + @"</td>
                                    <td><span class='" + (fundInfo.ChangePrice > 0 ? "fund-success-text" : fundInfo.ChangePrice == 0 ? "fund-warning-text" : "fund-danger-text") + @"'>" + String.Format("{0:0.0000}", fundInfo.ChangePrice) + @"</span></td>
                                    <td><span class='" + (fundInfo.ChangePer > 0 ? "fund-success" : fundInfo.ChangePer == 0 ? "fund-warning" : "fund-danger") + @"'>" + String.Format("{0:0.00}", fundInfo.ChangePer) + @" %</span></td>
                                    <td>" + utmcFundInformation.FundBaseCurrency + @"</td>
                                    <td>" + utmcFundInformation.LipperCategoryOfFund + @"</td>
                                    <td>" + fundInfo.CurrentNavDate.ToString("yyyy-MM-dd") + @" </td>
                                    <td class='hide'>" + utmcFundInformation.FundCls + @"</td>
                                    <td class='hide'>" + utmcFundInformation.IsRetail + @"</td>
                                </tr>");
                                
                                fundTableBody.InnerHtml = sb.ToString();

                                DateTime currentDate = fundInfo.CurrentNavDate;
                                DateTime ToDate = DateTime.Now;
                                string fileURL = ConfigurationManager.AppSettings["investorURL"].ToString();
                                string fundID = nameof(UtmcFundFile.UtmcFundInformationId);
                                Response responseUFFList = IutmcFundFile.GetDataByPropertyName(fundID, utmcFundInformation.Id.ToString(), true, 0, 0, false);
                                if (responseUFFList.IsSuccess)
                                {
                                    List<UtmcFundFile> Downloadfiles = ((List<UtmcFundFile>)responseUFFList.Data).Where(x => x.Status == 1).ToList();
                                    StringBuilder sbDownloadLinks = new StringBuilder();
                                    int index = 1;

                                    foreach (UtmcFundFile f in Downloadfiles)
                                    {
                                        if (f.Url != null)
                                        {
                                            f.Url =  "\""+ fileURL + f.Url + "\"";
                                            sbDownloadLinks.Append(@"<div class='panel'>
                                                    <div class='panel-heading'>
                                                         " + index + @". 
                                                        <a href=" + f.Url + @" target='_blank'>Click here for " + f.Name + @"</a>
                                                    </div>
                                                </div>");
                                            index++;
                                        }
                                    }
                                    downloadFileLinks.InnerHtml = sbDownloadLinks.ToString();
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                    "alert('" + responseUFFList.Message + "');", true);
                                }

                                UtmcFundDetail uFD = utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault();

                                if (utmcFundInformation.ForeignFund == "F" || utmcFundInformation.ForeignFund == "f")
                                {
                                    fundZone.InnerHtml = "Foreign";
                                }
                                else if (utmcFundInformation.ForeignFund == "L" || utmcFundInformation.ForeignFund == "l")
                                {
                                    fundZone.InnerHtml = "Local";
                                }

                                if (utmcFundInformation.IsRetail == 1)
                                {
                                    IsRetail.InnerHtml = "True";
                                }
                                else if (utmcFundInformation.IsRetail == 0)
                                {
                                    IsRetail.InnerHtml = "False";
                                }

                                if (utmcFundInformation.IsEmis == 1)
                                {
                                    IsEmis.InnerHtml = "True";
                                }
                                else if (utmcFundInformation.IsEmis == 0)
                                {
                                    IsEmis.InnerHtml = "False";
                                }

                                if (utmcFundInformation.IsRsp == 1)
                                {
                                    RspApplicable.InnerHtml = "True";
                                }
                                else if (utmcFundInformation.IsEmis == 0)
                                {
                                    IsEmis.InnerHtml = "False";
                                }

                                if (utmcFundInformation.Conventional == "C" || utmcFundInformation.Conventional == "c")
                                {
                                    fundStatus.InnerHtml = "Conventional";
                                }
                                else if (utmcFundInformation.Conventional == "S" || utmcFundInformation.Conventional == "s")
                                {
                                    fundStatus.InnerHtml = "Shariah";
                                }

                                SatGrp.InnerHtml = utmcFundInformation.SatGroup;
                                fundCls.InnerHtml = utmcFundInformation.FundCls;
                                fundIpdCode.InnerHtml = utmcFundInformation.IpdFundCode;
                                fundCategory.InnerHtml = utmcFundInformation.UtmcFundCategoriesDefIdUtmcFundCategoriesDef!= null ? utmcFundInformation.UtmcFundCategoriesDefIdUtmcFundCategoriesDef.Name : "";
                                fundLaunchDate.InnerHtml = uFD.LaunchDate.ToString("dd/MM/yyyy");
                                fundLaunchPrice.InnerHtml = "MYR " + uFD.LaunchPrice.ToString("N4", new CultureInfo("en-US"));
                                fundRelaunchDate.InnerHtml = uFD.RelaunchDate.Value.ToString("dd/MM/yyyy");
                                fundPricingBasis.InnerHtml = uFD.PricingBasis;
                                fundLatestNavPrice.InnerHtml = "MYR " + fundInfo.CurrentUnitPrice + " (" + fundInfo.CurrentNavDate.ToString("MMMM dd, yyyy") + ")";
                                fundHistoricalIncomeDistribution.InnerHtml = uFD.HistoricalIncomeDistribution == 0 ? "No" : "Yes";
                                fundApprovedByEPF.InnerHtml = uFD.IsEpfApproved == 0 ? "No" : "Yes";
                                fundShariahComplaint.InnerHtml = uFD.ShariahCompliant == 0 ? "No" : "Yes";
                                fundRiskRating.InnerHtml = uFD.RiskRating;
                                fundSize.InnerHtml = "MYR " + uFD.FundSizeRm;
                                fundMinimumInitialInvestment.InnerHtml = "MYR " + uFD.MinInitialInvestmentCash.ToString("N", new CultureInfo("en-US")) + "/MYR " + uFD.MinInitialInvestmentEpf.ToString("N", new CultureInfo("en-US"));
                                fundMinimumSubsequentInvestment.InnerHtml = "MYR " + uFD.MinSubsequentInvestmentCash.ToString("N", new CultureInfo("en-US")) + "/MYR " + uFD.MinSubsequentInvestmentEpf.ToString("N", new CultureInfo("en-US"));
                                fundMinimumRSPInvestment.InnerHtml = uFD.MinRspInvestmentInitialRm == 0 ? "-" : "MYR " + uFD.MinRspInvestmentInitialRm.ToString("N", new CultureInfo("en-US")) + "/MYR " + uFD.MinRspInvestmentAdditionalRm.ToString("N", new CultureInfo("en-US"));
                                fundMinimumRedemptionAmount.InnerHtml = uFD.MinRedAmountUnits == 0 ? "No" : uFD.MinRedAmountUnits.ToString("N", new CultureInfo("en-US")) + " Units";
                                fundMinimumHolding.InnerHtml = uFD.MinHoldingUnits.ToString("N", new CultureInfo("en-US")) + " Units";
                                fundCoolingOffPeriod.InnerHtml = uFD.CollingOffPeriod;
                                fundDistributionPolicy.InnerHtml = uFD.DistributionPolicy;
                                fundInvestmentObjective.InnerHtml = uFD.InvestmentObjective;
                                fundInvestmentStrategyAndPolicy.InnerHtml = uFD.InvestmentStrategyAndPolicy;
                                if (uFD.InvestmentStrategyAndPolicy != "" && uFD.InvestmentStrategyAndPolicy != null)
                                    fundInvestmentStrategyAndPolicyTD.Visible = true;

                                //Charges
                                UtmcFundCharge uFC = utmcFundInformation.UtmcFundInformationIdUtmcFundCharges.FirstOrDefault();
                                if (uFC != null)
                                {
                                    fundDiscountedInitialSalesCharge.InnerHtml = uFC.InitialSalesChargesPercent == 0 ? "NIL" : uFC.InitialSalesChargesPercent.ToString() + "%";
                                    fundEPFSalesCharge.InnerHtml = uFC.EpfSalesChargesPercent == 0 ? "NIL" : uFC.EpfSalesChargesPercent.ToString() + "%";
                                    fundAnnualManagementCharge.InnerHtml = uFC.AnnualManagementChargePercent == 0 ? "NIL" : uFC.AnnualManagementChargePercent.ToString() + "% (p.a. of the NAV)";
                                    fundTrusteeFee.InnerHtml = uFC.TrusteeFeePercent == 0 ? "NIL" : uFC.TrusteeFeePercent + "%  (p.a. of the NAV)";
                                    fundSwitchingFee.InnerHtml = uFC.SwitchingFeePercent == 0 ? "NIL" : "3 free switches per account per calendar year, subsequent switches will be charged a " + uFC.SwitchingFeePercent + "% of redemption moneys for administrative purpose.";
                                    fundRedemptionFee.InnerHtml = uFC.RedemptionFeePercent == 0 ? "NIL" : uFC.RedemptionFeePercent + "% (p.a. of the NAV) if units are redeemed within the 30-day Redemption Notice Period.";
                                    fundTransferFee.InnerHtml = uFC.TransferFeeRm == 0 ? "Not Applicable" : "A fee of RM" + uFC.TransferFeeRm + " will be charged for each transfer.";
                                    fundOtherSignificantFee.InnerHtml = uFC.OtherSignificantFeeRm == 0 ? "-" : "" + uFC.OtherSignificantFeeRm;
                                    fundGSTFee.InnerHtml = "* Subject to GST of " + uFC.GstPercent + "%";
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                    "alert('" + responseFIList.Message + "');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                "alert('" + responseUFDList.Message + "');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                            "alert('" + response.Message + "');", true);
                    }
                }
            }
        }
    }
}