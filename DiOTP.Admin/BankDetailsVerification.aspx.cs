﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace Admin
{
    public partial class BankDetailsVerification : System.Web.UI.Page
    {
        private static readonly Lazy<IBanksDefService> lazyBanksDefServiceObj = new Lazy<IBanksDefService>(() => new BanksDefService());

        public static IBanksDefService IBanksDefService { get { return lazyBanksDefServiceObj.Value; } }

        private static readonly Lazy<IMaHolderBankService> lazyHolderBankServiceObj = new Lazy<IMaHolderBankService>(() => new MaHolderBankService());

        public static IMaHolderBankService IMaHolderBankService { get { return lazyHolderBankServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserTypeService> lazyUserTypeServiceObj = new Lazy<IUserTypeService>(() => new UserTypeService());

        public static IUserTypeService IUserTypeService { get { return lazyUserTypeServiceObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder asb = new StringBuilder();
            StringBuilder filter = new StringBuilder();
            string mainQ = (@"select m.id, m.created_date as created_date, m.updated_date as settlement_date, u.username, u.id_no, b.name as bank_name, m.account_name, m.bank_account_no, m.image, m.status from ");
            string mainQCount = (@"SELECT count(*) from ");
            filter.Append(@" ma_holder_bank m join users u on m.user_id = u.id 
                            join banks_def b on m.bank_def_id = b.ID 
                            where 1=1 and b.status=1 ");
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                User user = (User)Session["admin"];
                Response response = IUserTypeService.GetDataByFilter("user_id = '" + user.Id + "'", 0, 0, false);
                UserType loginUserType = new UserType();
                if (response.IsSuccess)
                {
                    loginUserType = ((List<UserType>)response.Data).FirstOrDefault();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and (u.username like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or u.id_no like '%" + Search.Value.Trim() + "%' ");
                    filter.Append(" or m.bank_account_no like '%" + Search.Value.Trim() + "%') ");
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    if (FilterValue.Value != "All")
                        filter.Append(" and m.status in ('" + FilterValue.Value + "')");
                    else
                        filter.Append(@" and m.status in ('0','1','9') ");
                }
                else
                {
                    filter.Append(" and m.status in ('0')");
                }

                if (Request.QueryString["hdnOrderByColumn"] != "" && Request.QueryString["hdnOrderByColumn"] != null && Request.QueryString["hdnOrderByColumn"].Trim() != "-")
                {
                    hdnOrderByColumn.Value = Request.QueryString["hdnOrderByColumn"].ToString();
                    hdnOrderType.Value = Request.QueryString["hdnOrderType"].ToString();
                    filter.Append(" order by " + hdnOrderByColumn.Value + " " + hdnOrderType.Value + " ");
                }
                else
                {
                    if (FilterValue.Value == "0" || string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                    {
                        filter.Append("order by m.status, m.created_date desc");
                    }
                    else
                    {
                        filter.Append("order by m.status, m.updated_date desc");
                    }
                }


                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (Request.QueryString["FilterValue"] != "" && Request.QueryString["FilterValue"] != null && Request.QueryString["FilterValue"].Trim() != "-")
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    //filter.Append(" and status in ('" + FilterValue.Value + "')");
                }
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + filter.ToString()).Data.ToString();

                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }


                Response responseBankDetailsVerification = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, true);


                if (responseBankDetailsVerification.IsSuccess)
                {


                    var BankDefsDyn = responseBankDetailsVerification.Data;
                    var responseJSON = JsonConvert.SerializeObject(BankDefsDyn);
                    List<DiOTP.Utility.BankDetailsVerification> BankDefs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.BankDetailsVerification>>(responseJSON);

                    int index = 1;

                    foreach (DiOTP.Utility.BankDetailsVerification m in BankDefs)
                    {
                        string image = !string.IsNullOrEmpty(m.image) ? "Image <i class='fa fa-eye'></i>" : "";
                        string checkBox = string.Empty;
                        if (m.status == 0)
                        {
                            checkBox = @"<div class='square single-row'>
                                            <div class='checkbox'>
                                                <input type = 'checkbox' name='checkRow' class='checkRow' value='" + m.id + @"' /> <label>" + index + @"</label><br/>
                                            </div>
                                        </div>";
                        }
                        else
                        {
                            checkBox = @"<label>" + index + @"</label><br/>";
                        }


                        asb.Append(@"<tr>
                                        <td class='icheck'>
                                                " + checkBox + @"
                                            <span class='row-status'>" + (m.status == 1 ? "<span class='label label-success'>Approved</span>" :
                                                                            m.status == 0 ? "<span class='label label-warning'>Pending</span>" :
                                                                            m.status == 9 ? "<span class='label label-danger'>Rejected</span>" : "") + @"
                                            </span>
                                        </td>
                                        <td>
                                            " + m.created_date.ToString("dd/MM/yyyy") + @"
                                        </td>
                                        <td>
                                            " + (m.status == 0 ? "-" : m.settlement_date.Value.ToString("dd/MM/yyyy HH:mm:ss")) + @"
                                        </td>
                                        <td>" + m.username + @"</td>
                                        <td>" + m.id_no + @"</td>
                                        <td>
                                                <div><label>Bank name</label>: <span>" + m.bank_name + @"</span></div>
                                                <div><label>Account name</label>: <span>" + m.account_name + @"</span></div>
                                                <div><label>Account number</label>: <span>" + m.bank_account_no + @"</span></div>
                                        </td>
                                        <td><a target='_blank' href='" + m.image + @"'>" + image + @"</a></td>
                                    </tr>");
                        index++;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                    "alert('" + responseBankDetailsVerification.Message + "');", true);
                }



            }
            bankDetailsTbody.InnerHtml = asb.ToString();
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response response = new Response();
            string message = "";
            try
            {
                String title = "User Information Update";
                List<string> content1s = new List<string>();
                List<string> content2s = new List<string>();
                List<Email> emails = new List<Email>();
                List<String> dates = new List<String>();
                string idString = String.Join(",", ids);
                Response responseMHBList = IMaHolderBankService.GetDataByFilter(" ID in (" + idString + ")", 0, 0, false);
                if (responseMHBList.IsSuccess)
                {


                    List<MaHolderBank> maHolderBanks = (List<MaHolderBank>)responseMHBList.Data;
                    //}
                    if (action == "Activate")
                    {
                        //List<MaHolderBank> maHolderBanks1 = new List<MaHolderBank>();
                        maHolderBanks.ForEach(x =>
                        {
                            if (x.Status == 0)
                            {
                                x.Status = 1;
                                x.UpdatedDate = DateTime.Now;
                                Email email = new Email();
                                Response rU = IUserService.GetSingle(x.UserId);
                                if (rU.IsSuccess)
                                    email.user = (User)rU.Data;
                                content1s.Add("Bank account " + x.BankAccountNo.ToString() + " has been approved by Apex admin on ");
                                dates.Add(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                                content2s.Add("");
                                emails.Add(email);
                                UserLogMain z = new UserLogMain()
                                {
                                    TableName = "ma_holder_bank",
                                    Description = "Admin approved user request for add bank details",
                                    UpdatedDate = DateTime.Now,
                                    UserId = x.UserId,
                                    RefId = x.Id,
                                    RefValue = x.AccountName,
                                    StatusType = 1
                                };
                                IUserLogMainService.PostData(z);
                                

                                message += "" + x.BankAccountNo + " Approved.<br/>";
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Bank Details (" + x.BankAccountNo + ") Request Successfully approved",
                                    TableName = "ma_holder_bank",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "status",
                                    ValueOld = "0",
                                    ValueNew = "1",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);
                                //Audit Log Ends here
                            }
                            else if (x.Status == 9)
                            {
                                message += "" + x.BankAccountNo + " is already rejected.<br/>";
                            }
                            else if (x.Status == 1)
                            {
                                message += "" + x.BankAccountNo + " is already approved.<br/>";
                            }
                        });
                        IMaHolderBankService.UpdateBulkData(maHolderBanks);
                        
                    }
                    if (action == "Reject")
                    {
                        maHolderBanks.ForEach(x =>
                        {
                            if (x.Status == 0)
                            {
                                x.Status = 9;
                                x.UpdatedDate = DateTime.Now;
                                Email email = new Email();
                                Response rU = IUserService.GetSingle(x.UserId);
                                if (rU.IsSuccess)
                                    email.user = (User)rU.Data;
                                content1s.Add("Bank account " + x.BankAccountNo.ToString() + " has been rejected by Apex admin on ");
                                dates.Add(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                                content2s.Add("");
                                emails.Add(email);
                                UserLogMain z = new UserLogMain()
                                {
                                    TableName = "ma_holder_bank",
                                    Description = "Admin rejected user request for add bank details",
                                    UpdatedDate = DateTime.Now,
                                    UserId = x.UserId,
                                    RefId = x.Id,
                                    RefValue = x.AccountName,
                                    StatusType = 0
                                };
                                IUserLogMainService.PostData(z);
                                message += "" + x.BankAccountNo + " Rejected.<br/>";
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Bank Details (" + x.BankAccountNo + ") Request rejected",
                                    TableName = "ma_holder_bank",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "status",
                                    ValueOld = "1",
                                    ValueNew = "0",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);
                                //Audit Log Ends here
                            }
                            else if (x.Status == 9)
                            {
                                message += "" + x.BankAccountNo + " is already rejected.<br/>";
                            }
                            else if (x.Status == 1)
                            {
                                message += "" + x.BankAccountNo + " is already approved.<br/>";
                            }
                        });
                        IMaHolderBankService.UpdateBulkData(maHolderBanks);
                    }
                }
                emails.ForEach(x =>
                {
                    EmailService.SendUpdateMail(x, title, content1s[emails.IndexOf(x)], dates[emails.IndexOf(x)], content2s[emails.IndexOf(x)]);
                });

                response.IsSuccess = true;
                response.Message = message;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.WriteLine("Bank details action: " + ex.Message);
            }
            return response;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Download(string filter)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            string message = string.Empty;
            try
            {
                
                string tempPath = Path.GetTempPath() + "Bank_details_" + DateTime.Now.ToString("yyyyMMddhhmmssfff");
                FileInfo excelFile = new FileInfo(tempPath);
                message = excelFile.FullName;
                ExcelPackage excel = new ExcelPackage();
                excel.Workbook.Worksheets.Add("Bank Details");

                var headerRow = new List<string[]>()
            {
                new string[] { "S.No", "Status", "Requested date", "Appv/Rej date", "Username", "ID No.", "Bank name", "Account name", "Account.No", "Image URL" }
            };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A2:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "2";

                var worksheet = excel.Workbook.Worksheets["Bank Details"];

                string docDetails = "Bank Details";

                try
                {
                    // ------------------------------------------------
                    // Creation of header cells
                    // ------------------------------------------------
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    // ------------------------------------------------
                    // Populate sheet with some real data from list
                    // ------------------------------------------------
                    int row = 3; // start row (in row 1 are header cells)
                    StringBuilder filterQuery = new StringBuilder();
                    filterQuery.Append(@"select m.created_date as created_date, m.updated_date as settlement_date, u.username, u.id_no, b.name as bank_name, m.account_name, m.bank_account_no, m.image, m.status 
from ma_holder_bank m join users u on m.user_id = u.id 
join banks_def b on m.bank_def_id = b.ID where 1=1 ");

                    if (!string.IsNullOrEmpty(filter) && filter != "")
                    {

                        docDetails += " | Status: " + (filter == "0" ? "Pending" : (filter == "1" ? "Approved" : (filter == "9" ? "Rejected" : (filter == "All" ? "All (Pending, Approved, Rejected)" : "-")))) + " ";
                        if (filter != "All")
                            filterQuery.Append(" and m.status in ('" + filter + "')");
                        else
                            filterQuery.Append(@" and m.status in ('0','1','9') ");

                    }
                    else
                    {
                        docDetails += " | Status: Pending";
                        filterQuery.Append(@" and m.status in ('0') ");
                    }
                    int skip = 0, take = 20;
                    filterQuery.Append(@"order by status , m.created_date desc");

                    string docDetailsRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    worksheet.Cells[docDetailsRange].Merge = true;
                    worksheet.Cells[docDetailsRange].Value = docDetails;
                    worksheet.Cells[docDetailsRange].Style.Font.Bold = true;

                    Response responseMHBList = GenericService.GetDataByQuery(filterQuery.ToString(), 0, 0, false, null, false, null, true);

                    if (responseMHBList.IsSuccess)
                    {
                        var bankdetailsverifications = responseMHBList.Data;
                        var responseJSON = JsonConvert.SerializeObject(bankdetailsverifications);
                        List<DiOTP.Utility.BankDetailsVerification> bankDetailsVerification = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.BankDetailsVerification>>(responseJSON);


                        int index = 1;
                        bankDetailsVerification.ForEach(x =>
                        {
                            worksheet.Cells[row, 1].Value = index;
                            worksheet.Cells[row, 2].Value = (x.status == 1 ? "Approved" : x.status == 0 ? "Pending" : x.status == 9 ? "Rejected" : "");
                            worksheet.Cells[row, 3].Value = x.created_date.ToString("dd/MM/yyyy");
                            worksheet.Cells[row, 4].Value = x.settlement_date.ToString();
                            worksheet.Cells[row, 5].Value = x.username;
                            worksheet.Cells[row, 6].Value = x.id_no;
                            worksheet.Cells[row, 7].Value = x.bank_name;
                            worksheet.Cells[row, 8].Value = x.account_name;
                            worksheet.Cells[row, 9].Value = x.bank_account_no;
                            worksheet.Cells[row, 10].Value = x.image;
                            row++;
                            index++;
                        });
                    }

                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    // Apply some predefined styles for data to look nicely :)
                    worksheet.Cells[headerRange].Style.Font.Bold = true;

                    // Save this data as a file
                    excel.SaveAs(excelFile);

                    // Display SUCCESS message
                    response.IsSuccess = true;
                    response.Message = "Downloading...";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "Bank Details Verification List downloaded",
                        TableName = "ma_holder_bank",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);


                    //Audit Log ends here
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                }
                finally
                {
                    
                    //Empty variables
                    excel = null;
                    worksheet = null;

                    // Force garbage collector cleaning
                    GC.Collect();

                    //change to byte[]
                    byte[] result = File.ReadAllBytes(excelFile.ToString());
                    String file = Convert.ToBase64String(result);
                    //delete temporary file
                    File.Delete(excelFile.ToString());
                    response.Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + file;
                    response.Message = "Bank_Details_Verification" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    //response.Data = "data:application/vnd.ms-excel;base64," + file;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + message;
            }
            return response;
        }

    }
}