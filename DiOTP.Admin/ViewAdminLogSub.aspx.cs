﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class ViewAdminLogSub : System.Web.UI.Page
    {
        private static readonly Lazy<IAdminLogSubService> lazyIAdminLogSuberviceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());

        public static IAdminLogSubService IAdminLogSubService { get { return lazyIAdminLogSuberviceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyIAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());

        public static IAdminLogMainService IAdminLogMainService { get { return lazyIAdminLogMainServiceObj.Value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            else
            {
                String idString = Request.QueryString["id"];
                Response responseAL = IAdminLogMainService.GetSingle(Convert.ToInt32(idString));
                if (responseAL.IsSuccess)
                {
                    AdminLogMain adminLogMain = (AdminLogMain)responseAL.Data;
                    int index = 1;
                    Response responseULSList = IAdminLogSubService.GetDataByPropertyName(nameof(AdminLogSub.AdminLogMainId), idString, true, 0, 0, true);
                    if (responseULSList.IsSuccess)
                    {
                        List<AdminLogSub> uls = (List<AdminLogSub>)responseULSList.Data;
                        StringBuilder asb = new StringBuilder();
                        if (uls.Count() != 0)
                        {
                            foreach (AdminLogSub x in uls)
                            {
                                if (x.ColumnName != "modified_date" && x.ColumnName != "modified_by")
                                {
                                    Type t = GetType(adminLogMain.TableName.ToLower() + "_" + x.ColumnName.ToLower());
                                    //if (adminLogMain.TableName == "user_orders" && x.ColumnName == "order_status")
                                    if (t != null)
                                    {
                                        
                                        x.ValueOld = Enumeration.GetById(t, x.ValueOld);
                                        x.ValueNew = Enumeration.GetById(t, x.ValueNew);
                                        
                                    }
                                    Type tc = GetType("db_column");
                                    if (tc != null && Enumeration.GetById(tc, (adminLogMain.TableName + "_" + x.ColumnName).ToUpper()) != "")
                                    {
                                        x.ColumnName = Enumeration.GetById(tc, (adminLogMain.TableName + "_" + x.ColumnName).ToUpper());
                                    }
                                    if (IsValidFileFormat(x.ValueOld) && IsValidFileFormat(x.ValueNew))
                                    {
                                        asb.Append(@"<tr>
                                        <td>" + index.ToString() + @"</td>
                                        <td>" + x.ColumnName + @"</td>
                                        <td><a href='" + x.ValueOld + @"' target='_blank'><img height='100' src='" + x.ValueOld + @"' title='" + x.ValueOld + @"' /></a></td>
                                        <td><a href='" + x.ValueNew + @"' target='_blank'><img height='100' src='" + x.ValueNew + @"' title='" + x.ValueNew + @"'/></a></td>
                                    </tr>");
                                    }
                                    else
                                    {
                                        asb.Append(@"<tr>
                                        <td>" + index.ToString() + @"</td>
                                        <td>" + x.ColumnName + @"</td>
                                        <td>" + x.ValueOld + @"</td>
                                        <td>" + x.ValueNew + @"</td>
                                    </tr>");
                                    }
                                    index++;
                                }
                            }
                            userTbody.InnerHtml = asb.ToString();
                        }
                        else
                        {
                            asb.Append(@"<tr>
                                <td colspan='4' style='text-align:center'>No detail found!</td>
                            </tr>");
                            userTbody.InnerHtml = asb.ToString();
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                "alert('" + responseULSList.Message + "');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                "alert('" + responseAL.Message + "');", true);
                }
            }
        }

        public bool IsValidFileFormat(String url)
        {
            if (Path.GetExtension(url).ToLower() == ".jpg" ||
                Path.GetExtension(url).ToLower() == ".jpeg" ||
                Path.GetExtension(url).ToLower() == ".png" ||
                Path.GetExtension(url).ToLower() == ".pdf")
            {
                return true;
            }
            else
                return false;
        }


        public static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType("DiOTP.Utility.CustomClasses." + typeName);
                if (type != null)
                    return type;
            }
            return null;
        }
    }
}