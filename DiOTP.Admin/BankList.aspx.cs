﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;

namespace Admin
{
    public partial class BankList : System.Web.UI.Page
    {
        private static readonly Lazy<IBanksDefService> lazyBanksDefServiceObj = new Lazy<IBanksDefService>(() => new BanksDefService());
        public static IBanksDefService IBanksDefService { get { return lazyBanksDefServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isAccountAdmin"] != null && Session["isAccountAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                StringBuilder filter = new StringBuilder();
                 filter.Append(" 1 = 1 ");

                if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    string columnName = Converter.GetColumnNameByPropertyName<BanksDef>(nameof(BanksDef.Name));
                    filter.Append(" and " + columnName + " like '%" + Search.Value.Trim() + "%'");
                }
                               
                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = IBanksDefService.GetCountByFilter(filter.ToString()).ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }
                filter.Append(" order by name ");
                Response responseBDList = IBanksDefService.GetDataByFilter(filter.ToString(), skip, take, false);
                if (responseBDList.IsSuccess)
                {
                    List<BanksDef> banksDefs = (List<BanksDef>)responseBDList.Data;

                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    foreach (BanksDef bd in banksDefs)
                    {
                        asb.Append(@"<tr>
                                        <td class='icheck'>
                                            <div class='square single-row'>
                                                 <div class='checkbox'>
                                                    <input type='checkbox' name='checkRow' class='checkRow' value='" + bd.Id + @"' /> <label>" + index + @"</label><br/>
                                                 </div>
                                              </div>
                                             <span class='row-status'>" + (bd.Status == 1 ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Inactive</span>") + @"</span>
                                        </td>
                                        <td>" + bd.ShortName + @"</td>
                                        <td>" + bd.Name + @"</td>
                                     </tr>
                                  ");
                                    //<td>" + bd.NoFormat + @"</td>
                        index++;

                    }
                    bankTbody.InnerHtml = asb.ToString();
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            List<string> responseMsgs = new List<string>();
            try
            {
                string idString = String.Join(",", ids);
                Response responseBDList = IBanksDefService.GetDataByFilter(" ID in (" + idString + ")", 0, 0, false);
                if (responseBDList.IsSuccess)
                {
                    List<BanksDef> banksDefs = (List<BanksDef>)responseBDList.Data;
                    if (action == "Deactivate")
                    {
                        banksDefs.ForEach(x =>
                        {
                            x.Status = 0;
                            responseMsgs.Add("[" + x.Name + "] has been deactivated");
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = x.Name + " has been deactivated",
                                TableName = "banks_def",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }

                            //Audit Log Ends here
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "status",
                                ValueOld = "1",
                                ValueNew = "0",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                        });
                        IBanksDefService.UpdateBulkData(banksDefs);
                        
                    }
                    if (action == "Activate")
                    {
                        banksDefs.ForEach(x =>
                        {
                            x.Status = 1;
                            responseMsgs.Add("[" + x.Name + "] has been activated");
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = x.Name + " has been activated",
                                TableName = "banks_def",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id,
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "status",
                                ValueOld = "0",
                                ValueNew = "1",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                            //Audit Log Ends here

                        });
                        IBanksDefService.UpdateBulkData(banksDefs);
                    }
                    response.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Banks Defs action: " + ex.Message);
                response.IsSuccess = false;
                responseMsgs.Add(ex.Message);
            }
            response.Message = String.Join(",<br/>", responseMsgs.ToArray());
            return response;

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(BanksDef obj)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();
            Int32 numval;

            if (obj.Name == "")
            {
                response.IsSuccess = false;
                response.Message = "Enter the Name";
                return response;
            }
            //if (obj.NoFormat == "")
            //{
            //    response.IsSuccess = false;
            //    response.Message = "Enter the No of Format";
            //    return response;
            //}
            //if (Int32.TryParse(obj.NoFormat, out numval) == false)
            //{
            //    response.IsSuccess = false;
            //    response.Message = "Enter the No of Format in numeric only";
            //    return response;
            //}
            try
            {
                if (obj.Id == 0)
                {
                    Response responseBN = IBanksDefService.GetDataByPropertyName(nameof(BanksDef.Name), obj.Name, true, 0, 0, false);
                    if (responseBN.IsSuccess)
                    {
                        List<BanksDef> nameMatches = (List<BanksDef>)responseBN.Data;
                        if (nameMatches.Count > 0)
                        {
                            response.IsSuccess = false;
                            response.Message = "You have enter existing Bank Name";
                            return response;
                        }
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = responseBN.Message;
                        //Audit Log starts here
                        AdminLogMain alm = new AdminLogMain()
                        {
                            Description = obj.Name + " has been added into bank list",
                            TableName = "banks_def",
                            UpdatedDate = DateTime.Now,
                            UserId = loginUser.Id,
                        };
                        Response responseLog = IAdminLogMainService.PostData(alm);
                        if (!responseLog.IsSuccess)
                        {
                            //Audit log failed
                        }
                        else
                        {

                        }

                        //Audit Log Ends here
                        return response;
                    }
                    IBanksDefService.PostData(obj);
                    response.IsSuccess = true;
                    response.Message = "Success";
                }
                else
                {
                    IBanksDefService.UpdateData(obj);
                    response.IsSuccess = true;
                    response.Message = "Success";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = obj.Name + " from bank list has been modified",
                        TableName = "banks_def",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);
                    if (!responseLog.IsSuccess)
                    {
                        //Audit log failed
                    }
                    else
                    {

                    }

                    //Audit Log Ends here
                }
                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Add banks def action: " + ex.Message);
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }
    }
}