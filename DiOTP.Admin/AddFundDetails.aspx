﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddFundDetails.aspx.cs" Inherits="Admin.AddFundDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="addFundDetails" runat="server">
        <div id="FormId" data-value="addFundDetails">
            <div class="mb-10">
                <label class="label label-default">Fund Information</label>
            </div>
            <div class="row">
                <div class="col-md-6 fundvalidation">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">IPD Fund Code:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="hidden" name="Id" value="0" id="Id" runat="server" clientidmode="static" />
                            <input type="hidden" name="EpfIpdCode" value="IPD033" id="EpfIpdCode" runat="server" clientidmode="static" />
                            <input type="hidden" name="EffectiveDate" value="0" id="EffectiveDate" runat="server" clientidmode="static" />
                            <input type="hidden" name="ReportDate" id="ReportDate" runat="server" clientidmode="static" />
                            <input type="text" name="IpdFundCode" id="IpdFundCode" runat="server" clientidmode="static" class="form-control readPro" placeholder="Enter IPD Fund Code"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" readonly="readonly"/>
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Fund Name:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="FundName" id="FundName" runat="server" clientidmode="static" class="form-control readPro" placeholder="Enter Fund Name" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Launch Price:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="LaunchPrice" value="" id="LaunchPrice" runat="server" clientidmode="static" class="form-control" placeholder="Enter Launch Price" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Launch Date:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="hidden" name="UtmcFundInformationId" value="0" id="UtmcFundInformationId" runat="server" clientidmode="static" /><!--default-date-picker-->
                            <input type="text" name="LaunchDate" value="" id="LaunchDate" runat="server" clientidmode="static" class="form-control readPro" placeholder="Enter Launch Date" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Relaunch Date:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="RelaunchDateDisplay" value="" id="RelaunchDateDisplay" runat="server" clientidmode="static" class="form-control default-date-picker" placeholder="e.g. 06/08/1997" />
                            <input type="hidden" name="RelaunchDate" value="" id="RelaunchDate" runat="server" clientidmode="static" class="form-control readPro" placeholder="Enter Relaunch Date" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Pricing Basis:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="PricingBasis" value="" id="PricingBasis" runat="server" clientidmode="static" class="form-control" placeholder="Enter Pricing Basis" />
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            <div class="mb-10">
                <label class="label label-default">Fund Details</label>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Fund Code:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="FundCode" value="" id="FundCode" runat="server" clientidmode="static" class="form-control" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Fund Category:</label>
                        </div>
                        <div class="col-md-8">
                            <select id="UtmcFundCategoriesDefId" name="UtmcFundCategoriesDefId" class="form-control" runat="server" clientidmode="static">
                                <option value="">Select</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Lipper Category Of Fund:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="LipperCategoryOfFund" id="LipperCategoryOfFund" runat="server" clientidmode="static" class="form-control" placeholder="Enter Lipper Category Of Fund" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Conventional Status:</label>
                        </div>
                        <div class="col-md-8">
                            <select name="Conventional" id="Conventional" class="form-control" runat="server" clientidmode="static">
                                <option value="">Select</option>
                                <option value="C">Conventional</option>
                                <option value="S">Shariah</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Fund Status:</label>
                        </div>
                        <div class="col-md-8">
                            <select name="Status" id="Status" runat="server" clientidmode="static" class="form-control">
                                <option value="">Select</option>
                                <option value="Active">Active</option>
                                <option value="Suspended">Suspended</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Fund Zone:</label>
                        </div>
                        <div class="col-md-8">
                            <select name="ForeignFund" id="ForeignFund" runat="server" clientidmode="static" class="form-control">
                                <option value="">Select</option>
                                <option value="L">Local</option>
                                <option value="F">Foreign</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Fund Base Currency:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="FundBaseCurrency" id="FundBaseCurrency" runat="server" clientidmode="static" class="form-control" placeholder="e.g. MYR" />
                        </div>
                    </div>
                    <div class="row mb-6 hide">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Is Emis?:</label>
                        </div>
                        <div class="col-md-8">
                            <select id="IsEmis" name="IsEmis" class="form-control" runat="server" clientidmode="static">
                                <option value="">Select</option>
                                <option value="1">True</option>
                                <option value="0">False</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-6 hide">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Fund Class:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="FundCls" id="FundCls" runat="server" clientidmode="static" class="form-control" placeholder="Enter Fund Class" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Is Retail?:</label>
                        </div>
                        <div class="col-md-8">
                            <select id="IsRetail" name="IsRetail" class="form-control" runat="server" clientidmode="static">
                                <option value="">Select</option>
                                <option value="1">True</option>
                                <option value="0">False</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Sat Groups:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="SatGroup" id="SatGroup" runat="server" clientidmode="static" class="form-control" placeholder="e.g. G1 ','" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">RSP Applicable:</label>
                        </div>
                        <div class="col-md-8">
                            <select id="IsRsp" name="IsRsp" class="form-control" runat="server" clientidmode="static">
                                <option value="">Select</option>
                                <option value="1">True</option>
                                <option value="0">False</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row mb-6">
                        <div class="col-md-2">
                            <label class="fs-14 mt-2">Investment Objective:</label>
                        </div>
                        <div class="col-md-10">
                            <textarea name="InvestmentObjective" rows="5" value="" id="InvestmentObjective" runat="server" clientidmode="static" class="form-control" placeholder="Enter Investment Objective">
                            </textarea>
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-2">
                            <label class="fs-14 mt-2">Investment Strategy And Policy:</label>
                        </div>
                        <div class="col-md-10">
                            <textarea name="InvestmentStrategyAndPolicy" rows="5" value="" id="InvestmentStrategyAndPolicy" runat="server" clientidmode="static" class="form-control" placeholder="Enter Investment Strategy And Policy">
                            </textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Latest NAV Price:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="LatestNavPrice" value="" id="LatestNavPrice" runat="server" clientidmode="static" class="form-control readPro" placeholder="e.g. 0.2597" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Historical Income Distribution:</label>
                        </div>
                        <div class="col-md-8">
                            <select id="HistoricalIncomeDistribution" name="HistoricalIncomeDistribution" runat="server" clientidmode="static" class="form-control">
                                <option value="">Select</option>
                                <option value="1">True</option>
                                <option value="0">False</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Is Epf Approved:</label>
                        </div>
                        <div class="col-md-8">
                            <select id="IsEpfApproved" name="IsEpfApproved" runat="server" clientidmode="static" class="form-control">
                                <option value="">Select</option>
                                <option value="1">True</option>
                                <option value="0">False</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Shariah Compliant:</label>
                        </div>
                        <div class="col-md-8">
                            <select id="ShariahCompliant" name="ShariahCompliant" runat="server" clientidmode="static" class="form-control">
                                <option value="">Select</option>
                                <option value="1">True</option>
                                <option value="0">False</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Risk Rating:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="RiskRating" value="" id="RiskRating" runat="server" clientidmode="static" class="form-control" placeholder="e.g. Aggressive" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Fund Size RM:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="FundSizeRm" value="" id="FundSizeRm" runat="server" clientidmode="static" class="form-control" placeholder="e.g RM 2.69 millionM" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Min Initial Investment Cash:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="MinInitialInvestmentCash" value="" id="MinInitialInvestmentCash" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 2000" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Min Initial Investment Epf:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="MinInitialInvestmentEpf" value="" id="MinInitialInvestmentEpf" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 2000" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Min Subsequent Investment Cash:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="MinSubsequentInvestmentCash" value="" id="MinSubsequentInvestmentCash" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 500" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Min Subsequent Investment Epf:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="MinSubsequentInvestmentEpf" value="" id="MinSubsequentInvestmentEpf" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 500" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Min Rsp Investment Initial RM:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="MinRspInvestmentInitialRm" value="" id="MinRspInvestmentInitialRm" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 100" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Min Rsp Investment Additional RM:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="MinRspInvestmentAdditionalRm" value="" id="MinRspInvestmentAdditionalRm" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 100" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Min Red Amount Units:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="MinRedAmountUnits" value="" id="MinRedAmountUnits" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 1" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Min Holding Units:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="MinHoldingUnits" value="" id="MinHoldingUnits" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 2000" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row mb-6">
                        <div class="col-md-2">
                            <label class="fs-14 mt-2">Colling Off Period:</label>
                        </div>
                        <div class="col-md-10">
                            <textarea name="CollingOffPeriod" rows="5" value="" id="CollingOffPeriod" runat="server" clientidmode="static" class="form-control" placeholder="Enter CollingOffPeriod"></textarea>
                        </div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-md-2">
                            <label class="fs-14 mt-2">Distribution Policy:</label>
                        </div>
                        <div class="col-md-10">
                            <textarea name="DistributionPolicy" rows="5" value="" id="DistributionPolicy" runat="server" clientidmode="static" class="form-control" placeholder="Enter DistributionPolicy"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            <div class="mb-10">
                <label class="label label-default">Fund Charges</label>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Initial Sales Charges Percent:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="InitialSalesChargesPercent" value="" id="InitialSalesChargesPercent" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 5.2632" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">EPF Sales Charges Percent:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="EPFSalesChargesPercent" value="" id="EPFSalesChargesPercent" runat="server" clientidmode="static" class="form-control" placeholder="e.g.5.2632" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Annual Management Charge Percent:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="AnnualManagementChargePercent" value="" id="AnnualManagementChargePercent" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 1.50" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Trustee Fee Percent:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="TrusteeFeePercent" value="" id="TrusteeFeePercent" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 0.05" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Switching Fee Percent:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="SwitchingFeePercent" value="" id="SwitchingFeePercent" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 1" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Redemption Fee Percent:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="RedemptionFeePercent" value="" id="RedemptionFeePercent" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 1.5" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Transfer Fee RM:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="TransferFeeRm" value="" id="TransferFeeRm" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 5" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Other Significant Fee RM:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="OtherSignificantFeeRm" value="" id="OtherSignificantFeeRm" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 5" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-6">
                        <div class="col-md-4">
                            <label class="fs-14 mt-2">Gst Percent:</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="GstPercent" value="" id="GstPercent" runat="server" clientidmode="static" class="form-control" placeholder="e.g. 6" onkeypress="return isNumberKey(event,this.id)" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="mb-10">
                <label class="label label-default">Fund Downloads</label>
            </div>
            <div class="panel-group" id="downloadFileLinks" runat="server">
            </div>
        </div>
    </form>
</body>
</html>
<script>
    $ui(document).ready(function () {

        $(".readPro").prop("readonly", true);
        $('.readddlPro').attr("disabled", true); 


        $ui('.default-date-picker').datepicker({
            format: 'mm/dd/yyyy',
            pickTime: true,
            autoclose: true
        }).on('changeDate', function(e) {
            var date = $('#RelaunchDateDisplay').val();
            $("#RelaunchDate").val(date); 
        });;
    });

    function isNumberKey(evt, id) {
        try {
            var charCode = (evt.which) ? evt.which : event.keyCode;

            if (charCode == 46) {
                var txt = document.getElementById(id).value;
                if (!(txt.indexOf(".") > -1)) {

                    return true;
                }
            }
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        } catch (w) {
            alert(w);
        }
    }
</script>
