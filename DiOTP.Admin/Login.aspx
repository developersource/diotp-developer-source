﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Admin.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <link rel="shortcut icon" href="/Content/images/favicon.html" />
    <title>Login</title>
    <link href="Content/bs3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/css/bootstrap-reset.css" rel="stylesheet" />
    <link href="Content/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="Content/css/style.css" rel="stylesheet" />
    <link href="Content/css/style-responsive.css" rel="stylesheet" />
</head>

<body>

    <form class="form-signin" runat="server">
        <h2 class="form-signin-heading">sign in now</h2>
        <div class="login-wrap">
            <div class="user-login-info">
                <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                <asp:TextBox ID="txtPassword" AutoCompleteType="None" autocomplete="off" runat="server" TextMode="Password" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
            </div>
            <label class="checkbox">
                <asp:CheckBox ID="rememberMe" runat="server" />Remember Me
                <%--                <input type="checkbox" value="remember-me"/>
                Remember me--%>
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal">Forgot Password?</a>
                </span>
            </label>
            <asp:Button CssClass="btn btn-lg btn-login btn-block" Text="Sign in" runat="server" ID="btnSignIn" OnClientClick="return clientValidation()" OnClick="btnSignIn_Click" />
        </div>

        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Forgot Password ?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Enter your e-mail address below to reset your password.</p>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" ClientIDMode="Static" placeholder="Enter your Email ID"></asp:TextBox>
                    </div>
                    <div class="modal-footer">
                        <label class="label label-default pull-left" id="lblInfo" runat="server"></label>
                        <asp:Button ID="btnCancel" runat="server" data-dismiss="modal" class="btn btn-default" Text="Cancel" />
                        <asp:Button ID="btnSubmit" runat="server" class="btn btn-success" Text="Submit" OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnLocalIPAddress" runat="server" Value="0" ClientIDMode="Static" />
        <%--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>--%>
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <%--<script src="Content/js/jquery.js"></script>--%>
        <%--<script src="Content/bs3/js/bootstrap.min.js"></script>--%>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
        <script src="Content/mycj/inline-scripts.js"></script>

    </form>
</body>
</html>
