﻿using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static Admin.ServiceCalls.ServicesManager;
using static DiOTP.Utility.CustomClasses.CustomStatus;

namespace Admin
{
    public partial class AccountOpeningDetails : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IOccupationCodesDefService> lazyOccupationCodesDefServiceObj = new Lazy<IOccupationCodesDefService>(() => new OccupationCodesDefService());

        public static IOccupationCodesDefService IOccupationCodesDefService { get { return lazyOccupationCodesDefServiceObj.Value; } }

        private static readonly Lazy<IRaceDefService> lazyIRaceDefServiceObj = new Lazy<IRaceDefService>(() => new RaceDefService());

        public static IRaceDefService IRaceDefService { get { return lazyIRaceDefServiceObj.Value; } }

        private static readonly Lazy<INationalityDefService> lazyINationalityDefServiceObj = new Lazy<INationalityDefService>(() => new NationalityDefService());

        public static INationalityDefService INationalityDefService { get { return lazyINationalityDefServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            else
            {
                string siteUrl = ConfigurationManager.AppSettings["siteURL"];
                string investorURL = ConfigurationManager.AppSettings["investorURL"];
                String idString = Request.QueryString["id"];
                List<IncomeDTO> incomeDTOs = new List<IncomeDTO>();
                Response responseMonthlyIncome = ServicesManager.GetMonthlyIncomeData();
                if (responseMonthlyIncome.IsSuccess)
                {
                    incomeDTOs = (List<IncomeDTO>)responseMonthlyIncome.Data;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMonthlyIncome.Message + "\", '');", true);
                }
                if (!string.IsNullOrEmpty(idString))
                {
                    int id = Convert.ToInt32(idString);

                    string mainQ = (@"SELECT ao.ID, ao.account_type, ao.principle_id, ao.principle_user_id, ao.joint_relationship, ao.name, ao.mobile_no, ao.submitted_date, ao.id_no, ao.email, ao.settlement_date, ao.us_citizen, ao.is_mobile_verified, 
                                        ao.resident_of_malaysia, ao.nationality, ao.dob, ao.race, ao.bumiputra_status, ao.marital_status, ao.gender, ao.salutation, ao.are_you_pep, ao.pep_status, ao.agent_code, 
                                        ao.f_pep_status, ao.tax_resident_outside_malaysia, ao.tax_residency_status, ao.tin, ao.declaration, ao.remarks, ao.process_status, ao.status, ao.additional_desc,
                                        aobd.agreement, aobd.account_type as bank_account_type, aobd.currency, aobd.bank_id, bd.name as bank_name, aobd.account_name, aobd.account_no, 
                                        aofp.purpose, aofp.source, aofp.employed_by_fund_company, aofp.relationship, aofp.name_of_funder, funders_industory, aofp.is_funder_beneficial_owner, 
                                        aofp.fund_owner_name, aofp.estimated_net_worth, aoo.employed, aoo.occupation, aoo.designation, aoo.employer_name, aoo.nature_of_business, aoo.monthly_income, 
                                        aoo.occupation_others, ao.race_description, aofp.purpose_description FROM account_openings ao 
                                        left join ao_bank_details aobd on ao.ID = aobd.account_opening_id 
                                        join ao_financial_profiles aofp on ao.ID = aofp.account_opening_id 
                                        join ao_occupations aoo on ao.ID = aoo.account_opening_id 
                                        left join banks_def bd on bd.ID = aobd.bank_id 
                                        left join ao_files aof on ao.ID = aof.account_opening_id 
                                        where ao.ID='" + id + "' group by ao.ID ");
                    Response responseUList = GenericService.GetDataByQuery(mainQ, 0, 20, false, null, false, null, false);
                    if (responseUList.IsSuccess)
                    {
                        var UsersDyn = responseUList.Data;
                        var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                        List<DiOTP.Utility.AccountOpeningRequest> aoRequestList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AccountOpeningRequest>>(responseJSON);
                        DiOTP.Utility.AccountOpeningRequest accountOpeningRequest = aoRequestList.FirstOrDefault();

                        StringBuilder asb = new StringBuilder();
                        asb.Append(@"<tr>
                                <td>" + accountOpeningRequest.Name + @"</td>
                                <td>" + accountOpeningRequest.IdNo + @"</td>
                                <td>" + accountOpeningRequest.Salutation + @"</td>
                                <td>" + accountOpeningRequest.Gender + @"</td>
                                <td>" + (accountOpeningRequest.ResidentOfMalaysia == 1 ? "Yes" : "No") + @"</td>
                                <td>" + accountOpeningRequest.Nationality + @"</td>
                                <td>" + (accountOpeningRequest.Race == "Others" ? accountOpeningRequest.RaceDescription : accountOpeningRequest.Race) + @"</td>
                                <td>" + accountOpeningRequest.BumiputraStatus + @"</td>
                                <td>" + accountOpeningRequest.Dob + @"</td>
                                <td>" + accountOpeningRequest.MaritalStatus + @"</td>
                                <td>" + accountOpeningRequest.AgentCode + @"</td>
                            </tr>");
                        accountDetailInfoTbody.InnerHtml = asb.ToString();
                        asb.Clear();

                        Response responseAddr = GenericService.PullData<AccountOpeningAddress>(" account_opening_id='" + accountOpeningRequest.Id + "' ", 0, 0, false, null);
                        if (responseAddr.IsSuccess)
                        {
                            var addrsDyn = responseAddr.Data;
                            var addrsDynJSON = JsonConvert.SerializeObject(addrsDyn);
                            List<DiOTP.Utility.AccountOpeningAddress> accountOpeningAddresses = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AccountOpeningAddress>>(addrsDynJSON);
                            accountOpeningAddresses.ForEach(addr =>
                            {
                                asb.Append(@"<tr>
                                    <td>" + (addr.AddressType == 1 ? "Residential" : (addr.AddressType == 2 ? "Mailing" : (addr.AddressType == 3 ? "Office" : ""))) + @"</td>
                                    <td>" + addr.Addr1 + @",<br/>
                                    " + addr.Addr2 + @",<br/>
                                    " + addr.Addr3 + @"</td>
                                    <td>" + addr.City + @"</td>
                                    <td>" + addr.PostCode + @"</td>
                                    <td>" + addr.State + @"</td>
                                    <td>" + addr.Country + @"</td>
                                    <td>" + addr.TelNo + @"</td>
                                </tr>");
                            });

                            addressTbody.InnerHtml = asb.ToString();
                        }
                        asb.Clear();
                        Response responseOccDesc = GetOccupationDescData(accountOpeningRequest.Occupation);
                        OccupationDTO occupationDTO = new OccupationDTO();
                        if (responseOccDesc.IsSuccess)
                            occupationDTO = (OccupationDTO)responseOccDesc.Data;
                        asb.Append(@"<tr>
                                <td>" + (accountOpeningRequest.Occupation == "Others" ? accountOpeningRequest.OccupationOthers : (occupationDTO != null ? occupationDTO.OCCCODE : accountOpeningRequest.Occupation)) + @"</td>
                                <td>" + (accountOpeningRequest.Occupation == "Others" ? "-" : accountOpeningRequest.NatureOfBusiness) + @"<br/>
                                <td>" + (accountOpeningRequest.Occupation == "Others" ? "-" : (incomeDTOs.FirstOrDefault(m => m.INCOMECODE == accountOpeningRequest.MonthlyIncome) != null ? incomeDTOs.FirstOrDefault(m => m.INCOMECODE == accountOpeningRequest.MonthlyIncome).SDESC : accountOpeningRequest.MonthlyIncome)) + @"</td>
                                <td>" + (accountOpeningRequest.Occupation == "Others" ? "-" : accountOpeningRequest.EmployerName) + @"</td>
                            </tr>");
                        occupationTbody.InnerHtml = asb.ToString();
                        asb.Clear();
                        asb.Append(@"<tr>
                                <td>" + (accountOpeningRequest.Purpose == "Others" ? accountOpeningRequest.PurposeDescription : accountOpeningRequest.Purpose) + @"</td>
                                <td>" + (accountOpeningRequest.Source) + @"<br/>
                                <td>" + (accountOpeningRequest.Source == "Others" ? accountOpeningRequest.NameOfFunder : "N/A") + @"</td>
                                <td>" + (accountOpeningRequest.Source == "Others" ? accountOpeningRequest.Relationship : "N/A") + @"</td>
                                <td>" + (accountOpeningRequest.Source == "Others" ? accountOpeningRequest.FundersIndustory : "N/A") + @"</td>
                                <td>" + (accountOpeningRequest.Source == "Others" ? (accountOpeningRequest.IsFunderMoneyChanger == 0 ? "No" : "Yes") : "N/A") + @"</td>
                                <td>" + (accountOpeningRequest.Source == "Others" ? (accountOpeningRequest.IsFunderBeneficialOwner == 0 ? "No" : "Yes") : "N/A") + @"</td>
                                <td>" + (accountOpeningRequest.Source == "Others" ? accountOpeningRequest.FundOwnerName : "N/A") + @"</td>
                                <td>" + accountOpeningRequest.EstimatedNetWorth + @"</td>
                            </tr>");
                        financialTbody.InnerHtml = asb.ToString();
                        asb.Clear();
                        asb.Append(@"<tr>
                                <td>" + (accountOpeningRequest.AccountName == "" || accountOpeningRequest.AccountName == null ? "N/A" : accountOpeningRequest.AccountName) + @"</td>
                                <td>" + (accountOpeningRequest.AccountNo == "" || accountOpeningRequest.AccountNo == null ? "N/A" : accountOpeningRequest.AccountNo) + @"<br/>
                                <td>" + (accountOpeningRequest.BankName == "" || accountOpeningRequest.BankName == null ? "N/A" : accountOpeningRequest.BankName) + @"<br/>
                                <td>" + (accountOpeningRequest.BankAccountType == "" || accountOpeningRequest.BankAccountType == null ? "N/A" : accountOpeningRequest.BankAccountType) + @"</td>
                                <td>" + (accountOpeningRequest.Currency == "" ? "N/A" : accountOpeningRequest.Currency) + @"</td>
                            </tr>");
                        bankDetailsTbody.InnerHtml = asb.ToString();
                        asb.Clear();
                        asb.Append(@"<tr>
                                <td>" + (accountOpeningRequest.AreYouPep == 1 ? accountOpeningRequest.PepStatus : "No PEP") + @"</td>
                                <td>" + (accountOpeningRequest.AreYouPep == 1 ? accountOpeningRequest.FPepStatus : "N/A") + @"<br/>
                            </tr>");
                        pepTbody.InnerHtml = asb.ToString();
                        asb.Clear();
                        Response responseCRS = GenericService.PullData<AccountOpeningCRSDetail>(" account_opening_id='" + accountOpeningRequest.Id + "' ", 0, 0, false, null);
                        if (responseCRS.IsSuccess)
                        {
                            var crsDyn = responseCRS.Data;
                            var crsDynJSON = JsonConvert.SerializeObject(crsDyn);
                            List<DiOTP.Utility.AccountOpeningCRSDetail> accountOpeningCRSDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AccountOpeningCRSDetail>>(crsDynJSON);
                            accountOpeningCRSDetails.ForEach(crs =>
                            {
                                asb.Append(@"<tr>
                                                <td>" + (crs.TaxResidencyStatus == "" ? "N/A" : crs.TaxResidencyStatus) + @"<br/>
                                                <td>" + (crs.TaxResidencyStatus == "" ? "N/A" : (crs.Tin == "" ? crs.NoTinReason : crs.Tin)) + @"</td>
                                            </tr>");
                            });
                            if (accountOpeningCRSDetails.Count == 0)
                            {
                                asb.Append(@"<tr>
                                                <td>" + "No CRS" + @"<br/>
                                                <td>" + "N/A" + @"</td>
                                            </tr>");

                            }
                            crsTbody.InnerHtml = asb.ToString();
                            asb.Clear();
                        }
                        Response responseFile = GenericService.PullData<AccountOpeningFile>(" account_opening_id='" + accountOpeningRequest.Id + "' and status=1 order by FIELD(file_type, '1', '2', '3', '4', '6', '5', '7')", 0, 0, false, null);
                        if (responseFile.IsSuccess)
                        {
                            var filesDyn = responseFile.Data;
                            var filesDynJSON = JsonConvert.SerializeObject(filesDyn);
                            List<DiOTP.Utility.AccountOpeningFile> accountOpeningFiles = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AccountOpeningFile>>(filesDynJSON);
                            List<Int32> ReuploadFileTypes = new List<int>();
                            try
                            {
                                ReuploadFileTypes = accountOpeningRequest.AdditionalDesc != "" ? accountOpeningRequest.AdditionalDesc.Split(',').Select(x => Convert.ToInt32(x)).ToList() : new List<int>();
                            }
                            catch (Exception ex)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + ex.Message + "\");", true);
                            }
                            accountOpeningFiles.ForEach(file =>
                            {
                                asb.Append(@"<tr>
                                                <td>" + (file.FileType == 1 ? "NRIC Front" : (file.FileType == 2 ? "NRIC Back" : (file.FileType == 3 ? "Selfie" : (file.FileType == 4 ? "Signature" : (file.FileType == 5 ? "Additional" : (file.FileType == 6 ? "Supporting" : (file.FileType == 7 ? "Additional Document Requested by Admin" : ""))))))) + @"<br/>
                                                <td>" + file.CreatedDate + @"</td>
                                                <td><a target='_blank' href='" + (investorURL.TrimEnd('/') + file.Url) + @"'>View <i class='fa fa-eye'></i></a> " + (accountOpeningRequest.ProcessStatus == (int)AOProcessStatus.UnclearDocsRequested && ReuploadFileTypes.Contains(file.FileType) ? " (Requested to re-upload)" : (accountOpeningRequest.ProcessStatus == (int)AOProcessStatus.UnclearDocsUploaded && ReuploadFileTypes.Contains(file.FileType) ? " (Re-uploaded)" : "")) + @"</td>
                                            </tr>");
                            });
                            documentsTbody.InnerHtml = asb.ToString();
                        }

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                            "alert('" + responseUList.Message + "');", true);
                    }
                }
            }

        }
    }
}