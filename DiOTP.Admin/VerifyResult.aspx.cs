﻿using Admin.ServiceCalls;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class VerifyResult : System.Web.UI.Page
    {
        private static readonly Lazy<IAccountOpeningService> lazyIAccountOpeningServiceObj = new Lazy<IAccountOpeningService>(() => new AccountOpeningService());

        public static IAccountOpeningService IAccountOpeningService { get { return lazyIAccountOpeningServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            else
            {
                String idString = Request.QueryString["id"];

                if (!string.IsNullOrEmpty(idString))
                {
                    int id = Convert.ToInt32(idString);

                    Response responseUList = IAccountOpeningService.GetSingle(id);
                    if (responseUList.IsSuccess)
                    {
                        AccountOpening accountOpening = (AccountOpening)responseUList.Data;
                        if (accountOpening.IsIdentityVerified == 1)
                        {
                            var plainTextBytes = Convert.FromBase64String(accountOpening.IdentityStatus);
                            var result = Encoding.UTF8.GetString(plainTextBytes);
                            jsonResult.InnerHtml = result;
                            Trulioo.Client.V1.Model.VerifyResult verifyResult = JsonConvert.DeserializeObject<Trulioo.Client.V1.Model.VerifyResult>(result);
                            var res = JsonConvert.SerializeObject(verifyResult, Formatting.Indented);
                            jsonResult.InnerHtml = res;
                            StringBuilder stringBuilder = new StringBuilder();
                            int idx = 1;
                            Response resTR = DigitalizationServiceManager.GetTransactionRecord("", verifyResult.Record.TransactionRecordID);
                            if (resTR.IsSuccess)
                            {
                                Trulioo.Client.V1.Model.TransactionRecordResult tr = (Trulioo.Client.V1.Model.TransactionRecordResult)resTR.Data;
                                tr.InputFields.ForEach(x =>
                                {
                                    string fieldName = x.FieldName;
                                    string value = x.Value;
                                    stringBuilder.Append(@"<tr>
                                        <td>" + idx + @"</td>
                                        <td>" + fieldName + @"</td>
                                        <td>" + value + @"</td>
                                    </tr>");
                                    idx++;
                                });
                                tbodyInput.InnerHtml = stringBuilder.ToString();
                            }
                            if (verifyResult.Record.RecordStatus == "match")
                            {
                                verifyStatus.InnerHtml = "Verified on " + verifyResult.UploadedDt.ToLocalTime().ToString("dd/MM/yyyy hh:mm:ss");
                            }
                            else
                            {
                                verifyStatus.InnerHtml = "Verification failed on " + verifyResult.UploadedDt.ToLocalTime().ToString("dd/MM/yyyy hh:mm:ss");
                            }
                            stringBuilder = new StringBuilder();
                            idx = 1;
                            verifyResult.Record.DatasourceResults.ToList().ForEach(dr =>
                            {
                                string dataSourceName = dr.DatasourceName;
                                dr.DatasourceFields.ToList().ForEach(df =>
                                {
                                    string fieldName = df.FieldName;
                                    string status = df.Status;
                                    stringBuilder.Append(@"<tr>
                                        <td>" + idx + @"</td>
                                        <td>" + dataSourceName + @"</td>
                                        <td>" + fieldName + @"</td>
                                        <td>" + status + @"</td>
                                    </tr>");
                                    idx++;
                                });
                                dr.AppendedFields.ToList().ForEach(af =>
                                {
                                    string fieldName = af.FieldName;
                                    string Data = af.Data;
                                    stringBuilder.Append(@"<tr>
                                        <td>" + idx + @"</td>
                                        <td>" + dataSourceName + @"</td>
                                        <td>" + fieldName + @"</td>
                                        <td>" + Data + @"</td>
                                    </tr>");
                                    idx++;
                                });
                            });

                            VerifyResultJson.Value = "";

                            tbodyResult.InnerHtml = stringBuilder.ToString();
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseUList.Message + "\");", true);
                    }
                }
            }
        }
    }
}