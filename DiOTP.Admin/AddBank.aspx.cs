﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AddBank : System.Web.UI.Page
    {
        private static readonly Lazy<IBanksDefService> lazyBanksDefServiceObj = new Lazy<IBanksDefService>(() => new BanksDefService());
        public static IBanksDefService IBanksDefService { get { return lazyBanksDefServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            string idString = Request.QueryString["id"];
            if (idString != null && idString != "")
            {
                int id = Convert.ToInt32(idString);
                Response response = IBanksDefService.GetSingle(id);
                if (response.IsSuccess)
                {
                    BanksDef banksDef = (BanksDef)response.Data;

                    Id.Value = banksDef.Id.ToString();
                    Status.Value = banksDef.Status.ToString();

                    ISIN.Value = banksDef.Isin.ToString();
                    Code.Value = banksDef.Code.ToString();
                    ShortName.Value = banksDef.ShortName.ToString();
                    Name.Value = banksDef.Name.ToString();
                    //NoFormat.Value = banksDef.NoFormat.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                        "alert('" + response.Message + "');", true);
                }
            }
        }
    }
}