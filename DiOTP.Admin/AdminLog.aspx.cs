﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Admin
{
    public partial class AdminLog : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());

        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyIAdminLogSuberviceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());

        public static IAdminLogSubService IAdminLogSubService { get { return lazyIAdminLogSuberviceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder filter = new StringBuilder();
            string mainQ = (@"SELECT ulm.ID as c, ulm.ID, ulm.table_name, ulm.description, ulm.user_id, u.username, u.id_no, ulm.ref_id, ulm.ref_value, ut.user_type_id, ulm.updated_date, uls.column_name, uls.value_old, uls.value_new FROM ");
            string mainQCount = (@"select count(a.c) from (");
            filter.Append(@" admin_log_main ulm 
                            left join admin_log_sub uls on uls.admin_log_main_id = ulm.id
                            left join users u on u.id = ulm.user_id 
                            left join user_types ut on ut.user_id = u.id where 1=1 ");

            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                User loginUser = (User)HttpContext.Current.Session["admin"];
                bool isMainAdmin = false;
                if (Session["isMainAdmin"] != null && Session["isMainAdmin"].ToString() == "1")
                    isMainAdmin = true;

                if (Request.QueryString["hdnNumberPerPage"] != "" && Request.QueryString["hdnNumberPerPage"] != null)
                {
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();
                }
                if (Request.QueryString["hdnCurrentPageNo"] != "" && Request.QueryString["hdnCurrentPageNo"] != null)
                {
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();
                }
                if (Request.QueryString["hdnTotalRecordsCount"] != "" && Request.QueryString["hdnTotalRecordsCount"] != null)
                {
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FromDate"]))
                {
                    FromDate.Value = Request.QueryString["FromDate"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ToDate"]))
                {
                    ToDate.Value = Request.QueryString["ToDate"].ToString();
                }

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                if (Request.QueryString["IsNewSearch"] != "" && Request.QueryString["IsNewSearch"] != null)
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (Request.QueryString["Search"] != "" && Request.QueryString["Search"] != null)
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and (u.username like '%" + Search.Value.Trim() + "%'");
                    
                    filter.Append(" or ulm.description like '%" + Search.Value.Trim() + "%') ");
                }

                if (Request.QueryString["Module"] != "" && Request.QueryString["Module"] != null)
                {
                    Module.Value = Request.QueryString["Module"].ToString();
                    if (Module.Value == "1")
                    {
                        filter.Append(" and ulm.table_name in ('fund_info', 'fund_returns', 'fund_chart_info', 'utmc_daily_nav_fund', 'utmc_fund_categories_def', 'utmc_fund_charges', 'utmc_fund_corporate_actions', 'utmc_fund_details', 'utmc_fund_file_types_def', 'utmc_fund_files', 'utmc_fund_information') ");
                    }
                    if (Module.Value == "2")
                    {
                        //filter.Append(" and ulm.table_name in ('user_accounts', 'user_securities', 'user_statements', 'users') ");
                    }
                    if (Module.Value == "3")
                    {
                        filter.Append(" and ulm.table_name in ('banks_def') ");
                    }
                    if (Module.Value == "4")
                    {
                        filter.Append(" and ulm.table_name in ('site_content') ");
                    }
                    if (Module.Value == "5")
                    {
                        filter.Append(" and ulm.table_name in ('site_content') ");
                    }
                    if (Module.Value == "6")
                    {
                        filter.Append(" and ulm.table_name in ('users') and ut.user_type_id in ('3', '4', '5') ");
                    }
                }

                if (!string.IsNullOrEmpty(FromDate.Value) && !string.IsNullOrEmpty(ToDate.Value))
                {
                    DateTime from = DateTime.ParseExact(FromDate.Value.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime to = DateTime.ParseExact(ToDate.Value.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    if (FromDate.Value.Trim() == ToDate.Value.Trim())
                    {
                        to = to.AddDays(1);
                    }
                    else if (from > to)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                                "alert('From date cannot be greater than To date.');", true);
                    }
                    filter.Append(" and ulm.updated_date between '" + from.ToString("yyyy-MM-dd") + "' and '" + to.ToString("yyyy-MM-dd") + "'");
                }

                if (isMainAdmin == true)
                {
                    filter.Append(" and (ut.user_type_id in ('4','5') or (ut.user_type_id in ('3') and u.id in ('" + loginUser.Id + "'))) ");

                }

                filter.Append("group by ulm.ID  order by ulm.updated_date desc ");

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + mainQ + filter.ToString() + ") a").Data.ToString();
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseULMList = GenericService.GetDataByQuery(mainQ + filter.ToString(), skip, take, false, null, false, null, true);
                
                if (responseULMList.IsSuccess)
                {
                    var UlmsDyn = responseULMList.Data;
                    var responseJSON = JsonConvert.SerializeObject(UlmsDyn);
                    List<AuditLog> userLogMains = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AuditLog>>(responseJSON);

                    StringBuilder asb = new StringBuilder();
                    int index = 1;

                    foreach (AuditLog ulm in userLogMains)
                    {
                        Int32 ChildCount = IAdminLogSubService.GetCountByPropertyName(nameof(AdminLogSub.AdminLogMainId), ulm.ID.ToString(), true);
                        asb.Append(@"<tr>
                                        <td>" + index + @"</td>
                                        <td>" + ulm.username + @"</td>
                                        <td>" + ulm.description + @"</td>
                                        <td>" + ulm.updated_date.ToString("dd/MM/yyyy HH:mm:ss") + @"</td>
                                        <td style='text-align:center'>
                                            " + (ChildCount == 0 ? "-" : "<a href='javascript:;' data-id='" + ulm.ID + @"' data-original-title='View Detail' data-trigger='hover' data-placement='bottom' class='popovers action-button' data-url='ViewAdminLogSub.aspx' data-title='View details' data-action='View'><i class='fa fa-eye'></i></a>") + @"
                                        </td>
                                    </tr>");

                        index++;
                    }
                    changeLogTbody.InnerHtml = asb.ToString();
                }
            }
        }
    }
}