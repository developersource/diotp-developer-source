﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AddFundDetails : System.Web.UI.Page
    {
        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundDetailService> lazyUtmcFundDetailServiceObj = new Lazy<IUtmcFundDetailService>(() => new UtmcFundDetailService());

        public static IUtmcFundDetailService IUtmcFundDetailService { get { return lazyUtmcFundDetailServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundChargeService> lazyUtmcFundChargeServiceObj = new Lazy<IUtmcFundChargeService>(() => new UtmcFundChargeService());

        public static IUtmcFundChargeService IUtmcFundChargeService { get { return lazyUtmcFundChargeServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundFileService> lazyUtmcFundFileServiceObj = new Lazy<IUtmcFundFileService>(() => new UtmcFundFileService());

        public static IUtmcFundFileService IUtmcFundFileService { get { return lazyUtmcFundFileServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundFileTypesDefService> lazyUtmcFundFileTypesDefServiceObj = new Lazy<IUtmcFundFileTypesDefService>(() => new UtmcFundFileTypesDefService());

        public static IUtmcFundFileTypesDefService IUtmcFundFileTypesDefService { get { return lazyUtmcFundFileTypesDefServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundCategoriesDefService> lazyUtmcFundCategoriesDefServiceObj = new Lazy<IUtmcFundCategoriesDefService>(() => new UtmcFundCategoriesDefService());

        public static IUtmcFundCategoriesDefService IUtmcFundCategoriesDefService { get { return lazyUtmcFundCategoriesDefServiceObj.Value; } }

        private static readonly Lazy<IFundInfoService> lazyFundInfoObj = new Lazy<IFundInfoService>(() => new FundInfoService());

        public static IFundInfoService IFundInfoService { get { return lazyFundInfoObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=Index.aspx'", true);
            }
            string currentDateString = DateTime.Now.ToString("MM/dd/yyyy");
            EpfIpdCode.Value = "IPD033";
            EffectiveDate.Value = currentDateString;
            ReportDate.Value = currentDateString;

            Response responseUFCD = IUtmcFundCategoriesDefService.GetDataByFilter(" status='1'", 0, 0, false);
            if (responseUFCD.IsSuccess)
            {
                List<UtmcFundCategoriesDef> utmcFundCategoriesDefs = (List<UtmcFundCategoriesDef>)responseUFCD.Data;
                UtmcFundCategoriesDefId.DataSource = utmcFundCategoriesDefs;
                UtmcFundCategoriesDefId.DataTextField = "Name";
                UtmcFundCategoriesDefId.DataValueField = "Id";
                UtmcFundCategoriesDefId.DataBind();
            }

            string idString = Request.QueryString["id"];
            if (idString != null && idString != "")
            {
                int id = Convert.ToInt32(idString);
                Session["idString"] = id;
                Response response = IUtmcFundInformationService.GetSingle(id);
                if (response.IsSuccess)
                {
                    UtmcFundInformation obj = (UtmcFundInformation)response.Data;

                    Id.Value = obj.Id.ToString();
                    FundCode.Value = obj.FundCode;
                    IpdFundCode.Value = obj.IpdFundCode;
                    FundName.Value = obj.FundName.Capitalize();
                    //
                    UtmcFundCategoriesDefId.Value = obj.UtmcFundCategoriesDefId.ToString();
                    LipperCategoryOfFund.Value = obj.LipperCategoryOfFund;
                    //
                    Conventional.Value = obj.Conventional;
                    //
                    Status.Value = obj.Status;
                    ForeignFund.Value = obj.ForeignFund;
                    FundBaseCurrency.Value = obj.FundBaseCurrency;
                    //
                    IsEmis.Value = obj.IsEmis.ToString();
                    FundCls.Value = obj.FundCls;
                    //
                    IsRetail.Value = obj.IsRetail.ToString();
                    SatGroup.Value = obj.SatGroup;
                    IsRsp.Value = obj.IsRsp.ToString();

                    ////FUND DETAILS
                    Response responseUFD = IUtmcFundDetailService.GetDataByFilter(" utmc_fund_information_id='" + id + "'", 0, 0, false);
                    if (responseUFD.IsSuccess)
                    {
                        List<UtmcFundDetail> objUFDs = (List<UtmcFundDetail>)responseUFD.Data;
                        if (objUFDs.Count > 0)
                        {
                            String propName = nameof(UtmcFundInformation.FundCode);
                            Response responseFIList = IFundInfoService.GetDataByPropertyName(propName, obj.IpdFundCode, true, 0, 0, false);
                            if (responseFIList.IsSuccess)
                            {
                                FundInfo fundInfo = ((List<FundInfo>)responseFIList.Data).FirstOrDefault();
                                UtmcFundDetail objUFD = objUFDs.First();
                                UtmcFundInformationId.Value = objUFD.UtmcFundInformationId.ToString();
                                LaunchDate.Value = objUFD.LaunchDate.ToString("MM/dd/yyyy");
                                RelaunchDateDisplay.Value = (objUFD.RelaunchDate == null || objUFD.RelaunchDate == default(DateTime) ? "N/A" : objUFD.RelaunchDate.Value.ToString("MM/dd/yyyy"));
                                RelaunchDate.Value = objUFD.RelaunchDate.Value.ToString("MM/dd/yyyy");
                                LaunchPrice.Value = objUFD.LaunchPrice.ToString();
                                PricingBasis.Value = objUFD.PricingBasis;
                                InvestmentObjective.Value = objUFD.InvestmentObjective;
                                InvestmentStrategyAndPolicy.Value = objUFD.InvestmentStrategyAndPolicy;
                                if (fundInfo != null)
                                {
                                    LatestNavPrice.Value = fundInfo.CurrentUnitPrice.ToString();
                                }
                                //
                                HistoricalIncomeDistribution.Value = objUFD.HistoricalIncomeDistribution.ToString();
                                //
                                IsEpfApproved.Value = objUFD.IsEpfApproved.ToString();
                                //
                                ShariahCompliant.Value = objUFD.ShariahCompliant.ToString();
                                RiskRating.Value = objUFD.RiskRating;
                                FundSizeRm.Value = objUFD.FundSizeRm;
                                MinInitialInvestmentCash.Value = objUFD.MinInitialInvestmentCash.ToString();
                                MinInitialInvestmentEpf.Value = objUFD.MinInitialInvestmentEpf.ToString();
                                MinSubsequentInvestmentCash.Value = objUFD.MinSubsequentInvestmentCash.ToString();
                                MinSubsequentInvestmentEpf.Value = objUFD.MinSubsequentInvestmentEpf.ToString();
                                MinRspInvestmentInitialRm.Value = objUFD.MinRspInvestmentInitialRm.ToString();
                                MinRspInvestmentAdditionalRm.Value = objUFD.MinRspInvestmentAdditionalRm.ToString();
                                MinRedAmountUnits.Value = objUFD.MinRedAmountUnits.ToString();
                                MinHoldingUnits.Value = objUFD.MinHoldingUnits.ToString();
                                CollingOffPeriod.Value = objUFD.CollingOffPeriod;
                                DistributionPolicy.Value = objUFD.DistributionPolicy;
                            }
                        }
                    }

                    ////FUND CHARGES
                    Response responseUFC = IUtmcFundChargeService.GetDataByFilter(" utmc_fund_information_id='" + id + "'", 0, 0, false);
                    if (responseUFC.IsSuccess)
                    {
                        List<UtmcFundCharge> objUFCs = (List<UtmcFundCharge>)responseUFC.Data;
                        if (objUFCs.Count > 0)
                        {
                            UtmcFundCharge objUFC = objUFCs.First();
                            InitialSalesChargesPercent.Value = objUFC.InitialSalesChargesPercent.ToString();
                            EPFSalesChargesPercent.Value = objUFC.EpfSalesChargesPercent.ToString();
                            AnnualManagementChargePercent.Value = objUFC.AnnualManagementChargePercent.ToString();
                            TrusteeFeePercent.Value = objUFC.TrusteeFeePercent.ToString();
                            SwitchingFeePercent.Value = objUFC.SwitchingFeePercent.ToString();
                            RedemptionFeePercent.Value = objUFC.RedemptionFeePercent.ToString();
                            TransferFeeRm.Value = objUFC.TransferFeeRm.ToString();
                            OtherSignificantFeeRm.Value = objUFC.OtherSignificantFeeRm.ToString();
                            GstPercent.Value = objUFC.GstPercent.ToString();
                        }
                    }

                    Response responseFFT = IUtmcFundFileTypesDefService.GetDataByFilter(" status='1'", 0, 0, false);
                    if (responseFFT.IsSuccess)
                    {
                        List<UtmcFundFileTypesDef> utmcFundFileTypesDefs = (List<UtmcFundFileTypesDef>)responseFFT.Data;
                        UtmcFundFileTypesDef utmcFundFileTypesDefOther = utmcFundFileTypesDefs.FirstOrDefault(x => x.Name == "Others");
                        utmcFundFileTypesDefs = utmcFundFileTypesDefs.Where(x => x.Name != "Others").ToList();
                        StringBuilder sbDownloadLinks = new StringBuilder();

                        Response responseFF = IUtmcFundFileService.GetDataByFilter(" utmc_fund_information_id='" + id + "' and status='1'", 0, 0, false);
                        if (responseFF.IsSuccess)
                        {
                            List<UtmcFundFile> files = (List<UtmcFundFile>)responseFF.Data;
                            int index = 1;
                            utmcFundFileTypesDefs.ForEach(uftd =>
                            {
                                UtmcFundFile f = files.FirstOrDefault(fl => fl.UtmcFundFileTypesDefId == uftd.Id);
                                sbDownloadLinks.Append(@"<div class='panel'>
                                                    <div class='panel-heading'>
                                                         " + index + @". 
                                                        " + (f != null ? "<a href='" + f.Url + @"' target='_blank'>Click here for " + f.Name + @"</a>" : uftd.Name + " not uploaded") + @"                                                       
                                                        <a href='javascript:;' class='tab-menu-item btn btn-success btn-sm pull-right' data-innerId='" + (f != null ? f.Id : 0 ) + @"' data-fileTypeId='" + uftd.Id + @"' data-id='" + id + @"' data-isModal='1' data-showModal='1' data-modalSize='M' data-url='EditFundDownloads.aspx' data-title='Edit'><i class='fa fa-"+ (f != null ? "edit" : "plus") + @"'></i></a>
                                                        " + (f != null ? "<a href='javascript:;' class='tab-menu-item btn btn-danger btn-sm pull-right' data-innerId='" + (f != null ? f.Id : 0) + @"' data-fileTypeId='" + uftd.Id + @"' data-id='" + id + @"' data-isModal='1' data-showModal='1' data-modalSize='M' data-url='EditFundDownloads.aspx' data-title='Delete'><i class='fa fa-times'></i></a>" : "") + @"
                                                    </div>
                                                </div>");
                                index++;
                            });

                            
                            List<UtmcFundFile> otherFiles = files.Where(x=>x.UtmcFundFileTypesDefId == utmcFundFileTypesDefOther.Id).ToList();
                            if (otherFiles.Count > 0)
                            {
                                otherFiles.ForEach(f =>
                                {
                                    //UtmcFundFile f = files.First();
                                    sbDownloadLinks.Append(@"<div class='panel'>
                                                    <div class='panel-heading'>
                                                         " + index + @". 
                                                        <a href='"+ f.Url + @"' target='_blank'>Click here for " + f.Name + @"</a>
                                                        <a href='javascript:;' class='tab-menu-item btn btn-success btn-sm pull-right' data-innerId='" + f.Id + @"' data-fileTypeId='" + f.UtmcFundFileTypesDefId + @"' data-id='" + id + @"' data-isModal='1' data-showModal='1' data-modalSize='M' data-url='EditFundDownloads.aspx' data-title='Edit'><i class='fa fa-edit'></i></a>
                                                        <a href='javascript:;' class='tab-menu-item btn btn-danger btn-sm pull-right' data-innerId='" + f.Id + @"' data-fileTypeId='" + f.UtmcFundFileTypesDefId + @"' data-id='" + id + @"' data-isModal='1' data-showModal='1' data-modalSize='M' data-url='EditFundDownloads.aspx' data-title='Delete'><i class='fa fa-times'></i></a>
                                                    </div>
                                                </div>");
                                    index++;
                                });
                            }

                            sbDownloadLinks.Append(@"<div class='panel'>
                                                    <div class='panel-heading'>
                                                         " + index + @". 
                                                        Add Others
                                                        <a href='javascript:;' class='tab-menu-item btn btn-success btn-sm pull-right' data-innerId='0' data-fileTypeId='5' data-id='" + id + @"' data-isModal='1' data-showModal='1' data-modalSize='M' data-url='EditFundDownloads.aspx' data-title='Add Fund Downloads'><i class='fa fa-plus'></i></a>
                                                    </div>
                                                </div>");
                            
                        }


                        downloadFileLinks.InnerHtml = sbDownloadLinks.ToString();
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                        "alert('" + response.Message + "');", true);
                }
            }
        }
    }
}