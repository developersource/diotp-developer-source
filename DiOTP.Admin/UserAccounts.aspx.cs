﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using OfficeOpenXml;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;

namespace Admin
{
    public partial class UserAccounts : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        private static readonly Lazy<IUserTypeService> lazyUserTypeServiceObj = new Lazy<IUserTypeService>(() => new UserTypeService());

        public static IUserTypeService IUserTypeService { get { return lazyUserTypeServiceObj.Value; } }

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder filter = new StringBuilder();
            string mainQ = (@"SELECT u.ID as c, u.ID, u.username, u.mobile_number, u.created_date, u.id_no, ua.account_no, u.email_id, u.last_login_date, u.status, u.is_login_locked FROM ");
            string mainQCount = (@"select count(a.c) from (");
            filter.Append(@"users u join user_accounts ua 
                            on u.ID = ua.user_id and ua.holder_class not in ('RC','BC','NC','FC') join user_types t on u.ID = t.user_id 
                             where 1=1 ");
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                User user = (User)Session["admin"];
                Response response = IUserTypeService.GetDataByFilter("user_id = '" + user.Id + "'", 0, 0, false);
                UserType loginUserType = new UserType();
                if (response.IsSuccess)
                {
                    loginUserType = ((List<UserType>)response.Data).FirstOrDefault();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                //searching method here
                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }

                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }

                //filter based on the name user input
                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and (u.username like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or u.email_id like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ua.account_no like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or u.id_no like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or u.mobile_number like '%" + Search.Value.Trim() + "%')");
                }

                //filter based on the date of the user input based on the registration date of users
                if (!string.IsNullOrEmpty(Request.QueryString["FromDate"]) && !string.IsNullOrEmpty(Request.QueryString["ToDate"])) {

                    FromDate.Value = Request.QueryString["FromDate"].ToString();
                    string fromDate = DateTime.ParseExact(FromDate.Value, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");

                    ToDate.Value = Request.QueryString["ToDate"].ToString();
                    string toDate = DateTime.ParseExact(ToDate.Value, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                    filter.Append(" and u.created_date >= '" + fromDate + "'" );
                    filter.Append(" and u.created_date <= '" + toDate + "'");

                }

                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    filter.Append("and u.status in ('" + FilterValue.Value + "')");
                }
                else
                {
                    filter.Append(@"and u.status in ('0','1') ");
                }

                filter.Append("group by u.ID ");

                if (Request.QueryString["hdnOrderByColumn"] != "" && Request.QueryString["hdnOrderByColumn"] != null && Request.QueryString["hdnOrderByColumn"].Trim() != "-")
                {
                    hdnOrderByColumn.Value = Request.QueryString["hdnOrderByColumn"].ToString();
                    hdnOrderType.Value = Request.QueryString["hdnOrderType"].ToString();
                    filter.Append(" order by " + hdnOrderByColumn.Value + " " + hdnOrderType.Value + " ");
                }
                else
                    filter.Append("order by u.status desc, u.created_date desc ");

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);
                
                
                
                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + mainQ + filter.ToString() + ") a").Data.ToString();
                    
                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }
                
                Response responseUList = GenericService.GetDataByQuery(mainQ + filter.ToString() + "", skip, take, false, null, false, null, true);
                if (responseUList.IsSuccess)
                {
                    var UsersDyn = responseUList.Data;
                    var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                    List<IndividualAccounts> userslist = Newtonsoft.Json.JsonConvert.DeserializeObject<List<IndividualAccounts>>(responseJSON);
                    

                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    foreach (IndividualAccounts u in userslist)
                    {
                        string siteURL = ConfigurationManager.AppSettings["siteURL"];
                        string impersonateURL = ConfigurationManager.AppSettings["impersonateURL"];
                        Response responseUA = IUserAccountService.GetDataByFilter(" user_id='" + u.id + "' ", 0, 0, false);
                        if (responseUA.IsSuccess)
                        {
                            List<UserAccount> userAccounts = (List<UserAccount>)responseUA.Data;
                            asb.Append(@"<tr>
                                <td class='icheck'>
                                    <div class='square single-row'>
                                        <div class='checkbox'>
                                            <input type='checkbox' name='checkRow' class='checkRow' value='" + u.id + @"' /> <label>" + index + @"</label><br/>
                                        </div>
                                    </div>
                                    <span class='row-status'>" + (u.status == 1 ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Inactive</span>") + @"</span>
                                    <div class='" + (u.is_login_locked == 1 ? "lock-status" : "" ) + @"'>
                                        <strong>" + (u.is_login_locked == 1 ? "<a class='text-danger' data-toggle='tooltip' title='Locked'><i class='fa fa-lock'></i></a>" : "") + @"</strong>
                                    </div>
                                </td>
                                <td>
                                    <table class='table'>
                                        <tbody>
                                            <tr>
                                                <td style='width:50%;'>Username</td>
                                                <td><strong><a href='javascript:;' data-id='" + u.id + @"' data-original-title='View User Details' data-trigger='hover' data-placement='bottom' class='popovers action-button' data-url='UserDetails.aspx' data-title='View details' data-action='View'>" + u.username + @"&nbsp;<i class='fa fa-eye'></i></a></strong></td>
                                            </tr>
                                            <tr>
                                                <td>Mobile number</td>
                                                <td><strong>" + u.mobile_number + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Registered Date</td>
                                                <td><strong>" + (u.created_date == null ? "" : u.created_date.ToString("dd/MM/yyyy")) + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>ID No</td>
                                                <td><strong>" + u.id_no + @"</strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <table class='table'>
                                        <tbody>
                                            <tr>
                                                <td style='width:50%;'>MA Acc No</td>
                                                <td><strong>" + String.Join(", ", userAccounts.Select(x => x.AccountNo).ToArray()) + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><strong>" + u.email_id + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Last login</td>
                                                <td><strong>" + (u.last_login_date == null ? "" : u.last_login_date.ToString("dd/MM/yyyy HH:mm:ss")) + @"</strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td><a href='" + impersonateURL + "ImpersonateUser.aspx?id=" + CustomEncryptorDecryptor.EncryptText(u.id.ToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff")) + @"' target='_blank' class='btn btn-sm btn-primary'>Impersonate User</a></td>
                            </tr>");
                            index++;
                        }
                    }
                    usersTbody.InnerHtml = asb.ToString();
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            try
            {
                string idString = String.Join(",", ids);
                Response responseUList = IUserService.GetDataByFilter(" ID in (" + idString + ")", 0, 0, false);
                if (responseUList.IsSuccess)
                {
                    List<User> users = (List<User>)responseUList.Data;
                    if (action == "Deactivate")
                    {
                        users.ForEach(x =>
                        {
                            x.Status = 0;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = "Individual Account deactivation Successful",
                                TableName = "users",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "status",
                                ValueOld = "1",
                                ValueNew = "0",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);

                            //Audit Log Ends here
                        });
                        IUserService.UpdateBulkData(users);
                    }
                    if (action == "Activate")
                    {
                        users.ForEach(x =>
                        {
                            x.Status = 1;
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = "Individual Account activation Successful",
                                TableName = "users",
                                UpdatedDate = DateTime.Now,
                                UserId = loginUser.Id
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            alm = (AdminLogMain)responseLog.Data;
                            AdminLogSub als = new AdminLogSub()
                            {
                                AdminLogMainId = alm.Id,
                                ColumnName = "status",
                                ValueOld = "0",
                                ValueNew = "1",
                            };
                            Response response2 = IAdminLogSubService.PostData(als);
                            //Audit Log Ends here
                        });
                        IUserService.UpdateBulkData(users);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("User accounts action: " + ex.Message);
                return false;
            }
        }

        public static Response IsValidPhoneNumber(string phoneNumber)
        {
            Response response = new Response();
            try
            {
                //will match +61 or +66- or 0 or nothing followed by a nine digit number
                bool isValid = Regex.Match(phoneNumber,
                    @"^(\+?6?01)[0-46-9]-*[0-9]{7,8}$").Success;
                //to vary this, replace 61 with an international code of your choice 
                //or remove [\+]?61[-]? if international code isn't needed
                //{8} is the number of digits in the actual phone number less one
                if (isValid)
                {
                    response.IsSuccess = true;
                    response.Message = "Success";
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Invalid mobile number";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(User obj)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];

            Response response = new Response();

            if (obj.EmailId == "")
            {
                response.IsSuccess = false;
                response.Message = "Email cannot be empty";
                return response;
            }
            else
            {
                if (!CustomValidator.IsValidEmail(obj.EmailId))
                {
                    response.IsSuccess = false;
                    response.Message = "Invalid email";
                    return response;
                }
            }
            if (obj.MobileNumber == "")
            {
                response.IsSuccess = false;
                response.Message = "Mobile number cannot be empty";
                return response;
            }
            else
            {
                response = IsValidPhoneNumber(obj.MobileNumber);
                if (!response.IsSuccess)
                {
                    return response;
                }
            }

            Int32 LoginUserId = 0;
            if (HttpContext.Current.Session["admin"] != null)
            {
                User sessionUser = (User)HttpContext.Current.Session["admin"];
                LoginUserId = sessionUser.Id;
            }
            try
            {
                if (obj.Id != 0)
                {
                    Response responseGet = IUserService.GetSingle(obj.Id);
                    if (responseGet.IsSuccess)
                    {
                        User user = (User)responseGet.Data;

                        StringBuilder filter = new StringBuilder();
                        filter.Append(" 1=1");
                        filter.Append(" and " + Converter.GetColumnNameByPropertyName<User>(nameof(DiOTP.Utility.User.EmailId)) + " = '" + obj.EmailId + "'");
                        Response responseUList = IUserService.GetDataByFilter(filter.ToString(), 0, 0, false);
                        if (responseUList.IsSuccess)
                        {
                            List<User> userMatches = (List<User>)responseUList.Data;
                            if (userMatches.Count > 0)
                            {
                                foreach (User userExist in userMatches)
                                {
                                    if (userExist.Id == obj.Id)
                                    {
                                        user.ModifiedBy = LoginUserId;
                                        user.ModifiedDate = DateTime.Now;
                                        user.MobileNumber = obj.MobileNumber;
                                        user.EmailId = obj.EmailId;
                                        IUserService.UpdateData(user);

                                        response.IsSuccess = true;
                                        response.Message = "Success";

                                        //Audit Log starts here
                                        AdminLogMain alm = new AdminLogMain()
                                        {
                                            Description = "Individual User Added",
                                            TableName = "users",
                                            UpdatedDate = DateTime.Now,
                                            UserId = loginUser.Id,
                                        };
                                        Response responseLog = IAdminLogMainService.PostData(alm);


                                        //Audit Log ends here
                                    }
                                    else
                                    {
                                        response.IsSuccess = false;
                                        response.Message = "Account with this email already exists.";
                                        return response;
                                    }
                                }
                            }
                            else
                            {
                                user.ModifiedBy = LoginUserId;
                                user.ModifiedDate = DateTime.Now;
                                user.MobileNumber = obj.MobileNumber;
                                user.EmailId = obj.EmailId;
                                IUserService.UpdateData(user);

                                response.IsSuccess = true;
                                response.Message = "Success";
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "User Added",
                                    TableName = "users",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);


                                //Audit Log ends here
                            }
                        }
                    }
                    else
                    {
                        return responseGet;
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Add corporate user action: " + ex.Message);
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Download(string filter)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response response = new Response();
            string message = string.Empty;
            try
            {
                
                string tempPath = Path.GetTempPath() + "Individual_Accounts_" + DateTime.Now.ToString("yyyyMMddhhmmssfff");
                FileInfo excelFile = new FileInfo(tempPath);
                message = excelFile.FullName;
                ExcelPackage excel = new ExcelPackage();
                excel.Workbook.Worksheets.Add("Individual Accounts");

                var headerRow = new List<string[]>()
            {
                new string[] { "S.No.", "Status", "Username", "Mobile No", "Registered date", "ID No", "MA Accounts", "Email", "Last login date" }
            };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A2:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "2";

                var worksheet = excel.Workbook.Worksheets["Individual Accounts"];

                string docDetails = "Individual Accounts";

                try
                {
                    // ------------------------------------------------
                    // Creation of header cells
                    // ------------------------------------------------
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    // ------------------------------------------------
                    // Populate sheet with some real data from list
                    // ------------------------------------------------
                    int row = 3; // start row (in row 1 are header cells)
                    StringBuilder filterQuery = new StringBuilder();

                    filterQuery.Append(" users.user_role_id='3'");
                    filterQuery.Append(" and user_accounts.holder_class not in ('RC', 'BC', 'NC', 'FC')");
                    if (filter == "" || filter == null)
                    {
                        docDetails += " | Status: All (Active, Inactive)";
                        filterQuery.Append(" and users.status in (0, 1)");
                        
                    }
                    else
                    {
                        docDetails += " | Status: " + (filter == "1" ? "Active" : (filter == "0" ? "Inactive" : "-")) + " ";
                        filterQuery.Append(" and users.status in (" + filter + ")");
                    }

                    string docDetailsRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    worksheet.Cells[docDetailsRange].Merge = true;
                    worksheet.Cells[docDetailsRange].Value = docDetails;
                    worksheet.Cells[docDetailsRange].Style.Font.Bold = true;

                    Response responseUList = IUserService.GetDataByMA(filterQuery.ToString(), 0, 0, true);
                    if (responseUList.IsSuccess)
                    {
                        List<User> users = (List<User>)responseUList.Data;

                        StringBuilder asb = new StringBuilder();
                        int index = 1;
                        foreach (User u in users)
                        {
                            string siteURL = ConfigurationManager.AppSettings["siteURL"];
                            
                            Response responseUA = IUserAccountService.GetDataByFilter(" user_id='" + u.Id + "' ", 0, 0, false);
                            if (responseUA.IsSuccess)
                            {
                                List<UserAccount> userAccounts = (List<UserAccount>)responseUA.Data;

                                worksheet.Cells[row, 1].Value = index;
                                worksheet.Cells[row, 2].Value = (u.Status == 1 ? "Active" : "Inactive");
                                worksheet.Cells[row, 3].Value = u.Username;
                                worksheet.Cells[row, 4].Value = u.MobileNumber;
                                worksheet.Cells[row, 5].Value = u.CreatedDate == null ? "" : u.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss");
                                worksheet.Cells[row, 6].Value = u.IdNo;
                                worksheet.Cells[row, 7].Value = String.Join(", ", userAccounts.Select(x => x.AccountNo).ToArray());
                                worksheet.Cells[row, 8].Value = u.EmailId;
                                worksheet.Cells[row, 9].Value = (u.LastLoginDate == null ? "" : u.LastLoginDate.Value.ToString("dd/MM/yyyy HH:mm:ss"));
                                row++;
                                index++;
                            }
                        }
                    }

                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    // Apply some predefined styles for data to look nicely :)
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    // Save this data as a file
                    excel.SaveAs(excelFile);
                    // Display SUCCESS message
                    response.IsSuccess = true;
                    response.Message = "Downloading...";
                    //Audit Log starts here
                    AdminLogMain alm = new AdminLogMain()
                    {
                        Description = "Individual Account List downloaded",
                        TableName = "users",
                        UpdatedDate = DateTime.Now,
                        UserId = loginUser.Id,
                    };
                    Response responseLog = IAdminLogMainService.PostData(alm);


                    //Audit Log ends here
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                }
                finally
                {
                    //Empty variables
                    excel = null;
                    worksheet = null;

                    // Force garbage collector cleaning
                    GC.Collect();

                    //change to byte[]
                    byte[] result = File.ReadAllBytes(excelFile.ToString());
                    String file = Convert.ToBase64String(result);
                    //delete temporary file
                    File.Delete(excelFile.ToString());
                    response.Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + file;
                    response.Message = "User_list" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    //response.Data = "data:application/vnd.ms-excel;base64," + file;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + message;
            }
            return response;
        }

    }
}