﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using OfficeOpenXml;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using static DiOTP.Utility.CustomClasses.CustomStatus;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using Admin.ServiceCalls;
using DiOTP.Utility.OracleDTOs;
using static Admin.ServiceCalls.ServicesManager;

namespace Admin
{
    public partial class AccountOpeningRequest : System.Web.UI.Page
    {
        private static readonly Lazy<IAccountOpeningService> lazyAccountOpeningServiceObj = new Lazy<IAccountOpeningService>(() => new AccountOpeningService());

        public static IAccountOpeningService IAccountOpeningService { get { return lazyAccountOpeningServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        private static readonly Lazy<IUserTypeService> lazyUserTypeServiceObj = new Lazy<IUserTypeService>(() => new UserTypeService());

        public static IUserTypeService IUserTypeService { get { return lazyUserTypeServiceObj.Value; } }

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IMaHolderRegService> lazyMaHolderRegObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());

        public static IMaHolderRegService IMaHolderRegService { get { return lazyMaHolderRegObj.Value; } }

        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());
        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyAdminLogSubServiceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());
        public static IAdminLogSubService IAdminLogSubService { get { return lazyAdminLogSubServiceObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        private static readonly Lazy<IUserLogSubService> lazyIUserLogSuberviceObj = new Lazy<IUserLogSubService>(() => new UserLogSubService());
        public static IUserLogSubService IUserLogSubService { get { return lazyIUserLogSuberviceObj.Value; } }

        private static readonly Lazy<IUserSecurityService> lazyIUserSecurityService = new Lazy<IUserSecurityService>(() => new UserSecurityService());
        public static IUserSecurityService IUserSecurityService { get { return lazyIUserSecurityService.Value; } }

        private static readonly Lazy<IAccountOpeningOccupationService> lazyObjO = new Lazy<IAccountOpeningOccupationService>(() => new AccountOpeningOccupationService());
        public static IAccountOpeningOccupationService IAccountOpeningOccupationService { get { return lazyObjO.Value; } }

        private static readonly Lazy<IAccountOpeningAddressService> lazyObjAOA = new Lazy<IAccountOpeningAddressService>(() => new AccountOpeningAddressService());
        public static IAccountOpeningAddressService IAccountOpeningAddressService { get { return lazyObjAOA.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder filter = new StringBuilder();
            string mainQ = (@"SELECT ao.ID as c, ao.ID, ao.account_type, ao.principle_id, ao.principle_user_id, u.username, ao.joint_relationship, ao.name, ao.mobile_no, ao.submitted_date, ao.id_no, ao.email, ao.settlement_date, ao.us_citizen, ao.is_mobile_verified, 
                                ao.resident_of_malaysia, ao.nationality, ao.dob, ao.race, ao.bumiputra_status, ao.marital_status, ao.gender, ao.salutation, ao.are_you_pep, ao.pep_status, 
                                ao.f_pep_status, ao.tax_resident_outside_malaysia, ao.tax_residency_status, ao.tin, ao.declaration, ao.remarks, ao.process_status, ao.status, ao.is_identity_verified, 
                                ao.is_documents_verified, aoa.address_type, aoa.addr_1, aoa.addr_2, aoa.addr_3, aoa.tel_no, aobd.agreement, aobd.account_type as bank_account_type, aobd.currency, aobd.bank_id, 
                                aobd.account_name, aobd.account_no, aofp.purpose, aofp.source, aofp.employed_by_fund_company, aofp.relationship, aofp.name_of_funder, funders_industory, 
                                aofp.is_funder_beneficial_owner, aofp.fund_owner_name, aofp.estimated_net_worth, aoo.employed, aoo.occupation, aoo.designation, aoo.employer_name, 
                                aoo.nature_of_business, aoo.monthly_income, aoo.occupation_others, ao.identity_status, ao.documents_status, ao.register_ip_address, 
                                ao.is_additional_admin, ao.agent_code FROM ");
            string mainQCount = (@"select count(a.c) from (");
            filter.Append(@"account_openings ao join ao_addresses aoa 
                            on ao.ID = aoa.account_opening_id left join ao_bank_details aobd on ao.ID = aobd.account_opening_id join ao_financial_profiles aofp on ao.ID = aofp.account_opening_id 
                            join ao_occupations aoo on ao.ID = aoo.account_opening_id 
                            left join users u on ao.principle_user_id = u.id
                             where ao.status=1 ");
            if (Session["admin"] == null)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx?redirectUrl=default.aspx'", true);
            else if (Session["isContentAdmin"] != null && Session["isContentAdmin"].ToString() == "1")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('You do not have permission to access / on this panel.'); window.location.href='default.aspx'", true);
            else
            {
                User user = (User)Session["admin"];
                Response response = IUserTypeService.GetDataByFilter("user_id = '" + user.Id + "'", 0, 0, false);
                UserType loginUserType = new UserType();
                if (response.IsSuccess)
                {
                    loginUserType = ((List<UserType>)response.Data).FirstOrDefault();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["hdnNumberPerPage"]))
                    hdnNumberPerPage.Value = Request.QueryString["hdnNumberPerPage"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnCurrentPageNo"]))
                    hdnCurrentPageNo.Value = Request.QueryString["hdnCurrentPageNo"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["hdnTotalRecordsCount"]))
                    hdnTotalRecordsCount.Value = Request.QueryString["hdnTotalRecordsCount"].ToString();

                if (Request.QueryString["pageLength"] != "" && Request.QueryString["pageLength"] != null)
                {
                    pageLength.Value = Request.QueryString["pageLength"].ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsNewSearch"]))
                {
                    IsNewSearch.Value = Request.QueryString["IsNewSearch"].ToString();
                }
                if (IsNewSearch.Value == "1" || IsNewSearch.Value == "2")
                {
                    hdnCurrentPageNo.Value = "";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Search"]))
                {
                    Search.Value = Request.QueryString["Search"].ToString();
                    filter.Append(" and (ao.name COLLATE UTF8_GENERAL_CI like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ao.email COLLATE UTF8_GENERAL_CI like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ao.id_no COLLATE UTF8_GENERAL_CI like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ao.mobile_no like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or u.username like '%" + Search.Value.Trim() + "%'");
                    filter.Append(" or ao.joint_relationship like '%" + Search.Value.Trim() + "%')");
                }
                if (!string.IsNullOrEmpty(Request.QueryString["DateType"]))
                {
                    DateType.Value = Request.QueryString["DateType"].ToString();
                    if (!string.IsNullOrEmpty(Request.QueryString["FromDate"]))
                    {
                        FromDate.Value = Request.QueryString["FromDate"].ToString();
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["ToDate"]))
                    {
                        ToDate.Value = Request.QueryString["ToDate"].ToString();
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FilterValue"]))
                {
                    FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                    if (FilterValue.Value != "All")
                    {
                        if (FilterValue.Value == "0")
                        {
                            filter.Append(@"and ao.process_status in ('1', '2', '10', '11', '20', '21', '3') ");
                        }
                        else if (FilterValue.Value == "1")
                        {
                            filter.Append(@"and ao.process_status in ('4') ");
                        }
                        else if (FilterValue.Value == "2")
                        {
                            filter.Append(@"and ao.process_status in ('29', '39', '49') ");
                        }
                        else
                        {

                        }

                    }
                    else
                    {
                        filter.Append(@"and ao.process_status in ('1', '2', '29', '10', '11', '20', '21', '3', '39', '4', '49') ");
                    }
                }
                else
                {
                    filter.Append(@"and ao.process_status in ('1', '2', '3', '10', '11', '20', '21') ");
                }

                if (!string.IsNullOrEmpty(DateType.Value))
                {
                    if (!string.IsNullOrEmpty(FromDate.Value) && !string.IsNullOrEmpty(ToDate.Value))
                    {
                        DateTime from = DateTime.ParseExact(FromDate.Value.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        DateTime to = DateTime.ParseExact(ToDate.Value.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        if (FromDate.Value.Trim() == ToDate.Value.Trim())
                        {
                            to = to.AddDays(1);
                        }
                        else if (from > to)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                                    "alert('From date cannot be greater than To date.');", true);
                        }
                        if (DateType.Value == "1")
                            filter.Append(" and ao.submitted_date between '" + from.ToString("yyyy-MM-dd") + "' and '" + to.ToString("yyyy-MM-dd") + "'");
                        else if (DateType.Value == "2")
                            filter.Append(" and ao.settlement_date between '" + from.ToString("yyyy-MM-dd") + "' and '" + to.ToString("yyyy-MM-dd") + "'");
                    }
                }
                filter.Append("group by ao.ID ");

                if (Request.QueryString["hdnOrderByColumn"] != "" && Request.QueryString["hdnOrderByColumn"] != null && Request.QueryString["hdnOrderByColumn"].Trim() != "-")
                {
                    hdnOrderByColumn.Value = Request.QueryString["hdnOrderByColumn"].ToString();
                    hdnOrderType.Value = Request.QueryString["hdnOrderType"].ToString();
                    filter.Append(" order by " + hdnOrderByColumn.Value + " " + hdnOrderType.Value + " ");
                }
                else
                    filter.Append("order by FIELD(ao.process_status, '1', '2', '11', '21', '3', '10', '20', '4', '29', '39', '49'), ao.submitted_date desc ");

                int skip = 0, take = 20;
                if (pageLength.Value != "" && Int32.TryParse(pageLength.Value, out int o))
                    take = Convert.ToInt32(pageLength.Value);



                if (hdnCurrentPageNo.Value == "")
                {
                    skip = 0;
                    hdnNumberPerPage.Value = take.ToString();
                    hdnCurrentPageNo.Value = "1";
                    hdnTotalRecordsCount.Value = GenericService.GetCountByQuery(mainQCount + mainQ + filter.ToString() + ") a").Data.ToString();

                }
                else
                {
                    skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * take;
                }

                Response responseUList = GenericService.GetDataByQuery(mainQ + filter.ToString() + "", skip, take, false, null, false, null, false);
                if (responseUList.IsSuccess)
                {
                    var UsersDyn = responseUList.Data;
                    var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                    List<DiOTP.Utility.AccountOpeningRequest> aoRequestList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AccountOpeningRequest>>(responseJSON);


                    StringBuilder asb = new StringBuilder();
                    int index = 1;
                    foreach (DiOTP.Utility.AccountOpeningRequest ao in aoRequestList)
                    {
                        string idV = "";
                        if (ao.IsIdentityVerified == 1)
                        {
                            if (DateTime.TryParseExact(ao.IdentityStatus, "yyyy-MM-dd hh:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime idDateTime))
                            {
                                //DateTime idDateTime = DateTime.ParseExact(ao.IdentityStatus, "yyyy-MM-dd hh:mm:ss", CultureInfo.InvariantCulture);
                                if (ao.ProcessStatus == (int)AOProcessStatus.IDVerified || ao.ProcessStatus == (int)AOProcessStatus.DocVerified || ao.ProcessStatus == (int)AOProcessStatus.Approved)
                                {
                                    idV = "Pass (" + idDateTime.ToString("dd/MM/yyyy hh:mm:ss") + ")";
                                }
                                else if (ao.ProcessStatus == (int)AOProcessStatus.IDVerificationFailed)
                                {
                                    idV = "Failed (" + idDateTime.ToString("dd/MM/yyyy hh:mm:ss") + ")";
                                }
                            }
                            else if(ao.IdentityStatus == "")
                            {
                                idV = "-";
                            }
                            else
                            {
                                idV = "Old data";
                            }
                            //var plainTextBytes = Convert.FromBase64String(ao.IdentityStatus);
                            //var result = Encoding.UTF8.GetString(plainTextBytes);
                            //Trulioo.Client.V1.Model.VerifyResult verifyResult = JsonConvert.DeserializeObject<Trulioo.Client.V1.Model.VerifyResult>(result);
                            //if (verifyResult.Record.RecordStatus == "match")
                            //idV = "Pass (" + ao.IdentityStatus.ToString("dd/MM/yyyy hh:mm:ss") + ")";
                            //else
                            //    idV = "Failed (" + verifyResult.UploadedDt.ToLocalTime().ToString("dd/MM/yyyy hh:mm:ss") + ")";
                        }
                        else
                        {
                            idV = "-";
                        }
                        string docV = "";
                        if (ao.IsDocumentsVerified == 1)
                        {
                            DateTime docDateTime = DateTime.ParseExact(ao.DocumentsStatus, "yyyy-MM-dd hh:mm:ss", CultureInfo.InvariantCulture);
                            if (ao.ProcessStatus == (int)AOProcessStatus.DocVerified || ao.ProcessStatus == (int)AOProcessStatus.Approved)
                            {
                                docV = "Pass (" + docDateTime.ToString("dd/MM/yyyy hh:mm:ss") + ")";
                            }
                            else if (ao.ProcessStatus == (int)AOProcessStatus.DocVerificationFailed)
                            {
                                docV = "Failed (" + docDateTime.ToString("dd/MM/yyyy hh:mm:ss") + ")";
                            }
                        }
                        else if (ao.IsDocumentsVerified == 0 && ao.IsAdditionalAdmin == 1 && ao.ProcessStatus == (int)AOProcessStatus.DocRequiredByAdmin)
                        {
                            docV = "Additional Doc Requested";
                        }
                        else if (ao.IsDocumentsVerified == 0 && ao.IsAdditionalAdmin == 1 && ao.ProcessStatus == (int)AOProcessStatus.DocRequiredUploaded)
                        {
                            docV = "Additional Doc Uploaded";
                        }
                        else if (ao.IsDocumentsVerified == 0 && ao.ProcessStatus == (int)AOProcessStatus.UnclearDocsRequested)
                        {
                            docV = "Unclear Doc Requested";
                        }
                        else if (ao.IsDocumentsVerified == 0 && ao.ProcessStatus == (int)AOProcessStatus.UnclearDocsUploaded)
                        {
                            docV = "Unclear Doc Uploaded";
                        }
                        else
                        {
                            docV = "-";
                        }
                        asb.Append(@"<tr>
                                <td class='icheck'>
                                    <div class='square single-row'>
                                        <div class='checkbox'>
                                            <input type='checkbox' name='checkRow' class='checkRow' value='" + ao.Id + @"' /> <label>" + index + @"</label><br/>
                                        </div>
                                    </div>
                                    <span class='row-status'>" + (ao.ProcessStatus == (int)AOProcessStatus.Approved ? "<span class='label label-success'>Approved</span>" :
                                 ao.ProcessStatus == (int)AOProcessStatus.IDVerificationFailed || ao.ProcessStatus == (int)AOProcessStatus.Rejected ? "<span class='label label-danger'>Rejected</span>" :
                                 ao.ProcessStatus == (int)AOProcessStatus.IDVerified || ao.ProcessStatus == (int)AOProcessStatus.DocRequiredByAdmin || ao.ProcessStatus == (int)AOProcessStatus.DocRequiredUploaded || ao.ProcessStatus == (int)AOProcessStatus.DocVerified || ao.ProcessStatus == (int)AOProcessStatus.UnclearDocsRequested || ao.ProcessStatus == (int)AOProcessStatus.UnclearDocsUploaded ? "<span class='label label-warning'>Pending</span>" : (ao.ProcessStatus == (int)AOProcessStatus.Completed ? "<span class='label label-warning'>Requested</span>" : "")) + @"</span>
                                </td>
                                <td>
                                    <table class='table'>
                                        <tbody>
                                            <tr>
                                                <td>Account Type</td>
                                                <td><strong>" + (ao.AccountType == 1 ? "Individual" : (ao.AccountType == 2 ? "Joint (" + ao.JointRelationship + @") for <a href='javascript:;' data-id='" + ao.PrincipleUserId + @"' data-original-title='View User Details' data-trigger='hover' data-placement='bottom' class='popovers action-button' data-url='UserDetails.aspx' data-title='View details' data-action='View'>" + ao.Username + @"&nbsp;<i class='fa fa-eye'></i></a> " : "-")) + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td style='width:50%;'>Name</td>
                                                <td><strong><a href='javascript:;' data-id='" + ao.Id + @"' data-original-title='View User Details' data-trigger='hover' data-placement='bottom' class='popovers action-button' data-url='AccountOpeningDetails.aspx' data-title='View details' data-action='View'>" + ao.Name + @"&nbsp;<i class='fa fa-eye'></i></a></strong></td>
                                            </tr>
                                            <tr>
                                                <td>ID No</td>
                                                <td><strong>" + ao.IdNo + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><strong>" + ao.Email + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Mobile number</td>
                                                <td><strong>" + ao.MobileNo + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Public Ip Address, Local Ip Address</td>
                                                <td><strong>" + ao.RegisterIPAddress + @"</strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <table class='table'>
                                        <tbody>
                                            <tr>
                                            <tr>
                                                <td>Submited Date</td>
                                                <td><strong>" + ao.SubmittedDate + @"</strong></td>
                                            </tr>
                                                <td>E-KYC Verification</td>
                                                <!--<td>
                                                    <strong class='" + (idV == "-" ? "" : (idV == "Verified" ? "text-success" : (idV == "Verification Failed" ? "text-danger" : ""))) + @"'>
                                                        " + (idV == "-" ? idV : "<a href='javascript:;' data-id='" + ao.Id + @"' data-original-title='View User Details' data-trigger='hover' data-placement='bottom' class='popovers action-button' data-url='VerifyResult.aspx' data-title='View details' data-action='View'>" + idV + @"&nbsp;<i class='fa fa-eye'></i></a>") + @"
                                                    </strong>
                                                </td>-->
                                                <td><strong class='" + (idV == "-" ? "" : (idV.Contains("Pass") ? "text-success" : (idV.Contains("Failed") ? "text-danger" : "text-info"))) + "'>" + idV + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Document Verification</td>
                                                <td><strong class='" + (docV == "-" ? "" : (docV.Contains("Pass") ? "text-success" : (docV.Contains("Failed") ? "text-danger" : "text-info"))) + "'>" + docV + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Settlement Date</td>
                                                <td><strong>" + (ao.SettlementDate == null || ao.SettlementDate.Value.ToString("yyyy") == "0001" ? "-" : ao.SettlementDate.Value.ToString()) + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Agent Code</td>
                                                <td><strong>" + (ao.AgentCode == null ? "-" : ao.AgentCode) + @"</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Remarks</td>
                                                <td><strong>" + (ao.Remarks == null ? "-" : ao.Remarks) + @"</strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                
                            </tr>");
                        index++;
                    }
                    usersTbody.InnerHtml = asb.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseUList.Message + "\");", true);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Action(Int32[] ids, String action, String rejectReason = "")
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response response1 = new Response();
            List<string> responseMsgs = new List<string>();
            try
            {
                Email email = new Email();
                DateTime date = DateTime.Now;
                String title = "We regret to inform you that we were unable to process your Unit Trust Investment Account application dated " + date.ToLongDateString() + ".";
                String content = "Reason: Do not meet the required criteria(s).";
                String content2 = "";
                string idString = String.Join(",", ids);
                AccountOpeningOccupation accountOpeningOccupation = new AccountOpeningOccupation();
                AccountOpeningAddress accountOpeningAddress = new AccountOpeningAddress();
                Response responseUList = IAccountOpeningService.GetDataByFilter(" ID in (" + idString + ")", 0, 0, false);
                if (responseUList.IsSuccess)
                {
                    List<AccountOpening> accountOpenings = (List<AccountOpening>)responseUList.Data;
                    if (action == "Reject")
                    {
                        accountOpenings.ForEach(x =>
                        {

                            String auditLogRecording = "";
                            if (x.ProcessStatus != (int)AOProcessStatus.Approved && x.ProcessStatus != (int)AOProcessStatus.UnclearDocsRequested && x.ProcessStatus != (int)AOProcessStatus.DocRequiredByAdmin)
                            {
                                auditLogRecording = x.ProcessStatus.ToString();
                                //responseMsgs.Add("Cannot Reject as Documents are Verified. Pending for MA Creation.");
                                email.user = new User();
                                email.user.Username = x.Name;
                                email.user.EmailId = x.Email;
                                responseMsgs.Add("Request from [" + x.IdNo + "] is rejected");
                                x.ProcessStatus = 49;
                                x.SettlementDate = DateTime.Now;
                                x.Remarks = rejectReason;

                                EmailService.SendAccountOpeningMail(email, title, content, date.ToString(), content2, 4, x.AccountType);
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    //Description = (x.ProcessStatus == (int)AOProcessStatus.Completed ? "Identity for [" + x.IdNo + "] is rejected" : (x.ProcessStatus == (int)AOProcessStatus.IDVerified ? "Documents for [" + x.IdNo + "] is rejected" : (x.ProcessStatus == (int)AOProcessStatus.DocVerified ? "Application for [" + x.IdNo + "] is rejected" : ""))),
                                    Description = "Application for [" + x.IdNo + "] is rejected",
                                    TableName = "account_openings",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,
                                    RefId = x.Id,
                                    StatusType = 0
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                alm = (AdminLogMain)responseLog.Data;
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "process_status",
                                    ValueOld = auditLogRecording,
                                    ValueNew = "39",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);

                                //Audit Log Ends here
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.Approved)
                            {
                                responseMsgs.Add("Application for [" + x.IdNo + "] is already approved. Cannot Reject.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.IDVerificationFailed)
                            {
                                responseMsgs.Add("Application for [" + x.IdNo + "] already rejected. Cannot Reject.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.UnclearDocsRequested)
                            {
                                responseMsgs.Add("Unclear docs for [" + x.IdNo + "] requested. Cannot Reject.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.DocRequiredByAdmin)
                            {
                                responseMsgs.Add("Additional docs for [" + x.IdNo + "] requested. Cannot Reject.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.Rejected)
                            {
                                responseMsgs.Add("Application for [" + x.IdNo + "] is already rejected.");
                            }
                            else
                            {
                                responseMsgs.Add("Cannot Reject.");
                            }
                        });
                        IAccountOpeningService.UpdateBulkData(accountOpenings);

                    }
                    if (action == "Approve")
                    {
                        accountOpenings.ForEach(x =>
                        {
                            Response responseAo = IAccountOpeningOccupationService.GetDataByFilter(" account_opening_id = '" + x.Id + "' and status = 1 ", 0, 0, false);
                            if (responseAo.IsSuccess)
                            {
                                List<AccountOpeningOccupation> accountOpeningOccupations = (List<AccountOpeningOccupation>)responseAo.Data;
                                if (accountOpeningOccupations.Count == 1)
                                {
                                    accountOpeningOccupation = accountOpeningOccupations.FirstOrDefault();

                                }

                                //ADDRESS
                                Response responseAOAist = IAccountOpeningAddressService.GetDataByFilter(" account_opening_id = '" + x.Id + "' and address_type in (3) and status = 1 ", 0, 0, false);
                                if (responseAOAist.IsSuccess)
                                {
                                    List<AccountOpeningAddress> accountOpeningAs = (List<AccountOpeningAddress>)responseAOAist.Data;
                                    if (accountOpeningAs.Count == 1)
                                    {

                                        accountOpeningAddress = accountOpeningAs.FirstOrDefault();


                                        if (accountOpeningAddress.TelNo.Substring(0, 1) == "0")
                                            accountOpeningAddress.TelNo = accountOpeningAddress.TelNo.Substring(1, accountOpeningAddress.TelNo.Length - 1);





                                    }
                                    else
                                    {

                                        //accountOpeningAs.ForEach(x =>
                                        //{
                                        //    if (x.TelNo.Substring(0, 1) == "0")
                                        //        x.TelNo = x.TelNo.Substring(1, x.TelNo.Length - 1);
                                        //});
                                        //accountOpeningExisting.accountOpeningAddresses = accountOpeningAs;
                                    }
                                }
                                else
                                {

                                }
                            }
                            else
                            {

                            }
                            String auditLogRecording = "";
                            if (x.ProcessStatus == (int)AOProcessStatus.DocVerified)
                            {
                                //Response responseMaHolderReg = new Response();
                                //Response responseMANo = ServicesManager.GetNewMANo();
                                //if (responseMANo.IsSuccess)
                                //{
                                //    User principleUser = new User();
                                //    if (x.AccountType == 2)
                                //    {
                                //        Response responsePrimary = IUserService.GetSingle(x.PrincipleUserId);
                                //        if (responsePrimary.IsSuccess)
                                //        {
                                //            principleUser = (User)responsePrimary.Data;
                                //        }
                                //        else
                                //        {
                                //            response1.IsSuccess = false;
                                //            responseMsgs.Add(responsePrimary.Message);
                                //        }
                                //    }
                                //    string maNo = responseMANo.Data.ToString();
                                //    rejectReason = maNo;
                                //    if (x.AccountType == 2)
                                //    {
                                //        string insertQueryJ = @"Insert into UTS.JOINT_REG (HOLDER_NO, ID_NO, ID_NO_OLD, NAME, COUNTRY_RES, NATIONALITY, RACE, OCC_CODE, BIRTH_DT, SEX, INCOME_CODE, RELATION_CODE, NO_OF_DPNDNT, DOWNLOAD_IND, TEL_NO) Values (" + responseMANo.Data.ToString() + @", '" + (x.AccountType == 2 ? (x.IdNo.Length == 12 ? x.IdNo.Insert(6, "-").Insert(9, "-") : x.IdNo) : "") + @"', '', '" + x.Name + @"', 'MA', 'ML', 'C', 'NA', TO_DATE('" + x.Dob + @"', 'DD/MM/YYYY'), 'F', '03', '05', 0, 'N', '" + x.MobileNo + "') ";
                                //        Response responseMaHolderRegJ = ServicesManager.InsertRecord(insertQueryJ);
                                //        if (responseMaHolderRegJ.IsSuccess)
                                //        {

                                //        }
                                //        else
                                //        {
                                //            response1.IsSuccess = false;
                                //            responseMsgs.Add(responseMaHolderRegJ.Message);
                                //        }
                                //    }
                                //    string insertQuery = @"Insert into UTS.HOLDER_REG (ID_NO, ID_NO_2, HOLDER_NO, NAME_1, NAME_2, ADDR_1, ADDR_2, ADDR_3, ADDR_4, POSTCODE, TEL_NO, REGION_CODE, STATE_CODE, COUNTRY_RES, COUNTRY_INCORP, NATIONALITY, RACE, OCC_CODE, CORP_STATUS, BUSINESS_TYPE, ACCOUNT_TYPE, DIV_PYMT, HOLDER_CLS, HOLDER_IND, HOLDER_STATUS, REG_DT, REG_BRN, BIRTH_DT, SEX, AGENT_CODE, AGENT_TYPE, AGENT_ID, HAND_PHONE_NO, PRMNT_ADDR_1, PRMNT_ADDR_2, PRMNT_ADDR_3, PRMNT_ADDR_4, PRMNT_POST_CODE, PRMNT_REGION, PRMNT_STATE, SALUTATION, H_ACC_TYPE, MARITAL, NO_OF_DPNDNT, FIELD_2, DOWNLOAD_IND, IDENTITY_IND)Values('" + (x.AccountType == 1 ? (x.IdNo.Length == 12 ? x.IdNo.Insert(6, "-").Insert(9, "-") : x.IdNo) : (x.AccountType == 2 ? (x.PrincipleId.Length == 12 ? x.PrincipleId.Insert(6, "-").Insert(9, "-") : x.PrincipleId) : "")) + @"', '" + (x.AccountType == 2 ? (x.IdNo.Length == 12 ? x.IdNo.Insert(6, "-").Insert(9, "-") : x.IdNo) : "") + @"', " + responseMANo.Data.ToString() + @", '" + (x.AccountType == 1 ? x.Name : (x.AccountType == 2 ? principleUser.Username : "")) + @"', '" + (x.AccountType == 2 ? x.Name : "") + @"', '7 Jalan SS21/28', 'Damansara Utama', '47400 Petaling Jaya', 'SELANGOR', '47400', '03-7174445', '13', '10', 'MAL', 'MA', 'ML', 'C', 'PF', '01', '03', '01', '1', '" + (x.BumiputraStatus == "Bumiputra" ? "B" : "N") + @"" + (x.AccountType == 1 ? "I" : (x.AccountType == 2 ? "J" : "")) + @"', 'I', 'A', TO_DATE('" + DateTime.Now.ToString("dd/MM/yyyy") + @"', 'DD/MM/YYYY'), 'HQ', TO_DATE('" + (x.AccountType == 1 ? x.Dob : (x.AccountType == 2 ? x.Dob : "")) + @"', 'DD/MM/YYYY'), 'M', '00002475', 'A', '9999', '" + (x.AccountType == 1 ? x.MobileNo : (x.AccountType == 2 ? principleUser.MobileNumber : "")) + @"', '7 Jalan SS21/28', 'Damansara Utama', '47400 Petaling Jaya', 'SELANGOR', '47400', '13', 'SGR', 'Mr. Lim', 'CS', 'NA', 0, '23/03/2018', 'Y', 'NRIC') ";
                                //    responseMaHolderReg = ServiceCalls.ServicesManager.GetMaHolderRegByAccountNo(rejectReason, true, insertQuery);
                                //}

                                auditLogRecording = x.ProcessStatus.ToString();
                                Response responseMaHolderReg = ServiceCalls.ServicesManager.GetMaHolderRegByAccountNo(rejectReason);
                                if (responseMaHolderReg.IsSuccess)
                                {
                                    MaHolderReg maHolderReg = (MaHolderReg)responseMaHolderReg.Data;

                                    Response responseRegister = RegisterAccount(x, maHolderReg, loginUser);
                                    if (responseRegister.IsSuccess)
                                    {

                                        responseMsgs.Add("Request from [" + x.IdNo + "] is approved");
                                        email.user = new User();
                                        email.user.Username = x.Name;
                                        email.user.EmailId = x.Email;
                                        maHolderReg.OccCode = accountOpeningOccupation.Occupation;
                                        maHolderReg.Nob = accountOpeningOccupation.NatureOfBusiness;
                                        maHolderReg.NameOfEmployer = accountOpeningOccupation.EmployerName;
                                        maHolderReg.AccountType = accountOpeningOccupation.MonthlyIncome;
                                        maHolderReg.Addr1 = accountOpeningAddress.Addr1;
                                        maHolderReg.Addr2 = accountOpeningAddress.Addr2;
                                        maHolderReg.Addr3 = accountOpeningAddress.Addr3;
                                        maHolderReg.Postcode = Convert.ToInt32(accountOpeningAddress.PostCode);
                                        maHolderReg.OfficeCity = accountOpeningAddress.City;
                                        maHolderReg.OfficeState = accountOpeningAddress.State;
                                        maHolderReg.OfficeCountry = accountOpeningAddress.Country;
                                        maHolderReg.OfficeTelNo = accountOpeningAddress.TelNo;
                                        maHolderReg.OtpVersion = "0";
                                        maHolderReg.OtpEntBy = DateTime.Now;
                                        x.ProcessStatus = 4;
                                        x.SettlementDate = DateTime.Now;
                                        content2 = rejectReason;
                                        x.Remarks = rejectReason;
                                        IMaHolderRegService.PostData(maHolderReg);
                                        EmailService.SendAccountOpeningMail(email, title, content, x.SubmittedDate.ToString(), content2, 3, x.AccountType);
                                        email.link = ConfigurationManager.AppSettings["investorURL"];
                                        EmailService.SendActivationMail(email);
                                        if (x.AccountType == 2)
                                        {
                                            Response responsePrimary = IUserService.GetSingle(x.PrincipleUserId);
                                            if (responsePrimary.IsSuccess)
                                            {
                                                email.user = (User)responsePrimary.Data;
                                                EmailService.SendAccountOpeningMail(email, title, content, x.SubmittedDate.ToString(), content2, 3, x.AccountType);
                                            }
                                            else
                                            {
                                                response1.IsSuccess = false;
                                                responseMsgs.Add(responsePrimary.Message);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        response1.IsSuccess = false;
                                        responseMsgs.Add(responseRegister.Message);
                                    }

                                    //Audit Log starts here
                                    AdminLogMain alm = new AdminLogMain()
                                    {
                                        Description = "Account opening for " + x.IdNo + " is approved with MA number: " + x.Remarks,
                                        TableName = "account_openings",
                                        UpdatedDate = DateTime.Now,
                                        UserId = loginUser.Id,
                                        RefId = x.Id,
                                        StatusType = 1
                                    };
                                    Response responseLog = IAdminLogMainService.PostData(alm);
                                    if (!responseLog.IsSuccess)
                                    {
                                        //Audit log failed
                                    }
                                    else
                                    {

                                    }
                                    alm = (AdminLogMain)responseLog.Data;
                                    AdminLogSub als = new AdminLogSub()
                                    {
                                        AdminLogMainId = alm.Id,
                                        ColumnName = "process_status",
                                        ValueOld = auditLogRecording,
                                        ValueNew = "4",
                                    };
                                    Response response2 = IAdminLogSubService.PostData(als);
                                    //Audit Log Ends here
                                }
                                else
                                {
                                    response1.IsSuccess = false;
                                    responseMsgs.Add(responseMaHolderReg.Message);
                                }

                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.Completed)
                            {
                                responseMsgs.Add("Identity Verification [" + x.IdNo + "] is pending. Cannot Approve.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.IDVerificationFailed)
                            {
                                responseMsgs.Add("Application for [" + x.IdNo + "] is already rejected. Cannot Approve.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.UnclearDocsRequested)
                            {
                                responseMsgs.Add("Unclear Documents for [" + x.IdNo + "] is requested. Cannot Approve.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.DocRequiredByAdmin)
                            {
                                responseMsgs.Add("Additional Documents for [" + x.IdNo + "] is requested. Cannot Approve.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.IDVerified || x.ProcessStatus == (int)AOProcessStatus.DocRequiredUploaded || x.ProcessStatus == (int)AOProcessStatus.UnclearDocsUploaded)
                            {
                                responseMsgs.Add("Document Verification for [" + x.IdNo + "] is pending. Cannot Approve.");
                            }
                            else
                            {
                                responseMsgs.Add("Cannot Approve.");
                            }
                        });
                        IAccountOpeningService.UpdateBulkData(accountOpenings);
                    }
                    if (action == "RequestFile")
                    {
                        String auditLogRecording = "";
                        accountOpenings.ForEach(x =>
                        {
                            if (x.ProcessStatus == (int)AOProcessStatus.IDVerified || x.ProcessStatus == (int)AOProcessStatus.UnclearDocsUploaded)
                            {
                                if (x.IsAdditionalAdmin == 0)
                                {
                                    auditLogRecording = x.ProcessStatus.ToString();
                                    email.user = new User();
                                    email.user.Username = x.Name;
                                    email.user.EmailId = x.Email;
                                    responseMsgs.Add("Additional documents request email is sent for " + x.Name);
                                    x.ProcessStatus = 20;
                                    x.IsAdditionalAdmin = 1;
                                    //x.AdditionalDesc = rejectReason;
                                    //Response responseEmail = EmailService.SendeKYCDocumentVerificationEmail(x, true, rejectReason, x.SubmittedDate.ToString());
                                    //EmailService.SendAccountOpeningMail(email, title, content, date.ToString(), content2, 7);
                                    //Audit Log starts here
                                    AdminLogMain alm = new AdminLogMain()
                                    {
                                        Description = "Additional documents request email is sent for " + x.Name,
                                        TableName = "account_openings",
                                        UpdatedDate = DateTime.Now,
                                        UserId = loginUser.Id,
                                        RefId = x.Id,
                                        StatusType = 1
                                    };
                                    Response responseLog = IAdminLogMainService.PostData(alm);
                                    if (!responseLog.IsSuccess)
                                    {
                                        //Audit log failed
                                    }
                                    else
                                    {

                                    }
                                    alm = (AdminLogMain)responseLog.Data;
                                    AdminLogSub als = new AdminLogSub()
                                    {
                                        AdminLogMainId = alm.Id,
                                        ColumnName = "process_status",
                                        ValueOld = auditLogRecording,
                                        ValueNew = "20",
                                    };
                                    Response response2 = IAdminLogSubService.PostData(als);

                                    //Audit Log Ends here
                                }
                                else
                                {
                                    responseMsgs.Add("Additional documents already requested for " + x.Name);
                                }
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.Completed)
                            {
                                responseMsgs.Add("Identity Verification [" + x.IdNo + "] is pending. Cannot Request Files.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.IDVerificationFailed)
                            {
                                responseMsgs.Add("Application for [" + x.IdNo + "] is already rejected. Cannot Request Files.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.DocRequiredByAdmin)
                            {
                                responseMsgs.Add("Additional Documents for [" + x.IdNo + "] is already requested. Cannot Request Files.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.IDVerified || x.ProcessStatus == (int)AOProcessStatus.DocRequiredUploaded || x.ProcessStatus == (int)AOProcessStatus.UnclearDocsUploaded)
                            {
                                responseMsgs.Add("Document Verification for [" + x.IdNo + "] is pending. Cannot Request Files.");
                            }
                            else
                            {
                                responseMsgs.Add("Cannot Request Files.");
                            }

                        });
                        IAccountOpeningService.UpdateBulkData(accountOpenings);

                    }
                    if (action == "RequestUnclearFile")
                    {
                        String auditLogRecording = "";
                        accountOpenings.ForEach(x =>
                        {
                            if (x.ProcessStatus == (int)AOProcessStatus.IDVerified && x.ProcessStatus != (int)AOProcessStatus.DocRequiredByAdmin && x.ProcessStatus != (int)AOProcessStatus.UnclearDocsRequested)
                            {
                                String[] fileTypes = rejectReason.Split(',');
                                auditLogRecording = x.ProcessStatus.ToString();
                                email.user = new User();
                                email.user.Username = x.Name;
                                email.user.EmailId = x.Email;
                                responseMsgs.Add("Requested unclear documents for [" + x.IdNo + "]");
                                x.ProcessStatus = (x.ProcessStatus == (int)AOProcessStatus.IDVerified || x.ProcessStatus == (int)AOProcessStatus.DocRequiredUploaded ? (int)AOProcessStatus.UnclearDocsRequested : 0);
                                x.AdditionalDesc = rejectReason;
                                int relation = 0;
                                if (x.JointRelationship != null && x.JointRelationship != "")
                                {
                                    relation = 1;
                                }
                                else
                                {
                                    relation = 0;
                                }
                                List<String> ReuploadFileTypes = new List<string>();
                                fileTypes.ToList().ForEach(f =>
                                {
                                    if (f == "1")
                                        ReuploadFileTypes.Add("NRIC Front");
                                    if (f == "2")
                                        ReuploadFileTypes.Add("NRIC Back");
                                    if (f == "3")
                                        ReuploadFileTypes.Add("Selfie");
                                    if (f == "4")
                                        ReuploadFileTypes.Add("Signature");
                                    if (f == "5")
                                        ReuploadFileTypes.Add("Additional Documents");
                                    if (f == "6")
                                        ReuploadFileTypes.Add("Supporting Documents");
                                });

                                string ReuploadFileTypesString = "<ol type='i' style='text-align: left;line-height: 20px;;margin-top: 10px;margin-bottom: 20px;'>";
                                ReuploadFileTypes.ForEach(f =>
                                {
                                    ReuploadFileTypesString += "<li>" + f + "</li>";
                                });
                                ReuploadFileTypesString += "</ol>";
                                // responseEmail = EmailService.SendeKYCDocumentVerificationEmail(x, true, ReuploadFileTypesString, x.SubmittedDate.ToString(), true, relation);
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Requested unlcear documents for [" + x.IdNo + "]",
                                    TableName = "account_openings",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,
                                    RefId = x.Id,
                                    StatusType = 1
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                alm = (AdminLogMain)responseLog.Data;
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "process_status",
                                    ValueOld = auditLogRecording,
                                    ValueNew = "10",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);
                                //Audit Log Ends here
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.Completed)
                            {
                                responseMsgs.Add("Identity Verification [" + x.IdNo + "] is pending. Cannot Request.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.IDVerificationFailed)
                            {
                                responseMsgs.Add("Identity Verification for [" + x.IdNo + "] is failed. Cannot Request.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.DocVerified)
                            {
                                responseMsgs.Add("Documents from [" + x.IdNo + "] is already approved. Cannot Request.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.UnclearDocsRequested)
                            {
                                responseMsgs.Add("Unclear Documents for [" + x.IdNo + "] already requested. Cannot Request.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.UnclearDocsUploaded)
                            {
                                responseMsgs.Add("Unclear Documents for [" + x.IdNo + "] already uploaded. Cannot Request.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.DocRequiredByAdmin)
                            {
                                responseMsgs.Add("Additional Documents for [" + x.IdNo + "] is requested. Cannot Request.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.DocRequiredUploaded)
                            {
                                responseMsgs.Add("Additional Documents for [" + x.IdNo + "] is uploaded. Cannot Request.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.Rejected)
                            {
                                responseMsgs.Add("Application for [" + x.IdNo + "] is rejected. Cannot Request.");
                            }
                            else
                            {
                                responseMsgs.Add("Cannot Proceed.");
                            }
                        });
                        IAccountOpeningService.UpdateBulkData(accountOpenings);
                    }
                    if (action == "VerifyID") // ----------------------------------------------------------------------------------------------------------Not Using-------------------------------------------------------
                    {
                        accountOpenings.ForEach(accountOpening =>
                        {
                            //if (accountOpening.IsIdentityVerified == 0)
                            String auditLogRecording = "";
                            if (accountOpening.ProcessStatus == (int)AOProcessStatus.Completed)
                            {
                                auditLogRecording = accountOpening.ProcessStatus.ToString();
                                accountOpening.ProcessStatus = (int)AOProcessStatus.IDVerified;
                                accountOpening.IsIdentityVerified = 1;
                                accountOpening.IdentityStatus = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                                responseMsgs.Add("Identity for [" + accountOpening.IdNo + "] is approved");

                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Identity for [" + accountOpening.IdNo + "] is approved",
                                    TableName = "account_openings",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,
                                    RefId = accountOpening.Id,
                                    StatusType = 1
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                alm = (AdminLogMain)responseLog.Data;
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "process_status",
                                    ValueOld = auditLogRecording,
                                    ValueNew = "3",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);

                                //string[] names = accountOpening.Name.Split(' ');
                                //String[] dob = accountOpening.Dob.Split('/');
                                //Int32 DayOfBirth = Convert.ToInt32(dob[0]);
                                //Int32 MonthOfBirth = Convert.ToInt32(dob[1]);
                                //Int32 YearOfBirth = Convert.ToInt32(dob[2]);
                                //Trulioo.Client.V1.Model.PersonInfo personInfo = new Trulioo.Client.V1.Model.PersonInfo
                                //{
                                //    FirstGivenName = names[0],
                                //    FirstSurName = names.Length > 1 ? names[1] : names[0],
                                //    DayOfBirth = DayOfBirth,
                                //    MonthOfBirth = MonthOfBirth,
                                //    YearOfBirth = YearOfBirth
                                //};
                                //List<Trulioo.Client.V1.Model.NationalId> NationalIds = new List<Trulioo.Client.V1.Model.NationalId> { new Trulioo.Client.V1.Model.NationalId { Number = accountOpening.IdNo, Type = "nationalid" } };


                                //Response resVerify = VerifyeKYC(personInfo, new Trulioo.Client.V1.Model.Location { }, NationalIds, new Trulioo.Client.V1.Model.Document { });
                                //Trulioo.Client.V1.Model.VerifyResult verifyResult = (Trulioo.Client.V1.Model.VerifyResult)resVerify.Data;
                                //string result = JsonConvert.SerializeObject(verifyResult);
                                //if (resVerify.IsSuccess)
                                //{
                                //    response1.IsSuccess = true;
                                //    accountOpening.IsIdentityVerified = 1;
                                //    var plainTextBytes = Encoding.UTF8.GetBytes(result);
                                //    string base64String = Convert.ToBase64String(plainTextBytes);
                                //    accountOpening.IdentityStatus = base64String;
                                //    IAccountOpeningService.UpdateData(accountOpening);
                                //    responseMsgs.Add(result);
                                //}
                                //else
                                //{
                                //    accountOpening.IsIdentityVerified = 1;
                                //    var plainTextBytes = Encoding.UTF8.GetBytes(result);
                                //    string base64String = Convert.ToBase64String(plainTextBytes);
                                //    accountOpening.IdentityStatus = base64String;
                                //    IAccountOpeningService.UpdateData(accountOpening);
                                //    responseMsgs.Add(result);
                                //    //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Identity Verification failed.', '');", true);
                                //}

                            }
                            else if (accountOpening.ProcessStatus == (int)AOProcessStatus.IDVerified ||
                                        accountOpening.ProcessStatus == (int)AOProcessStatus.UnclearDocsRequested ||
                                        accountOpening.ProcessStatus == (int)AOProcessStatus.UnclearDocsUploaded ||
                                        accountOpening.ProcessStatus == (int)AOProcessStatus.DocRequiredByAdmin ||
                                        accountOpening.ProcessStatus == (int)AOProcessStatus.DocRequiredUploaded ||
                                        accountOpening.ProcessStatus == (int)AOProcessStatus.DocVerificationFailed || 
                                        accountOpening.ProcessStatus == (int)AOProcessStatus.DocVerified ||
                                        accountOpening.ProcessStatus == (int)AOProcessStatus.Rejected || 
                                        accountOpening.ProcessStatus == (int)AOProcessStatus.Approved)
                            {
                                responseMsgs.Add("Identity for [" + accountOpening.IdNo + "] is already verified.");
                            }
                            else if (accountOpening.ProcessStatus == (int)AOProcessStatus.IDVerificationFailed)
                            {
                                responseMsgs.Add("Identity for [" + accountOpening.IdNo + "] is already rejected. Cannot Verify.");
                            }
                        });
                        IAccountOpeningService.UpdateBulkData(accountOpenings);
                    }
                    if (action == "ApproveDoc")
                    {
                        accountOpenings.ForEach(x =>
                        {
                            String auditLogRecording = "";
                            if (x.ProcessStatus == (int)AOProcessStatus.IDVerified || x.ProcessStatus == (int)AOProcessStatus.DocRequiredUploaded || x.ProcessStatus == (int)AOProcessStatus.UnclearDocsUploaded)
                            {
                                auditLogRecording = x.ProcessStatus.ToString();
                                email.user = new User();
                                email.user.Username = x.Name;
                                email.user.EmailId = x.Email;
                                responseMsgs.Add("Documents from [" + x.IdNo + "] is approved");
                                x.ProcessStatus = (x.ProcessStatus == (int)AOProcessStatus.IDVerified || x.ProcessStatus == (int)AOProcessStatus.DocRequiredUploaded || x.ProcessStatus == (int)AOProcessStatus.UnclearDocsUploaded ? (int)AOProcessStatus.DocVerified : 0);
                                x.IsDocumentsVerified = 1;
                                x.DocumentsStatus = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");

                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "Documents from [" + x.IdNo + "] is approved",
                                    TableName = "account_openings",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,
                                    RefId = x.Id,
                                    StatusType = 1
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);
                                if (!responseLog.IsSuccess)
                                {
                                    //Audit log failed
                                }
                                else
                                {

                                }
                                alm = (AdminLogMain)responseLog.Data;
                                AdminLogSub als = new AdminLogSub()
                                {
                                    AdminLogMainId = alm.Id,
                                    ColumnName = "process_status",
                                    ValueOld = auditLogRecording,
                                    ValueNew = "3",
                                };
                                Response response2 = IAdminLogSubService.PostData(als);

                                //Audit Log Ends here
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.Completed)
                            {
                                responseMsgs.Add("Identity Verification [" + x.IdNo + "] is pending. Cannot Verify Documents.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.IDVerificationFailed)
                            {
                                responseMsgs.Add("Application for [" + x.IdNo + "] is already rejected. Cannot Verify Documents.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.DocVerified)
                            {
                                responseMsgs.Add("Documents for [" + x.IdNo + "] is already approved. Cannot Verify Documents.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.Rejected)
                            {
                                responseMsgs.Add("Application for [" + x.IdNo + "] is already rejected. Cannot Verify Documents.");
                            }
                            else if (x.ProcessStatus == (int)AOProcessStatus.DocRequiredByAdmin || x.ProcessStatus == (int)AOProcessStatus.UnclearDocsRequested)
                            {
                                responseMsgs.Add("Additional/Unclear Documents for [" + x.IdNo + "] is requested. Cannot Verify Documents.");
                            }
                            else
                            {
                                responseMsgs.Add("Cannot Verify Documents.");
                            }
                        });
                        IAccountOpeningService.UpdateBulkData(accountOpenings);
                    }
                    //if (action == "RejectDoc")
                    //{
                    //    accountOpenings.ForEach(x =>
                    //    {
                    //        
                    //        if (x.ProcessStatus == (int)AOProcessStatus.IDVerified || x.ProcessStatus == (int)AOProcessStatus.DocRequiredUploaded)
                    //        {
                    //            auditLogRecording = x.ProcessStatus.ToString();
                    //            email.user = new User();
                    //            email.user.Username = x.Name;
                    //            email.user.EmailId = x.Email;
                    //            responseMsgs.Add("Documents from [" + x.IdNo + "] is rejected");
                    //            x.ProcessStatus = (x.ProcessStatus == (int)AOProcessStatus.IDVerified || x.ProcessStatus == (int)AOProcessStatus.DocRequiredUploaded ? (int)AOProcessStatus.DocVerificationFailed : 0);
                    //            x.IsDocumentsVerified = 1;
                    //            x.SettlementDate = DateTime.Now;


                    //        }
                    //        else if (x.ProcessStatus == (int)AOProcessStatus.DocVerified)
                    //        {
                    //            responseMsgs.Add("Documents from [" + x.IdNo + "] is already approved");
                    //        }
                    //        else if (x.ProcessStatus == (int)AOProcessStatus.DocVerificationFailed)
                    //        {
                    //            responseMsgs.Add("Documents from [" + x.IdNo + "] is already rejected");
                    //        }
                    //        else if (x.ProcessStatus == (int)AOProcessStatus.IDVerificationFailed)
                    //        {
                    //            responseMsgs.Add("Identity Verification for [" + x.IdNo + "] is failed. Cannot Proceed.");
                    //        }
                    //        else if (x.ProcessStatus == (int)AOProcessStatus.DocRequiredByAdmin)
                    //        {
                    //            responseMsgs.Add("Additional Documents for [" + x.IdNo + "] is requested. Cannot Proceed.");
                    //        }
                    //        else
                    //        {
                    //            responseMsgs.Add("Cannot Proceed.");
                    //        }


                    //    });
                    //    IAccountOpeningService.UpdateBulkData(accountOpenings);
                    //}
                }

            }
            catch (Exception ex)
            {
                response1.IsSuccess = false;
                responseMsgs.Add("Action: " + ex.Message + " - Exception");
                Console.WriteLine("Account Opening action: " + ex.Message);
            }

            response1.Message = String.Join(",<br/>", responseMsgs.ToArray());
            return response1;
        }

        private static readonly Lazy<IAccountOpeningBankDetailService> lazyObjBD = new Lazy<IAccountOpeningBankDetailService>(() => new AccountOpeningBankDetailService());
        public static IAccountOpeningBankDetailService IAccountOpeningBankDetailService { get { return lazyObjBD.Value; } }

        private static readonly Lazy<IMaHolderBankService> lazyMaHolderBankServiceObj = new Lazy<IMaHolderBankService>(() => new MaHolderBankService());
        public static IMaHolderBankService IMaHolderBankService { get { return lazyMaHolderBankServiceObj.Value; } }
        public static Response RegisterAccount(AccountOpening accountOpening, MaHolderReg maHolderReg, User loginUser)
        {
            List<MaHolderReg> MaHolderRegDatas = new List<MaHolderReg>();
            string principleIC = accountOpening.IdNo;
            string EmailId = accountOpening.Email;
            string Password = accountOpening.Password;
            Response response = new Response();
            try
            {
                MaHolderRegDatas.Add(maHolderReg);
                //principleIC = txtICNo.Text;
                if (principleIC.Contains("-"))
                {
                    principleIC = principleIC.Replace("-", "");
                }
                if (principleIC.Contains(" "))
                {
                    principleIC = principleIC.Replace(" ", "");
                }
                string idno = maHolderReg.IdNo;
                if (idno.Contains("-"))
                {
                    idno = idno.Replace("-", "");
                }
                if (idno.Contains(" "))
                {
                    idno = idno.Replace(" ", "");
                }
                string idno2 = (maHolderReg.IdNo2 == null ? "" : maHolderReg.IdNo2);

                if (idno2.Contains("-"))
                {
                    idno2 = idno2.Replace("-", "");
                }
                if (idno2.Contains(" "))
                {
                    idno2 = idno2.Replace(" ", "");
                }

                if (idno == principleIC)
                {
                    User user = new User
                    {
                        Username = (principleIC == idno ? maHolderReg.Name1 : maHolderReg.Name2),
                        EmailId = EmailId.ToLower(),
                        Password = Password,
                        MobileNumber = (principleIC == idno ? maHolderReg.HandPhoneNo : maHolderReg.JointTelNo),
                        IdNo = principleIC,
                        CreatedDate = DateTime.Now,
                        IsActive = 1,
                        IsSecurityChecked = 1,
                        IsSatChecked = 0,
                        Status = 1,
                        RegisterIp = accountOpening.RegisterIPAddress,//TrackIPAddress.GetUserPublicIP(hdnLocalIPAddress.Value) + ", " + hdnLocalIPAddress.Value,
                        CreatedBy = loginUser.Id,
                        ModifiedBy = 0,
                        VerificationCode = 0,
                        VerifyExpired = 1,
                        ResetExpired = 1,

                    };
                    user.UserIdUserAccounts = new List<UserAccount>();
                    //                                           CASH HOLDER
                    List<MaHolderReg> maHolderRegCash = MaHolderRegDatas.Where(x => CustomValues.GetAccounPlan(x.HolderCls.Trim()) == "CASH").ToList();
                    if (maHolderRegCash.Count > 0)
                    {
                        MaHolderReg ma = maHolderRegCash.FirstOrDefault();
                        user.UserIdUserAccounts.Add(new UserAccount
                        {
                            UserId = user.Id,
                            AccountNo = ma.HolderNo.ToString(),
                            IdNo = principleIC,
                            CreatedBy = 0,
                            CreatedDate = DateTime.Now,
                            Status = 1,
                            IsVerified = 1,
                            MaHolderRegId = ma.Id,
                            ModifiedBy = 0,
                            HolderClass = ma.HolderCls,
                            MaHolderRegIdMaHolderReg = ma,
                            IsPrinciple = 1
                        });
                    }
                    ////                                              EPF HOLDER
                    //List<MaHolderReg> maHolderRegEpf = MaHolderRegDatas.Where(x => CustomValues.GetAccounPlan(x.HolderCls.Trim()) == "EPF").ToList();
                    //if (maHolderRegEpf.Count > 0)
                    //{
                    //    MaHolderReg ma = maHolderRegEpf.FirstOrDefault();
                    //    user.UserIdUserAccounts.Add(new UserAccount
                    //    {
                    //        UserId = user.Id,
                    //        AccountNo = ma.HolderNo.ToString(),
                    //        IdNo = principleIC,
                    //        CreatedBy = 0,
                    //        CreatedDate = DateTime.Now,
                    //        Status = 1,
                    //        IsVerified = 0,
                    //        MaHolderRegId = ma.Id,
                    //        ModifiedBy = 0,
                    //        HolderClass = ma.HolderCls,
                    //        MaHolderRegIdMaHolderReg = ma,
                    //        IsPrinciple = 1
                    //    });
                    //}
                    //                                                JOINT HOLDER
                    List<MaHolderReg> maHolderRegJoints = MaHolderRegDatas.Where(x => CustomValues.GetAccounPlan(x.HolderCls.Trim()) == "JOINT").ToList();
                    if (maHolderRegJoints.Count > 0)
                    {
                        List<MaHolderReg> mas = maHolderRegJoints;
                        List<UserAccount> userAccountJoints = new List<UserAccount>();
                        mas.ForEach(ma =>
                        {
                            string IDNo = ma.IdNo;
                            if (IDNo.Contains("-"))
                            {
                                IDNo = IDNo.Replace("-", "");
                            }
                            if (principleIC.Contains(" "))
                            {
                                IDNo = IDNo.Replace(" ", "");
                            }
                            userAccountJoints.Add(new UserAccount
                            {
                                UserId = user.Id,
                                AccountNo = ma.HolderNo.ToString(),
                                IdNo = principleIC,
                                CreatedBy = 0,
                                CreatedDate = DateTime.Now,
                                Status = 1,
                                IsVerified = 1,
                                MaHolderRegId = ma.Id,
                                ModifiedBy = 0,
                                HolderClass = ma.HolderCls,
                                MaHolderRegIdMaHolderReg = ma,
                                IsPrinciple = (principleIC == IDNo ? 1 : 0)
                            });
                        });
                        userAccountJoints = userAccountJoints.OrderByDescending(x => x.IsPrinciple).ToList();
                        user.UserIdUserAccounts.AddRange(userAccountJoints);
                    }
                    string primaryPlan = "";
                    List<UserAccount> uAs = user.UserIdUserAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "CASH").ToList();
                    if (uAs.Count > 0)
                    {
                        primaryPlan = "CASH";
                    }
                    else
                    {
                        uAs = user.UserIdUserAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "EPF").ToList();
                        if (uAs.Count > 0)
                        {
                            //primaryPlan = "EPF";
                        }
                        else
                        {
                            uAs = user.UserIdUserAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "JOINT").ToList();
                            if (uAs.Count > 0)
                            {
                                primaryPlan = "JOINT";
                            }
                        }
                    }
                    if (primaryPlan == "JOINT")
                    {
                        int i = 0;
                        user.UserIdUserAccounts.ForEach(x =>
                        {
                            string plan = CustomValues.GetAccounPlan(x.HolderClass);
                            string idno11 = x.MaHolderRegIdMaHolderReg.IdNo;
                            if (idno11.Contains("-"))
                                idno11 = idno11.Replace("-", "");
                            if (idno11.Contains(" "))
                                idno11 = idno11.Replace(" ", "");
                            string idno22 = x.MaHolderRegIdMaHolderReg.IdNo2;
                            if (idno22 != null && idno22.Contains("-"))
                                idno22 = idno22.Replace("-", "");
                            if (idno22 != null && idno22.Contains(" "))
                                idno22 = idno22.Replace(" ", "");
                            if (plan == primaryPlan && x.IsPrinciple == 1 && principleIC == idno11)
                            {
                                if (i == 0)
                                    x.IsPrimary = 1;
                                i++;
                            }
                            else if (plan == primaryPlan && x.IsPrinciple == 0 && principleIC == idno22)
                            {
                                if (i == 0)
                                    x.IsPrimary = 1;
                                i++;
                            }
                        });
                    }
                    else
                    {
                        user.UserIdUserAccounts.ForEach(x =>
                        {
                            string plan = CustomValues.GetAccounPlan(x.HolderClass);
                            if (plan == primaryPlan)
                            {
                                x.IsPrimary = 1;
                            }
                        });
                    }


                    Response responseUser = IUserService.PostData(user);
                    if (responseUser.IsSuccess)
                    {
                        User returnUser = (User)responseUser.Data;

                        UserLogMain log = new UserLogMain()
                        {
                            TableName = "users",
                            Description = "User id created",
                            UserId = returnUser.Id,
                            UpdatedDate = DateTime.Now,
                            RefId = returnUser.Id,
                            RefValue = returnUser.Username,
                            StatusType = 1
                        };
                        Response responseULM = IUserLogMainService.PostData(log);

                        returnUser.UserIdUserAccounts.ForEach(uA =>
                        {
                            UserLogMain logUA = new UserLogMain()
                            {
                                TableName = "user_accounts",
                                Description = "Master Accounts binded",
                                UserId = returnUser.Id,
                                UpdatedDate = DateTime.Now,
                                RefId = uA.Id,
                                RefValue = uA.AccountNo,
                                StatusType = 1
                            };
                            Response responseULMUA = IUserLogMainService.PostData(logUA);
                        });

                        Response responseBDList = IAccountOpeningBankDetailService.GetDataByFilter(" account_opening_id = '" + accountOpening.Id + "' and status = 1 ", 0, 0, false);
                        if (responseBDList.IsSuccess)
                        {
                            List<AccountOpeningBankDetail> accountOpeningBankDetails = (List<AccountOpeningBankDetail>)responseBDList.Data;
                            if (accountOpeningBankDetails.Count == 1)
                            {
                                accountOpening.AccountOpeningBankDetail = accountOpeningBankDetails.FirstOrDefault();
                            }
                        }
                        else
                        {
                            return responseBDList;
                        }
                        if (accountOpening.AccountOpeningBankDetail != null)
                        {
                            MaHolderBank mhb = new MaHolderBank()
                            {
                                UserId = returnUser.Id,
                                IsPrimary = 1,
                                BankDefId = accountOpening.AccountOpeningBankDetail.BankId,
                                AccountName = accountOpening.AccountOpeningBankDetail.AccountName,
                                BankAccountNo = accountOpening.AccountOpeningBankDetail.AccountNo,
                                Remarks = "",
                                CreatedDate = DateTime.Now,
                                CreatedBy = user.Id,
                                UpdatedDate = DateTime.Now,
                                UpdatedBy = user.Id,
                                Version = 1,
                                Status = 1,
                                //Image = dbpath,
                            };
                            Response responseX = IMaHolderBankService.PostData(mhb);
                            if (responseX.IsSuccess)
                            {
                                mhb = (MaHolderBank)responseX.Data;

                                if (user.UserIdUserAccounts.FirstOrDefault() != null)
                                {
                                    UserAccountBanks userAccountBanks = new UserAccountBanks
                                    {
                                        MaHolderBankId = Convert.ToInt32(mhb.Id),
                                        UserAccountId = user.UserIdUserAccounts.FirstOrDefault().Id,
                                        CreatedBy = user.Id,
                                        CreatedDate = DateTime.Now,
                                        UpdatedDate = DateTime.Now,
                                        Status = 1,
                                    };

                                    Response responsePostBank = GenericService.PostData<UserAccountBanks>(userAccountBanks);
                                    if (responsePostBank.IsSuccess)
                                    {
                                    }
                                }

                            }
                        }
                        AdminLogMain alm = new AdminLogMain()
                        {
                            Description = "Admin has approved user request on account opening",
                            TableName = "users",
                            UpdatedDate = DateTime.Now,
                            UserId = returnUser.Id,
                            RefId = returnUser.Id,
                            RefValue = returnUser.Username
                            //UserAccountId = 0,
                        };
                        Response responseLog = IAdminLogMainService.PostData(alm);
                        if (!responseLog.IsSuccess)
                        {
                            //Audit log failed
                            //response.IsSuccess = false;
                            //response.Message = re
                        }

                        //UserLogMain ulm = new UserLogMain()
                        //{
                        //    Description = "User registered and MasterAccounts binded",
                        //    TableName = "users",
                        //    UpdatedDate = DateTime.Now,
                        //    UserId = returnUser.Id,
                        //    UserAccountId = 0,
                        //    StatusType = 1,
                        //    RefId = returnUser.Id,
                        //    RefValue = returnUser.Username
                        //};
                        //Response responseUserLog = IUserLogMainService.PostData(ulm);
                        //if (!responseLog.IsSuccess)
                        //{
                        //    //Audit log failed
                        //    //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                        //}

                        try
                        {
                            MaHolderRegDatas.ForEach(x =>
                            {
                                x.OtpActSt = "2";
                            });
                            int count = IMaHolderRegService.PostBulkData(MaHolderRegDatas);
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(ex.Message);

                        }
                        //SMSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
                        //divMessage.Attributes.Add("class", "alert alert-success");
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'An activation link will be send to " + EmailId + " .<br/>If you required to update your mobile number, please <a href=\"/Contact.aspx\" target=\"_blank\">Contact Us</a> or visit our office if you need further assistance.', 'Index.aspx');", true);
                        // An OTP will sent to mobile number " + displayMoileNumber + ".<br/>
                        //sent activation email
                        string siteURL = ConfigurationManager.AppSettings["siteURL"];
                        response.IsSuccess = true;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = responseUser.Message;
                        //divMessage.Attributes.Add("class", "alert alert-danger");
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                    }


                }
                else if (idno2 == principleIC)
                {
                    Response responseUser = IUserService.GetSingle(accountOpening.PrincipleUserId);
                    if (responseUser.IsSuccess)
                    {
                        User principleUser = (User)responseUser.Data;
                        List<UserAccount> userAccounts = principleUser.UserIdUserAccounts;
                        if (userAccounts.FirstOrDefault(x => x.AccountNo == maHolderReg.HolderNo.ToString() && x.IsPrinciple == 1) == null)
                        {
                            UserAccount userAccount = new UserAccount
                            {
                                UserId = principleUser.Id,
                                AccountNo = maHolderReg.HolderNo.ToString(),
                                IdNo = principleIC,
                                CreatedBy = loginUser.Id,
                                CreatedDate = DateTime.Now,
                                Status = 1,
                                IsVerified = 1,
                                VerificationCode = 0,
                                VerifyExpired = 1,

                                ModifiedBy = 0,
                                HolderClass = maHolderReg.HolderCls,
                                MaHolderRegIdMaHolderReg = maHolderReg,
                                IsPrinciple = 1,
                            };
                            Response resUA = IUserAccountService.PostData(userAccount);
                            if (resUA.IsSuccess)
                            {
                                maHolderReg.OtpActSt = "2";
                                Response resMA = IMaHolderRegService.PostData(maHolderReg);
                                if (resMA.IsSuccess)
                                {
                                    User user = new User
                                    {
                                        Username = (principleIC == idno ? maHolderReg.Name1 : maHolderReg.Name2),
                                        EmailId = EmailId.ToLower(),
                                        Password = Password,
                                        MobileNumber = (principleIC == idno ? maHolderReg.HandPhoneNo : maHolderReg.JointTelNo),
                                        IdNo = principleIC,
                                        CreatedBy = loginUser.Id,
                                        CreatedDate = DateTime.Now,
                                        IsActive = 1,
                                        IsSecurityChecked = 1,
                                        IsSatChecked = 0,
                                        Status = 1,
                                        RegisterIp = accountOpening.RegisterIPAddress,
                                        ModifiedBy = 0,
                                        VerificationCode = 0,
                                        VerifyExpired = 1,
                                        ResetExpired = 1
                                    };
                                    user.UserIdUserAccounts = new List<UserAccount>();
                                    List<MaHolderReg> maHolderRegJoints = MaHolderRegDatas.Where(x => CustomValues.GetAccounPlan(x.HolderCls.Trim()) == "JOINT").ToList();
                                    if (maHolderRegJoints.Count > 0)
                                    {
                                        List<MaHolderReg> mas = maHolderRegJoints;
                                        List<UserAccount> userAccountJoints = new List<UserAccount>();
                                        mas.ForEach(ma =>
                                        {
                                            string IDNo = ma.IdNo;
                                            if (IDNo.Contains("-"))
                                            {
                                                IDNo = IDNo.Replace("-", "");
                                            }
                                            if (principleIC.Contains(" "))
                                            {
                                                IDNo = IDNo.Replace(" ", "");
                                            }
                                            userAccountJoints.Add(new UserAccount
                                            {
                                                UserId = user.Id,
                                                AccountNo = ma.HolderNo.ToString(),
                                                IdNo = principleIC,
                                                CreatedBy = loginUser.Id,
                                                CreatedDate = DateTime.Now,
                                                Status = 1,
                                                IsVerified = 1,
                                                MaHolderRegId = ma.Id,
                                                ModifiedBy = 0,
                                                HolderClass = ma.HolderCls,
                                                MaHolderRegIdMaHolderReg = ma,
                                                IsPrinciple = (principleIC == IDNo ? 1 : 0),
                                                IsPrimary = 1
                                            });
                                        });
                                        userAccountJoints = userAccountJoints.OrderByDescending(x => x.IsPrinciple).ToList();
                                        user.UserIdUserAccounts.AddRange(userAccountJoints);
                                        Response responseUserS = IUserService.PostData(user);
                                        if (responseUserS.IsSuccess)
                                        {
                                            //Response responseUTList = IUserTypeService.GetDataByPropertyName(nameof(UserType.UserId), user.Id.ToString(), true, 0, 0, true);
                                            //if (responseUTList.IsSuccess)
                                            //{
                                            //    UserType userType = ((List<UserType>)responseUTList.Data).FirstOrDefault();
                                            //    if (userType != null)
                                            //    {
                                            //        userType.IsVerified = 1;
                                            //        IUserTypeService.UpdateData(userType);
                                            //    }

                                            //}
                                            //else
                                            //{
                                            //    response.IsSuccess = false;
                                            //    response.Message = responseUTList.Message;
                                            //}

                                            User returnUser = (User)responseUserS.Data;

                                            //Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserSecurityTypeId), "1", true, 0, 0, true);
                                            //if (responseUSList.IsSuccess)
                                            //{
                                            //    UserSecurity userSecurity = ((List<UserSecurity>)responseUSList.Data).FirstOrDefault();
                                            //    if (userSecurity != null)
                                            //    {
                                            //        userSecurity.IsVerified = 1;
                                            //        IUserSecurityService.UpdateData(userSecurity);
                                            //    }
                                            //}
                                            //else
                                            //{
                                            //    response.IsSuccess = false;
                                            //    response.Message = responseUSList.Message;
                                            //}

                                            //IUserSecurityService.PostData(new UserSecurity
                                            //{
                                            //    CreatedDate = DateTime.Now,
                                            //    IsVerified = user.IsSecurityChecked == 1 ? 1 : 0,
                                            //    Status = 1,
                                            //    UserId = user.Id,
                                            //    Value = user.MobileNumber,
                                            //    UserSecurityTypeId = 2,
                                            //    VerificationPin = 0
                                            //});

                                            AdminLogMain alm = new AdminLogMain()
                                            {
                                                Description = "Admin has approved user request on account opening",
                                                TableName = "users",
                                                UpdatedDate = DateTime.Now,
                                                UserId = returnUser.Id,
                                                RefId = returnUser.Id,
                                                RefValue = returnUser.Username
                                                //UserAccountId = 0,
                                            };
                                            Response responseLog = IAdminLogMainService.PostData(alm);
                                            if (!responseLog.IsSuccess)
                                            {
                                                //Audit log failed
                                                //response.IsSuccess = false;
                                                //response.Message = re
                                            }

                                            UserLogMain ulm = new UserLogMain()
                                            {
                                                Description = "User registered and login link sent to email",
                                                TableName = "users",
                                                UpdatedDate = DateTime.Now,
                                                UserId = returnUser.Id,
                                                UserAccountId = 0,
                                                RefId = returnUser.Id,
                                                RefValue = returnUser.Username
                                            };
                                            Response responseUserLog = IUserLogMainService.PostData(ulm);
                                            if (!responseLog.IsSuccess)
                                            {
                                                //Audit log failed
                                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                                            }

                                            try
                                            {
                                                //int count = IMaHolderRegService.PostBulkData(MaHolderRegDatas);
                                            }
                                            catch (Exception ex)
                                            {
                                                Logger.WriteLog(ex.Message);

                                            }
                                            //SMSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
                                            //divMessage.Attributes.Add("class", "alert alert-success");
                                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'An activation link will be send to " + EmailId + " .<br/>If you required to update your mobile number, please <a href=\"/Contact.aspx\" target=\"_blank\">Contact Us</a> or visit our office if you need further assistance.', 'Index.aspx');", true);
                                            // An OTP will sent to mobile number " + displayMoileNumber + ".<br/>
                                            //sent activation email
                                            string siteURL = ConfigurationManager.AppSettings["siteURL"];
                                            response.IsSuccess = true;
                                        }
                                        else
                                        {
                                            response.IsSuccess = false;
                                            response.Message = responseUser.Message;
                                            //divMessage.Attributes.Add("class", "alert alert-danger");
                                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                                        }
                                    }
                                }
                                else
                                {
                                    response = resMA;
                                }
                                response.IsSuccess = true;
                            }
                            else
                            {
                                response = resUA;
                            }
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = "MA No already registered in eApexis.";
                        }
                    }
                    else
                    {
                        response = responseUser;
                    }
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "MA No is Invalid.";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public static Response IsValidPhoneNumber(string phoneNumber)
        {
            Response response = new Response();
            try
            {
                //will match +61 or +66- or 0 or nothing followed by a nine digit number
                bool isValid = Regex.Match(phoneNumber,
                    @"^(\+?6?01)[0-46-9]-*[0-9]{7,8}$").Success;
                //to vary this, replace 61 with an international code of your choice 
                //or remove [\+]?61[-]? if international code isn't needed
                //{8} is the number of digits in the actual phone number less one
                if (isValid)
                {
                    response.IsSuccess = true;
                    response.Message = "Success";
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Invalid mobile number";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Add(User obj)
        {
            User loginUser = (User)HttpContext.Current.Session["user"];

            Response response = new Response();

            if (obj.EmailId == "")
            {
                response.IsSuccess = false;
                response.Message = "Email cannot be empty";
                return response;
            }
            else
            {
                if (!CustomValidator.IsValidEmail(obj.EmailId))
                {
                    response.IsSuccess = false;
                    response.Message = "Invalid email";
                    return response;
                }
            }
            if (obj.MobileNumber == "")
            {
                response.IsSuccess = false;
                response.Message = "Mobile number cannot be empty";
                return response;
            }
            else
            {
                response = IsValidPhoneNumber(obj.MobileNumber);
                if (!response.IsSuccess)
                {
                    return response;
                }
            }

            Int32 LoginUserId = 0;
            if (HttpContext.Current.Session["admin"] != null)
            {
                User sessionUser = (User)HttpContext.Current.Session["admin"];
                LoginUserId = sessionUser.Id;
            }
            try
            {
                if (obj.Id != 0)
                {
                    Response responseGet = IUserService.GetSingle(obj.Id);
                    if (responseGet.IsSuccess)
                    {
                        User user = (User)responseGet.Data;

                        StringBuilder filter = new StringBuilder();
                        filter.Append(" 1=1");
                        filter.Append(" and " + Converter.GetColumnNameByPropertyName<User>(nameof(DiOTP.Utility.User.EmailId)) + " = '" + obj.EmailId + "'");
                        Response responseUList = IUserService.GetDataByFilter(filter.ToString(), 0, 0, false);
                        if (responseUList.IsSuccess)
                        {
                            List<User> userMatches = (List<User>)responseUList.Data;
                            if (userMatches.Count > 0)
                            {
                                foreach (User userExist in userMatches)
                                {
                                    if (userExist.Id == obj.Id)
                                    {
                                        user.ModifiedBy = LoginUserId;
                                        user.ModifiedDate = DateTime.Now;
                                        user.MobileNumber = obj.MobileNumber;
                                        user.EmailId = obj.EmailId;
                                        IUserService.UpdateData(user);

                                        response.IsSuccess = true;
                                        response.Message = "Success";

                                        //Audit Log starts here
                                        AdminLogMain alm = new AdminLogMain()
                                        {
                                            Description = "Individual User Added",
                                            TableName = "users",
                                            UpdatedDate = DateTime.Now,
                                            UserId = loginUser.Id,
                                            RefId = loginUser.Id,
                                            RefValue = loginUser.Username
                                        };
                                        Response responseLog = IAdminLogMainService.PostData(alm);


                                        //Audit Log ends here
                                    }
                                    else
                                    {
                                        response.IsSuccess = false;
                                        response.Message = "Account with this email already exists.";
                                        return response;
                                    }
                                }
                            }
                            else
                            {
                                user.ModifiedBy = LoginUserId;
                                user.ModifiedDate = DateTime.Now;
                                user.MobileNumber = obj.MobileNumber;
                                user.EmailId = obj.EmailId;
                                IUserService.UpdateData(user);

                                response.IsSuccess = true;
                                response.Message = "Success";
                                //Audit Log starts here
                                AdminLogMain alm = new AdminLogMain()
                                {
                                    Description = "User Added",
                                    TableName = "users",
                                    UpdatedDate = DateTime.Now,
                                    UserId = loginUser.Id,
                                    RefId = loginUser.Id,
                                    RefValue = loginUser.Username
                                };
                                Response responseLog = IAdminLogMainService.PostData(alm);


                                //Audit Log ends here
                            }
                        }
                    }
                    else
                    {
                        return responseGet;
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Add corporate user action: " + ex.Message);
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object Download(string filter)
        {
            User loginUser = (User)HttpContext.Current.Session["admin"];
            Response response = new Response();
            string message = string.Empty;
            try
            {

                string tempPath = Path.GetTempPath() + "Account_Opening_" + DateTime.Now.ToString("yyyyMMddhhmmssfff");
                FileInfo excelFile = new FileInfo(tempPath);
                message = excelFile.FullName;
                ExcelPackage excel = new ExcelPackage();
                excel.Workbook.Worksheets.Add("Account Opening");

                var headerRow = new List<string[]>()
            {
                new string[] { "S.No.", "Submited Date", "Settlement Date", "Process Status", "Identity Verification", "Document Verification", "Salutation", "Name", "ID No.", "Email", "Gender", "Resident of Malaysia", "Natianality", "Race", "Bumiputera Status", "Date of Birth", "Marital Status", "Residencial Address", "Mailing Address", "Occupation Address", "Mobile No.", "Home Tel No.", "Office Tel No.", "Name of Employer/ Name of Company for Self Employed", "Nature of Business", "Occupation", "Monthly Income", "Purpose", "Estimated NetWorth", "Source of Funds", "Funder Name", "Relationship", "Funders Industory", "Is Funder Money Changer", "Is Funder Beneficial Owner", "Is Funder Beneficial Owner", "Bank Account Type", "Currency", "Bank Name", "Account Name", "Bank Account No.", "PEP Status", "Family PEP Relationship", "CRS Tax Residency Status", "Country of Tax Residence", "NRIC Front", "NRIC Back", "Selfie", "Signature", "Supporting", "Additional", "Additional Document Requested by Admin ", "Public & Local IP Address", "Remarks", "Agent Code" }
            };

                // Determine the header range (e.g. A1:D1)
                int alpha = 0;
                string rangeEnd = "";
                if (headerRow[0].Length + 64 <= 90)
                {
                    rangeEnd = Char.ConvertFromUtf32(headerRow[0].Length + 64);
                }
                else if (headerRow[0].Length + 64 <= 116)
                {
                    alpha = (headerRow[0].Length + 64 - 90);






                    {
                        rangeEnd = "A" + Char.ConvertFromUtf32(alpha + 64);
                    }
                }
                else
                {
                    alpha = (headerRow[0].Length + 64 - 90 - 26);
                    rangeEnd = "B" + Char.ConvertFromUtf32(alpha + 64);
                }
                string headerRange = "A3:" + rangeEnd + "3";

                var worksheet = excel.Workbook.Worksheets["Account Opening"];

                string docDetails = "Account Opening";

                try
                {
                    List<IncomeDTO> incomeDTOs = new List<IncomeDTO>();
                    Response responseMonthlyIncome = ServicesManager.GetMonthlyIncomeData();
                    if (responseMonthlyIncome.IsSuccess)
                    {
                        incomeDTOs = (List<IncomeDTO>)responseMonthlyIncome.Data;

                        // ------------------------------------------------
                        // Creation of header cells
                        // ------------------------------------------------
                        worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                        // ------------------------------------------------
                        // Populate sheet with some real data from list
                        // ------------------------------------------------
                        int row = 4; // start row (in row 1 are header cells)
                        StringBuilder filterQuery = new StringBuilder();

                        //filterQuery.Append(" users.user_role_id='3'");

                        //FilterValue.Value = Request.QueryString["FilterValue"].ToString();
                        //if (FilterValue.Value == "0")
                        //{
                        //    filter.Append(@"and ao.process_status in ('2', '20', '21', '3') ");
                        //}
                        //else if (FilterValue.Value == "1")
                        //{
                        //    filter.Append(@"and ao.process_status in ('4') ");
                        //}
                        //else if (FilterValue.Value == "2")
                        //{
                        //    filter.Append(@"and ao.process_status in ('29', '39') ");
                        //}
                        //else
                        //{

                        //}

                        if (filter != "" && filter != null)
                        {
                            //filter details modify here
                            if (filter != "All")
                            {
                                docDetails += " | Status: " + (filter == "0" ? "Account Opening Request Pending" : (filter == "1" ? "Account Opening Request Approved" : (filter == "2" ? "Account Request Opening Rejected" : "")));
                                if (filter == "0")
                                {
                                    //pending
                                    filterQuery.Append(@"and ao.process_status in ('1','2', '10', '11', '20', '21', '3') ");
                                }
                                else if (filter == "1")
                                {
                                    //approved
                                    filterQuery.Append(@"and ao.process_status in ('4') ");
                                }
                                else if (filter == "2")
                                {
                                    //rejected
                                    filterQuery.Append(@"and ao.process_status in ('29', '39', '49') ");
                                }
                                else
                                {

                                }

                            }
                            else
                            {
                                docDetails += " | Status: All (Pending, Approved, Rejected)";
                                filterQuery.Append(@"and ao.process_status in ('1','2', '29', '10', '11', '20', '21', '3', '39', '4', '49') ");
                            }
                        }
                        else
                        {
                            filterQuery.Append(@"and ao.process_status in ('1','2', '3', '10', '11', '20', '21') ");
                        }

                        string docDetailsRange = "A1:" + Char.ConvertFromUtf32(alpha + 64) + "1";
                        worksheet.Cells[docDetailsRange].Merge = true;
                        worksheet.Cells[docDetailsRange].Value = docDetails;
                        worksheet.Cells[docDetailsRange].Style.Font.Bold = true;

                        worksheet.Cells["A2:Q2"].Merge = true;
                        worksheet.Cells["A2:Q2"].Value = "Account General Info";
                        worksheet.Cells["A2:Q2"].Style.Font.Bold = true;
                        worksheet.Cells["R2:W2"].Merge = true;
                        worksheet.Cells["R2:W2"].Value = "Contact Information";
                        worksheet.Cells["R2:W2"].Style.Font.Bold = true;
                        worksheet.Cells["X2:AA2"].Merge = true;
                        worksheet.Cells["X2:AA2"].Value = "Occupation";
                        worksheet.Cells["X2:AA2"].Style.Font.Bold = true;
                        worksheet.Cells["AB2:AJ2"].Merge = true;
                        worksheet.Cells["AB2:AJ2"].Value = "Financial Profiles";
                        worksheet.Cells["AB2:AJ2"].Style.Font.Bold = true;
                        worksheet.Cells["AK2:AO2"].Merge = true;
                        worksheet.Cells["AK2:AO2"].Value = "Direct Credit Bank";
                        worksheet.Cells["AK2:AO2"].Style.Font.Bold = true;
                        worksheet.Cells["AP2:AS2"].Merge = true;
                        worksheet.Cells["AP2:AS2"].Value = "Disclosure of Identity";
                        worksheet.Cells["AP2:AS2"].Style.Font.Bold = true;
                        worksheet.Cells["AT2:AU2"].Merge = true;
                        worksheet.Cells["AT2:AU2"].Value = "Upload Ddocuments";
                        worksheet.Cells["AT2:AU2"].Style.Font.Bold = true;

                        string mainQ = (@"SELECT ao.ID as c, ao.ID, ao.name, ao.mobile_no, ao.submitted_date, ao.id_no, ao.email, ao.settlement_date, ao.us_citizen, ao.is_mobile_verified, 
                                ao.resident_of_malaysia, ao.nationality, ao.dob, ao.race, ao.bumiputra_status, ao.marital_status, ao.gender, ao.salutation, ao.are_you_pep, ao.pep_status, 
                                ao.f_pep_status, ao.tax_resident_outside_malaysia, ao.tin, ao.declaration, ao.remarks, ao.process_status, ao.status, ao.is_identity_verified, ao.agent_code, 
                                ao.is_documents_verified, ao.principle_id, ao.principle_user_id, u.username, ao.joint_relationship,
                                if(aoa.address_type is null, 0, aoa.address_type) as address_type, 
                                aoa.addr_1, aoa.addr_2, aoa.addr_3, aoa.tel_no, aobd.account_type as bank_account_type, aobd.currency, aobd.account_name, aobd.account_no, 
                                if(aobd.bank_id is null, 0, aobd.bank_id) as bank_id, 
                                aofp.purpose, aofp.source, aofp.employed_by_fund_company, aofp.relationship, aofp.name_of_funder, funders_industory, 
                                if(aofp.is_funder_beneficial_owner is null, 0, aofp.is_funder_beneficial_owner) as is_funder_beneficial_owner, 
                                aofp.fund_owner_name, aofp.estimated_net_worth, aofp.purpose_description, aof.url, aof.created_date,
                                if(aoo.employed is null, 0, aoo.employed) as employed,  
                                aoo.occupation, aoo.designation, aoo.employer_name, aoo.nature_of_business, aoo.monthly_income, 
                                aoo.occupation_others, group_concat(DISTINCT concat(aoc.tax_residency_status, ' - ', if(aoc.tin = '', concat('No TIN - ', aoc.no_tin_reason), aoc.tin)) SEPARATOR ', \r\n') as tax_residency_status, 
                                group_concat(aoc.tin SEPARATOR ',\r\n') as tin, bd.name as bank_name, ao.account_type, ao.register_ip_address, ao.race_description  
                                FROM account_openings ao 
                                left join ao_addresses aoa on ao.ID = aoa.account_opening_id 
                                left join ao_bank_details aobd on ao.ID = aobd.account_opening_id 
                                left join ao_financial_profiles aofp on ao.ID = aofp.account_opening_id 
                                left join ao_occupations aoo on ao.ID = aoo.account_opening_id 
                                left join ao_crs_details aoc on ao.ID = aoc.account_opening_id 
                                left join ao_files aof on ao.ID = aof.account_opening_id 
                                left join banks_def bd on aobd.bank_id = bd.ID 
                                left join users u on ao.principle_user_id = u.id
                                where 1=1 ");
                        filterQuery.Append("group by ao.id ");
                        filterQuery.Append("order by FIELD(ao.process_status, '2','11','21','3','10','20','4','29','39'), ao.submitted_date desc ");
                        Response responseUList = GenericService.GetDataByQuery(mainQ + filterQuery, 0, 0, false, null, false, null, false);
                        if (responseUList.IsSuccess)
                        {
                            var UsersDyn = responseUList.Data;
                            var responseJSON = JsonConvert.SerializeObject(UsersDyn);
                            List<DiOTP.Utility.AccountOpeningRequest> aoRequestList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AccountOpeningRequest>>(responseJSON);

                            StringBuilder asb = new StringBuilder();
                            int index = 1;
                            foreach (DiOTP.Utility.AccountOpeningRequest ao in aoRequestList)
                            {
                                String telNo = "-";
                                String officeTelNo = "-";

                                Response responseOccDesc = GetOccupationDescData(ao.Occupation);
                                OccupationDTO occupationDTO = new OccupationDTO();
                                if (responseOccDesc.IsSuccess)
                                    occupationDTO = (OccupationDTO)responseOccDesc.Data;

                                worksheet.Cells[row, 1].Value = index;
                                worksheet.Cells[row, 2].Value = ao.SubmittedDate == null ? "" : ao.SubmittedDate.ToString("dd/MM/yyyy HH:mm:ss");
                                worksheet.Cells[row, 3].Value = ao.SettlementDate == null || ao.SettlementDate.ToString() == "01/01/0001 00:00:00" ? "-" : ao.SettlementDate.Value.ToString("dd/MM/yyyy HH:mm:ss");
                                worksheet.Cells[row, 4].Value = (ao.ProcessStatus == (int)AOProcessStatus.IDVerified || ao.ProcessStatus == (int)AOProcessStatus.DocVerified || ao.ProcessStatus == (int)AOProcessStatus.UnclearDocsRequested || ao.ProcessStatus == (int)AOProcessStatus.UnclearDocsUploaded || ao.ProcessStatus == (int)AOProcessStatus.Completed || ao.ProcessStatus == (int)AOProcessStatus.DocRequiredByAdmin || ao.ProcessStatus == (int)AOProcessStatus.DocRequiredUploaded ? "Pending " : ao.ProcessStatus == (int)AOProcessStatus.IDVerificationFailed || ao.ProcessStatus == (int)AOProcessStatus.Rejected ? "Rejected " : ao.ProcessStatus == (int)AOProcessStatus.Approved ? "Approved " : "") + " - " + (ao.AccountType == 1 ? "Individual " : (ao.AccountType == 2 ? "Joint (" + ao.JointRelationship + @") for "/* + ao.PrincipleUserId + @" "*/ + ao.Username : "-"));
                                worksheet.Cells[row, 5].Value = ao.IsIdentityVerified == 0 ? "Failed" : "Pass";
                                worksheet.Cells[row, 6].Value = ao.IsDocumentsVerified == 0 ?
                                (
                                    ao.IsAdditionalAdmin == 1 && ao.ProcessStatus == (int)AOProcessStatus.DocRequiredByAdmin ? "Additional Doc Requested" :
                                    (
                                        ao.IsAdditionalAdmin == 1 && ao.ProcessStatus == (int)AOProcessStatus.DocRequiredUploaded ? "Additional Doc Uploaded" :
                                        (
                                            ao.ProcessStatus == (int)AOProcessStatus.UnclearDocsRequested ? "Unclear Doc Requested" :
                                            (
                                                ao.ProcessStatus == (int)AOProcessStatus.UnclearDocsUploaded ? "Unclear Doc Uploaded" : "-"
                                            )
                                        )
                                    )
                                ) :
                                (
                                    ao.ProcessStatus == (int)AOProcessStatus.DocVerified ? "Pass" :
                                    (
                                        ao.ProcessStatus == (int)AOProcessStatus.DocVerificationFailed ? "Failed" : "-"
                                    )
                                );
                                worksheet.Cells[row, 7].Value = ao.Salutation;
                                worksheet.Cells[row, 8].Value = ao.Name;
                                worksheet.Cells[row, 9].Value = ao.IdNo;
                                worksheet.Cells[row, 10].Value = ao.Email;



                                worksheet.Cells[row, 11].Value = ao.Gender;
                                worksheet.Cells[row, 12].Value = (ao.ResidentOfMalaysia == 1 ? "Yes" : "No");
                                worksheet.Cells[row, 13].Value = ao.Nationality;
                                worksheet.Cells[row, 14].Value = (ao.Race == "Others" ? ao.RaceDescription : ao.Race);
                                worksheet.Cells[row, 15].Value = ao.BumiputraStatus;
                                worksheet.Cells[row, 16].Value = ao.Dob;
                                worksheet.Cells[row, 17].Value = ao.MaritalStatus;

                                int num = 18;
                                Response responseAddr = GenericService.PullData<AccountOpeningAddress>(" account_opening_id='" + ao.Id + "' ", 0, 0, false, null);
                                if (responseAddr.IsSuccess)
                                {
                                    var addrsDyn = responseAddr.Data;
                                    var addrsDynJSON = JsonConvert.SerializeObject(addrsDyn);
                                    List<DiOTP.Utility.AccountOpeningAddress> accountOpeningAddresses = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AccountOpeningAddress>>(addrsDynJSON);
                                    accountOpeningAddresses.ForEach(addr =>
                                    {
                                        worksheet.Cells[row, num].Value = addr.Addr1 + ", " + (addr.Addr2 == null || addr.Addr2 == "" ? "" : addr.Addr2 + ", " + (addr.Addr3 == null || addr.Addr3 == "" ? "" : ", " + addr.Addr3)) + (addr.PostCode.ToString() == null || addr.PostCode == "" ? "" : addr.PostCode.ToString()) + " " + (addr.City == null || addr.City == "" ? "" : addr.City) + " " + (addr.State == null || addr.State == "" ? "" : addr.State) + " " + (addr.Country == null || addr.Country == "" ? "" : addr.Country);
                                        num++;
                                        if (addr.AddressType == 2)
                                        {

                                        }
                                        else if (addr.AddressType == 1)
                                        {
                                            telNo = addr.TelNo;

                                        }
                                        else if (addr.AddressType == 3)
                                        {
                                            officeTelNo = addr.TelNo;
                                        }
                                    });
                                    if (accountOpeningAddresses.Count < 3)
                                    {
                                        worksheet.Cells[row, 20].Value = "-";
                                    }
                                }
                                worksheet.Cells[row, 21].Value = ao.MobileNo;
                                worksheet.Cells[row, 22].Value = telNo;
                                worksheet.Cells[row, 23].Value = officeTelNo;
                                worksheet.Cells[row, 24].Value = (ao.EmployerName == null || ao.EmployerName == "" ? "-" : ao.EmployerName.ToString());
                                worksheet.Cells[row, 25].Value = (ao.NatureOfBusiness == null || ao.NatureOfBusiness == "" ? "-" : ao.NatureOfBusiness.ToString());
                                worksheet.Cells[row, 26].Value = ((ao.Occupation == null || ao.Occupation == "" ? "-" : (ao.Occupation == "Others" ? ao.OccupationOthers : (occupationDTO != null ? occupationDTO.OCCCODE.ToString() : ao.Occupation)))); 
                                worksheet.Cells[row, 27].Value = (ao.MonthlyIncome == null || ao.MonthlyIncome == "" ? "-" : (incomeDTOs.FirstOrDefault(m => m.INCOMECODE == ao.MonthlyIncome.ToString()) != null ? incomeDTOs.FirstOrDefault(m => m.INCOMECODE == ao.MonthlyIncome.ToString()).SDESC : ao.MonthlyIncome.ToString()));
                                worksheet.Cells[row, 28].Value = (ao.Purpose == null || ao.Purpose == "" ? "-" : ao.Purpose == "Others" ? ao.PurposeDescription.ToString() : ao.Purpose.ToString());
                                worksheet.Cells[row, 29].Value = (ao.EstimatedNetWorth == null || ao.EstimatedNetWorth == "" ? "-" : ao.EstimatedNetWorth.ToString());
                                worksheet.Cells[row, 30].Value = (ao.Source == null || ao.Source == "" ? "-" : ao.Source.ToString());
                                worksheet.Cells[row, 31].Value = (ao.NameOfFunder == null || ao.NameOfFunder == "" ? "-" : ao.NameOfFunder.ToString());
                                worksheet.Cells[row, 32].Value = (ao.Relationship == null || ao.Relationship == "" || ao.Relationship == "Select" ? "-" : ao.Relationship.ToString());
                                worksheet.Cells[row, 33].Value = (ao.FundersIndustory == null || ao.FundersIndustory == "" || ao.FundersIndustory == "Select" ? "-" : ao.FundersIndustory.ToString());
                                worksheet.Cells[row, 34].Value = (ao.IsFunderMoneyChanger == 0 ? "No" : "Yes");
                                worksheet.Cells[row, 35].Value = (ao.IsFunderBeneficialOwner == 0 ? "No" : "Yes");
                                worksheet.Cells[row, 36].Value = (ao.FundOwnerName == null || ao.FundOwnerName == "" || ao.FundOwnerName == "Select" ? "-" : ao.FundOwnerName.ToString());
                                worksheet.Cells[row, 37].Value = (ao.BankAccountType == null || ao.BankAccountType == "" ? "-" : ao.BankAccountType.ToString());
                                worksheet.Cells[row, 38].Value = (ao.Currency == null || ao.Currency == "" ? "-" : ao.Currency.ToString());
                                worksheet.Cells[row, 39].Value = (ao.BankName == null || ao.BankName == "" ? "-" : ao.BankName.ToString());
                                worksheet.Cells[row, 40].Value = (ao.AccountName == null || ao.AccountName == "" ? "-" : ao.AccountName);
                                worksheet.Cells[row, 41].Value = (ao.AccountNo == null || ao.AccountNo == "" ? "-" : ao.AccountNo.ToString());



                                worksheet.Cells[row, 42].Value = (ao.PepStatus == null || ao.PepStatus == "" ? "-" : ao.PepStatus.ToString());
                                worksheet.Cells[row, 43].Value = (ao.FPepStatus == null || ao.FPepStatus == "" ? "-" : ao.FPepStatus.ToString());
                                worksheet.Cells[row, 44].Value = (ao.TaxResidencyStatus == null ? "No" : "Yes");
                                worksheet.Cells[row, 45].Value = (ao.TaxResidencyStatus == null || ao.TaxResidencyStatus == "" ? "-" : ao.TaxResidencyStatus.ToString());
                                StringBuilder fileString = new StringBuilder();
                                num = 46;
                                Response responseFile = GenericService.PullData<AccountOpeningFile>(" account_opening_id='" + ao.Id + "' order by FIELD(file_type, '1', '2', '3', '4', '6', '5', '7')", 0, 0, false, null);
                                string siteURL = ConfigurationManager.AppSettings["siteURL"];
                                string investorURL = ConfigurationManager.AppSettings["investorURL"];
                                if (responseFile.IsSuccess)
                                {
                                    var filesDyn = responseFile.Data;
                                    var filesDynJSON = JsonConvert.SerializeObject(filesDyn);
                                    List<DiOTP.Utility.AccountOpeningFile> accountOpeningFiles = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiOTP.Utility.AccountOpeningFile>>(filesDynJSON);
                                    accountOpeningFiles.ForEach(file =>
                                    {

                                        //string xx = @"HYPERLINK(" + siteURL + file.Url.TrimStart('/') + ", " + (file.FileType == 1 ? "NRIC Front" : (file.FileType == 2 ? "NRIC Back" : (file.FileType == 3 ? "Selfie" : (file.FileType == 4 ? "Signature" :
                                        //    (file.FileType == 5 ? "Additional" : ""))))) + ")";
                                        worksheet.Cells[row, num].Value = (file.FileType == 1 ? "NRIC Front" : (file.FileType == 2 ? "NRIC Back" : (file.FileType == 3 ? "Selfie" : (file.FileType == 4 ? "Signature" :
                                                (file.FileType == 5 ? "Additional" : (file.FileType == 6 ? "Supporting" : (file.FileType == 7 ? "Additional Document Requested by Admin" : "")))))));
                                        worksheet.Cells[row, num].Hyperlink = new Uri(investorURL + file.Url.TrimStart('/'));

                                        //worksheet.Cells[row, num].Value = (file.FileType == 1 ? "NRIC Front" : (file.FileType == 2 ? "NRIC Back" : (file.FileType == 3 ? "Selfie" : (file.FileType == 4 ? "Signature" :
                                        //    (file.FileType == 5 ? "Additional" : ""))))) + @": " + siteURL + file.Url.TrimStart('/') + @", ";
                                        num++;
                                    });
                                    //documentsTbody.InnerHtml = fileString.ToString();
                                }

                                //worksheet.Cells[row, 52].Value = (ao.CreatedDate == null ? "" : ao.CreatedDate.ToString());
                                worksheet.Cells[row, 53].Value = (ao.RegisterIPAddress == "" ? "" : ao.RegisterIPAddress);
                                worksheet.Cells[row, 54].Value = (ao.Remarks == null || ao.Remarks == "" ? "-" : ao.Remarks.ToString());
                                worksheet.Cells[row, 55].Value = (ao.AgentCode == null || ao.AgentCode == "" ? "-" : ao.AgentCode.ToString());


                                row++;
                                index++;

                            }
                        }

                        worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                        // Apply some predefined styles for data to look nicely :)
                        worksheet.Cells[headerRange].Style.Font.Bold = true;
                        // Save this data as a file
                        excel.SaveAs(excelFile);
                        // Display SUCCESS message
                        response.IsSuccess = true;
                        response.Message = "Downloading...";
                        //Audit Log starts here
                        AdminLogMain alm = new AdminLogMain()
                        {
                            Description = "Account Opening Request List downloaded",
                            TableName = "Account Opening",
                            UpdatedDate = DateTime.Now,
                            UserId = loginUser.Id,
                        };
                        Response responseLog = IAdminLogMainService.PostData(alm);


                        //Audit Log ends here
                    }
                    else
                    {
                        response = responseMonthlyIncome;
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                }
                finally
                {
                    //Empty variables
                    excel = null;
                    worksheet = null;

                    // Force garbage collector cleaning
                    GC.Collect();

                    //change to byte[]
                    byte[] result = File.ReadAllBytes(excelFile.ToString());
                    String file = Convert.ToBase64String(result);
                    //delete temporary file
                    File.Delete(excelFile.ToString());
                    response.Data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + file;
                    response.Message = "Account_Opening_Request_list" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    //response.Data = "data:application/vnd.ms-excel;base64," + file;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + message;
            }
            return response;
        }


        public static Response VerifyeKYC(Trulioo.Client.V1.Model.PersonInfo personInfo, Trulioo.Client.V1.Model.Location location, List<Trulioo.Client.V1.Model.NationalId> NationalIds, Trulioo.Client.V1.Model.Document document)
        {
            //personInfo.FirstGivenName = "Leo";
            //personInfo.FirstSurName = "Moggie";
            //personInfo.DayOfBirth = 1;
            //personInfo.MonthOfBirth = 10;
            //personInfo.YearOfBirth = 1941;
            //NationalIds = new List<Trulioo.Client.V1.Model.NationalId> { new Trulioo.Client.V1.Model.NationalId { Number = "411001-13-5027", Type = "nationalid" } };
            Response response = new Response() { IsSuccess = true };
            try
            {

                Response resAuth = DigitalizationServiceManager.TestAuth();
                if (resAuth.IsSuccess)
                {
                    //Response resEntities = DigitalizationServiceManager.GetTestEntities();
                    //if (resEntities.IsSuccess)
                    {
                        Response resConsents = DigitalizationServiceManager.GetConsents();
                        if (resConsents.IsSuccess)
                        {
                            List<string> Consents = (List<string>)resConsents.Data;
                            Response resDocumentTypes = DigitalizationServiceManager.GetDocumentTypes();
                            if (resDocumentTypes.IsSuccess)
                            {
                                DocumentType documentType = (DocumentType)resDocumentTypes.Data;
                                Trulioo.Client.V1.Model.VerifyRequest verifyRequestBody = new Trulioo.Client.V1.Model.VerifyRequest
                                {
                                    AcceptTruliooTermsAndConditions = true,
                                    CleansedAddress = true,
                                    ConfigurationName = "Identity Verification",
                                    ConsentForDataSources = Consents.ToArray(),
                                    CountryCode = "MY",
                                    DataFields = new Trulioo.Client.V1.Model.DataFields
                                    {
                                        PersonInfo = personInfo,
                                        Location = location,
                                        Document = document,
                                        NationalIds = NationalIds.ToArray()
                                    }
                                };
                                Response resVerify = DigitalizationServiceManager.Verify(verifyRequestBody);
                                if (!resVerify.IsSuccess)
                                    return resVerify;
                                else
                                {
                                    Trulioo.Client.V1.Model.VerifyResult verifyResult = (Trulioo.Client.V1.Model.VerifyResult)resVerify.Data;
                                    response.Data = verifyResult;
                                    if (verifyResult.Record.RecordStatus == "match")
                                    {
                                        response.IsSuccess = true;
                                        string result = JsonConvert.SerializeObject(verifyResult.Record.DatasourceResults);
                                        response.Message = result;
                                    }
                                    else
                                    {
                                        response.IsSuccess = false;
                                        string result = JsonConvert.SerializeObject(verifyResult.Record.DatasourceResults);
                                        response.Message = result;
                                    }
                                }
                            }
                        }
                        else
                        {
                            return resConsents;
                        }
                    }
                    //else
                    {
                        //return resEntities;
                    }
                }
                else
                {
                    return resAuth;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}