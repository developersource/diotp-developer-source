﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Admin
{
    public partial class AddCorporateAccounts : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            string currentDateString = DateTime.Now.ToString("MM/dd/yyyy");
            CreatedBy.Value = "0";
            ModifiedBy.Value = "0";
            CreatedDate.Value = currentDateString;
            ModifiedDate.Value = currentDateString;
            
            string idString = Request.QueryString["id"];
            if (idString != null && idString != "")
            {
                int id = Convert.ToInt32(idString);

                Response response = IUserService.GetSingle(id);
                if (response.IsSuccess)
                {
                    companyIdDiv.Visible = false;
                    LastLoginIp.Attributes.Add("readonly", "readonly");
                    
                    User obj = (User)response.Data;

                    Response responseUA = GenericService.GetDataByFilter<UserAccount>(" user_id='" + obj.Id + "' ", 0, 0, false, null, true, null, false, false, null);
                    if (responseUA.IsSuccess)
                    {
                        UserAccount userAccount = ((List<UserAccount>)responseUA.Data).FirstOrDefault();

                        UserRoleId.Value = obj.UserRoleId.ToString();

                        EmailId.Value = obj.EmailId;
                        MobileNumber.Value = obj.MobileNumber;
                        Password.Value = obj.Password;
                        TransPwd.Value = obj.TransPwd;
                        UniqueKey.Value = obj.UniqueKey;
                        IsOnline.Value = obj.IsOnline.ToString();
                        IsActive.Value = obj.IsActive.ToString();
                        IsPrimary.Value = obj.IsPrimary.ToString();
                        RegisterIp.Value = obj.RegisterIp.ToString();

                        LastLoginOn.Value = obj.LastLoginDate == null ? DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") : obj.LastLoginDate.Value.ToString("MM/dd/yyyy HH:mm:ss");
                        
                        LastLoginIp.Value = userAccount.AccountNo;

                        Id.Value = obj.Id.ToString();
                        Status.Value = obj.Status.ToString();
                        CreatedBy.Value = obj.CreatedBy.ToString();
                        CreatedDate.Value = obj.CreatedDate.ToString("MM/dd/yyyy HH:mm:ss");
                        ModifiedBy.Value = obj.ModifiedBy.ToString();
                        ModifiedDate.Value = obj.ModifiedDate == null ? DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") : obj.ModifiedDate.Value.ToString("MM/dd/yyyy HH:mm:ss");
                    }

                    

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                        "alert('" + response.Message + "');", true);
                }
            }
        }
    }
}