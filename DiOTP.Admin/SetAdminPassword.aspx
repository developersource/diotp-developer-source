﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetAdminPassword.aspx.cs" Inherits="Admin.SetAdminPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <link rel="shortcut icon" href="/Content/images/favicon.html" />
    <title>Set Password</title>
    <link href="Content/bs3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/css/bootstrap-reset.css" rel="stylesheet" />
    <link href="Content/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="Content/css/style.css" rel="stylesheet" />
    <link href="Content/css/style-responsive.css" rel="stylesheet" />
</head>
<body>
    <form class="form-signin" runat="server">
        <h2 class="form-signin-heading">Set Password</h2>
        <div class="login-wrap" id="setPassword" runat="server">
            <div class="user-login-info">
                <asp:TextBox TextMode="Password" ID="txtPassword" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter your New Password"></asp:TextBox>
                <asp:TextBox TextMode="Password" ID="txtConfirmPassword" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Confirm your Password"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Button ID="btnSetPassword" runat="server" CssClass="btn btn-block login-btn" Text="Submit" OnClick="btnSetPassword_Click" />
            </div>
            <div id="divMessage" runat="server"></div>
        </div>
        <div class="login-wrap" id="proceedLogin" runat="server" visible="false">
            <h4 class="text-success text-center" style="margin-bottom: 20px;">Successfully updated</h4>
            <button type="button" class="btn btn-block login-btn" onclick="javascript:window.location.href='Login.aspx';">Login now</button>
        </div>
        <asp:HiddenField ID="hdnLocalIPAddress" runat="server" Value="0" ClientIDMode="Static" />
    </form>

    <script>
/**
        * Get the user IP throught the webkitRTCPeerConnection
        * @param onNewIP {Function} listener function to expose the IP locally
        * @return undefined
        */
        function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
            //compatibility for firefox and chrome
            var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
            var pc = new myPeerConnection({
                iceServers: []
            }),
                noop = function () { },
                localIPs = {},
                ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
                key;

            function iterateIP(ip) {
                if (!localIPs[ip]) onNewIP(ip);
                localIPs[ip] = true;
            }

            //create a bogus data channel
            pc.createDataChannel("");

            // create offer and set local description
            pc.createOffer(function (sdp) {
                sdp.sdp.split('\n').forEach(function (line) {
                    if (line.indexOf('candidate') < 0) return;
                    line.match(ipRegex).forEach(iterateIP);
                });

                pc.setLocalDescription(sdp, noop, noop);
            }, noop);

            //listen for candidate events
            pc.onicecandidate = function (ice) {
                if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
                ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
            };
        }

        // Usage

        getUserIP(function (ip) {
            document.getElementById("hdnLocalIPAddress").value = ip;
        });
    </script>
</body>
</html>
