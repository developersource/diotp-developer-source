﻿namespace DiOTP.WindowServiceApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMigrateDailyNAV = new System.Windows.Forms.Button();
            this.btnGenerateLast12MonthsStatements = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnMigrateDailyNAV
            // 
            this.btnMigrateDailyNAV.Location = new System.Drawing.Point(342, 192);
            this.btnMigrateDailyNAV.Name = "btnMigrateDailyNAV";
            this.btnMigrateDailyNAV.Size = new System.Drawing.Size(113, 23);
            this.btnMigrateDailyNAV.TabIndex = 0;
            this.btnMigrateDailyNAV.Text = "Migrate Daily NAV";
            this.btnMigrateDailyNAV.UseVisualStyleBackColor = true;
            this.btnMigrateDailyNAV.Click += new System.EventHandler(this.BtnMigrateDailyNAV_Click);
            // 
            // btnGenerateLast12MonthsStatements
            // 
            this.btnGenerateLast12MonthsStatements.Location = new System.Drawing.Point(325, 221);
            this.btnGenerateLast12MonthsStatements.Name = "btnGenerateLast12MonthsStatements";
            this.btnGenerateLast12MonthsStatements.Size = new System.Drawing.Size(149, 23);
            this.btnGenerateLast12MonthsStatements.TabIndex = 1;
            this.btnGenerateLast12MonthsStatements.Text = "Generate past 12 months statements";
            this.btnGenerateLast12MonthsStatements.UseVisualStyleBackColor = true;
            this.btnGenerateLast12MonthsStatements.Click += new System.EventHandler(this.btnGenerateLast12MonthsStatements_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnGenerateLast12MonthsStatements);
            this.Controls.Add(this.btnMigrateDailyNAV);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnMigrateDailyNAV;
        private System.Windows.Forms.Button btnGenerateLast12MonthsStatements;
    }
}

