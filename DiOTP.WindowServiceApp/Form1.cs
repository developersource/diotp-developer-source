﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using DiOTP.WindowSerivceApp;
using DiOTP.WindowServiceApp.ServiceCalls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiOTP.WindowServiceApp
{
    public partial class Form1 : Form
    {
        public string ApiBaseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();

        string DailyProcessScheduleHour = ConfigurationManager.AppSettings["DailyProcessScheduleHour"].ToString();
        string OrderRemindScheduleHour = ConfigurationManager.AppSettings["OrderRemindScheduleHour"].ToString();
        string MonthlyProcessScheduleDay = ConfigurationManager.AppSettings["MonthlyProcessScheduleDay"].ToString();

        string OrderRemindScheduleDays = ConfigurationManager.AppSettings["OrderRemindScheduleDays"].ToString();
        string OrderVoidScheduleDays = ConfigurationManager.AppSettings["OrderVoidScheduleDays"].ToString();

        string AccountOpeningRemindScheduleHour = ConfigurationManager.AppSettings["AccountOpeningRemindScheduleHour"].ToString();
        string AccountOpeningRemindScheduleFrequencyBy = ConfigurationManager.AppSettings["AccountOpeningRemindScheduleFrequencyBy"].ToString();
        string AccountOpeningRemindScheduleFrequency = ConfigurationManager.AppSettings["AccountOpeningRemindScheduleFrequency"].ToString();

        bool isProcessingDaily = false;
        bool isProcessingMonthly = false;

        private BackgroundWorker backgroundWorkerProcessRealtime;


        public Form1()
        {
            Logger.WriteLog("Form1");
            InitializeComponent();
            InitializeBackgroundWorker();
        }

        private void InitializeBackgroundWorker()
        {
            Logger.WriteLog("InitializeBackgroundWorker");
            GetFunds();

            backgroundWorkerProcessRealtime = new BackgroundWorker();
            backgroundWorkerProcessRealtime.DoWork += new DoWorkEventHandler(backgroundWorkerProcessRealtime_DoWork);
            backgroundWorkerProcessRealtime.WorkerReportsProgress = false;
            backgroundWorkerProcessRealtime.WorkerSupportsCancellation = true;
            backgroundWorkerProcessRealtime.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorkerProcessRealtime_RunWorkerCompleted);
            backgroundWorkerProcessRealtime.RunWorkerAsync();

        }

        public bool isProcessedDailyCheck(int process_type)
        {
            try
            {
                StringBuilder filter = new StringBuilder();
                filter.Append(@"SELECT * from ws_processes where process_type=" + process_type + " and status=1 and CAST(processed_date AS DATE) = '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ");
                Response responseDailyProcessRow = GenericService.GetDataByQuery(filter.ToString(), 0, 0, false, null, false, null, true);
                if (responseDailyProcessRow.IsSuccess)
                {
                    var dynamicRow = responseDailyProcessRow.Data;
                    var dynToJSON = JsonConvert.SerializeObject(dynamicRow);
                    List<WS_Process> processList = JsonConvert.DeserializeObject<List<WS_Process>>(dynToJSON);
                    if (processList.Count > 0)
                    {
                        WS_Process wS_ProcessDaily = processList.FirstOrDefault();
                        if (wS_ProcessDaily.is_processed == 1)
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("isProcessedDailyCheck: " + ex.Message + " - Exception");
                return true;
            }
            return false;
        }

        public object DailyProcess(DateTime currentTime)
        {
            string[] splitHM = DailyProcessScheduleHour.Split(':');
            Int32 h = Convert.ToInt32(splitHM[0]);
            Int32 m = Convert.ToInt32(splitHM[1]);
            DateTime DailyProcessScheduleHourDT = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, h, m, 0);
            //Process type 1: MA Status
            if (currentTime >= DailyProcessScheduleHourDT && !isProcessedDailyCheck(1) && !isProcessingDaily)
            {
                GetFunds();
                isProcessingDaily = true;
                Logger.WriteLog("DailyProcess MA Status running");
                //Code
                CheckMA();
                Logger.WriteLog("DailyProcess MA Status end");
                isProcessingDaily = false;
            }
            //Process type 2: Daily NAV Price
            if (currentTime >= DailyProcessScheduleHourDT && !isProcessedDailyCheck(2) && !isProcessingDaily)
            {
                isProcessingDaily = true;
                Logger.WriteLog("DailyProcess Daily NAV Price runing");
                //Code
                CheckNAV();
                Logger.WriteLog("DailyProcess Daily NAV Price end");
                isProcessingDaily = false;
            }
            //Process type 3: Fund Information
            if (currentTime >= DailyProcessScheduleHourDT && !isProcessedDailyCheck(3) && !isProcessingDaily)
            {
                isProcessingDaily = true;
                Logger.WriteLog("DailyProcess Fund Information running");
                //Code
                CheckFundInfo();
                Logger.WriteLog("DailyProcess Fund Information end");
                isProcessingDaily = false;
            }
            //Process type 4: Fund Details
            if (currentTime >= DailyProcessScheduleHourDT && !isProcessedDailyCheck(4) && !isProcessingDaily)
            {
                isProcessingDaily = true;
                Logger.WriteLog("DailyProcess Fund Details running");
                //Code
                CheckFundDetails();
                Logger.WriteLog("DailyProcess Fund Details end");
                isProcessingDaily = false;
            }
            //Process type 5: Fund Income Distribution
            if (currentTime >= DailyProcessScheduleHourDT && !isProcessedDailyCheck(5) && !isProcessingDaily)
            {
                isProcessingDaily = true;
                Logger.WriteLog("DailyProcess Fund Income Distribution running");
                //Code
                CheckFundIncomeDistribution();
                Logger.WriteLog("DailyProcess Fund Income Distribution end");
                isProcessingDaily = false;
            }

            string[] splitOHM = OrderRemindScheduleHour.Split(':');
            Int32 oh = Convert.ToInt32(splitOHM[0]);
            Int32 om = Convert.ToInt32(splitOHM[1]);
            DateTime OrderRemindScheduleHourMDT = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, oh, om, 0);
            //Process type 6: Order Reminder
            if (currentTime >= OrderRemindScheduleHourMDT && !isProcessedDailyCheck(6) && !isProcessingDaily)
            {
                isProcessingDaily = true;
                Logger.WriteLog("DailyProcess Order Reminder running");
                //Code
                CheckOrders();
                Logger.WriteLog("DailyProcess Order Reminder end");
                isProcessingDaily = false;
            }

            string[] splitAHM = AccountOpeningRemindScheduleHour.Split(':');
            Int32 ah = Convert.ToInt32(splitAHM[0]);
            Int32 am = Convert.ToInt32(splitAHM[1]);
            DateTime AccountOpeningRemindScheduleHourMDT = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, ah, am, 0);
            //Process type 7: Account Opening Reminder
            if (currentTime >= AccountOpeningRemindScheduleHourMDT && !isProcessedDailyCheck(7) && !isProcessingDaily)
            {
                isProcessingDaily = true;
                Logger.WriteLog("DailyProcess Account Opening Reminder running");
                CheckAOs();
                Logger.WriteLog("DailyProcess Account Opening Reminder end");
                isProcessingDaily = false;
            }
            return "";
        }

        public void CheckMA()
        {
            try
            {
                Response responseMACount = GenericService.GetCountByQuery(" SELECT count(*) FROM user_accounts where status ='1' ");
                if (responseMACount.IsSuccess)
                {
                    Int32 MACount = Convert.ToInt32(responseMACount.Data);
                    decimal p = MACount / 10;
                    Int32 pageNos = Convert.ToInt32(Math.Ceiling(p));
                    List<Response> responses = new List<Response>();
                    for (int i = 0; i < pageNos; i++)
                    {
                        Response responseUAList = GenericService.GetDataByQuery(" SELECT * FROM user_accounts where status ='1' limit " + (i * 10) + ", 10", 0, 0, false, null, false, null, false);
                        var DynUAListString = responseUAList.Data;
                        var responseUAListJSON = JsonConvert.SerializeObject(DynUAListString);
                        List<UserAccount> userAccounts = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserAccount>>(responseUAListJSON);
                        userAccounts.ForEach(UA =>
                        {
                            Logger.WriteLog("Check MA " + UA.AccountNo.PadLeft(10, '0') + " running ");
                            Response responseMaHolderReg = ServicesManager.GetMaHolderRegByAccountNo(UA.AccountNo);
                            responses.Add(responseMaHolderReg);
                            if (responseMaHolderReg.IsSuccess)
                            {
                                MaHolderReg maHolderReg = (MaHolderReg)responseMaHolderReg.Data;
                                if (maHolderReg.HolderStatus == "S" || maHolderReg.HolderStatus == "I")
                                {
                                    UA.Status = (maHolderReg.HolderStatus == "I" ? 0 : (maHolderReg.HolderStatus == "S" ? 9 : 1));
                                    Response responseUAPost = GenericService.UpdateData<UserAccount>(UA);
                                    if (!responseUAPost.IsSuccess)
                                    {
                                        Logger.WriteLog("DailyProcess responseUAPost " + UA.AccountNo + " PostData: " + responseUAPost.Message + " - Exception");
                                    }
                                    responses.Add(responseUAPost);
                                }
                            }
                            else
                            {
                                Logger.WriteLog("DailyProcess responseMaHolderReg " + UA.AccountNo + " PostData: " + responseMaHolderReg.Message + " - Exception");
                            }
                            Logger.WriteLog("Check MA " + UA.AccountNo.PadLeft(10, '0') + " end ");
                        });
                    }
                    bool isExecute = false;
                    if (responses.Count > 0)
                    {
                        if (responses.Where(x => x.IsSuccess == false).ToList().Count == 0)
                        {
                            isExecute = true;
                        }
                    }
                    else
                    {
                        isExecute = true;
                    }
                    if (isExecute)
                    {
                        WS_Process wS_Process = new WS_Process
                        {
                            is_processed = 1,
                            processed_date = DateTime.Now,
                            process_type = 1,
                            status = 1,
                            message = "Success"
                        };
                        Response responsePost = GenericService.PostData<WS_Process>(wS_Process);
                        if (!responsePost.IsSuccess)
                        {
                            Logger.WriteLog("DailyProcess WS_Process PostData: " + responsePost.Message + " - Exception");
                        }
                    }
                }
                else
                {
                    Logger.WriteLog("CheckMA responseMACount: " + responseMACount.Message + " - Exception");
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("CheckMA: " + ex.Message + " - Exception");
            }
        }

        public void CheckNAV()
        {
            List<Response> responses = new List<Response>();
            try
            {
                Response responseUTMCFundInfo = GenericService.GetDataByQuery(" SELECT * FROM utmc_fund_information ", 0, 0, false, null, false, null, false);
                responses.Add(responseUTMCFundInfo);
                if (responseUTMCFundInfo.IsSuccess)
                {
                    var DynUTMCFundInfoString = responseUTMCFundInfo.Data;
                    var responseUTMCFundInfoJSON = JsonConvert.SerializeObject(DynUTMCFundInfoString);
                    List<UtmcFundInformation> utmcFundInformations = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcFundInformation>>(responseUTMCFundInfoJSON);
                    string fundIdsString = String.Join(",", utmcFundInformations.Select(x => "'" + x.IpdFundCode + "'").ToArray());
                    List<CurrentNAVDTO> currentNAVDTOs = ServicesManager.GetcurrentNAV(fundIdsString);
                    currentNAVDTOs.ForEach(currentNAV =>
                    {
                        var responseCJSON = JsonConvert.SerializeObject(currentNAV);
                        Logger.WriteLog("CheckNAV currentNAV " + responseCJSON);
                        Response response = GenericService.GetDataByQuery(" SELECT * FROM utmc_daily_nav_fund where IPD_Fund_Code='" + currentNAV.FUND_ID + "' and CAST(Daily_NAV_Date AS DATE) ='" + currentNAV.TRANS_DT.ToString("yyyy-MM-dd") + "' ", 0, 0, false, null, false, null, false);
                        var DynString = response.Data;
                        var responseJSON = JsonConvert.SerializeObject(DynString);
                        List<UtmcDailyNavFund> utmcDailyNavFunds = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcDailyNavFund>>(responseJSON);
                        if (utmcDailyNavFunds.Count == 0)
                        {
                            UtmcDailyNavFund utmcDailyNavFund = new UtmcDailyNavFund
                            {
                                EpfIpdCode = "IPD000033",
                                IpdFundCode = currentNAV.FUND_ID,
                                DailyNavDate = currentNAV.TRANS_DT,
                                DailyNav = currentNAV.UNIT_IN_ISSUE_PRICE,
                                ReportDate = currentNAV.TRANS_DT,
                                DailyUnitCreated = currentNAV.UNIT_IN_ISSUE,
                                DailyNavEpf = 0.2M,
                                DailyUnitCreatedEpf = 0.4M,
                                DailyUnitPrice = currentNAV.UP_SELL,
                                DailyUnitCreatedEpfAdj = 0.0M,
                            };
                            Response resDNF = GenericService.PostData<UtmcDailyNavFund>(utmcDailyNavFund);
                            responses.Add(resDNF);
                            if (!resDNF.IsSuccess)
                            {
                                Logger.WriteLog("CheckNAV UtmcDailyNavFund " + utmcDailyNavFund.IpdFundCode + " PostData: " + resDNF.Message + " - Exception");
                            }

                            UtmcFundInformation utmcFundInformation = utmcFundInformations.FirstOrDefault(x => x.IpdFundCode == currentNAV.FUND_ID);

                            Response responseUFD = GenericService.GetDataByQuery(" SELECT * FROM utmc_fund_details where utmc_fund_information_id='" + utmcFundInformation.Id + "' ", 0, 0, false, null, false, null, false);
                            var DynUFDString = responseUFD.Data;
                            var responseUFDJSON = JsonConvert.SerializeObject(DynUFDString);
                            List<UtmcFundDetail> utmcFundDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcFundDetail>>(responseUFDJSON);
                            UtmcFundDetail utmcFundDetail = utmcFundDetails.FirstOrDefault();
                            utmcFundDetail.LatestNavPrice = currentNAV.UP_SELL;
                            utmcFundDetail.LatestNavDate = currentNAV.TRANS_DT;
                            Response r1 = GenericService.UpdateData<UtmcFundDetail>(utmcFundDetail);
                            responses.Add(r1);
                            if (!r1.IsSuccess)
                            {
                                Logger.WriteLog("DailyNAV r1 UtmcFundDetail UpdateData: " + r1.Message + " - Exception");
                            }

                            Response responseFI = GenericService.GetDataByQuery(" SELECT * FROM fund_info where FUND_CODE='" + currentNAV.FUND_ID + "' ", 0, 0, false, null, false, null, false);
                            var DynFIString = responseFI.Data;
                            var responseFIJSON = JsonConvert.SerializeObject(DynFIString);
                            List<FundInfo> fundInfos = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FundInfo>>(responseFIJSON);
                            FundInfo fundInfo = fundInfos.FirstOrDefault();
                            fundInfo.CurrentUnitPrice = currentNAV.UP_SELL;
                            fundInfo.CurrentNavDate = currentNAV.TRANS_DT;
                            Response r2 = GenericService.UpdateData<FundInfo>(fundInfo);
                            responses.Add(r2);
                            if (!r2.IsSuccess)
                            {
                                Logger.WriteLog("CheckNAV r2 FundInfo UpdateData: " + r2.Message + " - Exception");
                            }
                        }
                    });
                }
                else
                {
                    Logger.WriteLog("CheckNAV responseUTMCFundInfo: " + responseUTMCFundInfo.Message + " - Exception");
                }
                bool isExecute = false;
                if (responses.Count > 0)
                {
                    if (responses.Where(x => x.IsSuccess == false).ToList().Count == 0)
                    {
                        isExecute = true;
                    }
                }
                else
                {
                    isExecute = true;
                }
                if (isExecute)
                {
                    WS_Process wS_Process = new WS_Process
                    {
                        is_processed = 1,
                        processed_date = DateTime.Now,
                        process_type = 2,
                        status = 1,
                        message = "Success"
                    };
                    Response responsePost = GenericService.PostData<WS_Process>(wS_Process);
                    if (!responsePost.IsSuccess)
                    {
                        Logger.WriteLog("DailyProcess WS_Process PostData: " + responsePost.Message + " - Exception");
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog("CheckNAV: " + ex.Message + " - Exception");
            }
        }


        //*************************************************************************************************************//
        //*************************************************************************************************************//
        //*************************************************************************************************************//
        //*********************************    EXCEPTION: NOT PUBLISHED LATEST API    *********************************//
        //*************************************************************************************************************//
        //*************************************************************************************************************//
        //*************************************************************************************************************//
        //*************************************************************************************************************//
        public void CheckFundInfo()
        {
            Response existingFundsResponse = GenericService.GetDataByQuery(" SELECT * FROM utmc_fund_information ", 0, 0, false, null, false, null, false);
            var DynString = existingFundsResponse.Data;
            var responseJSON = JsonConvert.SerializeObject(DynString);
            List<UtmcFundInformation> utmcFundInformations = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcFundInformation>>(responseJSON);

            string fundIdsString = String.Join(",", utmcFundInformations.Select(x => "'" + x.IpdFundCode + "'").ToArray());
            List<FundInfoDTO> fundInfoDTOs = ServicesManager.GetFundInfoByFundIds(fundIdsString);

            Response responseUFD = GenericService.GetDataByQuery(" SELECT * FROM utmc_fund_details ", 0, 0, false, null, false, null, false);
            var DynUFDString = responseUFD.Data;
            var responseUFDJSON = JsonConvert.SerializeObject(DynUFDString);
            List<UtmcFundDetail> utmcFundDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcFundDetail>>(responseUFDJSON);
            List<CurrentNAVDTO> currentNAVDTOs = ServicesManager.GetcurrentNAV(fundIdsString);


            List<Response> responses = new List<Response>();

            utmcFundInformations.ForEach(x =>
            {
                FundInfoDTO fundInfoDTO = fundInfoDTOs.FirstOrDefault(y => y.FUND_ID == x.IpdFundCode);
                CurrentNAVDTO currentNAVDTO = currentNAVDTOs.FirstOrDefault(y => y.FUND_ID == x.IpdFundCode);

                UtmcFundDetail utmcFundDetail = utmcFundDetails.FirstOrDefault(y => y.UtmcFundInformationId == x.Id);
                utmcFundDetail.LaunchDate = fundInfoDTO.COMMENCE_DT;
                utmcFundDetail.LatestNavPrice = currentNAVDTO.UP_SELL;
                Response r1 = GenericService.UpdateData<UtmcFundDetail>(utmcFundDetail);
                responses.Add(r1);
                if (!r1.IsSuccess)
                {
                    Logger.WriteLog("CheckFundInfo r1 UtmcFundDetail UpdateData: " + r1.Message + " - Exception");
                }


                bool isUpdate = false;
                if (fundInfoDTO.L_NAME != x.FundName)
                {
                    isUpdate = true;
                    x.FundName = fundInfoDTO.L_NAME;
                }
                if (isUpdate)
                {
                    Response updateResponse = GenericService.UpdateData<UtmcFundInformation>(x);
                    responses.Add(updateResponse);
                    if (!updateResponse.IsSuccess)
                    {
                        Logger.WriteLog("DailyProcess Fund Information updateResponse: " + updateResponse.Message + " - Exception");
                    }
                }
            });

            bool isExecute = false;
            if (responses.Count > 0)
            {
                if (responses.Where(x => x.IsSuccess == false).ToList().Count == 0)
                {
                    isExecute = true;
                }
            }
            else
            {
                isExecute = true;
            }
            if (isExecute)
            {
                WS_Process wS_Process = new WS_Process
                {
                    is_processed = 1,
                    processed_date = DateTime.Now,
                    process_type = 3,
                    status = 1,
                    message = "Success"
                };
                Response responsePost = GenericService.PostData<WS_Process>(wS_Process);
                if (!responsePost.IsSuccess)
                {
                    Logger.WriteLog("DailyProcess WS_Process PostData: " + responsePost.Message + " - Exception");
                }
            }

        }

        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        private static readonly Lazy<IFundInfoService> lazyIFundInfoServiceObj = new Lazy<IFundInfoService>(() => new FundInfoService());
        public static IFundInfoService IFundInfoService { get { return lazyIFundInfoServiceObj.Value; } }

        private static readonly Lazy<IFundReturnService> lazyIFundReturnServiceObj = new Lazy<IFundReturnService>(() => new FundReturnService());
        public static IFundReturnService IFundReturnService { get { return lazyIFundReturnServiceObj.Value; } }

        private static readonly Lazy<IFundChartInfoService> lazyIFundChartInfoServiceObj = new Lazy<IFundChartInfoService>(() => new FundChartInfoService());
        public static IFundChartInfoService IFundChartInfoService { get { return lazyIFundChartInfoServiceObj.Value; } }
        public void CheckFundDetails()
        {
            try
            {
                List<Response> responses = new List<Response>();
                Response responseUTMCFundInfo = GenericService.GetDataByQuery(" SELECT * FROM utmc_fund_information ", 0, 0, false, null, false, null, false);
                if (responseUTMCFundInfo.IsSuccess)
                {
                    var DynUTMCFundInfoString = responseUTMCFundInfo.Data;
                    var responseUTMCFundInfoJSON = JsonConvert.SerializeObject(DynUTMCFundInfoString);
                    List<UtmcFundInformation> utmcFundInformations = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcFundInformation>>(responseUTMCFundInfoJSON);
                    string fundIdsString = String.Join(",", utmcFundInformations.Select(x => "'" + x.IpdFundCode + "'").ToArray());

                    List<CurrentNAVDTO> currentNAVDTOs = ServicesManager.GetcurrentNAV(fundIdsString);
                    List<FundDetailedInformation> FundDetailedInformations = IUtmcFundInformationService.GetFundDetailedInformation();
                    Int32[] selectedFunds = FundDetailedInformations.Select(x => x.UtmcFundInformation.Id).ToArray();
                    Int32[] dataValues = new Int32[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
                    foreach (FundDetailedInformation fundDetailedInformation in FundDetailedInformations)
                    {
                        CurrentNAVDTO currentNAVDTO = currentNAVDTOs.FirstOrDefault(x => x.FUND_ID == fundDetailedInformation.UtmcFundInformation.IpdFundCode);
                        Logger.WriteLog("CheckFundDetails currentNAVDTO: " + currentNAVDTO.FUND_ID + " - " + currentNAVDTO.TRANS_DT.ToString("dd/MM/yyyy") + " - " + currentNAVDTO.UP_SELL);
                        CurrentNAVDTO prev1DayNAVDTO = ServicesManager.GetNAVByFundAndDate("'" + currentNAVDTO.FUND_ID + "'", currentNAVDTO.TRANS_DT.ToString("yyyy-MM-dd") + "prev");
                        Logger.WriteLog("CheckFundDetails prev1DayNAVDTO: " + JsonConvert.SerializeObject(prev1DayNAVDTO));
                        Logger.WriteLog("CheckFundDetails prev1DayNAVDTO: " + prev1DayNAVDTO.FUND_ID + " - " + prev1DayNAVDTO.TRANS_DT.ToString("dd/MM/yyyy") + " - " + prev1DayNAVDTO.UP_SELL);
                        string propertyName = nameof(FundReturn.FundCode);
                        bool isUpdated = false;
                        Response responseFIList = IFundInfoService.GetDataByFilter(" fund_code='" + fundDetailedInformation.UtmcFundInformation.IpdFundCode + "' ", 0, 0, false);
                        responses.Add(responseFIList);
                        if (responseFIList.IsSuccess)
                        {
                            List<FundInfo> fundInfos = (List<FundInfo>)responseFIList.Data;
                            if (fundInfos.Count != 0)
                                isUpdated = fundInfos.FirstOrDefault().UpdatedDate.ToString("yyyy-MM-dd") == DateTime.Now.ToString("yyyy-MM-dd");
                            if (!isUpdated)
                            {
                                FundInfo fI = new FundInfo();
                                fI.ChangePrice = (currentNAVDTO.UP_SELL - prev1DayNAVDTO.UP_SELL);//fundDetailedInformation.Change;
                                fI.ChangePer = (fI.ChangePrice / prev1DayNAVDTO.UP_SELL) * 100;//fundDetailedInformation.ChangePercent;
                                fI.CurrentNavDate = currentNAVDTO.TRANS_DT;
                                fI.CurrentUnitPrice = currentNAVDTO.UP_SELL;
                                fI.FundCode = fundDetailedInformation.UtmcFundInformation.IpdFundCode;
                                fI.UpdatedDate = DateTime.Now;
                                if (fundInfos.Count > 0)
                                {
                                    if (fundInfos.FirstOrDefault().UpdatedDate.ToString("yyyy-MM-dd") != DateTime.Now.ToString("yyyy-MM-dd"))
                                    {
                                        fI.Id = fundInfos.FirstOrDefault().Id;
                                        Response responseUpdate = IFundInfoService.UpdateData(fI);
                                        responses.Add(responseUpdate);
                                    }
                                }
                                else
                                {
                                    Response responsePost = IFundInfoService.PostData(fI);
                                    responses.Add(responsePost);
                                }
                            }
                            Int32[] allSelectedFunds = new Int32[] { fundDetailedInformation.UtmcFundInformation.Id };
                            Response responseFRList = IFundReturnService.GetDataByPropertyName(propertyName, fundDetailedInformation.UtmcFundInformation.IpdFundCode, true, 0, 0, false);
                            responses.Add(responseFRList);
                            if (responseFRList.IsSuccess)
                            {
                                List<FundReturn> frData = (List<FundReturn>)responseFRList.Data;
                                if (frData.Count != 0)
                                    isUpdated = frData.FirstOrDefault().UpdatedDate.ToString("yyyy-MM-dd") == DateTime.Now.ToString("yyyy-MM-dd");
                                if (!isUpdated)
                                {
                                    FundReturn fr = new FundReturn();
                                    fr.FundCode = fundDetailedInformation.UtmcFundInformation.IpdFundCode;
                                    fr.UpdatedDate = DateTime.Now;
                                    foreach (Int32 dataValue in dataValues)
                                    {
                                        Logger.WriteLog("CheckFundDetails fundChartInformation: " + allSelectedFunds + " - " + dataValue);
                                        FundChartInformation fundChartInformation = IUtmcFundInformationService.GenerateFundChartInfo(allSelectedFunds, dataValue).FirstOrDefault();
                                        if (fundChartInformation != null)
                                        {
                                            Response responsePost = IFundChartInfoService.PostChartData(fundChartInformation, dataValue);
                                            responses.Add(responsePost);
                                        }
                                        if (fundChartInformation != null && dataValue != 1 && dataValue != 2)
                                        {
                                            switch (dataValue)
                                            {
                                                case 3:
                                                    fr.OneWeek = Convert.ToDecimal(fundChartInformation.totalReturns);
                                                    break;
                                                case 4:
                                                    fr.OneMonth = Convert.ToDecimal(fundChartInformation.totalReturns);
                                                    break;
                                                case 5:
                                                    fr.ThreeMonth = Convert.ToDecimal(fundChartInformation.totalReturns);
                                                    break;
                                                case 6:
                                                    fr.SixMonth = Convert.ToDecimal(fundChartInformation.totalReturns);
                                                    break;
                                                case 7:
                                                    fr.OneYear = Convert.ToDecimal(fundChartInformation.totalReturns);
                                                    break;
                                                case 8:
                                                    fr.TwoYear = Convert.ToDecimal(fundChartInformation.totalReturns);
                                                    break;
                                                case 9:
                                                    fr.ThreeYear = Convert.ToDecimal(fundChartInformation.totalReturns);
                                                    break;
                                                case 10:
                                                    fr.FiveYear = Convert.ToDecimal(fundChartInformation.totalReturns);
                                                    break;
                                                case 11:
                                                    fr.TenYear = Convert.ToDecimal(fundChartInformation.totalReturns);
                                                    break;
                                            }
                                        }
                                    }
                                    if (frData.Count > 0)
                                    {
                                        if (frData.FirstOrDefault().UpdatedDate.ToString("yyyy-MM-dd") != DateTime.Now.ToString("yyyy-MM-dd"))
                                        {
                                            fr.Id = frData.FirstOrDefault().Id;
                                            Response responseUpdate = IFundReturnService.UpdateData(fr);
                                            responses.Add(responseUpdate);
                                        }
                                    }
                                    else
                                    {
                                        Response responsePost = IFundReturnService.PostData(fr);
                                        responses.Add(responsePost);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    Logger.WriteLog("CheckFundDetails responseUTMCFundInfo: " + responseUTMCFundInfo.Message + " - Exception");
                }
                bool isExecute = false;
                if (responses.Count > 0)
                {
                    if (responses.Where(x => x.IsSuccess == false).ToList().Count == 0)
                    {
                        isExecute = true;
                    }
                }
                else
                {
                    isExecute = true;
                }
                if (isExecute)
                {
                    WS_Process wS_Process = new WS_Process
                    {
                        is_processed = 1,
                        processed_date = DateTime.Now,
                        process_type = 4,
                        status = 1,
                        message = "Success"
                    };
                    Response responsePost = GenericService.PostData<WS_Process>(wS_Process);
                    if (!responsePost.IsSuccess)
                    {
                        Logger.WriteLog("DailyProcess WS_Process PostData: " + responsePost.Message + " - Exception");
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog("CheckFundDetails: " + ex.Message + " - Exception");
            }
        }



        private static readonly Lazy<IUtmcFundCorporateActionService> lazyIUtmcFundCorporateActionServiceObj = new Lazy<IUtmcFundCorporateActionService>(() => new UtmcFundCorporateActionService());
        public static IUtmcFundCorporateActionService IUtmcFundCorporateActionService { get { return lazyIUtmcFundCorporateActionServiceObj.Value; } }
        public void CheckFundIncomeDistribution()
        {
            List<Response> responses = new List<Response>();
            try
            {
                Response responseUTMCFundCorp = GenericService.GetDataByQuery(" SELECT c.IPD_Fund_Code, MAX(c.Corporate_Action_Date) as Corporate_Action_Date FROM utmc_fund_corporate_actions c group by c.IPD_Fund_Code ", 0, 0, false, null, false, null, false);
                responses.Add(responseUTMCFundCorp);
                if (responseUTMCFundCorp.IsSuccess)
                {
                    var DynUTMCFundCorpString = responseUTMCFundCorp.Data;
                    var responseUTMCFundCorpJSON = JsonConvert.SerializeObject(DynUTMCFundCorpString);
                    List<UtmcFundCorporateAction> utmcFundCorporateActionsCurrentMax = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcFundCorporateAction>>(responseUTMCFundCorpJSON);
                    utmcFundCorporateActionsCurrentMax.ForEach(x =>
                    {
                        //ORACLE QUERY
                        //SELECT FUND_ID,(div_tax_exem + div_tax + div_equalsn_rt), distn_dt ,NET_DIV_RT,reinv_dt FROM UTS.NEW_DIV_DISTN
                        //WHERE
                        //FUND_ID IN('01', '02', '03', '04', '05', '06', '07', '08', '09')
                        //ORDER BY FUND_ID, DISTN_DT desc
                        //IMPLEMENT
                        string query = "SELECT FUND_ID,DIV_TAX_EXEM,DIV_TAX,DIV_EQUALSN_RT,DISTN_DT,NET_DIV_RT,REINV_DT FROM UTS.NEW_DIV_DISTN WHERE FUND_ID IN ('" + x.IpdFundCode + "') and DISTN_DT > TO_DATE('" + x.CorporateActionDate.ToString("yyyy-MM-dd") + "', 'yyyy/mm/dd') ORDER BY FUND_ID, DISTN_DT desc";
                        Response responseFundIncomeDist = ServicesManager.SyncFundIncomeDistribution(query);
                        responses.Add(responseFundIncomeDist);
                        if (responseFundIncomeDist.IsSuccess)
                        {
                            List<FundIncomeDistDTO> fundIncomeDistDTOs = (List<FundIncomeDistDTO>)responseFundIncomeDist.Data;
                            fundIncomeDistDTOs.ForEach(fid =>
                            {
                                UtmcFundCorporateAction utmcFundCorporateAction = new UtmcFundCorporateAction
                                {
                                    EpfIpdCode = "IPD000033",
                                    IpdFundCode = fid.FUNDID,
                                    CorporateActionDate = fid.DISTNDT,
                                    Distributions = (fid.DIVTAXEXEM + fid.DIVTAX + fid.DIVEQUALSNRT),
                                    UnitSplits = 0.0000M,
                                    ReportDate = fid.REINVDT,
                                    NetDistribution = fid.NETDIVRT
                                };
                                Response responseUpdate = IUtmcFundCorporateActionService.PostData(utmcFundCorporateAction);
                                responses.Add(responseUpdate);
                                if (!responseUpdate.IsSuccess)
                                {
                                    Logger.WriteLog("CheckFundIncomeDistribution responseUpdate " + fid.FUNDID + ", " + fid.DISTNDT + ": " + responseUpdate.Message + " - Exception");
                                }
                            });
                        }
                        else
                        {
                            Logger.WriteLog("CheckFundIncomeDistribution responseFundIncomeDist: " + responseFundIncomeDist.Message + " - Exception");
                        }

                    });
                }
                else
                {
                    Logger.WriteLog("CheckFundIncomeDistribution responseUTMCFundCorp: " + responseUTMCFundCorp.Message + " - Exception");
                }

                bool isExecute = false;
                if (responses.Count > 0)
                {
                    if (responses.Where(x => x.IsSuccess == false).ToList().Count == 0)
                    {
                        isExecute = true;
                    }
                }
                else
                {
                    isExecute = true;
                }
                if (isExecute)
                {
                    WS_Process wS_Process = new WS_Process
                    {
                        is_processed = 1,
                        processed_date = DateTime.Now,
                        process_type = 5,
                        status = 1,
                        message = "Success"
                    };
                    Response responsePost = GenericService.PostData<WS_Process>(wS_Process);
                    if (!responsePost.IsSuccess)
                    {
                        Logger.WriteLog("DailyProcess WS_Process PostData: " + responsePost.Message + " - Exception");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("CheckFundIncomeDistribution: " + ex.Message + " - Exception");
            }
        }
        private void backgroundWorkerProcessRealtime_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = RealtimeProcess(worker, e);
        }
        private void backgroundWorkerProcessRealtime_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                //MessageBox.Show(e.Error.Message);
            }
            else if (e.Cancelled)
            {
            }
            else
            {
                backgroundWorkerProcessRealtime.RunWorkerAsync();
            }
        }

        public object RealtimeProcess(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool isConnectionSuccessfull = false;
            int idx = 0;
            while (!isConnectionSuccessfull)
            {
                Response responseCheckConn = ServicesManager.CheckConnection();
                if (responseCheckConn.IsSuccess)
                {
                    if (!responseCheckConn.IsApiAvailable)
                    {
                        if (idx == 0)
                            Logger.WriteLog("InitializeBackgroundWorker responseCheckConn !IsApiAvailable: " + responseCheckConn.Message);
                    }
                    else if (!responseCheckConn.IsDBAvailable)
                    {
                        if (idx == 0)
                            Logger.WriteLog("InitializeBackgroundWorker responseCheckConn !IsDBAvailable: " + responseCheckConn.Message);
                    }
                    else
                    {
                        isConnectionSuccessfull = true;
                    }
                }
                else
                {
                    if (idx == 0)
                        Logger.WriteLog("InitializeBackgroundWorker responseCheckConn !IsSuccess: " + responseCheckConn.Message);
                }
                idx++;
            }
            //Logger.WriteLog("InitializeBackgroundWorker responseCheckConn idx: " + idx);
            if (isConnectionSuccessfull)
            {
                try
                {
                    DailyProcess(DateTime.Now);
                    MonthlyProcess(DateTime.Now);
                    Response responseDailyNAVFunds = GenericService.GetDataByQuery(@" SELECT * FROM utmc_daily_nav_fund u
                inner join
                (
                    select max(Daily_NAV_Date) as latestDate, IPD_Fund_Code from utmc_daily_nav_fund group by IPD_Fund_Code
                ) maxU on(maxU.latestDate = u.Daily_NAV_Date and maxU.IPD_Fund_Code = u.IPD_Fund_Code) inner join utmc_fund_information f on u.IPD_Fund_Code = f.IPD_Fund_Code order by u.IPD_Fund_Code ", 0, 0, false, null, false, null, false);
                    if (responseDailyNAVFunds.IsSuccess)
                    {
                        var dynamicRow = responseDailyNAVFunds.Data;
                        var dynToJSON = JsonConvert.SerializeObject(dynamicRow);
                        List<UtmcDailyNavFund> utmcDailyNavFunds = JsonConvert.DeserializeObject<List<UtmcDailyNavFund>>(dynToJSON);

                        List<UserNotificationSetting> userNotificationSetting = new List<UserNotificationSetting>();
                        if (utmcDailyNavFunds != null && utmcDailyNavFunds.Count != 0)
                        {
                            foreach (var fund in utmcDailyNavFunds)
                            {
                                Response responseUNS = GenericService
                             .PullData<UserNotificationSetting>(" fund_code = '" + fund.IpdFundCode + "' and status=1 and notification_type_def_id in (2, 4) and datediff(NOW(),created_date) >= 1 ", 0, 0, false, nameof(UserNotificationSetting.Id))
                             .LeftJoin(nameof(UserNotificationSetting.UserIdUser))
                             .LeftJoin(nameof(UserNotificationSetting.UserAccountIdUserAccount));
                                if (responseUNS.IsSuccess)
                                {
                                    userNotificationSetting = (List<UserNotificationSetting>)responseUNS.Data;
                                    foreach (UserNotificationSetting uns in userNotificationSetting.Where(a => a.NotificationTypeDefId == 2 && ((a.ExpectedPrice >= a.ActualPrice && fund.DailyUnitPrice >= a.ExpectedPrice) || (a.ExpectedPrice <= a.ActualPrice && fund.DailyUnitPrice <= a.ExpectedPrice))).ToList())
                                    {
                                        // send mail
                                        Email email = new Email();
                                        email.body = "";
                                        email.subject = "Found Notification";
                                        email.user = uns.UserIdUser;
                                        String ma = uns.UserAccountIdUserAccount.AccountNo;
                                        String title = "Fund Price Target (MYR) reached";
                                        String content = "You have received this email because one of the funds that you are currently tracking under your fund watchlist has hit the “Fund Price Target (MYR)”\n \n <br/><br/>" + fund.FundName + " has hit “Fund Price Target (MYR)”,<br/> Current NAV per unit: " + uns.ActualPrice.ToString() + "\n ";
                                        EmailService.SendAlertUpdateMail(email, ma, title, content, true, Application.StartupPath + "\\EmailTemplate\\UpdateAlert.html");
                                        uns.Status = 11;
                                        GenericService.UpdateData<UserNotificationSetting>(uns);
                                    }

                                    foreach (UserNotificationSetting uns in userNotificationSetting.Where(a => a.NotificationTypeDefId == 4).ToList())
                                    {
                                        List<HolderInv> holderInvs = ServicesManager.GetHolderInvByHolderNo(uns.UserAccountIdUserAccount.AccountNo, utmcFundInformations);
                                        HolderInv holderInv = holderInvs.FirstOrDefault(x => x.FundId == uns.FundCode);
                                        if (holderInv != null && Math.Round(holderInv.HldrAveCost, 4) >= uns.ExpectedPrice)
                                        {
                                            // send mail
                                            Email email = new Email();
                                            email.body = "";
                                            email.subject = "Found Notification";
                                            email.user = uns.UserIdUser;
                                            String ma = uns.UserAccountIdUserAccount.AccountNo;
                                            String title = "Fund Performance Target (%) reached";
                                            String content = "You have received this email because one of the funds that you are currently tracking under your fund watchlist has hit the “Fund Performance Target (%))”\n \n " + uns.FundCode.ToString() + " has hit “Fund Performance Target (%)” of " + uns.Value.ToString() + ", Holder average cost: " + uns.ActualPrice.ToString() + "\n ";
                                            EmailService.SendAlertUpdateMail(email, ma, title, content, true, Application.StartupPath + "\\EmailTemplate\\UpdateAlert.html");
                                            uns.Status = 11;
                                            GenericService.UpdateData<UserNotificationSetting>(uns);
                                        }
                                    }
                                }
                                else
                                {
                                    Logger.WriteLog("WatchListNotification responseUNS: " + responseUNS.Message + " - Exception");
                                }
                            }

                        }

                    }

                    Response responseUNSStatement = GenericService
                             .PullData<UserNotificationSetting>(" status=1 and notification_type_def_id in (5, 6) ", 0, 0, false, nameof(UserNotificationSetting.Id))
                             .LeftJoin(nameof(UserNotificationSetting.UserIdUser))
                             .LeftJoin(nameof(UserNotificationSetting.UserAccountIdUserAccount));
                    if (responseUNSStatement.IsSuccess)
                    {
                        List<UserNotificationSetting> StatementUserNotificationSettings = (List<UserNotificationSetting>)responseUNSStatement.Data;
                        if (StatementUserNotificationSettings.Count > 0)
                        {
                            Logger.WriteLog("RealtimeProcess Statement notification running");
                            StatementUserNotificationSettings.ForEach(uns =>
                            {
                                string path = uns.Value;
                                string filename = path.Replace("/Content/Statements/Others/", "");
                                string fileName = filename.Replace(".PDF", "").Replace(".pdf", "");
                                string[] keys = fileName.Split('_');
                                int index = 5;
                                if (index == keys.Length)
                                {
                                    string accNo = keys[0];
                                    string fundId = keys[1].ToUpper();
                                    string statementType = keys[2].ToUpper();
                                    string statementDate = keys[3].ToUpper();
                                    string statementTag = keys[4].ToUpper();
                                    UtmcFundInformation utmcFundInformation = utmcFundInformations.FirstOrDefault(f => f.IpdFundCode == fundId);

                                    // send mail
                                    Email email = new Email();
                                    email.body = "The " + (uns.NotificationTypeDefId == 5 ? "Income Distribution" : (uns.NotificationTypeDefId == 6 ? "Unit Split" : "")) + " Statement for Apex " + utmcFundInformation.FundName + " is ready for viewing.";
                                    email.subject = "Statement Notification";
                                    email.user = uns.UserIdUser;
                                    String ma = uns.UserAccountIdUserAccount.AccountNo;
                                    String title = (uns.NotificationTypeDefId == 5 ? "Income Distribution Declaration Notification" : (uns.NotificationTypeDefId == 6 ? "Unit Split Declaration Notification" : ""));
                                    String content = "Please log on to eApexIs at www.xxxxxxxxxxxxxxxx to view or print your " + (uns.NotificationTypeDefId == 5 ? "Income Distribution details." : (uns.NotificationTypeDefId == 6 ? "Unit Split details." : "")) + "\n ";



                                    EmailService.SendAlertUpdateMail(email, ma, title, content, true, Application.StartupPath + "\\EmailTemplate\\UpdateAlert.html");
                                    uns.Status = 11;
                                    GenericService.UpdateData<UserNotificationSetting>(uns);
                                }
                            });
                            Logger.WriteLog("RealtimeProcess Statement notification end");
                        }
                    }

                }
                catch (Exception ex)
                {
                    Logger.WriteLog("WatchListNotification: " + ex.Message + " - Exception");
                }
            }
            return "";
        }

        public Response isProcessedMonthlyCheck(int process_type)
        {
            Response response = new Response();
            try
            {
                StringBuilder filter = new StringBuilder();
                filter.Append(@"SELECT * from ws_processes where process_type=" + process_type + " and status=0 and MONTH(processed_date) = '" + DateTime.Now.ToString("MM") + "' and YEAR(processed_date) = '" + DateTime.Now.ToString("yyyy") + "' order by ID desc ");
                Response responseDailyProcessRow = GenericService.GetDataByQuery(filter.ToString(), 0, 0, false, null, false, null, true);
                if (responseDailyProcessRow.IsSuccess)
                {
                    var dynamicRow = responseDailyProcessRow.Data;
                    var dynToJSON = JsonConvert.SerializeObject(dynamicRow);
                    List<WS_Process> processList = JsonConvert.DeserializeObject<List<WS_Process>>(dynToJSON);
                    if (processList.Count > 0)
                    {
                        WS_Process wS_ProcessDaily = processList.FirstOrDefault();
                        if (wS_ProcessDaily.is_processed == 1)
                        {
                            response.IsSuccess = true;
                            response.Data = wS_ProcessDaily;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Logger.WriteLog("isProcessedDailyCheck: " + ex.Message + " - Exception");
                //return true;
            }
            return response;
        }
        public object MonthlyProcess(DateTime currentTime)
        {
            //Process type 101: Generate Monthly Statement
            Response response = isProcessedMonthlyCheck(101);
            if (response.IsSuccess && !isProcessingMonthly)
            {
                isProcessingMonthly = true;
                Logger.WriteLog("MonthlyProcess Statement running");
                //Code
                GenerateMonthlyReport(currentTime, response);
                Logger.WriteLog("MonthlyProcess Statement end");
                isProcessingMonthly = false;
            }
            return "";
        }
        private static readonly Lazy<IAdminLogMainService> lazyAdminLogMainServiceObj = new Lazy<IAdminLogMainService>(() => new AdminLogMainService());

        public static IAdminLogMainService IAdminLogMainService { get { return lazyAdminLogMainServiceObj.Value; } }

        private static readonly Lazy<IAdminLogSubService> lazyIAdminLogSuberviceObj = new Lazy<IAdminLogSubService>(() => new AdminLogSubService());

        public static IAdminLogSubService IAdminLogSubService { get { return lazyIAdminLogSuberviceObj.Value; } }
        public void GenerateMonthlyReport(DateTime currentTime, Response response)
        {
            GetFunds();
            try
            {
                DateTime fromDate = new DateTime(currentTime.Year, currentTime.Month, 1);
                fromDate = fromDate.AddMonths(-1);
                DateTime toDate = new DateTime(fromDate.Year, fromDate.Month, DateTime.DaysInMonth(fromDate.Year, fromDate.Month));
                StatementService.GenerateMonthlyReport(fromDate, toDate, utmcFundInformations);

                WS_Process wS_ProcessExisting = (WS_Process)response.Data;
                wS_ProcessExisting.status = 1;
                Response responseUpdate = GenericService.UpdateData<WS_Process>(wS_ProcessExisting);
                if (responseUpdate.IsSuccess)
                {
                    StringBuilder filter = new StringBuilder();

                    string mainQ = (@"SELECT ulm.ID as c, ulm.ID, ulm.table_name, ulm.description, ulm.user_id, u.username, u.id_no, ut.user_type_id, ulm.updated_date, uls.column_name, uls.value_old, uls.value_new FROM ");
                    filter.Append(@" admin_log_main ulm 
                            left join admin_log_sub uls on uls.admin_log_main_id = ulm.id
                            left join users u on u.id = ulm.user_id 
                            left join user_types ut on ut.user_id = u.id where table_name='user_statements' and description='Generate Monthly Statements / Check Status clicked' order by ulm.id desc limit 1 ");
                    Response responseULMList = GenericService.GetDataByQuery(mainQ + filter.ToString(), 0, 0, false, null, false, null, true);
                    if (responseULMList.IsSuccess)
                    {
                        var UlmsDyn = responseULMList.Data;
                        var responseJSON = JsonConvert.SerializeObject(UlmsDyn);
                        List<AuditLog> userLogMains = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AuditLog>>(responseJSON);
                        AuditLog auditLog = userLogMains.FirstOrDefault();
                        if (auditLog != null)
                        {
                            //Audit Log starts here
                            AdminLogMain alm = new AdminLogMain()
                            {
                                Description = "Monthly Statements Succesfully Generated",
                                TableName = "user_statements",
                                UpdatedDate = DateTime.Now,
                                UserId = auditLog.user_id
                            };
                            Response responseLog = IAdminLogMainService.PostData(alm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            else
                            {

                            }
                            //Audit Log Ends here
                        }

                        Response responseUser = IUserService.GetSingle(auditLog.user_id);
                        if (responseUser.IsSuccess)
                        {
                            User user = (User)responseUser.Data;
                            WS_Process wS_Process = new WS_Process
                            {
                                is_processed = 1,
                                processed_date = DateTime.Now,
                                process_type = 101,
                                status = 1,
                                message = "Successfully Generated (Clicked by " + user.Username + ")"
                            };
                            Response responsePost = GenericService.PostData<WS_Process>(wS_Process);
                            if (!responsePost.IsSuccess)
                            {
                                Logger.WriteLog("GenerateMonthlyReport wS_Process responsePost: " + responsePost.Message + " - Exception");
                            }
                        }
                    }



                }
                else
                {
                    Logger.WriteLog("GenerateMonthlyReport wS_ProcessExisting responseUpdate: " + responseUpdate.Message + " - Exception");
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog("GenerateMonthlyReport: Exception - " + ex.Message);
            }
        }



        private static readonly Lazy<IUtmcDailyNavFundService> lazyUtmcDailyNavFundObj = new Lazy<IUtmcDailyNavFundService>(() => new UtmcDailyNavFundService());
        public static IUtmcDailyNavFundService IUtmcDailyNavFundService { get { return lazyUtmcDailyNavFundObj.Value; } }
        private void BtnMigrateDailyNAV_Click(object sender, EventArgs e)
        {
            try
            {
                Response responseUTMCFundInfo = GenericService.GetDataByQuery(" SELECT * FROM utmc_fund_information ", 0, 0, false, null, false, null, false);
                if (responseUTMCFundInfo.IsSuccess)
                {
                    var DynUTMCFundInfoString = responseUTMCFundInfo.Data;
                    var responseUTMCFundInfoJSON = JsonConvert.SerializeObject(DynUTMCFundInfoString);
                    List<UtmcFundInformation> utmcFundInformations = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcFundInformation>>(responseUTMCFundInfoJSON);


                    //List<CurrentNAVDTO> NAVHistory = ServicesManager.GetNAVHistoryByFund("'09'", 0);
                    //NAVHistory.Where(y => y.TRANS_DT > new DateTime(2019, 2, 28)).ToList().ForEach(y =>
                    // {
                    //     UtmcDailyNavFund utmcDailyNavFund = new UtmcDailyNavFund()
                    //     {
                    //         EpfIpdCode = "IPD000033",
                    //         IpdFundCode = y.FUND_ID,
                    //         DailyNavDate = y.TRANS_DT,
                    //         DailyNav = y.UNIT_IN_ISSUE_PRICE,
                    //         ReportDate = DateTime.Now,
                    //         DailyUnitCreated = y.UNIT_IN_ISSUE,
                    //         DailyNavEpf = 0.2M,
                    //         DailyUnitCreatedEpf = 0.4M,
                    //         DailyUnitPrice = y.UP_SELL,
                    //         DailyUnitCreatedEpfAdj = 0.0M
                    //     };
                    //     IUtmcDailyNavFundService.PostData(utmcDailyNavFund);
                    // });
                    Logger.WriteLog("BtnMigrateDailyNAV_Click - Start");

                    utmcFundInformations.ForEach(x =>
                    {
                        List<CurrentNAVDTO> NAVHistory = ServicesManager.GetNAVHistoryByFund("'" + x.IpdFundCode + "'", 0);
                        NAVHistory.ForEach(y =>
                        {
                            DateTime lastDayOfMonth = new DateTime(y.TRANS_DT.Year, y.TRANS_DT.Month, DateTime.DaysInMonth(y.TRANS_DT.Year, y.TRANS_DT.Month));
                            UtmcDailyNavFund utmcDailyNavFund = new UtmcDailyNavFund()
                            {
                                EpfIpdCode = "IPD000033",
                                IpdFundCode = y.FUND_ID,
                                DailyNavDate = y.TRANS_DT,
                                DailyNav = y.UNIT_IN_ISSUE_PRICE,
                                ReportDate = lastDayOfMonth,
                                DailyUnitCreated = y.UNIT_IN_ISSUE,
                                DailyNavEpf = 0.2M,
                                DailyUnitCreatedEpf = 0.4M,
                                DailyUnitPrice = y.UP_SELL,
                                DailyUnitCreatedEpfAdj = 0.0M
                            };
                            IUtmcDailyNavFundService.PostData(utmcDailyNavFund);
                        });
                    });

                    Logger.WriteLog("BtnMigrateDailyNAV_Click - End");
                }
                else
                {
                    Logger.WriteLog("BtnMigrateDailyNAV_Click responseUTMCFundInfo: " + responseUTMCFundInfo.Message + " - Exception");
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("BtnMigrateDailyNAV_Click: " + ex.Message + " - Exception");
            }
        }



        public static List<UtmcFundInformation> utmcFundInformations { get; set; }

        public static void GetFunds()
        {
            Response responseUFIs = IUtmcFundInformationService.GetDataByFilter(" 1=1 ", 0, 0, false);
            if (responseUFIs.IsSuccess)
            {
                utmcFundInformations = (List<UtmcFundInformation>)responseUFIs.Data;
            }
        }

        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUserOrderService> lazyUserOrderService = new Lazy<IUserOrderService>(() => new UserOrderService());
        public static IUserOrderService IUserOrderService { get { return lazyUserOrderService.Value; } }

        private static readonly Lazy<IAccountOpeningService> lazyAccountOpeningService = new Lazy<IAccountOpeningService>(() => new AccountOpeningService());
        public static IAccountOpeningService IAccountOpeningService { get { return lazyAccountOpeningService.Value; } }

        public void CheckOrders()
        {
            try
            {
                Int32 OrderRemindScheduleDaysCount = Convert.ToInt32(OrderRemindScheduleDays);
                Int32 OrderVoidScheduleDaysCount = Convert.ToInt32(OrderVoidScheduleDays);

                List<Response> responses = new List<Response>();
                Response responseUOList = IUserOrderService.GetDataByFilter(" order_type=1 and order_status='1' and status='1' and DATEDIFF(NOW(), created_date) > " + (OrderRemindScheduleDaysCount - 1) + " group by order_no ", 0, 0, true);
                responses.Add(responseUOList);
                List<UserOrder> userOrdersPending = new List<UserOrder>();
                if (responseUOList.IsSuccess)
                {
                    userOrdersPending = (List<UserOrder>)responseUOList.Data;
                    userOrdersPending.ForEach(x =>
                    {
                        Response response = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), x.OrderNo, true, 0, 0, false);
                        responses.Add(response);
                        if (response.IsSuccess)
                        {
                            List<UserOrder> multiOrders = (List<UserOrder>)response.Data;

                            if (multiOrders.Count > 0)
                            {
                                string fundName = "";
                                string fundName2 = "";
                                List<UtmcFundInformation> funds = new List<UtmcFundInformation>();
                                List<UtmcFundInformation> funds2 = new List<UtmcFundInformation>();

                                foreach (UserOrder uo in multiOrders)
                                {
                                    UtmcFundInformation utmcFundInformation1 = new UtmcFundInformation();
                                    Response responseUFI1 = IUtmcFundInformationService.GetDataByFilter(" id=" + uo.FundId + " ", 0, 0, true);
                                    responses.Add(responseUFI1);
                                    if (responseUFI1.IsSuccess)
                                    {
                                        List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)responseUFI1.Data;
                                        if (utmcFundInformations.Count > 0)
                                        {
                                            utmcFundInformation1 = utmcFundInformations.FirstOrDefault();
                                        }
                                    }
                                    else
                                    {
                                        Logger.WriteLog("CheckOrders responseUFI1: " + responseUFI1.Message + " - Exception");
                                    }
                                    UtmcFundInformation utmcFundInformation2 = new UtmcFundInformation();
                                    if (uo.ToFundId != 0)
                                    {
                                        Response responseUFI2 = IUtmcFundInformationService.GetDataByFilter(" id=" + uo.ToFundId + " ", 0, 0, true);
                                        responses.Add(responseUFI2);
                                        if (responseUFI2.IsSuccess)
                                        {
                                            List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)responseUFI2.Data;
                                            if (utmcFundInformations.Count > 0)
                                            {
                                                utmcFundInformation2 = utmcFundInformations.FirstOrDefault();
                                            }
                                        }
                                        else
                                        {
                                            Logger.WriteLog("CheckOrders responseUFI2: " + responseUFI2.Message + " - Exception");
                                        }
                                    }

                                    fundName = utmcFundInformation1.FundName.Capitalize();
                                    if (uo.ToFundId != 0)
                                        fundName2 = utmcFundInformation2.FundName.Capitalize();
                                    funds.Add(utmcFundInformation1);
                                    funds2.Add(utmcFundInformation2);
                                }
                                Logger.WriteLog("OrderNo: " + multiOrders.FirstOrDefault().OrderNo);

                                Response responseUAList = IUserAccountService.GetDataByFilter(" id=" + multiOrders.FirstOrDefault().UserAccountId + " and user_id=" + multiOrders.FirstOrDefault().UserId + " and status=1 ", 0, 0, true);
                                responses.Add(responseUAList);
                                if (responseUAList.IsSuccess)
                                {
                                    List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                                    UserAccount primaryAccount = new UserAccount();
                                    if (userAccounts.Count > 0)
                                    {
                                        primaryAccount = userAccounts.FirstOrDefault();
                                        Email email = new Email();
                                        Response responseUser = IUserService.GetSingle(multiOrders.FirstOrDefault().UserId);
                                        responses.Add(responseUser);
                                        if (responseUser.IsSuccess)
                                        {
                                            User user = (User)responseUser.Data;
                                            email.user = user;
                                            DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                                            DateTime cd = new DateTime(multiOrders.FirstOrDefault().CreatedDate.Year, multiOrders.FirstOrDefault().CreatedDate.Month, multiOrders.FirstOrDefault().CreatedDate.Day, 0, 0, 0);

                                            if ((now - cd).Days >= OrderVoidScheduleDaysCount)
                                            {
                                                multiOrders.ForEach(mo =>
                                                {
                                                    mo.OrderStatus = 19;
                                                });
                                                Int32 updatedRecordsCount = IUserOrderService.UpdateBulkData(multiOrders);
                                                if (updatedRecordsCount != multiOrders.Count)
                                                {
                                                    Logger.WriteLog("CheckOrders Update failed: " + multiOrders.FirstOrDefault().OrderNo + " - Error");
                                                    responses.Add(new Response { IsSuccess = false });
                                                }
                                                bool isSent = EmailService.SendOrderEmail(multiOrders, email, funds, funds2, primaryAccount.AccountNo, null, true, Application.StartupPath + "\\EmailTemplate\\OrderNotification.html");
                                                if (!isSent)
                                                {
                                                    Logger.WriteLog("CheckOrders Email void notify failed: " + multiOrders.FirstOrDefault().OrderNo + " - Error");
                                                    responses.Add(new Response { IsSuccess = false });
                                                }
                                            }
                                            else
                                            {
                                                Decimal days = (now - cd).Days / OrderRemindScheduleDaysCount;
                                                if (Math.Floor(days) != multiOrders.FirstOrDefault().IsCreditNote)
                                                {
                                                    multiOrders.ForEach(mo =>
                                                    {
                                                        mo.IsCreditNote++;
                                                    });
                                                    Int32 updatedRecordsCount = IUserOrderService.UpdateBulkData(multiOrders);
                                                    if (updatedRecordsCount != multiOrders.Count)
                                                    {
                                                        Logger.WriteLog("CheckOrders Update failed: " + multiOrders.FirstOrDefault().OrderNo + " - Error");
                                                        responses.Add(new Response { IsSuccess = false });
                                                    }
                                                    bool isSent = EmailService.SendOrderEmail(multiOrders, email, funds, funds2, primaryAccount.AccountNo, null, true, Application.StartupPath + "\\EmailTemplate\\OrderNotification.html");
                                                    if (!isSent)
                                                    {
                                                        Logger.WriteLog("CheckOrders Email payment remainder failed: " + multiOrders.FirstOrDefault().OrderNo + " - Error");
                                                        responses.Add(new Response { IsSuccess = false });
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Logger.WriteLog("CheckOrders responseUser: " + responseUser.Message + " - Exception");
                                        }



                                    }
                                }
                                else
                                {
                                    Logger.WriteLog("CheckOrders responseUAList: " + responseUAList.Message + " - Exception");
                                }
                            }

                        }
                        else
                        {
                            Logger.WriteLog("CheckOrders response: " + response.Message + " - Exception");
                        }
                    });

                }
                else
                {
                    Logger.WriteLog("CheckOrders responseUOList: " + responseUOList.Message + " - Exception");
                }

                bool isExecute = false;
                if (responses.Count > 0)
                {
                    if (responses.Where(x => x.IsSuccess == false).ToList().Count == 0)
                    {
                        isExecute = true;
                    }
                }
                else
                {
                    isExecute = true;
                }
                if (isExecute)
                {
                    WS_Process wS_Process = new WS_Process
                    {
                        is_processed = 1,
                        processed_date = DateTime.Now,
                        process_type = 6,
                        status = 1,
                        message = "Success"
                    };
                    Response responsePost = GenericService.PostData<WS_Process>(wS_Process);
                    if (!responsePost.IsSuccess)
                    {
                        Logger.WriteLog("DailyProcess WS_Process PostData: " + responsePost.Message + " - Exception");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("CheckOrders: " + ex.Message + " - Exception");
            }
        }

        public void CheckAOs()
        {
            try
            {
                Int32 Frequency = Convert.ToInt32(AccountOpeningRemindScheduleFrequency);
                String FrequencyBy = AccountOpeningRemindScheduleFrequencyBy;
                List<Response> responses = new List<Response>();
                string freqFilter = "";
                if (FrequencyBy == "Days" || FrequencyBy == "")
                {
                    freqFilter = "and DATEDIFF(NOW(), last_updated_date) > " + (Frequency - 1);
                }
                Response responseAOList = IAccountOpeningService.GetDataByFilter(" process_status=0 and status='1' and email != '' and is_email_verified=1 " + freqFilter + " ", 0, 0, true);
                responses.Add(responseAOList);
                List<AccountOpening> accountOpenings = new List<AccountOpening>();
                if (responseAOList.IsSuccess)
                {
                    accountOpenings = (List<AccountOpening>)responseAOList.Data;
                    accountOpenings.ForEach(x =>
                    {
                        Logger.WriteLog("ID No: " + x.IdNo);
                        DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                        DateTime cd = new DateTime(x.LastUpdatedDate.Value.Year, x.LastUpdatedDate.Value.Month, x.LastUpdatedDate.Value.Day, 0, 0, 0);

                        if (x.RemaindTimes < 3)
                        {
                            if ((FrequencyBy == "Days" || FrequencyBy == "") && (now - cd).Days >= Frequency)
                            {
                                x.RemaindTimes++;
                                Int32 relation = x.AccountType;
                                String url = ConfigurationManager.AppSettings["siteUrl"].ToString();
                                Email email = new Email();
                                email.user = new User();
                                email.user.Username = x.Name;
                                email.user.EmailId = x.Email;
                                String date = x.StartedDate.ToString("dd/MM/yyyy");
                                Logger.WriteLog("Data Ready to Send Email to " + x.Email);
                                bool isSent = EmailService.SendReminderAccountOpening(email, date, url, Application.StartupPath + "\\EmailTemplate\\AccountOpening.html", relation);
                                if (!isSent)
                                {
                                    Logger.WriteLog("CheckAOs Email notify failed: " + x.IdNo + " - Error");
                                    responses.Add(new Response { IsSuccess = false });
                                }
                                else
                                {
                                    Response updatedRes = IAccountOpeningService.UpdateData(x);
                                    if (!updatedRes.IsSuccess)
                                    {
                                        Logger.WriteLog("CheckAOs Update failed: " + x.IdNo + " " + updatedRes.Message + " - Error");
                                        responses.Add(updatedRes);
                                    }
                                }
                            }
                        }
                    });
                }
                else
                {
                    Logger.WriteLog("CheckAOs responseAOList: " + responseAOList.Message + " - Exception");
                }

                bool isExecute = false;
                if (responses.Count > 0)
                {
                    if (responses.Where(x => x.IsSuccess == false).ToList().Count == 0)
                    {
                        isExecute = true;
                    }
                }
                else
                {
                    isExecute = true;
                }
                if (isExecute)
                {
                    WS_Process wS_Process = new WS_Process
                    {
                        is_processed = 1,
                        processed_date = DateTime.Now,
                        process_type = 7,
                        status = 1,
                        message = "Success"
                    };
                    Response responsePost = GenericService.PostData<WS_Process>(wS_Process);
                    if (!responsePost.IsSuccess)
                    {
                        Logger.WriteLog("DailyProcess WS_Process PostData: " + responsePost.Message + " - Exception");
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog("CheckAOs: " + ex.Message + " - Exception");
            }
        }

        private void btnGenerateLast12MonthsStatements_Click(object sender, EventArgs e)
        {
            try
            {
                GetFunds();
                DateTime currentTime = DateTime.Now;
                DateTime lastTime = currentTime.AddMonths(-12);

                while (lastTime <= currentTime)
                {
                    DateTime fromDate = new DateTime(lastTime.Year, lastTime.Month, 1);
                    fromDate = fromDate.AddMonths(-1);
                    DateTime toDate = new DateTime(fromDate.Year, fromDate.Month, DateTime.DaysInMonth(fromDate.Year, fromDate.Month));
                    StatementService.GenerateMonthlyReport(fromDate, toDate, utmcFundInformations);
                    lastTime = lastTime.AddMonths(1);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("btnGenerateLast12MonthsStatements_Click: " + ex.Message + " - Exception");
            }
        }
    }
}
