﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using DiOTP.WindowServiceApp.ServiceCalls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiOTP.WindowSerivceApp
{
    public class StatementService
    {
        private static readonly Lazy<IUserStatementService> lazyIUserStatementService = new Lazy<IUserStatementService>(() => new UserStatementService());
        public static IUserStatementService IUserStatementService { get { return lazyIUserStatementService.Value; } }

        private static readonly Lazy<IUtmcDailyNavFundService> lazyUtmcDailyNavFundObj = new Lazy<IUtmcDailyNavFundService>(() => new UtmcDailyNavFundService());
        public static IUtmcDailyNavFundService IUtmcDailyNavFundService { get { return lazyUtmcDailyNavFundObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        //public static List<UtmcFundInformation> utmcFundInformations { get; set; }

        //public static void GetFunds()
        //{
        //    Response responseUFIs = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
        //    if (responseUFIs.IsSuccess)
        //    {
        //        utmcFundInformations = (List<UtmcFundInformation>)responseUFIs.Data;
        //    }
        //}
        public static void GenerateMonthlyReport(DateTime firstdForMonth, DateTime lastdForMonth, List<UtmcFundInformation> utmcFundInformations)
        {
            //GetFunds();
            //Response responseUAs = GenericService.PullData<UserAccount>(" account_no in ('36708', '23351', '36707', '23354') ", 0, 0, false, null);
            //Response responseUAs = GenericService.PullData<UserAccount>(" account_no in ('36981') ", 0, 0, false, null);
            List<UserAccount> allUA = UserAccountService.GetAll();
            //List<UserAccount> allUA = (List<UserAccount>)responseUAs.Data;

            foreach (UserAccount ua in allUA)
                GenerateReport(ua.AccountNo, firstdForMonth, lastdForMonth, 1, utmcFundInformations);
        }

        public static void GenerateYearlyReport(DateTime firstdForMonth, DateTime lastdForMonth, List<UtmcFundInformation> utmcFundInformations)
        {
            //GetFunds();
            List<UserAccount> allUA = UserAccountService.GetAll();
            foreach (UserAccount ua in allUA)
                GenerateReport(ua.AccountNo, firstdForMonth, lastdForMonth, 3, utmcFundInformations);
        }


        public static void GenerateReport(string accountNo, DateTime f, DateTime l, int statementCategory, List<UtmcFundInformation> utmcFundInformations)
        {
            Logger.WriteLog("GenerateReport: " + accountNo);

            UserStatements us = new UserStatements();
            UserAccount ua = new UserAccount();
            ua = UserAccountService.GetByid(accountNo);

            string time1 = DateTime.Now.ToString("dd-MM-yyyy");
            DateTime currenttime1 = DateTime.ParseExact(time1, "dd-MM-yyyy", CultureInfo.InvariantCulture);

            string time2 = DateTime.Now.ToString("dd-MM-yyyy");
            DateTime currenttime2 = DateTime.ParseExact(time2, "dd-MM-yyyy", CultureInfo.InvariantCulture);

            int userAID = ua.Id;
            int userID = ua.UserId;

            List<UserStatement> us1 = new List<UserStatement>();
            //us1 = UserStatementService.GetAllUS();
            if (statementCategory == 1)
            {
                Response response = IUserStatementService.GetDataByFilter(" user_account_id=" + ua.Id + " and statement_date = '" + f.ToString("yyyy-MM-dd") + "' and user_statement_category_id=" + statementCategory + " ", 0, 0, false);
                if (response.IsSuccess)
                    us1 = (List<UserStatement>)response.Data;
                else
                    Logger.WriteLog("if (statementCategory == 1): " + response.Message + " - Exception");
            }
            else if (statementCategory == 3)
            {
                Response response = IUserStatementService.GetDataByFilter(" user_account_id=" + ua.Id + " and YEAR(created_date)=" + currenttime1.Year + " and user_statement_category_id=" + statementCategory + " ", 0, 0, false);
                if (response.IsSuccess)
                    us1 = (List<UserStatement>)response.Data;
                else
                    Logger.WriteLog("if (statementCategory == 3): " + response.Message + " - Exception");
            }
            string pdfDate = DateTime.Now.ToString("ddMMyyyyHHmmssfff");

            if (us1.Count == 0 && (statementCategory == 1 || statementCategory == 3))
            {
                string StatementsPath = ConfigurationManager.AppSettings["StatementsPath"].ToString();

                Logger.WriteLog("StatementsPath: " + StatementsPath);

                string path = "";
                //Monthly
                if (statementCategory == 1)
                    path = (StatementsPath) + "\\" + l.ToString("MM-yyyy") + "\\" + accountNo + "_" + pdfDate + ".pdf";
                //path = Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy") + "/" + accountNo + "_" + pdfDate + ".pdf";
                //Yearly
                else if (statementCategory == 3)
                    path = (StatementsPath) + "\\" + l.ToString("MM-yyyy") + "_" + "yearlyReport" + "\\" + accountNo + "_" + pdfDate + ".pdf";
                //path = Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy") + "_" + "yearlyReport" + "/" + accountNo + "_" + pdfDate + ".pdf";

                if (!Directory.Exists((StatementsPath) + "\\" + l.ToString("MM-yyyy")))
                    Directory.CreateDirectory((StatementsPath) + "\\" + l.ToString("MM-yyyy"));

                if (!Directory.Exists((StatementsPath) + "\\" + l.ToString("MM-yyyy") + "_" + "yearlyReport"))
                    Directory.CreateDirectory((StatementsPath) + "\\" + l.ToString("MM-yyyy") + "_" + "yearlyReport");

                //if (!Directory.Exists(Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy")))
                //{
                //    Directory.CreateDirectory(Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy"));
                //}
                //if (!Directory.Exists(Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy") + "_" + "yearlyReport"))
                //{
                //    Directory.CreateDirectory(Path.GetFullPath("../../../DiOTP.WebApp/") + "/" + DateTime.Now.ToString("MM-yyyy") + "_" + "yearlyReport");
                //}


                Response maHG = ServicesManager.GetMaHolderRegByAccountNo(accountNo);
                if (maHG.IsSuccess)
                {
                    if (maHG.Data != null)
                    {
                        MaHolderReg maHolder = (MaHolderReg)maHG.Data;
                        DateTime RDT = DateTime.ParseExact(maHolder.RegDt, "M/d/yyyy", CultureInfo.InvariantCulture);
                        if (f >= RDT)
                        {
                            FileStream fs = new FileStream(path, FileMode.CreateNew);
                            Document doc = new Document(PageSize.A4, 36, 36, 36, 135);
                            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, fs);

                            Logger.WriteLog("maHG: " + maHG.Data);

                            doc.Open();
                            PageEvent e = new PageEvent();
                            e.AddLogo(pdfWriter, doc);
                            e.AddHeader(pdfWriter, doc, maHG, l);
                            pdfWriter.PageEvent = e;

                            //table style
                            Font fontText = FontFactory.GetFont("ARIAL", 9);
                            Font fontTitle = FontFactory.GetFont("ARIAL", 9, BaseColor.WHITE);
                            BaseColor headBackgroundColor = BaseColor.DARK_GRAY;

                            // table title - Summary Portfolio
                            PdfPTable tableSummaryPortfolio = new PdfPTable(5);
                            tableSummaryPortfolio.WidthPercentage = 100;
                            int[] intTblWidth = { 30, 10, 20, 15, 15 };
                            tableSummaryPortfolio.SetWidths(intTblWidth);

                            tableSummaryPortfolio.Rows.Add(new PdfPRow(new PdfPCell[] {
                                new PdfPCell(new Phrase("Trust Name", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                                new PdfPCell(new Phrase("Trust Code", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                                new PdfPCell(new Phrase("Units Held", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                                new PdfPCell(new Phrase("Redemption Price (RM)", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                                new PdfPCell(new Phrase("Current Market Value (RM)", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER}
                            }));

                            //table content -  Summary Portfolio 
                            decimal totalCurrentMarketValue = 0;
                            int i = 0;
                            List<SummaryPortfolio> summaryPortfolioList = new List<SummaryPortfolio>();
                            //summaryPortfolioList = GenerateStatementService.GetAll(l, accountNo);

                            List<HolderLedger> holderLedgers = ServicesManager.GetHolderLedgerByHolderNo(" select * from UTS.holder_ledger where TRANS_TYPE NOT IN ('CO','UC','RC','SC','MC') and holder_no = '" + accountNo + "' and trans_dt between TO_DATE('" + f.ToString("yyyy-MM-dd") + "', 'yyyy/mm/dd') and TO_DATE('" + l.ToString("yyyy-MM-dd") + "', 'yyyy/mm/dd') and fund_id in (" + String.Join(",", utmcFundInformations.Select(xx => "'" + xx.IpdFundCode + "'").ToArray()) + ") ");

                            Logger.WriteLog("holderLedgers.Count: " + holderLedgers.Count);

                            List<HolderLedger> holderLedgersFiltered = holderLedgers;
                            holderLedgers.ForEach(checkX =>
                            {
                                if (checkX.TransNO.Contains('X'))
                                {
                                    string transNoX = checkX.TransNO;
                                    string transNo = checkX.TransNO.TrimEnd('X');
                                    holderLedgersFiltered = holderLedgersFiltered.Where(y => y.TransNO != transNo && y.TransNO != transNoX).ToList();
                                }
                            });
                            holderLedgersFiltered = holderLedgersFiltered.OrderBy(y => y.FundID).ThenBy(y => y.TransDt).ThenBy(y => Convert.ToInt32(y.TransNO)).ToList();

                            Logger.WriteLog("holderLedgersFiltered.Count: " + holderLedgersFiltered.Count);


                            string groupByQ = "select FUND_ID, MAX(TRANS_DT) as TRANS_DT from uts.holder_ledger where TRANS_TYPE NOT IN ('CO','UC','RC','SC','MC') and HOLDER_NO='" + accountNo + "' and FUND_ID in ('01', '02', '03', '04', '05', '06', '07', '08', '09') and TRANS_DT <= TO_DATE('" + l.ToString("yyyy-MM-dd") + "', 'yyyy/mm/dd') GROUP BY FUND_ID ";
                            List<HolderLedger> holderLedgersSummary = ServicesManager
                                .GetHolderLedgerByHolderNo(@" select * from UTS.holder_ledger where TRANS_TYPE NOT IN ('CO','UC','RC','SC','MC') and holder_no = '" + accountNo + "' and fund_id in (" + String.Join(",", utmcFundInformations.Select(xx => "'" + xx.IpdFundCode + "'").ToArray()) + ") and TRANS_DT IN (SELECT TRANS_DT from (" + groupByQ + ")) ");
                            Logger.WriteLog("holderLedgersSummary.Count: " + holderLedgersSummary.Count);

                            //ACTUAL HOLDINGS*************************************************************
                            //List<HolderInv> holderInvs = ServicesManager.GetHolderInvByHolderNo(accountNo, utmcFundInformations);

                            //Logger.WriteLog("holderInvs.Count: " + holderInvs.Count);

                            //holderInvs.ForEach(y =>
                            //{
                            //    UtmcFundInformation utmcFundInformation = utmcFundInformations.FirstOrDefault(z => z.IpdFundCode == y.FundId);
                            //    DateTime l7 = DateTime.Now.AddDays(-20);
                            //    Response responseDNP = IUtmcDailyNavFundService.GetDataByFilter(" IPD_Fund_Code='" + y.FundId + "' and Daily_NAV_Date > '" + l7.ToString("yyyy-MM-dd") + "' and Daily_NAV_Date <='" + DateTime.Now.ToString("yyyy-MM-dd") + "' ", 0, 0, true);
                            //    UtmcDailyNavFund utmcDailyNavFund = new UtmcDailyNavFund();
                            //    if (responseDNP.IsSuccess)
                            //    {
                            //        List<UtmcDailyNavFund> utmcDailyNavFunds = (List<UtmcDailyNavFund>)responseDNP.Data;
                            //        if (utmcDailyNavFunds.Count > 0)
                            //        {
                            //            utmcDailyNavFund = utmcDailyNavFunds.FirstOrDefault();
                            //        }
                            //    }
                            //    summaryPortfolioList.Add(new SummaryPortfolio
                            //    {
                            //        Fund_Name = utmcFundInformation.FundName,
                            //        Fund_Code = utmcFundInformation.FundCode,
                            //        Net_Cumulative_Closing_Balance_Units = y.CurrUnitHldg,
                            //        Market_Price_NAV = utmcDailyNavFund.DailyUnitPrice,
                            //        Current_Market_Value = y.CurrUnitHldg * utmcDailyNavFund.DailyUnitPrice,
                            //    });
                            //});
                            //ACTUAL HOLDINGS*************************************************************

                            //LEDGER BASED HOLDINGS*************************************************************
                            holderLedgersFiltered.Where(y => y.TransDt <= l).GroupBy(y => y.FundID).Select(grp => grp.OrderByDescending(y => y.TransDt).ThenByDescending(y => Convert.ToInt32(y.TransNO)).ToList()).ToList().ForEach(grpByFund =>
                            {
                                UtmcFundInformation utmcFundInformation = utmcFundInformations.FirstOrDefault(y => y.IpdFundCode == grpByFund.FirstOrDefault().FundID);
                                DateTime l7 = l.AddDays(-20);
                                Response responseDNP = IUtmcDailyNavFundService.GetDataByFilter(" IPD_Fund_Code='" + grpByFund.FirstOrDefault().FundID + "' and Daily_NAV_Date > '" + l7.ToString("yyyy-MM-dd") + "' and Daily_NAV_Date <='" + l.ToString("yyyy-MM-dd") + "' ", 0, 0, true);
                                UtmcDailyNavFund utmcDailyNavFund = new UtmcDailyNavFund();
                                if (responseDNP.IsSuccess)
                                {
                                    List<UtmcDailyNavFund> utmcDailyNavFunds = (List<UtmcDailyNavFund>)responseDNP.Data;
                                    if (utmcDailyNavFunds.Count > 0)
                                    {
                                        utmcDailyNavFund = utmcDailyNavFunds.FirstOrDefault();
                                    }
                                }
                                //if (summaryPortfolioList.FirstOrDefault(s => s.Fund_Name == utmcFundInformation.FundName) != null)
                                //{
                                //    summaryPortfolioList.ForEach(s =>
                                //    {
                                //        if (s.Fund_Name == utmcFundInformation.FundName)
                                //        {
                                //            s.Fund_Name = utmcFundInformation.FundName;
                                //            s.Fund_Code = utmcFundInformation.FundCode;
                                //            s.Net_Cumulative_Closing_Balance_Units = grpByFund.FirstOrDefault().CurUnitHLDG.Value;
                                //            s.Market_Price_NAV = utmcDailyNavFund.DailyUnitPrice;
                                //            s.Current_Market_Value = grpByFund.FirstOrDefault().CurUnitHLDG.Value * utmcDailyNavFund.DailyUnitPrice;
                                //        }
                                //    });
                                //}
                                //else
                                {
                                    summaryPortfolioList.Add(new SummaryPortfolio
                                    {
                                        Ipd_Fund_Code = utmcFundInformation.IpdFundCode,
                                        Fund_Name = utmcFundInformation.FundName,
                                        Fund_Code = utmcFundInformation.FundCode,
                                        Net_Cumulative_Closing_Balance_Units = grpByFund.FirstOrDefault().CurUnitHLDG.Value,
                                        Market_Price_NAV = utmcDailyNavFund.DailyUnitPrice,
                                        Current_Market_Value = grpByFund.FirstOrDefault().CurUnitHLDG.Value * utmcDailyNavFund.DailyUnitPrice,
                                    });
                                    holderLedgersSummary.Remove(holderLedgersSummary.FirstOrDefault(hls => hls.FundID == grpByFund.FirstOrDefault().FundID));
                                }
                            });
                            //LEDGER BASED HOLDINGS*************************************************************
                            holderLedgersFiltered = holderLedgersFiltered.Where(y => y.TransDt <= l).ToList();
                            //List<UtmcCompositionalTransaction> utmcCompositionalTransactions = ServicesManager.GetCompositionalTransactionByHolderNo(accountNo, f.ToString("yyyy-MM-dd"), l.ToString("yyyy-MM-dd"));
                            //utmcCompositionalTransactions = utmcCompositionalTransactions.Where(y => y.DateOfTransaction.Value <= l).OrderBy(y => y.DateOfTransaction.Value).ThenBy(y => y.IpdUniqueTransactionId).ToList();
                            //utmcCompositionalTransactions.Where(y => y.ReportDate.Value <= l).GroupBy(y => y.IpdFundCode).Select(grp => grp.ToList()).ToList().ForEach(y =>
                            //{
                            //    summaryPortfolioList.Add(new SummaryPortfolio
                            //    {
                            //        Fund_Name = "",
                            //        Fund_Code = y.FirstOrDefault().IpdFundCode,
                            //        Net_Cumulative_Closing_Balance_Units = y.Sum(z => z.Units.Value),
                            //        Market_Price_NAV = y.Sum(z => z.Units.Value * z.TransPR),
                            //        Current_Market_Value = y.Sum(z => z.Units.Value * z.TransPR),
                            //    });
                            //});

                            Logger.WriteLog("holderLedgersSummary.Count: " + holderLedgersSummary.Count);
                            var holderLedgersSummaryGBF = holderLedgersSummary.GroupBy(hls => hls.FundID).Select(grp => grp.LastOrDefault()).ToList();
                            holderLedgersSummaryGBF.ForEach(hls =>
                            {
                                if (summaryPortfolioList.FirstOrDefault(spl => spl.Ipd_Fund_Code == hls.FundID) == null)
                                {
                                    UtmcFundInformation utmcFundInformation = utmcFundInformations.FirstOrDefault(y => y.IpdFundCode == hls.FundID);
                                    DateTime l7 = l.AddDays(-20);
                                    Response responseDNP = IUtmcDailyNavFundService.GetDataByFilter(" IPD_Fund_Code='" + hls.FundID + "' and Daily_NAV_Date > '" + l7.ToString("yyyy-MM-dd") + "' and Daily_NAV_Date <='" + l.ToString("yyyy-MM-dd") + "' ", 0, 0, true);
                                    UtmcDailyNavFund utmcDailyNavFund = new UtmcDailyNavFund();
                                    if (responseDNP.IsSuccess)
                                    {
                                        List<UtmcDailyNavFund> utmcDailyNavFunds = (List<UtmcDailyNavFund>)responseDNP.Data;
                                        if (utmcDailyNavFunds.Count > 0)
                                        {
                                            utmcDailyNavFund = utmcDailyNavFunds.FirstOrDefault();
                                        }
                                    }

                                    summaryPortfolioList.Add(new SummaryPortfolio
                                    {
                                        Ipd_Fund_Code = utmcFundInformation.IpdFundCode,
                                        Fund_Name = utmcFundInformation.FundName,
                                        Fund_Code = utmcFundInformation.FundCode,
                                        Net_Cumulative_Closing_Balance_Units = hls.CurUnitHLDG.Value,
                                        Market_Price_NAV = utmcDailyNavFund.DailyUnitPrice,
                                        Current_Market_Value = hls.CurUnitHLDG.Value * utmcDailyNavFund.DailyUnitPrice,
                                    });
                                }
                            });
                            Logger.WriteLog("holderLedgersSummaryGBF.Count: " + holderLedgersSummaryGBF.Count);

                            List<string> fundCodeList = new List<string>();

                            //summaryPortfolioList = summaryPortfolioList.Where(spl => spl.Net_Cumulative_Closing_Balance_Units > 0).ToList();
                            summaryPortfolioList = summaryPortfolioList.OrderBy(spl => spl.Ipd_Fund_Code).ToList();

                            foreach (SummaryPortfolio summaryPortfolio in summaryPortfolioList.Where(spl => spl.Net_Cumulative_Closing_Balance_Units > 0).ToList())
                            {
                                BaseColor contentBackgroundColor = i % 2 == 0 ? new BaseColor(236, 236, 236) : BaseColor.WHITE;
                                PdfPCell[] cells = new PdfPCell[] {
                                new PdfPCell(new Phrase(summaryPortfolio.Fund_Name.Capitalize(), fontText)){ BackgroundColor = contentBackgroundColor },
                                new PdfPCell(new Phrase(summaryPortfolio.Fund_Code, fontText)){ BackgroundColor = contentBackgroundColor, HorizontalAlignment = Element.ALIGN_LEFT},
                                new PdfPCell(new Phrase(summaryPortfolio.Net_Cumulative_Closing_Balance_Units.ToString("#,##0.0000"), fontText)){ BackgroundColor = contentBackgroundColor, HorizontalAlignment = Element.ALIGN_RIGHT},
                                new PdfPCell(new Phrase(summaryPortfolio.Market_Price_NAV.ToString("#,##0.000000000"), fontText)){ BackgroundColor = contentBackgroundColor, HorizontalAlignment = Element.ALIGN_RIGHT},
                                new PdfPCell(new Phrase(summaryPortfolio.Current_Market_Value.ToString("#,##0.00"), fontText)){ BackgroundColor = contentBackgroundColor, HorizontalAlignment = Element.ALIGN_RIGHT},
                            };
                                fundCodeList.Add(summaryPortfolio.Fund_Code);
                                tableSummaryPortfolio.Rows.Add(new PdfPRow(cells));
                                i++;
                                totalCurrentMarketValue += Convert.ToDecimal(summaryPortfolio.Current_Market_Value.ToString("#,##0.00"));
                            }

                            Font fontTextBold = FontFactory.GetFont("ARIAL", 9);
                            BaseColor contentBackgroundColor2 = i % 2 == 0 ? new BaseColor(236, 236, 236) : BaseColor.WHITE;
                            tableSummaryPortfolio.Rows.Add(new PdfPRow(new PdfPCell[] {
                                    new PdfPCell(new Phrase("Total Portfolio Value (RM)", fontTextBold)) { BackgroundColor = contentBackgroundColor2, Colspan=4},
                                    new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColor2},
                                    new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColor2},
                                    new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColor2},
                                    new PdfPCell(new Phrase(totalCurrentMarketValue.ToString("#,##0.00"), fontTextBold)) { BackgroundColor = contentBackgroundColor2, HorizontalAlignment = Element.ALIGN_RIGHT}

                            }));

                            PdfPTable noTransactionsTable = new PdfPTable(1);
                            if (holderLedgersFiltered.Count == 0)
                            {
                                Font fontAddress = FontFactory.GetFont("ARIAL", 7);
                                Paragraph noTransactionsParagraph = new Paragraph("", fontAddress);
                                noTransactionsParagraph.Alignment = Element.ALIGN_CENTER;
                                PdfPCell noTransactionsParagraphCell = new PdfPCell(noTransactionsParagraph);
                                noTransactionsTable.AddCell(noTransactionsParagraphCell);
                            }

                            // table title - Transaction
                            PdfPTable tableTransaction = new PdfPTable(8);
                            tableTransaction.WidthPercentage = 100;
                            int[] intTblWidth2 = { 12, 12, 10, 13, 14, 14, 12, 11 };
                            tableTransaction.SetWidths(intTblWidth2);
                            tableTransaction.Rows.Add(new PdfPRow(new PdfPCell[] {
                            new PdfPCell(new Phrase("Trans Date", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER},
                            new PdfPCell(new Phrase("Processed Date", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                            new PdfPCell(new Phrase("Trust Code", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                            new PdfPCell(new Phrase("Trans No", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                            new PdfPCell(new Phrase("Number of units", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                            new PdfPCell(new Phrase("Unit Cost (RM)", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                            new PdfPCell(new Phrase("Trans Amount (RM)", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER },
                            new PdfPCell(new Phrase("Servicing Agent", fontTitle)) { BackgroundColor = headBackgroundColor, HorizontalAlignment = Element.ALIGN_CENTER }
                        }));



                            //table content - Transaction
                            int x = 0;
                            int count = 1;
                            decimal totalUnits = 0;
                            int countTransaction = 0;

                            List<TransactionByDate> balanceUnitsList = new List<TransactionByDate>();
                            //balanceUnitsList = GenerateStatementService.GetLastMonthSum(l, accountNo);

                            List<TransactionByDate> transactionByDateList = new List<TransactionByDate>();
                            //transactionByDateList = GenerateStatementService.GetAllTransaction(f, l, accountNo);

                            holderLedgersFiltered
                                //.Where(y => y.ReportDate.Value.ToString("yyyy-MM-dd") == l.ToString("yyyy-MM-dd"))
                                .GroupBy(y => y.FundID)
                                .Select(grp => grp.ToList())
                                .ToList().ForEach(y =>
                                {
                                    balanceUnitsList.Add(new TransactionByDate
                                    {
                                        Sum_Units = y.Sum(z => z.TransUnits.Value),
                                        Report_Date = y.FirstOrDefault().TransDt.Value,
                                        Fund_Code = y.FirstOrDefault().FundID
                                    });
                                });
                            holderLedgersFiltered
                                .ToList().ForEach(y =>
                                {
                                    string[] transTypes = new string[] { "TR", "RD", "DD" };
                                    transactionByDateList.Add(new TransactionByDate
                                    {
                                        Date_Of_Transaction = y.TransDt.Value,
                                        Date_Of_Settlement = y.EnterDt.Value,
                                        Fund_Code = y.FundID,
                                        Fund_Name = "",
                                        Transaction_Code = y.AccType == "SW" ? "SW" + (y.TransType == "SA" ? "I" : (y.TransType == "RD" ? "O" : "")) : y.TransType,
                                        Transaction_Name = "",
                                        IPD_Unique_Transaction_ID = y.TransNO,
                                        Units = y.TransUnits.Value,
                                        Unit_Cost_RM = transTypes.Contains(y.TransType) ? y.TransPr.Value : y.TransAmt.Value / y.TransUnits.Value,
                                        Gross_Amount_RM = y.TransAmt.Value,
                                        Report_Date = y.TransDt.Value,
                                        Agent_Code = y.AgentCode
                                    });
                                });

                            string groupByQ1 = "select FUND_ID, MAX(TRANS_DT) as TRANS_DT from uts.holder_ledger where TRANS_TYPE NOT IN ('CO','UC','RC','SC','MC') and HOLDER_NO='" + accountNo + "' and FUND_ID in ('01', '02', '03', '04', '05', '06', '07', '08', '09') and TRANS_DT < TO_DATE('" + f.ToString("yyyy-MM-dd") + "', 'yyyy/mm/dd') GROUP BY FUND_ID ";
                            List<HolderLedger> holderLedgersBalance = ServicesManager
                                .GetHolderLedgerByHolderNo(@" select * from UTS.holder_ledger where TRANS_TYPE NOT IN ('CO','UC','RC','SC','MC') and holder_no = '" + accountNo + "' and fund_id in (" + String.Join(",", utmcFundInformations.Select(xx => "'" + xx.IpdFundCode + "'").ToArray()) + ") and TRANS_DT IN (SELECT TRANS_DT from (" + groupByQ1 + ")) ");
                            Logger.WriteLog("holderLedgersBalance.Count: " + holderLedgersBalance.Count);
                            var holderLedgersBalanceGBF = holderLedgersBalance.GroupBy(hls => hls.FundID).Select(grp => grp.LastOrDefault()).ToList();
                            Logger.WriteLog("holderLedgersSummaryGBF.Count: " + holderLedgersBalanceGBF.Count);


                            foreach (TransactionByDate balanceUnits in balanceUnitsList)
                            {
                                if (balanceUnits != null)
                                {
                                    HolderLedger bf = holderLedgersBalanceGBF.FirstOrDefault(hlb => hlb.FundID == balanceUnits.Fund_Code);
                                    BaseColor contentBackgroundColorLightGrey = x % 2 == 0 ? new BaseColor(236, 236, 236) : BaseColor.WHITE;
                                    if (bf != null)
                                    {
                                        DateTime prevMonth = f.AddMonths(-1);
                                        DateTime prevMonthLastDate = new DateTime(prevMonth.Year, prevMonth.Month, DateTime.DaysInMonth(prevMonth.Year, prevMonth.Month));
                                        string lastDay = (bf == null ? "" : DateTime.DaysInMonth(prevMonth.Year, prevMonth.Month).ToString());
                                        //BaseColor contentBackgroundColorLightGrey = x % 2 == 0 ? new BaseColor(236, 236, 236) : BaseColor.WHITE;
                                        PdfPCell[] cells = new PdfPCell[] {
                                        new PdfPCell(new Phrase(bf == null ? "-" : prevMonthLastDate.ToString("dd/MM/yyyy"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey, HorizontalAlignment = Element.ALIGN_LEFT },
                                        new PdfPCell(new Phrase("B/f Balance", fontText)){ BackgroundColor = contentBackgroundColorLightGrey, HorizontalAlignment = Element.ALIGN_CENTER },
                                        new PdfPCell(new Phrase(utmcFundInformations.FirstOrDefault(y => y.IpdFundCode == balanceUnits.Fund_Code).FundCode, fontText)){ BackgroundColor = contentBackgroundColorLightGrey, HorizontalAlignment = Element.ALIGN_CENTER },
                                        //new PdfPCell(new Phrase(balanceUnits.Fund_Code, fontText)){ BackgroundColor = contentBackgroundColorLightGrey, HorizontalAlignment = Element.ALIGN_CENTER },
                                        new PdfPCell(new Phrase(" ", fontText)){ BackgroundColor = contentBackgroundColorLightGrey},
                                        new PdfPCell(new Phrase(bf == null ? "0" : bf.CurUnitHLDG.Value.ToString("N4"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey, HorizontalAlignment = Element.ALIGN_RIGHT },
                                        new PdfPCell(new Phrase(" ", fontText)){ BackgroundColor = contentBackgroundColorLightGrey },
                                        new PdfPCell(new Phrase(" ", fontText)){ BackgroundColor = contentBackgroundColorLightGrey },
                                        new PdfPCell(new Phrase(" ", fontText)){ BackgroundColor = contentBackgroundColorLightGrey }
                                    };
                                        //totalUnits = balanceUnits.Sum_Units;
                                        tableTransaction.Rows.Add(new PdfPRow(cells));
                                    }

                                    List<TransactionByDate> sorted = new List<TransactionByDate>();
                                    sorted = transactionByDateList.Where(X => X.Fund_Code == balanceUnits.Fund_Code).ToList();

                                    foreach (TransactionByDate transactionByDate in sorted)
                                    {
                                        if (transactionByDate != null)
                                        {
                                            BaseColor contentBackgroundColorLightGrey2 = x % 2 == 0 ? new BaseColor(236, 236, 236) : BaseColor.WHITE;
                                            countTransaction++;
                                            PdfPCell[] cells2 = new PdfPCell[] {
                                            new PdfPCell(new Phrase(transactionByDate.Date_Of_Transaction.ToString("dd/MM/yyyy"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_LEFT },
                                            new PdfPCell(new Phrase(transactionByDate.Date_Of_Settlement.ToString("dd/MM/yyyy"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_LEFT },
                                            new PdfPCell(new Phrase(utmcFundInformations.FirstOrDefault(y => y.IpdFundCode == balanceUnits.Fund_Code).FundCode, fontText)){ BackgroundColor = contentBackgroundColorLightGrey, HorizontalAlignment = Element.ALIGN_CENTER },
                                            //new PdfPCell(new Phrase(transactionByDate.Fund_Code, fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_LEFT },
                                            new PdfPCell(new Phrase((transactionByDate.Transaction_Code + transactionByDate.IPD_Unique_Transaction_ID), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_LEFT },
                                            new PdfPCell(new Phrase(transactionByDate.Units.ToString("#,##0.0000"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_RIGHT },
                                            new PdfPCell(new Phrase(transactionByDate.Unit_Cost_RM.ToString("#,##0.000000000"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_RIGHT  },
                                            new PdfPCell(new Phrase(transactionByDate.Gross_Amount_RM.ToString("#,##0.00"), fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_RIGHT  },
                                            new PdfPCell(new Phrase(transactionByDate.Agent_Code, fontText)){ BackgroundColor = contentBackgroundColorLightGrey2, HorizontalAlignment = Element.ALIGN_LEFT }
                                        };
                                            tableTransaction.Rows.Add(new PdfPRow(cells2));
                                            totalUnits += transactionByDate.Units;
                                            x++;
                                        }
                                    }
                                    //tableTransaction.Rows.Add(new PdfPRow(cells));
                                    //x++;
                                    if (sorted.Count != 0)
                                    {
                                        BaseColor contentBackgroundColorGrey = new BaseColor(220, 220, 220);
                                        BaseColor contentBackgroundColor = BaseColor.WHITE;
                                        SummaryPortfolio sp = summaryPortfolioList.FirstOrDefault(hls => hls.Fund_Code == utmcFundInformations.FirstOrDefault(y => y.IpdFundCode == balanceUnits.Fund_Code).FundCode);
                                        //HolderLedger fb = holderLedgersSummaryGBF.FirstOrDefault(hls => hls.FundID == balanceUnits.Fund_Code);
                                        tableTransaction.Rows.Add(new PdfPRow(new PdfPCell[] {
                                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, BorderWidthTop=0, BorderWidthRight=0},
                                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, BorderWidthTop=0, BorderWidthRight=0, BorderWidthLeft=0},
                                        new PdfPCell(new Phrase("No of transaction: " + countTransaction, fontTextBold)) { BackgroundColor = contentBackgroundColorGrey, Colspan=2, BorderWidthRight=0, BorderWidthTop=0, BorderWidthLeft=0},
                                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, Border=0, BorderWidthTop=0},
                                        new PdfPCell(new Phrase(sp.Net_Cumulative_Closing_Balance_Units.ToString("N4"), fontTextBold)) { BackgroundColor = contentBackgroundColorGrey, BorderWidthTop=0, HorizontalAlignment = Element.ALIGN_RIGHT},
                                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, BorderWidthLeft=0,  BorderWidthTop=0, BorderWidthRight=0},
                                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, BorderWidthLeft=0,  BorderWidthTop=0, BorderWidthRight=0},
                                        new PdfPCell(new Phrase("")) { BackgroundColor = contentBackgroundColorGrey, BorderWidthLeft=0,  BorderWidthTop=0}
                                    }));

                                        BaseColor contentBackgroundColor3 = BaseColor.WHITE;
                                        tableTransaction.Rows.Add(new PdfPRow(new PdfPCell[] {
                                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthRight=0},
                                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0, BorderWidthRight=0},
                                        new PdfPCell(new Phrase(" ", fontText)) { BackgroundColor = contentBackgroundColor3, BorderWidthTop=0, BorderWidthLeft=0}
                                     }));
                                    }

                                    count++;
                                    countTransaction = 0;
                                    totalUnits = 0;
                                }
                            }


                            tableTransaction.DeleteLastRow();

                            tableSummaryPortfolio.SpacingBefore = 5f;
                            tableSummaryPortfolio.SpacingAfter = 12.5f;
                            tableTransaction.SpacingBefore = 5f;
                            tableTransaction.SpacingAfter = 12.5f;

                            doc.Add(tableSummaryPortfolio);
                            if (holderLedgersFiltered.Count == 0)
                            {
                                Font fontTextBold1 = FontFactory.GetFont("ARIAL", 9, Font.BOLD);
                                Paragraph paragraph = new Paragraph("No transaction completed during the period from " + f.ToString("dd MMMM, yyyy") + " to " + l.ToString("dd MMMM, yyyy") + ":", fontTextBold1);
                                doc.Add(paragraph);
                            }
                            else
                            {
                                e.AddHeader2(pdfWriter, doc, f, l);
                                doc.Add(tableTransaction);
                            }
                            e.AddInformation(doc);
                            e.OnEndPage(pdfWriter, doc);
                            pdfWriter.CloseStream = true;
                            doc.Close();
                            e.AddFooter(path, doc);

                            fs.Close();

                            us.user_id = userID;
                            us.user_account_id = Convert.ToInt32(userAID);
                            us.user_statment_category_id = statementCategory;
                            us.name = accountNo + "_" + pdfDate + ".pdf";

                            if (statementCategory == 1)
                                us.url = "/Content/Statements/" + l.ToString("MM-yyyy") + "/" + accountNo + "_" + pdfDate + ".pdf";
                            else if (statementCategory == 3)
                                us.url = "/Content/Statements/" + l.ToString("MM-yyyy") + "_" + "yearlyReport" + "/" + accountNo + "_" + pdfDate + ".pdf";
                            //us.url = "/" + DateTime.Now.ToString("MM-yyyy") + "/" + accountNo + "_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".pdf";
                            us.createdate = currenttime2;
                            us.statementdate = f;
                            us.status = 1;
                            us = UserStatementService.Insert(us);

                            Response responseUser = IUserService.GetSingle(ua.UserId);
                            if (responseUser.IsSuccess)
                            {
                                User user = (User)responseUser.Data;
                                Email email = new Email();
                                email.body = "Your Latest Monthly Statement of Accounts is ready for viewing";
                                email.subject = "Statement Notification";
                                email.user = user;
                                String ma = accountNo;
                                String title = "Monthly Statement Notification";
                                String content = "Please log on to eApexIs at <a href='https://eapexis.apexis.com.my/'>eapexis.apexis.com.my</a> to view or print your statement\n ";
                                EmailService.SendAlertUpdateMail(email, ma, title, content, true, Application.StartupPath + "\\EmailTemplate\\UpdateAlert.html");
                            }
                            else
                            {
                                Logger.WriteLog("responseUser: " + responseUser.Message + " - Exception");
                            }
                        }
                    }
                }
                else
                {
                    Logger.WriteLog("maHG: " + maHG.Message + " - Exception");
                }
            }

        }

        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserObj.Value; } }

    }
}
