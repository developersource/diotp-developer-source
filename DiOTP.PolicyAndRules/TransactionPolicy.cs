﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static DiOTP.PolicyAndRules.TransactionPolicy;
using static DiOTP.Utility.CustomClasses.CustomStatus;

namespace DiOTP.PolicyAndRules
{
    public static class TransactionPolicy
    {
        private static object responseCustom;

        public class TransactionClass
        {
            public OrderType OrderType { get; set; }
            public Int32 Step { get; set; }
            public UserAccount userAccount { get; set; }
            public MaHolderReg maHolderReg { get; set; }
            public List<HolderInv> holderInvs { get; set; }
            public Int32 Category { get; set; }
            public List<UtmcFundInformation> utmcFundInformations { get; set; }
            public Int32 UtmcFundInformationId { get; set; }
            public Decimal Amount { get; set; }
            public Decimal Units { get; set; }
            public Int32 DistributionID { get; set; }
            public Int32 BankID { get; set; }
            public string ConsultantID { get; set; }
        }

        public class ResponseBuyStep
        {
            public string Code { get; set; }
            public string Message { get; set; }
            public List<UtmcFundInformation> utmcFundInformations { get; set; }
            public List<HolderInv> holderInvs { get; set; }
            public UserAccount userAccount { get; set; }
            public MaHolderReg maHolderReg { get; set; }
            public List<UtmcFundInformation> categorizedUtmcFundInformations { get; set; }
            public List<string> Names { get; set; }
            public List<string> Risks { get; set; }
            public List<string> Ids { get; set; }
            public List<string> Codes { get; set; }
        }

        public enum TransactionPolicyEnum
        {
            [Description("SUCCESS")]
            S = 0,
            [Description("EXCEPTION")]
            E = 99,
            [Description("CORPORATE account can not buy funds online. Please contact Apex Management.")]
            UA1 = 1,
            [Description("EPF account can not buy funds online. Please contact Apex Management.")]
            UA2 = 2,
            [Description("Only PRINCIPAL account can do transaction.")]
            UA3 = 3,
            [Description("Please Proceed to Update SAT.")]
            UA4 = 4,
            [Description("Please Proceed to Update SAT.")]
            UA5 = 5,
        }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUserOrderService> lazyUserOrderService = new Lazy<IUserOrderService>(() => new UserOrderService());
        public static IUserOrderService IUserOrderService { get { return lazyUserOrderService.Value; } }
        public static object Validate(TransactionClass transactionClass)
        {
            Response response = new Response();
            try
            {
                if ((transactionClass.OrderType == OrderType.Buy && transactionClass.Step == 1) || transactionClass.OrderType == OrderType.Sell && transactionClass.Step == 1 || transactionClass.OrderType == OrderType.SwitchOut && transactionClass.Step == 1)
                {
                    ResponseBuyStep responseBuyStep1 = new ResponseBuyStep();
                    //MA Account Validation
                    string accPlan = CustomValues.GetAccounPlan(transactionClass.userAccount.HolderClass);
                    if (accPlan == "CORP")
                    {
                        responseBuyStep1.Code = TransactionPolicyEnum.UA1.ToString();
                        response.Data = responseBuyStep1;
                    }
                    else if (accPlan == "EPF" && transactionClass.OrderType == OrderType.Buy)
                    {
                        responseBuyStep1.Code = TransactionPolicyEnum.UA2.ToString();
                        response.Data = responseBuyStep1;
                    }
                    else
                    {
                        if (transactionClass.userAccount.IsPrinciple == 0)
                        {
                            responseBuyStep1.Code = TransactionPolicyEnum.UA3.ToString();
                            response.Data = responseBuyStep1;
                        }
                        else
                        {
                            Response responseSAT = CheckIfSATUpdated(transactionClass.userAccount);
                            if (responseSAT.IsSuccess)
                            {
                                bool isSATUpdated = (bool)responseSAT.Data;
                                bool isOneYearAgo = responseSAT.IsDBAvailable;
                                if (!isSATUpdated)
                                {
                                    if (!isOneYearAgo)
                                    {
                                        responseBuyStep1.Code = TransactionPolicyEnum.UA5.ToString();
                                        response.Data = responseBuyStep1;
                                    }
                                    else
                                    {
                                        responseBuyStep1.Code = TransactionPolicyEnum.UA4.ToString();
                                        response.Data = responseBuyStep1;
                                    }
                                }
                                else
                                {
                                    Response responseMHR = ServiceManager.GetMaHolderRegByAccountNo(transactionClass.userAccount.AccountNo);
                                    if (responseMHR.IsSuccess)
                                    {
                                        MaHolderReg maHolderReg = (MaHolderReg)responseMHR.Data;

                                        Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status = 'Active' ", 0, 0, false);
                                        if (responseUFIList.IsSuccess)
                                        {
                                            List<UtmcFundInformation> UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                                            Response responseHIList = GetHolderInvByHolderNo(transactionClass.userAccount.AccountNo, UTMCFundInformations);
                                            if (responseHIList.IsSuccess)
                                            {
                                                List<HolderInv> holderInvs = (List<HolderInv>)responseHIList.Data;
                                                //SUCCESS
                                                responseBuyStep1.Code = TransactionPolicyEnum.S.ToString();
                                                responseBuyStep1.utmcFundInformations = UTMCFundInformations;
                                                responseBuyStep1.holderInvs = holderInvs;
                                                responseBuyStep1.maHolderReg = maHolderReg;
                                                responseBuyStep1.userAccount = transactionClass.userAccount;

                                                response.IsSuccess = true;
                                                response.Data = responseBuyStep1;

                                            }
                                            else
                                            {
                                                //EXCEPTION
                                                responseBuyStep1.Code = TransactionPolicyEnum.E.ToString();
                                                responseBuyStep1.Message = "responseHIList: " + responseHIList.Message;
                                                response.Data = responseBuyStep1;
                                            }
                                        }
                                        else
                                        {
                                            //EXCEPTION
                                            responseBuyStep1.Code = TransactionPolicyEnum.E.ToString();
                                            responseBuyStep1.Message = "responseUFIList: " + responseUFIList.Message;
                                            response.Data = responseBuyStep1;
                                        }
                                    }
                                    else
                                    {
                                        //EXCEPTION
                                        responseBuyStep1.Code = TransactionPolicyEnum.E.ToString();
                                        responseBuyStep1.Message = "responseMHR: " + responseMHR.Message;
                                        response.Data = responseBuyStep1;
                                    }
                                }
                            }
                            else
                            {
                                //EXCEPTION
                                responseBuyStep1.Code = TransactionPolicyEnum.E.ToString();
                                responseBuyStep1.Message = "responseSAT: " + responseSAT.Message;
                                response.Data = responseBuyStep1;
                            }
                        }
                    }
                    //SAT Validation
                }
                //response.Data = (OrderType)transactionClass.OrderType;
                if (transactionClass.OrderType == OrderType.Buy)
                {
                    if (transactionClass.Step == 2)
                    {
                        //Fund Minimum Investment Validation
                        ResponseBuyStep responseBuyStep2 = new ResponseBuyStep();


                        List<UtmcFundInformation> UTMCFundInformations = transactionClass.utmcFundInformations;
                        //Categorizing Funds
                        if (CustomValues.isEPF(transactionClass.maHolderReg.HolderCls))
                        {
                            UTMCFundInformations = UTMCFundInformations.Where(x => x.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().IsEpfApproved == 1).ToList();
                            if (transactionClass.maHolderReg.EpfIStatus == "I")
                            {
                                UTMCFundInformations = UTMCFundInformations.Where(x => x.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().ShariahCompliant == 1).ToList();
                            }
                        }
                        string userAccSATGroupByScore = CustomValues.GetGroupByScore(transactionClass.userAccount.SatScore);
                        userAccSATGroupByScore = userAccSATGroupByScore.Split('-')[0].Trim();
                        int group = userAccSATGroupByScore[1];

                        List<UtmcFundInformation> categorizedUtmcFundInformations = new List<UtmcFundInformation>();
                        List<string> name = new List<string>();
                        List<string> id = new List<string>();
                        List<string> riskGroup = new List<string>();
                        List<string> code = new List<string>();
                        foreach (UtmcFundInformation uFI in UTMCFundInformations)
                        {
                            string riskCompared = "";
                            string risk = "";
                            if (uFI.SatGroup.ToLower().Contains(userAccSATGroupByScore.ToLower()))
                            {
                                riskCompared = "Recommended";
                            }
                            else
                            {
                                string uFSATGroup = uFI.SatGroup;
                                string[] gs = uFSATGroup.Split(',');
                                foreach (string g in gs)
                                {
                                    int groupF = g[1];
                                    if (groupF < group)
                                    {
                                        riskCompared = "Low Risk";
                                    }
                                    if (groupF > group)
                                    {
                                        riskCompared = "High Risk";
                                    }
                                }
                            }

                            risk = CustomValues.GetNameByGroup(uFI.SatGroup);

                            if (transactionClass.Category == 2)
                            {
                                if (riskCompared == "Low Risk")
                                {
                                    categorizedUtmcFundInformations.Add(uFI);

                                    riskGroup.Add(risk + " - " + riskCompared);
                                    name.Add(uFI.FundName.Capitalize());
                                    code.Add(uFI.FundCode);
                                    id.Add(uFI.Id.ToString());
                                }
                                if (riskCompared == "Recommended")
                                {
                                    categorizedUtmcFundInformations.Add(uFI);

                                    riskGroup.Add(risk + " - " + riskCompared);
                                    name.Add(uFI.FundName.Capitalize());
                                    code.Add(uFI.FundCode);
                                    id.Add(uFI.Id.ToString());
                                }
                            }
                            else if (transactionClass.Category == 3)
                            {
                                if (riskCompared == "High Risk")
                                {
                                    categorizedUtmcFundInformations.Add(uFI);

                                    riskGroup.Add(risk + " - " + riskCompared);
                                    name.Add(uFI.FundName.Capitalize());
                                    id.Add(uFI.Id.ToString());
                                    code.Add(uFI.FundCode);
                                }
                            }
                        }

                        responseBuyStep2.categorizedUtmcFundInformations = categorizedUtmcFundInformations;
                        responseBuyStep2.Names = name;
                        responseBuyStep2.Ids = id;
                        responseBuyStep2.Codes = code;
                        responseBuyStep2.Risks = riskGroup;


                        //Validation
                        if (transactionClass.UtmcFundInformationId != 0)
                        {
                            UtmcFundInformation utmcFundInformation = UTMCFundInformations.FirstOrDefault(x => x.Id == transactionClass.UtmcFundInformationId);
                            HolderInv holderInv = transactionClass.holderInvs.FirstOrDefault(x => x.FundId == utmcFundInformation.IpdFundCode);
                            Decimal Amount = transactionClass.Amount;
                            Int32 DistributionID = transactionClass.DistributionID;
                            Int32 BankID = transactionClass.BankID;

                            if (Amount == 0)
                            {
                                response.IsSuccess = false;
                                response.Message = "Amount is Required";
                                return response;
                            }
                            else if (DistributionID == 0)
                            {
                                response.IsSuccess = false;
                                response.Message = "Distribution instruction is Required";
                                return response;
                            }
                            else
                            {
                                var fundMinInvestCash = utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().MinInitialInvestmentCash;
                                var fundMinInvestEPF = utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().MinInitialInvestmentEpf;
                                var fundMinSubInvestCash = utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().MinSubsequentInvestmentCash;
                                var fundMinSubInvestEPF = utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().MinSubsequentInvestmentEpf;
                                var accountPlan = CustomValues.GetAccounPlan(transactionClass.userAccount.HolderClass);
                                if (accountPlan == "EPF")
                                {
                                    if (holderInv != null)
                                    {
                                        if (Amount < fundMinSubInvestEPF)
                                        {
                                            response.IsSuccess = false;
                                            response.Message = "Min Sub Investment Amount is MYR<span class='currencyFormat2dp'> " + fundMinSubInvestEPF.ToString("N2", new CultureInfo("en-US")) + "</span>";
                                            return response;
                                        }
                                    }
                                    else
                                    {
                                        if (Amount < fundMinInvestEPF)
                                        {
                                            response.IsSuccess = false;
                                            response.Message = "Min Sub Investment Amount is MYR<span class='currencyFormat2dp'> " + fundMinInvestEPF.ToString("N2", new CultureInfo("en-US")) + "</span>";
                                            return response;
                                        }
                                    }
                                }
                                else if (accountPlan == "CASH" || accountPlan == "JOINT")
                                {
                                    if (holderInv != null)
                                    {
                                        if (Amount < fundMinSubInvestCash)
                                        {
                                            response.IsSuccess = false;
                                            response.Message = "Min Sub Investment Amount is MYR<span class='currencyFormat2dp'> " + fundMinSubInvestCash.ToString("N2", new CultureInfo("en-US")) + "</span>";
                                            return response;
                                        }
                                    }
                                    else
                                    {
                                        if (Amount < fundMinInvestCash)
                                        {
                                            response.IsSuccess = false;
                                            response.Message = "Min Investment Amount is MYR<span class='currencyFormat2dp'> " + fundMinInvestCash.ToString("N2", new CultureInfo("en-US")) + "</span>";
                                            return response;
                                        }
                                    }
                                }
                            }
                        }




                        responseBuyStep2.Code = TransactionPolicyEnum.S.ToString();
                        response.IsSuccess = true;
                        response.Data = responseBuyStep2;

                    }
                    else if (transactionClass.Step == 3)
                    {
                        //Confirmation Validation
                    }
                    else if (transactionClass.Step == 4)
                    {
                        //Final Validation
                    }
                }
                else if (transactionClass.OrderType == OrderType.Sell)
                {
                    if (transactionClass.Step == 1)
                    {
                        //MA Account Validation

                        //SAT Validation
                    }
                    else if (transactionClass.Step == 2)
                    {
                        //Fund Redemption Validation
                        ResponseBuyStep responseBuyStep2 = new ResponseBuyStep();

                        //Validation
                        if (transactionClass.UtmcFundInformationId != 0)
                        {
                            List<UtmcFundInformation> UTMCFundInformations = transactionClass.utmcFundInformations;
                            UtmcFundInformation utmcFundInformation = UTMCFundInformations.FirstOrDefault(x => x.Id == transactionClass.UtmcFundInformationId);
                            HolderInv holderInv = transactionClass.holderInvs.FirstOrDefault(x => x.FundId == utmcFundInformation.IpdFundCode);
                            Decimal Units = transactionClass.Units;

                            if (Units == 0)
                            {
                                response.IsSuccess = false;
                                response.Message = "Units is Required";
                                return response;
                            }
                            else
                            {
                                var fundMinHoldingUnits = utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().MinHoldingUnits;
                                var fundMinHoldingRedemptionUnits = utmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().MinRedAmountUnits;
                                var accountPlan = CustomValues.GetAccounPlan(transactionClass.userAccount.HolderClass);
                                decimal availableUnits = 0;
                                Response responseUOList = IUserOrderService.GetDataByFilter(" user_account_id = '" + transactionClass.userAccount.Id + "' and fund_id = '" + utmcFundInformation.Id + "' and (order_type='2' or order_type='3') and (order_status='1' or order_status='2') and status=1 ", 0, 0, false);
                                if (responseUOList.IsSuccess)
                                {
                                    List<UserOrder> uos = (List<UserOrder>)responseUOList.Data;
                                    availableUnits = holderInv.CurrUnitHldg - uos.Sum(x => x.Units);
                                }
                                if (Units > availableUnits)
                                {
                                    response.IsSuccess = false;
                                    response.Message = "Available units insufficient.";
                                    return response;
                                }
                                else if (Units < fundMinHoldingRedemptionUnits)
                                {
                                    response.IsSuccess = false;
                                    response.Message = "Minimum redemption units is " + fundMinHoldingRedemptionUnits.ToString("N4", new CultureInfo("en-US")) + ".";
                                    return response;
                                }
                                else if (Units > availableUnits - fundMinHoldingUnits && (availableUnits - Units) != 0)
                                {
                                    response.IsSuccess = false;
                                    response.Message = "Minimum holding unit must be not less than " + fundMinHoldingUnits.ToString("N4", new CultureInfo("en-US"));
                                    return response;
                                }

                                else if (Units > holderInv.CurrUnitHldg)
                                {
                                    response.IsSuccess = false;
                                    response.Message = "Cannot exceed current holding: " + holderInv.CurrUnitHldg.ToString("N4", new CultureInfo("en-US"));
                                    return response;
                                }
                                else
                                {
                                    responseBuyStep2.Code = TransactionPolicyEnum.S.ToString();
                                    response.IsSuccess = true;
                                    response.Data = responseBuyStep2;
                                    return response;
                                }

                            }

                        }
                    }
                    else if (transactionClass.Step == 3)
                    {
                        //Confirmation Validation
                    }
                    else if (transactionClass.Step == 4)
                    {
                        //Final Validation
                    }
                }
                else if (transactionClass.OrderType == OrderType.SwitchOut)
                {
                    if (transactionClass.Step == 1)
                    {
                        //MA Account Validation

                        //SAT Validation
                    }
                    else if (transactionClass.Step == 2)
                    {
                        //Fund Minimum Investment Validation

                    }
                    else if (transactionClass.Step == 3)
                    {
                        //Confirmation Validation
                    }
                    else if (transactionClass.Step == 4)
                    {
                        //Final Validation
                    }
                }
                else if (transactionClass.OrderType == OrderType.SwitchIn)
                {
                    if (transactionClass.Step == 1)
                    {
                        //MA Account Validation

                        //SAT Validation
                    }
                    else if (transactionClass.Step == 2)
                    {
                        //Fund Minimum Investment Validation

                    }
                    else if (transactionClass.Step == 3)
                    {
                        //Confirmation Validation
                    }
                    else if (transactionClass.Step == 4)
                    {
                        //Final Validation
                    }
                }
                else if (transactionClass.OrderType == OrderType.RSP)
                {
                    if (transactionClass.Step == 1)
                    {
                        //MA Account Validation

                        //SAT Validation
                    }
                    else if (transactionClass.Step == 2)
                    {
                        //Fund Minimum Investment Validation

                    }
                    else if (transactionClass.Step == 3)
                    {
                        //Confirmation Validation
                    }
                    else if (transactionClass.Step == 4)
                    {
                        //Final Validation
                    }
                }
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public static Response CheckIfSATUpdated(UserAccount primaryAcc)
        {
            Response responseSAT = new Response();
            bool isOneYearAgo = false;
            bool isSATUpdated = false;
            try
            {

                if (primaryAcc.IsSatChecked == 0)
                    isSATUpdated = false;
                if (primaryAcc.SatUpdatedDate != null)
                {
                    DateTime SatUpdatedDate = primaryAcc.SatUpdatedDate.Value;
                    int yearDays = 365;
                    if (DateTime.IsLeapYear(SatUpdatedDate.Year))
                        yearDays = 366;

                    if ((DateTime.Now - SatUpdatedDate).TotalDays > yearDays)
                    {
                        isSATUpdated = false;
                        isOneYearAgo = false;
                    }
                    else
                    {
                        isSATUpdated = true;
                        isOneYearAgo = true;
                    }
                }
                else
                    isSATUpdated = false;
                responseSAT.IsSuccess = true;
                responseSAT.IsDBAvailable = isOneYearAgo;
            }
            catch (Exception ex)
            {
                responseSAT.IsSuccess = false;
                responseSAT.Message = ex.Message;
                Logger.WriteLog("ServicesManager CheckIfSATUpdated: " + ex.Message);
            }

            responseSAT.Data = isSATUpdated;

            return responseSAT;
        }
        
        public static Response GetHolderInvByHolderNo(string holderNo, List<UtmcFundInformation> UTMCFundInformations)
        {
            Response responseCustom = new Response();
            try
            {
                string fundIds = String.Join(",", UTMCFundInformations.Select(x => "'" + x.IpdFundCode + "'").ToList());
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync("api/HolderInvApi?holderNo=" + holderNo + "&fundIds=" + fundIds).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                        Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                        if (responseFromApi.IsSuccess)
                        {
                            responseCustom.IsSuccess = true;
                            List<HolderInv> holderInvs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<HolderInv>>(responseFromApi.Data.ToString());
                            holderInvs = holderInvs.OrderBy(x => x.FundId).ToList();
                            responseCustom.Data = holderInvs;
                        }
                        else
                            responseCustom = responseFromApi;
                    }
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

    }
    public static class TransactionPolicyEnumExtensions
    {
        public static string ToDescriptionString(this TransactionPolicyEnum val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
