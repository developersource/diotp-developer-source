﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static DiOTP.PolicyAndRules.FundPolicy;

namespace DiOTP.PolicyAndRules
{
    public class FundPolicy
    {
        public class FundClass
        {
            public Int32 Step { get; set; }
            public String FundCode { get; set; }
            public List<UtmcFundInformation> utmcFundInformations { get; set; }
            public Int32[] selectedFunds { get; set; }
            public Int32 dataValue { get; set; }
            public HttpContext httpContext { get; set; }
        }

        public class ResponseFundStep1
        {
            public string Code { get; set; }
            public string Message { get; set; }
            public List<UtmcFundInformation> utmcFundInformations { get; set; }
            public String utmcFundInformationsTbodyString { get; set; }
        }

        public class ResponseFundStep2
        {
            public string Code { get; set; }
            public string Message { get; set; }
            public UtmcFundInformation utmcFundInformation { get; set; }
            public FundInfo fundInfo { get; set; }
            public List<UtmcFundFile> Downloadfiles { get; set; }
            public List<UtmcDailyNavFund> utmcDailyNavFunds { get; set; }
            public List<UtmcFundCorporateActions> Distributions { get; set; }
            public FundReturn fr { get; set; }
        }

        public class ResponseFundStep3
        {
            public string Code { get; set; }
            public string Message { get; set; }
            public List<FundChartInformation> FundChartInformations { get; set; }
        }

        public enum FundPolicyEnum
        {
            [Description("SUCCESS")]
            S = 0,
            [Description("EXCEPTION")]
            E = 99,
        }

        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        private static readonly Lazy<IFundInfoService> lazyFundInfoObj = new Lazy<IFundInfoService>(() => new FundInfoService());

        public static IFundInfoService IFundInfoService { get { return lazyFundInfoObj.Value; } }

        private static readonly Lazy<IUtmcFundDetailService> lazyfdObj = new Lazy<IUtmcFundDetailService>(() => new UtmcFundDetailService());

        public static IUtmcFundDetailService IUtmcFundDetailService { get { return lazyfdObj.Value; } }


        private static readonly Lazy<IUtmcDailyNavFundService> lazyUtmcDailyNavFundObj = new Lazy<IUtmcDailyNavFundService>(() => new UtmcDailyNavFundService());

        public static IUtmcDailyNavFundService IUtmcDailyNavFundService { get { return lazyUtmcDailyNavFundObj.Value; } }

        private static readonly Lazy<IUtmcFundCorporateActionService> lazyUtmcFundCorporateActionObj = new Lazy<IUtmcFundCorporateActionService>(() => new UtmcFundCorporateActionService());

        public static IUtmcFundCorporateActionService IUtmcFundCorporateActionService { get { return lazyUtmcFundCorporateActionObj.Value; } }

        private static readonly Lazy<IFundReturnService> lazyIFundReturnServiceObj = new Lazy<IFundReturnService>(() => new FundReturnService());

        public static IFundReturnService IFundReturnService { get { return lazyIFundReturnServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundFileService> IutmcFundFileService = new Lazy<IUtmcFundFileService>(() => new UtmcFundFileService());

        public static IUtmcFundFileService IutmcFundFile { get { return IutmcFundFileService.Value; } }

        private static readonly Lazy<IFundChartInfoService> lazyIFundChartInfoServiceObj = new Lazy<IFundChartInfoService>(() => new FundChartInfoService());
        public static IFundChartInfoService IFundChartInfoService { get { return lazyIFundChartInfoServiceObj.Value; } }

        public static object Get(FundClass fundClass)
        {
            Response response = new Response();
            try
            {
                if (fundClass.Step == 1)
                {
                    ResponseFundStep1 responseFundStep1 = new ResponseFundStep1();
                    Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                    if (responseUFIList.IsSuccess)
                    {
                        List<UtmcFundInformation> UtmcFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;

                        Response responseFIList = IFundInfoService.GetData(0, 0, false);
                        if (responseFIList.IsSuccess)
                        {
                            List<FundInfo> FundInfos = (List<FundInfo>)responseFIList.Data;
                            StringBuilder sb = new StringBuilder();
                            int idx = 1;
                            foreach (UtmcFundInformation utmcFundInformation in UtmcFundInformations)
                            {
                                Response responseUFDList = IUtmcFundDetailService.GetDataByPropertyName(nameof(UtmcFundDetail.UtmcFundInformationId), utmcFundInformation.Id.ToString(), true, 0, 0, false);
                                if (responseUFDList.IsSuccess)
                                {
                                    UtmcFundDetail ufd = ((List<UtmcFundDetail>)responseUFDList.Data).FirstOrDefault();
                                    FundInfo fDI = FundInfos.Where(x => x.FundCode == utmcFundInformation.IpdFundCode).FirstOrDefault();
                                    //CurrentNAVDTO currentNAVDTO = GlobalProperties.CurrentNAVDTOs.FirstOrDefault(x => x.FUND_ID == utmcFundInformation.IpdFundCode);
                                    string loginMenu = "";
                                    Boolean IsImpersonate = false;
                                    User user = (User)fundClass.httpContext.Session["user"];
                                    bool showTrans = true;
                                    if (user != null)
                                    {
                                        IsImpersonate = IsImpersonated(fundClass.httpContext);
                                        showTrans = user.UserIdUserAccounts.Select(x => x.IsPrinciple).Distinct().Contains(1);
                                    }
                                    if (showTrans)
                                    {
                                        loginMenu += @"<li><a href='/BuyFunds.aspx?fundCode=" + utmcFundInformation.FundCode + @"'>Buy</a></li>";

                                    }
                                    if (user != null && !IsImpersonate && showTrans)
                                        loginMenu += @"<li><a href='/SellFunds.aspx?fundCode=" + utmcFundInformation.FundCode + @"'>Sell</a></li>
                                                        <li><a href='/SwitchFunds.aspx?fundCode=" + utmcFundInformation.FundCode + @"'>Switch</a></li>";

                                    sb.Append(@"<tr class='fs-14'>
                                                    <td class='fs-10'>" + idx + @"</td>
                                                    <td><span class='fundName'>" + utmcFundInformation.FundCode + @"</span> <span class='ml-10'>" + (utmcFundInformation.IsRetail == 0 ? "<img src='/Content/MyImage/12.png' height='16' style='margin-top:-5px;' />" : "") + @"</span><span class='fundType'>" + (ufd.IsEpfApproved == 1 ? "EPF-MIS" : "") + @"</span> <br><span class='fundFullname'>" + utmcFundInformation.FundName.Capitalize() + @"</span></td>
                                                    <td>" + String.Format("{0:0.0000}", fDI.CurrentUnitPrice) + @"</td>
                                                    <td><span class='" + (fDI.ChangePrice > 0 ? "fund-success-text" : fDI.ChangePrice == 0 ? "fund-warning-text" : "fund-danger-text") + @"'>" + String.Format("{0:0.0000}", fDI.ChangePrice) + @"</span></td>
                                                    <td><span class='" + (fDI.ChangePer > 0 ? "fund-success" : fDI.ChangePer == 0 ? "fund-warning" : "fund-danger") + @"'>" + String.Format("{0:0.00}", fDI.ChangePer) + @" %</span></td>
                                                    <td>" + utmcFundInformation.FundBaseCurrency + @"</td>
                                                    <td>" + utmcFundInformation.LipperCategoryOfFund + @"</td>
                                                    <td>" + fDI.CurrentNavDate.ToString("dd/MM/yyyy") + @" </td>
                                                    <td class='hide'>" + utmcFundInformation.FundCls + @"</td>
                                                    <td class='hide'>" + utmcFundInformation.IsRetail + @"</td>
                                                    <td>" + (IsImpersonate == false ? (utmcFundInformation.Status == "Active" && showTrans ? @"<div class='btn-groups'><li class='dropdown trans'><a href='#' class='dropdown-toggle btn my-btn1' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false' id='btnTrade'>Transact</a>
                                                                                <ul class='dropdown-menu'>
                                                                                    " + loginMenu + @"
                                                                                 </ul>
                                                                                </li>" : "") : "") + @"
                                                                               <a href='/Fund-Information.aspx?fundCode=" + utmcFundInformation.FundCode + @"' class='btn my-btn'>Details</a></div></td>
                                                </tr>");
                                    idx++;
                                }
                            }
                            responseFundStep1.utmcFundInformations = UtmcFundInformations;
                            responseFundStep1.utmcFundInformationsTbodyString = sb.ToString();
                            responseFundStep1.Code = FundPolicyEnum.S.ToString();

                            response.IsSuccess = true;
                            response.Data = responseFundStep1;
                        }
                        else
                        {
                            responseFundStep1.Code = FundPolicyEnum.E.ToString();
                            response.Message = responseFIList.Message;
                            response.Data = responseFundStep1;
                        }
                    }
                    else
                    {
                        responseFundStep1.Code = FundPolicyEnum.E.ToString();
                        response.Message = responseUFIList.Message;
                        response.Data = responseFundStep1;
                    }
                }
                else if (fundClass.Step == 2)
                {
                    ResponseFundStep2 responseFundStep2 = new ResponseFundStep2();

                    String propName = nameof(UtmcFundInformation.FundCode);
                    Response responseUFIList = IUtmcFundInformationService.GetDataByPropertyName(propName, fundClass.FundCode, true, 0, 0, false);
                    if (responseUFIList.IsSuccess)
                    {
                        UtmcFundInformation utmcFundInformation = ((List<UtmcFundInformation>)responseUFIList.Data).FirstOrDefault();
                        responseFundStep2.utmcFundInformation = utmcFundInformation;
                        propName = nameof(FundInfo.FundCode);

                        Response responseFIList = IFundInfoService.GetDataByPropertyName(propName, utmcFundInformation.IpdFundCode, true, 0, 0, false);
                        if (responseFIList.IsSuccess)
                        {
                            FundInfo fundInfo = ((List<FundInfo>)responseFIList.Data).FirstOrDefault();
                            responseFundStep2.fundInfo = fundInfo;

                            string fundID = nameof(UtmcFundFile.UtmcFundInformationId);
                            Response responseUFFList = IutmcFundFile.GetDataByPropertyName(fundID, utmcFundInformation.Id.ToString(), true, 0, 0, false);
                            if (responseUFFList.IsSuccess)
                            {
                                List<UtmcFundFile> Downloadfiles = ((List<UtmcFundFile>)responseUFFList.Data).Where(x => x.Status == 1).ToList();
                                responseFundStep2.Downloadfiles = Downloadfiles;


                                //Fund Performance
                                DateTime currentDate = fundInfo.CurrentNavDate;
                                DateTime ToDate = DateTime.Now;


                                //Nav History Popup
                                //ToDate = currentDate.AddMonths(-3);
                                //fundNameSpanNavPriceHistory.InnerHtml = utmcFundInformation.FundName.Capitalize() + " - <small>" + utmcFundInformation.FundCode + "</small>";
                                //fundNameSpanNavPriceHistory.Attributes.Add("href", "/FundInformation.aspx?fund="+ utmcFundInformation.FundCode);
                                StringBuilder sb = new StringBuilder();
                                int idx = 1;
                                propName = nameof(UtmcDailyNavFund.IpdFundCode);
                                //Re-check *****************
                                //Response responseUDNFList = IUtmcDailyNavFundService.GetDataByFilter(" ipd_fund_code = '" + utmcFundInformation.IpdFundCode + "' and daily_nav_date > '" + ToDate.ToString("yyyy-MM-dd") + "' ", 0, 0, true);
                                Response responseUDNFList = IUtmcDailyNavFundService.GetDataByFilter(" ipd_fund_code = '" + utmcFundInformation.IpdFundCode + "' ", 0, 0, true);
                                //List<CurrentNAVDTO> NAVHistory = ServicesManager.GetNAVHistoryByFund("'" + utmcFundInformation.IpdFundCode + "'", 3);
                                if (responseUDNFList.IsSuccess)
                                {
                                    List<UtmcDailyNavFund> utmcDailyNavFunds = (List<UtmcDailyNavFund>)responseUDNFList.Data;
                                    responseFundStep2.utmcDailyNavFunds = utmcDailyNavFunds;
                                    sb = new StringBuilder();
                                    idx = 1;
                                    String distributionQuery = "select * from utmc_fund_corporate_actions where ipd_fund_code = '" + utmcFundInformation.IpdFundCode + "' order by corporate_action_date desc";
                                    Response responseDistribution = GenericService.GetDataByQuery(distributionQuery, 0, 0, false, null, false, null, true);
                                    if (responseDistribution.IsSuccess)
                                    {
                                        var DistributionsDyn = responseDistribution.Data;
                                        var responseJSON = JsonConvert.SerializeObject(DistributionsDyn);
                                        List<UtmcFundCorporateActions> Distributions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcFundCorporateActions>>(responseJSON);
                                        responseFundStep2.Distributions = Distributions;
                                    }
                                    string propertyName = nameof(FundReturn.FundCode);
                                    Response responseFRList = IFundReturnService.GetDataByPropertyName(propertyName, utmcFundInformation.IpdFundCode, true, 0, 0, false);
                                    if (responseFRList.IsSuccess)
                                    {
                                        FundReturn fr = ((List<FundReturn>)responseFRList.Data).FirstOrDefault();
                                        responseFundStep2.fr = fr;
                                        responseFundStep2.Code = FundPolicyEnum.S.ToString();
                                        response.IsSuccess = true;
                                        response.Data = responseFundStep2;
                                    }
                                    else
                                    {
                                        responseFundStep2.Code = FundPolicyEnum.E.ToString();
                                        response.Message = responseFRList.Message;
                                        response.Data = responseFundStep2;
                                    }
                                }
                                else
                                {
                                    responseFundStep2.Code = FundPolicyEnum.E.ToString();
                                    response.Message = responseUDNFList.Message;
                                        response.Data = responseFundStep2;
                                }
                            }
                            else
                            {
                                responseFundStep2.Code = FundPolicyEnum.E.ToString();
                                response.Message = responseUFFList.Message;
                                        response.Data = responseFundStep2;
                            }
                        }
                        else
                        {
                            responseFundStep2.Code = FundPolicyEnum.E.ToString();
                            response.Message = responseFIList.Message;
                                        response.Data = responseFundStep2;
                        }
                    }
                    else
                    {
                        responseFundStep2.Code = FundPolicyEnum.E.ToString();
                        response.Message = responseUFIList.Message;
                                        response.Data = responseFundStep2;
                    }
                }
                else if (fundClass.Step == 3)
                {
                    ResponseFundStep3 responseFundStep3 = new ResponseFundStep3();
                    string propertyName = nameof(FundChartInfo.DataValue);
                    Response responseFCIList = IFundChartInfoService.GetDataByPropertyName(propertyName, fundClass.dataValue.ToString(), true, 0, 0, false);
                    if (responseFCIList.IsSuccess)
                    {
                        FundChartInfo fCI = ((List<FundChartInfo>)responseFCIList.Data).Where(x => x.FundId == fundClass.selectedFunds.FirstOrDefault()).FirstOrDefault();
                        List<FundChartInformation> FundChartInformations = new List<FundChartInformation>();
                        if (fCI != null)
                        {
                            FundChartInformations.Add(new FundChartInformation
                            {
                                average = fCI.Average,
                                currentNav = fCI.CurrentNav,
                                currentNavDate = fCI.CurrentNavDate,
                                fundId = fCI.FundId,
                                fundName = fCI.FundName.Capitalize(),
                                fundCode = fundClass.utmcFundInformations.Where(x => x.Id == fCI.FundId).FirstOrDefault().FundCode,
                                Labels = fCI.Labels.Split(',').ToList(),
                                maxChange = fCI.MaxChange,
                                maxNav = fCI.MaxNav,
                                minChange = fCI.MinChange,
                                minNav = fCI.MinNav,
                                NavDates = fCI.NavDates.Split(',').ToList(),
                                NavPrices = fCI.NavPrices.Split(',').ToList(),
                                threeYearAnnualisedPercent = fCI.ThreeYearAnnualisedPercent,
                                totalReturns = fCI.TotalReturns,
                                Values = fCI.NavChanges.Split(',').ToList()
                            });
                        }
                        responseFundStep3.Code = FundPolicyEnum.S.ToString();
                        responseFundStep3.FundChartInformations = FundChartInformations;
                        response.IsSuccess = true;
                        response.Data = responseFundStep3;
                    }
                    else
                    {
                        responseFundStep3.Code = FundPolicyEnum.E.ToString();
                        response.Message = responseFCIList.Message;
                        response.Data = responseFundStep3;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public static Boolean IsImpersonated(HttpContext Context)
        {
            Boolean IsImpersonated = false;
            if (Context.Session["IsImpersonated"] != null)
            {
                IsImpersonated = (Boolean)Context.Session["IsImpersonated"];
            }
            return IsImpersonated;
        }

    }
    public static class FundPolicyEnumExtensions
    {
        public static string ToDescriptionString(this FundPolicyEnum val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
