﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static DiOTP.PolicyAndRules.StatementPolicy;

namespace DiOTP.PolicyAndRules
{
    public class StatementPolicy
    {
        public class StatementClass
        {
            public Int32 Step { get; set; }
            public HttpContext httpContext { get; set; }
            public string FundAccount { get; set; }
            public string StatementType { get; set; }
            public string FromMonth { get; set; }
            public string ToMonth { get; set; }
        }

        public class ResponseStatementStep1
        {
            public string Code { get; set; }
            public string Message { get; set; }
            public List<UserAccount> userAccounts { get; set; }
        }

        public class ResponseStatementStep2
        {
            public string Code { get; set; }
            public string Message { get; set; }
            public string Username { get; set; }
            public String TbodyString { get; set; }
        }

        public enum StatementPolicyEnum
        {
            [Description("SUCCESS")]
            S = 0,
            [Description("EXCEPTION")]
            E = 99,
            [Description("Session expired!")]
            SE = 1,
            [Description("Please select Statement Type")]
            SST = 2,
            [Description("Please select a Month(From)")]
            SFM = 3,
            [Description("Please select a Month(To)")]
            STM = 4,
            [Description("Month(From) cannot be greater than Month(To)")]
            FMGTM = 5
        }

        private static readonly Lazy<IUserStatementService> lazyUserStamentServiceObj = new Lazy<IUserStatementService>(() => new UserStatementService());

        public static IUserStatementService IUserStatementService { get { return lazyUserStamentServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        public static object Get(StatementClass statementClass)
        {
            Response response = new Response();
            try
            {
                if (statementClass.Step == 1)
                {
                    ResponseStatementStep1 responseStatementStep1 = new ResponseStatementStep1();
                    if (statementClass.httpContext.Session["user"] != null)
                    {
                        User user = (User)statementClass.httpContext.Session["user"];

                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                            responseStatementStep1.Code = StatementPolicyEnum.S.ToString();
                            responseStatementStep1.userAccounts = userAccounts;
                            response.IsSuccess = true;
                            response.Data = responseStatementStep1;
                        }
                        else
                        {
                            responseStatementStep1.Code = StatementPolicyEnum.E.ToString();
                            response.IsSuccess = true; //ATHT20220210I
                            response.Data = responseStatementStep1;
                            response.Message = responseUAList.Message;
                        }
                    }
                    else
                    {
                        responseStatementStep1.Code = StatementPolicyEnum.SE.ToString();
                        response.IsSuccess = true; //ATHT20220210I
                        response.Data = responseStatementStep1;
                    }
                }
                else if(statementClass.Step == 2)
                {
                    ResponseStatementStep2 responseStatementStep2 = new ResponseStatementStep2();
                    if (statementClass.httpContext.Session["user"] != null)
                    {
                        string filter = "";
                        if (statementClass.StatementType == "1" || statementClass.StatementType == "3")
                        {
                            if (statementClass.StatementType == "1" && statementClass.FromMonth == "")
                            {
                                responseStatementStep2.Code = StatementPolicyEnum.SFM.ToString();
                                response.IsSuccess = true; //ATHT20220210I
                                response.Data = responseStatementStep2;
                            }
                            else if (statementClass.StatementType == "1" && statementClass.ToMonth == "")
                            {
                                responseStatementStep2.Code = StatementPolicyEnum.STM.ToString();
                                response.IsSuccess = true; //ATHT20220210I
                                response.Data = responseStatementStep2;
                            }
                            else
                            {
                                DateTime startMonth = (statementClass.FromMonth != "" ? new DateTime(Convert.ToInt32(statementClass.FromMonth.Split('_')[0]), Convert.ToInt32(statementClass.FromMonth.Split('_')[1]), 1) : default(DateTime));
                                DateTime endMonth = (statementClass.ToMonth != "" ? new DateTime(Convert.ToInt32(statementClass.ToMonth.Split('_')[0]), Convert.ToInt32(statementClass.ToMonth.Split('_')[1]), 1) : default(DateTime));

                                if (statementClass.StatementType.ToString() == "1" && (startMonth > endMonth))
                                {
                                    responseStatementStep2.Code = StatementPolicyEnum.FMGTM.ToString();
                                    response.IsSuccess = true; //ATHT20220210I
                                    response.Data = responseStatementStep2;
                                }
                                
                                else
                                {
                                    User user = (User)(statementClass.httpContext.Session["user"]);
                                    Response responseUAList = IUserAccountService.GetDataByFilter(" account_no in (" + statementClass.FundAccount.ToString() + ") and status=1 ", 0, 0, true);
                                    if (responseUAList.IsSuccess)
                                    {
                                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                                        if (statementClass.StatementType.ToString() == "1")
                                        {
                                            filter = " user_account_id in (" + String.Join(",", userAccounts.Select(x => x.Id).ToArray()) + ") and user_statement_category_id=1 and statement_date between '" + startMonth.ToString("yyyy-MM-dd") + "' and '" + endMonth.ToString("yyyy-MM-dd") + "' ";
                                        }

                                        Response responseUSList = IUserStatementService.GetDataByFilter(filter, 0, 0, true);
                                        if (responseUSList.IsSuccess)
                                        {
                                            List<UserStatement> userStatements = (List<UserStatement>)responseUSList.Data;
                                            userStatements = userStatements.OrderByDescending(x => x.StatementDate).ToList();
                                            responseStatementStep2.Username = user.Username;
                                            StringBuilder sb = new StringBuilder();
                                            if (userStatements.Count > 0)
                                            {
                                                int i = 1;
                                                int space = 0;
                                                string month = "";
                                                foreach (UserStatement us in userStatements)
                                                {
                                                    DateTime reportMonth = us.StatementDate.Value;
                                                    if (us.UserStatementCategoryId == 1)
                                                    {
                                                        month = CustomValues.GetMonthNameByNumber(reportMonth.Month);
                                                    }
                                                    if (us.UserStatementCategoryId == 3)
                                                    {
                                                        reportMonth = reportMonth.AddYears(-1);
                                                    }

                                                    space = us.Name.IndexOf('_');
                                                    Response response1 = IUserAccountService.GetSingle(us.UserAccountId);
                                                    if (response1.IsSuccess)
                                                    {
                                                        UserAccount uA = (UserAccount)response1.Data;

                                                        Response responseMHR = ServiceManager.GetMaHolderRegByAccountNo(uA.AccountNo);
                                                        MaHolderReg maHolderReg = new MaHolderReg();
                                                        if (responseMHR.IsSuccess)
                                                        {
                                                            maHolderReg = (MaHolderReg)responseMHR.Data;
                                                            sb.Append(@"<tr>
                                                                <td>" + i + @"</td>
                                                                <td>" + uA.AccountNo + "-" + CustomValues.GetAccounPlan(maHolderReg.HolderCls) + @"</td>
                                                                <td>" + month + reportMonth.Year + @"</td>
                                                                <td>" + (us.UserStatementCategoryId == 1 ? "Monthly" : "Yearly") + @"</td>
                                                                <td><a href='" + us.Url + @"' download> Download </a> </td>
                                                                </ tr > ");
                                                            i++;
                                                        }
                                                        else
                                                        {
                                                            responseStatementStep2.Code = StatementPolicyEnum.E.ToString();
                                                            response.IsSuccess = true; //ATHT20220210I
                                                            response.Message = responseMHR.Message;
                                                            response.Data = responseStatementStep2;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        responseStatementStep2.Code = StatementPolicyEnum.E.ToString();
                                                        response.IsSuccess = true; //ATHT20220210I
                                                        response.Message = response1.Message;
                                                        response.Data = responseStatementStep2;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                sb.Append(@"<tr>
                                                                <td colspan='5' class='text-center'>No records found</td>
                                                                </tr> ");
                                            }
                                            responseStatementStep2.Code = StatementPolicyEnum.S.ToString();
                                            responseStatementStep2.TbodyString = sb.ToString();
                                            response.IsSuccess = true;
                                            response.Data = responseStatementStep2;

                                        }
                                        else
                                        {
                                            responseStatementStep2.Code = StatementPolicyEnum.E.ToString();
                                            response.IsSuccess = true; //ATHT20220210I
                                            response.Message = responseUSList.Message;
                                            response.Data = responseStatementStep2;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            responseStatementStep2.Code = StatementPolicyEnum.SST.ToString();
                            response.IsSuccess = true; //ATHT20220210I
                            response.Data = responseStatementStep2;
                        }
                    }
                    else
                    {
                        responseStatementStep2.Code = StatementPolicyEnum.SE.ToString();
                        response.IsSuccess = true; //ATHT20220210I
                        response.Data = responseStatementStep2;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }

    public static class StatementPolicyEnumExtensions
    {
        public static string ToDescriptionString(this StatementPolicyEnum val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
