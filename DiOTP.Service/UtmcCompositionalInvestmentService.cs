﻿using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service
{
    public class UtmcCompositionalInvestmentService : IUtmcCompositionalInvestmentService
    {
        private static readonly Lazy<UtmcCompositionalInvestmentRepo> lazy = new Lazy<UtmcCompositionalInvestmentRepo>(() => new UtmcCompositionalInvestmentRepo());
        public static UtmcCompositionalInvestmentRepo IUtmcCompositionalInvestmentRepo { get { return lazy.Value; } }
        public Response PostData(UtmcCompositionalInvestment obj)
        {
            Response response = new Response();
            try
            {
                response = IUtmcCompositionalInvestmentRepo.PostData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UtmcCompositionalInvestment> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUtmcCompositionalInvestmentRepo.PostBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UtmcCompositionalInvestment obj)
        {
            Response response = new Response();
            try
            {
                response = IUtmcCompositionalInvestmentRepo.UpdateData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UtmcCompositionalInvestment> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUtmcCompositionalInvestmentRepo.UpdateBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UtmcCompositionalInvestment DeleteData(Int32 Id)
        {
            UtmcCompositionalInvestment obj = new UtmcCompositionalInvestment();
            try
            {
                obj = IUtmcCompositionalInvestmentRepo.DeleteData(Id);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return obj;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                result = IUtmcCompositionalInvestmentRepo.DeleteBulkData(Ids);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UtmcCompositionalInvestment obj = new UtmcCompositionalInvestment();
            try
            {
                response = IUtmcCompositionalInvestmentRepo.GetSingle(Id);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcCompositionalInvestment> objs = new List<UtmcCompositionalInvestment>();
            try
            {
                response = IUtmcCompositionalInvestmentRepo.GetData(skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                count = IUtmcCompositionalInvestmentRepo.GetCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcCompositionalInvestment> objs = new List<UtmcCompositionalInvestment>();
            try
            {
                response = IUtmcCompositionalInvestmentRepo.GetDataByPropertyName(propertyName, propertyValue, isEqual, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            Int32 count = 0;
            try
            {
                count = IUtmcCompositionalInvestmentRepo.GetCountByPropertyName(propertyName, propertyValue, isEqual);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcCompositionalInvestment> objs = new List<UtmcCompositionalInvestment>();
            try
            {
                response = IUtmcCompositionalInvestmentRepo.GetDataByFilter(filter, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                count = IUtmcCompositionalInvestmentRepo.GetCountByFilter(filter);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
