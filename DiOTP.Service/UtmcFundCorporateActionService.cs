using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service
{
    public class UtmcFundCorporateActionService : IUtmcFundCorporateActionService
    {
        private static readonly Lazy<IUtmcFundCorporateActionRepo> lazy = new Lazy<IUtmcFundCorporateActionRepo>(() => new UtmcFundCorporateActionRepo());
        public static IUtmcFundCorporateActionRepo IUtmcFundCorporateActionRepo { get { return lazy.Value; } }
        public Response PostData(UtmcFundCorporateAction obj)
        {
            Response response = new Response();
            try
            {
                response = IUtmcFundCorporateActionRepo.PostData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UtmcFundCorporateAction> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUtmcFundCorporateActionRepo.PostBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UtmcFundCorporateAction obj)
        {
            Response response = new Response();
            try
            {
                response = IUtmcFundCorporateActionRepo.UpdateData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UtmcFundCorporateAction> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUtmcFundCorporateActionRepo.UpdateBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UtmcFundCorporateAction DeleteData(Int32 Id)
        {
            UtmcFundCorporateAction obj = new UtmcFundCorporateAction();
            try
            {
                obj = IUtmcFundCorporateActionRepo.DeleteData(Id);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return obj;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                result = IUtmcFundCorporateActionRepo.DeleteBulkData(Ids);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UtmcFundCorporateAction obj = new UtmcFundCorporateAction();
            try
            {
                response = IUtmcFundCorporateActionRepo.GetSingle(Id);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundCorporateAction> objs = new List<UtmcFundCorporateAction>();
            try
            {
                response = IUtmcFundCorporateActionRepo.GetData(skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                count = IUtmcFundCorporateActionRepo.GetCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundCorporateAction> objs = new List<UtmcFundCorporateAction>();
            try
            {
                response = IUtmcFundCorporateActionRepo.GetDataByPropertyName(propertyName, propertyValue, isEqual, skip, take, false);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            Int32 count = 0;
            try
            {
                count = IUtmcFundCorporateActionRepo.GetCountByPropertyName(propertyName, propertyValue, isEqual);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundCorporateAction> objs = new List<UtmcFundCorporateAction>();
            try
            {
                response = IUtmcFundCorporateActionRepo.GetDataByFilter(filter, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                count = IUtmcFundCorporateActionRepo.GetCountByFilter(filter);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
