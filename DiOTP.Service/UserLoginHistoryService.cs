using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service
{
    public class UserLoginHistoryService : IUserLoginHistoryService
    {
        private static readonly Lazy<IUserLoginHistoryRepo> lazy = new Lazy<IUserLoginHistoryRepo>(() => new UserLoginHistoryRepo());
        public static IUserLoginHistoryRepo IUserLoginHistoryRepo { get { return lazy.Value; } }
        public Response PostData(UserLoginHistory obj)
        {
            Response response = new Response();
            try
            {
                response = IUserLoginHistoryRepo.PostData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UserLoginHistory> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUserLoginHistoryRepo.PostBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UserLoginHistory obj)
        {
            Response response = new Response();
            try
            {
                response = IUserLoginHistoryRepo.UpdateData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UserLoginHistory> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUserLoginHistoryRepo.UpdateBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UserLoginHistory DeleteData(Int32 Id)
        {
            UserLoginHistory obj = new UserLoginHistory();
            try
            {
                obj = IUserLoginHistoryRepo.DeleteData(Id);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return obj;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                result = IUserLoginHistoryRepo.DeleteBulkData(Ids);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UserLoginHistory obj = new UserLoginHistory();
            try
            {
                response = IUserLoginHistoryRepo.GetSingle(Id);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserLoginHistory> objs = new List<UserLoginHistory>();
            try
            {
                response = IUserLoginHistoryRepo.GetData(skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                count = IUserLoginHistoryRepo.GetCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserLoginHistory> objs = new List<UserLoginHistory>();
            try
            {
                response = IUserLoginHistoryRepo.GetDataByPropertyName(propertyName, propertyValue, isEqual, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            Int32 count = 0;
            try
            {
                count = IUserLoginHistoryRepo.GetCountByPropertyName(propertyName, propertyValue, isEqual);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserLoginHistory> objs = new List<UserLoginHistory>();
            try
            {
                response = IUserLoginHistoryRepo.GetDataByFilter(filter, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                count = IUserLoginHistoryRepo.GetCountByFilter(filter);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
