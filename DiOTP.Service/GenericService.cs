﻿using DiOTP.Data;
using DiOTP.Utility.CustomAttributes;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service
{
    public static class GenericService
    {
        static Response response = new Response();

        public static Response CheckConnection()
        {
            return GenericRepo.CheckConnection();
        }
        public static Response PostData<T>(T obj)
        {
            response = GenericRepo.PostData<T>(obj);
            return response;
        }

        public static Response UpdateData<T>(T obj)
        {
            response = GenericRepo.UpdateData<T>(obj);
            return response;
        }
        public static Response GetSingle<T>(int Id, bool isAllProperties, List<string> properties, bool isGetChildren)
        {
            response = GenericRepo.GetSingle<T>(Id, isAllProperties, properties, isGetChildren);
            return response;
        }

        public static Response GetData<T>(int skip, int take, bool isOrderByDesc, string orderByProp, bool isAllProperties, List<string> properties, bool isGetChildren, bool isGroupBy, string groupByProp)
        {
            response = GenericRepo.GetData<T>(skip, take, isOrderByDesc, orderByProp, isAllProperties, properties, isGetChildren, isGroupBy, groupByProp);
            return response;
        }

        public static Response GetCount<T>(bool isGroupBy, string groupByProp)
        {
            response = GenericRepo.GetCount<T>(isGroupBy, groupByProp);
            return response;
        }

        public static Response GetDataByFilter<T>(string filter, int skip, int take, bool isOrderByDesc, string orderByProp, bool isAllProperties, List<string> properties, bool isGetChildren, bool isGroupBy, string groupByProp)
        {
            response = GenericRepo.GetDataByFilter<T>(filter, skip, take, isOrderByDesc, orderByProp, isAllProperties, properties, isGetChildren, isGroupBy, groupByProp);
            return response;
        }

        public static Response GetCountByFilter<T>(string filter, bool isGroupBy, string groupByProp)
        {
            response = GenericRepo.GetCountByFilter<T>(filter, isGroupBy, groupByProp);
            return response;
        }

        public static Response GetDataByJoin<T>(string filter, Type typeb, string typebFilter, int skip, int take, bool isOrderByDesc, string orderByProp, bool isAllProperties, List<string> properties, bool isGetChildren, bool isGroupBy, string groupByProp)
        {
            response = GenericRepo.GetDataByJoin<T>(filter, typeb, typebFilter, skip, take, isOrderByDesc, orderByProp, isAllProperties, properties, isGetChildren, isGroupBy, groupByProp);
            return response;
        }

        public static Response GetCountByJoin<T>(string filter, Type typeb, string typebFilter, int skip, int take, bool isGroupBy, string groupByProp)
        {
            response = GenericRepo.GetCountByJoin<T>(filter, typeb, typebFilter, skip, take, isGroupBy, groupByProp);
            return response;
        }

        public static Response GetDataByQuery(string query, int skip, int take, bool isOrderByDesc, string orderByProp, bool isGroupBy, string groupByColumn, bool maintainColumnName)
        {
            response = GenericRepo.GetDataByQuery(query, skip, take, isOrderByDesc, orderByProp, isGroupBy, groupByColumn, maintainColumnName);
            return response;
        }

        public static Response GetCountByQuery(string query)
        {
            response = GenericRepo.GetCountByQuery(query);
            return response;
        }

        public static Response GetSingleNew<T>(this T obj, int Id, bool isAllProperties, List<string> properties, bool isGetChildren)
        {
            response = GenericRepo.GetSingleNew<T>(obj, Id, isAllProperties, properties, isGetChildren);
            return response;
        }

        //NEW
        public static Response GetSingleEx<T>(Int32 Id)
        {
            response = GenericRepo.GetSingleEx<T>(Id);
            return response;
        }

        /// <summary>
        /// Pulls data by query returns dataset
        /// </summary>
        public static Response PullData(string query, int skip, int take, bool maintainColumnName)
        {
            response = GenericRepo.PullData(query, skip, take, maintainColumnName);
            return response;
        }

        public static Response PullData<T>(int skip, int take)
        {
            response = GenericRepo.PullData<T>(skip, take);
            return response;
        }
        public static Response PullData<T>(int skip, int take, bool isOrderByDesc, string OrderByPropName)
        {
            response = GenericRepo.PullData<T>(skip, take, isOrderByDesc, OrderByPropName);
            return response;
        }

        public static Response PullData<T>(string filter, int skip, int take, bool isOrderByDesc, string OrderByPropName)
        {
            response = GenericRepo.PullData<T>(filter, skip, take, isOrderByDesc, OrderByPropName);
            return response;
        }

        public static Response LeftJoin<T>(this T obj, string propName)
        {
            response = GenericRepo.LeftJoin<T>(obj, propName);
            return response;
        }

        public static Response GetGroupByData<T>(int skip, int take, bool isOrderByDesc, string OrderByPropName, string GroupByPropName)
        {
            response = GenericRepo.GetGroupByData<T>(skip, take, isOrderByDesc, OrderByPropName, GroupByPropName);
            return response;
        }

        public static Response PullDataCount<T>(string filter, bool isGroupBy, string groupByProp)
        {
            response = GenericRepo.GetCountByFilter<T>(filter, isGroupBy, groupByProp);
            return response;
        }

    }
}
