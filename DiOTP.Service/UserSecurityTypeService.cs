using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service
{
    public class UserSecurityTypeService : IUserSecurityTypeService
    {
        private static readonly Lazy<IUserSecurityTypeRepo> lazy = new Lazy<IUserSecurityTypeRepo>(() => new UserSecurityTypeRepo());
        public static IUserSecurityTypeRepo IUserSecurityTypeRepo { get { return lazy.Value; } }
        public Response PostData(UserSecurityType obj)
        {
            Response response = new Response();
            try
            {
                response = IUserSecurityTypeRepo.PostData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UserSecurityType> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUserSecurityTypeRepo.PostBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UserSecurityType obj)
        {
            Response response = new Response();
            try
            {
                response = IUserSecurityTypeRepo.UpdateData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UserSecurityType> objs)
        {
            Int32 result = 0;
            try
            {
                result = IUserSecurityTypeRepo.UpdateBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UserSecurityType DeleteData(Int32 Id)
        {
            UserSecurityType obj = new UserSecurityType();
            try
            {
                obj = IUserSecurityTypeRepo.DeleteData(Id);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return obj;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                result = IUserSecurityTypeRepo.DeleteBulkData(Ids);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UserSecurityType obj = new UserSecurityType();
            try
            {
                response = IUserSecurityTypeRepo.GetSingle(Id);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserSecurityType> objs = new List<UserSecurityType>();
            try
            {
                response = IUserSecurityTypeRepo.GetData(skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                count = IUserSecurityTypeRepo.GetCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserSecurityType> objs = new List<UserSecurityType>();
            try
            {
                response = IUserSecurityTypeRepo.GetDataByPropertyName(propertyName, propertyValue, isEqual, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            Int32 count = 0;
            try
            {
                count = IUserSecurityTypeRepo.GetCountByPropertyName(propertyName, propertyValue, isEqual);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserSecurityType> objs = new List<UserSecurityType>();
            try
            {
                response = IUserSecurityTypeRepo.GetDataByFilter(filter, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                count = IUserSecurityTypeRepo.GetCountByFilter(filter);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
