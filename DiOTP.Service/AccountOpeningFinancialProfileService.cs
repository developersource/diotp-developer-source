﻿using DiOTP.Data;
using DiOTP.Data.IRepo;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Service
{
    public class AccountOpeningFinancialProfileService : IAccountOpeningFinancialProfileService
    {
        private static readonly Lazy<IAccountOpeningFinancialProfileRepo> lazy = new Lazy<IAccountOpeningFinancialProfileRepo>(() => new AccountOpeningFinancialProfileRepo());
        public static IAccountOpeningFinancialProfileRepo IAccountOpeningFinancialProfileRepo { get { return lazy.Value; } }
        public Response PostData(AccountOpeningFinancialProfile obj)
        {
            Response response = new Response();
            try
            {
                response = IAccountOpeningFinancialProfileRepo.PostData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<AccountOpeningFinancialProfile> objs)
        {
            Int32 result = 0;
            try
            {
                result = IAccountOpeningFinancialProfileRepo.PostBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(AccountOpeningFinancialProfile obj)
        {
            Response response = new Response();
            try
            {
                response = IAccountOpeningFinancialProfileRepo.UpdateData(obj);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<AccountOpeningFinancialProfile> objs)
        {
            Int32 result = 0;
            try
            {
                result = IAccountOpeningFinancialProfileRepo.UpdateBulkData(objs);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public AccountOpeningFinancialProfile DeleteData(Int32 Id)
        {
            AccountOpeningFinancialProfile obj = new AccountOpeningFinancialProfile();
            try
            {
                obj = IAccountOpeningFinancialProfileRepo.DeleteData(Id);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return obj;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                result = IAccountOpeningFinancialProfileRepo.DeleteBulkData(Ids);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            AccountOpeningFinancialProfile obj = new AccountOpeningFinancialProfile();
            try
            {
                response = IAccountOpeningFinancialProfileRepo.GetSingle(Id);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<AccountOpeningFinancialProfile> objs = new List<AccountOpeningFinancialProfile>();
            try
            {
                response = IAccountOpeningFinancialProfileRepo.GetData(skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                count = IAccountOpeningFinancialProfileRepo.GetCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<AccountOpeningFinancialProfile> objs = new List<AccountOpeningFinancialProfile>();
            try
            {
                response = IAccountOpeningFinancialProfileRepo.GetDataByPropertyName(propertyName, propertyValue, isEqual, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            Int32 count = 0;
            try
            {
                count = IAccountOpeningFinancialProfileRepo.GetCountByPropertyName(propertyName, propertyValue, isEqual);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<AccountOpeningFinancialProfile> objs = new List<AccountOpeningFinancialProfile>();
            try
            {
                response = IAccountOpeningFinancialProfileRepo.GetDataByFilter(filter, skip, take, isOrderByDesc);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                count = IAccountOpeningFinancialProfileRepo.GetCountByFilter(filter);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
