﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class Payments
    {
        public int ID { get; set; }
        public int user_id { get; set; }
        public String username { get; set; }
        public int user_acount_id { get; set; }
        public String account_no { get; set; }
        public DateTime created_date { get; set; }
        public DateTime updated_date { get; set; }
        public DateTime? payment_date { get; set; }
        public DateTime? settlement_date { get; set; }
        public String order_no { get; set; }
        public int order_type { get; set; }
        public decimal amount { get; set; }
        public decimal units { get; set; }
        public String payment_method { get; set; }
        public decimal SumAmount { get; set; }
        public decimal SumUnits { get; set; }
        public int order_status { get; set; }
        public String reject_reason { get; set; }
        public String fund_name { get; set; }
        public String fund_name2 { get; set; }
        public String allfunds1 { get; set; }
        public String allfunds2 { get; set; }
        public String allamounts { get; set; }
        public String allunits { get; set; }
        public String distribution_instruction { get; set; }
        public String fpx_bank_code { get; set; }
        public String consultantId { get; set; }
        public String bank_name { get; set; }
        public String bank_account_no { get; set; }
    }
}
