﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class FpxResponseCode
    {
        public Int32 ID { get; set; }
        public String Code { get; set; }
        public String Name { get; set; }
        public Int32 status { get; set; }
    }
}
