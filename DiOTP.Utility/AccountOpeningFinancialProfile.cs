﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("ao_financial_profiles")]
    public class AccountOpeningFinancialProfile
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("account_opening_id"), Display("AccountOpeningId")]
        public Int32 AccountOpeningId { get; set; }
        [Database("purpose"), Display("Purpose")]
        public String Purpose { get; set; }
        [Database("purpose_description"), Display("PurposeDescription")]
        public String PurposeDescription { get; set; }
        
        [Database("source"), Display("Source")]
        public String Source { get; set; }
        [Database("employed_by_fund_company"), Display("EmployedByFundCompany")]
        public Int32 EmployedByFundCompany { get; set; }
        [Database("relationship"), Display("Relationship")]
        public String Relationship { get; set; }
        [Database("name_of_funder"), Display("NameOfFunder")]
        public String NameOfFunder { get; set; }
        [Database("funders_industory"), Display("FundersIndustory")]
        public String FundersIndustory { get; set; }
        [Database("is_funder_money_changer"), Display("IsFunderMoneyChanger")]
        public Int32 IsFunderMoneyChanger { get; set; }
        [Database("is_funder_beneficial_owner"), Display("IsFunderBeneficialOwner")]
        public Int32 IsFunderBeneficialOwner { get; set; }
        [Database("fund_owner_name"), Display("FundOwnerName")]
        public String FundOwnerName { get; set; }
        [Database("estimated_net_worth"), Display("EstimatedNetWorth")]
        public String EstimatedNetWorth { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        [Database("annual_income"), Display("Anuual Income")]
        public string AnnualIncome { get; set; }
        [Database("is_sophisticated_investor"), Display("Is Sophisticated Investor")]
        public Int32 IsSI { get; set; }
        [Database("sophisticated_investor_type"), Display("Sophisticated Investor Type")]
        public String SIType { get; set; }
    }
}
