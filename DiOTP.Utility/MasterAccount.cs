﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class MasterAccount
    {
        public string IPD_Member_Acc_No { get; set; }
        public string HOLDER_CLS { get; set; }

        public string NAME_1 { get; set; }
        public string ADDR_1 { get; set; }
        public string ADDR_2 { get; set; }
        public string ADDR_3 { get; set; }
        public string ADDR_4 { get; set; }
    }
}
