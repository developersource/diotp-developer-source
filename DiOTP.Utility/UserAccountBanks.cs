﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("user_account_banks")]
    public class UserAccountBanks
    {
        [Database("ID"), Display("ID")]
        public Int32 ID { get; set; }
        [Database("ma_holder_bank_id"), Display("MaHolderBankId")]
        public Int32 MaHolderBankId { get; set; }
        [Database("user_account_id"), Display("UserAccountId")]
        public Int32 UserAccountId { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
    }
}
