using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("user_corp_accounts")]
    public class UserCorpAccount
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("is_primary"), Display("IsPrimary")]
        public Int32 IsPrimary { get; set; }
        [Database("ma_holder_reg_id"), Display("MaHolderRegId")]
        public Int32 MaHolderRegId { get; set; }
        [Database("account_no"), Display("AccountNo")]
        public String AccountNo { get; set; }
        [Database("is_verified"), Display("IsVerified")]
        public Int32 IsVerified { get; set; }
        [Database("verification_code"), Display("VerificationCode")]
        public Int32? VerificationCode { get; set; }
        [Database("is_sat_checked"), Display("IsSatChecked")]
        public Int32 IsSatChecked { get; set; }
        [Database("sat_updated_date"), Display("SatUpdatedDate")]
        public DateTime? SatUpdatedDate { get; set; }
        [Database("sat_score"), Display("SatScore")]
        public Int32 SatScore { get; set; }
        [Database("is_hardcopy"), Display("IsHardcopy")]
        public Int32 IsHardcopy { get; set; }
        [Database("hardcopy_updated_date"), Display("HardcopyUpdatedDate")]
        public DateTime? HardcopyUpdatedDate { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("modified_by"), Display("ModifiedBy")]
        public Int32? ModifiedBy { get; set; }
        [Database("modified_date"), Display("ModifiedDate")]
        public DateTime? ModifiedDate { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        public User ModifiedByUser { get; set; }
        public User CreatedByUser { get; set; }
        public MaHolderReg MaHolderRegIdMaHolderReg { get; set; }
        public User UserIdUser { get; set; }
    }
}
