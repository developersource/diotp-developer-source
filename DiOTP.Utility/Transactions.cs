﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class Transactions
    {
        public int ID { get; set; }
        public int user_id { get; set; }
        public String username { get; set; }
        public int user_acount_id { get; set; }
        public String account_no { get; set; }
        public DateTime updated_date { get; set; }
        public String order_no { get; set; }
        public int order_type { get; set; }
        public decimal amount { get; set; }
        public decimal units { get; set; }
        public String payment_method { get; set; }
        public decimal SumAmount { get; set; }
        public decimal SumUnits { get; set; }
        public int order_status { get; set; }
        public String reject_reason { get; set; }
        public String fpx_bank_code { get; set; }
    }
}
