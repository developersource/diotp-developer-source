using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("notification_types_def")]
    public class NotificationTypesDef
    {
        public List<UserNotificationSetting> NotificationTypeDefIdUserNotificationSettings { get; set; }
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("notification_category_def_id"), Display("NotificationCategoryDefId")]
        public Int32 NotificationCategoryDefId { get; set; }
        [Database("name"), Display("Name")]
        public String Name { get; set; }
        [Database("field_id"), Display("BindId")]
        public String FieldId { get; set; }
        [Database("description"), Display("Description")]
        public String Description { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        public NotificationCategoriesDef NotificationCategoryDefIdNotificationCategoriesDef { get; set; }
    }
}
