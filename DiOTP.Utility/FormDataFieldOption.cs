using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("form_data_field_options")]
    public class FormDataFieldOption
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("form_data_field_id"), Display("FormDataFieldId")]
        public Int32 FormDataFieldId { get; set; }
        [Database("name"), Display("Name")]
        public String Name { get; set; }
        [Database("score"), Display("Score")]
        public Int32 Score { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        public FormDataField FormDataFieldIdFormDataField { get; set; }
    }
}
