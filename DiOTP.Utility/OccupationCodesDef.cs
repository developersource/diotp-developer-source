using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("occupation_codes_def")]
    public class OccupationCodesDef
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("code"), Display("Code")]
        public String Code { get; set; }
        [Database("name"), Display("Name")]
        public String Name { get; set; }
        [Database("download_ind"), Display("DownloadInd")]
        public String DownloadInd { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
    }
}
