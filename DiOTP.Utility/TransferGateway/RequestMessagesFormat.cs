﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.TransferGateway
{
    public class RequestMessagesFormat
    {
    }

    public class PRODUCT
    {
        public String CODE { get; set; }
        public String RESERVE1 { get; set; }
        public String RESERVE2 { get; set; }
        public String RESERVE3 { get; set; }
    }

    public class RequestMessagesFormatBodyTR
    {
        public String CORRADDRESS1 { get; set; }
        public String CORRADDRESS2 { get; set; }
        public String CORRADDRESS3 { get; set; }
        public String CORRADDRESS4 { get; set; }
        public String CORRADDRESS5 { get; set; }
        public String CORRCITY { get; set; }
        public Int32 CORRPOSTCODE { get; set; }
        public String CORRSTATE { get; set; }
        public String CORRCOUNTRY { get; set; }
        public Int32 CORRMOBILENO { get; set; }
        public Int32 CORRHOUSENO { get; set; }
        public Int32 CORROFFICENO { get; set; }
        public String CORREMAIL { get; set; }
        public Decimal WDWAMOUNT { get; set; }
        public List<THIRDPARTY> THIRDPARTYS { get; set; }
    }

    public class RequestMessagesFormatFooterTR
    {
        public String AMENDMENTURL { get; set; }
        public String COMPLETIONURL { get; set; }
    }

}
