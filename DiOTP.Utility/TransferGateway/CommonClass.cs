﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.TransferGateway
{
    public class CommonClass
    {
        
    }

    public class HEADER
    {
        public String MSGTYPE { get; set; }
        public String SCHEME { get; set; }
        public String TOKEN { get; set; }
        public String REFID { get; set; }
        public String LANGUAGEFLAG { get; set; }
        public DateTime TRXNDATETIME { get; set; }
        public String CHANNELCODE { get; set; }
        public String ENCRYPTVALUE { get; set; }
    }

    public class THIRDPARTY
    {
        public String CODE { get; set; }
        public String REFNO { get; set; }
        public String ACCNO { get; set; }
        public List<PRODUCT> PRODUCTS { get; set; }
    }
}
