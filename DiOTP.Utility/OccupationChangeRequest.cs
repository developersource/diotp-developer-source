﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class OccupationChangeRequest
    {
        public Int32 Id { get; set; }
        public String IdNo { get; set; }
        public String Name1 { get; set; }
        public String HolderNo { get; set; }
        public String OccCode { get; set; }
        public String IncomeCode { get; set; }
        public String Nob { get; set; }
        public String NameOfEmployer { get; set; }
        public String OtpActSt { get; set; }
        public String OtpEmailAdd { get; set; }
        public String OtpMobileNo { get; set; }
        public DateTime? OtpExpiryDate { get; set; }
        public DateTime? OtpEntDt { get; set; }
        public String OtpVersion { get; set; }
        public DateTime? OtpEntBy { get; set; }
        public DateTime? OtpTacCd { get; set; }
        public String OfficeAddress1 { get; set; }
        public String OfficeAddress2 { get; set; }
        public String OfficeAddress3 { get; set; }
        public String OfficePostCode { get; set; }
        public String OfficeCity { get; set; }
        public String OfficeState { get; set; }
        public String OfficeCountry { get; set; }
        public String OfficeTelNo { get; set; }
        public String Addr1 { get; set; }
        public String Addr2 { get; set; }
        public String Addr3 { get; set; }
        public String PostCode { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String Country { get; set; }
        public String OfficeTelNo2 { get; set; }       
        public Int32 Isemployed { get; set; }
    }
}
