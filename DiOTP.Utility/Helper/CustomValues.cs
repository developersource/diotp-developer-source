﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.Helper
{
    public class CustomValues
    {
        public static bool isEPF(string holderClass)
        {
            if (holderClass == "EB" ||
                holderClass == "EN" ||
                holderClass == "ER" ||
                holderClass == "ES" ||
                holderClass == "EZ")
                return true;
            else
                return false;
        }

        public static string GetAccounPlan(string holderClass)
        {
            if (holderClass == "EB" ||
                holderClass == "EN" ||
                holderClass == "ER" ||
                holderClass == "ES" ||
                holderClass == "EZ")
                return "EPF";
            else if (holderClass == "RN" ||
                holderClass == "BI" ||
                holderClass == "NI" ||
                holderClass == "FI" ||
                holderClass == "B1" ||
                holderClass == "B2" ||
                holderClass == "B3" ||
                holderClass == "N1" ||
                holderClass == "N2" ||
                holderClass == "N3" ||
                holderClass == "H1" ||
                holderClass == "H2" ||
                holderClass == "SB" ||
                holderClass == "SF" ||
                holderClass == "SN" ||
                holderClass == "SS" ||
                holderClass == "MS"
                )
                return "CASH";
            else if (holderClass == "RC" ||
                holderClass == "BC" ||
                holderClass == "NC" ||
                holderClass == "FC")
                return "CORP";
            else
                return "JOINT";
        }

        public static string GetGroupByScore(Int32 SatScore)
        {
            if (SatScore >= 8 && SatScore <= 14)
            {
                return "G1 - Conservative";
            }
            else if (SatScore >= 15 && SatScore <= 21)
            {
                return "G2 - Moderate";
            }
            else if (SatScore >= 22 && SatScore <= 27)
            {
                return "G3 - Moderately Aggressive"; 
            }
            else if (SatScore >= 28)
            {
                return "G4 - Aggressive";
            }
            //else if (SatScore >= 31)
            //{
            //    return "G5 - Aggressive";
            //}
            else
            {
                return "";
            }
        }

        public static string GetGroupOnlyByScore(Int32 SatScore)
        {
            if (SatScore >= 8 && SatScore < 14)
                return "G1";
            //else if (SatScore >= 6 && SatScore <= 13)
            //    return "G2";
            else if (SatScore >= 15 && SatScore <= 21)
                return "G2";
            else if (SatScore >= 22 && SatScore <= 27)
                return "G3";
            else if (SatScore >= 28)
                return "G4";
            else
                return "";
        }

        public static string GetNameByGroup(String group)
        {
            string name = "";

            switch (group)
            {
                //case "G1":
                //case "G2":
                //    name = "Conservative";
                //    break;
                //case "G1,G2":
                //    name = "Conservative";
                //    break;
                //case "G3":
                //    name = "Moderate";
                //    break;
                //case "G4":
                //    name = "Moderately Aggressive";
                //    break;
                //case "G5":
                //    name = "Aggressive";
                //    break;
                //default:
                //    name = "";
                //    break;

                case "G1":
                    name = "Conservative";
                    break;
                case "G2":
                    name = "Moderate";
                    break;
                case "G3":
                    name = "Moderately Aggressive";
                    break;
                case "G4":
                    name = "Aggressive";
                    break;
                default:
                    name = "";
                    break;
            }
            return name;
        }

        public static string GetIncomeByCode(String code)
        {
            string codeDesc = "";
            try
            {
                switch (code)
                {
                    case "":
                        codeDesc = "No Code";
                        break;
                    case "01":
                        codeDesc = "BELOW 500";
                        break;
                    case "02":
                        codeDesc = "500 TO 1,000";
                        break;
                    case "03":
                        codeDesc = "1,001 TO 2,000";
                        break;
                    case "04":
                        codeDesc = "2,001 TO 5,000";
                        break;
                    case "05":
                        codeDesc = "5,001 TO 10,000";
                        break;
                    case "06":
                        codeDesc = "10,001 AND ABOVE";
                        break;
                    case "07":
                        codeDesc = "NIL";
                        break;
                }
            }
            catch(Exception ex)
            {
                codeDesc = ex.Message;
            }
            return codeDesc;
        }

        public static string GetPaymentMethodByCode(String code)
        {
            string codeDesc = "";
            try
            {
                switch (code)
                {
                    case "":
                        codeDesc = "No Code";
                        break;
                    case "IA":
                        codeDesc = "KWSP I ACCOUNT";
                        break;
                    case "CS":
                        codeDesc = "CASH";
                        break;
                    case "CQ":
                        codeDesc = "CHEQUE";
                        break;
                    case "BD":
                        codeDesc = "BANK DRAFT";
                        break;
                    case "CO":
                        codeDesc = "CASHIERS ORDER";
                        break;
                    case "BT":
                        codeDesc = "BANK TRANSFER";
                        break;
                    case "MO":
                        codeDesc = "MONEY ORDER";
                        break;
                    case "TC":
                        codeDesc = "TRAVELLERS CHEQUE";
                        break;
                    case "PO":
                        codeDesc = "POSTAL ORDER";
                        break;
                    case "AC":
                        codeDesc = "ACCOUNT TRANSFER";
                        break;
                    case "SI":
                        codeDesc = "STANDING INSTRUCTION";
                        break;
                    case "QD":
                        codeDesc = "QUICKDEPO SLIP";
                        break;
                    case "KS":
                        codeDesc = "KWSP STAFF";
                        break;
                    case "ON":
                        codeDesc = "ONLINE TRANSACTION";
                        break;
                }
            }
            catch (Exception ex)
            {
                codeDesc = ex.Message;
            }
            return codeDesc.Capitalize();
        }

        public static string GetMonthNameByNumber(int month)
        {
            string monthName = "";
            switch (month)
            {
                case 1:
                    monthName = "Jan ";
                    break;
                case 2:
                    monthName = "Feb ";
                    break;
                case 3:
                    monthName = "Mar ";
                    break;
                case 4:
                    monthName = "Apr ";
                    break;
                case 5:
                    monthName = "May ";
                    break;
                case 6:
                    monthName = "June ";
                    break;
                case 7:
                    monthName = "July ";
                    break;
                case 8:
                    monthName = "Aug ";
                    break;
                case 9:
                    monthName = "Sep ";
                    break;
                case 10:
                    monthName = "Oct ";
                    break;
                case 11:
                    monthName = "Nov ";
                    break;
                case 12:
                    monthName = "Dec ";
                    break;
                default:
                    monthName = "-";
                    break;
            }
            return monthName;
        }

    }
}
