using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomAttributes;
using System.Dynamic;
using System.ComponentModel;
using System.Collections;

namespace DiOTP.Utility.Helper
{
    public static class Converter
    {
        public static List<T> DataTableToList<T>(this DataTable table)
        {
            try
            {
                const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
                var columnNames = table.Columns.Cast<DataColumn>().Select(c => c.ColumnName.ToLower()).ToList();
                var objectProperties = typeof(T).GetProperties(flags).Where(y => y.PropertyType.Namespace == "System" && y.GetCustomAttribute(typeof(DatabaseAttribute), true) != null);
                var generatedProperties = typeof(T).GetProperties(flags).Where(y => y.GetCustomAttribute(typeof(DatabaseAttribute), true) == null);
                List<T> targetList = table.AsEnumerable().Select(dataRow =>
                                            {
                                                var instanceOfT = Activator.CreateInstance<T>();
                                                foreach (var properties in
                                                    objectProperties
                                                        .Where(
                                                            properties =>
                                                            columnNames
                                                                .Contains(
                                                                    (
                                                                        (properties
                                                                        .GetCustomAttributes(typeof(DatabaseAttribute), true)
                                                                        .FirstOrDefault() as DatabaseAttribute)
                                                                        .GetValue()
                                                                    )
                                                                )
                                                                &&
                                                                dataRow[(properties
                                                                    .GetCustomAttributes(typeof(DatabaseAttribute), true)
                                                                    .FirstOrDefault() as DatabaseAttribute)
                                                                    .GetValue()] != DBNull.Value
                                                        )
                                                )
                                                {
                                                    var columnName = (properties
                                                                        .GetCustomAttributes(typeof(DatabaseAttribute), true)
                                                                        .FirstOrDefault() as DatabaseAttribute)
                                                                        .GetValue();
                                                    var dataValue = dataRow[columnName];
                                                    var columnDataType = dataRow.Table.Columns[columnName].DataType.Name;
                                                    properties.SetValue(instanceOfT, columnDataType == "Boolean" ? Convert.ToInt32(dataValue) : dataValue, null);
                                                }

                                                
                                                return instanceOfT;
                                            }).ToList<T>();





                return targetList;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                return null;
            }
        }
        public static string ObjectToQuery<T>(this T obj, string type)
        {
            DatabaseAttribute dynamicType = typeof(T).GetCustomAttributes(typeof(DatabaseAttribute), true).FirstOrDefault() as DatabaseAttribute;
            string tableName = dynamicType.GetValue();
            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
            var objectProperties = typeof(T)
                .GetProperties(flags)
                .Where(y => y.PropertyType.Namespace == "System" && y.GetCustomAttributes(typeof(DatabaseAttribute), true).FirstOrDefault() as DatabaseAttribute != null)
                .Select(y => (y.GetCustomAttributes(typeof(DatabaseAttribute), true).FirstOrDefault() as DatabaseAttribute).GetValue()).Skip(1).ToList();
            var objectPropertyId = obj.GetType().GetProperties(flags).Where(y => y.PropertyType.Namespace == "System").Select(y => y.PropertyType == typeof(DateTime) || y.PropertyType == typeof(DateTime?) ? "STR_TO_DATE('" + Convert.ToDateTime(y.GetValue(obj)).ToString("MM/dd/yyyy hh:mm:ss tt") + "', '%c/%e/%Y %r')" : "\"" + (y.GetValue(obj).ToString().Contains("\"") ? y.GetValue(obj).ToString().Replace("\"", "\\\"") : y.GetValue(obj)) + "\"").FirstOrDefault();

            List<string> objectPropertyValues = new List<string>();
            objectProperties.ForEach(x =>
            {
                var prop = obj
               .GetType()
               .GetProperties(flags).Where(y => y.PropertyType.Namespace == "System" && y.GetCustomAttributes(typeof(DatabaseAttribute), true).FirstOrDefault() as DatabaseAttribute != null)
               .FirstOrDefault(y => (y.GetCustomAttributes(typeof(DatabaseAttribute), true).FirstOrDefault() as DatabaseAttribute).GetValue() == x);
                object propValue = prop.GetValue(obj);
                string value = "";
                if (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?))
                {
                    value = "STR_TO_DATE('" + Convert.ToDateTime(propValue).ToString("MM/dd/yyyy hh:mm:ss tt") + "', '%c/%e/%Y %r')";
                }
                else if (propValue != null && propValue.ToString().Contains("\""))
                {
                    value = "\"" + propValue.ToString().Replace("\"", "\\\"") + "\"";
                }
                else if (propValue != null && propValue.ToString().Contains("\\"))
                {
                    value = "\"" + propValue.ToString().Replace("\\", "\\\\") + "\"";
                }
                else
                {
                    value = "\"" + (propValue == null ? "" : propValue.ToString()) + "\"";
                }
                objectPropertyValues.Add(value);
            });

            List<string> columnNames = objectProperties.ToList();
            StringBuilder sCommand = new StringBuilder();
            List<string> Rows = new List<string>();
            switch (type)
            {
                case "insert":
                    sCommand = new StringBuilder("INSERT INTO " + tableName + "(" + String.Join(", ", columnNames.ToArray()) + ") VALUES ");
                    Rows.Add(string.Format("({0})", String.Join(",", objectPropertyValues.ToArray())));
                    sCommand.Append(string.Join(", ", Rows));
                    sCommand.Append("; ");
                    break;
                case "update":
                    string columnsAndParams = String.Join(", ", columnNames.Select(x => x + " = ###" + x + "###").ToArray());
                    int index = 0;
                    foreach (string val in objectPropertyValues)
                    {
                        string columnName = columnNames[index];
                        columnsAndParams = columnsAndParams.Replace("###" + columnName + "###", val);
                        index++;
                    }
                    sCommand = new StringBuilder("UPDATE " + tableName + " SET " + columnsAndParams);
                    sCommand.Append(" where ID = " + objectPropertyId);
                    sCommand.Append(";");
                    break;
            }
            return sCommand.ToString();
        }

        public static string GetColumnNameByPropertyName<T>(string propertyName)
        {
            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
            string columnName = typeof(T)
                .GetProperties(flags)
                .Where(y => y.Name == propertyName)
                .Select(y =>
                {
                    if (y.GetCustomAttributes(typeof(DatabaseAttribute), true).FirstOrDefault() == null)
                    {
                        return "";
                    }
                    else
                    {
                        return (y
                            .GetCustomAttributes(typeof(DatabaseAttribute), true)
                            .FirstOrDefault() as DatabaseAttribute
                        )
                        .GetValue();
                    }
                }
                ).FirstOrDefault().ToString();
            return columnName;
        }

        public static string GetColumnNameByPropertyName(Type type, string propertyName)
        {
            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
            string columnName = type
                .GetProperties(flags)
                .Where(y => y.Name == propertyName)
                .Select(y =>
                    (y
                        .GetCustomAttributes(typeof(DatabaseAttribute), true)
                        .FirstOrDefault() as DatabaseAttribute
                    )
                    .GetValue()
                ).FirstOrDefault().ToString();
            return columnName;
        }

        public static string GetPropertyNameByColumnName<T>(string columnName)
        {
            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
            string propertyName = typeof(T)
                .GetProperties(flags)
                .Where(y => (y
                        .GetCustomAttributes(typeof(DatabaseAttribute), true)
                        .FirstOrDefault() as DatabaseAttribute
                    ) != null)
                .Where(y => (y.
                        GetCustomAttributes(typeof(DatabaseAttribute), true)
                        .FirstOrDefault() as DatabaseAttribute
                        ).dbName.ToLower() == columnName.ToLower())
                .Select(y =>
                    y.Name
                ).FirstOrDefault();
            if (propertyName != null)
                propertyName = propertyName.ToString();
            else
                propertyName = "";
            return propertyName;
        }

        public static string GetPropertyNameByColumnName(Type type, string columnName)
        {
            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
            
            string propertyName = type
                .GetProperties(flags)
                .Where(y => (y
                        .GetCustomAttributes(typeof(DatabaseAttribute), true)
                        .FirstOrDefault() as DatabaseAttribute
                    ) != null)
                .Where(y => (y.
                        GetCustomAttributes(typeof(DatabaseAttribute), true)
                        .FirstOrDefault() as DatabaseAttribute
                        ).dbName.ToLower() == columnName.ToLower())
                .Select(y =>
                    y.Name
                ).FirstOrDefault().ToString();
            return propertyName;
        }

        public static string GetDatabaseName<T>()
        {
            var dnAttribute = typeof(T).GetCustomAttributes(
                typeof(DatabaseAttribute), true
            ).FirstOrDefault() as DatabaseAttribute;
            if (dnAttribute != null)
            {
                return dnAttribute.dbName;
            }
            return null;
        }

        public static string GetDatabaseName(Type type)
        {
            var dnAttribute = type.GetCustomAttributes(
                typeof(DatabaseAttribute), true
            ).FirstOrDefault() as DatabaseAttribute;
            if (dnAttribute != null)
            {
                return dnAttribute.dbName;
            }
            return null;
        }


        public static string GetJoinNameByPropertyName(Type type, string propertyName)
        {
            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
            string columnName = type
                .GetProperties(flags)
                .Where(y => y.Name == propertyName)
                .Select(y =>
                    (y
                        .GetCustomAttributes(typeof(JoinAttribute), true)
                        .FirstOrDefault() as JoinAttribute
                    )
                    .GetValue()
                ).FirstOrDefault().ToString();
            return columnName;
        }

        public static List<T> ToDynamicObject<T>(this DataTable dt, bool maintainColumnName = true)
        {
            var dynamicDt = new List<T>();
            try
            {
                foreach (DataRow row in dt.Rows)
                {
                    T dyn = (T)Activator.CreateInstance(typeof(T));
                    dynamic dynObj = new ExpandoObject();
                    var obj = dyn.GetType();
                    foreach (DataColumn column in dt.Columns)
                    {
                        string propName = GetPropertyNameByColumnName<T>(column.ColumnName);
                        if (propName != "")
                        {
                            var prop = dyn.GetType().GetProperties().Where(x => x.Name == propName).FirstOrDefault();
                            if (prop != null)
                            {
                                if (prop.PropertyType.Name == typeof(String).Name)
                                {
                                    prop.SetValue(dyn, row[column] is System.DBNull ? string.Empty : row[column]);
                                }
                                else if (prop.PropertyType.Name == typeof(Decimal).Name)
                                {
                                    var val = row[column];
                                    prop.SetValue(dyn, row[column] is System.DBNull ? 0.00m : row[column]);
                                }
                                else if (prop.PropertyType.Name == typeof(Nullable<>).Name)
                                {
                                    if (prop.PropertyType.GenericTypeArguments.FirstOrDefault().Name == typeof(DateTime).Name)
                                    {
                                        prop.SetValue(dyn, row[column] is System.DBNull ? null : row[column]);
                                    }
                                    else if (prop.PropertyType.GenericTypeArguments.FirstOrDefault().Name == typeof(Int32).Name)
                                    {
                                        prop.SetValue(dyn, row[column] is System.DBNull ? 0 : row[column]);
                                    }
                                    else if (prop.PropertyType.GenericTypeArguments.FirstOrDefault().Name == typeof(Decimal).Name)
                                    {
                                        prop.SetValue(dyn, row[column] is System.DBNull ? 0.0M : row[column]);
                                    }
                                    else
                                    {
                                        prop.SetValue(dyn, row[column] is System.DBNull ? string.Empty : row[column]);
                                    }
                                }
                                else if (prop.PropertyType.Name == typeof(DateTime).Name)
                                {
                                    if (row[column] == System.DBNull.Value)
                                    {
                                        prop.SetValue(dyn, default(DateTime));
                                    }
                                    else
                                    {
                                        prop.SetValue(dyn, row[column]);
                                    }
                                }
                                else
                                {
                                    prop.SetValue(dyn, row[column]);
                                }
                            }
                        }
                        else
                        {
                            if (obj.Name == nameof(Object))
                            {
                                IDictionary<string, object> myUnderlyingObject = dynObj;
                                string propertyName = "";
                                if (!maintainColumnName)
                                {
                                    string[] columnNameParts = column.ColumnName.Split('_');
                                    foreach (string columnNamePart in columnNameParts)
                                    {
                                        propertyName += char.ToUpper(columnNamePart[0]) + columnNamePart.Substring(1);
                                    }
                                    propertyName = (propertyName == "" ? column.ColumnName : propertyName);
                                }
                                else
                                    propertyName = column.ColumnName;
                                myUnderlyingObject.Add(propertyName, row[column]);
                            }
                        }
                    }

                    if (obj.Name != nameof(Object))
                        dynamicDt.Add(dyn);
                    else
                        dynamicDt.Add(dynObj);
                    


                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);

            }
            return dynamicDt;
        }

        public static dynamic ToDynamicObject(this DataTable dt, Type type)
        {
            var listType = typeof(List<>);
            var constructedListType = listType.MakeGenericType(type);
            var dynamicDt = (IList)Activator.CreateInstance(constructedListType);
            //var dynamicDt = new List<object>();
            foreach (DataRow row in dt.Rows)
            {
                object dyn = Activator.CreateInstance(type);
                foreach (DataColumn column in dt.Columns)
                {
                    string propName = GetPropertyNameByColumnName(type, column.ColumnName);
                    var prop = dyn.GetType().GetProperties().Where(x => x.Name == propName).FirstOrDefault();
                    if (prop != null)
                    {
                        if (prop.PropertyType.Name == typeof(String).Name)
                        {
                            prop.SetValue(dyn, row[column] is System.DBNull ? string.Empty : row[column]);
                        }
                        else if (prop.PropertyType.Name == typeof(Nullable<>).Name)
                        {
                            if (prop.PropertyType.GenericTypeArguments.FirstOrDefault().Name == typeof(DateTime).Name)
                            {
                                prop.SetValue(dyn, row[column] is System.DBNull ? null : row[column]);
                            }
                            else if (prop.PropertyType.GenericTypeArguments.FirstOrDefault().Name == typeof(Int32).Name)
                            {
                                prop.SetValue(dyn, row[column] is System.DBNull ? 0 : row[column]);
                            }
                            else
                            {
                                prop.SetValue(dyn, row[column] is System.DBNull ? string.Empty : row[column]);
                            }
                        }
                        else
                        {
                            prop.SetValue(dyn, row[column]);
                        }
                    }
                }
                var newObj = Convert.ChangeType(dyn, type);
                dynamicDt.Add(newObj);
            }
            return dynamicDt;
        }

        public static object ToStaticClass(Type type, object expando)
        {
            var entity = new object();

            //ExpandoObject implements dictionary
            var properties = expando as IDictionary<string, object>;

            if (properties == null)
                return entity;

            foreach (var entry in properties)
            {
                var propertyInfo = entity.GetType().GetProperty(entry.Key);
                if (propertyInfo != null)
                    propertyInfo.SetValue(entity, entry.Value, null);
            }
            return entity;
        }

        public static object ToStaticListClass(Type type, object expando)
        {
            var entity = new List<object>();
            //ExpandoObject implements dictionary
            if (type.IsGenericType)
            {
                if (type.GetGenericTypeDefinition() == typeof(List<>))
                {
                    var listObjClass = type.GenericTypeArguments.FirstOrDefault();
                    var classFullName = Assembly.GetExecutingAssembly().CreateInstance(listObjClass.FullName);

                    var properties = listObjClass.GetProperties();

                    if (properties == null)
                        return entity;
                    ((List<object>)expando).ForEach(obj =>
                    {
                        foreach (var entry in properties)
                        {
                            var propertyInfo = obj.GetType().GetProperty(entry.Name);
                            if (propertyInfo != null)
                                propertyInfo.SetValue(obj, obj.GetType().GetProperties().Where(x => x.Name == entry.Name).FirstOrDefault().GetValue(obj), null);
                        }
                    });
                }
            }


            return Convert.ChangeType(expando, type);
        }

        public static object ChangeType(object value, Type type)
        {
            if (value == null && type.IsGenericType) return Activator.CreateInstance(type);
            if (value == null) return null;
            if (type == value.GetType()) return value;
            if (type.IsEnum)
            {
                if (value is string)
                    return Enum.Parse(type, value as string);
                else
                    return Enum.ToObject(type, value);
            }
            if (!type.IsInterface && type.IsGenericType)
            {
                Type innerType = type.GetGenericArguments()[0];
                object innerValue = ChangeType(value, innerType);
                return Activator.CreateInstance(type, new object[] { innerValue });
            }
            if (value is string && type == typeof(Guid)) return new Guid(value as string);
            if (value is string && type == typeof(Version)) return new Version(value as string);
            if (!(value is IConvertible)) return value;
            return Convert.ChangeType(value, type);
        }

        public static dynamic DynamicCast(object entity, Type to)
        {
            var openCast = entity.GetType().GetMethod("Cast", BindingFlags.Static | BindingFlags.NonPublic);
            var closeCast = openCast.MakeGenericMethod(to);
            return closeCast.Invoke(entity, new[] { entity });
        }

        public static string Capitalize(this string words)
        {
            string capitalized = "";

            string capitalizedspace = "";
            foreach (string word in words.Split(' '))
            {
                if(word != "" && word == "IT")
                    capitalizedspace += word.Substring(0, 2).ToUpper() + " ";

                else if (word != "")
                    capitalizedspace += word.Substring(0, 1).ToUpper() + word.Substring(1).ToLower() + " ";
            }
            string capitalizedcomma = "";
            foreach (string word in capitalizedspace.Split(','))
            {
                capitalizedcomma += word.Substring(0, 1).ToUpper() + word.Substring(1) + ",";
            }
            capitalizedcomma = capitalizedcomma.TrimEnd(',');
            string capitalizedhipen = "";
            foreach (string word in capitalizedcomma.Split('-'))
            {
                capitalizedhipen += word.Substring(0, 1).ToUpper() + word.Substring(1) + "-";
            }
            capitalizedhipen = capitalizedhipen.TrimEnd('-');

            capitalized = capitalizedhipen.Trim();
            return capitalized;
        }


    }
}
