﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("ao_crs_details")]
    public class AccountOpeningCRSDetail
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("account_opening_id"), Display("AccountOpeningId")]
        public Int32 AccountOpeningId { get; set; }
        [Database("tax_residency_status"), Display("TaxResidencyStatus")]
        public String TaxResidencyStatus { get; set; }
        [Database("tin"), Display("Tin")]
        public String Tin { get; set; }
        [Database("no_tin_reason"), Display("NoTinReason")]
        public String NoTinReason { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
    }
}
