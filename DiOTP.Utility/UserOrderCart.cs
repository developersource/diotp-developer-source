using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("user_order_carts")]
    public class UserOrderCart
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("ref_no"), Display("RefNo")]
        public String RefNo { get; set; }
        [Database("order_type"), Display("OrderType")]
        public Int32 OrderType { get; set; }
        [Database("fund_id"), Display("FundId")]
        public Int32 FundId { get; set; }
        [Database("to_fund_id"), Display("ToFundId")]
        public Int32 ToFundId { get; set; }
        [Database("to_account_id"), Display("ToAccountId")]
        public Int32 ToAccountId { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("user_account_id"), Display("UserAccountId")]
        public Int32 UserAccountId { get; set; }
        [Database("order_no"), Display("OrderNo")]
        public String OrderNo { get; set; }
        [Database("payment_method"), Display("PaymentMethod")]
        public String PaymentMethod { get; set; }
        [Database("amount"), Display("Amount")]
        public Decimal Amount { get; set; }
        [Database("units"), Display("Units")]
        public Decimal Units { get; set; }
        [Database("trans_id"), Display("TransId")]
        public Int32 TransId { get; set; }
        [Database("trans_no"), Display("TransNo")]
        public String TransNo { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32? CreatedBy { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("updated_by"), Display("UpdatedBy")]
        public Int32? UpdatedBy { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime? UpdatedDate { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        [Database("distribution_id"), Display("DistributionId")]
        public Int32 DistributionId { get; set; }
        [Database("consultant_id"),Display("ConsultantId")]
        public String ConsultantId { get; set; }
        [Database("distribution_bank_id"), Display("DistributionBankId")]
        public Int32 DistributionBankId { get; set; }
        
    }
}
