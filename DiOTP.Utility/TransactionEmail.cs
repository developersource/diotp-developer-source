﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class TransactionEmail
    {
        
        public Int32 Id { get; set; }
        public String RefNo { get; set; }
        public Int32 OrderType { get; set; }
        public Int32 FundId { get; set; }
        public Int32 ToFundId { get; set; }
        public Int32 ToAccountId { get; set; }
        public Int32 UserId { get; set; }
        public Int32 UserAccountId { get; set; }
        public String OrderNo { get; set; }
        public String PaymentMethod { get; set; }
        public Decimal Amount { get; set; }
        public Decimal Units { get; set; }
        public Int32 TransId { get; set; }
        public String TransNo { get; set; }
        public Int32? CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string ConsultantId { get; set; }
        public Int32 IsTaxInvoice { get; set; }
        public Int32 IsCas { get; set; }
        public Int32 IsCreditNote { get; set; }
        public Int32 Status { get; set; }
        public string RejectReason { get; set; }
        public Int32 OrderStatus { get; set; }
        public string FpxTransactionId { get; set; }
        public string FpxStatus { get; set; }
        public string BankCode { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime? SettlementDate { get; set; }
        public Int32 DistributionInstruction { get; set; }
        public decimal InitialSalesCharges { get; set; }
        public Int32 BankId { get; set; }
    }
}
