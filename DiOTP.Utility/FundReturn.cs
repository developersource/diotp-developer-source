using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("fund_returns")]
    public class FundReturn
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("fund_code"), Display("FundCode")]
        public String FundCode { get; set; }
        [Database("one_week"), Display("OneWeek")]
        public Decimal OneWeek { get; set; }
        [Database("one_month"), Display("OneMonth")]
        public Decimal OneMonth { get; set; }
        [Database("three_month"), Display("ThreeMonth")]
        public Decimal ThreeMonth { get; set; }
        [Database("six_month"), Display("SixMonth")]
        public Decimal SixMonth { get; set; }
        [Database("one_year"), Display("OneYear")]
        public Decimal OneYear { get; set; }
        [Database("two_year"), Display("TwoYear")]
        public Decimal TwoYear { get; set; }
        [Database("three_year"), Display("ThreeYear")]
        public Decimal ThreeYear { get; set; }
        [Database("five_year"), Display("FiveYear")]
        public Decimal FiveYear { get; set; }
        [Database("ten_year"), Display("TenYear")]
        public Decimal TenYear { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }
    }
}
