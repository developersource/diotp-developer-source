using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("utmc_fund_corporate_actions")]
    public class UtmcFundCorporateAction
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("epf_ipd_code"), Display("EpfIpdCode")]
        public String EpfIpdCode { get; set; }
        [Database("ipd_fund_code"), Display("IpdFundCode")]
        public String IpdFundCode { get; set; }
        [Database("corporate_action_date"), Display("CorporateActionDate")]
        public DateTime CorporateActionDate { get; set; }
        [Database("distributions"), Display("Distributions")]
        public Decimal Distributions { get; set; }
        [Database("unit_splits"), Display("UnitSplits")]
        public Decimal? UnitSplits { get; set; }
        [Database("report_date"), Display("ReportDate")]
        public DateTime ReportDate { get; set; }
        [Database("net_distribution"), Display("NetDistribution")]
        public Decimal NetDistribution { get; set; }
    }
}
