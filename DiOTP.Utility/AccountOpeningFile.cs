﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("ao_files")]
    public class AccountOpeningFile
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("account_opening_id"), Display("AccountOpeningId")]
        public Int32 AccountOpeningId { get; set; }
        [Database("file_type"), Display("FileType")]
        public Int32 FileType { get; set; }
        [Database("name"), Display("Name")]
        public String Name { get; set; }
        [Database("url"), Display("Url")]
        public String Url { get; set; }
        [Database("created_date"), Display("created_date")]
        public DateTime CreatedDate { get; set; }
        [Database("updated_date"), Display("updated_date")]
        public DateTime UpdatedDate { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
    }
}
