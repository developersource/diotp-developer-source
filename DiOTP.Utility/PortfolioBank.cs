﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class PortfolioBank
    {
        public int id { get; set; }
        public String account_no { get; set; }
        public String name { get; set; }
        public String account_name { get; set; }
        public DateTime created_date { get; set; }
        public DateTime updated_date { get; set; }
        public String bank_account_no { get; set; }
        public int ma_holder_bank_id { get; set; }
        public int created_by { get; set; }
        public int status { get; set; }
        public int user_account_id { get; set; }
        public string image { get; set; }
    }
}
