using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("user_files")]
    public class UserFile
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("user_file_type_id"), Display("UserFileTypeId")]
        public Int32 UserFileTypeId { get; set; }
        [Database("title"), Display("Title")]
        public String Title { get; set; }
        [Database("url"), Display("Url")]
        public String Url { get; set; }
        [Database("thumbnail_url"), Display("ThumbnailUrl")]
        public String ThumbnailUrl { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        public UserFileType UserFileTypeIdUserFileType { get; set; }
        public User UserIdUser { get; set; }
    }
}
