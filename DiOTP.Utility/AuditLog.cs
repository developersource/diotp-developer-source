﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class AuditLog
    {
        public Int32 ID { get; set; }
        public string table_name { get; set; }
        public string description { get; set; }
        public Int32 user_id { get; set; }
        public string username { get; set; }
        public string id_no { get; set; }
        public Int32 user_account_id { get; set; }
        public Int32 ref_id { get; set; }
        public string ref_value { get; set; }
        public string account_no { get; set; }
        public DateTime updated_date { get; set; }
    }
}
