﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.CustomClasses
{
    public static class CustomStatus
    {
        //public enum OrderStatus
        //{
        //    Buy = 1,
        //    Sell = 2,
        //    RSP = 6
        //}
        public enum OrderType
        {
            Buy = 1,
            Sell = 2,
            SwitchOut = 3,
            SwitchIn = 4,
            RSP = 6
        }
        public enum Order_Status
        {
            Order_Placed = 1,
            Order_Canceld = 18,
            Payment_Pending = 22,
            Payment_Success = 2,
            Payment_Approved = 2,
            Payment_Rejected = 29,
            Order_Verified = 2,
            Order_Approved = 3,
            Order_Rejected = 39,
        }
        public enum AOR_Status
        {
            Request_Pending = 1,
            Request_Rejected = 19,
            Request_Pending_for_Additional_Documents = 22,
            Request_Pending_Post_Additional_Docs = 2,
            Request_Rejected_Step2 = 29,
            Request_Approved = 3,
        }
        public enum UserBank_Status
        {
            Pending = 0,
            Approved = 1,
            OldApproved = 2,
            Rejected = 9
        }
        public enum DistributionIns
        {
            Payout = 1,//Cheque
            Bank = 2,
            Reinvest = 3
        }

        public enum AOProcessStatus
        {
            Incomplete = 0,
            Completed = 1,
            IDVerified = 2,
            IDVerificationFailed = 29,
            UnclearDocsRequested = 10,
            UnclearDocsUploaded = 11,
            DocRequiredByAdmin = 20,
            DocRequiredUploaded = 21,
            DocVerified = 3,
            DocVerificationFailed = 39,
            Approved = 4,
            Rejected = 49
        }

        //public enum user_orders_order_status
        //{
        //    [Description("Payment Pending")]
        //    PaymentPending = 22,
        //    [Description("Payment Success")]
        //    PaymentSuccess = 2,
        //    [Description("Payment Rejected")]
        //    PaymentRejected = 29,
        //    [Description("Order Approved")]
        //    OrderSuccess = 3,
        //    [Description("Order Rejected")]
        //    OrderRejected = 39,
        //}

    }

    public abstract class Enumeration : IComparable
    {
        public string Name { get; private set; }

        public int Id { get; private set; }
        public string IdS { get; private set; }

        protected Enumeration(int id, string name)
        {
            Id = id;
            Name = name;
        }

        protected Enumeration(string ids, string name)
        {
            IdS = ids;
            Name = name;
        }

        public override string ToString() => Name;

        public static IEnumerable<T> GetAll<T>() where T : Enumeration
        {
            var fields = typeof(T).GetFields(BindingFlags.Public |
                                             BindingFlags.Static |
                                             BindingFlags.DeclaredOnly);

            return fields.Select(f => f.GetValue(null)).Cast<T>();
        }

        public override bool Equals(object obj)
        {
            var otherValue = obj as Enumeration;

            if (otherValue == null)
                return false;

            var typeMatches = GetType().Equals(obj.GetType());
            var valueMatches = Id.Equals(otherValue.Id);

            return typeMatches && valueMatches;
        }

        public int CompareTo(object other) => Id.CompareTo(((Enumeration)other).Id);

        // Other utility methods ...

        private static T parse<T, K>(K value, string description, Func<T, bool> predicate) where T : Enumeration, new()
        {
            var matchingItem = GetAll<T>().FirstOrDefault(predicate);

            if (matchingItem == null)
            {
                var message = string.Format("'{0}' is not a valid {1} in {2}", value, description, typeof(T));
                throw new ApplicationException(message);
            }

            return matchingItem;
        }

        public static T FromValue<T>(int value) where T : Enumeration, new()
        {
            var matchingItem = parse<T, int>(value, "value", item => item.Id == value);
            return matchingItem;
        }

        public static string GetById(Type t, string Id)
        {
            var fields = t.GetFields(BindingFlags.Public |
                                             BindingFlags.Static |
                                             BindingFlags.DeclaredOnly);

            var fs = fields.Select(f => f.GetValue(null)).Cast<dynamic>();
            if (Int32.TryParse(Id, out int res))
            {
                var match = fs.FirstOrDefault(x => x.Id == res);
                return match.Name;
            }
            else
            {
                var match = fs.FirstOrDefault(x => x.IdS == Id);
                if (match != null)
                    return match.Name;
                else
                    return "";
            }
        }

    }

    public class user_orders_order_status : Enumeration
    {
        public static readonly user_orders_order_status PaymentPending = new user_orders_order_status(22, "Payment Pending");
        public static readonly user_orders_order_status PaymentSuccess = new user_orders_order_status(2, "Payment Success");
        public static readonly user_orders_order_status PaymentRejected = new user_orders_order_status(29, "Payment Rejected");
        public static readonly user_orders_order_status OrderApproved = new user_orders_order_status(3, "Order Approved");
        public static readonly user_orders_order_status OrderRejected = new user_orders_order_status(39, "Order Rejected");

        public user_orders_order_status(int id, string name)
            : base(id, name)
        {
        }

        public string GetDesc(int id)
        {
            return "";
        }
    }

    public class utmc_fund_information_utmc_fund_categories_def_id : Enumeration
    {
        public static readonly utmc_fund_information_utmc_fund_categories_def_id Nothing = new utmc_fund_information_utmc_fund_categories_def_id(0, "-");
        public static readonly utmc_fund_information_utmc_fund_categories_def_id Equity = new utmc_fund_information_utmc_fund_categories_def_id(1, "Equity ");
        public static readonly utmc_fund_information_utmc_fund_categories_def_id EquityI = new utmc_fund_information_utmc_fund_categories_def_id(2, "Equity (Islamic)");
        public static readonly utmc_fund_information_utmc_fund_categories_def_id MixedAsset = new utmc_fund_information_utmc_fund_categories_def_id(3, "Mixed Asset");
        public static readonly utmc_fund_information_utmc_fund_categories_def_id MixedAssetI = new utmc_fund_information_utmc_fund_categories_def_id(4, "Mixed Asset (Islamic)");
        public static readonly utmc_fund_information_utmc_fund_categories_def_id Balanced = new utmc_fund_information_utmc_fund_categories_def_id(5, "Balanced");
        public static readonly utmc_fund_information_utmc_fund_categories_def_id BalancedI = new utmc_fund_information_utmc_fund_categories_def_id(6, "Balanced (Islamic)");
        public static readonly utmc_fund_information_utmc_fund_categories_def_id MoneyMarket = new utmc_fund_information_utmc_fund_categories_def_id(7, "Money Market");
        public static readonly utmc_fund_information_utmc_fund_categories_def_id MoneyMarketI = new utmc_fund_information_utmc_fund_categories_def_id(8, "Money Market (Islamic)");

        public utmc_fund_information_utmc_fund_categories_def_id(int id, string name)
            : base(id, name)
        {
        }

        public string GetDesc(int id)
        {
            return "";
        }
    }

    public class utmc_fund_information_fund_cls : Enumeration
    {
        public static readonly utmc_fund_information_fund_cls Nothing = new utmc_fund_information_fund_cls("", "-");
        public static readonly utmc_fund_information_fund_cls Equity = new utmc_fund_information_fund_cls("EE", "Equity");
        public static readonly utmc_fund_information_fund_cls MixedAsset = new utmc_fund_information_fund_cls("MA", "Mixed Asset");
        public static readonly utmc_fund_information_fund_cls MoneyMarket = new utmc_fund_information_fund_cls("MM", "Money Market");

        public utmc_fund_information_fund_cls(string id, string name)
            : base(id, name)
        {
        }

        public string GetDesc(int id)
        {
            return "";
        }
    }

    public class utmc_fund_information_foreign_fund : Enumeration
    {
        public static readonly utmc_fund_information_foreign_fund Nothing = new utmc_fund_information_foreign_fund("", "-");
        public static readonly utmc_fund_information_foreign_fund Equity = new utmc_fund_information_foreign_fund("L", "Local");
        public static readonly utmc_fund_information_foreign_fund EquityI = new utmc_fund_information_foreign_fund("F", "Foreign");

        public utmc_fund_information_foreign_fund(string id, string name)
            : base(id, name)
        {
        }

        public string GetDesc(int id)
        {
            return "";
        }
    }

    public class user_types_user_type_id : Enumeration
    {
        public static readonly user_types_user_type_id Nothing = new user_types_user_type_id(0, "-");
        public static readonly user_types_user_type_id Equity = new user_types_user_type_id(1, "Trader");
        public static readonly user_types_user_type_id EquityI = new user_types_user_type_id(2, "Agent)");
        public static readonly user_types_user_type_id MixedAsset = new user_types_user_type_id(3, "Main Admin");
        public static readonly user_types_user_type_id MixedAssetI = new user_types_user_type_id(4, "Account Admin");
        public static readonly user_types_user_type_id Balanced = new user_types_user_type_id(5, "Content Admin");

        public user_types_user_type_id(int id, string name)
            : base(id, name)
        {
        }

        public string GetDesc(int id)
        {
            return "";
        }
    }

    public class ma_holder_reg_otp_act_st : Enumeration
    {
        public static readonly ma_holder_reg_otp_act_st Pending = new ma_holder_reg_otp_act_st(0, "Pending");
        public static readonly ma_holder_reg_otp_act_st Approved = new ma_holder_reg_otp_act_st(1, "Approved");
        public static readonly ma_holder_reg_otp_act_st Rejected = new ma_holder_reg_otp_act_st(9, "Rejected");
        public ma_holder_reg_otp_act_st(int id, string name)
            : base(id, name)
        {
        }

        public string GetDesc(int id)
        {
            return "";
        }
    }

    public class users_is_hard_copy : Enumeration
    {
        public static readonly users_is_hard_copy Pending = new users_is_hard_copy(0, "No");
        public static readonly users_is_hard_copy Approved = new users_is_hard_copy(1, "Yes");
        public users_is_hard_copy(int id, string name)
            : base(id, name)
        {
        }

        public string GetDesc(int id)
        {
            return "";
        }
    }

    public class users_status : Enumeration
    {
        public static readonly users_status Inactive = new users_status(0, "Inactive");
        public static readonly users_status Active = new users_status(1, "Active");
        public users_status(int id, string name)
            : base(id, name)
        {
        }

        public string GetDesc(int id)
        {
            return "";
        }
    }

    public class site_content_status : Enumeration
    {
        public static readonly site_content_status Inactive = new site_content_status(0, "Inactive");
        public static readonly site_content_status Active = new site_content_status(1, "Active");
        public site_content_status(int id, string name)
            : base(id, name)
        {
        }

        public string GetDesc(int id)
        {
            return "";
        }
    }

    public class account_openings_process_status : Enumeration
    {
        public static readonly account_openings_process_status Incomplete = new account_openings_process_status(0, "Incomplete");
        public static readonly account_openings_process_status Completed = new account_openings_process_status(1, "Completed");
        public static readonly account_openings_process_status IdentityV = new account_openings_process_status(2, "Identity Verified");
        public static readonly account_openings_process_status IdentityF = new account_openings_process_status(29, "Identity Failed");
        public static readonly account_openings_process_status Doc10 = new account_openings_process_status(10, "Unclear Documents Requested");
        public static readonly account_openings_process_status Doc11 = new account_openings_process_status(11, "Unclear Documents Uploaded");
        public static readonly account_openings_process_status Doc20 = new account_openings_process_status(20, "Additional Documents Requested");
        public static readonly account_openings_process_status Doc21 = new account_openings_process_status(21, "Additional Documents Uploaded");
        public static readonly account_openings_process_status DocV = new account_openings_process_status(3, "Documents Verified");
        public static readonly account_openings_process_status DocF = new account_openings_process_status(39, "Documents Rejected");
        public static readonly account_openings_process_status AA = new account_openings_process_status(4, "Account Approved");
        public static readonly account_openings_process_status AR = new account_openings_process_status(49, "Account Rejected");
        public account_openings_process_status(int id, string name)
            : base(id, name)
        {
        }

        public string GetDesc(int id)
        {
            return "";
        }
    }

    public class db_column : Enumeration
    {
        public static readonly db_column OTPACTST = new db_column("MA_HOLDER_REG_OTP_ACT_ST", "Registration Status");
        public static readonly db_column HARDCOPY = new db_column("USERS_IS_HARD_COPY", "Hardcopy");
        public static readonly db_column JHDOWNLOADINDICATOR = new db_column("MA_HOLDER_REG_JOINT_DOWNLOAD_IND", "Joint holder download indicator");
        public static readonly db_column JHNOOFDPNDNT = new db_column("MA_HOLDER_REG_JOINT_NO_OF_DPNDNT", "Number of dependant");
        public static readonly db_column JHSIGNATUREIND = new db_column("MA_HOLDER_REG_JOINT_SIGNATURE_IND", "Joint signature indicator");
        public static readonly db_column JHRELATIONCODE = new db_column("MA_HOLDER_REG_JOINT_RELATION_CODE", "Joint relation code");
        public static readonly db_column JHINCOMECODE = new db_column("MA_HOLDER_REG_JOINT_INCOME_CODE", "Joint income code");
        public static readonly db_column JHSEX = new db_column("MA_HOLDER_REG_JOINT_SEX", "Joint Sex");
        public static readonly db_column JHBIRTHDATE = new db_column("MA_HOLDER_REG_JOINT_BIRTH_DT", "Joint birth date");
        public static readonly db_column JHOCCCODE = new db_column("MA_HOLDER_REG_JOINT_OCC_CODE", "Joint occupation code");
        public static readonly db_column JHRACE = new db_column("MA_HOLDER_REG_JOINT_RACE", "Joint race");
        public static readonly db_column JHNATIONALITY = new db_column("MA_HOLDER_REG_JOINT_NATIONALITY", "Joint nationality	");
        public static readonly db_column JHCOUNTRYRESIDENT = new db_column("MA_HOLDER_REG_JOINT_COUNTRY_RES", "Joint country res");
        public static readonly db_column JHSTATE = new db_column("MA_HOLDER_REG_JOINT_STATE", "Joint state");
        public static readonly db_column JHREGION = new db_column("MA_HOLDER_REG_JOINT_REGION", "Joint region");
        public static readonly db_column JHPOSTCODE = new db_column("MA_HOLDER_REG_JOINT_POSTCODE", "Joint postcode");
        public static readonly db_column JHADDRESS4 = new db_column("MA_HOLDER_REG_JOINT_ADDR_4", "Joint address line 4");
        public static readonly db_column JHADDRESS3 = new db_column("MA_HOLDER_REG_JOINT_ADDR_3", "Joint address line 3");
        public static readonly db_column JHADDRESS2 = new db_column("MA_HOLDER_REG_JOINT_ADDR_2", "Joint address line 2");
        public static readonly db_column JHADDRESS1 = new db_column("MA_HOLDER_REG_JOINT_ADDR_1", "Joint address line 1");
        public static readonly db_column JHTELNO = new db_column("MA_HOLDER_REG_JOINT_TEL_NO", "Joint telephone number");
        public static readonly db_column CHGTIME = new db_column("MA_HOLDER_REG_CHG_TIME", "Change time");
        public static readonly db_column CHGDT = new db_column("MA_HOLDER_REG_CHG_DT", "Change date");
        public static readonly db_column ADDRESS1 = new db_column("MA_HOLDER_REG_ADDR_1", "Address line 1");
        public static readonly db_column ADDRESS2 = new db_column("MA_HOLDER_REG_ADDR_2", "Address line 2");
        public static readonly db_column ADDRESS3 = new db_column("MA_HOLDER_REG_ADDR_3", "Address line 3");
        public static readonly db_column ADDRESS4 = new db_column("MA_HOLDER_REG_ADDR_4", "Address line 4");

        public static readonly db_column USER_ACCOUNTS_SAT_UPDATED_DATE = new db_column("USER_ACCOUNTS_SAT_UPDATED_DATE", "SAT Update Date");
        public static readonly db_column USER_ACCOUNTS_SAT_SCORE = new db_column("USER_ACCOUNTS_SAT_SCORE", "SAT Score");

        public static readonly db_column PROCESS_STATUS = new db_column("ACCOUNT_OPENINGS_PROCESS_STATUS", "Process Status");

        public db_column(string dbColumnName, string name)
            : base(dbColumnName, name)
        {
        }

        public string GetDesc(int id)
        {
            return "";
        }
    }


    public abstract class FPXEnumeration : IComparable
    {
        public string BankId { get; private set; }
        public string BankName { get; private set; }
        public string DisplayName { get; private set; }

        protected FPXEnumeration(string bankId, string bankName, string displayName)
        {
            BankId = bankId;
            BankName = bankName;
            DisplayName = displayName;
        }

        public override string ToString() => DisplayName;

        public static IEnumerable<T> GetAll<T>() where T : FPXEnumeration
        {
            var fields = typeof(T).GetFields(BindingFlags.Public |
                                             BindingFlags.Static |
                                             BindingFlags.DeclaredOnly);

            return fields.Select(f => f.GetValue(null)).Cast<T>();
        }

        public override bool Equals(object obj)
        {
            var otherValue = obj as FPXEnumeration;

            if (otherValue == null)
                return false;

            var typeMatches = GetType().Equals(obj.GetType());
            var valueMatches = BankId.Equals(otherValue.BankId);

            return typeMatches && valueMatches;
        }

        public int CompareTo(object other) => BankId.CompareTo(((FPXEnumeration)other).BankId);

        // Other utility methods ...

        private static T parse<T, K>(K value, string description, Func<T, bool> predicate) where T : FPXEnumeration, new()
        {
            var matchingItem = GetAll<T>().FirstOrDefault(predicate);

            if (matchingItem == null)
            {
                var message = string.Format("'{0}' is not a valid {1} in {2}", value, description, typeof(T));
                throw new ApplicationException(message);
            }

            return matchingItem;
        }

        public static T FromValue<T>(string value) where T : FPXEnumeration, new()
        {
            var matchingItem = parse<T, string>(value, "value", item => item.BankId == value);
            return matchingItem;
        }

        public static FPXBank GetByBankId(Type t, string BankId)
        {
            var fields = t.GetFields(BindingFlags.Public |
                                             BindingFlags.Static |
                                             BindingFlags.DeclaredOnly);

            var fs = fields.Select(f => f.GetValue(null)).Cast<dynamic>();
            var match = fs.FirstOrDefault(x => x.BankId == BankId);
            if (match != null)
                return match;
            else
                return null;
        }

    }
    public class FPXBank : FPXEnumeration
    {
        //Staging Bank ID - B2C

        public static readonly FPXBank ABB0234 = new FPXBank("ABB0234", "Affin Bank Berhad B2C - Test ID", "Affin B2C - Test ID");
        public static readonly FPXBank ABB0233 = new FPXBank("ABB0233", "Affin Bank Berhad", "Affin Bank");
        public static readonly FPXBank AGRO01 = new FPXBank("AGRO01", "BANK PERTANIAN MALAYSIA BERHAD (AGROBANK)", "AGRONet");
        public static readonly FPXBank ABMB0212 = new FPXBank("ABMB0212", "Alliance Bank Malaysia Berhad", "Alliance Bank (Personal)");
        public static readonly FPXBank AMBB0209 = new FPXBank("AMBB0209", "AmBank Malaysia Berhad", "AmBank");
        public static readonly FPXBank BIMB0340 = new FPXBank("BIMB0340", "Bank Islam Malaysia Berhad", "Bank Islam");
        public static readonly FPXBank BMMB0341 = new FPXBank("BMMB0341", "Bank Muamalat Malaysia Berhad", "Bank Muamalat");
        public static readonly FPXBank BKRM0602 = new FPXBank("BKRM0602", "Bank Kerjasama Rakyat Malaysia Berhad", "Bank Rakyat");
        public static readonly FPXBank BSN0601 = new FPXBank("BSN0601", "Bank Simpanan Nasional Merchant", "BSN");
        public static readonly FPXBank BCBB0235 = new FPXBank("BCBB0235", "CIMB Bank Berhad", "CIMB Clicks");
        public static readonly FPXBank CIT0219 = new FPXBank("CIT0219", "CITI Bank Berhad", "Citibank");
        public static readonly FPXBank HLB0224 = new FPXBank("HLB0224", "Hong Leong Bank Berhad", "Hong Leong Bank");
        public static readonly FPXBank HSBC0223 = new FPXBank("HSBC0223", "HSBC Bank Malaysia Berhad", "HSBC Bank");
        public static readonly FPXBank KFH0346_FPX = new FPXBank("KFH0346 FPX", "Kuwait Finance House (Malaysia) Berhad", "KFH");
        public static readonly FPXBank MB2U0227 = new FPXBank("MB2U0227", "Malayan Banking Berhad (M2U)", "Maybank2U");
        public static readonly FPXBank OCBC0229 = new FPXBank("OCBC0229", "OCBC Bank Malaysia Berhad", "OCBC Bank");
        public static readonly FPXBank PBB0233 = new FPXBank("PBB0233", "Public Bank Berhad", "Public Bank");
        public static readonly FPXBank RHB0218 = new FPXBank("RHB0218", "RHB Bank Berhad", "RHB Bank");
        public static readonly FPXBank TEST0021 = new FPXBank("TEST0021", "SBI Bank A", "SBI Bank A");
        public static readonly FPXBank TEST0022 = new FPXBank("TEST0022", "SBI Bank B", "SBI Bank B");
        public static readonly FPXBank TEST0023_Reference = new FPXBank("TEST0023 Reference", "SBI Bank C", "SBI Bank C");
        public static readonly FPXBank SCB0216 = new FPXBank("SCB0216", "Standard Chartered Bank", "Standard Chartered");
        public static readonly FPXBank UOB0226 = new FPXBank("UOB0226", "United Overseas Bank", "UOB Bank");
        public static readonly FPXBank UOB0229 = new FPXBank("UOB0229", "United Overseas Bank - B2C Test", "UOB Bank - Test ID");

        ////Staging Bank ID - B2B

        public static readonly FPXBank ABB0232 = new FPXBank("ABB0232", "Affin Bank Berhad", "Affin Bank");
        public static readonly FPXBank ABB0235 = new FPXBank("ABB0235", "Affin Bank Berhad B2B", "AFFINMAX");
        public static readonly FPXBank ABMB0213 = new FPXBank("ABMB0213", "Alliance Bank Malaysia Berhad", "Alliance Bank (Business)");
        public static readonly FPXBank AMBB0208 = new FPXBank("AMBB0208", "AmBank Malaysia Berhad", "AmBank");
        //public static readonly FPXBank BIMB0340 = new FPXBank("BIMB0340", "Bank Islam Malaysia Berhad", "Bank Islam ");
        public static readonly FPXBank BMMB0342 = new FPXBank("BMMB0342", "Bank Muamalat Malaysia Berhad", "Bank Muamalat");
        public static readonly FPXBank BNP003 = new FPXBank("BNP003", "BNP Paribas Malaysia Berhad", "BNP Paribas");
        //public static readonly FPXBank BCBB0235 = new FPXBank("BCBB0235", "CIMB Bank Berhad", "CIMB Bank  ");
        public static readonly FPXBank CIT0218 = new FPXBank("CIT0218", "CITI Bank Berhad", "Citibank Corporate Banking");
        public static readonly FPXBank DBB0199 = new FPXBank("DBB0199", "Deutsche Bank Berhad", "Deutsche Bank");
        //public static readonly FPXBank HLB0224 = new FPXBank("HLB0224", "Hong Leong Bank Berhad", "Hong Leong Bank ");
        //public static readonly FPXBank HSBC0223 = new FPXBank("HSBC0223", "HSBC Bank Malaysia Berhad", "HSBC Bank ");
        //public static readonly FPXBank BKRM0602 = new FPXBank("BKRM0602", "Bank Kerjasama Rakyat Malaysia Berhad", "i-bizRAKYAT ");
        public static readonly FPXBank KFH0346 = new FPXBank("KFH0346", "Kuwait Finance House (Malaysia) Berhad", "KFH");
        public static readonly FPXBank MBB0228 = new FPXBank("MBB0228", "Malayan Banking Berhad (M2E)", "Maybank2E");
        //public static readonly FPXBank OCBC0229 = new FPXBank("OCBC0229", "OCBC Bank Malaysia Berhad", "OCBC Bank ");
        //public static readonly FPXBank PBB0233 = new FPXBank("PBB0233", "Public Bank Berhad", "Public Bank ");
        public static readonly FPXBank PBB0234 = new FPXBank("PBB0234", "Public Bank Enterprise", "PB Enterprise Only");
        //public static readonly FPXBank RHB0218 = new FPXBank("RHB0218", "RHB Bank Berhad Merchant", "RHB Bank ");
        //public static readonly FPXBank TEST0021 = new FPXBank("TEST0021", "SBI Bank A", "SBI Bank A ");
        //public static readonly FPXBank TEST0022 = new FPXBank("TEST0022", "SBI Bank B", "SBI Bank B ");
        public static readonly FPXBank TEST0023 = new FPXBank("TEST0023", "SBI Bank C", "SBI Bank C");
        public static readonly FPXBank SCB0215 = new FPXBank("SCB0215", "Standard Chartered Bank", "Standard Chartered");
        public static readonly FPXBank UOB0227 = new FPXBank("UOB0227", "United Overseas Bank", "UOB Bank ");
        public static readonly FPXBank UOB0228_FPX = new FPXBank("UOB0228 FPX", "United Overseas Bank B2B Regional", "UOB Regional");

        //Production Bank ID - B2C //B2C = business to customer

        //public static readonly FPXBank ABB0233 = new FPXBank("ABB0233", "Affin Bank Berhad", "Affin Bank ");
        //public static readonly FPXBank ABMB0212 = new FPXBank("ABMB0212", "Alliance Bank Malaysia Berhad", "Alliance Bank (Personal) ");
        //public static readonly FPXBank AMBB0209 = new FPXBank("AMBB0209", "AmBank Malaysia Berhad", "AmBank ");
        //public static readonly FPXBank BIMB0340 = new FPXBank("BIMB0340", "Bank Islam Malaysia Berhad", "Bank Islam ");
        //public static readonly FPXBank BMMB0341 = new FPXBank("BMMB0341", "Bank Muamalat Malaysia Berhad", "Bank Muamalat ");
        //public static readonly FPXBank BKRM0602 = new FPXBank("BKRM0602", "Bank Kerjasama Rakyat Malaysia Berhad", "Bank Rakyat ");
        //public static readonly FPXBank BSN0601 = new FPXBank("BSN0601", "Bank Simpanan Nasional", "BSN ");
        //public static readonly FPXBank BCBB0235 = new FPXBank("BCBB0235", "CIMB Bank Berhad", "CIMB Clicks ");
        //public static readonly FPXBank CIT0219 = new FPXBank("CIT0219", "CITI Bank Bhd - Coming Soon", "Citibank - Coming Soon ");
        //public static readonly FPXBank HLB0224 = new FPXBank("HLB0224", "Hong Leong Bank Berhad", "Hong Leong Bank ");
        //public static readonly FPXBank HSBC0223 = new FPXBank("HSBC0223", "HSBC Bank Malaysia Berhad", "HSBC Bank ");
        //public static readonly FPXBank KFH0346 = new FPXBank("KFH0346", "Kuwait Finance House (Malaysia) Berhad", "KFH  ");
        //public static readonly FPXBank MBB0228 = new FPXBank("MBB0228", "Malayan Banking Berhad (M2E)", "Maybank2E ");
        //public static readonly FPXBank MB2U0227 = new FPXBank("MB2U0227", "Malayan Banking Berhad (M2U)", "Maybank2U ");
        //public static readonly FPXBank OCBC0229 = new FPXBank("OCBC0229", "OCBC Bank Malaysia Berhad", "OCBC Bank Only");
        //public static readonly FPXBank PBB0233 = new FPXBank("PBB0233", "Public Bank Berhad Merchant", "Public Bank ");
        //public static readonly FPXBank RHB0218 = new FPXBank("RHB0218", "RHB Bank Berhad", "RHB Bank ");
        //public static readonly FPXBank SCB0216 = new FPXBank("SCB0216", "Standard Chartered Bank", "Standard Chartered ");
        //public static readonly FPXBank UOB0226 = new FPXBank("UOB0226", "United Overseas Bank", "UOB Bank ");

        //Production Bank ID - B2C //B2B = business to business

        //public static readonly FPXBank ABB0232 = new FPXBank("ABB0232", "Affin Bank Berhad", "Affin Bank");
        //public static readonly FPXBank ABB0235 = new FPXBank("ABB0235", "Affin Bank Berhad", "AFFINMAX");
        //public static readonly FPXBank ABMB0213 = new FPXBank("ABMB0213", "Alliance Bank Malaysia Berhad", "Alliance Bank (Business)");
        //public static readonly FPXBank AMBB0208 = new FPXBank("AMBB0208", "AmBank Malaysia Berhad", "AmBank");
        //public static readonly FPXBank BIMB0340 = new FPXBank("BIMB0340", "Bank Islam Malaysia Berhad", "Bank Islam");
        //public static readonly FPXBank BMMB0342 = new FPXBank("BMMB0342", "Bank Muamalat Malaysia Berhad", "Bank Muamalat");
        //public static readonly FPXBank BNP003 = new FPXBank("BNP003", "BNP Paribas Malaysia Berhad", "BNP Paribas");
        //public static readonly FPXBank BCBB0235 = new FPXBank("BCBB0235", "CIMB Bank Berhad", "CIMB Bank");
        //public static readonly FPXBank CIT0218 = new FPXBank("CIT0218", "CITI Bank Bhd", "Citibank Corporate Banking");
        //public static readonly FPXBank DBB0199 = new FPXBank("DBB0199", "Deutsche Bank Berhad", "Deutsche Bank");
        //public static readonly FPXBank HLB0224 = new FPXBank("HLB0224", "Hong Leong Bank Berhad", "Hong Leong Bank");
        //public static readonly FPXBank HSBC0223 = new FPXBank("HSBC0223", "HSBC Bank Malaysia Berhad", "HSBC Bank");
        //public static readonly FPXBank BKRM0602 = new FPXBank("BKRM0602", "Bank Kerjasama Rakyat Malaysia Berhad", "i-bizRAKYAT");
        //public static readonly FPXBank KFH0346 = new FPXBank("KFH0346", "Kuwait Finance House (Malaysia) Berhad", "KFH");
        //public static readonly FPXBank MBB0228 = new FPXBank("MBB0228", "Malayan Banking Berhad (M2E)", "Maybank2E");
        //public static readonly FPXBank OCBC0229 = new FPXBank("OCBC0229", "OCBC Bank Malaysia Berhad", "OCBC Bank Only");
        //public static readonly FPXBank PBB0233 = new FPXBank("PBB0233", "Public Bank Berhad", "Public Bank");
        //public static readonly FPXBank PBB0234 = new FPXBank("PBB0234", "Public Bank Enterprise Merchant", "PB Enterprise");
        //public static readonly FPXBank RHB0218 = new FPXBank("RHB0218", "RHB Bank Berhad", "RHB Bank");
        //public static readonly FPXBank SCB0215 = new FPXBank("SCB0215", "Standard Chartered Bank", "Standard Chartered");
        //public static readonly FPXBank UOB0227 = new FPXBank("UOB0227", "United Overseas Bank", "UOB Bank");
        public static readonly FPXBank UOB0228 = new FPXBank("UOB0228", "United Overseas Bank B2B Regional", "UOB Regional");
        public FPXBank(string BankId, string BankName, string DisplayName)
            : base(BankId, BankName, DisplayName)
        {
        }

        public string GetDesc(int id)
        {
            return "";
        }
    }

}
