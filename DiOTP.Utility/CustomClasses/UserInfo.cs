﻿using DiOTP.Utility.OracleDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.CustomClasses
{
    public class UserInfo : User
    {
        public string holderNo { get; set; }
        public List<UtmcDetailedMemberInvestment> utmcDetailedMemberInvestments { get; set; }
    }

    public class UtmcDetailedMemberInvestment
    {
        public UtmcFundInformation utmcFundInformation { get; set; }
        public List<UtmcMemberInvestment> utmcMemberInvestments { get; set; }
        public HolderInv holderInv { get; set; }
        public List<UtmcCompositionalTransaction> utmcCompositionalTransactions { get; set; }
        public List<HolderLedger> holderLedgers { get; set; }
    }
}
