﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.CustomClasses
{
    public class Cart
    {
        public Int32 Id { get; set; }
        public UserAccount userAccount { get; set; }
        public UserAccount userAccount2 { get; set; }
        public UtmcFundInformation UtmcFundInformation { get; set; }
        public UtmcFundInformation UtmcFundInformation2 { get; set; }
        public string PaymentMethod { get; set; }
        public Decimal Amount { get; set; }
        public Decimal Units { get; set; }
        public Int32 Type { get; set; }
        public string refNo { get; set; }
        public string ConsultantId { get; set; }
        public Int32 DistributionId { get; set; }
        public Int32 DistributionBankId { get; set; }
        public Int32 isExisting { get; set; }
    }
}
