﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.CustomClasses
{
    public class Response
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public bool IsApiAvailable { get; set; }
        public bool IsDBAvailable { get; set; }
        public List<CustomValidation> CustomValidations { get; set; }
    }

    public class CustomValidation
    {
        public string Name { get; set; }
        public string Message { get; set; }
    }
}
