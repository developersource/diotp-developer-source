﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.CustomClasses
{
    class GenerateStatement
    {

    }

    public class SummaryPortfolio
    {
        public string Ipd_Fund_Code { get; set; }
        public string Fund_Name { get; set; }
        public string Fund_Code { get; set; }
        public decimal Net_Cumulative_Closing_Balance_Units { get; set; }
        public decimal Market_Price_NAV { get; set; }
        public decimal Current_Market_Value { get; set; }
    }

    public class TransactionByDate
    {
        public DateTime Date_Of_Transaction { get; set; }
        public DateTime Date_Of_Settlement { get; set; }
        public string Fund_Code { get; set; }
        public string Transaction_Code { get; set; }
        public string Transaction_Name { get; set; }
        public string IPD_Unique_Transaction_ID { get; set; }
        public decimal Units { get; set; }
        public decimal Unit_Cost_RM { get; set; }
        public decimal Gross_Amount_RM { get; set; }
        public string Fund_Name { get; set; }
        public DateTime Report_Date { get; set; }
        public decimal Sum_Units { get; set; }
        public String Agent_Code { get; set; }
    }
}
