﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.CustomClasses
{
    class WindowServiceDTOs
    {
    }

    [Database("ws_processes")]
    public class WS_Process
    {
        [Database("id"), Display("Id")]
        public Int32 ID { get; set; }
        [Database("process_type"), Display("process_type")]
        public Int32 process_type { get; set; }
        [Database("is_processed"), Display("is_processed")]
        public Int32 is_processed { get; set; }
        [Database("processed_date"), Display("processed_date")]
        public DateTime processed_date { get; set; }
        [Database("message"), Display("message")]
        public String message { get; set; }
        [Database("status"), Display("status")]
        public Int32 status { get; set; }
    }
}
