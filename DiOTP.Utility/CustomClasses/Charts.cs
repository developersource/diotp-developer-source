﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.CustomClasses
{
    public class Charts
    {

    }
    public class InvestmentPerformance
    {
        public DateTime ReportDate { get; set; }
        public Decimal InvestUnits { get; set; }
        public Decimal InvestCost { get; set; }
        public Decimal InvestMarketValue { get; set; }
        public Decimal DividendPayout { get; set; }
        public Decimal GainLoss { get; set; }
        public Decimal CumGainLoss { get; set; }
    }
    public class PortfolioMarketValueMovement
    {
        public DateTime ReportDate { get; set; }
        public Decimal InvestUnits { get; set; }
        public Decimal InvestMarketValue { get; set; }
        public Decimal RealizedProfit { get; set; }
        public Decimal DividendPayout { get; set; }
        public Decimal GainLoss { get; set; }
    }
    public class PortfolioAllocation
    {
        public string AccountPlan { get; set; }
        public Decimal Investment { get; set; }
        public Decimal Percent { get; set; }
    }

    public class FundAllocation
    {
        public string FundName { get; set; }
        public string FundCode { get; set; }
        public Decimal Investment { get; set; }
        public Decimal Percent { get; set; }
    }

    public class FCAllocation
    {
        public string FundCategoryName { get; set; }
        public Decimal Investment { get; set; }
        public Decimal Percent { get; set; }
    }

    public class AccountAllocation
    {
        public string AccountNo { get; set; }
        public string AccountPlan { get; set; }
        public Decimal Investment { get; set; }
        public Decimal Percent { get; set; }
    }
}
