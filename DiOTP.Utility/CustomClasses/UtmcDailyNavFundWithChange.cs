﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.CustomClasses
{
    public class UtmcDailyNavFundWithChange
    {
        public UtmcDailyNavFund UtmcDailyNavFund { get; set; }

        public Decimal Change { get; set; }
        public Decimal ChangePer { get; set; }

        public UtmcDailyNavFundWithChange()
        {
            Change = 0;
            ChangePer = 0;
        }
    }
}
