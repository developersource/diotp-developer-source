﻿using DiOTP.Utility.OracleDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.CustomClasses
{
    public static class GlobalProperties
    {
        //public static List<FundDetailedInformation> FundDetailedInformations { get; set; }
        public static DateTime FundChartUpdatedDate { get; set; }
        public static bool isExecutedToday { get; set; }
        public static DateTime ExecutedDate { get; set; }
        public static List<CurrentNAVDTO> CurrentNAVDTOs { get; set; }
    }
}
