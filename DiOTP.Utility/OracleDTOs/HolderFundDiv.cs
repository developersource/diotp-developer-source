﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.OracleDTOs
{
    [Database("uts.holder_fund_div")]
    public class HolderFundDiv
    {
        [Database("fund_id"), Display("FundId")]
        public string FundId { get; set; }

        [Database("holder_no"), Display("HolderNo")]
        public Int32 HolderNo { get; set; }

        [Database("REG_CHK_DIGIT"), Display("RegChkDigit")]
        public Int32? RegChkDigit { get; set; }

        [Database("DIV_PYMT"), Display("DivPymt")]
        public string DivPymt { get; set; }
        [Database("ACCOUNT_NAME"), Display("AccountName")]
        public string AccountName { get; set; }
        [Database("ACCOUNT_NO"), Display("AccountNo")]
        public string AccountNo { get; set; }
        [Database("BANK_ID"), Display("BankId")]
        public string BankId { get; set; }
        [Database("DOWNLOAD_IND"), Display("DownloadInd")]
        public string DownloadInd { get; set; }
    }
}
