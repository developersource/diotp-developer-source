﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.OracleDTOs
{
    public class FundInfoDTO
    {
        public string FUND_ID { get; set; }
        public string FUND_TYPE { get; set; }
        public string L_NAME { get; set; }
        public string EPF_APPROVE { get; set; }
        public decimal UNIT_CREATN_MIN { get; set; }
        public decimal UNIT_CREATN_MAX { get; set; }
        public decimal REINV_AMT_MIN { get; set; }
        public decimal REINV_AMT_MAX { get; set; }
        public decimal UNIT_HLDG_MAX { get; set; }
        public decimal UNIT_HLDG_MIN { get; set; }
        public DateTime COMMENCE_DT { get; set; }
        public string CURRENCY { get; set; }
        public decimal UP_SELL { get; set; }
        public decimal SUBSEQ_SALE_MIN { get; set; }
        public decimal INITIAL_SALE_MIN { get; set; }
        public Int32 COOLING_PRD { get; set; }
        public Int32 NO_OF_HOLDER { get; set; }
        public decimal CASH_AVAIL { get; set; }
    }
}
