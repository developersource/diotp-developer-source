﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.OracleDTOs
{
    [Database("uts.holder_inv")]
    public class HolderInv
    {
        [Database("holder_no"), Display("HolderNo")]
        public int HolderNo { get; set; }
        [Database("fund_id"), Display("FundId")]
        public string FundId { get; set; }
        [Database("id_no"), Display("IdNo")]
        public string IdNo { get; set; }
        [Database("id_no_old"), Display("IdNoOld")]
        public string IdNoOld { get; set; }
        [Database("sale_val_cum"), Display("SaleValCum")]
        public Decimal SaleValCum { get; set; }
        [Database("sale_unit_cum"), Display("SaleUnitCum")]
        public Decimal SaleUnitCum { get; set; }
        [Database("rdm_val_cum"), Display("RdmValCum")]
        public Decimal RdmValCum { get; set; }
        [Database("rdm_unit_cum"), Display("RdmUnitCum")]
        public Decimal RdmUnitCum { get; set; }
        [Database("bonus_val_cum"), Display("BonusValCum")]
        public Decimal BonusValCum { get; set; }
        [Database("bonus_unit_cum"), Display("BonusUnitCum")]
        public Decimal BonusUnitCum { get; set; }
        [Database("col_val_cum"), Display("ColValCum")]
        public Decimal ColValCum { get; set; }
        [Database("col_unit_cum"), Display("ColUnitCum")]
        public Decimal ColUnitCum { get; set; }
        [Database("tax_witheld"), Display("TaxWitheld")]
        public Decimal TaxWitheld { get; set; }
        [Database("div_acc"), Display("DivAcc")]
        public Decimal DivAcc { get; set; }
        [Database("curr_unit_hldg"), Display("CurrUnitHldg")]
        public Decimal CurrUnitHldg { get; set; }
        [Database("curr_unit_hldg_xd"), Display("CurrUnitHldgXd")]
        public Decimal CurrUnitHldgXd { get; set; }
        [Database("no_active_cert"), Display("NoActiveCert")]
        public int NoActiveCert { get; set; }
        [Database("no_cancel_cert"), Display("NoCancelCert")]
        public int NoCancelCert { get; set; }
        [Database("last_sale_dt"), Display("LastSaleDt")]
        public DateTime LastSaleDt { get; set; }
        [Database("last_rdm_dt"), Display("LastRdmDt")]
        public DateTime LastRdmDt { get; set; }
        [Database("stmt_unit_cum"), Display("StmtUnitCum")]
        public Decimal StmtUnitCum { get; set; }
        [Database("trf_unit_cum"), Display("TrfUnitCum")]
        public Decimal TrfUnitCum { get; set; }
        [Database("tot_sp"), Display("TotSp")]
        public Decimal TotSp { get; set; }
        [Database("bi_val_cum"), Display("BiValCum")]
        public Decimal BiValCum { get; set; }
        [Database("bi_unit_cum"), Display("BiUnitCum")]
        public Decimal BiUnitCum { get; set; }
        [Database("hldr_ave_cost"), Display("HldrAveCost")]
        public Decimal HldrAveCost { get; set; }
        [Database("hldr_ave_price"), Display("HldrAvePrice")]
        public Decimal HldrAvePrice { get; set; }
        [Database("hldr_ave_fee"), Display("HldrAveFee")]
        public Decimal HldrAveFee { get; set; }
        [Database("last_ave_cost"), Display("LastAveCost")]
        public Decimal LastAveCost { get; set; }
        [Database("last_ave_price"), Display("LastAvePrice")]
        public Decimal LastAvePrice { get; set; }
        [Database("last_ave_fee"), Display("LastAveFee")]
        public Decimal LastAveFee { get; set; }
        [Database("cum_value_hldg"), Display("CumValueHldg")]
        public Decimal CumValueHldg { get; set; }
        [Database("cum_fee"), Display("CumFee")]
        public Decimal CumFee { get; set; }
        [Database("download_ind"), Display("DownloadInd")]
        public string DownloadInd { get; set; }
        public int isDisFromOracle { get; set; }
        public string DistributionInsString { get; set; }
        public int DistributionIns { get; set; }

    }
}
