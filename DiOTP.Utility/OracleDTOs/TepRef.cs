﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.OracleDTOs
{
    [Database("uts.tep_ref")]
    public class TepRef
    {
        [Database("ref_code"), Display("RefCode")]
        public String RefCode { get; set; }
        [Database("g_code"), Display("GCode")]
        public String GCode { get; set; }
        [Database("g_code_desc"), Display("GCodeDesc")]
        public String GCodeDesc { get; set; }
        [Database("download_ind"), Display("DownloadInd")]
        public String DownloadInd { get; set; }
    }
}
