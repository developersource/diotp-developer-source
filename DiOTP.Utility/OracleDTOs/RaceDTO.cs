﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.OracleDTOs
{
    public class RaceDTO
    {
        public String RACECODE { get; set; }
        public String SDESC { get; set; }
        public String DOWNLOADIND { get; set; }
    }
}
