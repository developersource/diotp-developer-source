﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.OracleDTOs
{
    public class CurrencyDTO
    {
        public String CURRENCY { get; set; }
        public String SDESC { get; set; }
        public Decimal CONVERSION_RT { get; set; }
    }
}
