﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.OracleDTOs
{
    [Database("UTS.NEW_DIV_DISTN")]
    public class FundIncomeDistDTO
    {
        [Database("FUND_ID"), Display("FUNDID")]
        public string FUNDID { get; set; }
        [Database("DIV_TAX"), Display("DIVTAX")]
        public Decimal DIVTAX { get; set; }
        [Database("DIV_TAX_EXEM"), Display("DIVTAXEXEM")]
        public Decimal DIVTAXEXEM { get; set; }
        [Database("DIV_EQUALSN_RT"), Display("DIVEQUALSNRT")]
        public Decimal DIVEQUALSNRT { get; set; }
        [Database("DISTN_DT"), Display("DISTNDT")]
        public DateTime DISTNDT { get; set; }
        [Database("NET_DIV_RT"), Display("NETDIVRT")]
        public Decimal NETDIVRT { get; set; }
        [Database("REINV_DT"), Display("REINVDT")]
        public DateTime REINVDT { get; set; }
    }

}
