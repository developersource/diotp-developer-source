﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility.CustomAttributes
{
    public class ValidationAttribute : Attribute
    {

    }

    public class RequiredAttribute : Attribute
    {
        public RequiredAttribute()
        {
        }
    }

    public class LengthOrValueAttribute : Attribute
    {
        public int Min { get; set; }
        public int Max { get; set; }
        public LengthOrValueAttribute(int Min, int Max)
        {
            this.Min = Min;
            this.Max = Max;
        }
    }

    public class EmailAttribute : Attribute
    {
        public EmailAttribute()
        {
        }
    }
}
