using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("notification_categories_def")]
    public class NotificationCategoriesDef
    {
        public List<NotificationTypesDef> NotificationCategoryDefIdNotificationTypesDefs { get; set; }
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("name"), Display("Name")]
        public String Name { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        [Database("field_id"), Display("FieldId")]
        public String FieldId { get; set; }
    }
}
