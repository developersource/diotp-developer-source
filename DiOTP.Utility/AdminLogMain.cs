﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("admin_log_main")]
    public class AdminLogMain
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("status_type"), Display("StatusType")]
        public Int32 StatusType { get; set; }
        [Database("table_name"), Display("TableName")]
        public string TableName { get; set; }
        [Database("ref_id"), Display("RefId")]
        public Int32 RefId { get; set; }
        [Database("ref_value"), Display("RefValue")]
        public string RefValue { get; set; }
        [Database("description"), Display("Description")]
        public string Description { get; set; }
        [Database("user_id"), Display("UserId")]
        public Int32 UserId { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }
    }
}
