﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class IndividualAccounts
    {
        public String id { get; set; }
        public String username { get; set; }
        public String mobile_number { get; set; }
        public String id_no { get; set; }
        public String account_no { get; set; }
        public String email_id { get; set; }
        public DateTime last_login_date { get; set; }
        public DateTime created_date { get; set; }
        public int status { get; set; }
        public int is_login_locked { get; set; }
    }
}
