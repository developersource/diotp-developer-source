﻿using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("ao_occupations")]
    public class AccountOpeningOccupation
    {
        [Database("ID"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("account_opening_id"), Display("AccountOpeningId")]
        public Int32 AccountOpeningId { get; set; }
        [Database("employed"), Display("Employed")]
        public Int32 Employed { get; set; }
        [Database("self_employed"), Display("Self Employed")]
        public Int32 SelfEmployed { get; set; }
        [Database("occupation"), Display("Occupation")]
        public String Occupation { get; set; }
        [Database("employer_name"), Display("EmployerName")]
        public String EmployerName { get; set; }
        [Database("nature_of_business"), Display("NatureOfBusiness")]
        public String NatureOfBusiness { get; set; }
        [Database("monthly_income"), Display("MonthlyIncome")]
        public String MonthlyIncome { get; set; }
        [Database("occupation_others"), Display("OccupationOthers")]
        public String OccupationOthers { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
    }
}
