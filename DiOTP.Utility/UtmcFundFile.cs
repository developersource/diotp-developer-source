using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("utmc_fund_files")]
    public class UtmcFundFile
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("utmc_fund_information_id"), Display("UtmcFundInformationId")]
        public Int32 UtmcFundInformationId { get; set; }
        [Database("utmc_fund_file_types_def_id"), Display("UtmcFundFileTypesDefId")]
        public Int32 UtmcFundFileTypesDefId { get; set; }
        [Database("name"), Display("Name")]
        public String Name { get; set; }
        [Database("url"), Display("Url")]
        public String Url { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime? UpdatedDate { get; set; }
        [Database("updated_by"), Display("UpdatedBy")]
        public Int32? UpdatedBy { get; set; }
        [Database("status"), Display("Status")]
        public Int32? Status { get; set; }
        public UtmcFundFileTypesDef UtmcFundFileTypesDefIdUtmcFundFileTypesDef { get; set; }
        public UtmcFundInformation UtmcFundInformationIdUtmcFundInformation { get; set; }
    }
}
