using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("site_content")]
    public class SiteContent
    {
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("site_content_type_id"), Display("SiteContentTypeId")]
        public Int32? SiteContentTypeId { get; set; }
        [Database("priority"), Display("Priority")]
        public Int32 Priority { get; set; }
        [Database("is_display"), Display("IsDisplay")]
        public Int32 IsDisplay { get; set; }
        [Database("is_content_display"), Display("IsContentDisplay")]
        public Int32 IsContentDisplay { get; set; }
        [Database("title"), Display("Title")]
        public String Title { get; set; }
        [Database("title_color"), Display("TitleColor")]
        public String TitleColor { get; set; }
        [Database("sub_title"), Display("SubTitle")]
        public String SubTitle { get; set; }
        [Database("short_display_content"), Display("ShortDisplayContent")]
        public String ShortDisplayContent { get; set; }
        [Database("content"), Display("Content")]
        public String Content { get; set; }
        [Database("content_color"), Display("ContentColor")]
        public String ContentColor { get; set; }
        [Database("image_url"), Display("ImageUrl")]
        public String ImageUrl { get; set; }
        [Database("publish_date"), Display("PublishDate")]
        public DateTime PublishDate { get; set; }
        [Database("status"), Display("Status")]
        public Int32 Status { get; set; }
        [Database("created_by"), Display("CreatedBy")]
        public Int32 CreatedBy { get; set; }
        [Database("created_date"), Display("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Database("updated_by"), Display("UpdatedBy")]
        public Int32? UpdatedBy { get; set; }
        [Database("updated_date"), Display("UpdatedDate")]
        public DateTime? UpdatedDate { get; set; }
        public SiteContentType SiteContentTypeIdSiteContentType { get; set; }
    }
}
