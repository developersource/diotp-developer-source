using DiOTP.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    [Database("utmc_daily_nav_fund")]
    public class UtmcDailyNavFund
    {
        public UtmcDailyNavFund() {
            this.FundName = "";
        }
        [Database("id"), Display("Id")]
        public Int32 Id { get; set; }
        [Database("epf_ipd_code"), Display("EpfIpdCode")]
        public String EpfIpdCode { get; set; }
        [Database("ipd_fund_code"), Display("IpdFundCode")]
        public String IpdFundCode { get; set; }
        [Database("daily_nav_date"), Display("DailyNavDate")]
        public DateTime DailyNavDate { get; set; }
        [Database("daily_nav"), Display("DailyNav")]
        public Decimal DailyNav { get; set; }
        [Database("report_date"), Display("ReportDate")]
        public DateTime ReportDate { get; set; }
        [Database("daily_unit_created"), Display("DailyUnitCreated")]
        public Decimal DailyUnitCreated { get; set; }
        [Database("daily_nav_epf"), Display("DailyNavEpf")]
        public Decimal DailyNavEpf { get; set; }
        [Database("daily_unit_created_epf"), Display("DailyUnitCreatedEpf")]
        public Decimal DailyUnitCreatedEpf { get; set; }
        [Database("daily_unit_price"), Display("DailyUnitPrice")]
        public Decimal DailyUnitPrice { get; set; }
        [Database("daily_unit_created_epf_adj"), Display("DailyUnitCreatedEpfAdj")]
        public Decimal DailyUnitCreatedEpfAdj { get; set; }

        public String FundName { get; set; }
    }
}
