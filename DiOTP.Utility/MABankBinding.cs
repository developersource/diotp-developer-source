﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Utility
{
    public class MABankBinding
    {
        public String username { get; set; }
        public String id_no { get; set; }
        public String ma_no { get; set; }
        public String bank_name { get; set; }
        public String account_name { get; set; }
        public String bank_account_no { get; set; }
        public String image { get; set; }
        public DateTime updated_date { get; set; }
    }
}
