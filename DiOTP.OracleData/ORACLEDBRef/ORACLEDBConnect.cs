using MySql.Data.MySqlClient;
using System.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using DiOTP.Utility.Helper;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.OracleData.ORACLEDBRef
{
    public class ORACLEDBConnect
    {
        public OracleConnection connection;
        public string connectionString = "";
        public ORACLEDBConnect()
        {
            string Host = ConfigurationManager.AppSettings["Oracle_Host"];
            string Port = ConfigurationManager.AppSettings["Oracle_Port"];
            string DatabaseName = ConfigurationManager.AppSettings["Oracle_DatabaseName"];
            string Useraame = ConfigurationManager.AppSettings["Oracle_UserName"];
            string Password = ConfigurationManager.AppSettings["Oracle_Password"];
            string ConnectionString = "Data Source = (DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = " + Host + ")(PORT = " + Port + ")))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = " + DatabaseName + "))); User Id = " + Useraame + "; Password = " + Password + ";";
            connectionString = ConnectionString;
            Initialize();
        }
        public void Initialize()
        {
            connection = new OracleConnection(connectionString);
        }
        public bool OpenConnection()
        {
            try
            {
                if (connection.State == ConnectionState.Open)
                    Initialize();
                connection.Open();
                return true;
            }
            catch(MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        //MessageBox.Show("Cannot connect to server.Contact administrator");
                        break;
                    case 1045:
                        //MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        public Response CheckOpenConnection()
        {
            Response response = new Response();
            try
            {
                if (connection.State == ConnectionState.Open)
                    Initialize();
                connection.Open();
                response.IsSuccess = true;
                return response;
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        //MessageBox.Show("Cannot connect to server.Contact administrator");
                        break;
                    case 1045:
                        //MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }

        public bool CloseConnection()
        {
            try
            {
                connection.Close();
                //connection.Dispose();
                return true;
            }
            catch(OracleException ex)
            {
                Logger.WriteLog(ex.Message);
                //MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
