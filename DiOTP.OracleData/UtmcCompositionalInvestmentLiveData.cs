﻿using DiOTP.Utility;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.OracleData
{
    public class UtmcCompositionalInvestmentLiveData
    {
        static DataManager DB = new DataManager();

        public static List<UtmcCompositionalInvestment> GetDataByHolderNo(string holderNo)
        {
            List<UtmcCompositionalInvestment> objs = new List<UtmcCompositionalInvestment>();

            try
            {
                //string query = "select * from UTS.HOLDER_LEDGER A where trans_dt between to_date('" + Start_date + "', 'yyyy/mm/dd')  and to_date('" + End_date + "', 'yyyy/mm/dd')";
                string query = "select * from UTS.HOLDER_LEDGER A where holder_no='"+ holderNo + "'";
                objs = GenerateData(query, holderNo);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }

            return objs;
        }

        public static List<UtmcCompositionalInvestment> GenerateData(string Query, string holderNo)
        {
            List<UtmcCompositionalInvestment> objs = new List<UtmcCompositionalInvestment>();

            DataTable ds = new DataTable();
            string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"].ToString();

            List<UtmcMemberInvestment> utmcMemberInvestments = UtmcMemberInvestmentLiveData.GetDataByHolderNo(holderNo);

            utmcMemberInvestments.ForEach(x =>
            {
                string IPD_Fund_Code = x.IpdFundCode;
                string EPF_No = x.MemberEpfNo;
                string IPD_Member_Acc_No = x.IpdMemberAccNo;
                // Next
                string Next__Opening_Balance_Units = "0.00";          //1
                string Next_Opening_Balance_Cost_RM = "0.00";         //2                        

                // Current
                decimal Curr_Net_Cumulative_Closing_Balance_Units = 0.00M;    //1
                decimal Curr_Net_Cumulative_Closing_Balance_Cost_RM = 0.00M;   //2                                  
                decimal Curr_Market_Price_NAV = 0.00M;                     //
                decimal Curr_Unrealised_Gain_Loss_RM = 0.00M;              //

                objs.Add(new UtmcCompositionalInvestment()
                {
                    EpfIpdCode = EPF_IPD_Code,
                    IpdFundCode = IPD_Fund_Code,
                    MemberEpfNo = EPF_No,
                    IpdMemberAccNo = IPD_Member_Acc_No,
                    EffectiveDate = x.EffectiveDate,
                    OpeningBalanceUnits = Convert.ToDecimal(Next__Opening_Balance_Units),
                    OpeningBalanceCostRm = Convert.ToDecimal(Next_Opening_Balance_Cost_RM),
                    OpeningBalanceDate = Convert.ToDateTime(x.TransDt),
                    NetCumulativeClosingBalanceUnits = Curr_Net_Cumulative_Closing_Balance_Units,
                    NetCumulativeClosingBalanceCostRm = Curr_Net_Cumulative_Closing_Balance_Cost_RM,
                    NetCumulativeClosingBalanceDate = x.ReportDate,
                    MarketPriceNav = Curr_Market_Price_NAV,
                    MarketPriceEffectiveDate = x.ReportDate,
                    UnrealisedGainLossRm = Curr_Unrealised_Gain_Loss_RM,
                    ReportDate = x.ReportDate,
                    ReportKey = x.ReportDate.ToString("yyyyMMddHHmmssfff")
                });


            });


            objs.ForEach(x => {
                string IpdMemberAccNo = x.IpdMemberAccNo;
                string IPD_fund_Code = x.IpdFundCode;

                List<UtmcMemberInvestment> utmcMemberInvestments1 = utmcMemberInvestments.Where(y => y.IpdMemberAccNo == IpdMemberAccNo && y.IpdFundCode == IPD_fund_Code).ToList();

                utmcMemberInvestments1.ForEach(z=> {
                    string Balance = z.ActualTransferredFromEpfRm.ToString();
                    string Net_Cumulative_Closing_Balance_Units = z.MarketValue.ToString();
                    string MarketPrice = z.MarketValue.ToString();
                    string Gain = (Convert.ToDecimal(MarketPrice) - Convert.ToDecimal(Balance)).ToString();

                    /* gordon */
                    string MarketPricePU = decimal.Round((Convert.ToDecimal(MarketPrice) / Convert.ToDecimal(Net_Cumulative_Closing_Balance_Units)), 4).ToString();

                    x.NetCumulativeClosingBalanceUnits = Convert.ToDecimal(Net_Cumulative_Closing_Balance_Units);
                    x.NetCumulativeClosingBalanceCostRm = Convert.ToDecimal(Balance);
                    x.MarketPriceNav = Convert.ToDecimal(MarketPricePU);
                    x.UnrealisedGainLossRm = Convert.ToDecimal(Gain);

                    //string OpeningBalance = z.ActualTransferredFromEpfRm.ToString();
                    //string OpeningBalanceUnit = z.MarketValue.ToString();

                    //x.OpeningBalanceUnits = Convert.ToDecimal(OpeningBalanceUnit);
                    //x.OpeningBalanceCostRm = Convert.ToDecimal(OpeningBalance);
                });

            });


            return objs;
        }
    }
}
