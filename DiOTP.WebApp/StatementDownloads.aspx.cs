﻿using DiOTP.PolicyAndRules;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static DiOTP.PolicyAndRules.StatementPolicy;

namespace DiOTP.WebApp
{
    public partial class StatementDownloads : System.Web.UI.Page
    {
        private static readonly Lazy<IUserStatementService> lazyUserStamentServiceObj = new Lazy<IUserStatementService>(() => new UserStatementService());

        public static IUserStatementService IUserStatementService { get { return lazyUserStamentServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                if (Session["isVerified"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                }
                if (!IsPostBack)
                {
                    Response responseStatements = (Response)StatementPolicy.Get(new StatementPolicy.StatementClass { Step = 1, httpContext = Context,  });

                    if (responseStatements.IsSuccess)
                    {
                        ResponseStatementStep1 responseStatementStep1 = (ResponseStatementStep1)responseStatements.Data;
                        StatementPolicyEnum statementPolicyEnum = (StatementPolicyEnum)Enum.Parse(typeof(StatementPolicyEnum), responseStatementStep1.Code);
                        string message = statementPolicyEnum.ToDescriptionString();
                        if (message == "SUCCESS")
                        {
                            List<UserAccount> userAccounts = responseStatementStep1.userAccounts;
                            string all = String.Join(",", userAccounts.Select(x => "'" + x.AccountNo + "'").ToArray());
                            ddlFundAccount.Items.Add(new ListItem("All Accounts", all));
                            foreach (UserAccount userAccount in userAccounts)
                            {
                                ddlFundAccount.Items.Add(new ListItem(CustomValues.GetAccounPlan(userAccount.HolderClass) + " - " + userAccount.AccountNo, userAccount.AccountNo));
                            }

                            DateTime now = DateTime.Now;
                            DateTime lastYears = now.AddMonths(-12);
                            ddlFromMonth.Items.Add(new ListItem { Text = "Select", Value = "" });
                            ddlToMonth.Items.Add(new ListItem { Text = "Select", Value = "" });
                            while (lastYears < now)
                            {
                                
                                ddlFromMonth.Items.Add(new ListItem { Text = CustomValues.GetMonthNameByNumber(lastYears.Month) + ", " + lastYears.Year, Value = lastYears.Year + "_" + lastYears.Month, Selected = (lastYears == now.AddMonths(-1)) });
                                ddlToMonth.Items.Add(new ListItem { Text = CustomValues.GetMonthNameByNumber(lastYears.Month) + ", " + lastYears.Year, Value = lastYears.Year + "_" + lastYears.Month, Selected = (lastYears == now.AddMonths(-1)) });
                                
                                lastYears = lastYears.AddMonths(1);
                            }

                            tbodyStatementDownload.InnerHtml = "";
                            GenerateStatement();

                        }
                        else if (message == "EXCEPTION")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + responseStatementStep1.Message + "','');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + message + "','');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + responseStatements.Message + "','');", true);
                    }
                    
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            tbodyStatementDownload.InnerHtml = "";
            GenerateStatement();
        }

        public void GenerateStatement()
        {
            try
            {
                Response responseStatements = (Response)StatementPolicy.Get(new StatementPolicy.StatementClass { Step = 2, httpContext = Context, StatementType = ddlStatementType.SelectedValue, FundAccount = ddlFundAccount.SelectedValue, FromMonth = ddlFromMonth.SelectedValue, ToMonth = ddlToMonth.SelectedValue });

                if (responseStatements.IsSuccess)
                {
                    ResponseStatementStep2 responseStatementStep2 = (ResponseStatementStep2)responseStatements.Data;
                    StatementPolicyEnum statementPolicyEnum = (StatementPolicyEnum)Enum.Parse(typeof(StatementPolicyEnum), responseStatementStep2.Code);
                    string message = statementPolicyEnum.ToDescriptionString();
                    if (message == "SUCCESS")
                    {
                        spanUsername.InnerHtml = responseStatementStep2.Username;
                        tbodyStatementDownload.InnerHtml = responseStatementStep2.TbodyString;
                    }
                    else if (message == "EXCEPTION")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + responseStatementStep2.Message + "','');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + message + "','');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + responseStatements.Message + "','');", true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("StatementDownloads Page load: " + ex.Message);
            }

        }
    }
}