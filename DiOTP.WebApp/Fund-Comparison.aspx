﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="Fund-Comparison.aspx.cs" Inherits="DiOTP.WebApp.Fund_Comparison" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Content/css/jquery-ui.css" rel="stylesheet" />
    <link href="/Content/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="/Content/css/fixedHeader.bootstrap.min.css" rel="stylesheet" />
    <link href="/Content/css/responsive.bootstrap.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="section section-fund-comaparison">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx" id="homeLink" runat="server">Home</a></li>
                            <li><a href="/Funds-listing.aspx"> Fund Center</a></li>
                            <li><div id="fundInformation" runat="server" style="float:right;"><a href="/Fund-Information.aspx">Fund Information</a></div></li>
                            <li class="active">Fund Comparison</li>
                        </ol>
                    </div>

                    <h3 class="hr-left mb-20 pb-12 d-ib mob-fund-head tab-fund-head">Funds Comparison</h3>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-head">
                                <h5 class="mt-0 ff-ls" style="margin-bottom: 2px !important;">Chart Center</h5>
                                <asp:HiddenField ID="hdnFundId" runat="server" ClientIDMode="Static" />
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <ol id="availableFundsList" class="selectable mt-20">
                                        <li class="ui-state-default" style="width: 100%">Loading Funds <i class="fa fa-spinner fa-spin"></i></li>
                                    </ol>
                                </div>
                                <div class="col-md-9">
                                    <div class="row mt-20">
                                        <div class="col-md-12 text-center">
                                            <ul class="nav nav-tabs chart-tabs d-ib">
                                                <!-- chart-tabs: Used in script -->
                                                <li>
                                                    <a href="#" data-toggle="tab" data-value="1">YTD</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-toggle="tab" data-value="2">Last Year</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-toggle="tab" data-value="3">1 WK</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-toggle="tab" data-value="4">1 MTH</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-toggle="tab" data-value="5">3 MTH</a>
                                                </li>
                                                <li>
                                                    <a class="active" href="#" data-toggle="tab" data-value="6">6 MTH</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-toggle="tab" data-value="7">1 YR</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-toggle="tab" data-value="8">2 YR</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-toggle="tab" data-value="9">3 YR</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-toggle="tab" data-value="10">5 YR</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-toggle="tab" data-value="11">10 YR</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="loadingChartDiv">
                                                <div class="typing_loader"></div>
                                                <div class="text-center">Loading...</div>
                                                <%--<i class="fa fa-spinner fa-spin"></i>--%>
                                            </div>
                                            <div id="linechart"></div>

                                            <table class="table chart-table table-bordered mb-5 myDataTable fund-table">
                                                <thead>
                                                    <tr class="fs-13">
                                                        <th width="35%">Fund Name</th>
                                                        <th>Period</th>
                                                        <th>Total Returns </th>
                                                        <th class="hide">3 yr Annualised Volatility(%) </th>
                                                        <th>Min NAV </th>
                                                        <th>Max NAV </th>
                                                        <th>Current NAV </th>
                                                    </tr>
                                                </thead>
                                                <tbody id="chartTableBody">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="server">
    <script src="/Content/js/jquery-ui.js"></script>

    <script src="/Content/js/jquery.dataTables.min.js"></script>
    <script src="/Content/js/dataTables.bootstrap.min.js"></script>
    <script src="/Content/js/dataTables.responsive.min.js"></script>
    <script src="/Content/js/responsive.bootstrap.min.js"></script>
    <script src="/Content/js/highcharts.js"></script>
    <script>
        var chart;
        var dataValue = 0;
        var fundChartObject = new Array();
        var dataTable;
        var selectedFunds;
        var isChartUpdated = 1;
        var fundExisting = new Array();
        $(document).ready(function () {
            $('.chart-tabs a').click(function () {
                dataValue = $(this).attr('data-value');
                isChartUpdated = 1;
                PlotChart();
            });
            $(".selectable").bind("mousedown", function (e) {
                e.metaKey = true;
            }).selectable({
                stop: function () {
                    PlotChart();
                }
            });
            function PlotChart() {
                var AllFunds = $.map($('#availableFundsList li'), function (li) {
                    return parseInt($(li).attr('value'));
                });
                selectedFunds = $.map($('#availableFundsList li.ui-selected'), function (li) {
                    return parseInt($(li).attr('value'));
                });
                console.log(selectedFunds);
                //$('.loadingChartDiv').fadeIn();
                $.each(AllFunds, function (index, fundId) {
                    if ($('#availableFundsList li[value="' + fundId + '"]').hasClass('ui-selected')) {
                        if (($('.data-fund-id-' + fundId).length == 0 || isChartUpdated == 1)) {
                            var funds = new Array();
                            var fundPlot = {
                                fundId: fundId,
                                dataValue: dataValue
                            };
                            funds[0] = fundId;
                            console.log(fundExisting.indexOf(fundPlot));
                            if (fundExisting.indexOf(fundPlot) == -1) {
                                
                                $.ajax({
                                url: "Fund-Comparison.aspx/GetFundChartSeries",
                                async: false,
                                contentType: 'application/json; charset=utf-8',
                                type: "GET",
                                dataType: "JSON",
                                data: { 'selectedFunds': JSON.stringify(funds), 'dataValue': dataValue },
                                success: function (data) {
                                    var json = data.d;
                                    if (json.IsSuccess) {
                                        if (isChartUpdated == 1) {
                                            while (chart.series.length > 0) chart.series[0].remove(true);
                                            fundChartObject = new Array();
                                            isChartUpdated = 0;
                                            chart.xAxis[0].setCategories(json.Data[0].Labels);
                                        }
                                        fundChartObject.push(json.Data[0]);
                                        var html = "";
                                        $.each(json.Data, function (idx, y) {
                                            var series = [];
                                            for (var x = 0; x < y.Values.length; x++) {
                                                series.push(parseFloat(y.Values[x]));
                                            }
                                            chart.addSeries({
                                                data: series,
                                                name: y.fundName,
                                                className: 'data-fund-id-' + fundId,
                                                useHTML: true
                                            });
                                            dataTable = $('.myDataTable').DataTable({
                                                destroy: true,
                                                dom: '',
                                                responsive: true,
                                                sorting: [],
                                                searching: false,
                                                paging: false,
                                                ordering: false
                                            });
                                        });
                                        $('#chartTableBody .dataTables_empty').remove();


                                        var dataValueName = $('[data-value="' + dataValue + '"]').text();
                                        $.each(fundChartObject, function (idx, y) {
                                            var date = formatDate(y.currentNavDate);
                                            $('#chartTableBody').append("<tr class='fs-13' data-fund-id='" + y.fundId + "'><td>" + y.fundName + "</td><td>" + dataValueName + "</td><td>" + y.totalReturns.toFixed(4) + "</td><td class='hide'>" + y.threeYearAnnualisedPercent + "</td><td>" + y.minNav.toFixed(4) + "</td><td>" + y.maxNav.toFixed(4) + "</td><td>" + y.currentNav.toFixed(4) + " (" + date + ")</td></tr>");
                                        });
                                    }
                                    else {
                                        ShowCustomMessage('Exception', json.Message, '');
                                    }
                                }
                            });
                            }
                            else
                            {

                            }
                            
                        }
                        else {
                            if ($('.data-fund-id-' + fundId).length > 0) {
                                var className = $('.data-fund-id-' + fundId).attr('class');
                                var lastClass = className.split(' ').filter(value => /^highcharts-series-+/.test(value))[0];
                                var seriesIndex = parseInt(lastClass.split('-')[2]);
                                chart.series[seriesIndex].show();
                                $('#chartTableBody tr[data-fund-id="' + fundId + '"]').show();
                            }
                        }
                    }
                    else {
                        if ($('.data-fund-id-' + fundId).length > 0) {
                            var className = $('.data-fund-id-' + fundId).attr('class');
                            var lastClass = className.split(' ').filter(value => /^highcharts-series-+/.test(value))[0];
                            var seriesIndex = parseInt(lastClass.split('-')[2]);
                            chart.series[seriesIndex].remove();
                            fundChartObject.splice(seriesIndex, 1);
                            $('#chartTableBody tr[data-fund-id="' + fundId + '"]').remove();

                        }
                    }
                });
                
            }
            function formatDate(input) {
                
                var datePart = input.match(/\d+/g),
                    year = datePart[0], // get only two digits
                    month = datePart[1], day = datePart[2];
                return day + '/' + month + '/' + year;
            }
            $('.section-ticker').fadeOut();
            var selectedFundId = $('#hdnFundId').val();
            $.ajax({
                url: "Fund-Comparison.aspx/GetFunds",
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: function (data) {
                    return data;
                },
                success: function (data) {
                    var json = data.d;
                    var html = "";
                    var fundsHTML = '';
                    if (json.IsSuccess) {
                        $.each(json.Data, function (idx, y) {
                            if (selectedFundId == y.Id) {
                                fundsHTML += '<li class="ui-state-default ui-selectee ui-selected click-selected" value="' + y.Id + '">' + y.FundName + (y.UtmcFundInformationIdUtmcFundDetails[0].IsEpfApproved == 1 ? '(EPF-EMIS)' : '') + '</li>';
                            }
                            else {
                                fundsHTML += '<li class="ui-state-default" value="' + y.Id + '">' + y.FundName + (y.UtmcFundInformationIdUtmcFundDetails[0].IsEpfApproved == 1 ? '(EPF-EMIS)' : '') + '</li>';
                            }
                        });
                        $('#availableFundsList').html(fundsHTML);
                        $('[data-value="6"]').click();
                    }
                    else {
                        ShowCustomMessage('Exception', json.Message, '');
                    }
                }
            });

            chart = Highcharts.chart('linechart', {
                chart: {
                    resetZoomButton: {
                        position: {
                            // align: 'right', // by default
                            // verticalAlign: 'top', // by default
                            x: 0,
                            y: -80
                        }
                    },
                    height: '450px',
                    type: 'line',
                    events: {
                        addSeries: function () {
                            $('.loadingChartDiv').fadeOut();
                            var label = this.renderer.label('A series was added', 30, 30)
                                       .attr({
                                           fill: Highcharts.getOptions().colors[0],
                                           padding: 10,
                                           zIndex: 8
                                       })
                                       .css({
                                           color: '#FFFFFF'
                                       })
                                       .add();

                            setTimeout(function () {
                                label.fadeOut();
                            }, 1000);
                        },
                        load: function () {
                        }
                    },
                    borderColor: '#000000',
                    borderWidth: 0,
                    inverted: false,
                    zoomType: 'x',
                    spacingRight: 20
                },
                title: {
                    useHTML: true,
                    text: '<p class="j-m-text">Based on Change</p>'
                },
                subtitle: {
                    useHTML: true,
                    text: (document.ontouchstart === undefined ?
                        'Click and drag in the plot area to zoom in' :
                        '<div class="pinchZoom">Pinch the chart to zoom in</div>')
                },
                xAxis: {
                    allowDecimals: true,
                    labels: {
                        rotation: -45,
                        step: 30,
                        showLastLabel: true,
                        endOnTick: true,
                        x: -10,
                        formatter: function () {
                            var x = this.value;
                            var fundDetails = fundChartObject[0];
                            var labelIndex = jQuery.inArray(($.grep(fundDetails.Labels, function (obj) {
                                return obj === x;
                            })[0]), fundDetails.Labels);
                            var navDate = fundDetails.NavDates[labelIndex];
                            var xLabelYears = $.unique($.map(fundDetails.NavDates, function (obj) {
                                return (new Date(obj).getFullYear());
                            }));
                            
                            if (xLabelYears.length == 1) {
                                var xLabelMonths = $.unique($.map(fundDetails.NavDates, function (obj) {
                                    return (new Date(obj).getMonth() + 1);
                                }));
                                if (xLabelMonths.length == 2) {
                                    chart.xAxis[0].options.labels.step = 3;
                                    return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                                }
                                else if (xLabelMonths.length == 1) {
                                    chart.xAxis[0].options.labels.step = 1;
                                    return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                                }
                                else if (xLabelMonths.length == 4) {
                                    chart.xAxis[0].options.labels.step = 10;
                                    return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                                }
                                else if (xLabelMonths.length == 12) {
                                    console.log(xLabelMonths.length);
                                    chart.xAxis[0].options.labels.step = 30;
                                    return Highcharts.dateFormat('%b %Y', new Date(navDate));
                                }
                                else {
                                    chart.xAxis[0].options.labels.step = 14;
                                    return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                                }
                            }
                            else if (xLabelYears.length == 2) {
                                var xLabelMonths = $.unique($.map(fundDetails.NavDates, function (obj) {
                                    return (new Date(obj).getMonth() + 1);
                                }));
                                if (xLabelMonths.length == 1) {
                                    chart.xAxis[0].options.labels.step = 1;
                                    return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                                }
                                else if (xLabelMonths.length == 2) {
                                    chart.xAxis[0].options.labels.step = 3;
                                    return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                                }
                                else if (xLabelMonths.length == 4) {
                                    chart.xAxis[0].options.labels.step = 10;
                                    return Highcharts.dateFormat('%e %b %Y', new Date(navDate));
                                }
                                else if (xLabelMonths.length == 12) {
                                    console.log(xLabelMonths.length);
                                    chart.xAxis[0].options.labels.step = 30;
                                    return Highcharts.dateFormat('%b %Y', new Date(navDate));
                                }
                                else {
                                    chart.xAxis[0].options.labels.step = 29;
                                    return Highcharts.dateFormat('%b %Y', new Date(navDate));
                                }
                            }
                            else if (xLabelYears.length == 3) {
                                chart.xAxis[0].options.labels.step = 58;
                                return Highcharts.dateFormat('%b %Y', new Date(navDate));
                            }
                            else if (xLabelYears.length == 4) {
                                chart.xAxis[0].options.labels.step = 1;
                                return Highcharts.dateFormat('%b %Y', new Date(navDate));
                            }
                            else if (xLabelYears.length == 6) {
                                chart.xAxis[0].options.labels.step = 4;
                                return Highcharts.dateFormat('%b %Y', new Date(navDate));
                            }
                            else {
                                chart.xAxis[0].options.labels.step = 6;
                                return Highcharts.dateFormat('%b %Y', new Date(navDate));
                            }
                            //Highcharts.dateFormat('%b %Y', new Date(this.value))
                        },
                        style: {
                            fontSize: '10px'
                        },
                        padding: 2,
                        distance: 5
                    },
                    categories: []
                },
                yAxis: {
                    endOnTick: false,
                    allowDecimals: true,
                    title: {
                        text: 'Change'
                    }
                },
                tooltip: {
                    shared: true,
                    pointFormat: '{series.name} produced <b>{point.y}</b><br/>warheads in {point.x}',
                    formatter: function () {
                        var s = [];
                        $.each(this.points, function (i, point) {
                            var seriesName = point.series.name;
                            var y = point.y;
                            var x = point.x;
                            var fundDetails = $.grep(fundChartObject, function (obj) { return obj.fundName == seriesName })[0];
                            var labelIndex = jQuery.inArray(($.grep(fundDetails.Labels, function (obj) {
                                return obj === x;
                            })[0]), fundDetails.Labels);
                            var navPrice = fundDetails.NavPrices[labelIndex];
                            var navDate = fundDetails.NavDates[labelIndex];
                            s.push('<span style="color:' + point.color + '">' + seriesName + ' </span><br/><span style="color:' + point.color + '">Price: ' + navPrice + ' (Change: ' + y + ') on ' +
                            navDate + ' (Day: ' + x + ')</span>');
                            //Highcharts.dateFormat('%b %Y', x)
                        });


                        return s.join('<br />');
                    },
                    followPointer: true,
                    //positioner: { x: 100, y: 100 },
                    crosshairs: [true, true],
                    crosshairs: {
                        color: 'rgba(0,0,0,0.3)',
                        dashStyle: 'Solid',
                        width: 2
                    },
                },
                credits: {
                    href: '/Index.aspx',
                    text: 'eapexis.apexis.com.my',
                    position: {
                        align: 'right',
                        verticalAlign: 'bottom',
                    }
                },
                plotOptions: {
                    line: {
                        className: 'highchart-line-class',
                        marker: {
                            enabled: false,
                            symbol: 'circle',
                            radius: 2,
                            states: {
                                hover: {
                                    enabled: true
                                },
                            }
                        }
                    },
                    series: {
                        events: {
                            legendItemClick: function () {
                                this.slice(null);
                                return false;
                            }
                        }
                    }
                },
                series: [
                //{
                //    name: 'Nav Price',
                //    data: data11
                //},
                //{
                //    name: 'Nav',
                //    data: [null, null, null, null, null, null, null, null, null, null,
                //        5, 25, 50, 120, 150, 200, 426, 660, 869, 1060, 1605, 2471, 3322,
                //        4238, 5221, 6129, 7089, 8339, 9399, 10538, 11643, 13092, 14478,
                //        15915, 17385, 19055, 21205, 23044, 25393, 27935, 30062, 32049,
                //        33952, 35804, 37431, 39197, 45000, 43000, 41000, 39000, 37000,
                //        35000, 33000, 31000, 29000, 27000, 25000, 24000, 23000, 22000,
                //        21000, 20000, 19000, 18000, 18000, 17000, 16000]
                //}
                ],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                layout: 'horizontal'
                            },
                            yAxis: {
                                labels: {
                                    align: 'left',
                                    x: 0,
                                    y: -5
                                },
                                title: {
                                    text: null
                                }
                            },
                            subtitle: {
                                text: null
                            },
                            credits: {
                                enabled: false
                            }
                        }
                    }],
                }
            });
            Highcharts.setOptions({
                lang: {
                    resetZoom: 'Reset zoom', resetZoomTitle: 'Reset zoom level 1:1'
                }
            });

        });
    </script>
</asp:Content>
