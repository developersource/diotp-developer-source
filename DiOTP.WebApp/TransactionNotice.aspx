﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="TransactionNotice.aspx.cs" Inherits="DiOTP.WebApp.TransactionNotice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx">Home</a></li>
                            <li class="active">Transaction Notice</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Transaction Notice</h3>
                    </div>
                    <p>This facility providing for any transactions permitted by the Master Prospectus / Prospectus / Information Memorandum and its Supplementary(ies) / Replacement EXCEPT for transfer, cooling-off right and application for account opening. All the online transactions provided by this facility will be processed by us upon receipt and a confirmation of receipt of the online transaction will be sent to your last known email addressed maintained in our record.</p>
                    <b>Alternative Method of Transaction</b>
                    <p>Any alternative method of transactions (other than online transactions), you may carry out these transactions by submitting the physical forms to our business office stated in the respective Master Prospectus / Prospectus / Information Memorandum and its Supplementary(ies) / Replacement. You may also submit the physical forms to our consultants, our authorised distributors or our representatives. Please contact our Customer Service at 603 - 2095 9999 (9.00am - 6.00pm, Monday to Friday) for any enquiries or more details.</p>
                    <b>Registration of Supplementary or Replacement Master Prospectus / Prospectus Prior to Application for units</b>
                    <p>If you have applied for units in a fund and a Supplementary or Replacement Master Prospectus / Prospectus is submitted to the Securities Commission Malaysia for registration before the issuance or transfer of units to you.</p>
                    <p>You will be given the opportunity to withdraw your application within 14 days from the date of your receipt of a notice of the registration of the applicable Supplementary or Replacement Master Prospectus / Prospectus.</p>
                    <b>Registration of Supplementary or Replacement Master Prospectus / Prospectus after Application for units</b>
                    <p>You may not withdraw your application for units in the fund(s), where a supplementary or replacement Master Prospectus / Prospectus has been registered after issuance or transfer of the units to you.</p>
                    <b>Suspension of Online Method of Transaction</b>
                    <p>In the event online method of transaction is suspended for whatever reasons, you may still continue to affect transaction via submission of physical forms, please refer to alternative method of transaction for further details.</p>
                    <b>Transfer</b>
                    <p>Transfer is not available in this facility. You may carry out transfer by submitting the physical forms to our business office stated in the respective Master Prospectus / Prospectus / Information Memorandum and its Supplementary(ies) / Replacement. You may also submit the physical forms to our consultants, our authorised distributors or our representatives. Please contact our Customer Service at 603 - 2095 9999 (9.00am - 6.00pm, Monday to Friday) for any enquiries or more details.</p>
                    <b>Cooling-off Right</b>
                    <p>Cooling-off is not offered as part of the online transaction. If you intend to exercise your cooling-off right, you may do so by submitting the physical forms to our business office stated in the respective Master Prospectus / Prospectus / Information Memorandum and its Supplementary(ies) / Replacement. You may also submit the physical forms to our consultants, our authorised distributors or our representatives. Please refer to the relevant Master Prospectus / Prospectus / Information Memorandum and its Supplementary(ies) / Replacement for more details.</p>
                    <b>Contact Us</b>
                    <p>If you require any assistance or clarification in relation to the above, or you may confirm the status of your online application, by reaching us at:</p>
                    <p>Apex Investment Services Berhad<br/>3rd Floor, Menara MBSB,<br/>46 Jalan Dungun,<br/>Damansara Heights,<br/>50490 Kuala Lumpur, Malaysia.</p>
                    <p>Tel: (603) 2095 9999<br />Fax: (603) 2095 0693</p>
                    <p>Website: www.apexis.com.my<br/>Email: enquiry@apexis.com.my</p>
                </div>
            </div>
        </div>
    </section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
