﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomAttributes;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class MA_Account_Details : System.Web.UI.Page
    {
        private static readonly Lazy<IMaHolderRegService> lazyMaHolderRegObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());

        public static IMaHolderRegService IMaHolderRegService { get { return lazyMaHolderRegObj.Value; } }

        private static readonly Lazy<IUtmcMemberInvestmentService> lazyUtmcMemberInvestmentObj = new Lazy<IUtmcMemberInvestmentService>(() => new UtmcMemberInvestmentService());

        public static IUtmcMemberInvestmentService IUtmcMemberInvestmentService { get { return lazyUtmcMemberInvestmentObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    User user = (User)Session["user"];
                    Int32 MAHolderRegID = Convert.ToInt32(user.UserIdUserAccounts.FirstOrDefault().MaHolderRegIdMaHolderReg.Id);
                    Response response = IMaHolderRegService.GetSingle(MAHolderRegID);
                    if (response.IsSuccess)
                    {
                        MaHolderReg maHolderReg = (MaHolderReg)response.Data;

                        StringBuilder sbMaHolder = new StringBuilder();
                        foreach (PropertyInfo p in typeof(MaHolderReg).GetProperties().Where(x => x.GetValue(maHolderReg) != null).Where(x => x.GetValue(maHolderReg).ToString().Trim() != ""))
                        {
                            sbMaHolder.Append("<div class='col-md-6'><strong>" + (p.GetCustomAttributes(typeof(DisplayAttribute), true).FirstOrDefault() as DisplayAttribute).displayName + "</strong> <div>" + p.GetValue(maHolderReg) + "</div> </div>");
                        }

                        MAAccountDetails.InnerHtml = sbMaHolder.ToString();

                        String propertyName = nameof(UtmcMemberInvestment.IpdMemberAccNo);

                        Response responseUMIList = IUtmcMemberInvestmentService.GetDataByPropertyName(propertyName, maHolderReg.HolderNo.ToString(), true, 0, 0, false);
                        if (responseUMIList.IsSuccess)
                        {
                            List<UtmcMemberInvestment> utmcMemberInvestments = ((List<UtmcMemberInvestment>)responseUMIList.Data).OrderByDescending(x => x.ReportDate).ToList();

                            StringBuilder sbMemInvests = new StringBuilder();
                            int index = 1;
                            foreach (UtmcMemberInvestment u in utmcMemberInvestments)
                            {
                                sbMemInvests.Append("<tr class='fs-12'>");
                                sbMemInvests.Append("<td>" + index + "</td>");
                                sbMemInvests.Append("<td>" + u.EffectiveDate + "</td>");
                                sbMemInvests.Append("<td>" + u.ReportDate + "</td>");
                                sbMemInvests.Append("<td>" + u.ActualTransferredFromEpfRm + "</td>");
                                sbMemInvests.Append("<td>" + u.ActualCost + "</td>");
                                sbMemInvests.Append("<td>" + u.Units + "</td>");
                                sbMemInvests.Append("<td>" + u.BookValue + "</td>");
                                sbMemInvests.Append("<td>" + u.MarketValue + "</td>");
                                sbMemInvests.Append("<td>" + u.RedemptionCost + "</td>");
                                sbMemInvests.Append("<td>" + u.MemberEpfNo + "</td>");
                                sbMemInvests.Append("<td>" + u.EpfIpdCode + "</td>");
                                sbMemInvests.Append("<td>" + u.IpdFundCode + "</td>");
                                sbMemInvests.Append("<td>" + u.IpdMemberAccNo + "</td>");
                                sbMemInvests.Append("<td>" + u.Isactive + "</td>");
                                sbMemInvests.Append("</tr>");
                                index++;
                            }
                            tbodyInvestments.InnerHtml = sbMemInvests.ToString();
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }
    }
}