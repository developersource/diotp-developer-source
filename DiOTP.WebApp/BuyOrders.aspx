﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="BuyOrders.aspx.cs" Inherits="DiOTP.WebApp.BuyOrders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <style>
        .loader-bg {
            display: none;
        }

        .order-detail-div {
            position: absolute;
            padding: 15px;
            border: 1px solid #059cce;
            z-index: 999;
            background: #fff;
        }

            .order-detail-div .table {
                margin-bottom: 0;
            }

        .order-detail-div-close {
            content: 'X';
            position: absolute;
            right: 0;
            top: 0;
            padding: 5px;
            background: #059cce;
            color: #fff;
            font-weight: bold;
            line-height: 12px;
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/Portfolio.aspx">eApexIs</a></li>
                    <li><a href="javascript:;">User Center</a></li>
                    <li class="active">Transaction History</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Transaction  History</h3>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h5>Order history for Master Account: <span id="spanUsername" runat="server"></span></h5>

                    <div class="tabs-horizontal my-tabs mob" id="tabsDiv" runat="server" clientidmode="static">
                        <ul class="nav nav-tabs my-nav-tabs mb-10 pull-left">
                            <li id="type9" runat="server">
                                <asp:DropDownList CssClass="form-control mr-5" ID="ddlUserAccountId" runat="server" ClientIDMode="Static"></asp:DropDownList>
                            </li>
                            <li id="type0" runat="server" style="display: none">
                                <a href="javascript:;" data-type="0" class="mr-5 ml-5 OrdertypeFliter">All</a>
                            </li>
                            <%-- <li id="type1" runat="server">
                                <a href="javascript:;" data-type="1" class="mr-5 ml-5 OrdertypeFliter">Buy</a>
                            </li>
                            <li id="type2" runat="server">
                                <a href="javascript:;" data-type="2" class="mr-5 ml-5 OrdertypeFliter">Sell</a>
                            </li>
                            <li id="type3" runat="server">
                                <a href="javascript:;" data-type="3" class="mr-5 ml-5 OrdertypeFliter">Switch</a>
                            </li>
                           <li id="type6" runat="server">
                                <a href="javascript:;" data-type="6" class="mr-5 ml-5 OrdertypeFliter">RSP</a>
                            </li>--%>
                            <li>
                                <span class="ml-10 mr-10">|</span>
                            </li>
                            <li id="type5" runat="server" class="mr-5 mb-m-5">
                                <asp:DropDownList ID="ddlTransactionType" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="10" Text="All"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Pending"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Approved"></asp:ListItem>
                                    <asp:ListItem Value="9" Text="Denied"></asp:ListItem>
                                </asp:DropDownList>
                            </li>
                            <li id="type4" runat="server">
                                <asp:DropDownList ID="ddlTransactionPeriod" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0" Text="Transaction Period"></asp:ListItem>
                                    <asp:ListItem Value="-3" Text="Past 3 months"></asp:ListItem>
                                    <asp:ListItem Value="-6" Text="Past 6 months"></asp:ListItem>
                                    <asp:ListItem Value="-12" Text="Past 12 months"></asp:ListItem>
                                </asp:DropDownList>
                            </li>
                            <li id="type8" runat="server">
                                <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="btn btn-infos1 ml-10 mr-5" OnClick="btnsearch_Click" />
                            </li>
                            <li id="type7" runat="server">
                                <asp:Button ID="btnreset" runat="server" CssClass="btn btn-infos1 ml-5 mr-5" Text="Reset" OnClick="btnreset_Click" />
                            </li>
                        </ul>

                        <ul class="nav nav-tabs my-nav-tabs mb-10 pull-right">
                            <li>
                                <button type="button" class="btn btn-infos1 ml-5 mr-5" onclick="window.history.back();">Back</button>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div style="text-align: left;">
                                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Buy History</h3>
                            </div>
                            <div class="tab-pane fade active in" id="buy">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-font-size-13 table-cell-pad-5 table-bordered OrderHistoryTable">
                                            <thead id="theadUserOrders" runat="server">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Order Date</th>
                                                    <th>Order Number</th>
                                                    <th>Total Amount(MYR)/<br />
                                                        Total Units</th>
                                                    <th>Payment Method</th>
                                                    <th>Order Status</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyUserOrders" runat="server" clientidmode="Static">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div style="text-align: left;">
                                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Sell History</h3>
                            </div>


                            <div class="tab-pane active in" id="sell">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-font-size-13 table-cell-pad-5 table-bordered OrderHistoryTable">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Order Date</th>
                                                    <th>Order Number</th>
                                                    <th>Total Amount(MYR)/<br />
                                                        Total Units</th>
                                                    <th>Order Status</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyUserSellOrders" runat="server" clientidmode="Static">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <asp:HiddenField ID="hdnNumberPerPage" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnTotalRecordsCount" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnCurrentPageNo" runat="server" ClientIDMode="Static" />

    <asp:HiddenField ID="hdnCancelOrderNo" runat="server" ClientIDMode="Static" />
    <asp:Button ID="CancelOrder" runat="server" ClientIDMode="Static" CssClass="hide" OnClick="CancelOrder_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/4.1.0/autoNumeric.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".order-detail-div").draggable();
        });
    </script>
    <script>
        var isCurFormat;
        var isCurFormatNoSymbol;
        function FormatAllCurrency() {
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ' });
            });
            $('.currencyFormatNoSymbol').each(function () {
                isCurFormatNoSymbol = new AutoNumeric(this, { currencySymbol: '' });
            });
        }
        FormatAllCurrency();
        var isUniFormat;
        function FormatAllUnit() {
            $('.unitFormat').each(function () {
                isUniFormat = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: 4 });
            });
            $('.unitFormatNoDecimal').each(function () {
                isUniFormat = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: 0 });
            });
        }
        FormatAllUnit();

        $(document).ready(function () {

            $('.order-detail-link').click(function () {
                $('.order-detail-div').addClass('hide');
                $(this).next().removeClass('hide');
            });

            $('.order-detail-div-close').click(function () {
                $('.order-detail-div').addClass('hide');
            });

            $('form').on('submit', function () {
                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            });

            //$('.OrdertypeFliter').click(function () {
            //    var type = $(this).data('type');
            //    var accNo = $('#ddlUserAccountId').val();
            //    var url = "OrderList.aspx?type=" + type + "&accNo=" + accNo;
            //    window.location.href = url;
            //});
            $('#ddlUserAccountId').change(function () {
                var type = $('#tabsDiv li.active a').data('type');
                var accNo = $(this).val();
                var url = "OrderList.aspx?type=" + type + "&accNo=" + accNo;
                window.location.href = url;
            });

            $('#tbodyUserOrders').on('click', '.cancel-order', function () {
                var orderNo = $(this).data('orderno');
                $('#hdnCancelOrderNo').val(orderNo);
                $('#CancelOrder').click();
            });

            var orderHistoryTable = $('.OrderHistoryTable').DataTable({
                dom: "t<'row'<'col-md-6 infoDiv'><'col-md-6 text-right paginationDiv'>>",
                select: true,
                destroy: true,
                responsive: true,
                order: [],
                ordering: false,
                searching: false,
                length: false,
                autoWidth: false
            });

            var pageNo = parseInt($('#hdnCurrentPageNo').val());
            var total = parseInt($('#hdnTotalRecordsCount').val());
            var numPerPage = parseInt($('#hdnNumberPerPage').val());
            var pageNos = parseInt((total / numPerPage) + 1);

            $('.infoDiv').html("Showing page " + (pageNo == NaN ? "1" : pageNo) + " of " + pageNos);

            var ul = $('<ul class="pagination" />');
            var liFirst = $('<li />').addClass('firstPage');
            var aFirst = $('<a />').attr('href', 'javascript:;').addClass('pageNumber ' + (pageNo == 1 ? 'active' : '')).html('First').appendTo(liFirst);
            liFirst.appendTo(ul);
            var liPrev = $('<li />').addClass('prev');
            var aPrev = $('<a />').attr('href', 'javascript:;').addClass('pageNumber').html('Prev').appendTo(liPrev);
            liPrev.appendTo(ul);
            var className = 'pageNumber pageNoDis';
            for (i = 1; i <= pageNos; i++) {
                if (i < 3) {
                    if (i == pageNo)
                        className = 'pageNumber pageNoDis active';
                    else
                        className = 'pageNumber pageNoDis';
                    var li = $('<li />');
                    if (i == pageNo)
                        li = $('<li class="active" />');
                    var a = $('<a href="javascript:;" class="' + className + '" />').html(i).appendTo(li);
                    li.appendTo(ul);
                }
                else if (i > pageNo - 3 && i < pageNo + 3) {
                    if (i == pageNo)
                        className = 'pageNumber pageNoDis active';
                    else
                        className = 'pageNumber pageNoDis';
                    var li = $('<li />');
                    if (i == pageNo)
                        li = $('<li class="active" />');
                    var a = $('<a href="javascript:;" class="' + className + '" />').html(i).appendTo(li);
                    li.appendTo(ul);
                }
                else if (i > pageNos - 2) {
                    className = 'pageNumber pageNoDis';
                    var li = $('<li />');
                    if (i == pageNo)
                        li = $('<li class="active" />');
                    var a = $('<a href="javascript:;" class="' + className + '" />').html(i).appendTo(li);
                    li.appendTo(ul);
                }
                if (i == 3 && pageNo >= 6 || i == pageNos - 3 && pageNo <= pageNos - 6) {
                    className = 'pageNumber pageNoDis';
                    var li = $('<li />');
                    if (i == pageNo)
                        li = $('<li class="active" />');
                    var a = $('<a href="javascript:;" />').html('...').appendTo(li);
                    li.appendTo(ul);
                }
            }
            var liNext = $('<li />').addClass('next');
            var aNext = $('<a />').attr('href', 'javascript:;').addClass('pageNumber').html('Next').appendTo(liNext);
            liNext.appendTo(ul);
            var liLast = $('<li />').addClass('lastPage');
            var aLast = $('<a />').attr('href', 'javascript:;').addClass('pageNumber ' + (pageNo == pageNos ? 'active' : '')).html('Last').appendTo(liLast);
            liLast.appendTo(ul);
            $('.paginationDiv').append(ul);
            $('li.firstPage').click(function () {
                var curPageNo = parseInt(1);
                $('#hdnCurrentPageNo').val(curPageNo);
                $('form').submit();
            });
            $('li.lastPage').click(function () {
                var curPageNo = parseInt($('.pageNoDis').last().text());
                $('#hdnCurrentPageNo').val(curPageNo);
                $('form').submit();
            });
            $('li.next').click(function () {
                var curPageNo = parseInt($('#buy .pageNumber.pageNoDis.active').text());
                if (curPageNo >= parseInt($('.pageNoDis').last().text())) {

                }
                else {
                    $('#hdnCurrentPageNo').val(curPageNo + 1);
                    $('form').submit();
                }

            });
            $('li.prev').click(function () {
                var curPageNo = parseInt($('#buy .pageNumber.pageNoDis.active').text());
                if (curPageNo <= parseInt($('.pageNoDis').first().text())) {

                }
                else {
                    $('#hdnCurrentPageNo').val(curPageNo - 1);
                    $('form').submit();
                }
            });
            $('.pageNumber.pageNoDis').click(function () {
                var curPageNo = parseInt($(this).text());
                $('#hdnCurrentPageNo').val(curPageNo);
                $('form').submit();
            });
        });
    </script>
</asp:Content>

