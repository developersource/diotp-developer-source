﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="DiOTP.WebApp.Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx" id="homeLink" runat="server">Home</a></li>
                            <li class="active">Contact Us</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Contact Us</h3>
                    </div>

                    <div class="row mb-20">
                        <div class="col-md-8">
                            <div class="googlemapcontainer">
                                <div id="map" class="googlemap" style="width: 100%; height: 320px;"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="contact-form">
                                <h4>Contact Us</h4>
                                <hr />
                                <asp:TextBox BackColor="LightBlue" ID="txtName" ClientIDMode="Static" runat="server" placeholder="Enter your Name" CssClass="form-control"></asp:TextBox>
                                <asp:TextBox BackColor="LightBlue" ID="txtEmail" runat="server" ClientIDMode="Static" placeholder="Enter your Email ID" CssClass="form-control"></asp:TextBox>
                                <asp:TextBox BackColor="LightBlue" ID="txtDescription" runat="server" ClientIDMode="Static" placeholder="Description" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                <asp:Button ID="btnSubmit" OnClientClick="return clientValidation()" runat="server" Text="Submit" CssClass="btn btn-block" OnClick="btnSubmit_Click" />
                                <label id="lblStatus" style="display: none; color: green">Thank You, Your information has been submitted.</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-4 col-sm-6">
                                    <div class="contact">
                                        <div class="contact-content">
                                            <img src="Content/MyImage/loc.png" alt="Location" height="180" class="img-responsive" />
                                            <p>
                                                Apex Investment Services Berhad, 
                                                <br />
                                                3rd Floor, Menara MBSB,
                                                46 Jalan Dungun, Damansara Heights, 50490 Kuala Lumpur, Malaysia.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-3">
                                    <div class="contact">
                                        <div class="contact-content">
                                            <img src="Content/MyImage/mail-icon.png" alt="Location" height="180" class="img-responsive" />
                                            <p>
                                                <i class="fa fa-globe"></i><a href="https://www.apexis.com.my">www.apexis.com.my</a>
                                                <br />
                                                <i class="fa fa-envelope fs-13"></i><a href="mailto:enquiry@apexis.com.my">enquiry@apexis.com.my</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-3">
                                    <div class="contact">
                                        <div class="contact-content">
                                            <img src="Content/MyImage/phone-icon2.png" alt="Location" height="180" class="img-responsive" />
                                            <p>
                                                <i class="fa fa-phone"></i>+(603) 2095 9999
                                                <br />
                                                <i class="fa fa-fax fs-13"></i>+(603) 2095 0693
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="contact22">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mch">
                                            <h4 style="font-size: 17px; margin-bottom: 15px; font-family: 'Montserrat', sans-serif; font-weight: 600;">How to lodge a complaint?</h4>
                                        </div>
                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">For internal dispute resolution, you may contact our Customer Service personnel:</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                    <div class="panel-body">
                                                        <table class="table">
                                                            <tr>
                                                                <th style="width: 20%">via phone to</th>
                                                                <td>03-2095 9999 (Business Office)</td>
                                                            </tr>
                                                            <tr>
                                                                <th style="width: 20%">via fax to</th>
                                                                <td>03-2095 0693 (Business Office)</td>
                                                            </tr>
                                                            <tr>
                                                                <th style="width: 20%">via email to</th>
                                                                <td><a href="mailto:enquiry@apexis.com.my">enquiry@apexis.com.my</a></td>
                                                            </tr>
                                                            <tr>
                                                                <th style="width: 20%">via letter to</th>
                                                                <td>Apex Investment Services Berhad <br/>
                                                                    3rd Floor, Menara MBSB, 46, Jalan Dungun,
                                                                    Damansara Heights, 50490 Kuala Lumpur</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <h4 class="panel-title">
                                                        <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">If you are dissatisfied with the outcome of the internal dispute resolution process, please refer your dispute to the Securities Industries Dispute Resolution Corporation (SIDREC):</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="panel-body">
                                                        <table class="table">
                                                            <tr>
                                                                <th>via phone</th>
                                                                <td>03-2282 2280</td>
                                                            </tr>
                                                            <tr>
                                                                <th>via fax to</th>
                                                                <td>03-2282 3855</td>
                                                            </tr>
                                                            <tr>
                                                                <th>via email to</th>
                                                                <td><a href="mailto:info@sidrec.com.my">info@sidrec.com.my</a></td>
                                                            </tr>
                                                            <tr>
                                                                <th>website</th>
                                                                <td><a href="https://www.sidrec.com.my">www.sidrec.com.my</a></td>
                                                            </tr>
                                                            <tr>
                                                                <th>via letter to</th>
                                                                <td>Securities Industry Dispute Resolution Center (SIDREC) <br/>
                                                                    Unit A-9-1, Level 9, Tower A, Menara UOA Bangsar,
                                                                    No 5, Jalan Bangsar Utama 1, 59000 Kuala Lumpur.</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingThree">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">You can also direct your complaint to the SC even if you have initiated a dispute resolution process with SIDREC. To make a complaint, please contact the SC’s Consumer & Investor Office:</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="panel-body">
                                                        <table class="table">
                                                            <tr>
                                                                <th style="width: 20%">via phone to</th>
                                                                <td>03-6204 8999</td>
                                                            </tr>
                                                            <tr>
                                                                <th style="width: 20%">via fax to</th>
                                                                <td>03-6204 8991</td>
                                                            </tr>
                                                            <tr>
                                                                <th style="width: 20%">via email to</th>
                                                                <td><a href="mailto:aduan@seccom.com.my">aduan@seccom.com.my</a></td>
                                                            </tr>
                                                            <tr>
                                                                <th style="width: 20%">via online complaint form available at: </th>
                                                                <td><a href="https://www.sc.com.my">www.sc.com.my</a></td>
                                                            </tr>
                                                            <tr>
                                                                <th style="width: 20%">via letter to Consumer & Investor Office: </th>
                                                                <td>Securities Commission Malaysia<br/>No 3, Persiaran Bukit Kiara, Bukit Kiara, 50490 Kuala Lumpur.</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFour">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Alternatively, you can also escalate your complaint to the Federation of Investment Managers Malaysia (FIMM):</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="panel-body">
                                                        <table class="table">
                                                            <tr>
                                                                <th style="width: 20%">via phone to</th>
                                                                <td>03-2092 3800</td>
                                                            </tr>
                                                            <tr>
                                                                <th style="width: 20%">via fax to</th>
                                                                <td>03-2093 2700</td>
                                                            </tr>
                                                            <tr>
                                                                <th style="width: 20%">via email to</th>
                                                                <td><a href="mailto:complaints@fimm.com.my">complaints@fimm.com.my</a></td>
                                                            </tr>
                                                            <tr>
                                                                <th style="width: 20%">via online complaint form available at: </th>
                                                                <td><a href="https://www.fimm.com.my">www.fimm.com.my</a></td>
                                                            </tr>
                                                            <tr>
                                                                <th>via letter to:</th>
                                                                <td>Legal, Secretarial & Regulatory Affairs,<br/>
                                                                    Federation of Investment Managers Malaysia,<br/>
                                                                    19-06-1, 6th Floor Wisma Tune,
                                                                    No. 19, Lorong Dungun,
                                                                    Damansara Heights,
                                                                    50490 Kuala Lumpur.</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">

    <script>
        function initMap() {
            var Apex = { lat: 3.1521707, lng: 101.6636593 };
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: Apex,

            });
            var marker = new google.maps.Marker({
                position: Apex,
                map: map
            });
        }

        function clientValidation() {


            var isValid = true;
            $('#txtName,#txtEmail,#txtDescription').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": "LightBlue"
                    });
                }
            });
            if (isEmail($("#txtEmail").val()) == false) {
                 $('#txtEmail').css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                });
                return false;
            }
            if (isValid) {
                return true;
                //$('#lblStatus').show();
                //setTimeout(function () {
                //    $('#lblStatus').fadeOut(2000);
                //    $('#txtName,#txtEmail,#txtDescription').val('');
                //}, 5000);
            }
            return false;
        }

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

    </script>
    <script async="async" defer="defer" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDztzXKYxBoYV5MRg8jq99H59XEsm2Vz-k&callback=initMap"></script>

</asp:Content>
