﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class ActivateAccount : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IUserSecurityService> lazyIUserSecurityService = new Lazy<IUserSecurityService>(() => new UserSecurityService());
        public static IUserSecurityService IUserSecurityService { get { return lazyIUserSecurityService.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        public string EmailValidityTimeInMinutes = ConfigurationManager.AppSettings["EmailValidityTimeInMinutes"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {


                string url = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, "/");
                string key = Request.QueryString["key"];
                if (!string.IsNullOrEmpty(key))
                {
                    key = key.TrimEnd().Replace(" ", "+");//.Replace('_', '/').PadRight(4 * ((key.Length + 3) / 4), '=');
                    key = CustomEncryptorDecryptor.DecryptText(key);
                    string date = key.Split('_')[1];
                    DateTime dateTime = DateTime.ParseExact(date, "yyyyMMddHHmmssfff", null);
                    if ((DateTime.Now - dateTime).TotalMinutes > Convert.ToInt32(EmailValidityTimeInMinutes))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Link expired!', 'Index.aspx');", true);
                    }
                    else
                    {
                        String emailCode = key.Split('_')[2];

                        key = key.Split('_')[0];

                        string propName = nameof(Utility.User.UniqueKey);
                        Response responseUList = IUserService.GetDataByPropertyName(propName, key, true, 0, 0, false);
                        if (responseUList.IsSuccess)
                        {
                            List<User> users = ((List<User>)responseUList.Data);

                            if (users.Count > 0)
                            {
                                User user = ((List<User>)responseUList.Data).FirstOrDefault();
                                if (user.EmailCode == emailCode)
                                {
                                    UserSecurity uS = user.UserIdUserSecurities.Where(x => x.UserSecurityTypeId == 1).FirstOrDefault();
                                    uS.IsVerified = 1;
                                    uS.VerifiedDate = DateTime.Now;
                                    IUserSecurityService.UpdateData(uS);

                                    UserLogMain ulm = new UserLogMain()
                                    {
                                        Description = "Verified email (" + user.EmailId + ") by activation link",
                                        TableName = "user_securities",
                                        UpdatedDate = DateTime.Now,
                                        UserId = user.Id,
                                        UserAccountId = 0,
                                        RefId = uS.Id,
                                        RefValue = uS.Value,
                                        StatusType = 1
                                    };
                                    Response responseLog = IUserLogMainService.PostData(ulm);
                                    if (!responseLog.IsSuccess)
                                    {
                                        //Audit log failed
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                                    }

                                    Response.Redirect("/SetPassword.aspx?key=" + key, false);
                                }
                                else
                                {
                                    UserSecurity uS = user.UserIdUserSecurities.Where(x => x.UserSecurityTypeId == 1).FirstOrDefault();
                                    UserLogMain ulm = new UserLogMain()
                                    {
                                        Description = "Verification email (" + user.EmailId + ") link expired",
                                        TableName = "user_securities",
                                        UpdatedDate = DateTime.Now,
                                        UserId = user.Id,
                                        UserAccountId = 0,
                                        RefId = uS.Id,
                                        RefValue = uS.Value,
                                        StatusType = 0
                                    };
                                    Response responseLog = IUserLogMainService.PostData(ulm);
                                    //if (!responseLog.IsSuccess)
                                    //{
                                    //    //Audit log failed
                                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                                    //}
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Link expired!', 'Index.aspx');", true);
                                }
                            }
                            else
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Broken link!', 'Index.aspx');", true);

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUList.Message + "\", '');", true);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }
    }
}