﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class OpenAgentAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            if (txtIntroCode.Text == "" || txtUplineCode.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"Please fill the details.\", '');", true);
            }
            else
            {
                //Response.Redirect("/AccountOpening.aspx");
            }
        }
    }
}