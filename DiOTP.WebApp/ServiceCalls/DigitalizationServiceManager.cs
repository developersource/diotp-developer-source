﻿using DiOTP.Utility.CustomClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace DiOTP.WebApp.ServiceCalls
{
    public static class DigitalizationServiceManager
    {
        public static string baseURL = "https://gateway.trulioo.com";
        public static string apiKey = "b0a149684916b389b9c66c485d1f0e29";

        public static Response CallApi(string url, HttpMethod httpMethod, StringContent stringContent = null)
        {
            Response response = new Response();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("x-trulioo-api-key", apiKey);
                    var responseApi = new HttpResponseMessage();
                    if (httpMethod == HttpMethod.Get)
                        responseApi = client.GetAsync(url).Result;
                    else if (httpMethod == HttpMethod.Post)
                        responseApi = client.PostAsync(url, stringContent).Result;
                    if (responseApi.IsSuccessStatusCode)
                    {
                        string responseString = responseApi.Content.ReadAsStringAsync().Result;
                        response.IsSuccess = true;
                        response.Data = responseString;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = responseApi.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public static Response TestAuth()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            Response response = new Response();
            try
            {
                ///connection/v1/testauthentication
                Response responseApi = CallApi("/trial/connection/v1/testauthentication", HttpMethod.Get);
                if (responseApi.IsSuccess)
                {
                    String res = Newtonsoft.Json.JsonConvert.DeserializeObject<String>(responseApi.Data.ToString());
                    //Hello JoeNapoli_API_Demo
                    response.Data = res;
                }
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public static Response GetCountryCodes()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            Response response = new Response();
            try
            {
                ///configuration/v1/countrycodes/{configurationName}
                Response responseApi = CallApi("/trial/configuration/v1/countrycodes/Identity Verification", HttpMethod.Get);
                if (responseApi.IsSuccess)
                {
                    List<String> res = Newtonsoft.Json.JsonConvert.DeserializeObject<List<String>>(responseApi.Data.ToString());
                    response.Data = res;
                    /*
                     [
                      "AU",
                      "CA",
                      "CN",
                      "IN",
                      "US",
                      "GB",
                      "IT",
                      "MY",
                      "NZ",
                      "BE"
                    ]
                     */
                }
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public static Response GetTestEntities()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            Response response = new Response();
            try
            {
                ///configuration/v1/testentities/{configurationName}/{countryCode}
                Response responseApi = CallApi("/trial/configuration/v1/testentities/Identity Verification/MY", HttpMethod.Get);
                if (responseApi.IsSuccess)
                {
                    List<TestEntity> res = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TestEntity>>(responseApi.Data.ToString());
                    response.Data = res;
                    /*
                        [
                            {
                            "PersonInfo": {
                                "FirstGivenName": "John",
                                "MiddleName": "Henry",
                                "FirstSurName": "Smith",
                                "DayOfBirth": "5",
                                "MonthOfBirth": "3",
                                "YearOfBirth": "1983",
                                "Gender": "M"
                            },
                            "Location": {
                                "BuildingNumber": "10",
                                "UnitNumber": "3",
                                "StreetName": "Lawford",
                                "StreetType": "st",
                                "Suburb": "Doncaster",
                                "StateProvinceCode": "VIC",
                                "PostalCode": "3108"
                            },
                            "Communication": {
                                "Telephone": "03 9896 8785",
                                "EmailAddress": "testpersonAU@gdctest.com"
                            },
                            "DriverLicence": {
                                "Number": "076310691",
                                "State": "VIC",
                                "DayOfExpiry": "3",
                                "MonthOfExpiry": "4",
                                "YearOfExpiry": "2021"
                            },
                            "NationalIds": [
                                {
                                "Number": "5643513953",
                                "Type": "Health"
                                }
                            ],
                            "Passport": {
                                "Mrz1": "P<SAGSMITH<<JOHN<HENRY<<<<<<<<<<<<<<<<<<<<<<",
                                "Mrz2": "N1236548<1AUS8303052359438740809<<<<<<<<<<54",
                                "Number": "N1236548",
                                "DayOfExpiry": 5,
                                "MonthOfExpiry": 12,
                                "YearOfExpiry": 2018
                            },
                            "CountrySpecific": {
                                "AU": {
                                "AuImmiCardNumber": "EIS123456",
                                "CitizenshipAcquisitionDay": "15",
                                "CitizenshipAcquisitionMonth": "4",
                                "CitizenshipAcquisitionYear": "1987",
                                "CountryOfBirth": "Australia",
                                "FamilyNameAtBirth": "Smith",
                                "MedicareColor": "Blue",
                                "MedicareMonthOfExpiry": "12",
                                "MedicareReference": "2",
                                "MedicareYearOfExpiry": "2017",
                                "PassportCountry": "Australia",
                                "PlaceOfBirth": "Melbourne",
                                "RegistrationNumber": "565659",
                                "RegistrationState": "NSW",
                                "StockNumber": "ACD1234567"
                                }
                            }
                            }
                        ]
                     */
                }
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public static Response GetConsents()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            Response response = new Response();
            try
            {
                ///configuration/v1/testentities/{configurationName}/{countryCode}
                Response responseApi = CallApi("/trial/configuration/v1/consents/Identity Verification/MY", HttpMethod.Get);
                if (responseApi.IsSuccess)
                {
                    List<String> res = Newtonsoft.Json.JsonConvert.DeserializeObject<List<String>>(responseApi.Data.ToString());
                    response.Data = res;
                    /*
                        [
                          "Australia Driver Licence",
                          "Australia Passport",
                          "Birth Registry",
                          "Visa Verification",
                          "DVS Driver License Search",
                          "DVS Medicare Search",
                          "DVS Passport Search",
                          "DVS Visa Search",
                          "DVS ImmiCard Search",
                          "DVS Citizenship Certificate Search",
                          "DVS Certificate of Registration by Descent Search",
                          "Credit Agency"
                        ]
                     */
                }
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public static Response Verify(Trulioo.Client.V1.Model.VerifyRequest verifyRequestBody)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            Response response = new Response();
            try
            {
                ///configuration/v1/testentities/{configurationName}/{countryCode}
                /*Request body
                     {
                      "AcceptTruliooTermsAndConditions": true,
                      "CleansedAddress": false,
                      "ConfigurationName": "Identity Verification",
                      "ConsentForDataSources": [
                        "Visa Verification"
                      ],
                      "CountryCode": "AU",
                      "DataFields": {
                        "PersonInfo": {
                          "FirstGivenName": "J",
                          "FirstSurName": "Smith",
                          "MiddleName": "Henry",
                          "DayOfBirth": 5,
                          "MonthOfBirth": 3,
                          "YearOfBirth": 1983,
                          "MinimumAge": 0
                        },
                        "Location": {
                          "BuildingNumber": "10",
                          "PostalCode": "3108",
                          "StateProvinceCode": "VIC",
                          "StreetName": "Lawford",
                          "StreetType": "St",
                          "Suburb": "Doncaster",
                          "UnitNumber": "3"
                        },
                        "Communication": {
                          "EmailAddress": "testpersonAU%40gdctest.com",
                          "Telephone": "03 9896 8785"
                        },
                        "Passport": {
                          "Number": "N1236548"
                        }
                      }
                    }
                 */
                //VerifyRequestBody verifyRequestBody = new VerifyRequestBody
                //{
                //    AcceptTruliooTermsAndConditions = true,
                //    CleansedAddress = false,
                //    ConfigurationName = "Identity Verification",
                //    ConsentForDataSources = new List<string>() { "Visa Verification" },
                //    CountryCode = "MY",
                //    DataFields = new DataFields
                //    {
                //        PersonInfo = new PersonInfo
                //        {
                //            FirstGivenName = "Leo",
                //            FirstSurName = "Moggie",
                //            DayOfBirth = 1,
                //            MonthOfBirth = 10,
                //            YearOfBirth = 1941
                //        },
                //        Location = new Location
                //        {

                //        },
                //        Communication = new Communication
                //        {

                //        },
                //        Passport = new Passport
                //        {

                //        },
                //        NationalIds = new List<NationalId> { new NationalId { Number = "411001-13-5027", Type = "nationalid" } }
                //    }
                //};
                String requestJson = JsonConvert.SerializeObject(verifyRequestBody);
                Response responseApi = CallApi("/trial/verifications/v1/verify", HttpMethod.Post, new StringContent(requestJson));
                if (responseApi.IsSuccess)
                {
                    Trulioo.Client.V1.Model.VerifyResult res = Newtonsoft.Json.JsonConvert.DeserializeObject<Trulioo.Client.V1.Model.VerifyResult>(responseApi.Data.ToString());
                    //List<Response> responses = new List<Response>();
                    //res.Record.DatasourceResults.ForEach(x =>
                    //{
                    //    x.DatasourceFields.ForEach(y =>
                    //    {
                    //        if (y.Status == "nomatch")
                    //        {
                    //            responses.Add(new Response
                    //            {
                    //                IsSuccess = false,
                    //                Message = y.FieldName
                    //            });
                    //        }
                    //    });
                    //});
                    if (res.Errors.Count() > 0)
                    {
                        response.IsSuccess = false;
                        response.Message = String.Join(", ", res.Errors.Select(x => x.Message).ToArray());
                    }
                    else
                    {
                        response.IsSuccess = true;
                        response.Data = res;
                    }
                    /*
                        {
                          "TransactionID": "c03ad3c9-da6b-452c-8a2e-fd1923973b99",
                          "UploadedDt": "2015-02-19T13:25:50.000Z",
                          "CountryCode": null,
                          "ProductName": null,
                          "Record": {
                            "TransactionRecordID": "c47d485d-005a-4e0c-8ef0-06f16ddbaeb6",
                            "RecordStatus": "match",
                            "DatasourceResults": [
                              {
                                "DatasourceStatus": "match",
                                "DatasourceName": "Consumer Intl",
                                "DatasourceFields": [
                                  {
                                    "FieldName": "FirstSurName",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "FirstGivenName",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "City",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "County",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "BuildingName",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "BuildingNumber",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "PostalCode",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "StreetName",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "StreetType",
                                    "Status": "match",
                                    "FieldGroup": null
                                  }
                                ],
                                "AppendedFields": [],
                                "Errors": [],
                                "FieldGroups": []
                              },
                              {
                                "DatasourceStatus": "match",
                                "DatasourceName": "Resident Files",
                                "DatasourceFields": [
                                  {
                                    "FieldName": "FirstSurName",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "FirstGivenName",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "BuildingName",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "BuildingNumber",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "City",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "County",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "StreetName",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "StreetType",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "PostalCode",
                                    "Status": "match",
                                    "FieldGroup": null
                                  }
                                ],
                                "AppendedFields": [],
                                "Errors": [],
                                "FieldGroups": []
                              },
                              {
                                "DatasourceStatus": "match",
                                "DatasourceName": "International Watchlist",
                                "DatasourceFields": [
                                  {
                                    "FieldName": "FirstGivenName",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "MiddleName",
                                    "Status": "match",
                                    "FieldGroup": null
                                  },
                                  {
                                    "FieldName": "FirstSurName",
                                    "Status": "match",
                                    "FieldGroup": null
                                  }
                                ],
                                "AppendedFields": [],
                                "Errors": [],
                                "FieldGroups": []
                              }
                            ],
                            "Errors": [
                              {
                                "Code": "1008",
                                "Message": "Field format invalid: FirstInitial"
                              }
                            ],
                            "Rule": null
                          },
                          "CustomerReferenceID": "abc-1234",
                          "Errors": []
                        }
                     */
                }
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        //Documents
        public static Response GetDocumentTypes()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            Response response = new Response();
            try
            {
                ///configuration/v1/testentities/{configurationName}/{countryCode}
                Response responseApi = CallApi("/trial/configuration/v1/documentTypes/MY", HttpMethod.Get);
                if (responseApi.IsSuccess)
                {
                    DocumentType res = Newtonsoft.Json.JsonConvert.DeserializeObject<DocumentType>(responseApi.Data.ToString());
                    response.Data = res;
                    /*
                        [
                            {
                            "PersonInfo": {
                                "FirstGivenName": "John",
                                "MiddleName": "Henry",
                                "FirstSurName": "Smith",
                                "DayOfBirth": "5",
                                "MonthOfBirth": "3",
                                "YearOfBirth": "1983",
                                "Gender": "M"
                            },
                            "Location": {
                                "BuildingNumber": "10",
                                "UnitNumber": "3",
                                "StreetName": "Lawford",
                                "StreetType": "st",
                                "Suburb": "Doncaster",
                                "StateProvinceCode": "VIC",
                                "PostalCode": "3108"
                            },
                            "Communication": {
                                "Telephone": "03 9896 8785",
                                "EmailAddress": "testpersonAU@gdctest.com"
                            },
                            "DriverLicence": {
                                "Number": "076310691",
                                "State": "VIC",
                                "DayOfExpiry": "3",
                                "MonthOfExpiry": "4",
                                "YearOfExpiry": "2021"
                            },
                            "NationalIds": [
                                {
                                "Number": "5643513953",
                                "Type": "Health"
                                }
                            ],
                            "Passport": {
                                "Mrz1": "P<SAGSMITH<<JOHN<HENRY<<<<<<<<<<<<<<<<<<<<<<",
                                "Mrz2": "N1236548<1AUS8303052359438740809<<<<<<<<<<54",
                                "Number": "N1236548",
                                "DayOfExpiry": 5,
                                "MonthOfExpiry": 12,
                                "YearOfExpiry": 2018
                            },
                            "CountrySpecific": {
                                "AU": {
                                "AuImmiCardNumber": "EIS123456",
                                "CitizenshipAcquisitionDay": "15",
                                "CitizenshipAcquisitionMonth": "4",
                                "CitizenshipAcquisitionYear": "1987",
                                "CountryOfBirth": "Australia",
                                "FamilyNameAtBirth": "Smith",
                                "MedicareColor": "Blue",
                                "MedicareMonthOfExpiry": "12",
                                "MedicareReference": "2",
                                "MedicareYearOfExpiry": "2017",
                                "PassportCountry": "Australia",
                                "PlaceOfBirth": "Melbourne",
                                "RegistrationNumber": "565659",
                                "RegistrationState": "NSW",
                                "StockNumber": "ACD1234567"
                                }
                            }
                            }
                        ]
                     */
                }
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public static Response GetTransactionRecord(String ExperienceTransactionId, String TransactionRecordId)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            Response response = new Response();
            try
            {
                ///configuration/v1/testentities/{configurationName}/{countryCode}
                Response responseApi = CallApi("/trial/verifications/v1/transactionrecord/" + TransactionRecordId, HttpMethod.Get);
                if (responseApi.IsSuccess)
                {
                    Trulioo.Client.V1.Model.TransactionRecordResult res = Newtonsoft.Json.JsonConvert.DeserializeObject<Trulioo.Client.V1.Model.TransactionRecordResult>(responseApi.Data.ToString());
                    response.Data = res;
                }
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }


    public class TestEntity
    {
        public PersonInfo PersonInfo { get; set; }
        public Location Location { get; set; }
        public Communication Communication { get; set; }
        public DriverLicence DriverLicence { get; set; }
        public List<NationalId> NationalIds { get; set; }
        public Passport Passport { get; set; }
        public CountrySpecific CountrySpecific { get; set; }
    }

    public class PersonInfo
    {
        public String FirstGivenName { get; set; }
        public String MiddleName { get; set; }
        public String FirstSurName { get; set; }
        public Int32 DayOfBirth { get; set; }
        public Int32 MonthOfBirth { get; set; }
        public Int32 YearOfBirth { get; set; }
        public String Gender { get; set; }
    }

    public class Location
    {
        public String BuildingNumber { get; set; }
        public String UnitNumber { get; set; }
        public String StreetName { get; set; }
        public String StreetType { get; set; }
        public String Suburb { get; set; }
        public String StateProvinceCode { get; set; }
        public String PostalCode { get; set; }

    }

    public class Communication
    {
        public String Telephone { get; set; }
        public String EmailAddress { get; set; }

    }

    public class DriverLicence
    {
        public String Number { get; set; }
        public String State { get; set; }
        public String DayOfExpiry { get; set; }
        public String MonthOfExpiry { get; set; }
        public String YearOfExpiry { get; set; }

    }

    public class NationalId
    {
        public String Number { get; set; }
        public String Type { get; set; }

    }

    public class Passport
    {
        public String Mrz1 { get; set; }
        public String Mrz2 { get; set; }
        public String Number { get; set; }
        public String DayOfExpiry { get; set; }
        public String MonthOfExpiry { get; set; }
        public String YearOfExpiry { get; set; }

    }

    public class Document
    {
        public Byte[] DocumentFrontImage { get; set; }
        public Byte[] DocumentBackImage { get; set; }
        public Byte[] LivePhoto { get; set; }
        public String DocumentType { get; set; }
        public Boolean ValidateDocumentImageQuality { get; set; }
    }

    public class CountrySpecific
    {
        public String AU { get; set; }
        public String AuImmiCardNumber { get; set; }
        public String CitizenshipAcquisitionDay { get; set; }
        public String CitizenshipAcquisitionMonth { get; set; }
        public String CitizenshipAcquisitionYear { get; set; }
        public String CountryOfBirth { get; set; }
        public String FamilyNameAtBirth { get; set; }
        public String MedicareColor { get; set; }
        public String MedicareMonthOfExpiry { get; set; }
        public String MedicareReference { get; set; }
        public String MedicareYearOfExpiry { get; set; }
        public String PassportCountry { get; set; }
        public String PlaceOfBirth { get; set; }
        public String RegistrationNumber { get; set; }
        public String RegistrationState { get; set; }
        public String StockNumber { get; set; }

    }


    public class VerifyRequestBody
    {
        public Boolean AcceptTruliooTermsAndConditions { get; set; }
        public Boolean CleansedAddress { get; set; }
        public String ConfigurationName { get; set; }
        public List<String> ConsentForDataSources { get; set; }
        public String CountryCode { get; set; }
        public DataFields DataFields { get; set; }
    }

    public class DataFields
    {
        public PersonInfo PersonInfo { get; set; }
        public Document Document { get; set; }
        public Location Location { get; set; }
        public Communication Communication { get; set; }
        public Passport Passport { get; set; }
        public List<NationalId> NationalIds { get; set; }
    }

    public class VerifyResponse
    {
        public String TransactionID { get; set; }
        public String UploadedDt { get; set; }
        public String CountryCode { get; set; }
        public String ProductName { get; set; }
        public Record Record { get; set; }
        public List<Error> Errors { get; set; }
    }

    public class Record
    {
        public String TransactionRecordID { get; set; }
        public String RecordStatus { get; set; }
        public List<DatasourceResult> DatasourceResults { get; set; }
        public List<Error> Errors { get; set; }
        public Rule Rule { get; set; }

    }

    public class DatasourceResult
    {
        public String DatasourceName { get; set; }
        public List<DatasourceField> DatasourceFields { get; set; }
        public List<String> AppendedFields { get; set; }
        public List<Error> Errors { get; set; }
        public List<String> FieldGroups { get; set; }

    }

    public class DatasourceField
    {
        public String FieldName { get; set; }
        public String Status { get; set; }

    }

    public class Error
    {
        public String Code { get; set; }
        public String Message { get; set; }
    }

    public class Rule
    {
        public String RuleName { get; set; }
        public String Note { get; set; }

    }


    //Documents
    public class DocumentType
    {
        public List<String> MY { get; set; }
    }
}