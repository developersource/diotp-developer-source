﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace DiOTP.WebApp.ServiceCalls
{
    public class LanguageManager
    {
        private static readonly Lazy<ILanguageDirService> lazyLanguageDirService = new Lazy<ILanguageDirService>(() => new LanguageDirService());

        public static ILanguageDirService ILanguageDirService { get { return lazyLanguageDirService.Value; } }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetLanguageDirByModule(int module)
        {
            StringBuilder filter = new StringBuilder();
            filter.Append(" " + (Converter.GetColumnNameByPropertyName<LanguageDir>(nameof(LanguageDir.Module))) + "='"+ module + "'");
            Response responseLDList = ILanguageDirService.GetDataByFilter(filter.ToString(), 0, 0, false);
            if (responseLDList.IsSuccess)
            {
                List<LanguageDir> languageDirectory = (List<LanguageDir>)responseLDList.Data;
                return languageDirectory;
            }
            return null;
        }

        public enum DiOTPModule
        {
            UID = 0,
            FUND = 1,
            TRANSACTION = 2,
            STATEMENT = 3
        }
    }
}