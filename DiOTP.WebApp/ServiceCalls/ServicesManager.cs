﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using static DiOTP.Utility.CustomClasses.CustomStatus;

namespace DiOTP.WebApp.ServiceCalls
{
    public class ServicesManager
    {

        public static Response CheckConnection()
        {
            Response response = new Response();
            try
            {
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response1 = client.GetAsync("api/HolderLedgerApi/Get?filter=check").Result;
                    if (response1.IsSuccessStatusCode)
                    {
                        //string responseString = response1.Content.ReadAsStringAsync().Result;
                        var modelObject = response1.Content.ReadAsAsync<Response>().Result;
                        return modelObject;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.IsApiAvailable = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public static Response responseCustom = new Response();

        public static Response GetMaHolderRegByAccountNo(string accNo)
        {
            Response responseC = CheckConnection();
            if (responseC.IsSuccess)
            {
                MaHolderReg maHolderReg = new MaHolderReg();
                try
                {
                    using (var client = new HttpClient())
                    {
                        string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                        client.BaseAddress = new Uri(baseURL);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                        StringBuilder filter = new StringBuilder();
                        filter.Append(" left join UTS.ADD_INFO a on a.INFO_NUMBER = h.HOLDER_NO and a.REF_CODE='09' where h.holder_no = '" + accNo + "'");
                        var response = client.GetAsync("api/MaHolderRegApi/Get?filter=" + filter.ToString()).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            string responseString = response.Content.ReadAsStringAsync().Result;
                            Response responseFromAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                            if (responseFromAPI.IsSuccess)
                            {
                                List<MaHolderReg> maHolderRegs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MaHolderReg>>(responseFromAPI.Data.ToString());
                                maHolderReg = maHolderRegs.FirstOrDefault();
                                if (maHolderReg != null)
                                {
                                    responseCustom.IsSuccess = true;
                                    responseCustom.Data = maHolderReg;
                                }
                                else
                                {
                                    responseCustom.IsSuccess = false;
                                    responseCustom.Message = accNo + " MA doen't exist.";
                                    Logger.WriteLog("Portfolio Page VerifyLogin " + accNo + " MA doen't exist.");

                                }
                            }
                            else
                            {
                                responseCustom = responseFromAPI;
                            }
                        }
                        else
                        {
                            responseCustom.IsSuccess = false;
                            responseCustom.Message = response.ReasonPhrase;
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseCustom.IsSuccess = false;
                    responseCustom.Message = ex.Message;
                }
            }
            else
            {
                responseCustom = responseC;
            }
            return responseCustom;
        }

        public static Response GetMaHolderRegsByIdNo(string IdNo)
        {
            List<MaHolderReg> maHolderRegs = new List<MaHolderReg>();
            try
            {
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    StringBuilder filter = new StringBuilder();
                    //filter.Append(" and id_no_2 not in ('STA', 'STA  2', 'STA 2', 'STA 2 (10JF)', 'STA 3', 'STA2', 'STA3')");
                    //filter.Append(" and id_no_ol not in ('STA2', '10 JF', 'STA2')");
                    //filter.Append(" and id_no_3 not in ('10JF', 'STA 3', 'STA3')");
                    filter.Append(" left join UTS.ADD_INFO a on a.INFO_NUMBER = h.HOLDER_NO and a.REF_CODE='09' where h.id_no = '" + IdNo + "'");
                    filter.Append(" or h.id_no_old = '" + IdNo + "'");
                    filter.Append(" or h.id_no_2 = '" + IdNo + "'");
                    filter.Append(" or h.id_no_old_2 = '" + IdNo + "'");
                    filter.Append(" or h.id_no_3 = '" + IdNo + "'");
                    filter.Append(" or h.id_no_old_3 = '" + IdNo + "'");
                    filter.Append(" or h.id_no_4 = '" + IdNo + "'");
                    filter.Append(" or h.id_no_old_4 = '" + IdNo + "'");
                    filter.Append(" or h.id_no_5 = '" + IdNo + "'");
                    filter.Append(" or h.id_no_old_5 = '" + IdNo + "'");
                    var response = client.GetAsync("api/MaHolderRegApi/Get?filter=" + filter.ToString()).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        Response responseFromAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                        if (responseFromAPI.IsSuccess)
                        {
                            maHolderRegs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MaHolderReg>>(responseFromAPI.Data.ToString());
                            responseCustom.IsSuccess = true;
                            responseCustom.Data = maHolderRegs;
                        }
                        else
                        {
                            responseCustom = responseFromAPI;
                        }
                        //Newtonsoft.Json.Linq.JObject json = Newtonsoft.Json.Linq.JObject.Parse(responseString);
                    }
                    else
                    {
                        responseCustom.IsSuccess = false;
                        responseCustom.Message = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static string UpdateMaHolderRegByHolderNo(MaHolderReg obj)
        {
            MaHolderReg maHolderReg = new MaHolderReg();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpContent content = new StringContent(obj.ToString(), Encoding.UTF8, "application/json");
                var response = client.PostAsJsonAsync("api/MaHolderRegApi/PostSingle", obj).Result;
                string responseString = "";
                if (response.IsSuccessStatusCode)
                {
                    responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JObject json = Newtonsoft.Json.Linq.JObject.Parse(responseString);
                    //maHolderReg = Newtonsoft.Json.JsonConvert.DeserializeObject<MaHolderReg>(responseString);
                }
                return responseString;
            }
        }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        public static List<UtmcDetailedMemberInvestment> GetMAAccountSummary(string holderNo, List<UtmcFundInformation> UTMCFundInformations)
        {
            List<UtmcDetailedMemberInvestment> objs = new List<UtmcDetailedMemberInvestment>();

            DateTime currentDate = DateTime.Now;
            //DateTime yearsBefore = currentDate.AddYears(-10);
            DateTime yearsBefore = new DateTime(2000, 1, 1);

            List<HolderInv> holderInvs = GetHolderInvByHolderNo(holderNo, UTMCFundInformations).Where(x => x.CurrUnitHldg > 0).ToList();
            if (holderInvs.Count != 0)
            {
                //string filterDate = DateTime.Now.AddYears(-3).ToString("yyyy/MM/dd");
                List<HolderLedger> holderLedgers = GetHolderLedgerByHolderNo(" select * from uts.holder_ledger where TRANS_TYPE NOT IN ('CO','UC','RC','SC','MC') and HOLDER_NO='" + holderNo + "' and FUND_ID in (" + String.Join(",", holderInvs.Select(x => "'" + x.FundId + "'").ToArray()) + ") ORDER By TRANS_DT desc ");

                //List<UtmcMemberInvestment> utmcMemberInvestments = GetMemberInvestmentsByHolderNo(holderNo, yearsBefore.ToString("yyyy-MM-dd"), currentDate.ToString("yyyy-MM-dd"), String.Join(",", holderInvs.Select(x => "'" + x.FundId + "'").ToArray()));

                //List<UtmcCompositionalTransaction> utmcCompositionalTransactions = GetCompositionalTransactionByHolderNo(holderNo, yearsBefore.ToString("yyyy-MM-dd"), currentDate.ToString("yyyy-MM-dd"));
                //List<FundDetailedInformation> fundDetailedInformations = IUtmcFundInformationService.GetFundDetailedInformation();

                foreach (string fundCode in holderInvs.Select(x => x.FundId).ToList())
                {
                    //UTMCFundInformations.ForEach(x=> {
                    //    x.UtmcDailyNavFundsWithChanges.Clear();
                    //    x.UtmcDailyNavFundWithChangeMonthEnd.Clear();
                    //    x.FundDetailsAfterDividend.utmcDailyNavFundAfterDividends.Clear();
                    //    x.UtmcFundInformation.UtmcFundInformationIdUtmcFundCharges.Clear();
                    //});

                    UtmcDetailedMemberInvestment utmcDetailedMemberInvestment = new UtmcDetailedMemberInvestment
                    {
                        holderInv = holderInvs.Where(x => x.FundId == fundCode).FirstOrDefault(),
                        //utmcMemberInvestments = utmcMemberInvestments.Where(x => x.IpdFundCode == fundCode).OrderByDescending(x => x.EffectiveDate).ToList(),
                        //--utmcCompositionalTransactions = utmcCompositionalTransactions.Where(x => x.IpdFundCode == fundCode).OrderByDescending(x => x.DateOfTransaction).ToList(),
                        //--utmcMemberInvestments = utmcMemberInvestments.Where(x => x.ReportDate == utmcMemberInvestments.Max(y => y.ReportDate)).Where(x => x.IpdFundCode == fundCode).ToList(),
                        utmcFundInformation = UTMCFundInformations.Where(x => x.IpdFundCode == fundCode).FirstOrDefault(),
                        holderLedgers = holderLedgers.Where(x => x.FundID == fundCode).ToList()
                    };
                    objs.Add(utmcDetailedMemberInvestment);
                }
            }
            return objs;
        }

        public static List<HolderInv> GetHolderInvByHolderNo(string holderNo, List<UtmcFundInformation> UTMCFundInformations)
        {
            if (UTMCFundInformations.Count == 0)
            {
                Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                if (responseUFIList.IsSuccess)
                {
                    UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                }
            }
            string fundIds = String.Join(",", UTMCFundInformations.Select(x => "'" + x.IpdFundCode + "'").ToList());
            List<HolderInv> holderInvs = new List<HolderInv>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/HolderInvApi?holderNo=" + holderNo + "&fundIds=" + fundIds).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    holderInvs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<HolderInv>>(responseFromApi.Data.ToString());
                }
            }

            List<HolderFundDiv> holderFundDivs = GetHolderFundDivByHolderNo(holderNo, UTMCFundInformations);

            holderInvs.ForEach(hi =>
            {
                HolderFundDiv holderFundDiv = holderFundDivs.Where(x => x.FundId == hi.FundId).FirstOrDefault();
                if (holderFundDiv != null)
                {
                    DistributionIns distributionIns = (DistributionIns)(Convert.ToInt32(holderFundDiv.DivPymt));
                    hi.isDisFromOracle = 1;
                    hi.DistributionIns = (Convert.ToInt32(holderFundDiv.DivPymt));
                    hi.DistributionInsString = distributionIns.ToString();
                }
                else
                {
                    hi.isDisFromOracle = 0;
                    hi.DistributionIns = (Int32)(DistributionIns.Reinvest);
                    hi.DistributionInsString = DistributionIns.Reinvest.ToString();
                }
            });
            holderInvs = holderInvs.OrderBy(x => x.FundId).ToList();

            return holderInvs;
        }

        public static List<UtmcMemberInvestment> GetMemberInvestmentsByHolderNo(string holderNo, string startDate, string endDate, string fundIds)
        {
            List<UtmcMemberInvestment> utmcMemberInvestments = new List<UtmcMemberInvestment>();
            if (fundIds != "")
            {
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync("api/UtmcMemberInvestmentApi?holderNo=" + holderNo + "&startDate=" + startDate + "&endDate=" + endDate + "&fundIds=" + fundIds).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                        utmcMemberInvestments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcMemberInvestment>>(responseString);
                    }
                }
            }
            return utmcMemberInvestments;
        }

        public static List<UtmcCompositionalTransaction> GetCompositionalTransactionByHolderNo(string holderNo)
        {
            List<UtmcCompositionalTransaction> utmcCompositionalTransactions = new List<UtmcCompositionalTransaction>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/UtmcCompositionalTransactionApi?holderNo=" + holderNo).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    utmcCompositionalTransactions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcCompositionalTransaction>>(responseString);
                }
            }
            return utmcCompositionalTransactions;
        }

        public static List<UtmcCompositionalTransaction> GetCompositionalTransactionByHolderNo(string holderNo, string startDate, string endDate)
        {
            List<UtmcCompositionalTransaction> utmcCompositionalTransactions = new List<UtmcCompositionalTransaction>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/UtmcCompositionalTransactionApi?holderNo=" + holderNo + "&startDate=" + startDate + "&endDate=" + endDate).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    utmcCompositionalTransactions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UtmcCompositionalTransaction>>(responseString);
                }
            }
            return utmcCompositionalTransactions;
        }

        public static Boolean IsImpersonated(HttpContext Context)
        {
            Boolean IsImpersonated = false;
            if (Context.Session["IsImpersonated"] != null)
            {
                IsImpersonated = (Boolean)Context.Session["IsImpersonated"];
            }
            return IsImpersonated;
        }

        public static Response GetAdditionalInfoByHolderNo(string holderNo)
        {
            Response responseAddInfo = new Response();
            try
            {
                List<AddInfo> addInfos = new List<AddInfo>();
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync("api/AddInfoApi?holderNo=" + holderNo).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                        Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                        addInfos = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AddInfo>>(responseFromApi.Data.ToString());
                        responseAddInfo.IsSuccess = true;
                        responseAddInfo.Data = addInfos;
                    }
                }
            }
            catch (Exception ex)
            {
                responseAddInfo.IsSuccess = false;
                responseAddInfo.Message = ex.Message;
            }
            return responseAddInfo;
        }

        public static List<HolderLedger> GetHolderLedgerByHolderNo(string filter)
        {
            List<HolderLedger> holderLedgers = new List<HolderLedger>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/HolderLedgerApi?filter=" + filter).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    holderLedgers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<HolderLedger>>(responseFromApi.Data.ToString());
                }
            }
            return holderLedgers;
        }


        public static List<HolderFundDiv> GetHolderFundDivByHolderNo(string holderNo, List<UtmcFundInformation> UTMCFundInformations)
        {
            if (UTMCFundInformations == null)
            {
                Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                if (responseUFIList.IsSuccess)
                {
                    UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                }
            }
            string fundIds = String.Join(",", UTMCFundInformations.Select(x => "'" + x.IpdFundCode + "'").ToList());
            List<HolderFundDiv> holderFundDivs = new List<HolderFundDiv>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/HolderFundDivApi?holderNo=" + holderNo + "&fundIds=" + fundIds).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    holderFundDivs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<HolderFundDiv>>(responseFromApi.Data.ToString());
                }
            }
            return holderFundDivs;
        }


        public static List<CurrentNAVDTO> GetcurrentNAV(string fundIdsString)
        {
            List<CurrentNAVDTO> CurrentNAVDTOs = new List<CurrentNAVDTO>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/NAVApi/Get?fundIdsString=" + fundIdsString).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    CurrentNAVDTOs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CurrentNAVDTO>>(responseFromApi.Data.ToString());
                }
            }
            return CurrentNAVDTOs;
        }

        public static List<CurrentNAVDTO> GetNAVHistoryByFund(string fundIdsString, int noOfMonths)
        {
            List<CurrentNAVDTO> CurrentNAVDTOs = new List<CurrentNAVDTO>();
            using (var client = new HttpClient())
            {
                string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("api/NAVApi/GetNAVHistory?fundIdsString=" + fundIdsString + "&noOfMonths=" + noOfMonths).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    //Newtonsoft.Json.Linq.JArray json = Newtonsoft.Json.Linq.JArray.Parse(responseString);
                    Response responseFromApi = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                    CurrentNAVDTOs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CurrentNAVDTO>>(responseFromApi.Data.ToString());
                }
            }
            return CurrentNAVDTOs;
        }

        public static Response CheckIfSATUpdated(UserAccount primaryAcc)
        {
            Response responseSAT = new Response();
            bool isSATUpdated = false;
            try
            {

                if (primaryAcc.IsSatChecked == 0)
                    isSATUpdated = false;
                if (primaryAcc.SatUpdatedDate != null)
                {
                    DateTime SatUpdatedDate = primaryAcc.SatUpdatedDate.Value;
                    int yearDays = 365;
                    if (DateTime.IsLeapYear(SatUpdatedDate.Year))
                        yearDays = 366;

                    if ((DateTime.Now - SatUpdatedDate).TotalDays > yearDays)
                        isSATUpdated = false;
                    else
                        isSATUpdated = true;
                }
                else
                    isSATUpdated = false;
                responseSAT.IsSuccess = true;
            }
            catch (Exception ex)
            {
                responseSAT.IsSuccess = false;
                responseSAT.Message = ex.Message;
                Logger.WriteLog("ServicesManager CheckIfSATUpdated: " + ex.Message);
            }

            responseSAT.Data = isSATUpdated;

            return responseSAT;
        }

        //AO
        public static Response GetDataByQuery(string query)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    string baseURL = ConfigurationManager.AppSettings["apiBaseURL"].ToString();
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync("api/HolderLedgerApi/Get?filter=" + query).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        Response responseFromAPI = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(responseString);
                        if (responseFromAPI.IsSuccess)
                        {
                            responseCustom.IsSuccess = true;
                            responseCustom.Data = responseFromAPI.Data;
                        }
                        else
                        {
                            responseCustom = responseFromAPI;
                        }
                    }
                    else
                    {
                        responseCustom.IsSuccess = false;
                        responseCustom.Message = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static Response GetMonthlyIncomeDescData(String incCode)
        {
            try
            {
                Response response = GetDataByQuery("select * from uts.income where income_code ='" + incCode + "'");
                if (response.IsSuccess)
                {
                    List<IncomeDTO> incomeDTOs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<IncomeDTO>>(response.Data.ToString());


                    incomeDTOs.ForEach(x =>
                    {
                        x.DOWNLOADIND = x.INCOMECODE;
                    });
                    incomeDTOs.ForEach(x =>
                    {
                        x.DOWNLOADIND = x.INCOMECODE;
                        x.INCOMECODE = x.INCOMECODE.Trim();
                    });
                    IncomeDTO incomeDTONA = incomeDTOs.FirstOrDefault();

                    incomeDTOs = incomeDTOs.OrderBy(x => x.INCOMECODE).ToList();
                    if (incomeDTONA != null)
                        incomeDTOs.Add(incomeDTONA);

                    var groupBy = incomeDTOs.GroupBy(x => x.INCOMECODE).Select(grp => grp.Count() > 1 ? grp.FirstOrDefault(y => y.SDESC.Contains("L-")) : grp.FirstOrDefault()).ToList();
                    responseCustom.IsSuccess = true;
                    responseCustom.Data = groupBy.FirstOrDefault();
                }
                else
                {
                    responseCustom = response;
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static Response GetMonthlyIncomeData()
        {
            try
            {
                Response response = GetDataByQuery("select * from uts.income");
                if (response.IsSuccess)
                {
                    List<IncomeDTO> incomeDTOs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<IncomeDTO>>(response.Data.ToString());
                    responseCustom.IsSuccess = true;
                    responseCustom.Data = incomeDTOs;
                }
                else
                {
                    responseCustom = response;
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static Response GetMonthlyIncomeDataByCode(string code)
        {
            try
            {
                Response response = GetDataByQuery("select * from uts.income where income_code='" + code + "'");
                if (response.IsSuccess)
                {
                    List<IncomeDTO> incomeDTOs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<IncomeDTO>>(response.Data.ToString());
                    if (incomeDTOs.Count == 1)
                    {
                        responseCustom.IsSuccess = true;
                        responseCustom.Data = incomeDTOs.First();
                    }
                    else
                    {
                        responseCustom.IsSuccess = false;
                        responseCustom.Message = "Income code not found";
                    }
                }
                else
                {
                    responseCustom = response;
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static Response GetRaceData()
        {
            try
            {
                Response response = GetDataByQuery("select * from uts.race");
                if (response.IsSuccess)
                {
                    List<RaceDTO> raceDTOs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RaceDTO>>(response.Data.ToString());
                    responseCustom.IsSuccess = true;
                    responseCustom.Data = raceDTOs;
                }
                else
                {
                    responseCustom = response;
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static Response GetOccupationData(bool isGetOld = false)
        {
            List<string> excludeList = new List<string> {
                "H- SENIOR MANAGEMENT",
                "H-BANKING",
                "H-CONSTRUCTION",
                "H-CONSULTANT",
                "H-ENGINEERING",
                "H-GAMBLING",
                "H-GAMING",
                "H-HEALTHCARE",
                "H-HOUSEWIFE",
                "H-IMPORT/EXPORT",
                "H-JEWEL/GEM DEALER",
                "H-MONEY CHARGER",
                "H-PAWN BROKERS",
                "H-PROFESSIONAL",
                "H-RETIREE",
                "H-RETIRER",
                "H-STOCK BROKER",
                "H-STUDENT",
                "H-TRAVEL AGENCIES",
                "H-TRUCK DEALER",
                "H-USED AUTOMOBILE",
                "L-(SE) AGRICULTURE",
                "L-AGENT",
                "L-APPAREL/FASHION",
                "L-AUTOMOTIVE",
                "L-BANKING",
                "L-CLERICAL",
                "L-COMMUNICATION",
                "L-CONSTRUCTION",
                "L-CONSULTANCY",
                "L-DESIGNER",
                "L-EDUCATION",
                "L-ENGINEERING",
                "L-ENTERTAINMENT",
                "L-FACTORY",
                "L-FOOD/BEVERAGE",
                "L-HEALTHCARE",
                "L-HOUSEKEEPER",
                "L-HOUSEWIFE",
                "L-IT & COMPUTER",
                "L-LECTURER",
                "L-MANUFACTURAL",
                "L-MIDDLE MANAGEMENT",
                "L-ONLINE BUSINESS",
                "L-OPERATOR",
                "L-PROFESSIONAL",
                "L-PROPERTY",
                "L-PUBLIC WORKER",
                "L-RETIREE",
                "L-RETIRER",
                "L-SALES & MARKETING",
                "L-SECRETARY",
                "L-SECURITY GUARD",
                "L-SELF EMPLOYED",
                "L-SENIOR MANAGEMENT",
                "L-STUDENT",
                "L-SUPPLIER",
                "L-TECHNICIAN",
                "L-TRADING",
                "L-TRAINER/EVENT",
                "L-TRANSPORTATION",
                "H-OTHERS",
                "NOT AVAILABLE",
                "H-HORSE RACING",
                "H- IT & COMPUTER",
                "H-LECTURER",
                "H- OTHERS",
                "H- STUDENT"
            };
            try
            {
                Response response = GetDataByQuery("select * from uts.occpn");
                if (response.IsSuccess)
                {
                    List<OccupationDTO> occupationDTOs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OccupationDTO>>(response.Data.ToString());
                    if (!isGetOld)
                        occupationDTOs = occupationDTOs.Where(x => !excludeList.Contains(x.SDESC)).ToList();
                    occupationDTOs = occupationDTOs.Where(x => x.SDESC != "L-STUDENT" && x.SDESC != "NOT AVAILABLE" && x.SDESC != "L-RETIRER" && x.SDESC != "H-RETIRER" && x.SDESC != "L-HOUSEWIFE" && x.SDESC != "H-HOUSEWIFE").ToList();
                    occupationDTOs.ForEach(x =>
                    {
                        x.DOWNLOADIND = x.OCCCODE;
                        x.OCCCODE = x.SDESC.Replace("L-", "").Replace("H-", "").Replace("(SE) ", "");
                    });
                    occupationDTOs.ForEach(x =>
                    {
                        x.OCCCODE = x.OCCCODE.Trim().Capitalize();
                    });
                    OccupationDTO occupationDTONA = occupationDTOs.FirstOrDefault(x => x.OCCCODE == "Not Available");
                    occupationDTOs = occupationDTOs.Where(x => x.OCCCODE != "Not Available").ToList();
                    occupationDTOs = occupationDTOs.OrderBy(x => x.OCCCODE).ToList();
                    if (occupationDTONA != null)
                        occupationDTOs.Add(occupationDTONA);

                    var groupBy = occupationDTOs.GroupBy(x => x.OCCCODE).Select(grp => grp.Count() > 1 ? grp.FirstOrDefault(y => y.SDESC.Contains("L-")) : grp.FirstOrDefault()).ToList();
                    responseCustom.IsSuccess = true;
                    responseCustom.Data = groupBy;
                }
                else
                {
                    responseCustom = response;
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static Response GetOccupationDescData(String occuCode)
        {
            List<string> excludeList = new List<string> {
                //"H- SENIOR MANAGEMENT",
                //"H-BANKING",
                //"H-CONSTRUCTION",
                //"H-CONSULTANT",
                //"H-ENGINEERING",
                //"H-GAMBLING",
                //"H-GAMING",
                //"H-HEALTHCARE",
                //"H-HOUSEWIFE",
                //"H-IMPORT/EXPORT",
                //"H-JEWEL/GEM DEALER",
                //"H-MONEY CHARGER",
                //"H-PAWN BROKERS",
                //"H-PROFESSIONAL",
                //"H-LECTURER",
                //"H-RETIRER",
                //"H-STUDENT",
                //"H-STOCK BROKER",
                //"H-TRAVEL AGENCIES",
                //"H-TRUCK DEALER",
                //"H-USED AUTOMOBILE",
                //"L-(SE) AGRICULTURE",
                //"L-AGENT",
                //"L-APPAREL/FASHION",
                //"L-AUTOMOTIVE",
                //"L-BANKING",
                //"L-CLERICAL",
                //"L-COMMUNICATION",
                //"L-CONSTRUCTION",
                //"L-CONSULTANCY",
                //"L-DESIGNER",
                //"L-EDUCATION",
                //"L-ENGINEERING",
                //"L-ENTERTAINMENT",
                //"L-FACTORY",
                //"L-FOOD/BEVERAGE",
                //"L-HEALTHCARE",
                //"L-HOUSEKEEPER",
                //"L-HOUSEWIFE",
                //"L-IT & COMPUTER",
                //"L-LECTURER",
                //"L-MANUFACTURAL",
                //"L-MIDDLE MANAGEMENT",
                //"L-ONLINE BUSINESS",
                //"L-OPERATOR",
                //"L-PROFESSIONAL",
                //"L-PROPERTY",
                //"L-PUBLIC WORKER",
                //"L-RETIREE",
                //"L-SALES & MARKETING",
                //"L-SECRETARY",
                //"L-SECURITY GUARD",
                //"L-SELF EMPLOYED",
                //"L-SENIOR MANAGEMENT",
                //"L-STUDENT",
                //"L-SUPPLIER",
                //"L-TECHNICIAN",
                //"L-TRADING",
                //"L-TRAINER/EVENT",
                //"L-TRANSPORTATION",
                "NOT AVAILABLE"
            };
            try
            {
                Response response = GetDataByQuery("select * from uts.occpn where occ_code ='" + occuCode + "'");
                if (response.IsSuccess)
                {
                    List<OccupationDTO> occupationDTOs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OccupationDTO>>(response.Data.ToString());
                    occupationDTOs = occupationDTOs.Where(x => !excludeList.Contains(x.SDESC)).ToList();
                    occupationDTOs = occupationDTOs.Where(x => x.SDESC != "L-STUDENT" && /*x.SDESC != "NOT AVAILABLE" &&*/ x.SDESC != "L-RETIRER" && x.SDESC != "H-RETIRER" && x.SDESC != "L-HOUSEWIFE" && x.SDESC != "H-HOUSEWIFE").ToList();
                    occupationDTOs.ForEach(x =>
                    {
                        x.DOWNLOADIND = x.OCCCODE;
                        x.OCCCODE = x.SDESC.Replace("L-", "").Replace("H-", "").Replace("(SE) ", "");
                    });
                    occupationDTOs.ForEach(x =>
                    {
                        x.DOWNLOADIND = x.OCCCODE;
                        x.OCCCODE = x.OCCCODE.Trim().Capitalize();
                    });
                    OccupationDTO occupationDTONA = occupationDTOs.FirstOrDefault(x => x.OCCCODE == "Not Available");
                    occupationDTOs = occupationDTOs.Where(x => x.OCCCODE != "Not Available").ToList();
                    occupationDTOs = occupationDTOs.OrderBy(x => x.OCCCODE).ToList();
                    if (occupationDTONA != null)
                        occupationDTOs.Add(occupationDTONA);

                    var groupBy = occupationDTOs.GroupBy(x => x.OCCCODE).Select(grp => grp.Count() > 1 ? grp.FirstOrDefault(y => y.SDESC.Contains("L-")) : grp.FirstOrDefault()).ToList();
                    responseCustom.IsSuccess = true;
                    responseCustom.Data = groupBy.FirstOrDefault();
                }
                else
                {
                    responseCustom = response;
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

        public static Response GetCurrencyData()
        {
            try
            {
                Response response = GetDataByQuery("select * from uts.currency");
                if (response.IsSuccess)
                {
                    List<CurrencyDTO> currencyDTOs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CurrencyDTO>>(response.Data.ToString());
                    responseCustom.IsSuccess = true;
                    responseCustom.Data = currencyDTOs;
                }
                else
                {
                    responseCustom = response;
                }
            }
            catch (Exception ex)
            {
                responseCustom.IsSuccess = false;
                responseCustom.Message = ex.Message;
            }
            return responseCustom;
        }

    }
}