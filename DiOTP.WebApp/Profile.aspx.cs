﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomAttributes;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class Profile : System.Web.UI.Page
    {
        private static User user = new User();

        private static readonly Lazy<IMaHolderRegService> lazyMaHolderRegObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());

        public static IMaHolderRegService IMaHolderRegService { get { return lazyMaHolderRegObj.Value; } }

        private static readonly Lazy<IMaHolderClassService> lazyMaHolderClassObj = new Lazy<IMaHolderClassService>(() => new MaHolderClassService());

        public static IMaHolderClassService IMaHolderClassService { get { return lazyMaHolderClassObj.Value; } }

        private static readonly Lazy<IMaHolderBankService> lazyMaHolderBankServiceObj = new Lazy<IMaHolderBankService>(() => new MaHolderBankService());

        public static IMaHolderBankService IMaHolderBankService { get { return lazyMaHolderBankServiceObj.Value; } }

        private static readonly Lazy<IBanksDefService> lazyBanksDefServiceObj = new Lazy<IBanksDefService>(() => new BanksDefService());

        public static IBanksDefService IBanksDefService { get { return lazyBanksDefServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IOccupationCodesDefService> lazyOccupationCodesDefServiceObj = new Lazy<IOccupationCodesDefService>(() => new OccupationCodesDefService());

        public static IOccupationCodesDefService IOccupationCodesDefService { get { return lazyOccupationCodesDefServiceObj.Value; } }

        private static readonly Lazy<IRaceDefService> lazyIRaceDefServiceObj = new Lazy<IRaceDefService>(() => new RaceDefService());

        public static IRaceDefService IRaceDefService { get { return lazyIRaceDefServiceObj.Value; } }

        private static readonly Lazy<INationalityDefService> lazyINationalityDefServiceObj = new Lazy<INationalityDefService>(() => new NationalityDefService());

        public static INationalityDefService INationalityDefService { get { return lazyINationalityDefServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                if (Session["user"] != null)
                    RefreshData();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        public void RefreshData()
        {
            try
            {
                user = (User)Session["user"];
                txtUsername.Text = user.Username;
                txtEmail.Text = user.EmailId;
                txtMobileNumber.Text = user.MobileNumber;
                if (Session["LastLoginTime"] != null)
                {
                    lastLoginTime.InnerHtml = Convert.ToDateTime(Session["LastLoginTime"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                    lastLoginIP.InnerHtml = Session["LastLoginIP"].ToString();
                }
                else
                {
                    lastLoginSpan.Visible = false;
                }
                List<UserSecurity> userSecurities = user.UserIdUserSecurities;
                UserSecurity EmailSecurity = userSecurities.Where(x => x.UserSecurityTypeId == 1).FirstOrDefault();
                UserSecurity MobileSecurity = userSecurities.Where(x => x.UserSecurityTypeId == 2).FirstOrDefault();
                UserSecurity GoogleSecurity = userSecurities.Where(x => x.UserSecurityTypeId == 3).FirstOrDefault();
                Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                if (responseUAList.IsSuccess)
                {
                    List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', '" + responseUAList.Message + "', '');", true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Profile Page RefreshData: " + ex.Message);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetMAAccountDetails(String HolderID)
        {
            Int32 MAHolderRegID = Convert.ToInt32(user.UserIdUserAccounts.FirstOrDefault().MaHolderRegIdMaHolderReg.Id);
            Response response = IMaHolderRegService.GetSingle(MAHolderRegID);
            if (response.IsSuccess)
            {
                MaHolderReg maHolderReg = (MaHolderReg)response.Data;
                return maHolderReg;
            }
            return null;
        }
    }
}