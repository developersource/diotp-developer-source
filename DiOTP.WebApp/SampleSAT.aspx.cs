﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class SampleSAT : System.Web.UI.Page
    {
        private static readonly Lazy<IFormTypeService> lazyFormTypeServiceObj = new Lazy<IFormTypeService>(() => new FormTypeService());

        public static IFormTypeService IFormTypeService { get { return lazyFormTypeServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string propName = nameof(FormType.Code);
                Response responseFTList = IFormTypeService.GetDataByPropertyName(propName, "SAT (NON-INDIVIDUAL)", true, 0, 0, false);
                if (responseFTList.IsSuccess)
                {
                    FormType SATNonIndividualForm = ((List<FormType>)responseFTList.Data).FirstOrDefault();
                    StringBuilder sb = new StringBuilder();
                    foreach (FormDataField fdf in SATNonIndividualForm.FormTypeIdFormDataFields)
                    {
                        sb.AppendLine(fdf.Name + "<br />");
                        if (fdf.FormDataFieldIdFormDataFieldOptions != null && fdf.FormDataFieldIdFormDataFieldOptions.Count != 0)
                            foreach (FormDataFieldOption fdfo in fdf.FormDataFieldIdFormDataFieldOptions)
                                if (fdf.FieldType == "checkbox")
                                    sb.Append(" <input type='checkbox' /> " + fdfo.Name + "(" + fdfo.Score + ")");

                        sb.AppendLine("<hr />");
                    }

                    fields.InnerHtml = sb.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseFTList.Message + "\", '');", true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }
    }
}