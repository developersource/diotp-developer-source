﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Globalization;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using static DiOTP.Utility.CustomClasses.CustomStatus;
using DiOTP.PolicyAndRules;
using static DiOTP.PolicyAndRules.TransactionPolicy;

namespace DiOTP.WebApp
{
    public partial class SellFunds : System.Web.UI.Page
    {
        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        private static readonly Lazy<IMaHolderBankService> lazyMaHolderBankServiceObj = new Lazy<IMaHolderBankService>(() => new MaHolderBankService());

        public static IMaHolderBankService IMaHolderBankService { get { return lazyMaHolderBankServiceObj.Value; } }

        private static readonly Lazy<IFundReturnService> lazyIFundReturnServiceObj = new Lazy<IFundReturnService>(() => new FundReturnService());

        public static IFundReturnService IFundReturnService { get { return lazyIFundReturnServiceObj.Value; } }

        private static readonly Lazy<IFundInfoService> lazyFundInfoObj = new Lazy<IFundInfoService>(() => new FundInfoService());

        public static IFundInfoService IFundInfoService { get { return lazyFundInfoObj.Value; } }

        private static readonly Lazy<IUserOrderService> lazyUserOrderObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderObj.Value; } }

        private static readonly Lazy<IDvDistributionIntructionService> lazyDvDistributionIntructionObj = new Lazy<IDvDistributionIntructionService>(() => new DvDistributionIntructionService());

        public static IDvDistributionIntructionService IDvDistributionIntructionService { get { return lazyDvDistributionIntructionObj.Value; } }

        private static readonly Lazy<IUserOrderCartService> lazyUserOrderCartObj = new Lazy<IUserOrderCartService>(() => new UserOrderCartService());

        public static IUserOrderCartService IUserOrderCartService { get { return lazyUserOrderCartObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());

        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainObj.Value; } }

        private static readonly Lazy<IUserOrderFileService> lazyUserOrderFileObj = new Lazy<IUserOrderFileService>(() => new UserOrderFileService());

        public static IUserOrderFileService IUserOrderFileService { get { return lazyUserOrderFileObj.Value; } }

        private static readonly Lazy<IBanksDefService> lazyIBanksDefServiceObj = new Lazy<IBanksDefService>(() => new BanksDefService());

        public static IBanksDefService IBanksDefService { get { return lazyIBanksDefServiceObj.Value; } }
        public static List<UtmcFundInformation> UTMCFundInformations = new List<UtmcFundInformation>();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                RefreshData();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        public void RefreshData()
        {
            try
            {


                String fundCode = Request.QueryString["fundCode"];
                if (!string.IsNullOrEmpty(fundCode))
                {
                    Session["transFundCode"] = fundCode;
                    hdnFundId.Value = fundCode;
                }
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx?redirectUrl=SellFunds.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx?redirectUrl=SellFunds.aspx');", true);
                }
                else
                {
                    Boolean IsImpersonate = ServicesManager.IsImpersonated(Context);
                    if (IsImpersonate)
                    {
                        Response.Redirect("Portfolio.aspx", false);
                    }
                    String isVerified = "0";
                    if (Session["isVerified"] == null)
                    {
                        if (Request.Browser.IsMobileDevice)
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                        else
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                    }
                    else
                    {
                        isVerified = (Session["isVerified"].ToString());
                        if (isVerified == "0")
                        {
                            if (Request.Browser.IsMobileDevice)
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx?redirectUrl=SellFunds.aspx');", true);
                            else
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx?redirectUrl=SellFunds.aspx');", true);
                        }
                    }
                    if (isVerified == "1")
                    {
                        var isTermsModalPopup = Session["tcAcceptSell"];
                        if (isTermsModalPopup != null && isTermsModalPopup.ToString() == "0")
                        {
                            hdnTermsPop.Value = "0";
                            string selectedValues = "";
                            if (ddlUserAccountId.Items.Count != 0 && !string.IsNullOrEmpty(ddlUserAccountId.SelectedValue))
                            {
                                selectedValues = ddlUserAccountId.SelectedValue;
                            }
                            if (!IsPostBack)
                            {
                                Session["cartItems"] = null;
                            }
                            if (Session["user"] != null)
                            {
                                User user = (User)Session["user"];
                                Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                                if (responseUAList.IsSuccess)
                                {
                                    List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;

                                    ddlUserAccountId.Items.Clear();
                                    foreach (UserAccount x in userAccounts)
                                        if (x.IsPrinciple == 1)
                                        {
                                            ddlUserAccountId.Items.Add(new ListItem(CustomValues.GetAccounPlan(x.HolderClass) + " - " + x.AccountNo.ToString(), x.AccountNo.ToString()));
                                        }

                                    ddlUserAccountId.Items.Insert(0, new ListItem("Select Account", ""));
                                    if (!string.IsNullOrEmpty(Request.QueryString["AccountNo"]) && Request.QueryString["AccountNo"] != "0")
                                    {
                                        selectedValues = Request.QueryString["AccountNo"].ToString();
                                    }
                                    if (!String.IsNullOrEmpty(selectedValues))
                                    {
                                        ddlUserAccountId.SelectedValue = selectedValues;
                                        ddlUserAccountId.Enabled = false;
                                        BindAccounts(userAccounts);
                                        BindOrderList();
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                                }
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed.', 'Portfolio.aspx?redirectUrl=SellFunds.aspx');", true);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private static ResponseBuyStep responseBuyStep1 { get; set; }
        public void BindAccounts(List<UserAccount> userAccounts)
        {
            UserAccount primaryAcc = new UserAccount();
            primaryAcc = userAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();

            Response responseBuy = (Response)TransactionPolicy.Validate(new TransactionPolicy.TransactionClass { OrderType = OrderType.Sell, Step = 1, userAccount = primaryAcc });

            if (responseBuy.IsSuccess)
            {
                responseBuyStep1 = (ResponseBuyStep)responseBuy.Data;
                TransactionPolicyEnum transactionPolicyEnum = (TransactionPolicyEnum)Enum.Parse(typeof(TransactionPolicyEnum), responseBuyStep1.Code);
                string message = transactionPolicyEnum.ToDescriptionString();
                if (message == "SUCCESS")
                {
                    hdnAccountPlan.Value = CustomValues.isEPF(primaryAcc.HolderClass) ? "EPF" : "CASH";

                    if (CustomValues.isEPF(primaryAcc.HolderClass))
                        bankAccountDll.Visible = false;
                    else
                        bankAccountDll.Visible = true;

                    MaHolderReg maHolderReg = responseBuyStep1.maHolderReg;
                    int agentId = maHolderReg.AgentCode.HasValue ? maHolderReg.AgentCode.Value : 0;

                    UTMCFundInformations = responseBuyStep1.utmcFundInformations;

                    List<HolderInv> holderInvs = responseBuyStep1.holderInvs;
                    StringBuilder sb = new StringBuilder();
                    foreach (HolderInv holderInv in holderInvs.Where(x => x.CurrUnitHldg > 0).ToList())
                    {
                        UtmcFundInformation uFI = UTMCFundInformations.FirstOrDefault(x => x.IpdFundCode == holderInv.FundId);
                        sb.Append(@"<tr>
                                                        <td>" + uFI.FundName.Capitalize() + @"</td>
                                                        <td class='unitFormatNoSymbol text-right'>" + holderInv.CurrUnitHldg + @"</td>
                                                        <td class='currencyFormatNoSymbol text-right'>" + uFI.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice + @"</td>
                                                        <td class='unitFormatNoSymbolMarketValue text-right'>" + (holderInv.CurrUnitHldg * uFI.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice) + @"</td>
                                                    </tr>");
                    }
                    if (holderInvs.Count > 0)
                        tbodyAccountFunds.InnerHtml = sb.ToString();

                    UtmcFundInformation utmcFundInformation = null;
                    if (Session["transFundCode"] != null)
                    {
                        utmcFundInformation = UTMCFundInformations.FirstOrDefault(x => x.FundCode == Session["transFundCode"].ToString());
                    }

                    string userAccSATGroupByScore = CustomValues.GetGroupOnlyByScore(primaryAcc.SatScore);
                    int group = userAccSATGroupByScore[1];

                    List<UtmcFundInformation> UTMCFundInformationsL = UTMCFundInformations.Where(x => holderInvs.Where(y => y.CurrUnitHldg > 0).Select(y => y.FundId).Contains(x.IpdFundCode)).ToList();

                    List<UtmcFundInformation> UTMCFundInformationsRec = UTMCFundInformationsL.Where(x => x.SatGroup.ToLower() == userAccSATGroupByScore.ToLower()).ToList();
                    List<UtmcFundInformation> UTMCFundInformationsOthers = UTMCFundInformationsL.Where(x => x.SatGroup.ToLower() != userAccSATGroupByScore.ToLower()).ToList();
                    UTMCFundInformationsOthers = UTMCFundInformationsOthers.OrderBy(x => x.SatGroup).ToList();
                    ddlFundsList.Items.Clear();
                    if (UTMCFundInformationsRec.Count != 0)
                    {
                        foreach (UtmcFundInformation uFI in UTMCFundInformationsRec)
                        {
                            string riskCompared = "";
                            var risk = "";
                            if (uFI.SatGroup.ToLower().Contains(userAccSATGroupByScore.ToLower()))
                            {
                                riskCompared = "Recommended";
                            }
                            else
                            {
                                string uFSATGroup = uFI.SatGroup;
                                string[] gs = uFSATGroup.Split(',');
                                foreach (string g in gs)
                                {
                                    int groupF = g[1];
                                    if (groupF < group)
                                    {
                                        riskCompared = "Low Risk";
                                    }
                                    if (groupF > group)
                                    {
                                        riskCompared = "High Risk";
                                    }
                                }
                            }
                            risk = CustomValues.GetNameByGroup(uFI.SatGroup);

                            ListItem listItem = new ListItem
                            {
                                Text = uFI.FundName.Capitalize(),
                                Value = uFI.Id.ToString(),
                            };
                            listItem.Attributes.Add("data-holding-units", holderInvs.Where(x => x.FundId == uFI.IpdFundCode).FirstOrDefault().CurrUnitHldg.ToString());
                            listItem.Attributes.Add("risk-group", risk + " - " + riskCompared);
                            listItem.Attributes.Add("data-code", uFI.FundCode);

                            if (utmcFundInformation != null)
                            {
                                if (uFI.Id == utmcFundInformation.Id)
                                {
                                    listItem.Selected = true;
                                    ddlFundsList.Items.Add(listItem);
                                }
                                else
                                {
                                    ddlFundsList.Items.Add(listItem);
                                }
                            }
                            else
                            {
                                ddlFundsList.Items.Add(listItem);
                            }
                        }
                    }

                    foreach (UtmcFundInformation uFI in UTMCFundInformationsOthers)
                    {
                        string riskCompared = "";
                        var risk = "";

                        if (uFI.SatGroup.ToLower().Contains(userAccSATGroupByScore.ToLower()))
                        {
                            riskCompared = "Recommended";
                        }
                        else
                        {
                            string uFSATGroup = uFI.SatGroup;
                            string[] gs = uFSATGroup.Split(',');
                            foreach (string g in gs)
                            {
                                int groupF = g[1];
                                if (groupF < group)
                                {
                                    riskCompared = "Low Risk";
                                }
                                if (groupF > group)
                                {
                                    riskCompared = "High Risk";
                                }
                            }
                        }

                        risk = CustomValues.GetNameByGroup(uFI.SatGroup);

                        ListItem listItem = new ListItem
                        {
                            Text = uFI.FundName.Capitalize(),
                            Value = uFI.Id.ToString(),
                        };
                        listItem.Attributes.Add("data-holding-units", holderInvs.Where(x => x.FundId == uFI.IpdFundCode).FirstOrDefault().CurrUnitHldg.ToString());
                        listItem.Attributes.Add("risk-group", risk + " - " + riskCompared);
                        if (utmcFundInformation != null)
                        {
                            if (uFI.Id == utmcFundInformation.Id)
                            {
                                listItem.Selected = true;
                                ddlFundsList.Items.Add(listItem);
                            }
                            else
                            {
                                ddlFundsList.Items.Add(listItem);
                            }
                        }
                        else
                        {
                            ddlFundsList.Items.Add(listItem);
                        }
                    }
                    ddlFundsList.Items.Insert(0, new ListItem("Select Fund", "0"));

                    ddlBankAccountsList.Items.Clear();

                    string accountfilter = " user_account_id=" + primaryAcc.Id + " and status=1 ";
                    Response responseUABList = GenericService.GetDataByFilter<UserAccountBanks>(accountfilter, 0, 0, false, null, true, null, false, false, null);
                    List<UserAccountBanks> listBanks = (List<UserAccountBanks>)responseUABList.Data;
                    if (listBanks.Count == 0)
                    {
                        if (CustomValues.GetAccounPlan(primaryAcc.HolderClass) != "EPF" && CustomValues.GetAccounPlan(primaryAcc.HolderClass) != "CORP" && !string.IsNullOrEmpty(ddlUserAccountId.SelectedValue))
                        {
                            // No Bank Account Added
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", @"ShowCustomMessage('Alert', 'Please bind bank details before proceeding.', 'Settings.aspx?AccountNo=" + ddlUserAccountId.SelectedValue + "&isPopup=1&Popup=openPopup(\\'bankDetails\\')');", true);
                        }
                    }
                    else
                    {
                        Response responseMHBList = IMaHolderBankService.GetDataByFilter(" id in (" + String.Join(",", listBanks.Select(x => x.MaHolderBankId).ToArray()) + ")", 0, 0, false);
                        if (responseMHBList.IsSuccess)
                        {
                            List<MaHolderBank> accountBanks = ((List<MaHolderBank>)responseMHBList.Data).Where(x => x.Status == 1).ToList();
                            if (accountBanks.Count > 0)
                            {
                                foreach (MaHolderBank b in accountBanks)
                                {
                                    Response response = IBanksDefService.GetSingle(b.BankDefId);
                                    if (response.IsSuccess && response.Data != null)
                                    {
                                        BanksDef banksDef = (BanksDef)response.Data;
                                        if (banksDef.Status == 1)
                                        {
                                            ListItem listItem = new ListItem
                                            {
                                                Text = banksDef.Name + "  (" + b.AccountName + " - " + b.BankAccountNo + ")",
                                                Value = b.Id.ToString(),
                                            };
                                            if (accountBanks.Count == 1)
                                            {
                                                listItem.Selected = true;
                                            }
                                            ddlBankAccountsList.Items.Add(listItem);
                                        }
                                        else if (banksDef.Status == 0)
                                        {
                                            ListItem listItem = new ListItem
                                            {
                                                Text = "Binded bank is inactive! Please contact administrator.",
                                                Value = "",
                                            };
                                            ddlBankAccountsList.Items.Add(listItem);
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", @"ShowCustomMessage('Alert', 'Binded bank is inactive! Please contact administrator or bind other bank details before proceeding.', 'Settings.aspx?AccountNo=" + ddlUserAccountId.SelectedValue + "&isPopup=1&Popup=openPopup(\\'bankDetails\\')');", true);
                                        }
                                    }
                                }
                            }
                            else
                            {

                                if (CustomValues.GetAccounPlan(maHolderReg.HolderCls) != "EPF" && CustomValues.GetAccounPlan(maHolderReg.HolderCls) != "CORP" && !string.IsNullOrEmpty(ddlUserAccountId.SelectedValue))
                                {
                                    // No Bank Account Added
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please add bank details before proceeding.', 'Settings.aspx?AccountNo=" + ddlUserAccountId.SelectedValue + "');", true);
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHBList.Message + "\", '');", true);
                        }
                    }
                }
                else if (message == "EXCEPTION")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + responseBuyStep1.Message + "','');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + message + "','" + (message.Contains("SAT") ? "SAT-Form.aspx?redirectUrl=SellFunds.aspx&AccountNo=" + ddlUserAccountId.SelectedValue + "" : "") + "');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + responseBuy.Message + "','');", true);
            }

        }

        public void RunScript(string fundCode)
        {
            if (!string.IsNullOrEmpty(ddlUserAccountId.SelectedValue))
            {
                if (!string.IsNullOrEmpty(fundCode))
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Proceed to Update SAT.', 'SAT-Form.aspx?redirectUrl=SellFunds.aspx&AccountNo=" + ddlUserAccountId.SelectedValue + "');", true);
                else
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Proceed to Update SAT.', 'SAT-Form.aspx?redirectUrl=SellFunds.aspx&AccountNo=" + ddlUserAccountId.SelectedValue + "');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetFundDetails(Int32 Id, string userAccountNo)
        {
            UtmcFundInformation UtmcFundInformation = new UtmcFundInformation();
            List<UtmcDetailedMemberInvestment> utmcDetailedMemberInvestmentsByFund = new List<UtmcDetailedMemberInvestment>();
            List<int> splits = new List<int>();
            try
            {
                Response response = IUtmcFundInformationService.GetSingle(Id);
                if (response.IsSuccess)
                {
                    UtmcFundInformation = (UtmcFundInformation)response.Data;

                    User user = (User)System.Web.HttpContext.Current.Session["user"];

                    Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                        UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == userAccountNo).FirstOrDefault();

                        Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                        MaHolderReg maHolderReg = new MaHolderReg();
                        if (responseMHR.IsSuccess)
                        {
                            maHolderReg = (MaHolderReg)responseMHR.Data;
                        }
                        else
                            return new { IsSuccess = false, Message = responseMHR.Message, splits = splits, UtmcFundInformation = UtmcFundInformation };

                        List<HolderInv> holderInvs = ServicesManager.GetHolderInvByHolderNo(primaryAcc.AccountNo, UTMCFundInformations);
                        Decimal maAccountRM = 0;
                        Decimal maAccountFundUnits = 0;
                        foreach (HolderInv holderInv in holderInvs)
                        {
                            Response responseUFIs = IUtmcFundInformationService.GetDataByFilter(" IPD_Fund_Code='" + holderInv.FundId + "' ", 0, 0, true);
                            if (responseUFIs.IsSuccess)
                            {
                                List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)responseUFIs.Data;
                                if (utmcFundInformations.Count > 0)
                                {
                                    UtmcFundInformation uFI = utmcFundInformations.FirstOrDefault();
                                    if (uFI.Id == Id)
                                    {
                                        maAccountRM += (holderInv.CurrUnitHldg + uFI.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice);
                                        maAccountFundUnits += holderInv.CurrUnitHldg;
                                    }
                                }
                            }
                            else
                                return new { IsSuccess = false, Message = responseUFIs.Message, splits = splits, UtmcFundInformation = UtmcFundInformation };
                        }

                        splits = CustomGenerator.SplitNumber(0, Convert.ToInt32(maAccountRM), 5);
                        return new { IsSuccess = true, splits = splits, UtmcFundInformation = UtmcFundInformation, Units = maAccountFundUnits };
                    }
                    else
                        return new { IsSuccess = false, Message = responseUAList.Message, splits = splits, UtmcFundInformation = UtmcFundInformation };
                }
                else
                    return new { IsSuccess = false, Message = response.Message, splits = splits, UtmcFundInformation = UtmcFundInformation };
            }
            catch (Exception ex)
            {
                return new { IsSuccess = false, Message = ex.Message, splits = splits, UtmcFundInformation = UtmcFundInformation };
            }
        }

        public static List<Cart> GetAllCartItems()
        {
            List<Cart> cartItems = new List<Cart>();
            if (HttpContext.Current.Session["user"] != null)
            {
                if (HttpContext.Current.Session["cartItems"] != null)
                {
                    cartItems = (List<Cart>)HttpContext.Current.Session["cartItems"];
                }
            }
            return cartItems;
        }

        public static void UpdateCart(List<Cart> cartItems)
        {
            HttpContext.Current.Session["cartItems"] = cartItems;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object BindFunds(string userAccountNo)
        {
            User user = (User)HttpContext.Current.Session["user"];
            Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
            if (responseUAList.IsSuccess)
            {
                List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                UserAccount primaryAcc = new UserAccount();
                if (userAccountNo != null)
                {
                    primaryAcc = userAccounts.Where(x => x.AccountNo == userAccountNo).FirstOrDefault();
                }
                List<Cart> cartItems = GetAllCartItems().Where(x => x.userAccount.Id == primaryAcc.Id).ToList();
                int id = 1;
                cartItems.ForEach(x =>
                {
                    x.Id = id;
                    id++;
                });
                UpdateCart(cartItems);
                Decimal totalSales = cartItems.Sum(x => x.Units);
                return new
                {
                    CartItems = cartItems,
                    TotalUnits = totalSales
                };
            }
            return null;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object AddFund(Int32 fundId, Decimal units, string userAccountNo)
        {
            Response response = new Response();
            try
            {
                User user = (User)HttpContext.Current.Session["user"];
                TransactionPolicyEnum transactionPolicyEnum1 = (TransactionPolicyEnum)Enum.Parse(typeof(TransactionPolicyEnum), responseBuyStep1.Code);
                string message1 = transactionPolicyEnum1.ToDescriptionString();
                if (message1 == "SUCCESS")
                {
                    UserAccount primaryAcc = responseBuyStep1.userAccount;
                    MaHolderReg maHolderReg = responseBuyStep1.maHolderReg;
                    List<UtmcFundInformation> UTMCFundInformations = responseBuyStep1.utmcFundInformations;

                    Response responseBuy = (Response)TransactionPolicy.Validate(new TransactionPolicy.TransactionClass
                    {
                        OrderType = OrderType.Sell,
                        Step = 2,
                        userAccount = primaryAcc,
                        maHolderReg = maHolderReg,
                        utmcFundInformations = UTMCFundInformations,
                        holderInvs = responseBuyStep1.holderInvs,
                        UtmcFundInformationId = fundId,
                        Units = units
                    });
                    if (responseBuy.IsSuccess)
                    {
                        ResponseBuyStep responseBuyStep21 = (ResponseBuyStep)responseBuy.Data;
                        TransactionPolicyEnum transactionPolicyEnum = (TransactionPolicyEnum)Enum.Parse(typeof(TransactionPolicyEnum), responseBuyStep21.Code);
                        string message = transactionPolicyEnum.ToDescriptionString();
                        if (message == "SUCCESS")
                        {
                            List<Cart> cartItems = GetAllCartItems();
                            bool isAddNow = false;
                            if (cartItems.Count > 0)
                            {
                                Cart matchedItem = cartItems.Where(x => x.UtmcFundInformation.Id == fundId).FirstOrDefault();
                                if (matchedItem != null)
                                {
                                    isAddNow = false;
                                }
                                else
                                {
                                    isAddNow = true;
                                }
                            }
                            else
                            {
                                isAddNow = true;
                            }
                            if (isAddNow)
                            {
                                Response response1 = IUtmcFundInformationService.GetSingle(fundId);
                                if (response1.IsSuccess)
                                {
                                    cartItems.Add(new Cart
                                    {
                                        UtmcFundInformation = (UtmcFundInformation)response1.Data,
                                        Units = units,
                                        Type = 2,
                                        userAccount = primaryAcc
                                    });
                                }
                            }
                            else
                            {
                                cartItems.ForEach(x =>
                                {
                                    if (x.UtmcFundInformation.Id == fundId)
                                        x.Units = units;
                                });
                            }
                            UpdateCart(cartItems);
                            response.IsSuccess = true;
                            response.Data = BindFunds(userAccountNo);
                        }
                        else if (message == "EXCEPTION")
                        {
                            response.IsSuccess = false;
                            response.Message = responseBuyStep1.Message;
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = message;
                        }
                    }
                    else
                    {
                        response = responseBuy;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Logger.WriteLog("SellFunds Page AddFund: " + ex.Message);
            }

            return response;

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object EditFund(Int32 Id, Decimal Amount, string userAccountNo)
        {
            Response response = new Response();
            try
            {
                User user = (User)HttpContext.Current.Session["user"];
                TransactionPolicyEnum transactionPolicyEnum1 = (TransactionPolicyEnum)Enum.Parse(typeof(TransactionPolicyEnum), responseBuyStep1.Code);
                string message1 = transactionPolicyEnum1.ToDescriptionString();
                if (message1 == "SUCCESS")
                {
                    UserAccount primaryAcc = responseBuyStep1.userAccount;
                    MaHolderReg maHolderReg = responseBuyStep1.maHolderReg;
                    List<UtmcFundInformation> UTMCFundInformations = responseBuyStep1.utmcFundInformations;
                    List<Cart> cartItems = GetAllCartItems();
                    if (cartItems.Count > 0)
                    {
                        Cart editCart = cartItems.Where(x => x.Id == Id).FirstOrDefault();
                        Response responseBuy = (Response)TransactionPolicy.Validate(new TransactionPolicy.TransactionClass
                        {
                            OrderType = OrderType.Sell,
                            Step = 2,
                            userAccount = primaryAcc,
                            maHolderReg = maHolderReg,
                            utmcFundInformations = UTMCFundInformations,
                            holderInvs = responseBuyStep1.holderInvs,
                            UtmcFundInformationId = editCart.UtmcFundInformation.Id,
                            Units = Amount
                        });
                        if (responseBuy.IsSuccess)
                        {
                            ResponseBuyStep responseBuyStep21 = (ResponseBuyStep)responseBuy.Data;
                            TransactionPolicyEnum transactionPolicyEnum = (TransactionPolicyEnum)Enum.Parse(typeof(TransactionPolicyEnum), responseBuyStep21.Code);
                            string message = transactionPolicyEnum.ToDescriptionString();
                            if (message == "SUCCESS")
                            {
                                if (cartItems.Count > 0)
                                {
                                    editCart.Units = Amount;
                                    List<Cart> remainingItems = cartItems.Where(x => x.Id != Id).ToList();
                                    remainingItems.Add(editCart);
                                    cartItems = remainingItems;
                                }
                                UpdateCart(cartItems);
                                response.IsSuccess = true;
                                response.Data = BindFunds(userAccountNo);
                            }
                            else if (message == "EXCEPTION")
                            {
                                response.IsSuccess = false;
                                response.Message = responseBuyStep1.Message;
                            }
                            else
                            {
                                response.IsSuccess = false;
                                response.Message = message;
                            }
                        }
                        else
                        {
                            response = responseBuy;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Logger.WriteLog("SellFunds Page EditFund: " + ex.Message);
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object RemoveFund(Int32 Id, string userAccountNo)
        {
            User user = (User)HttpContext.Current.Session["user"];
            Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
            if (responseUAList.IsSuccess)
            {
                List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                UserAccount primaryAcc = new UserAccount();
                if (userAccountNo != null)
                {
                    primaryAcc = userAccounts.Where(x => x.AccountNo == userAccountNo).FirstOrDefault();
                }
                List<Cart> cartItems = GetAllCartItems();
                if (cartItems.Count > 0)
                {
                    List<Cart> remainingItems = cartItems.Where(x => x.Id != Id).ToList();
                    cartItems = remainingItems;
                }
                UpdateCart(cartItems);
            }
            return BindFunds(userAccountNo);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetFundDetailsGet(Int32 Id, string userAccountNo)
        {
            Response response = IUtmcFundInformationService.GetSingle(Id);
            if (response.IsSuccess)
            {
                UtmcFundInformation UtmcFundInformation = (UtmcFundInformation)response.Data;

                User user = (User)System.Web.HttpContext.Current.Session["user"];
                Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                if (responseUAList.IsSuccess)
                {
                    List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                    UserAccount primaryAcc = new UserAccount();
                    MaHolderReg maHolderReg = new MaHolderReg();
                    if (userAccountNo != null)
                    {
                        primaryAcc = userAccounts.Where(x => x.AccountNo == userAccountNo).FirstOrDefault();

                        Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                        if (responseMHR.IsSuccess)
                        {
                            maHolderReg = (MaHolderReg)responseMHR.Data;

                        }
                    }

                    List<HolderInv> holderInvs = ServicesManager.GetHolderInvByHolderNo(primaryAcc.AccountNo, UTMCFundInformations);
                    Decimal maAccountRM = 0;

                    foreach (HolderInv holderInv in holderInvs)
                    {
                        Response responseUFIs = IUtmcFundInformationService.GetDataByFilter(" IPD_Fund_Code='" + holderInv.FundId + "' ", 0, 0, true);
                        if (responseUFIs.IsSuccess)
                        {
                            List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)responseUFIs.Data;
                            if (utmcFundInformations.Count > 0)
                            {
                                UtmcFundInformation uFI = utmcFundInformations.FirstOrDefault();
                                if (uFI.Id == Id)
                                    maAccountRM += (holderInv.CurrUnitHldg + uFI.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().LatestNavPrice);
                            }
                        }

                    }
                    decimal total = 0;
                    Response responseUOList = IUserOrderService.GetDataByFilter(" user_account_id = '" + primaryAcc.Id + "' and fund_id = '" + Id + "' and (order_type='2' or order_type='3') and (order_status='1' or order_status='2') and status=1 ", 0, 0, false);
                    if (responseUOList.IsSuccess)
                    {
                        List<UserOrder> uos = (List<UserOrder>)responseUOList.Data;
                        total = uos.Sum(x => x.Units);

                        List<int> splits = CustomGenerator.SplitNumber(UtmcFundInformation.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault().MinInitialInvestmentCash, Convert.ToInt32(maAccountRM), 5);

                        return new { UtmcFundInformation = UtmcFundInformation, splits = splits, total = total };
                    }
                }
            }
            return null;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetFundPerformance(int Id)
        {
            Response response = IUtmcFundInformationService.GetSingle(Id);
            if (response.IsSuccess)
            {
                UtmcFundInformation UtmcFundInformation = (UtmcFundInformation)response.Data;
                String propName = nameof(UtmcFundInformation.FundCode);
                string propertyName = nameof(FundReturn.FundCode);
                Response responseFIList = IFundInfoService.GetDataByPropertyName(propName, UtmcFundInformation.IpdFundCode, true, 0, 0, false);
                if (responseFIList.IsSuccess)
                {
                    FundInfo fundInfo = ((List<FundInfo>)responseFIList.Data).FirstOrDefault();
                    Response responseFRList = IFundReturnService.GetDataByPropertyName(propertyName, UtmcFundInformation.IpdFundCode, true, 0, 0, false);
                    if (responseFRList.IsSuccess)
                    {
                        FundReturn fr = ((List<FundReturn>)responseFRList.Data).FirstOrDefault();

                        DateTime currentDate = fundInfo.CurrentNavDate;
                        DateTime ToDate = DateTime.Now;

                        ToDate = currentDate.AddDays(-7);
                        string weekCValueAvg = Math.Round(fr.OneWeek / (currentDate - ToDate).Days, 4).ToString();

                        ToDate = currentDate.AddMonths(-1);
                        string monthCValueAvg = Math.Round(fr.OneMonth / (currentDate - ToDate).Days, 4).ToString();

                        ToDate = currentDate.AddMonths(-3);
                        string month3CValueAvg = Math.Round(fr.ThreeMonth / (currentDate - ToDate).Days, 4).ToString();

                        ToDate = currentDate.AddMonths(-6);
                        string month6CValueAvg = Math.Round(fr.SixMonth / (currentDate - ToDate).Days, 4).ToString();

                        ToDate = currentDate.AddYears(-1);
                        string yearCValueAvg = Math.Round(fr.OneYear / (currentDate - ToDate).Days, 4).ToString();

                        ToDate = currentDate.AddYears(-2);
                        string year2CValueAvg = Math.Round(fr.TwoYear / (currentDate - ToDate).Days, 4).ToString();

                        ToDate = currentDate.AddYears(-3);
                        string year3CValueAvg = Math.Round(fr.ThreeYear / (currentDate - ToDate).Days, 4).ToString();

                        ToDate = currentDate.AddYears(-5);
                        string year5CValueAvg = Math.Round(fr.FiveYear / (currentDate - ToDate).Days, 4).ToString();

                        ToDate = currentDate.AddYears(-5);
                        string year10CValueAvg = Math.Round(fr.TenYear / (currentDate - ToDate).Days, 4).ToString();

                        return new
                        {
                            w1 = fr.OneWeek,
                            m1 = fr.OneMonth,
                            m3 = fr.ThreeMonth,
                            m6 = fr.SixMonth,
                            y1 = fr.OneYear,
                            y2 = fr.TwoYear,
                            y3 = fr.ThreeYear,
                            y5 = fr.FiveYear,
                            y10 = fr.TenYear,
                            w1a = weekCValueAvg,
                            m1a = monthCValueAvg,
                            m3a = month3CValueAvg,
                            m6a = month6CValueAvg,
                            y1a = yearCValueAvg,
                            y2a = year2CValueAvg,
                            y3a = year3CValueAvg,
                            y5a = year5CValueAvg,
                            y10a = year10CValueAvg,
                            date = ToDate.ToString("yyyy-MM-dd")
                        };
                    }
                }

            }
            return null;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Session["transFundCode"] = null;
            try
            {
                User user = (User)Session["user"];
                Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                if (responseUAList.IsSuccess)
                {
                    List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                    UserAccount primaryAccount = new UserAccount();
                    if (ddlUserAccountId.SelectedValue != null)
                    {
                        primaryAccount = userAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();
                    }
                    List<Cart> cartItems = (List<Cart>)(Session["cartItems"]);
                    cartItems = cartItems.Where(x => x.userAccount.Id == primaryAccount.Id).ToList();

                    string fundName = "";
                    string fundName2 = "";
                    string regGuid = "";
                    List<string> fundNames = new List<string>();
                    List<string> fundNames2 = new List<string>();
                    List<UserOrder> orders = new List<UserOrder>();

                    DateTime createdDate = DateTime.Now;
                    Response responseLastOrder = IUserOrderService.GetData(0, 1, true);
                    UserOrder lastUO = ((List<UserOrder>)responseLastOrder.Data).FirstOrDefault();
                    int lastOrderNo = Convert.ToInt32(lastUO.OrderNo.Substring(4, lastUO.OrderNo.Length - 4));
                    string lastyy = lastUO.OrderNo.Substring(2, 2);
                    string yy = DateTime.Now.ToString("yy");
                    if (lastyy != yy)
                    {
                        lastOrderNo = 0;
                    }
                    string orderNo = "RD" + yy + (lastOrderNo + 1).ToString().PadLeft(8, '0');



                    foreach (Cart cartItem in cartItems)
                    {
                        //buy and redirect have ref_no during addtocart
                        if (cartItems.FirstOrDefault().Type != 1 && cartItems.FirstOrDefault().Type != 6)
                            regGuid = Guid.NewGuid().ToString();
                        else
                            regGuid = cartItem.refNo;

                        if (cartItems.FirstOrDefault().Type == 3)
                        {
                            //switch out order
                            orders.Add(new UserOrder
                            {
                                FundId = cartItem.UtmcFundInformation.Id,
                                RefNo = regGuid,
                                OrderType = cartItem.Type,
                                ToFundId = (cartItem.UtmcFundInformation2 != null ? cartItem.UtmcFundInformation2.Id : 0),
                                ToAccountId = (cartItem.userAccount2 != null ? cartItem.userAccount2.Id : 0),
                                UserId = user.Id,
                                UserAccountId = primaryAccount.Id,
                                OrderNo = orderNo,
                                PaymentMethod = "0", /*not selected*/
                                Amount = cartItem.Amount,
                                Units = cartItem.Units,
                                TransId = 0,
                                CreatedBy = user.Id,
                                CreatedDate = createdDate,
                                UpdatedDate = createdDate,
                                UpdatedBy = 0,
                                Status = 1,
                                ConsultantId = (cartItem.ConsultantId == "0" ? "" : cartItem.ConsultantId),
                                TransNo = createdDate.ToString("yyyyMMddHHmmss"),
                                OrderStatus = 1,
                                BankCode = ""
                            });

                            //switch in order
                            orders.Add(new UserOrder
                            {
                                FundId = cartItem.UtmcFundInformation.Id,
                                RefNo = regGuid,
                                OrderType = 4,
                                ToFundId = (cartItem.UtmcFundInformation2 != null ? cartItem.UtmcFundInformation2.Id : 0),
                                ToAccountId = (cartItem.userAccount2 != null ? cartItem.userAccount2.Id : 0),
                                UserId = user.Id,
                                UserAccountId = primaryAccount.Id,
                                OrderNo = orderNo,
                                PaymentMethod = "0", /*not selected*/
                                Amount = cartItem.Amount,
                                Units = cartItem.Units,
                                TransId = 0,
                                CreatedBy = user.Id,
                                CreatedDate = createdDate,
                                UpdatedDate = createdDate,
                                UpdatedBy = 0,
                                Status = 1,
                                ConsultantId = (cartItem.ConsultantId == "0" ? "" : cartItem.ConsultantId),
                                TransNo = createdDate.ToString("yyyyMMddHHmmss"),
                                OrderStatus = 1,
                                BankCode = ""
                            });
                        }
                        else
                        {
                            //buy 
                            orders.Add(new UserOrder
                            {
                                FundId = cartItem.UtmcFundInformation.Id,
                                RefNo = regGuid,
                                OrderType = cartItem.Type,
                                ToFundId = (cartItem.UtmcFundInformation2 != null ? cartItem.UtmcFundInformation2.Id : 0),
                                ToAccountId = (cartItem.userAccount2 != null ? cartItem.userAccount2.Id : 0),
                                UserId = user.Id,
                                UserAccountId = primaryAccount.Id,
                                OrderNo = orderNo,
                                PaymentMethod = "0", /*not selected*/
                                Amount = cartItem.Amount,
                                Units = cartItem.Units,
                                TransId = 0,
                                CreatedBy = user.Id,
                                CreatedDate = createdDate,
                                UpdatedDate = createdDate,
                                UpdatedBy = 0,
                                Status = 1,
                                ConsultantId = (cartItem.ConsultantId == "0" ? "" : cartItem.ConsultantId),
                                TransNo = createdDate.ToString("yyyyMMddHHmmss"),
                                OrderStatus = 1,
                                BankCode = ddlBankAccountsList.SelectedValue
                            });
                        }
                        //buy and redirect might change ditribution instruction
                        if (cartItem.Type == 1 || cartItem.Type == 6)
                        {
                            Response responseDDIList = IDvDistributionIntructionService.GetDataByPropertyName(nameof(DvDistributionIntruction.UserId), user.Id.ToString(), true, 0, 0, false);
                            if (responseDDIList.IsSuccess)
                            {
                                List<DvDistributionIntruction> dvs = (List<DvDistributionIntruction>)responseDDIList.Data;
                                DvDistributionIntruction Existingdv = dvs.Where(x => x.UserAccountId == primaryAccount.Id && x.FundId == cartItem.UtmcFundInformation.Id).FirstOrDefault();

                                if (Existingdv == null)
                                {
                                    DvDistributionIntruction dv = new DvDistributionIntruction
                                    {
                                        UserId = user.Id,
                                        UserAccountId = primaryAccount.Id,
                                        FundId = cartItem.UtmcFundInformation.Id,
                                        DistributionIntruction = cartItem.DistributionId,
                                        ConsultantId = (cartItem.ConsultantId == "0" ? "" : cartItem.ConsultantId),
                                        Status = 1,
                                    };

                                    IDvDistributionIntructionService.PostData(dv);
                                }
                                else
                                {

                                    Existingdv.DistributionIntruction = cartItem.DistributionId;
                                    Existingdv.ConsultantId = (cartItem.ConsultantId == "0" ? "" : cartItem.ConsultantId);

                                    IDvDistributionIntructionService.UpdateData(Existingdv);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseDDIList.Message + "\", '');", true);
                            }
                        }

                        fundName = cartItem.UtmcFundInformation.FundName.Capitalize();
                        if (cartItem.UtmcFundInformation2 != null)
                            fundName2 = cartItem.UtmcFundInformation2.FundName.Capitalize();
                        fundNames.Add(fundName);
                        fundNames2.Add(fundName2);
                    }
                    Int32 result = IUserOrderService.PostBulkData(orders);
                    if (result == (cartItems.FirstOrDefault().Type == 3 ? (cartItems.Count * 2) : cartItems.Count))
                    {
                        foreach (Cart c in cartItems)
                        {
                            IUserOrderCartService.DeleteDataDB(c.Id);
                        }
                        UserLogMain log = new UserLogMain()
                        {
                            TableName = "user_orders",
                            Description = "Create transaction - order_no : " + orderNo,
                            UserId = user.Id,
                            UserAccountId = primaryAccount.Id,
                            UpdatedDate = DateTime.Now,
                            RefId = 0,
                            RefValue = orderNo,
                            StatusType = 1
                        };
                        Response response = IUserLogMainService.PostData(log);
                        log = (UserLogMain)response.Data;
                        Response.Redirect("/Show-Cart.aspx?UON=" + orderNo, false);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Order count not match with Cart count.', '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "<br>" + ex.StackTrace + "\", '');", true);
                Logger.WriteLog("SellFunds Page btnSubmit_Click: " + ex.Message);
                Logger.WriteLog("SellFunds Page btnSubmit_Click: " + ex.StackTrace);
            }
        }

        public void BindOrderList()
        {
            {
                User user = (User)Session["user"];
                string accountNos = ddlUserAccountId.SelectedValue;
                string orderType = "('1','2','3','4')";
                string orderStatus = "('1','2','22')";
                string transPeriod = "";
                StringBuilder filter = new StringBuilder();
                filter.Append(@"SELECT uo.ID, uo.user_id, uo.user_account_id, ua.account_no, uo.order_no, uo.order_type, uo.created_date, uo.amount, uo.units, ufi.Fund_Name, ufi2.Fund_Name as Fund_Name2, uo.payment_method, uo.payment_date, uo.settlement_date, uo.order_status, uo.fpx_bank_code, uo.reject_reason as trans_no, uo.confirmation_date FROM user_orders uo
                            left join user_accounts ua on ua.ID = uo.user_account_id
                            left join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            where uo.user_id='" + user.Id + @"' 
                            and uo.order_type in " + orderType + @" 
                            and uo.order_status in " + orderStatus + @" 
                            and ua.account_no in (" + accountNos + @") 
                            group by uo.order_no 
                            order by FIELD(uo.order_status, '1', '22', '2', '3'), uo.created_date desc");

                Response responseUserOrderList = GenericService.GetDataByQuery(filter.ToString(), 0, 10, false, null, false, null, true);
                if (responseUserOrderList.IsSuccess)
                {
                    var rawDynData = responseUserOrderList.Data;
                    var responseJSON = JsonConvert.SerializeObject(rawDynData);
                    List<OrderListDTO> UserOrderList = JsonConvert.DeserializeObject<List<OrderListDTO>>(responseJSON);

                    StringBuilder tableBuilder = new StringBuilder();
                    int index = 1;

                    UserOrderList.Where(x => x.order_status == 1).ToList().ForEach(x =>
                    {
                        Response responseMO = GenericService.GetDataByQuery(@"SELECT uo.ID, uo.user_id, uo.user_account_id, ua.account_no, uo.order_no, uo.order_type, uo.created_date, uo.amount, uo.units,
                            ufi.Fund_Name, ufi2.Fund_Name as Fund_Name2, uo.payment_method, uo.payment_date, uo.settlement_date, ufc.INITIAL_SALES_CHARGES_PERCENT, ufc2.INITIAL_SALES_CHARGES_PERCENT,
                            uo.order_status, uo.fpx_bank_code, uo.reject_reason as trans_no, ufd.LATEST_NAV_PRICE as LATEST_NAV_PRICE1, ufd2.LATEST_NAV_PRICE as LATEST_NAV_PRICE2, uo.trans_amt, 
                            uo.trans_units, uo.app_fee, uo.exit_fee, uo.fee_amt, uo.net_amt, uo.trans_pr FROM user_orders uo
                            left join user_accounts ua on ua.ID = uo.user_account_id
                            left join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            left join utmc_fund_charges ufc on ufc.utmc_fund_information_id = uo.fund_id
                            left join utmc_fund_charges ufc2 on ufc2.utmc_fund_information_id = uo.to_fund_id 
                            left join utmc_fund_details ufd on ufd.utmc_fund_information_id = ufi.ID
                            left join utmc_fund_details ufd2 on ufd2.utmc_fund_information_id = ufi2.ID
                            where uo.order_no='" + x.order_no + @"' ", 0, 0, false, null, false, null, true);

                        if (responseMO.IsSuccess)
                        {
                            var rawDynMOData = responseMO.Data;
                            var responseMOJSON = JsonConvert.SerializeObject(rawDynMOData);
                            List<OrderListDTO> UserOrderMOList = JsonConvert.DeserializeObject<List<OrderListDTO>>(responseMOJSON);
                            string totalAmt = UserOrderMOList.Sum(y => y.amount).ToString("N2", new CultureInfo("en-US"));
                            string totalUnits = UserOrderMOList.Sum(y => y.units).ToString("N4", new CultureInfo("en-US"));
                            decimal totalAmtEst = 0;
                            decimal totalUnitsEst = 0;

                            string ordertype = "";
                            if (x.order_type == (int)OrderType.Buy)
                                ordertype = "BUY";
                            else if (x.order_type == (int)OrderType.Sell)
                                ordertype = "SELL";
                            else if (x.order_type == (int)OrderType.SwitchIn || x.order_type == (int)OrderType.SwitchOut)
                                ordertype = "SWITCH";
                            else if (x.order_type == (int)OrderType.RSP)
                                ordertype = "RSP";

                            string status = "";
                            if (x.order_status == 1)
                            {
                                status = "Pending";// Order Placed but not yet proceed to FPX/ not yet verified by user
                            }
                            else if (x.order_status == 2 && (x.order_type == (int)OrderType.RSP))
                            {
                                status = "Processing";//Pending: FPX verified
                            }
                            else if (x.order_status == 2 && (x.order_type == (int)OrderType.Sell || x.order_type == (int)OrderType.SwitchIn || x.order_type == (int)OrderType.SwitchOut))
                                status = "Processing";// Pending for order approval
                            else if (x.order_status == 18)
                                status = "Order cancelled";
                            else if (x.order_status == 22 && x.payment_method == "1")
                                status = "Processing";//Pending: for FPX Payment
                            else if (x.order_status == 22 && x.payment_method == "2")
                                status = "Processing";//Pending: For payment approval
                            else if (x.order_status == 29 && x.payment_method == "1")
                                status = "Payment failed";
                            else if (x.order_status == 29 && x.payment_method == "2")
                                status = "Payment rejected";
                            else if (x.order_status == 3)
                                status = "Completed";
                            else if (x.order_status == 39)
                                status = "Order rejected";
                            else if (x.order_status == 2)
                                status = "Processing";//pending: Payment success

                            tableBuilder.Append(@"<tr>
                                                <td><a href='javascript:;' class='show-extended-row'><i class='fa fa-plus'></i></a></td>
                                                <td>" + index + @"</td>
                                                <td>" + x.account_no + @"</td>
                                                <td>" + x.order_no + @"</td>
                                                <td>" + ordertype + @"</td>
                                                <td>" + ((status == "Pending") ? x.created_date.ToString("dd/MM/yyyy HH:mm:ss") : ((status == "Processing") ? ((x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") != "01/01/0001 00:00:00") ? x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") : "-") : ((status == "Completed" || status == "Payment failed" || status == "Order rejected") ? ((x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") != "01/01/0001 00:00:00") ? x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") : "-") : "-"))) + @"</td>
                                                <td>" + status + @"</td>
                                                <td class='text-right'>" + (ordertype == "BUY" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? totalUnits : "")) + @"</td>
                                            </tr>");
                            StringBuilder mos = new StringBuilder();
                            string actionsHTML = "-";
                            if (x.order_status == 1)
                            {
                                actionsHTML = "";
                                actionsHTML += "<div class='btn-group'>";
                                actionsHTML += "<a class='btn btn-info btn-sm mb-5' href='Show-Cart.aspx?UON=" + x.order_no + "'>" + (x.order_type == 1 ? "Pay" : (x.order_type == 2 || x.order_type == 3 ? "Request" : (x.order_type == 6 ? "Verify" : "-"))) + "</a>";
                                actionsHTML += "<a class='btn btn-default btn-sm cancel-order mb-5' data-orderno='" + x.order_no + "' href='javascript:;'>Cancel</a>";
                                actionsHTML += "</div>";
                            }

                            int indexsub = 0;
                            Decimal totalUnitsValueMYR = 0;
                            string totalUnitsInValue = "";
                            int idx = 0;
                            UserOrderMOList.ForEach(y =>
                            {
                                string transno = y.trans_no == "" ? "-" : y.trans_no;
                                OrderListDTO uo = y;
                                int i = 0;

                                string url = "";
                                Response responseUOFList = IUserOrderFileService.GetDataByPropertyName(nameof(UserOrderFile.OrderNo), uo.order_no, true, 0, 0, true);
                                if (responseUOFList.IsSuccess)
                                {
                                    UserOrderFile uof = ((List<UserOrderFile>)responseUOFList.Data).FirstOrDefault();
                                    if (uof != null)
                                        url = uof.Url;
                                    else
                                        url = "FPX PAYMENT";
                                }
                                if (ordertype == "BUY" || ordertype == "RSP")
                                {
                                    decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);
                                    totalAmtEst += UnitsValueMYR;
                                    decimal UnitsValue = (y.amount * y.LATEST_NAV_PRICE1);
                                    totalUnitsEst += UnitsValue;

                                    mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            <td>" + (uo.payment_method == "1" ? "FPX PAYMENT (" + uo.fpx_bank_code + @")" : (uo.payment_method == "2" ? "Proof of Payment <a href=\"" + url + "\" target='_blank'>view</a>" : "-")) + @"</td>
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            " + (y.order_status == 3 ? "<td class='text-right'>" + (y.fee_amt.HasValue ? y.fee_amt.Value.ToString("N2", new CultureInfo("en-US")) : "-") + "</td>" : "") + @"
                                            " + (y.order_status == 1 || y.order_status == 2 || y.order_status == 22 ? "<td>" + y.initial_sales_charges_percent + "</td>" : "") + @"
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "BUY" ? /*"To Be Confirmed"*/ "(Est.) " + UnitsValue.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SELL" ? y.units.ToString("N4", new CultureInfo("en-US")) : "-"))) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_amt.HasValue ? y.trans_amt.Value.ToString("N2", new CultureInfo("en-US")) : (ordertype == "BUY" ? y.amount.ToString("N2", new CultureInfo("en-US")) : (ordertype == "SELL" ? "To Be Confirmed"/*"(Est.) " + UnitsValueMYR.ToString("N2", new CultureInfo("en-US"))*/ : "-"))) + @"</td>
                                            </tr>");
                                    indexsub++;
                                    idx++;
                                }
                                else if (ordertype == "SELL")
                                {
                                    totalUnitsValueMYR = (UserOrderMOList.Sum(yy => yy.units) * y.LATEST_NAV_PRICE1);
                                    decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);



                                    mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "BUY" ? "To Be Confirmed" /*"(Est.) " + UnitsValue.ToString("N4", new CultureInfo("en-US"))*/ : (ordertype == "SELL" ? y.units.ToString("N4", new CultureInfo("en-US")) : "-"))) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_amt.HasValue ? y.trans_amt.Value.ToString("N2", new CultureInfo("en-US")) : (ordertype == "BUY" ? y.amount.ToString("N2", new CultureInfo("en-US")) : (ordertype == "SELL" ? "To Be Confirmed"/*"(Est.) " + UnitsValueMYR.ToString("N2", new CultureInfo("en-US"))*/ : "-"))) + @"</td>
                                        </tr>");
                                    indexsub++;
                                    idx++;
                                }
                                else if (ordertype == "SWITCH" && y.order_type == (int)OrderType.SwitchOut)
                                {
                                    totalUnitsValueMYR = (UserOrderMOList.Sum(yy => yy.units) * y.LATEST_NAV_PRICE1);
                                    decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);

                                    totalUnitsInValue = (totalUnitsValueMYR * y.LATEST_NAV_PRICE2.Value).ToString("N4", new CultureInfo("en-US"));

                                    mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            <td> Switch Sell </td>
                                            
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            " + (y.order_status == 3 ? "<td class='text-right'></td>" : "") + @"
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SWITCH" ? "" + y.units.ToString("N4", new CultureInfo("en-US")) : "")) + @"</td>
                                        </tr>");
                                    indexsub++;
                                    idx++;
                                }
                                else if (ordertype == "SWITCH" && y.order_type == (int)OrderType.SwitchIn)
                                {
                                    totalUnitsValueMYR = (UserOrderMOList.Sum(yy => yy.units) * y.LATEST_NAV_PRICE1);
                                    OrderListDTO so = UserOrderMOList.FirstOrDefault(yy => yy.order_no == y.order_no && yy.order_type == (int)OrderType.SwitchOut);
                                    decimal UnitsValueMYR = (so.units * y.LATEST_NAV_PRICE1);

                                    totalUnitsInValue = (totalUnitsValueMYR * y.LATEST_NAV_PRICE2.Value).ToString("N4", new CultureInfo("en-US"));
                                    string UnitsInValue = (UnitsValueMYR * y.LATEST_NAV_PRICE2.Value).ToString("N4", new CultureInfo("en-US"));

                                    mos.Append(@"<tr>
                                            <td>" + y.Fund_Name2.Capitalize() + @"</td>
                                            <td> Switch Buy </td>
                                            
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            " + (y.order_status == 3 ? "<td class='text-right'>" + (y.app_fee.HasValue ? Math.Round((y.amount * y.app_fee.Value) / 100, 2).ToString("N2", new CultureInfo("en-US")) : "-") + "</td>" : "") + @"
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SWITCH" ? "To Be Confirmed" /*"(Est.) " + UnitsInValue*/ : "")) + @"</td>
                                        </tr>");
                                    idx++;
                                }
                                i++;
                            });

                            if (ordertype == "BUY" || ordertype == "RSP")
                            {
                                if (x.order_status == 3)
                                {
                                    totalAmt = UserOrderMOList.Sum(y => y.trans_amt.HasValue ? y.trans_amt.Value : 0).ToString("N2", new CultureInfo("en-US"));
                                    totalUnits = UserOrderMOList.Sum(y => y.trans_units.HasValue ? y.trans_units.Value : 0).ToString("N4", new CultureInfo("en-US"));
                                }
                                else
                                {
                                    totalAmt = UserOrderMOList.Sum(y => y.amount).ToString("N2", new CultureInfo("en-US"));
                                    totalUnits = UserOrderMOList.Sum(y => y.units).ToString("N4", new CultureInfo("en-US"));
                                }
                                tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='8' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead>
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            <th>Payment Method</th>
                                                            <th>Transaction no</th>
                                                            <th>Trans date</th>
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 1 || UserOrderMOList.FirstOrDefault().order_status == 2 || UserOrderMOList.FirstOrDefault().order_status == 22 ? "<th>Sales Charge (%)</th>" : "") + @"                                                            
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 3 ? "<th class='text-right'>Sales Charge (MYR)</th>" : "") + @"
                                                            <th class='text-right'>Units</th>
                                                            <th class='text-right'>Amount (MYR)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 1 || UserOrderMOList.FirstOrDefault().order_status == 2 || UserOrderMOList.FirstOrDefault().order_status == 3 || UserOrderMOList.FirstOrDefault().order_status == 22 ? @"<td></td>" : "") + @"
                                                            <td class='text-right'>Total</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalUnits : (ordertype == "SELL" ? totalUnits : "(Est.) " + totalUnitsEst.ToString("N4", new CultureInfo("en-US")))) + @"</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalAmt : (ordertype == "BUY" ? totalAmt : "To Be Confirmed")) + @"</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>");
                            }
                            else if (ordertype == "SELL")
                            {
                                tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='8' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead>
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            
                                                            <th>Transaction no</th>
                                                            <th>Trans date</th>
                                                            <th class='text-right'>Units</th>
                                                            <th class='text-right'>Amount (MYR)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <tr>
                                                            <td></td>
                                                            
                                                            <td></td>
                                                            <td class='text-right'>Total</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalUnits : (ordertype == "SELL" ? totalUnits : "To Be Confirmed")) + @"</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalAmt : (ordertype == "BUY" ? totalAmt : "To Be Confirmed")) + @"</td>
                                                            </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>");
                            }
                            else if (ordertype == "SWITCH")
                            {
                                tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='8' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead>
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            <th>Type</th>
                                                            
                                                            <th>Transaction no</th>
                                                            <th>Trans date</th>
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 3 ? "<th class='text-right'>Sales Charge (MYR)</th>" : "") + @"
                                                            <th class='text-right'>Units</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <!--<tr>
                                                            <td></td>
                                                            <td></td>
                                                            
                                                            <td class='text-right'>Total (" + (ordertype == "BUY" || ordertype == "RSP" ? "MYR" : (ordertype == "SELL" || ordertype == "SWITCH" ? "Units" : "")) + @")</td>
                                                            <td class='text-right'>" + (ordertype == "BUY" || ordertype == "RSP" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? totalUnits : "")) + @"</td>
                                                            <td class='text-right'>" + (ordertype == "BUY" || ordertype == "RSP" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? "To Be Confirmed" : "")) + @"</td>
                                                            </tr>-->
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>");
                            }

                            index++;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMO.Message + "\", '');", true);
                        }
                    });
                    UserOrderList.Where(x => x.order_status == 2 || x.order_status == 22).OrderByDescending(x => x.created_date).ToList().ForEach(x =>
                    {
                        Response responseMO = GenericService.GetDataByQuery(@"SELECT uo.ID, uo.user_id, uo.user_account_id, ua.account_no, uo.order_no, uo.order_type, uo.created_date, uo.amount, uo.units,
                            ufi.Fund_Name, ufi2.Fund_Name as Fund_Name2, uo.payment_method, uo.payment_date, uo.settlement_date, ufc.INITIAL_SALES_CHARGES_PERCENT, ufc2.INITIAL_SALES_CHARGES_PERCENT,
                            uo.order_status, uo.fpx_bank_code, uo.reject_reason as trans_no, ufd.LATEST_NAV_PRICE as LATEST_NAV_PRICE1, ufd2.LATEST_NAV_PRICE as LATEST_NAV_PRICE2, uo.trans_amt, 
                            uo.trans_units, uo.app_fee, uo.exit_fee, uo.fee_amt, uo.net_amt, uo.trans_pr FROM user_orders uo
                            left join user_accounts ua on ua.ID = uo.user_account_id
                            left join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            left join utmc_fund_charges ufc on ufc.utmc_fund_information_id = uo.fund_id
                            left join utmc_fund_charges ufc2 on ufc2.utmc_fund_information_id = uo.to_fund_id 
                            left join utmc_fund_details ufd on ufd.utmc_fund_information_id = ufi.ID
                            left join utmc_fund_details ufd2 on ufd2.utmc_fund_information_id = ufi2.ID
                            where uo.order_no='" + x.order_no + @"' ", 0, 0, false, null, false, null, true);

                        if (responseMO.IsSuccess)
                        {
                            var rawDynMOData = responseMO.Data;
                            var responseMOJSON = JsonConvert.SerializeObject(rawDynMOData);
                            List<OrderListDTO> UserOrderMOList = JsonConvert.DeserializeObject<List<OrderListDTO>>(responseMOJSON);
                            string totalAmt = UserOrderMOList.Sum(y => y.amount).ToString("N2", new CultureInfo("en-US"));
                            string totalUnits = UserOrderMOList.Sum(y => y.units).ToString("N4", new CultureInfo("en-US"));
                            decimal totalAmtEst = 0;
                            decimal totalUnitsEst = 0;

                            string ordertype = "";
                            if (x.order_type == (int)OrderType.Buy)
                                ordertype = "BUY";
                            else if (x.order_type == (int)OrderType.Sell)
                                ordertype = "SELL";
                            else if (x.order_type == (int)OrderType.SwitchIn || x.order_type == (int)OrderType.SwitchOut)
                                ordertype = "SWITCH";
                            else if (x.order_type == (int)OrderType.RSP)
                                ordertype = "RSP";

                            string status = "";
                            if (x.order_status == 1)
                            {
                                status = "Pending";// Order Placed but not yet proceed to FPX/ not yet verified by user
                            }
                            else if (x.order_status == 2 && (x.order_type == (int)OrderType.RSP))
                            {
                                status = "Processing";//Pending: FPX verified
                            }
                            else if (x.order_status == 2 && (x.order_type == (int)OrderType.Sell || x.order_type == (int)OrderType.SwitchIn || x.order_type == (int)OrderType.SwitchOut))
                                status = "Processing";// Pending for order approval
                            else if (x.order_status == 18)
                                status = "Order cancelled";
                            else if (x.order_status == 22 && x.payment_method == "1")
                                status = "Processing";//Pending: for FPX Payment
                            else if (x.order_status == 22 && x.payment_method == "2")
                                status = "Processing";//Pending: For payment approval
                            else if (x.order_status == 29 && x.payment_method == "1")
                                status = "Payment failed";
                            else if (x.order_status == 29 && x.payment_method == "2")
                                status = "Payment rejected";
                            else if (x.order_status == 3)
                                status = "Completed";
                            else if (x.order_status == 39)
                                status = "Order rejected";
                            else if (x.order_status == 2)
                                status = "Processing";//pending: Payment success

                            tableBuilder.Append(@"<tr>
                                                <td><a href='javascript:;' class='show-extended-row'><i class='fa fa-plus'></i></a></td>
                                                <td>" + index + @"</td>
                                                <td>" + x.account_no + @"</td>
                                                <td>" + x.order_no + @"</td>
                                                <td>" + ordertype + @"</td>
                                                <td>" + ((status == "Pending") ? x.created_date.ToString("dd/MM/yyyy HH:mm:ss") : ((status == "Processing") ? ((x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") != "01/01/0001 00:00:00") ? x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") : "-") : ((status == "Completed" || status == "Payment failed" || status == "Order rejected") ? ((x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") != "01/01/0001 00:00:00") ? x.confirmation_date.ToString("dd/MM/yyyy HH:mm:ss") : "-") : "-"))) + @"</td>
                                                <td>" + status + @"</td>
                                                <td class='text-right'>" + (ordertype == "BUY" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? totalUnits : "")) + @"</td>
                                            </tr>");
                            StringBuilder mos = new StringBuilder();
                            string actionsHTML = "-";
                            if (x.order_status == 1)
                            {
                                actionsHTML = "";
                                actionsHTML += "<div class='btn-group'>";
                                actionsHTML += "<a class='btn btn-info btn-sm mb-5' href='Show-Cart.aspx?UON=" + x.order_no + "'>" + (x.order_type == 1 ? "Pay" : (x.order_type == 2 || x.order_type == 3 ? "Request" : (x.order_type == 6 ? "Verify" : "-"))) + "</a>";
                                actionsHTML += "<a class='btn btn-default btn-sm cancel-order mb-5' data-orderno='" + x.order_no + "' href='javascript:;'>Cancel</a>";
                                actionsHTML += "</div>";
                            }

                            int indexsub = 0;
                            Decimal totalUnitsValueMYR = 0;
                            string totalUnitsInValue = "";
                            int idx = 0;
                            UserOrderMOList.ForEach(y =>
                            {
                                string transno = y.trans_no == "" ? "-" : y.trans_no;
                                OrderListDTO uo = y;
                                int i = 0;

                                string url = "";
                                Response responseUOFList = IUserOrderFileService.GetDataByPropertyName(nameof(UserOrderFile.OrderNo), uo.order_no, true, 0, 0, true);
                                if (responseUOFList.IsSuccess)
                                {
                                    UserOrderFile uof = ((List<UserOrderFile>)responseUOFList.Data).FirstOrDefault();
                                    if (uof != null)
                                        url = uof.Url;
                                    else
                                        url = "FPX PAYMENT";
                                }
                                if (ordertype == "BUY" || ordertype == "RSP")
                                {
                                    decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);
                                    totalAmtEst += UnitsValueMYR;
                                    decimal UnitsValue = (y.amount * y.LATEST_NAV_PRICE1);
                                    totalUnitsEst += UnitsValue;

                                    mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            <td>" + (uo.payment_method == "1" ? "FPX PAYMENT (" + uo.fpx_bank_code + @")" : (uo.payment_method == "2" ? "Proof of Payment <a href=\"" + url + "\" target='_blank'>view</a>" : "-")) + @"</td>
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            " + (y.order_status == 3 ? "<td class='text-right'>" + (y.fee_amt.HasValue ? y.fee_amt.Value.ToString("N2", new CultureInfo("en-US")) : "-") + "</td>" : "") + @"
                                            " + (y.order_status == 1 || y.order_status == 2 || y.order_status == 22 ? "<td>" + y.initial_sales_charges_percent + "</td>" : "") + @"
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "BUY" ? /*"To Be Confirmed"*/ "(Est.) " + UnitsValue.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SELL" ? y.units.ToString("N4", new CultureInfo("en-US")) : "-"))) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_amt.HasValue ? y.trans_amt.Value.ToString("N2", new CultureInfo("en-US")) : (ordertype == "BUY" ? y.amount.ToString("N2", new CultureInfo("en-US")) : (ordertype == "SELL" ? "To Be Confirmed"/*"(Est.) " + UnitsValueMYR.ToString("N2", new CultureInfo("en-US"))*/ : "-"))) + @"</td>
                                            </tr>");
                                    indexsub++;
                                    idx++;
                                }
                                else if (ordertype == "SELL")
                                {
                                    totalUnitsValueMYR = (UserOrderMOList.Sum(yy => yy.units) * y.LATEST_NAV_PRICE1);
                                    decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);

                                    mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "BUY" ? "To Be Confirmed" /*"(Est.) " + UnitsValue.ToString("N4", new CultureInfo("en-US"))*/ : (ordertype == "SELL" ? y.units.ToString("N4", new CultureInfo("en-US")) : "-"))) + @"</td>
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_amt.HasValue ? y.trans_amt.Value.ToString("N2", new CultureInfo("en-US")) : (ordertype == "BUY" ? y.amount.ToString("N2", new CultureInfo("en-US")) : (ordertype == "SELL" ? "To Be Confirmed"/*"(Est.) " + UnitsValueMYR.ToString("N2", new CultureInfo("en-US"))*/ : "-"))) + @"</td>
                                        </tr>");
                                    indexsub++;
                                    idx++;
                                }
                                else if (ordertype == "SWITCH" && y.order_type == (int)OrderType.SwitchOut)
                                {
                                    totalUnitsValueMYR = (UserOrderMOList.Sum(yy => yy.units) * y.LATEST_NAV_PRICE1);
                                    decimal UnitsValueMYR = (y.units * y.LATEST_NAV_PRICE1);

                                    totalUnitsInValue = (totalUnitsValueMYR * y.LATEST_NAV_PRICE2.Value).ToString("N4", new CultureInfo("en-US"));

                                    mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            <td> Switch Sell </td>
                                            
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            " + (y.order_status == 3 ? "<td class='text-right'></td>" : "") + @"
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SWITCH" ? "" + y.units.ToString("N4", new CultureInfo("en-US")) : "")) + @"</td>
                                        </tr>");
                                    indexsub++;
                                    idx++;
                                }
                                else if (ordertype == "SWITCH" && y.order_type == (int)OrderType.SwitchIn)
                                {
                                    totalUnitsValueMYR = (UserOrderMOList.Sum(yy => yy.units) * y.LATEST_NAV_PRICE1);
                                    OrderListDTO so = UserOrderMOList.FirstOrDefault(yy => yy.order_no == y.order_no && yy.order_type == (int)OrderType.SwitchOut);
                                    decimal UnitsValueMYR = (so.units * y.LATEST_NAV_PRICE1);

                                    totalUnitsInValue = (totalUnitsValueMYR * y.LATEST_NAV_PRICE2.Value).ToString("N4", new CultureInfo("en-US"));
                                    string UnitsInValue = (UnitsValueMYR * y.LATEST_NAV_PRICE2.Value).ToString("N4", new CultureInfo("en-US"));

                                    mos.Append(@"<tr>
                                            <td>" + y.Fund_Name2.Capitalize() + @"</td>
                                            <td> Switch Buy </td>
                                            
                                            <td>" + transno + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy")) + @"</td>
                                            " + (y.order_status == 3 ? "<td class='text-right'>" + (y.app_fee.HasValue ? Math.Round((y.amount * y.app_fee.Value) / 100, 2).ToString("N2", new CultureInfo("en-US")) : "-") + "</td>" : "") + @"
                                            <td class='text-right'>" + (y.order_status == 3 && y.trans_units.HasValue ? y.trans_units.Value.ToString("N4", new CultureInfo("en-US")) : (ordertype == "SWITCH" ? "To Be Confirmed" /*"(Est.) " + UnitsInValue*/ : "")) + @"</td>
                                        </tr>");
                                    idx++;
                                }
                                i++;
                            });

                            if (ordertype == "BUY" || ordertype == "RSP")
                            {
                                if (x.order_status == 3)
                                {
                                    totalAmt = UserOrderMOList.Sum(y => y.trans_amt.HasValue ? y.trans_amt.Value : 0).ToString("N2", new CultureInfo("en-US"));
                                    totalUnits = UserOrderMOList.Sum(y => y.trans_units.HasValue ? y.trans_units.Value : 0).ToString("N4", new CultureInfo("en-US"));
                                }
                                else
                                {
                                    totalAmt = UserOrderMOList.Sum(y => y.amount).ToString("N2", new CultureInfo("en-US"));
                                    totalUnits = UserOrderMOList.Sum(y => y.units).ToString("N4", new CultureInfo("en-US"));
                                }
                                tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='8' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead>
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            <th>Payment Method</th>
                                                            <th>Transaction no</th>
                                                            <th>Trans date</th>
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 1 || UserOrderMOList.FirstOrDefault().order_status == 2 || UserOrderMOList.FirstOrDefault().order_status == 22 ? "<th>Sales Charge (%)</th>" : "") + @"                                                            
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 3 ? "<th class='text-right'>Sales Charge (MYR)</th>" : "") + @"
                                                            <th class='text-right'>Units</th>
                                                            <th class='text-right'>Amount (MYR)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 1 || UserOrderMOList.FirstOrDefault().order_status == 2 || UserOrderMOList.FirstOrDefault().order_status == 3 || UserOrderMOList.FirstOrDefault().order_status == 22 ? @"<td></td>" : "") + @"
                                                            <td class='text-right'>Total</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalUnits : (ordertype == "SELL" ? totalUnits : /*"To Be Confirmed"*/ "(Est.) " + totalUnitsEst.ToString("N4", new CultureInfo("en-US")))) + @"</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalAmt : (ordertype == "BUY" ? totalAmt : "To Be Confirmed" /*"(Est.) " + totalAmtEst.ToString("N2", new CultureInfo("en-US"))*/)) + @"</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>");
                            }
                            else if (ordertype == "SELL")
                            {
                                tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='8' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead>
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            
                                                            <th>Transaction no</th>
                                                            <th>Trans date</th>
                                                            <th class='text-right'>Units</th>
                                                            <th class='text-right'>Amount (MYR)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <tr>
                                                            <td></td>
                                                            
                                                            <td></td>
                                                            <td class='text-right'>Total</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalUnits : (ordertype == "SELL" ? totalUnits : "To Be Confirmed")) + @"</td>
                                                            <td class='text-right'>" + (x.order_status == 3 ? totalAmt : (ordertype == "BUY" ? totalAmt : "To Be Confirmed")) + @"</td>
                                                            </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>");
                            }
                            else if (ordertype == "SWITCH")
                            {
                                tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='8' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead>
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            <th>Type</th>
                                                            
                                                            <th>Transaction no</th>
                                                            <th>Trans date</th>
                                                            " + (UserOrderMOList.FirstOrDefault().order_status == 3 ? "<th class='text-right'>Sales Charge (MYR)</th>" : "") + @"
                                                            <th class='text-right'>Units</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <!--<tr>
                                                            <td></td>
                                                            <td></td>
                                                            
                                                            <td class='text-right'>Total (" + (ordertype == "BUY" || ordertype == "RSP" ? "MYR" : (ordertype == "SELL" || ordertype == "SWITCH" ? "Units" : "")) + @")</td>
                                                            <td class='text-right'>" + (ordertype == "BUY" || ordertype == "RSP" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? totalUnits : "")) + @"</td>
                                                            <td class='text-right'>" + (ordertype == "BUY" || ordertype == "RSP" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? "To Be Confirmed" : "")) + @"</td>
                                                            </tr>-->
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>");
                            }

                            index++;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMO.Message + "\", '');", true);
                        }
                    });
                    if (UserOrderList.Count == 0)
                    {
                        tableBuilder.Append(@"<tr>
                                                <td colspan='8' class='text-center'>No data available in table</td>
                                            </tr>");
                    }
                    tbodyUserOrders.InnerHtml = tableBuilder.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUserOrderList.Message + "\", '');", true);
                }
            }
        }

        public void BindOrderHistory(int id)
        {
            if (Session["user"] != null)
            {
                tbodyUserOrders.InnerHtml = "";
                User user = (User)Session["user"];
                StringBuilder filter = new StringBuilder();
                int skip = 0, take = 10;
                filter.Append(" order_type=" + (int)OrderType.Sell);
                filter.Append(" and user_account_id=" + id.ToString());
                filter.Append(" group by order_no");
                Response responseUOList = IUserOrderService.GetDataByFilter(filter.ToString(), skip, take, true);
                if (responseUOList.IsSuccess)
                {
                    List<UserOrder> userOrders = (List<UserOrder>)responseUOList.Data;

                    if (userOrders.Count > 0)
                    {
                        string ordertype = "";
                        int i = 1;
                        StringBuilder sb = new StringBuilder();
                        Response response = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                        List<UtmcFundInformation> funds = new List<UtmcFundInformation>();
                        if (response.IsSuccess)
                        {
                            funds = (List<UtmcFundInformation>)response.Data;
                        }
                        foreach (UserOrder uo in userOrders.OrderByDescending(a => a.CreatedDate).ToList())
                        {
                            string status = "";
                            if (uo.OrderStatus == 1 && uo.OrderType == (int)OrderType.Buy)
                            {
                                status = "Order Placed";
                            }
                            else if (uo.OrderStatus == 1 && (uo.OrderType == (int)OrderType.Buy || uo.OrderType == (int)OrderType.SwitchIn || uo.OrderType == (int)OrderType.SwitchOut))
                                status = "Pending for order approval";
                            else if (uo.OrderStatus == 18)
                                status = "Order Cancelled";
                            else if (uo.OrderStatus == (int)OrderType.Sell)
                                status = "Pending for order approval";
                            else if (uo.OrderStatus == 22 && uo.PaymentMethod == "1")
                                status = "Pending for payment";
                            else if (uo.OrderStatus == 22 && uo.PaymentMethod == "3")
                                status = "Pending for payment approval";
                            else if (uo.OrderStatus == 29 && uo.PaymentMethod == "1")
                                status = "Payment failed";
                            else if (uo.OrderStatus == 29 && uo.PaymentMethod == "3")
                                status = "Payment rejected";
                            else if (uo.OrderStatus == 3)
                                status = "Order approved";
                            else if (uo.OrderStatus == 39)
                                status = "Order rejected";

                            if (uo.OrderType == (int)OrderType.Buy)
                                ordertype = "BUY";
                            else if (uo.OrderType == (int)OrderType.Sell)
                                ordertype = "SELL";
                            else if (uo.OrderType == (int)OrderType.SwitchIn || uo.OrderType == (int)OrderType.SwitchOut)
                                ordertype = "SWITCH";
                            else if (uo.OrderType == (int)OrderType.RSP)
                                ordertype = "RSP";

                            string url = "";

                            Response responseUOFList = IUserOrderFileService.GetDataByPropertyName(nameof(UserOrderFile.OrderNo), uo.OrderNo, true, 0, 0, true);
                            if (responseUOFList.IsSuccess)
                            {
                                UserOrderFile uof = ((List<UserOrderFile>)responseUOFList.Data).FirstOrDefault();
                                if (uof != null)
                                    url = uof.Url;
                                else
                                    url = "FPX PAYMENT";
                                StringBuilder sub1 = new StringBuilder();

                                if (funds != null && funds.Count != 0)
                                {
                                    UtmcFundInformation fund = funds.FirstOrDefault(a => a.Id == uo.FundId);
                                    sub1.Append(@"<tr>
                                                                    <td style='width: 250px;'>" + fund.FundName.Capitalize() + @"</td>
                                                                    <td>" + uo.Amount + @"</td>
                                                                </tr>");
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                                }

                                sb.Append(@"<tr>
                                                    <td>" + i + @"</td>
                                                    <td>" + uo.CreatedDate.ToString("dd/MM/yyyy") + @"</td>
                                                    <td><a href='OrderConfirmation.aspx?OrderNo=" + uo.OrderNo + "' target='_blank'>" + uo.OrderNo + @"</a></td>
                                                    <td>
                                                        <table>
                                                            <tbody>
                                                                " + sub1.ToString() + @"
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td><span class='' style='padding-left:50px;'>" + (uo.Amount == 0 ? uo.Units.ToString() : uo.Amount.ToString()) + @"</span></td>
                                                    <td>" + status + @"</td>
                                                </tr>");
                                i++;

                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUOFList.Message + "\", '');", true);
                            }
                        }
                        tbodyUserOrders.InnerHtml = sb.ToString();
                    }
                }
            }
        }
        protected void ddlUserAccountId_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(ddlUserAccountId.SelectedValue))
            {
                if (!string.IsNullOrEmpty(Request.QueryString["fundCode"]))
                {
                    hdnFundId.Value = Request.QueryString["fundCode"].ToString();
                }
                ddlUserAccountId.Enabled = false;
            }
            User user = (User)Session["user"];
            Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
            if (responseUAList.IsSuccess)
            {
                List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                BindAccounts(userAccounts);
                BindOrderList();
            }

        }

        protected void btnAcceptTerms_Click(object sender, EventArgs e)
        {
            Session["tcAcceptSell"] = "0";
            hdnTermsPop.Value = "0";
            string accNo = (Session["transAccountNo"] != null ? Session["transAccountNo"].ToString() : "");
            Session["transAccountNo"] = null;
            Response.Redirect("SellFunds.aspx?fundCode=" + (Session["transFundCode"] != null ? Session["transFundCode"].ToString() : "") + "&AccountNo=" + accNo);

        }

    }
}