﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="Disclaimer.aspx.cs" Inherits="DiOTP.WebApp.Disclaimer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx">Home</a></li>
                            <li class="active">Disclaimer</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Disclaimer</h3>
                    </div>
                    <p>All information and descriptions contained in this portal is provided strictly for general information only and shall not be construed as an offer or solicitation of any offer to buy unit trust funds by anyone in any other jurisdiction except in Malaysia and by any person in Malaysia only. The information displayed on this portal is subject to change without prior notice.</p>
                    <p>This portal and its contents are for general information only and may include inaccuracies or typographical errors. Changes are periodically added to the information herein. The information on this portal is obtained from sources believed to be reliable and is based on certain assumptions, information and conditions available as at the relevant date(s) and may be subject to change at any time without notice. Any advice received via this portal should not be relied upon for personal, medical, legal or financial decisions and you should consult an appropriate independent professional for specific advice tailored to your circumstances, requirements or needs.</p>
                    <p>By accessing this portal, you will assume all risk and responsibility for the use of this portal. Apex Investment Services Berhad (‘AISB”), its subsidiaries and affiliates exclude all liability to you, whether express or implied, and any losses arising thereof in respect of your use of, reliance upon, or inability to access or use this portal, and will not be liable for any loss of use, profits, savings, investments or data or any direct or indirect, special, incidental, consequential or punitive damages or losses arising in contract, negligence or torts. If you do not accept the Terms contained herein, you have the discretion to discontinue the use and access of this portal.</p>
                    <p>We recommend that you read and understand the contents of the respective fund's Master Prospectus / Prospectus / Supplemental Prospectus (if any), Product Highlights Sheet (“PHS”) or Information Memorandum / replacement Information Memorandum (collectively referred to as the “Disclosure Document”) before investing in any of the funds. A copy of the Disclosure Document featured in this portal has been registered or lodged with the Securities Commission Malaysia.</p>
                    <p>Investors should be aware that investments in unit trust funds and wholesale funds carry risks. An outline of general risk and specific risk associated to the funds are contained in the Disclosure Document. Units will be issued upon receipt of completed application and relevant account opening forms accompanying the Disclosure Document and subject to terms and conditions therein. Unit prices and income distribution, if any, may rise or fall and income distributions are not guaranteed. Past performance of a fund is not an indication of future performance. There are fees and charges involved in investing in the funds. Please consider the fees and charges involved before investing. This is neither an offer nor solicitation to purchase units of the funds. AISB does not guarantee any returns on the investment.</p>
                    <p>Investors are advised that the unit trusts or wholesale funds offered are solely on the basis of the information contained in the Disclosure Document and no other material other than the Disclosure Document. Any other material outside the designated area of the Disclosure Document does not form part of the Disclosure Document.</p>
                    <p>The information contained in the Disclosure Document is general information only and does not take into account your individual objectives, financial situations or needs. You should seek your own financial advice from an appropriately licensed adviser before investing.</p>
                    <p>Copies of the Disclosure Document are available at our business office or from any of our consultants, authorised distributors and representatives. Please feel free to contact us at 03-2095 9999 or write to us at <a href="mailto:enquiry@apexis.com.my">enquiry@apexis.com.my</a> with your request or enquiries.</p>
                </div>
            </div>
        </div>
    </section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
