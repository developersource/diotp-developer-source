﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using DiOTP.WebApp.ServiceCalls;
using Google.Authenticator;
using KellermanSoftware.CompareNetObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using static DiOTP.PolicyAndRules.UserPolicy;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DiOTP.PolicyAndRules;

namespace DiOTP.WebApp
{
    public partial class Settings : System.Web.UI.Page
    {
        private static readonly Lazy<IUtmcFundInformationService> lazyUserAccountObj2 = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUserAccountObj2.Value; } }

        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        private static readonly Lazy<IUserSecurityService> lazyIUserSecurityService = new Lazy<IUserSecurityService>(() => new UserSecurityService());

        public static IUserSecurityService IUserSecurityService { get { return lazyIUserSecurityService.Value; } }

        private static readonly Lazy<ICountriesDefService> lazyCountriesDefServiceObj = new Lazy<ICountriesDefService>(() => new CountriesDefService());

        public static ICountriesDefService ICountriesDefService { get { return lazyCountriesDefServiceObj.Value; } }

        private static readonly Lazy<IStatesDefService> lazyStatesDefServiceObj = new Lazy<IStatesDefService>(() => new StatesDefService());

        public static IStatesDefService IStatesDefService { get { return lazyStatesDefServiceObj.Value; } }

        private static readonly Lazy<IBanksDefService> lazyBanksDefServiceObj = new Lazy<IBanksDefService>(() => new BanksDefService());

        public static IBanksDefService IBanksDefService { get { return lazyBanksDefServiceObj.Value; } }

        private static readonly Lazy<IMaHolderBankService> lazyMaHolderBankServiceObj = new Lazy<IMaHolderBankService>(() => new MaHolderBankService());

        public static IMaHolderBankService IMaHolderBankService { get { return lazyMaHolderBankServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IUserService> lazyUserServiceObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserServiceObj.Value; } }

        private static readonly Lazy<IMaHolderRegService> lazyMaHolderRegServiceObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());

        public static IMaHolderRegService IMaHolderRegService { get { return lazyMaHolderRegServiceObj.Value; } }

        private static readonly Lazy<IMaHolderClassService> lazyMaHolderClassServiceObj = new Lazy<IMaHolderClassService>(() => new MaHolderClassService());

        public static IMaHolderClassService IMaHolderClassService { get { return lazyMaHolderClassServiceObj.Value; } }

        private static readonly Lazy<INotificationCategoriesDefService> lazyNotificationCategoriesDefServiceObj = new Lazy<INotificationCategoriesDefService>(() => new NotificationCategoriesDefService());

        public static INotificationCategoriesDefService INotificationCategoriesDefService { get { return lazyNotificationCategoriesDefServiceObj.Value; } }

        private static readonly Lazy<INotificationTypesDefService> lazyNotificationTypesDefServiceObj = new Lazy<INotificationTypesDefService>(() => new NotificationTypesDefService());

        public static INotificationTypesDefService INotificationTypesDefService { get { return lazyNotificationTypesDefServiceObj.Value; } }

        private static readonly Lazy<IUserNotificationSettingService> lazyUserNotificationSettingServiceObj = new Lazy<IUserNotificationSettingService>(() => new UserNotificationSettingService());

        public static IUserNotificationSettingService IUserNotificationSettingService { get { return lazyUserNotificationSettingServiceObj.Value; } }

        private static readonly Lazy<IOccupationCodesDefService> lazyOccupationCodesDefServiceObj = new Lazy<IOccupationCodesDefService>(() => new OccupationCodesDefService());

        public static IOccupationCodesDefService IOccupationCodesDefService { get { return lazyOccupationCodesDefServiceObj.Value; } }

        private static readonly Lazy<IRaceDefService> lazyIRaceDefServiceObj = new Lazy<IRaceDefService>(() => new RaceDefService());

        public static IRaceDefService IRaceDefService { get { return lazyIRaceDefServiceObj.Value; } }

        private static readonly Lazy<INationalityDefService> lazyINationalityDefServiceObj = new Lazy<INationalityDefService>(() => new NationalityDefService());

        public static INationalityDefService INationalityDefService { get { return lazyINationalityDefServiceObj.Value; } }

        private static readonly Lazy<IUserLogService> lazyIUserLogServiceObj = new Lazy<IUserLogService>(() => new UserLogService());

        public static IUserLogService IUserLogService { get { return lazyIUserLogServiceObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());

        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        private static readonly Lazy<IUserLogSubService> lazyIUserLogSuberviceObj = new Lazy<IUserLogSubService>(() => new UserLogSubService());

        public static IUserLogSubService IUserLogSubService { get { return lazyIUserLogSuberviceObj.Value; } }

        private static readonly Lazy<IFormTypeService> lazyFormTypeServiceObj = new Lazy<IFormTypeService>(() => new FormTypeService());

        public static IFormTypeService IFormTypeService { get { return lazyFormTypeServiceObj.Value; } }

        private static readonly Lazy<IFormDataFieldValueService> lazyFormDataFieldValueServiceObj = new Lazy<IFormDataFieldValueService>(() => new FormDataFieldValueService());

        public static IFormDataFieldValueService IFormDataFieldValueService { get { return lazyFormDataFieldValueServiceObj.Value; } }

        private static readonly Lazy<IDvDistributionIntructionService> lazyDvDistributionIntructionServiceObj = new Lazy<IDvDistributionIntructionService>(() => new DvDistributionIntructionService());

        public static IDvDistributionIntructionService IDvDistributionIntructionService { get { return lazyDvDistributionIntructionServiceObj.Value; } }

        private static readonly Lazy<IAccountOpeningOccupationService> lazyObjO = new Lazy<IAccountOpeningOccupationService>(() => new AccountOpeningOccupationService());

        public static IAccountOpeningOccupationService IAccountOpeningOccupationService { get { return lazyObjO.Value; } }

        private static readonly Lazy<IAccountOpeningAddressService> lazyObjAOA = new Lazy<IAccountOpeningAddressService>(() => new AccountOpeningAddressService());

        public static IAccountOpeningAddressService IAccountOpeningAddressService { get { return lazyObjAOA.Value; } }

        public static Int32 SMSExpirationTimeInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["SMSExpirationTimeInSeconds"]);

        public static Int32 SMSLockTimeInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["SMSLockTimeInSeconds"]);

        private static readonly Lazy<IAccountOpeningService> lazyObjAO = new Lazy<IAccountOpeningService>(() => new AccountOpeningService());

        public static IAccountOpeningService IAccountOpeningService { get { return lazyObjAO.Value; } }

        public static Response responseMA = new Response();

        public static UserAccount userAccount = new UserAccount();
        public static List<UserAccount> userAccounts = new List<UserAccount>();

        public static List<UtmcFundInformation> UTMCFundInformations = new List<UtmcFundInformation>();


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string accountNoX = "";

                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                if (Session["isVerified"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                }
                if (Session["user"] != null)
                {
                    if (ddlUserAccountId.SelectedValue != "")
                    {
                        Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status = 'Active' ", 0, 0, false);
                        if (responseUFIList.IsSuccess)
                        {
                            UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUFIList.Message + "\", '');", true);
                        }
                        MaHolderReg maHolderReg = new MaHolderReg();
                        User user = (User)Session["user"];
                        User sessionUser = (User)Session["user"];
                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            userAccounts = (List<UserAccount>)responseUAList.Data;
                            UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();
                            userAccount = primaryAcc;

                            alert.Visible = true;
                            satGroupScore.Visible = true;
                            distribution.Visible = true;
                            bankDiv.Visible = true;
                            btnEdit.Visible = true;
                            if (primaryAcc.IsPrinciple == 0)
                            {

                                //Hide all the keyable inputs & submits
                                alert.Visible = false;

                                bankDiv.Visible = false;
                                btnEdit.Visible = false;
                            }


                            responseMA = ServicesManager.GetMaHolderRegByAccountNo(sessionUser.UserIdUserAccounts.Where(x => x.IsPrimary == 1).FirstOrDefault().AccountNo);
                            if (responseMA.IsSuccess)
                            {
                                maHolderReg = (MaHolderReg)responseMA.Data;
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMA.Message + "\", '');", true);
                            }
                            if (CustomValues.GetAccounPlan(maHolderReg.HolderCls) == "CORP")
                            {
                                bankDiv.Visible = false;
                                btnEdit.Visible = false;
                                satGroupScore.Visible = false;
                                distribution.Visible = true;
                            }
                            string acPlan = CustomValues.GetAccounPlan(primaryAcc.HolderClass);
                            if (acPlan == "EPF")
                            {
                                bankDiv.Visible = false;
                            }
                            else
                            {

                            }
                            Boolean IsImpersonate = ServicesManager.IsImpersonated(Context);
                            if (IsImpersonate)
                            {
                                btnEdit.Visible = false;
                                btnSaveAlert.Visible = false;
                                txtFundPriceTargetText.Enabled = false;
                                ddlFundPriceTargetFund.Enabled = false;
                                txtFundPerformanceTarget.Enabled = false;
                                ddlFundPerformanceTargetFund.Enabled = false;
                                btnSubmitEditOccupation.Visible = false;
                                btnOccupationEdit.Visible = false;
                            }

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                        }
                    }
                    else if (UTMCFundInformations.Count == 0)
                    {
                        Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status = 'Active' ", 0, 0, false);
                        if (responseUFIList.IsSuccess)
                        {
                            UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUFIList.Message + "\", '');", true);
                        }
                    }
                }
                hdnSMSExpirationTimeInSeconds.Value = SMSExpirationTimeInSeconds.ToString();
                hdnSMSLockTimeInSeconds.Value = SMSLockTimeInSeconds.ToString();
                if (!IsPostBack)
                {

                    if (!string.IsNullOrEmpty(Request.QueryString["isPopup"]))
                    {
                        if (Request.QueryString["isPopup"].ToString() == "1")
                        {
                            if (!string.IsNullOrEmpty(Request.QueryString["Popup"]))
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup(" + Request.QueryString["Popup"].ToString() + ");", true);
                            }
                        }
                    }
                    bindAccount();
                    if (ddlUserAccountId.SelectedValue != "")
                    {
                        BindWatchList();
                        User sessionUser = (User)Session["user"];
                        BindBankApproval(sessionUser.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }


        public void bindAccount()
        {

            divalert.Visible = false;
            string accountId = string.Empty;
            if (Session["user"] != null)
            {
                User user = (User)Session["user"];
                User sessionUser = (User)Session["user"];
                Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                if (responseUAList.IsSuccess)
                {
                    string Id = ddlUserAccountId.SelectedValue;
                    List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                    ddlUserAccountId.Items.Clear();
                    foreach (UserAccount x in userAccounts)
                        ddlUserAccountId.Items.Add(new ListItem(CustomValues.GetAccounPlan(x.HolderClass) + " - " + x.AccountNo.ToString(), x.AccountNo.ToString()));
                    ddlUserAccountId.Items.Insert(0, new ListItem("Select Account", ""));

                    string accNoSelected = Request.QueryString["accNo"];

                    UserAccount primaryAccount = userAccounts.Where(x => x.IsPrimary == 1).FirstOrDefault();

                    if (!string.IsNullOrEmpty(Request.QueryString["AccountNo"]))
                    {
                        Session["UserAccountId"] = Request.QueryString["AccountNo"].ToString();
                        ddlUserAccountId.SelectedValue = Request.QueryString["AccountNo"].ToString();
                        UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();
                        userAccount = primaryAcc;
                    }



                    if (Session["UserAccountId"] != null)
                    {
                        Id = Session["UserAccountId"].ToString();
                        Session["UserAccountId"] = null;

                    }
                    if (!string.IsNullOrEmpty(accNoSelected))
                    {
                        Id = accNoSelected;

                    }
                    if (!string.IsNullOrEmpty(Id)) //Happens only when during sell, but bank not binded and redirect to account settings bank details
                    {
                        ddlUserAccountId.SelectedIndex = ddlUserAccountId.Items.IndexOf(ddlUserAccountId.Items.FindByValue(Id));
                        primaryAccount = userAccounts.Where(x => x.AccountNo == Id).FirstOrDefault();

                        satATag.HRef = "/SAT-Form.aspx?redirectUrl=Settings.aspx&AccountNo=" + primaryAccount.AccountNo;

                        if (CustomValues.GetAccounPlan(primaryAccount.HolderClass) == "EPF")
                        {
                            bankDiv.Visible = false;
                        }

                        bindAddress(primaryAccount);
                        Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAccount.AccountNo);
                        MaHolderReg maHolderReg = new MaHolderReg();
                        if (responseMHR.IsSuccess)
                        {
                            maHolderReg = (MaHolderReg)responseMHR.Data;
                            ViewOccupation(maHolderReg.OccCode, maHolderReg);
                            Response responseUAList2 = IUserAccountService.GetDataByFilter(" account_no = '" + primaryAccount.AccountNo + "' and status='1' ", 0, 0, false);
                            if (responseUAList2.IsSuccess)
                            {
                                UserAccount ua = ((List<UserAccount>)responseUAList2.Data).FirstOrDefault();
                                if (ua.SatScore != 0)
                                    satScore.InnerHtml = CustomValues.GetGroupByScore(ua.SatScore) + " - " + ua.SatScore;
                                else
                                {
                                    satScore.InnerHtml = "-";
                                    satATag.InnerText = "Update";
                                }
                                BindSettings();
                                int ifChangeInstruction = 0;
                                if (Request.QueryString["Instruction"] != null && Request.QueryString["Iid"] != null)
                                {
                                    Response response = IDvDistributionIntructionService.GetSingle(Convert.ToInt32(Request.QueryString["Iid"].ToString()));
                                    if (response.IsSuccess)
                                    {
                                        DvDistributionIntruction x = (DvDistributionIntruction)response.Data;
                                        if (Request.QueryString["Instruction"] == "ctdc")
                                            x.DistributionIntruction = 2;
                                        else
                                            x.DistributionIntruction = 1;
                                        x.UpdatedDate = DateTime.Now;
                                        Response response2 = IDvDistributionIntructionService.UpdateData(x);
                                        x = (DvDistributionIntruction)response2.Data;
                                        ifChangeInstruction = 1;
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                                    }
                                }
                                bindDistributionSetting();
                                if (ifChangeInstruction == 1)
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openDvModal();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList2.Message + "\", '');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR.Message + "\", '');", true);
                        }
                    }

                    if (ddlUserAccountId.SelectedValue != null)
                    {
                        primaryAccount = userAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();
                    }
                    Boolean IsImpersonate = ServicesManager.IsImpersonated(Context);
                    if (IsImpersonate)
                    {
                        btnEdit.Visible = false;
                        btnSaveAlert.Enabled = false;
                        txtFundPriceTargetText.Enabled = false;
                        ddlFundPriceTargetFund.Enabled = false;
                        txtFundPerformanceTarget.Enabled = false;
                        ddlFundPerformanceTargetFund.Enabled = false;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                }
            }
            else
            {
                if (Request.Browser.IsMobileDevice)
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                else
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
            }
        }

        public void bindAddress(UserAccount primaryAccount)
        {
            addressnotverified.Visible = false;
            addressverified.Visible = false;
            addressdenied.Visible = false;
            addressdetail.InnerHtml = "";
            string accountNoX = "";
            if (primaryAccount != null)
            {
                Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAccount.AccountNo);
                MaHolderReg maHolder = new MaHolderReg();
                if (responseMHR.IsSuccess)
                {
                    maHolder = (MaHolderReg)responseMHR.Data;
                    if (maHolder != null)
                    {
                        txtMANumber.Text = maHolder.HolderNo.ToString();
                        txtAddressLine1.Text = maHolder.Addr1;
                        hdnAddressLine1.Value = maHolder.Addr1;
                        txtAddressLine2.Text = maHolder.Addr2;
                        hdnAddressLine2.Value = maHolder.Addr2;
                        txtAddressLine3.Text = maHolder.Addr3;
                        hdnAddressLine3.Value = maHolder.Addr3;
                        txtAddressLine4.Text = maHolder.Addr4;
                        hdnAddressLine4.Value = maHolder.Addr4;
                        txtPostCode.Text = maHolder.Postcode.ToString();
                        hdnPostCode.Value = maHolder.Postcode.ToString();

                        Response responseCDList = ICountriesDefService.GetData(0, 0, false);
                        int selectedCountry = 0;
                        if (responseCDList.IsSuccess)
                        {
                            ddlCountry.Items.Clear();
                            List<CountriesDef> countries = (List<CountriesDef>)responseCDList.Data;
                            ddlCountry.Items.Add(new ListItem
                            {
                                Text = "Select Country",
                                Value = ""
                            });
                            foreach (CountriesDef c in countries)
                            {
                                ListItem listItem = new ListItem
                                {
                                    Text = c.Name,
                                    Value = c.Code
                                };
                                if (maHolder != null)
                                {
                                    if (maHolder.CountryRes == c.Code)
                                    {
                                        listItem.Selected = true;
                                        selectedCountry = c.Id;
                                    }
                                }
                                ddlCountry.Items.Add(listItem);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseCDList.Message + "\", '');", true);
                        }
                        ddlState.Items.Clear();
                        ddlState.Items.Add(new ListItem
                        {
                            Text = "Select State",
                            Value = ""
                        });

                        if (selectedCountry != 0)
                        {
                            Response responseSDList = IStatesDefService.GetDataByPropertyName(nameof(StatesDef.CountryDefId), selectedCountry.ToString(), true, 0, 0, false);
                            if (responseSDList.IsSuccess)
                            {
                                List<StatesDef> states = (List<StatesDef>)responseSDList.Data;
                                foreach (StatesDef s in states.OrderBy(a => a.Name).ToList())
                                {
                                    int code = Int32.Parse(s.Code);
                                    ListItem listItem = new ListItem
                                    {
                                        Text = s.Name,
                                        Value = code.ToString()
                                    };
                                    if (maHolder != null)
                                    {
                                        if (maHolder.StateCode == code)
                                        {
                                            listItem.Selected = true;
                                        }
                                    }
                                    ddlState.Items.Add(listItem);
                                }
                                hdnState.Value = maHolder.StateCode.ToString();
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseSDList.Message + "\", '');", true);
                            }
                        }
                        Response responseMHR2 = IMaHolderRegService.GetDataByPropertyName(nameof(MaHolderReg.HolderNo), primaryAccount.AccountNo, true, 0, 0, false);
                        if (responseMHR2.IsSuccess)
                        {
                            MaHolderReg maHolder2 = ((List<MaHolderReg>)responseMHR2.Data).FirstOrDefault();
                            if (maHolder2 != null)
                            {
                                if (maHolder2.OtpActSt != null)
                                {
                                    if (maHolder2.OtpActSt == "0")
                                    {
                                        //pending
                                        addressnotverified.Visible = true;
                                        btnEdit.Visible = false;
                                    }
                                    else if (maHolder2.OtpActSt == "1")
                                    {
                                        //approved
                                        addressverified.Visible = true;
                                    }
                                    else if (maHolder2.OtpActSt == "9")
                                    {
                                        //denied
                                        addressdenied.Visible = true;
                                    }

                                    if (maHolder2.OtpActSt != "1" && maHolder2.OtpActSt != "2")
                                    {
                                        string state = "";
                                        string country = "";
                                        Response responses = IStatesDefService.GetDataByFilter(" code = '" + maHolder2.StateCode.ToString().PadLeft(2, '0') + "'", 0, 0, false);
                                        if (responses.IsSuccess)
                                        {
                                            List<StatesDef> stateMatches = (List<StatesDef>)responses.Data;
                                            if (stateMatches.Count > 0)
                                            {
                                                state = stateMatches.FirstOrDefault().Name;
                                            }
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responses.Message + "\", '');", true);
                                        }
                                        Response responses2 = ICountriesDefService.GetDataByFilter(" code = '" + maHolder2.CountryRes + "'", 0, 0, false);
                                        if (responses2.IsSuccess)
                                        {
                                            List<CountriesDef> countriesMatch = (List<CountriesDef>)responses2.Data;
                                            if (countriesMatch.Count > 0)
                                            {
                                                country = countriesMatch.FirstOrDefault().Name;
                                            }
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responses2.Message + "\", '');", true);
                                        }
                                        //show record
                                        string htmlPending = "<tr><th>Address Line 1</th><td>" + maHolder2.Addr1 + "</td></tr>";
                                        htmlPending += "<tr><th>Address Line 2</th><td>" + maHolder2.Addr2 + "</td></tr>";
                                        htmlPending += "<tr><th>Address Line 3</th><td>" + maHolder2.Addr3 + "</td></tr>";
                                        htmlPending += "<tr><th>Address Line 4</th><td>" + maHolder2.Addr4 + "</td></tr>";
                                        htmlPending += "<tr><th>Postcode</th><td>" + maHolder2.Postcode.ToString() + "</td></tr>";
                                        htmlPending += "<tr><th>State</th><td>" + state + "</td></tr>";
                                        htmlPending += "<tr><th>Country</th><td>" + country + "</td></tr>";
                                        addressdetail.InnerHtml = htmlPending;
                                    }
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR2.Message + "\", '');", true);
                        }
                    }

                    string accountPlan = CustomValues.GetAccounPlan(maHolder.HolderCls);
                    Response responseMHCList = IMaHolderClassService.GetDataByPropertyName(nameof(MaHolderClass.Code), maHolder.HolderCls, true, 0, 0, false);
                    if (responseMHCList.IsSuccess)
                    {
                        MaHolderClass hc = ((List<MaHolderClass>)responseMHCList.Data).FirstOrDefault();
                        //
                        //Account
                        txtMANumber2.Text = maHolder.HolderNo.ToString();

                        txtHolderClass.Text = hc.Name;

                        DateTime RDT = DateTime.ParseExact(maHolder.RegDt, "M/d/yyyy", CultureInfo.InvariantCulture);
                        txtRegDate.Text = RDT.ToString("dd/MM/yyyy");

                        if (accountPlan == "CORP")
                        {
                            visibilityOldIC.Visible = false;
                            txtIdNoOld.Visible = false;

                            visivilityBirthDate.Visible = false;
                            txtBirthDate.Visible = false;
                            visibilityGender.Visible = false;
                            txtSex.Visible = false;
                            visibilityHandPhoneNo.Visible = false;
                            txtHandphoneNo.Visible = false;

                            visibilityRace.Visible = false;
                            txtRace.Visible = false;

                            //visibilityNoOfDependants.Visible = false;
                            //txtNoOfDependants.Visible = false;
                            corpContactPerson.Visible = true;
                            txtContactPerson.Text = maHolder.ContactPerson;
                            positionHeld.Text = maHolder.PositionHeld;
                            visibilityNationality.Visible = false;
                            txtNationality.Visible = false;
                            //visibilityIncome.Visible = false;
                            //txtIncome.Visible = false;
                            //visibilityOccupation.Visible = false;
                            //txtOccupation.Visible = false;


                            txtHolderAccType.Text = "Corporate Account";
                        }
                        else
                        {
                            corpContactPerson.Visible = false;
                        }

                        if (accountPlan == "EPF")
                        {
                            txtHolderAccType.Text = "EPF Account";
                            txtEpfType.Text = maHolder.EpfIStatus == "C" ? "-Conventional" : maHolder.EpfIStatus == "I" ? "-Islamic" : "";
                        }
                        if (accountPlan == "CASH")
                        {
                            txtHolderAccType.Text = "Cash Account";
                        }
                        if (accountPlan == "JOINT")
                        {
                            txtHolderAccType.Text = "Joint Account";
                        }
                        //Identity

                        txtIdNo.Text = maHolder.IdNo;
                        txtIdNoOld.Text = maHolder.IdNoOld;

                        //Personal


                        //Name 
                        String Name = maHolder.Name1 + (maHolder.Name2 != null && maHolder.Name2.Trim() != "" ? " " + maHolder.Name2.Trim() + (maHolder.Name3 != null && maHolder.Name3.Trim() != "" ? " " + maHolder.Name3.Trim() + (maHolder.Name4 != null && maHolder.Name4.Trim() != "" ? " " + maHolder.Name4.Trim() + (maHolder.Name5 != null && maHolder.Name5.Trim() != "" ? " " + maHolder.Name5.Trim() : "") : "") : "") : "");
                        String Salutation = maHolder.Salutation;

                        txtSex.Text = maHolder.Sex == "M" ? "Male" : maHolder.Sex == "F" ? "Female" : "N/A";
                        if (accountPlan == "CASH" || accountPlan == "CORP")
                        {
                            DateTime today = DateTime.Now;
                            int currentYear = Convert.ToInt32(today.Year.ToString().Substring(0, 2));
                            int IdYear = Convert.ToInt32(maHolder.IdNo.ToString().Substring(0, 2));
                            String YearA = (currentYear).ToString();
                            String YearB = (currentYear - 1).ToString();
                            Name = maHolder.Name1;
                            DateTime BDT = DateTime.ParseExact(maHolder.BirthDt, "M/d/yyyy", CultureInfo.InvariantCulture);

                            txtBirthDate.Text = (BDT.ToString("dd/MM/yyyy") == "01/01/0001 00:00:00" || BDT.ToString("dd/MM/yyyy") == "" || BDT.ToString("dd/MM/yyyy") == null ? maHolder.IdNo.ToString().Substring(4, 2) + "/" + maHolder.IdNo.ToString().Substring(2, 2) + "/" + (IdYear > currentYear ? YearB : YearA) : BDT.ToString("dd/MM/yyyy"));

                            txtTelNo.Text = (maHolder.TelNo == null || maHolder.TelNo == "" ? "-" : maHolder.TelNo.ToString());
                            txtHandphoneNo.Text = (maHolder.HandPhoneNo == null || maHolder.HandPhoneNo == "" ? "-" : maHolder.HandPhoneNo.ToString());
                            //txtIncome.Text = (CustomValues.GetIncomeByCode(maHolder.AccountType) == null ? "-" : CustomValues.GetIncomeByCode(maHolder.AccountType).ToLower());
                            //txtNoOfDependants.Text = (maHolder.NoOfDpndnt == null ? "-" : maHolder.NoOfDpndnt.ToString());
                            Response responseNDList = INationalityDefService.GetDataByFilter(" code = '" + maHolder.Nationality + "'", 0, 0, true);
                            if (responseNDList.IsSuccess)
                            {
                                List<NationalityDef> nds = (List<NationalityDef>)responseNDList.Data;
                                if (nds.Count > 0)
                                {
                                    txtNationality.Text = (nds.FirstOrDefault().Name == "" || nds.FirstOrDefault().Name == null ? "-" : nds.FirstOrDefault().Name);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseNDList.Message + "\", '');", true);
                            }
                            Response responseRDList = IRaceDefService.GetDataByFilter(" code='" + maHolder.Race + "'", 0, 0, true);
                            if (responseRDList.IsSuccess)
                            {
                                List<RaceDef> rds = (List<RaceDef>)responseRDList.Data;
                                if (rds.Count > 0)
                                {
                                    txtRace.Text = (rds.FirstOrDefault().Name == "" || rds.FirstOrDefault().Name == null ? "-" : rds.FirstOrDefault().Name.ToLower());
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseRDList.Message + "\", '');", true);
                            }
                        }
                        else if (accountPlan == "EPF")
                        {
                            DateTime today = DateTime.Now;
                            int currentYear = Convert.ToInt32(today.Year.ToString().Substring(0, 2));
                            int IdYear = Convert.ToInt32(maHolder.IdNo.ToString().Substring(0, 2));
                            String YearA = (currentYear).ToString();
                            String YearB = (currentYear - 1).ToString();
                            Name = maHolder.Name1;
                            DateTime BDT = DateTime.ParseExact(maHolder.BirthDt, "M/d/yyyy", CultureInfo.InvariantCulture);

                            txtBirthDate.Text = (BDT.ToString("dd/MM/yyyy") == "01/01/0001 00:00:00" || BDT.ToString("dd/MM/yyyy") == "" || BDT.ToString("dd/MM/yyyy") == null ? maHolder.IdNo.ToString().Substring(4, 2) + "/" + maHolder.IdNo.ToString().Substring(2, 2) + "/" + (IdYear > currentYear ? YearB : YearA) : BDT.ToString("dd/MM/yyyy"));


                            txtTelNo.Text = (maHolder.TelNo == null || maHolder.TelNo == "" ? "-" : maHolder.TelNo.ToString());
                            txtHandphoneNo.Text = (maHolder.HandPhoneNo == null || maHolder.HandPhoneNo == "" ? "-" : maHolder.HandPhoneNo.ToString());
                            //txtIncome.Text = (CustomValues.GetIncomeByCode(maHolder.AccountType) == null ? "-" : CustomValues.GetIncomeByCode(maHolder.AccountType).ToLower());
                            //txtNoOfDependants.Text = (maHolder.NoOfDpndnt == null ? "-" : maHolder.NoOfDpndnt.ToString());
                            Response responseNDList = INationalityDefService.GetDataByFilter(" code = '" + maHolder.Nationality + "'", 0, 0, true);
                            if (responseNDList.IsSuccess)
                            {
                                List<NationalityDef> nds = (List<NationalityDef>)responseNDList.Data;
                                if (nds.Count > 0)
                                {
                                    txtNationality.Text = (nds.FirstOrDefault().Name == "" || nds.FirstOrDefault().Name == null ? "-" : nds.FirstOrDefault().Name);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseNDList.Message + "\", '');", true);
                            }
                            Response responseRDList = IRaceDefService.GetDataByFilter(" code='" + maHolder.Race + "'", 0, 0, true);
                            if (responseRDList.IsSuccess)
                            {
                                List<RaceDef> rds = (List<RaceDef>)responseRDList.Data;
                                if (rds.Count > 0)
                                {
                                    txtRace.Text = (rds.FirstOrDefault().Name == "" || rds.FirstOrDefault().Name == null ? "-" : rds.FirstOrDefault().Name.ToLower());
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseRDList.Message + "\", '');", true);
                            }
                        }
                        else if (accountPlan == "JOINT")
                        {
                            Salutation = "";
                            if (primaryAccount.IsPrinciple == 0)
                            {
                                DateTime today = DateTime.Now;
                                int currentYear = Convert.ToInt32(today.Year.ToString().Substring(0, 2));
                                int IdYear = Convert.ToInt32(maHolder.IdNo2.ToString().Substring(0, 2));
                                String YearA = (currentYear).ToString();
                                String YearB = (currentYear - 1).ToString();

                                string bdt = maHolder.JointBirthDt != null && maHolder.JointBirthDt != "" ? DateTime.ParseExact(maHolder.JointBirthDt, "M/d/yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy") : "";


                                txtBirthDate.Text = (bdt == "" ? maHolder.IdNo2.ToString().Substring(4, 2) + "/" + maHolder.IdNo2.ToString().Substring(2, 2) + "/" + (IdYear > currentYear ? YearB : YearA) + maHolder.IdNo2.ToString().Substring(0, 2) : bdt);

                                txtIdNo.Text = (maHolder.IdNo2 == "" || maHolder.IdNo2 == null ? "-" : maHolder.IdNo2) + "<br/><small>(Principle: " + (maHolder.IdNo == "" || maHolder.IdNo == null ? "-" : maHolder.IdNo) + " )</small>";
                                txtIdNoOld.Text = (maHolder.IdNoOld2 == "" || maHolder.IdNoOld2 == null ? "-" : maHolder.IdNoOld2) + "<br/><small>(Principle: " + (maHolder.IdNoOld == "" || maHolder.IdNoOld == null ? "-" : maHolder.IdNoOld) + " )</small>";
                                Name = maHolder.Name2;
                                type.InnerHtml = "(Principle: " + maHolder.Name1 + ")";

                                txtSex.Text = maHolder.JointSex == "M" ? "Male" : maHolder.JointSex == "F" ? "Female" : "-";
                                txtTelNo.Text = "-";
                                txtHandphoneNo.Text = (maHolder.JointTelNo == null ? "-" : maHolder.JointTelNo);
                                //txtNoOfDependants.Text = (maHolder.JointNoOfDpndnt == null ? "-" : maHolder.JointNoOfDpndnt.ToString());
                                txtNationality.Text = "-";
                                if (maHolder.JointNationality != "" && maHolder.JointNationality != null)
                                {
                                    Response responseNDList = INationalityDefService.GetDataByFilter(" code = '" + maHolder.JointNationality + "'", 0, 0, true);
                                    if (responseNDList.IsSuccess)
                                    {
                                        List<NationalityDef> nds = (List<NationalityDef>)responseNDList.Data;
                                        if (nds.Count > 0)
                                        {
                                            txtNationality.Text = (nds.FirstOrDefault().Name == "" || nds.FirstOrDefault().Name == null ? "-" : nds.FirstOrDefault().Name);
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseNDList.Message + "\", '');", true);
                                    }
                                }
                                txtRace.Text = "-";
                                if (maHolder.JointRace != "" && maHolder.JointRace != null)
                                {
                                    Response responseRDList = IRaceDefService.GetDataByFilter(" code='" + maHolder.JointRace + "'", 0, 0, true);
                                    if (responseRDList.IsSuccess)
                                    {
                                        List<RaceDef> rds = (List<RaceDef>)responseRDList.Data;
                                        if (rds.Count > 0)
                                        {
                                            txtRace.Text = (rds.FirstOrDefault().Name == "" || rds.FirstOrDefault().Name == null ? "-" : rds.FirstOrDefault().Name.ToLower());
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseRDList.Message + "\", '');", true);
                                    }
                                }
                                telephoneNo.Visible = false;
                                //txtIncome.Visible = false;
                                //monthlyIncome.Visible = false;
                            }
                            else
                            {
                                txtIdNo.Text = (maHolder.IdNo == "" || maHolder.IdNo == null ? "-" : maHolder.IdNo) + "<br/><small>(Secondary: " + (maHolder.IdNoOld2 == "" || maHolder.IdNo2 == null ? "-" : maHolder.IdNo2) + " )</small>";
                                txtIdNoOld.Text = (maHolder.IdNoOld == "" || maHolder.IdNoOld == null ? "-" : maHolder.IdNoOld) + "<br/><small>(Secondary: " + (maHolder.IdNoOld2 == "" || maHolder.IdNoOld2 == null ? "-" : maHolder.IdNoOld2) + " )</small>";
                                Name = maHolder.Name1;
                                type.InnerHtml = "(Joint: " + maHolder.Name2 + ")";
                                DateTime BDT = DateTime.ParseExact(maHolder.BirthDt, "M/d/yyyy", CultureInfo.InvariantCulture);
                                txtBirthDate.Text = BDT.ToString("dd/MM/yyyy");
                                txtTelNo.Text = (maHolder.TelNo == null || maHolder.TelNo == "" ? "-" : maHolder.TelNo.ToString());
                                txtHandphoneNo.Text = (maHolder.HandPhoneNo == null ? "-" : maHolder.HandPhoneNo);
                                //txtNoOfDependants.Text = (maHolder.NoOfDpndnt == null ? "-" : maHolder.NoOfDpndnt.ToString());
                                //txtIncome.Text = (CustomValues.GetIncomeByCode(maHolder.AccountType) == null ? "-" : CustomValues.GetIncomeByCode(maHolder.AccountType).ToLower());
                                Response responseNDList = INationalityDefService.GetDataByFilter(" code = '" + maHolder.Nationality + "'", 0, 0, true);
                                if (responseNDList.IsSuccess)
                                {
                                    List<NationalityDef> nds = (List<NationalityDef>)responseNDList.Data;
                                    if (nds.Count > 0)
                                    {
                                        txtNationality.Text = (nds.FirstOrDefault().Name == "" || nds.FirstOrDefault().Name == null ? "" : nds.FirstOrDefault().Name);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseNDList.Message + "\", '');", true);
                                }
                                Response responseRDList = IRaceDefService.GetDataByFilter(" code='" + maHolder.Race + "'", 0, 0, true);
                                if (responseRDList.IsSuccess)
                                {
                                    List<RaceDef> rds = (List<RaceDef>)responseRDList.Data;
                                    if (rds.Count > 0)
                                    {
                                        txtRace.Text = (rds.FirstOrDefault().Name == "" || rds.FirstOrDefault().Name == null ? "-" : rds.FirstOrDefault().Name.ToLower());
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseRDList.Message + "\", '');", true);
                                }
                            }
                        }
                        txtName.Text = Name;
                        txtSalutation.Text = Salutation;


                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHCList.Message + "\", '');", true);
                    }


                    IsNotPrinciple.Visible = true;


                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', \"" + responseMHR.Message + "\", '');", true);
                }
            }
        }

        public void BindOrderHistory()
        {
            try
            {
                User user = new User();
                if (Session["user"] != null)
                {
                    user = (User)Session["user"];
                    Response response = IUserService.GetSingle(user.Id);
                    if (response.IsSuccess)
                    {
                        user = (User)response.Data;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                    }
                }
                if (ddlUserAccountId.SelectedValue != null)
                {
                    Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                        UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();
                        Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                        MaHolderReg maHolderReg = new MaHolderReg();
                        if (responseMHR.IsSuccess)
                        {
                            maHolderReg = (MaHolderReg)responseMHR.Data;
                            if (maHolderReg != null)
                            {
                                StringBuilder filter = new StringBuilder();
                                filter.Append("user_account_id=" + primaryAcc.Id.ToString());
                                filter.Append(" group by order_no");
                                Response responseUOList = IUserOrderService.GetDataByFilter(filter.ToString(), 0, 10, false);
                                if (responseUOList.IsSuccess)
                                {
                                    List<UserOrder> xs = (List<UserOrder>)responseUOList.Data;

                                    xs = xs.OrderByDescending(x => x.Id).ToList();
                                    int i = 0;
                                    StringBuilder sb = new StringBuilder();
                                    string type = "";
                                    string status = "";

                                    foreach (UserOrder x in xs)
                                    {
                                        i++;

                                        if (x.OrderType == 1)
                                            type = "Buy";
                                        else if (x.OrderType == 2)
                                            type = "Sell";
                                        else if (x.OrderType == 3 || x.OrderType == 4)
                                            type = "Switch";
                                        else if (x.OrderType == 6)
                                            type = "RSP";

                                        if (x.OrderStatus == 1)
                                            status = "Order Placed";
                                        else if (x.OrderStatus == 19)
                                            status = "Order Cancelled";
                                        else if (x.OrderStatus == 2)
                                            status = "Payment Pending";
                                        else if (x.OrderStatus == 3)
                                            status = "Payment Successful";
                                        else if (x.OrderStatus == 39)
                                            status = "Payment failed";

                                        Response response = IUtmcFundInformationService.GetSingle(x.FundId);
                                        if (response.IsSuccess)
                                        {
                                            UtmcFundInformation z = (UtmcFundInformation)response.Data;

                                            sb.Append(@"<tr>
                                        <td><a href='OrderConfirmation.aspx?OrderNo=" + x.OrderNo + "' target='_blank'>" + x.OrderNo + @"</a></td>
                                        <td>" + type + @"</td>
                                        <td>" + status + @"</td>
                                        <td>" + x.UpdatedDate + @"</td>
                                  </tr>");

                                            if (i == 10)
                                                break;
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                                        }
                                    }
                                    OrderHistory.InnerHtml = sb.ToString();
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUOList.Message + "\", '');", true);
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                    }
                    Response responseMonthlyIncome = ServicesManager.GetMonthlyIncomeData();
                    if (responseMonthlyIncome.IsSuccess)
                    {
                        List<IncomeDTO> incomeDTOs = (List<IncomeDTO>)responseMonthlyIncome.Data;
                        ddlMonthlyIncome.Items.Clear();
                        ddlMonthlyIncome.DataSource = incomeDTOs.Where(x => x.SDESC != "NIL").Select(x => new IncomeDTO { SDESC = x.SDESC.Capitalize() }).ToList();
                        ddlMonthlyIncome.DataTextField = "SDESC";
                        ddlMonthlyIncome.DataValueField = "INCOMECODE";
                        ddlMonthlyIncome.DataBind();
                        ddlMonthlyIncome.Items.Insert(0, new ListItem { Text = "Select Monthly Income", Value = "" });
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMonthlyIncome.Message + "\", '');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Setting Page BindOrderHistory " + ex.Message);
            }
        }

        public void BindSettings()
        {
            try
            {
                User user = (User)Session["user"];
                Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
                if (responseUSList.IsSuccess)
                {
                    List<UserSecurity> userSecurities = (List<UserSecurity>)responseUSList.Data;
                    UserSecurity emailSec = userSecurities.Where(x => x.UserSecurityTypeId == 1).FirstOrDefault();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUSList.Message + "\", '');", true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Setting Page BindSettings " + ex.Message);
            }
        }

        public void ViewOccupation(String OccCode, MaHolderReg maHolderReg)
        {
            Response responseCDList = ICountriesDefService.GetData(0, 0, false);
            CountriesDef countriesDef = new CountriesDef();
            if (responseCDList.IsSuccess)
            {
                txtOfficeCountry.Items.Clear();
                List<CountriesDef> countries = (List<CountriesDef>)responseCDList.Data;
                if (countries.Count > 0)
                {
                    ListItem brunei = new ListItem { Text = "Brunei", Value = "Brunei" };
                    ListItem singapore = new ListItem { Text = "Singapore", Value = "Singpore" };
                    ListItem malaysia = new ListItem { Text = "Malaysia", Value = "Malaysia" };

                    txtOfficeCountry.Items.Clear();
                    txtOfficeCountry.DataSource = countries.Where(x => x.Name != "MALAYSIA" && x.Name != "SINGAPORE" && x.Name != "BRUNEI").Select(x => new CountriesDef { Name = x.Name.Capitalize() }).ToList();
                    txtOfficeCountry.DataTextField = "Name";
                    txtOfficeCountry.DataValueField = "Name";
                    txtOfficeCountry.DataBind();
                    txtOfficeCountry.Items.Insert(0, brunei);
                    txtOfficeCountry.Items.Insert(0, singapore);
                    txtOfficeCountry.Items.Insert(0, malaysia);
                    txtOfficeCountry.Value = "Malaysia";
                    //countries.IndexOf(countriesDef);
                    //country.Disabled = true;


                    //ddlCountry.Disabled = true;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseCDList.Message + "\", '');", true);
            }
            //GET OCC DATA FROM ORACLE
            String occdesc = "";
            Response responseOccValue = ServicesManager.GetOccupationDescData(OccCode);
            if (responseOccValue.IsSuccess)
            {

                OccupationDTO occupationDTO = (OccupationDTO)responseOccValue.Data;
                occdesc = occupationDTO.OCCCODE;
                lblViewOccupation.InnerText = occdesc;
                ddlOccupation.Value = occupationDTO.DOWNLOADIND;
                txtNatureOfBusiness.InnerText = String.IsNullOrEmpty(maHolderReg.Nob) ? "-" : maHolderReg.Nob;
                //ddlNatureOfBusiness.Value = String.IsNullOrEmpty(maHolderReg.Nob) ? "-" : maHolderReg.Nob;
            }

            Response responseOcc = ServicesManager.GetOccupationData();
            if (responseOcc.IsSuccess)
            {
                List<OccupationDTO> occupationDTOs = (List<OccupationDTO>)responseOcc.Data;
                ddlOccupation.Items.Clear();
                ddlOccupation.DataSource = occupationDTOs;
                ddlOccupation.DataTextField = "OCCCODE";
                ddlOccupation.DataValueField = "DOWNLOADIND";
                ddlOccupation.DataBind();
                ddlOccupation.Items.Insert(0, new ListItem { Text = "Select Occupation", Value = "", Selected = true });

                //ddlFunderIndustry.Items.Clear();
                //ddlFunderIndustry.DataSource = occupationDTOs;
                //ddlFunderIndustry.DataTextField = "OCCCODE";
                //ddlFunderIndustry.DataValueField = "SDESC";
                //ddlFunderIndustry.DataBind();
                //ddlFunderIndustry.Items.Insert(0, new ListItem { Text = "Select Industry", Value = "", Selected = true });
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseOcc.Message + "\", '');", true);
            }
            /*

            lblEmployerName.InnerText = maHolderReg.NameOfEmployer == null ? "-" : maHolderReg.NameOfEmployer;
            txtIncome.Text = maHolderReg.AccountType;
            txtOfficeAddress1.Value = maHolderReg.OfficeAddress1 == null ? "-" : maHolderReg.OfficeAddress1;
            txtOfficeAddress2.Value = maHolderReg.OfficeAddress2 == null ? "-" : maHolderReg.OfficeAddress2;
            txtOfficeAddress3.Value = maHolderReg.OfficeAddress3 == null ? "-" : maHolderReg.OfficeAddress3;
            txtOfficePostCode.Value = maHolderReg.OfficePostCode == null ? "-" : maHolderReg.OfficePostCode;
            txtCity.Value = maHolderReg.OfficeCity == null ? "-" : maHolderReg.OfficeCity;
            ddlOfficeState.Value = maHolderReg.OfficeState == null ? "-" : maHolderReg.OfficeState;
            txtOfficeCountry.Value = maHolderReg.OfficeCountry == null ? "-" : maHolderReg.OfficeCountry;
            if (maHolderReg.OfficeTelNo == null)
            {
                txtTelephone.Value = "-";
            }
            else if (maHolderReg.OfficeTelNo.Substring(0, 1) == "0")
            {
                maHolderReg.OfficeTelNo = maHolderReg.OfficeTelNo.Substring(1, maHolderReg.OfficeTelNo.Length - 1);
                officeFrontNumber.Value = "60";
                txtTelephone.Value = maHolderReg.OfficeTelNo;
            }
            */

            // MURALI

            string result = string.Empty;
            StringBuilder filterQ = new StringBuilder();

            string mainQ = (@"select  mhr.OCC_CODE, mhr.ACCOUNT_TYPE,mhr.nob, mhr.OTP_VERSION, ocr.holder_no,mhr.name_of_employer,mhr.office_address_1,mhr.office_address_2,mhr.office_address_3,
mhr.office_postcode,mhr.office_city,mhr.office_state,mhr.office_country,mhr.office_tel_no
 from ma_holder_reg  mhr left join occupation_requests ocr on mhr.HOLDER_NO=ocr.HOLDER_NO where ocr.holder_no = '" + maHolderReg.HolderNo + "'  ORDER BY mhr.id DESC  ");

            Response reponseBankBinded = GenericService.GetDataByQuery(mainQ + filterQ.ToString(), 0, 1, false, null, false, null, true);
            if (reponseBankBinded.IsSuccess)
            {
                var uoDyn = reponseBankBinded.Data;
                var responseJSON = JsonConvert.SerializeObject(uoDyn);
                List<OccupationView> hbs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OccupationView>>(responseJSON);
                if (hbs.Count == 0)
                {
                    string value = "Not Binded";
                }
                else if (hbs.Count > 0)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    int idx = 1;
                    foreach (var hb in hbs)
                    {



                        if (hb.office_tel_no == null || hb.office_tel_no == "")
                        {
                            txtTelephone.Value = "-";
                        }
                        else if (hb.office_tel_no.ToString() == "N/A")
                        {
                            txtTelephone.Value = hb.office_tel_no;
                        }
                        else
                        {
                            officeFrontNumber.Value = "60";
                            txtTelephone.Value = hb.office_tel_no;
                        }
                        hdnTempOfficeNum.Value = txtTelephone.Value;
                        if (hb.OTP_VERSION == "0")
                        {
                            btnOccupationEdit.Disabled = true;
                            spanR.Visible = false;
                            spanP.Visible = true;
                            spanA.Visible = false;
                        }
                        else if (hb.OTP_VERSION == "1")
                        {
                            btnOccupationEdit.Disabled = false;
                            spanR.Visible = false;
                            spanP.Visible = false;
                            spanA.Visible = true;
                        }
                        else if (hb.OTP_VERSION == "9")
                        {
                            btnOccupationEdit.Disabled = false;
                            spanR.Visible = true;
                            spanP.Visible = false;
                            spanA.Visible = false;
                        }
                        if (occdesc == "Others" || occdesc == "Housewife" || occdesc == "Student" || occdesc == "Retiree")
                        {
                            divEmployedFields.Attributes.Add("class", "hide");
                            if (occdesc == "Others")
                            {
                                lblViewOccupation.InnerText = hb.OCC_CODE;
                            }
                            //txtIncome.Text = "N/A";
                            //txtOfficeCountry.Value = "N/A";
                            //txtTelephone.Value = "N/A";
                            //txtNatureOfBusiness.InnerText = "N/A";
                            //txtOfficeAddress1.Value = "N/A";
                            //txtOfficeAddress2.Value = "N/A";
                            //txtOfficeAddress3.Value = "N/A";
                            //txtOfficePostCode.Value = "N/A";
                            //txtCity.Value = "N/A";
                            //ddlOfficeState.Value = "N/A";
                            //lblEmployerName.InnerText = "N/A";
                        }
                        else
                        {
                            lblEmployerName.InnerText = (String.IsNullOrEmpty(hb.name_of_employer) ? "-" : hb.name_of_employer);
                            if (maHolderReg.AccountType != null)
                            {
                                Response responseMonthlyIncome = ServiceCalls.ServicesManager.GetMonthlyIncomeDataByCode(maHolderReg.AccountType);
                                if (responseMonthlyIncome.IsSuccess)
                                {
                                    IncomeDTO incomeDTO = (IncomeDTO)responseMonthlyIncome.Data;
                                    txtIncome.Text = incomeDTO.SDESC;
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "alert(\"" + responseMonthlyIncome.Message + "\");", true);
                                }
                            }
                            else
                            {
                                txtIncome.Text = String.IsNullOrEmpty(maHolderReg.AccountType) ? "-" : maHolderReg.AccountType;
                            }
                            txtOfficeAddress1.Value = String.IsNullOrEmpty(hb.office_address_1) ? "-" : hb.office_address_1;
                            txtOfficeAddress2.Value = String.IsNullOrEmpty(hb.office_address_2) ? "-" : hb.office_address_2;
                            txtOfficeAddress3.Value = String.IsNullOrEmpty(hb.office_address_3) ? "-" : hb.office_address_3;
                            txtOfficePostCode.Value = String.IsNullOrEmpty(hb.office_postcode) ? "-" : hb.office_postcode;
                            txtCity.Value = String.IsNullOrEmpty(hb.office_city) ? "-" : hb.office_city;
                            ddlOfficeState.Value = String.IsNullOrEmpty(hb.office_state) ? "-" : hb.office_state;
                            txtOfficeCountry.Value = String.IsNullOrEmpty(hb.office_country) ? "-" : hb.office_country;
                        }

                        hdnTempOccCode.Value = (maHolderReg.OccCode == "ZZ" ? "Others" : maHolderReg.OccCode);
                        txtOccupationDesc.Value = (maHolderReg.OccCode == "ZZ" ? hb.OCC_CODE : "");
                        hdnTempIncomeCode.Value = (maHolderReg.AccountType == "07" ? "" : maHolderReg.AccountType);
                        hdnTempNob.Value = maHolderReg.Nob;
                        hdnTempNoE.Value = lblEmployerName.InnerText;
                        hdnTempAddr1.Value = txtOfficeAddress1.Value;
                        hdnTempAddr2.Value = txtOfficeAddress2.Value;
                        hdnTempAddr3.Value = txtOfficeAddress3.Value;
                        hdnTempPostcode.Value = txtOfficePostCode.Value;
                        hdnTempCity.Value = txtCity.Value;
                        hdnTempState.Value = ddlOfficeState.Value;

                    }
                }

            }
        }

        public void BindBankApproval(int UserId)
        {
            Response responseBDList = IBanksDefService.GetData(0, 0, true);
            List<BanksDef> bankdefs2 = new List<BanksDef>();
            if (responseBDList.IsSuccess)
            {
                bankdefs2 = (List<BanksDef>)responseBDList.Data;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseBDList.Message + "\", '');", true);
            }
            string filter = " user_id = '" + UserId + "' and status=1 ";

            Response responseuserMHBList = IMaHolderBankService.GetDataByFilter(filter, 0, 0, true);
            if (responseuserMHBList.IsSuccess)
            {
                List<MaHolderBank> hbs = (List<MaHolderBank>)responseuserMHBList.Data;
                if (hbs.Count > 0)
                {
                    string accountfilter = " user_account_id=" + userAccount.Id + " and status=1 ";
                    var banks = GenericService.GetDataByFilter<UserAccountBanks>(accountfilter, 0, 0, false, null, true, null, false, false, null);
                    if (banks.IsSuccess)
                    {
                        List<UserAccountBanks> listBanks = (List<UserAccountBanks>)banks.Data;

                        StringBuilder stringBuilder = new StringBuilder();
                        if (listBanks.Count == 0)
                        {
                            bankDetailsBindStatus.InnerHtml = "Not Binded";
                            bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                        }
                        else
                        {
                            foreach (var hb in hbs)
                            {
                                if (bankdefs2.FirstOrDefault(a => a.Id == hb.BankDefId).Status == 1)
                                {
                                    bankDetailsBindStatus.InnerHtml = "Binded";
                                    bankDetailsBindStatus.Attributes.Add("class", "text-success");
                                }
                                else
                                {
                                    bankDetailsBindStatus.InnerHtml = "Binded Bank Inactive";
                                    bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                                }
                            }


                        }
                        Boolean IsImpersonate = ServicesManager.IsImpersonated(Context);
                        foreach (var hb in hbs)
                        {
                            string bankName = string.Empty;
                            if (bankdefs2.Count != 0)
                            {
                                if (bankdefs2.FirstOrDefault(a => a.Id == hb.BankDefId) != null)
                                {
                                    bankName = bankdefs2.FirstOrDefault(a => a.Id == hb.BankDefId).Name;
                                    string bankStatus = string.Empty;
                                    string image = !string.IsNullOrEmpty(hb.Image) ? "Image <i class='fa fa-eye'></i>" : "";
                                    UserAccountBanks userAccountBanks = listBanks.Where(x => x.MaHolderBankId == hb.Id).FirstOrDefault();
                                    if (userAccountBanks != null)
                                    {
                                        bankStatus = (IsImpersonate ? "-" : "<a href='javascript:;' data-id='" + hb.Id + "' class='btn btn-sm btn-primary unbindBank btn-block'>UnBind</a>");
                                    }
                                    else
                                    {
                                        bankStatus = (IsImpersonate ? "-" : "<a href='javascript:;' data-id='" + hb.Id + "' class='btn btn-sm btn-primary bindBank btn-block'>Bind</a>");
                                    }

                                    stringBuilder.Append(@"<tr>
                                        <td>" + userAccount.AccountNo + @"</td>
                                        <td>" + bankName + @"</td>
                                        <td>" + hb.BankAccountNo + @"</td>
                                        <td><a target='_blank' href='" + hb.Image + @"'>" + image + @"</a></td>
                                        <td>" + (userAccountBanks != null ? userAccountBanks.UpdatedDate.ToString("dd/MMM/yyyy HH:mm:ss") : "-") + @"</td>
                                        <td>" + (bankdefs2.FirstOrDefault(a => a.Id == hb.BankDefId).Status == 0 ? "Inactive bank" : bankStatus) + @"</td>
                                    </tr>");
                                }
                            }
                        }
                        tbodyUserBankAccounts.InnerHtml = stringBuilder.ToString();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + banks.Message + "\", '');", true);
                    }
                }
                else
                {
                    bankDetailsBindStatus.InnerHtml = "Not Added";
                    bankDetailsBindStatus.Attributes.Add("class", "text-danger");

                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseuserMHBList.Message + "\", '');", true);
            }
        }


        //public void bindBankDetails(User user, MaHolderReg maHolderReg, UserAccount primaryAccount)
        //{
        //    string acPlan = CustomValues.GetAccounPlan(primaryAccount.HolderClass);
        //    if (acPlan == "EPF")
        //    {
        //        bankDiv.Visible = false;
        //    }
        //    else
        //    {

        //    }
        //    //Bank Details
        //    ddlBank.Items.Clear();
        //    Response responseBDList = IBanksDefService.GetData(0, 0, true);
        //    List<BanksDef> bankdefs = new List<BanksDef>();
        //    if (responseBDList.IsSuccess)
        //    {
        //        bankdefs = (List<BanksDef>)responseBDList.Data;
        //        ListItem selectItem = new ListItem
        //        {
        //            Text = "Select Bank",
        //            Value = "0"
        //        };
        //        ddlBank.Items.Add(selectItem);
        //        foreach (BanksDef x in bankdefs.OrderBy(a => a.Name).ToList())
        //        {
        //            ListItem listItem = new ListItem
        //            {
        //                Text = x.Name,
        //                Value = x.Id.ToString()
        //            };
        //            listItem.Attributes.Add("accountnumlength", x.NoFormat);
        //            ddlBank.Items.Add(listItem);
        //        }
        //    }
        //    else
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseBDList.Message + "\", '');", true);
        //    }
        //    ddlAccountname.Items.Clear();
        //    if (CustomValues.GetAccounPlan(maHolderReg.HolderCls) == "JOINT")
        //    {
        //        ListItem listItem = new ListItem
        //        {
        //            Text = maHolderReg.Name1,
        //            Value = maHolderReg.Name1
        //        };
        //        ddlAccountname.Items.Add(listItem);

        //    }
        //    else
        //    {
        //        ddlAccountname.Items.Add(new ListItem(maHolderReg.Name1, maHolderReg.Name1));
        //    }
        //    bankdetail.InnerHtml = "";
        //    bankdenied.Visible = false;

        //    Response responseMHBList = IMaHolderBankService.GetDataByPropertyName(nameof(MaHolderBank.UserAccountId), primaryAccount.Id.ToString(), true, 0, 0, true);
        //    if (responseMHBList.IsSuccess)
        //    {
        //        List<MaHolderBank> hbs = (List<MaHolderBank>)responseMHBList.Data;
        //        if (hbs.Count > 0)
        //        {
        //            MaHolderBank hb = hbs.FirstOrDefault(x => x.Status == 1);
        //            if (hb != null)
        //            {
        //                hb.BankDefIdBanksDef = bankdefs.FirstOrDefault(x => x.Id == hb.BankDefId);
        //                hdnBankNoFormat.Value = hb.BankDefIdBanksDef.NoFormat;
        //                if (CustomValues.GetAccounPlan(maHolderReg.HolderCls) == "JOINT")
        //                {
        //                    ddlAccountname.SelectedValue = hb.AccountName;
        //                }

        //                if (hb.Status == 1)
        //                {
        //                    ddlBank.SelectedValue = hb.BankDefId.ToString();
        //                    hb.BankDefIdBanksDef = bankdefs.Where(x => x.Id == hb.BankDefId).FirstOrDefault();
        //                    hdnBankNoFormat.Value = hb.BankDefIdBanksDef.NoFormat;
        //                    txtAccountNumber.Text = hb.BankAccountNo;
        //                    hdnAccountNumber.Value = hb.BankAccountNo;
        //                    hdnMaHolderBankId.Value = hb.BankDefId.ToString();
        //                    lblBankAttachment.Text = !string.IsNullOrEmpty(hb.Image) ? hb.Image : "";
        //                }
        //            }
        //            if (hbs.FirstOrDefault().Status != 1)
        //            {
        //                hb = hbs.FirstOrDefault(x => x.Status == 0 || x.Status == 9);
        //                if (hb != null)
        //                {
        //                    if (hb.Status == 0 || hb.Status == 9)
        //                    {
        //                        hb.BankDefIdBanksDef = bankdefs.Where(x => x.Id == hb.BankDefId).LastOrDefault();
        //                        string htmlPending = "<tr><th>Bank Name</th><td>" + hb.BankDefIdBanksDef.Name + "</td></tr>";
        //                        htmlPending += "<tr><th>Account Name</th><td>" + hb.AccountName + "</td></tr>";
        //                        htmlPending += "<tr><th>Account Number</th><td>" + hb.BankAccountNo + "</td></tr>";
        //                        bankdetail.InnerHtml = htmlPending;
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            hdnBankNoFormat.Value = bankdefs.FirstOrDefault().NoFormat;
        //            hdnBankNoFormat.Value = "";
        //            hdnAccountNumber.Value = "";
        //            hdnMaHolderBankId.Value = "";
        //            txtAccountNumber.Text = "";
        //            ddlBank.SelectedValue = "0";
        //            abank.InnerText = "Update";
        //        }
        //    }
        //    else
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHBList.Message + "\", '');", true);
        //    }
        //    BindBankApproval(primaryAccount);
        //}

        public void BindBankApproval(UserAccount primaryAccount)
        {
            banknotverified.Visible = false;
            bankverified.Visible = false;
            bankdenied.Visible = false;

            User user = (User)Session["user"];

            if (Session["user"] != null)
            {
                Response responseBA = IMaHolderBankService.GetDataByPropertyName(nameof(MaHolderBank.UserAccountId), primaryAccount.Id.ToString(), true, 0, 0, true);
                if (responseBA.IsSuccess)
                {
                    List<MaHolderBank> maHolderBanks = (List<MaHolderBank>)responseBA.Data;
                    MaHolderBank ba = maHolderBanks.FirstOrDefault();
                    if (ba == null)
                    {
                        bankDetailsBindStatus.InnerHtml = "Not Binded";
                        bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                    }
                    if (ba != null)
                    {
                        if (ba.Status == 0)
                        {
                            //pending
                            banknotverified.Visible = true;
                            bankDetailsBindStatus.InnerHtml = "Not Binded";
                            bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                        }
                        else if (ba.Status == 1)
                        {
                            //approved
                            bankverified.Visible = true;
                            bankDetailsBindStatus.InnerHtml = "Binded";
                            bankDetailsBindStatus.Attributes.Add("class", "text-success");
                        }
                        else
                        {
                            //denied
                            bankdenied.Visible = true;
                            bankDetailsBindStatus.InnerHtml = "Not Binded";
                            bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseBA.Message + "\", '');", true);
                }
            }
            else
            {
                if (Request.Browser.IsMobileDevice)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                }
            }
        }

        public void btnAddressSubmit_Click(object sender, EventArgs e)
        {
            if (txtAddressLine1.Text != "" && txtAddressLine2.Text != "" && txtPostCode.Text != "" && ddlCountry.SelectedValue != "" && ddlState.SelectedValue != "" && txtCurrentPasswordAddress.Text != "")
            {
                if (Int32.TryParse(txtPostCode.Text.Trim(), out int result) && txtPostCode.Text.Length == 5)
                {
                    if (txtCurrentPasswordAddress.Text != null || txtCurrentPasswordAddress.Text != "")
                    {
                        User user = (User)Session["user"];
                        string encryptedPassword = CustomEncryptorDecryptor.EncryptPassword(txtCurrentPasswordAddress.Text);
                        if (user.Password == encryptedPassword)
                        {
                            Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                            if (responseUAList.IsSuccess)
                            {
                                List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                                UserAccount primaryAccount = userAccounts.Where(x => x.IsPrimary == 1).FirstOrDefault();
                                if (ddlUserAccountId.SelectedValue != null)
                                {
                                    primaryAccount = userAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();
                                }

                                Response responseMHR1 = IMaHolderRegService.GetDataByPropertyName(nameof(MaHolderReg.HolderNo), primaryAccount.AccountNo, true, 0, 0, false);
                                MaHolderReg mHR = new MaHolderReg();
                                if (responseMHR1.IsSuccess)
                                {
                                    mHR = ((List<MaHolderReg>)responseMHR1.Data).FirstOrDefault();
                                    if (mHR != null)
                                    {
                                        mHR.Addr1 = txtAddressLine1.Text;
                                        mHR.Addr2 = txtAddressLine2.Text;
                                        mHR.Addr3 = txtAddressLine3.Text;
                                        mHR.Addr4 = txtAddressLine4.Text;
                                        mHR.Postcode = Convert.ToInt32(txtPostCode.Text);

                                        mHR.StateCode = Convert.ToInt32(ddlState.SelectedValue);
                                        mHR.OtpActSt = "0";
                                        mHR.OtpExpiryDate = DateTime.Now;
                                        Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAccount.AccountNo);
                                        MaHolderReg OrimHR = new MaHolderReg();
                                        if (responseMHR.IsSuccess)
                                        {
                                            OrimHR = (MaHolderReg)responseMHR.Data;
                                            OrimHR.OtpActSt = "0";
                                            OrimHR.HandPhoneNo = mHR.HandPhoneNo;
                                            OrimHR.Id = mHR.Id;
                                            OrimHR.OtpExpiryDate = mHR.OtpExpiryDate;
                                            OrimHR.OtpEntDt = mHR.OtpEntDt;
                                            OrimHR.OtpEntDt = mHR.OtpEntDt;
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR.Message + "\", '');", true);
                                        }
                                        foreach (var propertyInfo in OrimHR.GetType().GetProperties())
                                        {
                                            if (propertyInfo.PropertyType == typeof(string))
                                            {
                                                if (propertyInfo.GetValue(OrimHR, null) == null)
                                                {
                                                    propertyInfo.SetValue(OrimHR, "", null);
                                                }
                                            }
                                        }


                                        Response responseMHR2 = IMaHolderRegService.UpdateData(mHR);
                                        if (responseMHR2.IsSuccess)
                                        {
                                            UserLogMain ulm = new UserLogMain()
                                            {
                                                TableName = "ma_holder_reg",
                                                Description = "Request address change successful",
                                                UserId = user.Id,
                                                UserAccountId = primaryAccount.Id,
                                                UpdatedDate = DateTime.Now,
                                                RefId = mHR.Id,
                                                RefValue = mHR.HolderNo.Value.ToString(),
                                                StatusType = 1
                                            };

                                            Response response = IUserLogMainService.PostData(ulm);
                                            ulm = (UserLogMain)response.Data;
                                            //COMPARE 2 OBJECT

                                            int propertyCount = typeof(MaHolderReg).GetProperties().Length;

                                            CompareLogic basicComparison = new CompareLogic()
                                            { Config = new ComparisonConfig() { MaxDifferences = propertyCount } };
                                            List<Difference> diffs = basicComparison.Compare(OrimHR, mHR).Differences;
                                            int noOfDifferent = diffs.Count();

                                            foreach (Difference diff in diffs)
                                            {
                                                string columnName = Converter.GetColumnNameByPropertyName<MaHolderReg>(diff.PropertyName);

                                                UserLogSub x = new UserLogSub()
                                                {
                                                    UserLogMainId = ulm.Id,
                                                    ColumnName = columnName,
                                                    ValueOld = diff.Object1Value,
                                                    ValueNew = diff.Object2Value,
                                                };
                                                Response response1 = IUserLogSubService.PostData(x);
                                                x = (UserLogSub)response1.Data;
                                            }
                                            Email email = new Email();
                                            email.user = user;
                                            EmailService.SendAlertUpdateMail(email, primaryAccount.AccountNo, "Address update notification", "Address change - " + primaryAccount.AccountNo + " has been requested", false, "");
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Address change request sent.', 'Settings.aspx?accNo=" + primaryAccount.AccountNo + "');", true);

                                            bindAddress(primaryAccount);
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR2.Message + "\", '');", true);
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please contact Apex admin to update.', '');", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR1.Message + "\", '');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('address');ShowCustomMessage('Alert', 'Wrong password.', '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('address');ShowCustomMessage('Alert', 'Please enter password.', '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('address');ShowCustomMessage('Alert', 'Invalid Postcode.', '');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('address');ShowCustomMessage('Alert', 'Please enter required fields.', '');", true);
            }
        }
        protected void btnSubmitEditProfile_Click(object sender, EventArgs e)
        {

        }
        protected void btnSubmitEditOccupation_Click(object sender, EventArgs e)
        {
            User user = (User)Session["user"];
            Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
            if (responseUAList.IsSuccess)
            {
                List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;

                UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();


                Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                MaHolderReg maHolderReg = new MaHolderReg();
                if (responseMHR.IsSuccess)
                {
                    maHolderReg = (MaHolderReg)responseMHR.Data;

                    List<CustomValidation> customValidations = new List<CustomValidation>();
                    Response responseMHR1 = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                    MaHolderReg mHR = new MaHolderReg();
                    if (responseMHR1.IsSuccess)
                    {
                        mHR = (MaHolderReg)responseMHR1.Data;
                        string accountPlan = CustomValues.GetAccounPlan(maHolderReg.HolderCls);
                        if (txtName.Text != "")
                        {
                            Boolean canContinue = false;
                            if (accountPlan == "CASH" && txtHandphoneNo.Text != "" /*&& txtNoOfDependants.Text != ""*/)
                            {
                                canContinue = true;
                            }
                            else if (accountPlan == "JOINT" && txtHandphoneNo.Text != ""/* && (txtNoOfDependants.Text == "0" || txtNoOfDependants.Text == "")*/)
                            {
                                canContinue = true;
                            }
                            else if (accountPlan == "EPF" && txtHandphoneNo.Text != "" /*&& (txtNoOfDependants.Text == "0" || txtNoOfDependants.Text == "")*/)
                            {
                                canContinue = true;
                            }
                            else if (accountPlan == "CORP" && txtHandphoneNo.Text == "" /*&& (txtNoOfDependants.Text == "0" || txtNoOfDependants.Text == "")*/)
                            {
                                canContinue = true;
                            }
                            if (canContinue)
                            {
                                if (txtOccupationPassword.Text != "")
                                {
                                    string encryptedPassword = CustomEncryptorDecryptor.EncryptPassword(txtOccupationPassword.Text);
                                    if (user.Password == encryptedPassword)
                                    {
                                        if (
                                    (Employed.Checked == false && Unemployed.Checked == false)
                                    ||
                                    ddlOccupation.Value == "" ||
                                    (ddlOccupation.Value == "Others" && txtOccupationDesc.Value == "") ||
                                        (
                                            Employed.Checked == true &&
                                            (
                                                txtEmployerName.Value == "" || ddlNatureOfBusiness.Value == "" || ddlMonthlyIncome.Value == "" || txtOfficeAddress1.Value == "" || txtCity.Value == "" || txtOfficePostCode.Value == "" || ddlOfficeState.Value == "" || txtOfficeCountry.Value == ""
                                            )
                                        )
                                    )
                                        {
                                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('occupation');ShowCustomMessage('Alert', 'Please enter all required fields.', '');", true);

                                            if (Employed.Checked == false && Unemployed.Checked == false)
                                            {
                                                customValidations.Add(new CustomValidation { Name = "checkbox-group-2", Message = "Employment status is required" });
                                            }
                                            if (ddlOccupation.Value == "")
                                            {
                                                if (Employed.Checked == true)
                                                    customValidations.Add(new CustomValidation { Name = "ddlOccupation", Message = "Occupation is required" });
                                                else if (Unemployed.Checked == true)
                                                    customValidations.Add(new CustomValidation { Name = "ddlOccupation", Message = "Status is required" });
                                            }
                                            if (ddlOccupation.Value == "Others" && txtOccupationDesc.Value == "")
                                                customValidations.Add(new CustomValidation { Name = "txtOccupationDesc", Message = "Occupation description is required" });
                                            if (Employed.Checked == true && txtEmployerName.Value == "")
                                                customValidations.Add(new CustomValidation { Name = "txtEmployerName", Message = "Employer name is required" });
                                            if (Employed.Checked == true && (ddlNatureOfBusiness.Value == "" || ddlNatureOfBusiness.Value == "Select"))
                                                customValidations.Add(new CustomValidation { Name = "ddlNatureOfBusiness", Message = "Nature of business is required" });
                                            if (Employed.Checked == true && (ddlMonthlyIncome.Value == "" || ddlMonthlyIncome.Value == "Select"))
                                                customValidations.Add(new CustomValidation { Name = "ddlMonthlyIncome", Message = "Monthly income is required" });

                                            if (Employed.Checked == true && txtOfficeAddress1.Value == "")
                                                customValidations.Add(new CustomValidation { Name = "txtOfficeAddress1", Message = "Office address is required" });
                                            if (Employed.Checked == true && txtCity.Value == "")
                                                customValidations.Add(new CustomValidation { Name = "txtCity", Message = "Office city is required" });
                                            if (Employed.Checked == true && txtOfficePostCode.Value == "")
                                                customValidations.Add(new CustomValidation { Name = "txtOfficePostCode", Message = "Office city is required" });
                                            if (Employed.Checked == true && ddlOfficeState.Value == "")
                                                customValidations.Add(new CustomValidation { Name = "ddlOfficeState", Message = "Office state is required" });
                                            //if (Employed.Checked == true && txtOfficeCountry.Value == "")
                                            //    customValidations.Add(new CustomValidation { Name = "txtOfficeCountry", Message = "Office country is required" });
                                            //if (Employed != null && txtTelephone == "")
                                            //    customValidations.Add(new CustomValidation { Name = "txtTelephone", Message = "Office telephone number is required" });
                                            //response.IsSuccess = false;
                                            //response.Message = "Please fill in details";
                                        }

                                        if (Unemployed.Checked == true)
                                        {

                                            txtEmployerName.Value = "";
                                            ddlNatureOfBusiness.Value = "";
                                            ddlMonthlyIncome.Value = "";
                                            ddlMonthlyIncome.Value = "";
                                            txtOfficeAddress1.Value = "";
                                            txtOfficeAddress2.Value = "";
                                            txtOfficeAddress3.Value = "";
                                            txtCity.Value = "";
                                            txtOfficePostCode.Value = "";
                                            ddlOfficeState.Value = "";
                                            txtOfficeCountry.Value = "";

                                            if (hdnEmployedOccupation.Value.ToString() == "Student" || hdnEmployedOccupation.Value.ToString() == "Retired" || hdnEmployedOccupation.Value.ToString() == "Housewife" || (hdnEmployedOccupation.Value.ToString() == "Others" && txtOccupationDesc.Value != ""))
                                            {
                                                Response responseMHRID = GenericService.GetDataByQuery("select * from ma_holder_reg where holder_no = '" + maHolderReg.HolderNo.ToString() + "' order by Id desc", 0, 20, false, null, false, null, false);
                                                if (responseMHRID.IsSuccess)
                                                {
                                                    var MaHolderRegIDDyn = responseMHRID.Data;
                                                    var responseJSON = JsonConvert.SerializeObject(MaHolderRegIDDyn);
                                                    List<MaHolderReg> MaHolderRegID = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MaHolderReg>>(responseJSON);

                                                    MaHolderReg maHolderRegLocal = MaHolderRegID.FirstOrDefault();
                                                    maHolderReg.Id = maHolderRegLocal.Id;
                                                    maHolderReg.OccCode = maHolderRegLocal.OccCode;
                                                    maHolderReg.NameOfEmployer = maHolderRegLocal.NameOfEmployer;
                                                    maHolderReg.OfficeAddress1 = maHolderRegLocal.OfficeAddress1;
                                                    maHolderReg.OfficeAddress2 = maHolderRegLocal.OfficeAddress2;
                                                    maHolderReg.OfficeAddress3 = maHolderRegLocal.OfficeAddress3;
                                                    maHolderReg.OfficeCity = maHolderRegLocal.OfficeCity;
                                                    maHolderReg.OfficeState = maHolderRegLocal.OfficeState;
                                                    maHolderReg.OfficeCountry = maHolderRegLocal.OfficeCountry;
                                                    maHolderReg.OfficeTelNo = maHolderRegLocal.OfficeTelNo;
                                                    maHolderReg.OfficePostCode = maHolderRegLocal.OfficePostCode;

                                                    mHR.OccCode = maHolderRegLocal.OccCode;
                                                    mHR.NameOfEmployer = maHolderRegLocal.NameOfEmployer;
                                                    mHR.OfficeAddress1 = maHolderRegLocal.OfficeAddress1;
                                                    mHR.OfficeAddress2 = maHolderRegLocal.OfficeAddress2;
                                                    mHR.OfficeAddress3 = maHolderRegLocal.OfficeAddress3;
                                                    mHR.OfficeCity = maHolderRegLocal.OfficeCity;
                                                    mHR.OfficeState = maHolderRegLocal.OfficeState;
                                                    mHR.OfficeCountry = maHolderRegLocal.OfficeCountry;
                                                    mHR.OfficeTelNo = maHolderRegLocal.OfficeTelNo;
                                                    mHR.OfficePostCode = maHolderRegLocal.OfficePostCode;
                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR1.Message + "\", '');", true);
                                                }
                                                //Response responseOR = GenericService.GetDataByQuery("", 0, 20, false, null, false, null, false);
                                                OccupationRequests occupationRequest = new OccupationRequests();

                                                //maHolderReg.OccCode = (hdnEmployedOccupation.Value.ToString() == "Others" ? txtOccupationDesc.Value : hdnEmployedOccupation.Value.ToString());
                                                //maHolderReg.Nob = "";
                                                //maHolderReg.AccountType = "";
                                                occupationRequest.OccCode = (hdnEmployedOccupation.Value.ToString() == "Others" ? txtOccupationDesc.Value : hdnEmployedOccupation.Value.ToString());
                                                occupationRequest.IncomeCode = "";
                                                occupationRequest.Nob = "";
                                                occupationRequest.NameOfEmployer = "";
                                                occupationRequest.Addr1 = "";
                                                occupationRequest.HolderNo = maHolderReg.HolderNo.ToString();
                                                occupationRequest.Postcode = "";
                                                occupationRequest.City = "";
                                                occupationRequest.State = "";
                                                occupationRequest.Country = "";
                                                occupationRequest.OfficeTelNo2 = "";
                                                occupationRequest.Isemployed = 0;
                                                maHolderReg.OtpVersion = "0";
                                                maHolderReg.OtpEntBy = DateTime.Now;


                                                int propertyCount = typeof(MaHolderReg).GetProperties().Length;

                                                CompareLogic basicComparison = new CompareLogic()
                                                { Config = new ComparisonConfig() { MaxDifferences = propertyCount } };
                                                List<Difference> diffs = basicComparison.Compare(mHR, maHolderReg).Differences;
                                                int noOfDifferent = diffs.Count();

                                                if (noOfDifferent > 0)
                                                {
                                                    Response responseEOR = GenericService.GetCountByQuery("select count(*) from occupation_requests where holder_no = '" + maHolderReg.HolderNo.ToString() + "'");
                                                    if (responseEOR.IsSuccess)
                                                    {
                                                        var OccupationRequestDyn = responseEOR.Data;
                                                        var responseJSON = JsonConvert.SerializeObject(OccupationRequestDyn);
                                                        int occupationRequests = Newtonsoft.Json.JsonConvert.DeserializeObject<int>(responseJSON);

                                                        if (occupationRequests > 0)
                                                        {
                                                            Response responseOcc = GenericService.GetDataByQuery("select * from occupation_requests where holder_no = '" + maHolderReg.HolderNo.ToString() + "'", 0, 1, false, null, false, null, false);
                                                            if (responseOcc.IsSuccess)
                                                            {
                                                                var OccupationRequestData = responseOcc.Data;
                                                                var occResponseJSON = JsonConvert.SerializeObject(OccupationRequestData);
                                                                List<OccupationRequests> occupationChangeRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OccupationRequests>>(occResponseJSON);

                                                                occupationRequest.Id = occupationChangeRequest.FirstOrDefault().Id;
                                                                Response responseOR = GenericService.UpdateData<OccupationRequests>(occupationRequest);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Response responseOR = GenericService.PostData<OccupationRequests>(occupationRequest);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR1.Message + "\", '');", true);
                                                    }
                                                    //Response responseOR = GenericService.PostData<OccupationRequests>(occupationRequest);
                                                    Response responseMaHR = IMaHolderRegService.UpdateData(maHolderReg);


                                                    UserLogMain ulm = new UserLogMain()
                                                    {
                                                        TableName = "holder_reg",
                                                        Description = "User update ma profile",
                                                        UserId = user.Id,
                                                        UserAccountId = primaryAcc.Id,
                                                        UpdatedDate = DateTime.Now,
                                                        RefId = maHolderReg.Id,
                                                        RefValue = maHolderReg.HolderNo.Value.ToString(),
                                                        StatusType = 1
                                                    };

                                                    Response response = IUserLogMainService.PostData(ulm);
                                                    ulm = (UserLogMain)response.Data;


                                                    foreach (Difference diff in diffs)
                                                    {
                                                        string columnName = Converter.GetColumnNameByPropertyName<MaHolderReg>(diff.PropertyName);

                                                        UserLogSub x = new UserLogSub()
                                                        {
                                                            UserLogMainId = ulm.Id,
                                                            ColumnName = columnName,
                                                            ValueOld = diff.Object1Value,
                                                            ValueNew = diff.Object2Value,
                                                        };
                                                        Response response1 = IUserLogSubService.PostData(x);
                                                        x = (UserLogSub)response1.Data;

                                                    }


                                                    divalert.Visible = true;
                                                    divalert.InnerText = "Success! Profile successfully updated.";
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Occupation change request sent successfully.', '');", true);

                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('occupation');ShowCustomMessage('Alert', 'No changes!', '');", true);
                                                }
                                                btnOccupationEdit.Disabled = true;
                                                spanR.Visible = false;
                                                spanP.Visible = true;
                                                spanA.Visible = false;
                                            }
                                            else
                                            {

                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('occupation');ShowCustomMessage('Alert', 'Please enter your occupation status.', '');", true);
                                            }
                                        }
                                        else
                                        {
                                            if (Employed.Checked == true && txtEmployerName.Value != "" && ddlNatureOfBusiness.Value != "" && ddlMonthlyIncome.Value != "" && txtOfficeAddress1.Value != "" && txtCity.Value != "" && txtOfficePostCode.Value != "" && ddlOfficeState.Value != "" && txtOfficeCountry.Value != "" && txtTelephone.Value != "")
                                            {
                                                Response responseMHRID = GenericService.GetDataByQuery("select * from ma_holder_reg where holder_no = '" + maHolderReg.HolderNo.ToString() + "' order by Id desc", 0, 20, false, null, false, null, false);
                                                if (responseMHRID.IsSuccess)
                                                {
                                                    var MaHolderRegIDDyn = responseMHRID.Data;
                                                    var responseJSON = JsonConvert.SerializeObject(MaHolderRegIDDyn);
                                                    List<MaHolderReg> MaHolderRegID = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MaHolderReg>>(responseJSON);

                                                    MaHolderReg maHolderRegLocal = MaHolderRegID.FirstOrDefault();
                                                    maHolderReg.Id = maHolderRegLocal.Id;
                                                    maHolderReg.OccCode = maHolderRegLocal.OccCode;
                                                    maHolderReg.NameOfEmployer = maHolderRegLocal.NameOfEmployer;
                                                    maHolderReg.OfficeAddress1 = maHolderRegLocal.OfficeAddress1;
                                                    maHolderReg.OfficeAddress2 = maHolderRegLocal.OfficeAddress2;
                                                    maHolderReg.OfficeAddress3 = maHolderRegLocal.OfficeAddress3;
                                                    maHolderReg.OfficeCity = maHolderRegLocal.OfficeCity;
                                                    maHolderReg.OfficeState = maHolderRegLocal.OfficeState;
                                                    maHolderReg.OfficeCountry = maHolderRegLocal.OfficeCountry;
                                                    maHolderReg.OfficeTelNo = maHolderRegLocal.OfficeTelNo;
                                                    maHolderReg.OfficePostCode = maHolderRegLocal.OfficePostCode;

                                                    mHR.OccCode = maHolderRegLocal.OccCode;
                                                    mHR.NameOfEmployer = maHolderRegLocal.NameOfEmployer;
                                                    mHR.OfficeAddress1 = maHolderRegLocal.OfficeAddress1;
                                                    mHR.OfficeAddress2 = maHolderRegLocal.OfficeAddress2;
                                                    mHR.OfficeAddress3 = maHolderRegLocal.OfficeAddress3;
                                                    mHR.OfficeCity = maHolderRegLocal.OfficeCity;
                                                    mHR.OfficeState = maHolderRegLocal.OfficeState;
                                                    mHR.OfficeCountry = maHolderRegLocal.OfficeCountry;
                                                    mHR.OfficeTelNo = maHolderRegLocal.OfficeTelNo;
                                                    mHR.OfficePostCode = maHolderRegLocal.OfficePostCode;
                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR1.Message + "\", '');", true);
                                                }
                                                //Response responseOR = GenericService.GetDataByQuery("", 0, 20, false, null, false, null, false);
                                                OccupationRequests occupationRequest = new OccupationRequests();

                                                occupationRequest.OccCode = hdnEmployedOccupation.Value;
                                                occupationRequest.IncomeCode = ddlMonthlyIncome.Value;
                                                occupationRequest.Nob = ddlNatureOfBusiness.Value;
                                                occupationRequest.NameOfEmployer = txtEmployerName.Value;
                                                occupationRequest.Addr1 = txtOfficeAddress1.Value;
                                                occupationRequest.Addr2 = txtOfficeAddress2.Value;
                                                occupationRequest.Addr3 = txtOfficeAddress3.Value;
                                                occupationRequest.HolderNo = maHolderReg.HolderNo.ToString();
                                                occupationRequest.Postcode = txtOfficePostCode.Value;
                                                occupationRequest.City = txtCity.Value;
                                                occupationRequest.State = ddlOfficeState.Value;
                                                occupationRequest.Country = txtOfficeCountry.Value;
                                                occupationRequest.OfficeTelNo2 = txtTelephone.Value;
                                                occupationRequest.Isemployed = 1;
                                                maHolderReg.OtpVersion = "0";
                                                maHolderReg.OtpEntBy = DateTime.Now;



                                                //maHolderReg.NoOfDpndnt = Convert.ToInt32(txtNoOfDependants.Text);
                                                //COMPARE 2 OBJECT

                                                int propertyCount = typeof(MaHolderReg).GetProperties().Length;

                                                CompareLogic basicComparison = new CompareLogic()
                                                { Config = new ComparisonConfig() { MaxDifferences = propertyCount } };
                                                List<Difference> diffs = basicComparison.Compare(mHR, maHolderReg).Differences;
                                                int noOfDifferent = diffs.Count();

                                                if (noOfDifferent > 0)
                                                {
                                                    Response responseEOR = GenericService.GetCountByQuery("select count(*) from occupation_requests where holder_no = '" + maHolderReg.HolderNo.ToString() + "'");
                                                    if (responseEOR.IsSuccess)
                                                    {
                                                        var OccupationRequestDyn = responseEOR.Data;
                                                        var responseJSON = JsonConvert.SerializeObject(OccupationRequestDyn);
                                                        int occupationRequests = Newtonsoft.Json.JsonConvert.DeserializeObject<int>(responseJSON);

                                                        if (occupationRequests > 0)
                                                        {
                                                            Response responseOcc = GenericService.GetDataByQuery("select * from occupation_requests where holder_no = '" + maHolderReg.HolderNo.ToString() + "'", 0, 1, false, null, false, null, false);
                                                            if (responseOcc.IsSuccess)
                                                            {
                                                                var OccupationRequestData = responseOcc.Data;
                                                                var occResponseJSON = JsonConvert.SerializeObject(OccupationRequestData);
                                                                List<OccupationRequests> occupationChangeRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OccupationRequests>>(occResponseJSON);

                                                                occupationRequest.Id = occupationChangeRequest.FirstOrDefault().Id;
                                                                Response responseOR = GenericService.UpdateData<OccupationRequests>(occupationRequest);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Response responseOR = GenericService.PostData<OccupationRequests>(occupationRequest);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR1.Message + "\", '');", true);
                                                    }
                                                    //Response responseOR = GenericService.PostData<OccupationRequests>(occupationRequest);
                                                    Response responseMaHR = IMaHolderRegService.UpdateData(maHolderReg);


                                                    UserLogMain ulm = new UserLogMain()
                                                    {
                                                        TableName = "holder_reg",
                                                        Description = "User update ma profile",
                                                        UserId = user.Id,
                                                        UserAccountId = primaryAcc.Id,
                                                        UpdatedDate = DateTime.Now,
                                                        RefId = maHolderReg.Id,
                                                        RefValue = maHolderReg.HolderNo.Value.ToString(),
                                                        StatusType = 1
                                                    };

                                                    Response response = IUserLogMainService.PostData(ulm);
                                                    ulm = (UserLogMain)response.Data;


                                                    foreach (Difference diff in diffs)
                                                    {
                                                        string columnName = Converter.GetColumnNameByPropertyName<MaHolderReg>(diff.PropertyName);

                                                        UserLogSub x = new UserLogSub()
                                                        {
                                                            UserLogMainId = ulm.Id,
                                                            ColumnName = columnName,
                                                            ValueOld = diff.Object1Value,
                                                            ValueNew = diff.Object2Value,
                                                        };
                                                        Response response1 = IUserLogSubService.PostData(x);
                                                        x = (UserLogSub)response1.Data;

                                                    }


                                                    divalert.Visible = true;
                                                    divalert.InnerText = "Success! Profile successfully updated.";
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Occupation change request sent successfully.', '');", true);

                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('occupation');ShowCustomMessage('Alert', 'No changes!', '');", true);
                                                }
                                                btnOccupationEdit.Disabled = true;
                                                spanR.Visible = false;
                                                spanP.Visible = true;
                                                spanA.Visible = false;
                                            }
                                            else
                                            {
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('occupation');ShowCustomMessage('Alert', 'Please enter all the required fields.', '');", true);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('occupation');ShowCustomMessage('Alert', 'Wrong password.', '');", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('occupation');ShowCustomMessage('Alert', 'Please enter password.', '');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('occupation');ShowCustomMessage('Alert', 'Please fill-in correct information!', '');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('occupation');ShowCustomMessage('Alert', 'Please fill-in correct information!', '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR1.Message + "\", '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR.Message + "\", '');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('occupation');ShowCustomMessage('Alert', \"" + responseUAList.Message + "!\", '');", true);
            }
        }

        protected void btnBankDetailsSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                User user = (User)Session["user"];
                Response responseBD = IBanksDefService.GetSingle(Convert.ToInt32(ddlBank.SelectedValue));
                if (responseBD.IsSuccess)
                {
                    UserAccount primaryAccount = new UserAccount();
                    Response responsePAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                    if (responsePAList.IsSuccess)
                    {
                        List<UserAccount> userAccounts = (List<UserAccount>)responsePAList.Data;
                        primaryAccount = userAccounts.Where(x => x.IsPrimary == 1).FirstOrDefault();
                        if (ddlUserAccountId.SelectedValue != null)
                        {
                            primaryAccount = userAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();
                        }

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responsePAList.Message + "\", '');", true);
                    }
                    BanksDef bd = (BanksDef)responseBD.Data;
                    Boolean isValidAccountNumber = false;
                    if (bd.NoFormat != null)
                    {
                        int result;
                        if (txtAccountNumber.Text.Trim().Length == Convert.ToInt32(bd.NoFormat) && Regex.IsMatch(txtAccountNumber.Text.Trim(), @"^\d+$"))
                        {
                            isValidAccountNumber = true;
                        }
                    }
                    if (isValidAccountNumber)
                    {
                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;

                            if (ddlUserAccountId.SelectedValue != null)
                            {
                                UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();

                                Response responseMHBList = IMaHolderBankService.GetDataByPropertyName(nameof(MaHolderBank.UserId), primaryAcc.UserId.ToString(), true, 0, 0, true);
                                if (responseMHBList.IsSuccess)
                                {
                                    MaHolderBank hboriginal = ((List<MaHolderBank>)responseMHBList.Data).FirstOrDefault();

                                    Response responseMHBList2 = IMaHolderBankService.GetDataByPropertyName(nameof(MaHolderBank.UserId), primaryAcc.UserId.ToString(), true, 0, 0, true);
                                    if (responseMHBList2.IsSuccess)
                                    {
                                        MaHolderBank hbnew = ((List<MaHolderBank>)responseMHBList2.Data).FirstOrDefault();
                                        int checkAuthenticationNumber = 0;
                                        //google authenticator check
                                        if (rdnGoogleBank.Checked == true)
                                        {
                                            Response responseUSList = IUserSecurityService.GetDataByPropertyName(nameof(UserSecurity.UserId), user.Id.ToString(), true, 0, 0, false);
                                            if (responseUSList.IsSuccess)
                                            {
                                                UserSecurity googleAuthUserSecurity = ((List<UserSecurity>)responseUSList.Data).Where(x => x.UserSecurityTypeId == 3).FirstOrDefault();
                                                if (googleAuthUserSecurity.IsVerified != 0)
                                                {
                                                    if (Int32.TryParse(txtPacNoBank.Text.Trim(), out int result))
                                                    {
                                                        TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                                                        bool isCorrectPIN = tfa.ValidateTwoFactorPIN(user.UniqueKey, txtPacNoBank.Text);
                                                        if (isCorrectPIN)
                                                        {
                                                            checkAuthenticationNumber = 1;
                                                        }
                                                        else
                                                        {
                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Please enter correct authentication code.', '');", true);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Authentication code must be Numeric.', '');", true);
                                                    }
                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Please bind Google Authenticator.', '');", true);
                                                }
                                            }
                                            else
                                            {
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseUSList.Message + "\", '');", true);
                                            }
                                        }
                                        else if (rdnMobileBank.Checked == true)
                                        {
                                            Response responseUser = IUserService.GetSingle(user.Id);
                                            if (responseUser.IsSuccess)
                                            {
                                                user = (User)responseUser.Data;
                                                if (!string.IsNullOrEmpty(txtPacNoBank.Text))
                                                {
                                                    if (user.VerificationCode != 0)
                                                    {
                                                        if (user.VerifyExpired == 0)
                                                        {
                                                            if (Int32.TryParse(txtPacNoBank.Text.Trim(), out int result))
                                                            {
                                                                if (user.VerificationCode == Convert.ToInt32(txtPacNoBank.Text.ToString()))
                                                                {
                                                                    checkAuthenticationNumber = 1;
                                                                    hdnMobilePinRequestedBank.Value = "0";
                                                                }
                                                                else
                                                                {
                                                                    checkAuthenticationNumber = 0;
                                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Wrong OTP. Please Enter Again.', '');", true);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'OTP must be Numeric.', '');", true);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'OTP Expired. Please Request Again.', '');", true);
                                                            hdnMobilePinRequestedBank.Value = "0";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Please Request OTP.', '');", true);
                                                        hdnMobilePinRequestedBank.Value = "0";
                                                    }
                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Please Enter OTP.', '');", true);
                                                }
                                            }
                                            else
                                            {
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUser.Message + "\", '');", true);
                                            }
                                        }
                                        string dbpath = string.Empty;
                                        string Date = DateTime.Now.ToString("ddMMyyyy-mmsshh");
                                        string G = Guid.NewGuid().ToString();

                                        if (FileUpload1.HasFile)
                                        {
                                            if (Path.GetExtension(FileUpload1.FileName).ToLower() == ".jpg" ||
                                                Path.GetExtension(FileUpload1.FileName).ToLower() == ".jpeg" ||
                                                Path.GetExtension(FileUpload1.FileName).ToLower() == ".png" ||
                                                Path.GetExtension(FileUpload1.FileName).ToLower() == ".pdf")
                                            {
                                                dbpath = ConfigurationManager.AppSettings["BankImages"].ToString();
                                                string subdir = Server.MapPath(dbpath);

                                                if (!Directory.Exists(subdir))
                                                    Directory.CreateDirectory(subdir);

                                                string filename = "ID" + user.Id + "_" + Date + (Path.GetFileName(FileUpload1.FileName));
                                                subdir = subdir + filename;
                                                dbpath = dbpath + filename;
                                                FileUpload1.SaveAs(subdir);
                                            }
                                        }
                                        if (checkAuthenticationNumber == 1)
                                        {
                                            Response responseNew = IUserService.GetSingle(user.Id);
                                            if (responseNew.IsSuccess)
                                            {
                                                User newUser = (User)responseNew.Data;
                                                newUser.SmsAttempts = 0;
                                                newUser.VerifyExpired = 1;
                                                newUser.VerificationCode = 0;
                                                Response responseNew2 = IUserService.UpdateData(newUser);
                                                newUser = (User)responseNew2.Data;
                                                Session["user"] = newUser;

                                                if (hboriginal == null)
                                                {
                                                    MaHolderBank x = new MaHolderBank()
                                                    {
                                                        // I have change the user column

                                                        UserId = primaryAcc.UserId,
                                                        IsPrimary = 1,
                                                        BankDefId = Convert.ToInt32(ddlBank.SelectedValue),
                                                        AccountName = ddlAccountname.SelectedValue,
                                                        BankAccountNo = txtAccountNumber.Text,
                                                        Remarks = "",
                                                        CreatedDate = DateTime.Now,
                                                        CreatedBy = primaryAcc.UserId,

                                                        UpdatedDate = DateTime.Now,
                                                        UpdatedBy = primaryAcc.UserId,
                                                        Version = 1,
                                                        Status = 0,
                                                        Image = dbpath,
                                                        UserAccountId = primaryAcc.Id
                                                    };

                                                    Response response = IMaHolderBankService.PostData(x);
                                                    x = (MaHolderBank)response.Data;

                                                    UserLogMain z = new UserLogMain()
                                                    {
                                                        TableName = "ma_holder_bank",
                                                        Description = "User added bank details",
                                                        UpdatedDate = DateTime.Now,
                                                        UserId = primaryAcc.UserId,
                                                        UserAccountId = primaryAcc.Id,
                                                        RefId = x.Id,
                                                        RefValue = x.BankAccountNo,
                                                        StatusType = 1
                                                    };

                                                    Response response1 = IUserLogMainService.PostData(z);
                                                    z = (UserLogMain)response1.Data;

                                                    // alert message
                                                    Session["UserAccountId"] = ddlUserAccountId.SelectedValue;
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Success! bank details successfully updated.', 'Settings.aspx');", true);
                                                }
                                                else if (hbnew.Status == 0)
                                                {
                                                    hbnew.UserId = primaryAcc.UserId;
                                                    hbnew.IsPrimary = 1;
                                                    hbnew.BankDefId = Convert.ToInt32(ddlBank.SelectedValue);
                                                    hbnew.AccountName = ddlAccountname.SelectedValue;
                                                    hbnew.BankAccountNo = txtAccountNumber.Text;
                                                    hbnew.UserAccountId = primaryAcc.Id;

                                                    if (FileUpload1.HasFile)
                                                    {
                                                        if (Path.GetExtension(FileUpload1.FileName).ToLower() == ".jpg" ||
                                                            Path.GetExtension(FileUpload1.FileName).ToLower() == ".jpeg" ||
                                                            Path.GetExtension(FileUpload1.FileName).ToLower() == ".png" ||
                                                            Path.GetExtension(FileUpload1.FileName).ToLower() == ".pdf")
                                                        {
                                                            dbpath = ConfigurationManager.AppSettings["BankImages"].ToString();
                                                            string subdir = Server.MapPath(dbpath);

                                                            if (!Directory.Exists(subdir))
                                                                Directory.CreateDirectory(subdir);

                                                            string filename = "ID" + user.Id + "_" + Date + (Path.GetFileName(FileUpload1.FileName));
                                                            subdir = subdir + filename;
                                                            dbpath = dbpath + filename;
                                                            FileUpload1.SaveAs(subdir);
                                                        }
                                                    }
                                                    int propertyCount = typeof(MaHolderBank).GetProperties().Length;

                                                    CompareLogic basicComparison = new CompareLogic()
                                                    { Config = new ComparisonConfig() { MaxDifferences = propertyCount } };
                                                    List<Difference> diffs = basicComparison.Compare(hboriginal, hbnew).Differences;
                                                    int noOfDifferent = diffs.Count();

                                                    if (noOfDifferent > 0)
                                                    {
                                                        hbnew.Remarks = "";
                                                        hbnew.UpdatedDate = DateTime.Now;
                                                        hbnew.Version = hboriginal.Version + 1;
                                                        hbnew.Status = 0;
                                                        hbnew.Image = dbpath;

                                                        Response response = IMaHolderBankService.UpdateData(hbnew);
                                                        if (response.IsSuccess)
                                                        {
                                                            hbnew = (MaHolderBank)response.Data;

                                                            UserLogMain z = new UserLogMain()
                                                            {
                                                                TableName = "ma_holder_bank",
                                                                Description = "User updated bank details",
                                                                UpdatedDate = DateTime.Now,
                                                                UserId = primaryAcc.UserId,
                                                                UserAccountId = primaryAcc.Id,
                                                                RefId = hbnew.Id,
                                                                RefValue = hbnew.BankAccountNo,
                                                                StatusType = 1
                                                            };

                                                            Response response1 = IUserLogMainService.PostData(z);
                                                            z = (UserLogMain)response1.Data;

                                                            foreach (Difference diff in diffs)
                                                            {
                                                                string columnName = Converter.GetColumnNameByPropertyName<MaHolderBank>(diff.PropertyName);

                                                                UserLogSub x = new UserLogSub()
                                                                {
                                                                    UserLogMainId = z.Id,
                                                                    ColumnName = columnName,
                                                                    ValueOld = diff.Object1Value,
                                                                    ValueNew = diff.Object2Value,
                                                                };
                                                                Response response2 = IUserLogSubService.PostData(x);
                                                                x = (UserLogSub)response2.Data;
                                                            }
                                                            Session["UserAccountId"] = ddlUserAccountId.SelectedValue;
                                                            // alert message
                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Success! bank details successfully updated.', 'Settings.aspx');", true);
                                                        }
                                                        else
                                                        {
                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    hbnew.UserId = primaryAcc.UserId;
                                                    hbnew.IsPrimary = 1;
                                                    hbnew.BankDefId = Convert.ToInt32(ddlBank.SelectedValue);
                                                    hbnew.AccountName = ddlAccountname.SelectedValue;
                                                    hbnew.BankAccountNo = txtAccountNumber.Text;
                                                    hbnew.UserAccountId = primaryAcc.Id;
                                                    hbnew.Image = dbpath;
                                                    int propertyCount = typeof(MaHolderBank).GetProperties().Length;

                                                    CompareLogic basicComparison = new CompareLogic()
                                                    { Config = new ComparisonConfig() { MaxDifferences = propertyCount } };
                                                    List<Difference> diffs = basicComparison.Compare(hboriginal, hbnew).Differences;
                                                    int noOfDifferent = diffs.Count();

                                                    if (noOfDifferent > 0)
                                                    {
                                                        hbnew.CreatedBy = primaryAcc.UserId;
                                                        hbnew.CreatedDate = DateTime.Now;
                                                        hbnew.Remarks = "";
                                                        hbnew.UpdatedDate = DateTime.Now;
                                                        hbnew.Version = hboriginal.Version + 1;
                                                        hbnew.Status = 0;

                                                        Response response = IMaHolderBankService.PostData(hbnew);
                                                        hbnew = (MaHolderBank)response.Data;

                                                        UserLogMain z = new UserLogMain()
                                                        {
                                                            TableName = "ma_holder_bank",
                                                            Description = "User updated bank details",
                                                            UpdatedDate = DateTime.Now,
                                                            UserId = primaryAcc.UserId,
                                                            UserAccountId = primaryAcc.Id,
                                                            RefId = hbnew.Id,
                                                            RefValue = hbnew.BankAccountNo,
                                                            StatusType = 1
                                                        };

                                                        Response response1 = IUserLogMainService.PostData(z);
                                                        z = (UserLogMain)response1.Data;



                                                        foreach (Difference diff in diffs)
                                                        {
                                                            string columnName = Converter.GetColumnNameByPropertyName<MaHolderBank>(diff.PropertyName);

                                                            UserLogSub x = new UserLogSub()
                                                            {
                                                                UserLogMainId = z.Id,
                                                                ColumnName = columnName,
                                                                ValueOld = diff.Object1Value,
                                                                ValueNew = diff.Object2Value,
                                                            };
                                                            Response response2 = IUserLogSubService.PostData(x);
                                                            x = (UserLogSub)response2.Data;
                                                        }
                                                        Session["UserAccountId"] = ddlUserAccountId.SelectedValue;
                                                        // alert message
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Success! bank details successfully updated.', 'Settings.aspx');", true);
                                                        // send email
                                                        Email email = new Email();
                                                        email.user = user;
                                                        EmailService.SendAlertUpdateMail(email, primaryAccount.AccountNo, "User Information Update Notification", "Bank Account has successfully binded", false, "");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseNew.Message + "\", '');", true);
                                            }
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Please enter correct authentication code.', '');", true);
                                        }

                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseMHBList2.Message + "\", '');", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseMHBList.Message + "\", '');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Holder number not selected!', '');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseUAList.Message + "!\", '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Account number Invalid!', '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseBD.Message + "\", '');", true);
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog("Setting Page btnBankDetailsSubmit_Click " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + ex.Message + "\", '');", true);
            }
        }

        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        //public static object GetOccupation(String OccCode, MaHolderReg maHolderReg)
        //{
        //    User user = (User)HttpContext.Current.Session["user"];

        //    string toMobileNumber = user.MobileNumber;
        //    return CheckAndRequestPin(toMobileNumber, title);

        //}

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object PhoneVerify(String title)
        {
            User user = (User)HttpContext.Current.Session["user"];

            string toMobileNumber = user.MobileNumber;
            return CheckAndRequestPin(toMobileNumber, title);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static bool PasswordVerify(string password)
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                User user = (User)HttpContext.Current.Session["user"];
                string encryptPassword = CustomEncryptorDecryptor.EncryptPassword(password);
                if (user.Password == encryptPassword)
                    return true;
            }
            return false;
        }

        public static Int32 currentSecLock = 0;
        public static void PinTimerForSMSLock()
        {
            try
            {
                User sessionUser = (User)HttpContext.Current.Session["user"];
                User user = sessionUser;

                while (currentSecLock <= SMSLockTimeInSeconds)
                {
                    if (currentSecLock == SMSLockTimeInSeconds)
                    {
                        Response responseUser = IUserService.GetSingle(user.Id);
                        user = (User)responseUser.Data;
                        user.SmsAttempts = 0;
                        user.IsSmsLocked = 0;
                        IUserService.UpdateData(user);
                        currentSecLock = 0;
                        break;
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        currentSecLock++;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("UserSettings Page PinTimerForSMSLock: " + ex.Message);
            }
        }
        public static Int32 currentSec = 0;
        public static void PinTimer()
        {
            try
            {
                User sessionUser = (User)HttpContext.Current.Session["user"];
                User user = sessionUser;

                while (currentSec <= SMSExpirationTimeInSeconds)
                {
                    if (currentSec == SMSExpirationTimeInSeconds)
                    {
                        Response responseUser = IUserService.GetSingle(user.Id);
                        user = (User)responseUser.Data;
                        user.VerifyExpired = 1;
                        IUserService.UpdateData(user);
                        currentSec = 0;
                        break;
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        currentSec++;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Settings Page PinTimer: " + ex.Message);
            }
        }

        public static string CheckAndRequestPin(String mobileNo, String title)
        {
            string returnString = "";
            try
            {
                User user = (User)HttpContext.Current.Session["user"];
                Response responseUList = IUserService.GetDataByPropertyName(nameof(Utility.User.MobileNumber), mobileNo, true, 0, 0, false);
                if (responseUList.IsSuccess)
                {
                    user = ((List<User>)responseUList.Data).FirstOrDefault();
                    if (user != null)
                    {
                        if (user.IsSmsLocked == 1)
                        {
                            Thread PinTimerThread = new Thread(PinTimerForSMSLock);
                            PinTimerThread.Start();
                            returnString = "sms locked";
                        }
                        else if (user.VerifyExpired == 1)
                        {
                            if (user.SmsAttempts >= 3)
                            {
                                user.IsSmsLocked = 1;
                                user.VerifyExpired = 1;
                                Thread PinTimerThread = new Thread(PinTimerForSMSLock);
                                PinTimerThread.Start();
                                returnString = "sms locked";
                            }
                            else
                            {
                                String toMobileNumber = user.MobileNumber;
                                String mobileVerificationPin = CustomGenerator.GenerateSixDigitPin();
                                String result = "";
                                result = SMSService.SendSMS(toMobileNumber, mobileVerificationPin, title);
                                Int32 length = toMobileNumber.Length;
                                String displayMoileNumber = new String('X', length - 4) + toMobileNumber.Substring(length - 4);
                                user.VerificationCode = Convert.ToInt32(mobileVerificationPin);
                                user.VerifyExpired = 0;
                                user.SmsAttempts = user.SmsAttempts + 1;

                                Thread PinTimerThread = new Thread(PinTimer);
                                PinTimerThread.Start();
                                returnString = "sent";
                            }
                            IUserService.UpdateData(user);
                            return returnString;
                        }
                        else
                        {
                            Thread PinTimerThread = new Thread(PinTimer);
                            PinTimerThread.Start();
                            returnString = "already sent";
                        }
                    }
                    else
                        returnString = "no account";
                }
                else
                {
                    Logger.WriteLog("Settings Page CheckAndRequestPin: " + responseUList.Message);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Settings Page CheckAndRequestPin: " + ex.Message);
            }
            return returnString;
        }

        protected void bindDistributionSetting()
        {
            StringBuilder sb = new StringBuilder();

            if (Session["user"] != null)
            {
                User sessionUser = (User)Session["user"];
                List<HolderInv> holderInvs = ServicesManager.GetHolderInvByHolderNo(ddlUserAccountId.SelectedValue, UTMCFundInformations);
                holderInvs.ForEach(x =>
                {

                    {
                        UtmcFundInformation I = UTMCFundInformations.FirstOrDefault(y => y.IpdFundCode == x.FundId);
                        if (I != null)
                        {
                            sb.Append(@"<tr>
                                <td>" + x.HolderNo + @"</td>
                                <td>" + I.FundName.Capitalize() + @"</td>
                                <td>" + (x.DistributionInsString) + @"</td>
                            </tr>");
                        }
                        else
                        {
                            sb.Append(@"<tr>
                                <td colspan='3'>Fund not found</td>
                            </tr>");
                        }
                    }

                });
                TbodyDistributionInstruction.InnerHtml = sb.ToString();

            }
        }



        protected void btnDistributionInstruction_Click(object sender, EventArgs e)
        {

        }



        protected void ddlUserAccountId_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                User user = (User)Session["user"];
                if (user != null)
                {
                    User sessionUser = (User)Session["user"];
                    Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                        UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();


                        bindAddress(primaryAcc);
                        BindWatchList();
                        if (primaryAcc != null)
                        {
                            Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                            MaHolderReg maHolderReg = new MaHolderReg();
                            if (responseMHR.IsSuccess)
                            {
                                maHolderReg = (MaHolderReg)responseMHR.Data;
                                ViewOccupation(maHolderReg.OccCode, maHolderReg);
                                //txtIncome.Text = ((maHolderReg.AccountType) == null ? "-" : CustomValues.GetIncomeByCode(maHolderReg.AccountType).ToLower());
                                Response responseUAList2 = IUserAccountService.GetDataByFilter(" account_no = '" + primaryAcc.AccountNo + "' and status='1' ", 0, 0, false);
                                if (responseUAList2.IsSuccess)
                                {
                                    UserAccount ua = ((List<UserAccount>)responseUAList2.Data).FirstOrDefault();
                                    if (ua.SatScore != 0)
                                        satScore.InnerHtml = CustomValues.GetGroupByScore(ua.SatScore) + " - " + ua.SatScore;
                                    else
                                    {
                                        satScore.InnerHtml = "-";
                                        satATag.InnerText = "Update";
                                    }

                                    satATag.HRef = "/SAT-Form.aspx?redirectUrl=Settings.aspx&AccountNo=" + primaryAcc.AccountNo;
                                    BindSettings();
                                    BindBankApproval(user.Id);
                                    string acPlan = CustomValues.GetAccounPlan(primaryAcc.HolderClass);
                                    if (acPlan == "EPF")
                                    {
                                        bankDiv.Visible = false;
                                        hdnAccountTypePlan.Value = "EPF";
                                        ClientScript.RegisterStartupScript(Page.GetType(), "OnLoad", "UpdateAccPlan()", true);
                                    }
                                    else if (acPlan == "CASH")
                                    {
                                        bankDiv.Visible = true;
                                        hdnAccountTypePlan.Value = "CASH";
                                        ClientScript.RegisterStartupScript(Page.GetType(), "OnLoad", "UpdateAccPlan()", true);
                                    }
                                    else
                                    {

                                        hdnAccountTypePlan.Value = "JOINT";
                                        ClientScript.RegisterStartupScript(Page.GetType(),"OnLoad","UpdateAccPlan()",true);
                                    }

                                    int ifChangeInstruction = 0;
                                    if (Request.QueryString["Instruction"] != null && Request.QueryString["Iid"] != null)
                                    {
                                        Response response = IDvDistributionIntructionService.GetSingle(Convert.ToInt32(Request.QueryString["Iid"].ToString()));
                                        if (response.IsSuccess)
                                        {
                                            DvDistributionIntruction x = (DvDistributionIntruction)response.Data;
                                            if (Request.QueryString["Instruction"] == "ctdc")
                                                x.DistributionIntruction = 2;
                                            else
                                                x.DistributionIntruction = 1;

                                            Response response2 = IDvDistributionIntructionService.UpdateData(x);
                                            x = (DvDistributionIntruction)response2.Data;
                                            ifChangeInstruction = 1;
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                                        }
                                    }
                                    bindDistributionSetting();
                                    if (ifChangeInstruction == 1)
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openDvModal();", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', \"" + responseUAList2.Message + "\", '');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', \"" + responseMHR.Message + "\", '');", true);
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                    }
                }
                else
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " ddlUserAccountId_SelectedIndexChanged: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        private static UserCenterStep responseUserStep1 { get; set; }

        protected void btnBindBank_Click(object sender, EventArgs e)
        {
            User sessionUser = (User)Session["user"];
            string idString = hdnBindBankId.Value;
            Int32 id = Convert.ToInt32(idString);

            Response responseBindBank = (Response)UserPolicy.AddBank(new UserPolicy.UserClass { UserAccount = userAccount, bankID = idString, User = sessionUser, Step = 2, Condition1 = true, httpContext = HttpContext.Current });
            try
            {
                if (responseBindBank.IsSuccess)
                {
                    responseUserStep1 = (UserCenterStep)responseBindBank.Data;
                    UserPolicyEnum3 userPolicyEnum3 = (UserPolicyEnum3)Enum.Parse(typeof(UserPolicyEnum3), responseUserStep1.Code);
                    string message = userPolicyEnum3.ToDescriptionString();
                    if (message == "SUCCESS")
                    {
                        Session["isVerified"] = 1;
                    }
                    else if (message == "EXCEPTION")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseBindBank.Message + "\", '');addBank();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + message + "\", '');addBank();", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseBindBank.Message + "\", '');", true);
                }
                hdnBindBankId.Value = "";
                Response responseBindBankApproval = (Response)UserPolicy.BindBankApproval(new UserPolicy.UserClass { UserAccount = userAccount, Step = 1, Condition1 = false, User = sessionUser, httpContext = HttpContext.Current });
                if (responseBindBankApproval.IsSuccess)
                {
                    responseUserStep1 = (UserCenterStep)responseBindBank.Data;
                    UserPolicyEnum3 userPolicyEnum3 = (UserPolicyEnum3)Enum.Parse(typeof(UserPolicyEnum3), responseUserStep1.Code);
                    if (responseUserStep1.maHolderBank == null)
                    {

                    }
                    string message = userPolicyEnum3.ToDescriptionString();
                    if (message == "SUCCESS")
                    {
                        Session["isVerified"] = 1;
                        if (responseUserStep1.maHolderBank == null)
                        {
                            bankDetailsBindStatus.InnerHtml = "Not Binded";
                            bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                        }
                        if (responseUserStep1.maHolderBank != null)
                        {
                            if (responseUserStep1.maHolderBank.Status == 0)
                            {
                                //pending
                                banknotverified.Visible = true;
                                bankDetailsBindStatus.InnerHtml = "Not Binded";
                                bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                            }
                            else if (responseUserStep1.maHolderBank.Status == 1)
                            {
                                //approved
                                bankverified.Visible = true;
                                bankDetailsBindStatus.InnerHtml = "Binded";
                                bankDetailsBindStatus.Attributes.Add("class", "text-success");
                            }
                            else
                            {
                                //denied
                                bankdenied.Visible = true;
                                bankDetailsBindStatus.InnerHtml = "Not Binded";
                                bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                            }
                        }
                        string redirectBack = "";
                        if (!string.IsNullOrEmpty(Request.QueryString["redirectBack"]))
                        {
                            redirectBack = Request.QueryString["redirectBack"].ToString();
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Bank details successfully binded', '" + redirectBack + "?AccountNo=" + userAccount.AccountNo + "&isPopup=1&Popup=openPopup(\\'bankDetails\\')');", true);
                    }
                    else if (message == "EXCEPTION")
                    {
                        responseBindBank.IsSuccess = false;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseBindBank.Message + "\", '');addBank();", true);


                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + message + "\", '');addBank();", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseBindBankApproval.Message + "\", '');", true);
                }

                //BindBankApproval(sessionUser.Id);


            }
            catch
            {

            }

            //string accountfilter = " user_account_id=" + userAccount.Id + " ";
            //var banks = GenericService.GetDataByFilter<UserAccountBanks>(accountfilter, 0, 0, false, null, true, null, false, false, null);
            //List<UserAccountBanks> listBanks = (List<UserAccountBanks>)banks.Data;
            ////bool isUpdated = false;
            //listBanks.ForEach(x =>
            //{
            //    if (x.MaHolderBankId == id) //bind bank 
            //    {
            //        //isUpdated = true;
            //        x.Status = 1;
            //        x.UpdatedDate = DateTime.Now;
            //        Response maHolderBankResponse = IMaHolderBankService.GetSingle(x.MaHolderBankId);
            //        MaHolderBank maHolderBank = (MaHolderBank)maHolderBankResponse.Data;
            //        Response banksDefResponse = IBanksDefService.GetSingle(maHolderBank.BankDefId);
            //        BanksDef banksDef = (BanksDef)banksDefResponse.Data;
            //        UserLogMain ulm = new UserLogMain()
            //        {
            //            Description = "Bind Bank (" + maHolderBank.BankAccountNo + " - " + banksDef.Name + ") to MA (" + userAccount.AccountNo + ") successful",
            //            TableName = "user_account_banks",
            //            UpdatedDate = DateTime.Now,
            //            UserId = sessionUser.Id,
            //            UserAccountId = userAccount.Id,
            //        };
            //        Response responseLog = IUserLogMainService.PostData(ulm);
            //        if (!responseLog.IsSuccess)
            //        {
            //            //Audit log failed
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
            //        }

            //        GenericService.UpdateData<UserAccountBanks>(x);
            //    }
            //    else if (x.Status != 0) //unbind bank
            //    {
            //        x.Status = 0;

            //        Response maHolderBankResponse = IMaHolderBankService.GetSingle(x.MaHolderBankId);
            //        MaHolderBank maHolderBank = (MaHolderBank)maHolderBankResponse.Data;
            //        Response banksDefResponse = IBanksDefService.GetSingle(maHolderBank.BankDefId);
            //        BanksDef banksDef = (BanksDef)banksDefResponse.Data;
            //        UserLogMain ulm = new UserLogMain()
            //        {
            //            Description = "Auto Unbind Bank (" + maHolderBank.BankAccountNo + " - " + banksDef.Name + ") from MA (" + userAccount.AccountNo + ") successful",
            //            TableName = "user_account_banks",
            //            UpdatedDate = DateTime.Now,
            //            UserId = sessionUser.Id,
            //            UserAccountId = userAccount.Id,
            //        };
            //        Response responseLog = IUserLogMainService.PostData(ulm);
            //        if (!responseLog.IsSuccess)
            //        {
            //            //Audit log failed
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
            //        }
            //        GenericService.UpdateData<UserAccountBanks>(x);
            //    }
            //});


            //if (!isUpdated)
            //{
            //    UserAccountBanks userAccountBanks = new UserAccountBanks
            //    {
            //        MaHolderBankId = id,
            //        UserAccountId = userAccount.Id,
            //        CreatedBy = sessionUser.Id,
            //        CreatedDate = DateTime.Now,
            //        UpdatedDate = DateTime.Now,
            //        Status = 1,
            //    };

            //    Response maHolderBankResponse = IMaHolderBankService.GetSingle(id);
            //    MaHolderBank maHolderBank = (MaHolderBank)maHolderBankResponse.Data;
            //    Response banksDefResponse = IBanksDefService.GetSingle(maHolderBank.BankDefId);
            //    BanksDef banksDef = (BanksDef)banksDefResponse.Data;
            //    UserLogMain ulm = new UserLogMain()
            //    {
            //        Description = "Bind Bank (" + maHolderBank.BankAccountNo + " - " + banksDef.Name + ") to MA (" + userAccount.AccountNo + ") successful",
            //        TableName = "user_account_banks",
            //        UpdatedDate = DateTime.Now,
            //        UserId = sessionUser.Id,
            //        UserAccountId = userAccount.Id,
            //    };
            //    Response responseLog = IUserLogMainService.PostData(ulm);
            //    if (!responseLog.IsSuccess)
            //    {
            //        //Audit log failed
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
            //    }

            //    Response response = GenericService.PostData<UserAccountBanks>(userAccountBanks);
            //}
            //hdnBindBankId.Value = "";
            //BindBankApproval(sessionUser.Id);

            //string redirectBack = "";
            //if (!string.IsNullOrEmpty(Request.QueryString["redirectBack"]))
            //{
            //    redirectBack = Request.QueryString["redirectBack"].ToString();
            //}
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Bank details successfully binded', '" + redirectBack + "?AccountNo=" + userAccount.AccountNo + "');", true);
        }

        protected void btnUnbindBank_Click(object sender, EventArgs e)
        {
            User sessionUser = (User)HttpContext.Current.Session["user"];
            string idString = hdnBindBankId.Value;
            Int32 id = Convert.ToInt32(idString);

            Response responseBindBank = (Response)UserPolicy.AddBank(new UserPolicy.UserClass { UserAccount = userAccount, bankID = idString, User = sessionUser, Step = 2, Condition1 = false, httpContext = HttpContext.Current });
            try
            {
                if (responseBindBank.IsSuccess)
                {
                    responseUserStep1 = (UserCenterStep)responseBindBank.Data;
                    UserPolicyEnum3 userPolicyEnum3 = (UserPolicyEnum3)Enum.Parse(typeof(UserPolicyEnum3), responseUserStep1.Code);
                    string message = userPolicyEnum3.ToDescriptionString();
                    if (message == "SUCCESS")
                    {
                        Session["isVerified"] = 1;


                        if (responseUserStep1.maHolderBank == null)
                        {
                            bankDetailsBindStatus.InnerHtml = "Not Binded";
                            bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                        }
                        if (responseUserStep1.maHolderBank != null)
                        {
                            if (responseUserStep1.maHolderBank.Status == 0)
                            {
                                //pending
                                banknotverified.Visible = true;
                                bankDetailsBindStatus.InnerHtml = "Not Binded";
                                bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                            }
                            else if (responseUserStep1.maHolderBank.Status == 1)
                            {
                                //approved
                                bankverified.Visible = true;
                                bankDetailsBindStatus.InnerHtml = "Binded";
                                bankDetailsBindStatus.Attributes.Add("class", "text-success");
                            }
                            else
                            {
                                //denied
                                bankdenied.Visible = true;
                                bankDetailsBindStatus.InnerHtml = "Not Binded";
                                bankDetailsBindStatus.Attributes.Add("class", "text-danger");
                            }
                        }
                    }
                    else if (message == "EXCEPTION")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseBindBank.Message + "\", '');addBank();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + message + "\", '');addBank();", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', \"" + responseBindBank.Message + "\", '');", true);
                }
                hdnBindBankId.Value = "";
                Response responseBindBankApproval = (Response)UserPolicy.BindBankApproval(new UserPolicy.UserClass { UserAccount = userAccount, Step = 1, Condition1 = false, User = sessionUser, httpContext = HttpContext.Current });
                if (responseBindBankApproval.IsSuccess)
                {
                    responseUserStep1 = (UserCenterStep)responseBindBank.Data;
                    UserPolicyEnum3 userPolicyEnum3 = (UserPolicyEnum3)Enum.Parse(typeof(UserPolicyEnum3), responseUserStep1.Code);
                    if (responseUserStep1.maHolderBank == null)
                    {

                    }
                    string message = userPolicyEnum3.ToDescriptionString();
                    if (message == "SUCCESS")
                    {
                        Session["isVerified"] = 1;
                        string redirectBack = "";
                        if (!string.IsNullOrEmpty(Request.QueryString["redirectBack"]))
                        {
                            redirectBack = Request.QueryString["redirectBack"].ToString();
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Bank details successfully unbinded', '" + redirectBack + "?AccountNo=" + userAccount.AccountNo + "&isPopup=1&Popup=openPopup(\\'bankDetails\\')');", true);
                    }
                    else if (message == "EXCEPTION")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + responseBindBank.Message + "\", '');addBank();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('bankDetails');ShowCustomMessage('Alert', \"" + message + "\", '');addBank();", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', \"" + responseBindBankApproval.Message + "\", '');", true);
                }

                //BindBankApproval(sessionUser.Id);


            }
            catch
            {

            }
            //Response responseBindBank = (Response)UserPolicy.AddBank(new UserPolicy.UserClass { UserAccount = userAccount, bankID = idString, User = sessionUser, Step = 2, Condition1 = false, httpContext = HttpContext.Current });
            //try
            //{
            //    string accountfilter = " user_account_id=" + userAccount.Id + " and ma_holder_bank_id='" + id + "' ";
            //    var banks = GenericService.GetDataByFilter<UserAccountBanks>(accountfilter, 0, 0, false, null, true, null, false, false, null);
            //    List<UserAccountBanks> listBanks = (List<UserAccountBanks>)banks.Data;
            //    UserAccountBanks userAccountBanks = listBanks.FirstOrDefault();
            //    userAccountBanks.Status = 0;
            //    userAccountBanks.UpdatedDate = DateTime.Now;
            //    GenericService.UpdateData<UserAccountBanks>(userAccountBanks);

            //    Response maHolderBankResponse = IMaHolderBankService.GetSingle(userAccountBanks.MaHolderBankId);
            //    MaHolderBank maHolderBank = (MaHolderBank)maHolderBankResponse.Data;
            //    Response banksDefResponse = IBanksDefService.GetSingle(maHolderBank.BankDefId);
            //    BanksDef banksDef = (BanksDef)banksDefResponse.Data;
            //    UserLogMain ulm = new UserLogMain()
            //    {
            //        Description = "Unbind Bank (" + maHolderBank.BankAccountNo + " - " + banksDef.Name + ") from MA (" + userAccount.AccountNo + ") successful",
            //        TableName = "user_account_banks",
            //        UpdatedDate = DateTime.Now,
            //        UserId = sessionUser.Id,
            //        UserAccountId = userAccount.Id,
            //    };
            //    Response responseLog = IUserLogMainService.PostData(ulm);
            //    if (!responseLog.IsSuccess)
            //    {
            //        //Audit log failed
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
            //    }


            //    hdnBindBankId.Value = "";
            //    BindBankApproval(sessionUser.Id);

            //    string redirectBack = "";
            //    if (!string.IsNullOrEmpty(Request.QueryString["redirectBack"]))
            //    {
            //        redirectBack = Request.QueryString["redirectBack"].ToString();
            //    }
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "openPopup('bankDetails');ShowCustomMessage('Alert', 'Bank details successfully unbinded', '" + redirectBack + "?AccountNo=" + userAccount.AccountNo + "&isPopup=1&Popup=openPopup(\\'bankDetails\\')');", true);
            //}
            //catch
            //{

            //}
        }


        private static readonly Lazy<IUtmcFundDetailService> lazyUtmcFundDetailServiceObj = new Lazy<IUtmcFundDetailService>(() => new UtmcFundDetailService());
        public static IUtmcFundDetailService IUtmcFundDetailService { get { return lazyUtmcFundDetailServiceObj.Value; } }

        public void BindWatchList()
        {
            Boolean IsImpersonate = ServicesManager.IsImpersonated(Context);
            if (Session["user"] != null)
            {
                User user = (User)Session["user"];
                Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                if (responseUAList.IsSuccess)
                {
                    List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                    UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();

                    ddlFundPriceTargetFund.Items.Clear();
                    ddlFundPerformanceTargetFund.Items.Clear();

                    ddlFundPriceTargetFund.Items.Add(new ListItem("Select Fund", ""));
                    ddlFundPerformanceTargetFund.Items.Add(new ListItem("Select Fund", ""));


                    {

                        foreach (UtmcFundInformation fund in UTMCFundInformations)
                        {
                            ddlFundPriceTargetFund.Items.Add(new ListItem(fund.FundName.Capitalize(), fund.Id.ToString()));
                        }
                    }
                    if (primaryAcc != null)
                    {
                        List<HolderInv> holderInvs = ServicesManager.GetHolderInvByHolderNo(primaryAcc.AccountNo, UTMCFundInformations);
                        List<UtmcFundInformation> fundInfo2 = UTMCFundInformations.Where(z => holderInvs.Where(zz => zz.CurrUnitHldg > 0).Select(zz => zz.FundId).Contains(z.IpdFundCode)).ToList();

                        foreach (UtmcFundInformation fund in fundInfo2)
                        {
                            ddlFundPerformanceTargetFund.Items.Add(new ListItem(fund.FundName.Capitalize(), fund.Id.ToString()));
                        }

                        if (ddlUserAccountId.SelectedValue != "")
                        {
                            Response responseUNSList = IUserNotificationSettingService.GetDataByFilter(" user_id='" + user.Id + "' and user_account_id='" + primaryAcc.Id + "' and notification_type_def_id in (2, 4) and status='1' ", 0, 0, true);
                            if (responseUNSList.IsSuccess)
                            {
                                List<UserNotificationSetting> UserNotifications = (List<UserNotificationSetting>)responseUNSList.Data;
                                StringBuilder asb = new StringBuilder();
                                UserNotifications = UserNotifications.OrderBy(x => x.FundId).ToList();
                                List<long> fundCodeList = new List<long>();
                                foreach (UserNotificationSetting u in UserNotifications)
                                {
                                    fundCodeList.Add(u.FundId);
                                }
                                fundCodeList = fundCodeList.Distinct().ToList();

                                List<string> fundNameL = new List<string>();
                                List<string> fundPriceTargetL = new List<string>();

                                List<string> fundPerformanceTargetL = new List<string>();

                                string fundname = "";
                                string fundpricetarget = "";

                                string fundperformancetarget = "";

                                foreach (long i in fundCodeList)
                                {


                                    {
                                        fundname = UTMCFundInformations.FirstOrDefault(x => x.Id == Convert.ToInt32(i)).FundName.Capitalize();

                                        if (UserNotifications.Where(x => x.FundId == Convert.ToInt32(i) && x.NotificationTypeDefId == 2).FirstOrDefault() != null)
                                            fundpricetarget = Convert.ToDecimal(UserNotifications.Where(x => x.FundId == Convert.ToInt32(i) && x.NotificationTypeDefId == 2).FirstOrDefault().Value).ToString("N4", new CultureInfo("en-US"));

                                        if (UserNotifications.Where(x => x.FundId == Convert.ToInt32(i) && x.NotificationTypeDefId == 4).FirstOrDefault() != null)
                                            fundperformancetarget = UserNotifications.Where(x => x.FundId == Convert.ToInt32(i) && x.NotificationTypeDefId == 4).FirstOrDefault().Value.ToString();

                                        fundNameL.Add(fundname);

                                        if (fundpricetarget != "")
                                            fundPriceTargetL.Add(fundpricetarget);
                                        else
                                            fundPriceTargetL.Add("-");

                                        if (fundperformancetarget != "")
                                            fundPerformanceTargetL.Add(fundperformancetarget);
                                        else
                                            fundPerformanceTargetL.Add("-");

                                        fundname = "";
                                        fundpricetarget = "";

                                        fundperformancetarget = "";
                                    }
                                }

                                int pricetarget = 0;

                                int performancetarget = 0;


                                for (int i = 0; i < fundCodeList.Count(); i++)
                                {
                                    if (UserNotifications.Where(x => Convert.ToDecimal(x.Value).ToString("N4", new CultureInfo("en-US")) == fundPriceTargetL[i] && x.FundId == fundCodeList[i]).FirstOrDefault() != null)
                                        pricetarget = UserNotifications.Where(x => Convert.ToDecimal(x.Value).ToString("N4", new CultureInfo("en-US")) == fundPriceTargetL[i] && x.FundId == fundCodeList[i]).FirstOrDefault().Id;



                                    if (UserNotifications.Where(x => x.Value == fundPerformanceTargetL[i] && x.FundId == fundCodeList[i]).FirstOrDefault() != null)
                                        performancetarget = UserNotifications.Where(x => x.Value == fundPerformanceTargetL[i] && x.FundId == fundCodeList[i]).FirstOrDefault().Id;


                                    asb.Append(@"  <tr>
                <td>" + fundNameL[i] + @"</td> ");
                                    if (fundPriceTargetL[i] == "-")
                                        asb.Append(@"<td><span style='float:left;'>" + fundPriceTargetL[i] + @"</span></td>");
                                    else
                                        asb.Append(@"<td><span class='mobile-alert1' style='float:left;'>" + fundPriceTargetL[i] + @"</span>" + (IsImpersonate ? "" : "<a class='text-danger alert-remove delete-notify' data-id='" + (pricetarget != 0 ? pricetarget.ToString() : "0") + "' href='javascript:;' style='position: relative; float: left; left: 10%; width:25%;'> <i class='fa fa-trash-o' aria-hidden='true'></i> </a>") + @"</td>");

                                    if (fundPerformanceTargetL[i] == "-")
                                        asb.Append(@"<td> " + fundPerformanceTargetL[i] + @"</td></tr>");
                                    else
                                        asb.Append(@"<td><span class='mobile-alert1' style='float:left;'>" + fundPerformanceTargetL[i] + @"</span>" + (IsImpersonate ? "" : "<a class='text-danger alert-remove delete-notify' data-id='" + (performancetarget != 0 ? performancetarget.ToString() : "0") + "' href='javascript:;' style='position: relative;float: left; left: 10%; width:25%;'>  <i class='fa fa-trash-o' aria-hidden='true'></i> </a>") + @"</td></tr>");

                                    pricetarget = 0;

                                    performancetarget = 0;

                                }
                                tbodyWatchList.InnerHtml = asb.ToString();

                                if (UserNotifications.Count == 0)
                                {
                                    watchListDiv.Visible = false;
                                }
                                else
                                {
                                    watchListDiv.Visible = true;
                                }



                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', \"" + responseUNSList.Message + "\", '');", true);
                            }
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                }
            }

        }
        protected void btnSaveAlert_Click(object sender, EventArgs e)
        {
            User user = (User)Session["user"];
            UserAccount primaryAcc = userAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();
            Response responseUNSList = IUserNotificationSettingService.GetDataByPropertyName(nameof(UserNotificationSetting.UserId), user.Id.ToString(), true, 0, 0, false);
            if (responseUNSList.IsSuccess)
            {
                List<UserNotificationSetting> ExistingUserNotifications = (List<UserNotificationSetting>)responseUNSList.Data;
                List<UserNotificationSetting> UserNotifications = new List<UserNotificationSetting>();

                string fundPriceTargetFund = ddlFundPriceTargetFund.SelectedValue;
                string fundPriceTarget = txtFundPriceTargetText.Text;

                string fundPerformanceTargetFund = ddlFundPerformanceTargetFund.SelectedValue;
                string fundPerformanceTarget = txtFundPerformanceTarget.Text;

                if (fundPriceTargetFund == "" && fundPerformanceTargetFund == "" && fundPriceTarget == "" && fundPerformanceTarget == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('alerts');ShowCustomMessage('Alert', 'Please select atleast one fund and enter the value to save!', '');", true);
                }
                else
                {

                    if (fundPriceTargetFund != "")
                    {
                        if (fundPriceTarget != "")
                        {
                            Response responseUFIList = IUtmcFundDetailService.GetDataByFilter("utmc_fund_information_id=" + fundPriceTargetFund, 0, 0, false);
                            if (responseUFIList.IsSuccess)
                            {
                                UtmcFundDetail fundDetail = ((List<UtmcFundDetail>)responseUFIList.Data).FirstOrDefault();
                                UtmcFundInformation uFI = UTMCFundInformations.FirstOrDefault(x => x.Id == Convert.ToInt32(fundPriceTargetFund));
                                UserNotifications.Add(new UserNotificationSetting
                                {
                                    UserId = user.Id,
                                    NotificationTypeDefId = 2,
                                    FundId = uFI.Id,
                                    FundCode = uFI.IpdFundCode,
                                    UserAccountId = primaryAcc.Id,
                                    Value = fundPriceTarget,
                                    CreatedBy = user.Id,
                                    CreatedDate = DateTime.Now,
                                    UpdatedBy = user.Id,
                                    Status = 1,
                                    ActualPrice = fundDetail.LatestNavPrice,
                                    ExpectedPrice = Convert.ToDecimal(fundPriceTarget)
                                });
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', \"" + responseUFIList.Message + "\", '');", true);
                            }
                        }
                        else
                        {
                            txtFundPriceTargetText.Style.Add("border-bottom", "1px solid red");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('alerts');ShowCustomMessage('Alert', 'Please enter required fields!', '');", true);
                        }
                    }
                    else if (fundPriceTarget != "")
                    {
                        if (fundPriceTargetFund == "")
                        {
                            ddlFundPriceTargetFund.Style.Add("border-bottom", "1px solid red");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('alerts');ShowCustomMessage('Alert', 'Please enter required fields!', '');", true);
                        }
                    }

                    if (fundPerformanceTargetFund != "")
                    {
                        if (fundPerformanceTarget != "")
                        {

                            UtmcFundInformation uFI = UTMCFundInformations.FirstOrDefault(x => x.Id == Convert.ToInt32(fundPerformanceTargetFund));
                            List<HolderInv> holderInvs = ServicesManager.GetHolderInvByHolderNo(primaryAcc.AccountNo, UTMCFundInformations);
                            HolderInv holderInv = holderInvs.FirstOrDefault(x => x.FundId == uFI.IpdFundCode);
                            UserNotifications.Add(new UserNotificationSetting
                            {
                                UserId = user.Id,
                                NotificationTypeDefId = 4,
                                FundId = uFI.Id,
                                FundCode = uFI.IpdFundCode,
                                UserAccountId = primaryAcc.Id,
                                Value = fundPerformanceTarget,
                                CreatedBy = user.Id,
                                CreatedDate = DateTime.Now,
                                UpdatedBy = user.Id,
                                Status = 1,
                                ActualPrice = holderInv.HldrAveCost,
                                ExpectedPrice = holderInv.HldrAveCost * (1 + (Convert.ToDecimal(fundPerformanceTarget) / 100))
                            });
                        }
                        else
                        {
                            txtFundPerformanceTarget.Style.Add("border-bottom", "1px solid red");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('alerts');ShowCustomMessage('Alert', 'Please enter required fields!', '');", true);
                        }
                    }
                    else if (fundPerformanceTarget != "")
                    {
                        if (fundPerformanceTargetFund == "")
                        {
                            ddlFundPerformanceTargetFund.Style.Add("border-bottom", "1px solid red");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('alerts');ShowCustomMessage('Alert', 'Please enter required fields!', '');", true);
                        }
                    }

                    if (UserNotifications.Count > 0)
                    {

                        List<UserNotificationSetting> newUns = new List<UserNotificationSetting>();
                        List<UserNotificationSetting> oldUns = new List<UserNotificationSetting>();
                        List<UserNotificationSetting> deletedUns = new List<UserNotificationSetting>();

                        foreach (UserNotificationSetting newuns in UserNotifications)
                        {
                            if (newuns.Value != "" && newuns.FundId != 0)
                            {
                                UserNotificationSetting checkexisted = ExistingUserNotifications.Where(x => x.NotificationTypeDefId == newuns.NotificationTypeDefId && x.FundId == newuns.FundId && x.Value == newuns.Value && x.Status == 1 && x.UserAccountId == Convert.ToInt32(ddlUserAccountId.SelectedValue)).FirstOrDefault();
                                if (checkexisted != null)
                                {
                                    //can be skip because exactly same record found.
                                    //continue;
                                }

                                UserNotificationSetting checkexistedUpdate = ExistingUserNotifications.Where(x => x.NotificationTypeDefId == newuns.NotificationTypeDefId && x.FundId == newuns.FundId && x.UserAccountId == primaryAcc.Id).FirstOrDefault();
                                if (checkexistedUpdate != null)
                                {
                                    checkexistedUpdate.Value = newuns.Value;
                                    checkexistedUpdate.FundId = newuns.FundId;
                                    checkexistedUpdate.Status = 1;
                                    checkexistedUpdate.UserAccountId = Convert.ToInt32(ddlUserAccountId.SelectedValue);
                                    checkexistedUpdate.UpdatedBy = user.Id;
                                    checkexistedUpdate.UpdatedDate = DateTime.Now;
                                    //Update record since value is different.
                                    oldUns.Add(checkexistedUpdate);
                                    //continue;
                                }

                                newUns.Add(newuns);
                            }
                        }

                        //UserLogMain ulm = new UserLogMain()
                        //{
                        //    Description = "Watchlist configuration update successful",
                        //    TableName = "user_notification_settings",
                        //    UserId = user.Id,
                        //    UserAccountId = primaryAcc.Id,
                        //    UpdatedDate = DateTime.Now,
                        //    RefId = primaryAcc.Id,
                        //    RefValue = primaryAcc.AccountNo,
                        //    StatusType = 1
                        //};

                        //Response response = IUserLogMainService.PostData(ulm);
                        //ulm = (UserLogMain)response.Data;

                        if (oldUns.Count > 0)
                            IUserNotificationSettingService.UpdateBulkData(oldUns);
                        if (newUns.Count > 0)
                            IUserNotificationSettingService.PostBulkData(newUns);
                        if (deletedUns.Count > 0)
                            IUserNotificationSettingService.UpdateBulkData(deletedUns);

                        string siteURL = ConfigurationManager.AppSettings["siteURL"];
                        try
                        {
                            BindWatchList();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('alerts');ShowCustomMessage('Alert', 'Added successfully', '');", true);
                            ddlFundPriceTargetFund.SelectedValue = "";
                            txtFundPriceTargetText.Text = "";
                            ddlFundPerformanceTargetFund.SelectedValue = "";
                            txtFundPerformanceTarget.Text = "";

                            Email email = new Email();
                            email.user = user;
                            EmailService.SendAlertUpdateMail(email, ddlUserAccountId.SelectedItem.Text, "Watch List configuration updated", "Watch List configuration has been updated", false, "");
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog("Alert Page SendEmail: " + ex.Message);
                        }

                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('alerts');ShowCustomMessage('Alert', \"" + responseUNSList.Message + "\", '');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static bool HideButton()
        {
            if (userAccount.IsPrinciple == 0)
            {
                return true;
            }
            else
                return false;
        }

        protected void Delete_Setting_Click(object sender, EventArgs e)
        {
            User user = (User)Session["user"];
            UserAccount primaryAcc = user.UserIdUserAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();
            if (SettingId.Value != "")
            {
                int id = Convert.ToInt32(SettingId.Value);

                Response response = IUserNotificationSettingService.GetSingle(id);
                if (response.IsSuccess)
                {
                    UserNotificationSetting usn = (UserNotificationSetting)response.Data;

                    if (usn != null)
                    {
                        usn.Status = 0;
                        Response resUpdate = IUserNotificationSettingService.UpdateData(usn);
                        if (resUpdate.IsSuccess)
                        {
                            SettingId.Value = "";
                            BindWatchList();
                            //UserLogMain ulm = new UserLogMain()
                            //{
                            //    Description = "Watchlist delete configuration successful",
                            //    TableName = "user_notification_settings",
                            //    UserId = user.Id,
                            //    UserAccountId = primaryAcc.Id,
                            //    UpdatedDate = DateTime.Now,
                            //    RefId = primaryAcc.Id,
                            //    RefValue = primaryAcc.AccountNo,
                            //    StatusType = 1
                            //};

                            //Response responseDeleteWL = IUserLogMainService.PostData(ulm);
                            //ulm = (UserLogMain)responseDeleteWL.Data;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('alerts');ShowCustomMessage('Alert', 'Deleted successfully', '');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('alerts');ShowCustomMessage('Alert', \"" + resUpdate.Message + "\", '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('alerts');ShowCustomMessage('Alert', \"no record with ID\", '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('alerts');ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "openPopup('alerts');ShowCustomMessage('Alert', \"no ID\", '');", true);
            }
        }

        private static readonly Lazy<IFundInfoService> lazyFundInfoObj = new Lazy<IFundInfoService>(() => new FundInfoService());

        public static IFundInfoService IFundInfoService { get { return lazyFundInfoObj.Value; } }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetNAVPrice(Int32 fundId)
        {
            Response response = new Response();
            try
            {

                {
                    UtmcFundInformation utmcFundInformation = UTMCFundInformations.FirstOrDefault(x => x.Id == fundId);
                    Response responseFIList = IFundInfoService.GetDataByFilter(" FUND_CODE = '" + utmcFundInformation.IpdFundCode + "'", 0, 0, false);
                    if (responseFIList.IsSuccess)
                    {
                        List<FundInfo> FundInfos = (List<FundInfo>)responseFIList.Data;
                        if (FundInfos.Count > 0)
                        {
                            FundInfo fundInfo = FundInfos.FirstOrDefault();
                            response.IsSuccess = true;
                            response.Data = fundInfo.CurrentUnitPrice;
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = " Fund info is empty";
                        }
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = responseFIList.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

    }

}
