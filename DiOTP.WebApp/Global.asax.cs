﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.Caching;

namespace DiOTP.WebApp
{
    public class Global : System.Web.HttpApplication
    {
        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        private static readonly Lazy<IFundInfoService> lazyIFundInfoServiceObj = new Lazy<IFundInfoService>(() => new FundInfoService());

        public static IFundInfoService IFundInfoService { get { return lazyIFundInfoServiceObj.Value; } }

        private static readonly Lazy<IFundReturnService> lazyIFundReturnServiceObj = new Lazy<IFundReturnService>(() => new FundReturnService());

        public static IFundReturnService IFundReturnService { get { return lazyIFundReturnServiceObj.Value; } }

        public static List<FundDetailedInformation> FundDetailedInformations = new List<FundDetailedInformation>();

        private static readonly Lazy<IFundChartInfoService> lazyIFundChartInfoServiceObj = new Lazy<IFundChartInfoService>(() => new FundChartInfoService());

        public static IFundChartInfoService IFundChartInfoService { get { return lazyIFundChartInfoServiceObj.Value; } }

        private static CacheItemRemovedCallback OnCacheRemove = null;

        protected void Application_Start(object sender, EventArgs e)
        {
            
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(DoWork);
            worker.WorkerReportsProgress = false;
            worker.WorkerSupportsCancellation = true;
            worker.RunWorkerCompleted +=
                   new RunWorkerCompletedEventHandler(WorkerCompleted);
            worker.RunWorkerAsync();
        }

        private void AddTask(string name, int seconds)
        {
            OnCacheRemove = new CacheItemRemovedCallback(CacheItemRemoved);
            HttpRuntime.Cache.Insert(name, seconds, null,
                DateTime.Now.AddSeconds(seconds), Cache.NoSlidingExpiration,
                CacheItemPriority.NotRemovable, OnCacheRemove);
        }

        public void CacheItemRemoved(string k, object v, CacheItemRemovedReason r)
        {
            DateTime date = DateTime.Now;
            string strDate = date.ToString("yyyy-MM-dd");
            if ((strDate.Substring(strDate.Length - 2)) == "01")
            {
                
            }
            AddTask(k, Convert.ToInt32(v));
        }

        private static void DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
            }
            catch(Exception ex)
            {
                Logger.WriteLog(" Global.asax DoWork: " + ex.Message + " - Exception");
            }
        }

        private static void WorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            if (worker != null)
            {
                System.Threading.Thread.Sleep(10000);
                worker.RunWorkerAsync();
                GlobalProperties.isExecutedToday = true;
            }
        }
        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //Exception ex = Server.GetLastError();

            //if (ex is HttpUnhandledException)
            //{
            //    // Pass the error on to the error page.
            //    Logger.WriteLog("Application_Error: " + ex.Message);
            //    Server.Transfer("Index.aspx?handler=Application_Error-Global.asax", true);
            //}

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        protected void Application_PreSendRequestHeaders()
        {
            //if (HttpContext.Current != null)
            //{
            //    HttpContext.Current.Response.Headers.Remove("Server");
            //}
        }
    }
}