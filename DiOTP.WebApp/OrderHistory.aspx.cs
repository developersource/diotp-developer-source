﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.FPXLibary;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static DiOTP.Utility.CustomClasses.CustomStatus;

namespace DiOTP.WebApp
{
    public partial class OrderHistory : System.Web.UI.Page
    {
        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUserAccountObj2 = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUserAccountObj2.Value; } }

        private static readonly Lazy<IUserOrderFileService> lazyUserOrderFileObj = new Lazy<IUserOrderFileService>(() => new UserOrderFileService());

        public static IUserOrderFileService IUserOrderFileService { get { return lazyUserOrderFileObj.Value; } }

        public int UserChoosetype;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                if (Session["isVerified"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                }
                if (Session["user"] != null && Session["isVerified"] != null)
                {
                    User user = (User)Session["user"];

                    string accNo = Request.QueryString["accNo"];
                    string typeString = Request.QueryString["type"];
                    int type = 0;
                    if (string.IsNullOrEmpty(typeString) || typeString == "0")
                    {
                        UserChoosetype = 0;
                        type = 0;
                        typeString = "1', '2', '3', '6";
                    }
                    else
                    {
                        UserChoosetype = Convert.ToInt32(typeString);
                        type = Convert.ToInt32(typeString);
                    }
                    HtmlControl li = (HtmlControl)tabsDiv.FindControl("type" + type);
                    li.Attributes.Add("class", "active");
                    Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                        if (ddlUserAccountId.Items.Count == 0)
                            foreach (UserAccount x in userAccounts)
                                if (x.IsPrinciple == 1)
                                    ddlUserAccountId.Items.Add(new ListItem(CustomValues.GetAccounPlan(x.HolderClass) + " - " + x.AccountNo.ToString(), x.AccountNo.ToString()));
                        
                        if (!string.IsNullOrEmpty(accNo))
                        {
                            
                        }
                        else
                        {
                            accNo = ddlUserAccountId.SelectedValue;
                        }

                        UserAccount primaryAcc = new UserAccount();
                        MaHolderReg maHolderReg = new MaHolderReg();
                        
                        {
                            Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                            if (responseMHR.IsSuccess)
                            {
                                maHolderReg = (MaHolderReg)responseMHR.Data;
                            }
                        }
                        spanUsername.InnerHtml = primaryAcc.AccountNo + " - " + maHolderReg.Name1;

                        string columnNameUserAccountId = Converter.GetColumnNameByPropertyName<UserOrder>(nameof(UserOrder.UserAccountId));
                        string columnNameOrderType = Converter.GetColumnNameByPropertyName<UserOrder>(nameof(UserOrder.OrderType));
                        string columnNameCreatedDate = Converter.GetColumnNameByPropertyName<UserOrder>(nameof(UserOrder.CreatedDate));

                        string Approvetype = "";
                        string period = "";


                        StringBuilder filter = new StringBuilder();
                        tbodyUserOrders.InnerHtml = "";

                        if (Request.QueryString["Approvetype"] != null && Request.QueryString["period"] != null)
                        {
                            Approvetype = Request.QueryString["Approvetype"].ToString();
                            period = Request.QueryString["period"].ToString();

                            if (Approvetype != "10" && period != "0")
                                filter.Append(columnNameUserAccountId + " = '" + primaryAcc.Id + "' and " + columnNameOrderType + " in ('" + typeString + "') and " + columnNameCreatedDate + " >= '" + DateTime.Now.AddMonths(Convert.ToInt32(period)).ToString("yyyy-MM-dd hh:mm:ss") + "' and " + columnNameCreatedDate + " <= '" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "' and status = '" + Approvetype + "'");
                            else if (Approvetype != "10" && period == "0")
                                filter.Append(columnNameUserAccountId + " = '" + primaryAcc.Id + "' and " + columnNameOrderType + " in ('" + typeString + "') and status = '" + Approvetype + "'");
                            else if (Approvetype == "10" && period != "0")
                                filter.Append(columnNameUserAccountId + " = '" + primaryAcc.Id + "' and " + columnNameOrderType + " in ('" + typeString + "') and " + columnNameCreatedDate + " >= '" + DateTime.Now.AddMonths(Convert.ToInt32(period)).ToString("yyyy-MM-dd hh:mm:ss") + "' and " + columnNameCreatedDate + " <= '" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "'");
                            else if (Approvetype == "10" && period == "0")
                                filter.Append(columnNameUserAccountId + " = '" + primaryAcc.Id + "' and " + columnNameOrderType + " in ('" + typeString + "')");
                        }
                        else
                            filter.Append(columnNameUserAccountId + " = '" + primaryAcc.Id + "' and " + columnNameOrderType + " in ('" + typeString + "')");

                        if (!IsPostBack)
                        {
                            ddlTransactionPeriod.SelectedIndex = ddlTransactionPeriod.Items.IndexOf(ddlTransactionPeriod.Items.FindByValue(period));
                            ddlTransactionType.SelectedIndex = ddlTransactionType.Items.IndexOf(ddlTransactionType.Items.FindByValue(Approvetype));
                            ddlUserAccountId.SelectedIndex = ddlUserAccountId.Items.IndexOf(ddlUserAccountId.Items.FindByValue(accNo));
                        }
                        
                        int skip = 0, take = 10;
                        if (hdnCurrentPageNo.Value == "")
                        {
                            skip = 0;
                            take = 10;
                            hdnNumberPerPage.Value = "10";
                            hdnCurrentPageNo.Value = "1";
                            hdnTotalRecordsCount.Value = IUserOrderService.GetCountByFilter(filter.ToString()).ToString();
                        }
                        else
                        {
                            skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * 10;
                            take = 10;
                        }

                        filter.Append(" group by order_no");
                        Response responseUOList = IUserOrderService.GetDataByFilter(filter.ToString(), skip, take, true);
                        if (responseUOList.IsSuccess)
                        {
                            List<UserOrder> userOrders = (List<UserOrder>)responseUOList.Data;

                            if (userOrders.Count > 0)
                            {
                                string ordertype = "";
                                int i = 1;
                                StringBuilder sb = new StringBuilder();
                                foreach (UserOrder uo in userOrders.OrderByDescending(a => a.CreatedDate).ToList())
                                {
                                    Response responseSubList = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), uo.OrderNo, true, 0, 0, true);
                                    List<UserOrder> uosubList = (List<UserOrder>)responseSubList.Data;
                                    string actionsHTML = "-";
                                    string status = "";
                                    if (uo.OrderStatus == 1)
                                    {
                                        status = "Order placed";
                                        actionsHTML += "<a href='Show-Cart.aspx?UON=" + uo.OrderNo + "' class='btn btn-infos1 btn-sm mb-5'>" + (uo.OrderType == 1 ? "Pay Now" : (uo.OrderType == 2 || uo.OrderType == 3 ? "Request Now" : (uo.OrderType == 6 ? "Verify Now" : "-"))) + "</a><br />";
                                        actionsHTML += "<a data-orderno='" + uo.OrderNo + "' href='javascript:;' class='btn btn-infos1 btn-sm cancel-order mb-5'>Cancel</a>";
                                    }
                                    else if (uo.OrderStatus == 2 && (uo.OrderType == (int)OrderType.RSP))
                                    {
                                        status = "FPX verified";
                                    }
                                    else if (uo.OrderStatus == 2 && (uo.OrderType == (int)OrderType.Sell || uo.OrderType == (int)OrderType.SwitchIn || uo.OrderType == (int)OrderType.SwitchOut))
                                        status = "Pending for order approval";
                                    else if (uo.OrderStatus == 18)
                                        status = "Order cancelled";
                                    else if (uo.OrderStatus == 22 && uo.PaymentMethod == "1")
                                        status = "Pending for payment";
                                    else if (uo.OrderStatus == 22 && uo.PaymentMethod == "3")
                                        status = "Pending for payment approval";
                                    else if (uo.OrderStatus == 29 && uo.PaymentMethod == "1")
                                        status = "Payment failed";
                                    else if (uo.OrderStatus == 29 && uo.PaymentMethod == "3")
                                        status = "Payment rejected";
                                    else if (uo.OrderStatus == 3)
                                        status = "Order approved";
                                    else if (uo.OrderStatus == 39)
                                        status = "Order rejected";
                                    else if (uo.OrderStatus == 2)
                                        status = "Order success";

                                    if (uo.OrderType == (int)OrderType.Buy)
                                        ordertype = "BUY";
                                    else if (uo.OrderType == (int)OrderType.Sell)
                                        ordertype = "SELL";
                                    else if (uo.OrderType == (int)OrderType.SwitchIn || uo.OrderType == (int)OrderType.SwitchOut)
                                        ordertype = "SWITCH";
                                    else if (uo.OrderType == (int)OrderType.RSP)
                                        ordertype = "RSP";

                                    string url = "";

                                    Response responseUOFList = IUserOrderFileService.GetDataByPropertyName(nameof(UserOrderFile.OrderNo), uo.OrderNo, true, 0, 0, true);
                                    if (responseUOFList.IsSuccess)
                                    {
                                        UserOrderFile uof = ((List<UserOrderFile>)responseUOFList.Data).FirstOrDefault();
                                        if (uof != null)
                                            url = uof.Url;
                                        else
                                            url = "FPX PAYMENT";
                                        StringBuilder sub1 = new StringBuilder();
                                        if (uo.OrderType == (int)CustomStatus.OrderType.Buy || uo.OrderType == (int)CustomStatus.OrderType.RSP)
                                        {
                                            uosubList.ForEach(x =>
                                            {
                                                
                                                Response response = IUtmcFundInformationService.GetSingle(x.FundId);
                                                if (response.IsSuccess)
                                                {
                                                    UtmcFundInformation fund = (UtmcFundInformation)response.Data;
                                                    sub1.Append(@"<tr>
                                                                    <td style='width: 250px;'>" + fund.FundName.Capitalize() + @"</td>
                                                                    <td><span class='currencyFormatNoSymbol text-left'>" + x.Amount + @"</span></td>
                                                                </tr>");
                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', '" + response.Message + "', '');", true);
                                                }
                                            });
                                        }
                                        if (uo.OrderType == (int)CustomStatus.OrderType.Sell)
                                        {
                                            uosubList.ForEach(x =>
                                            {
                                                Response response = IUtmcFundInformationService.GetSingle(uo.FundId);
                                                if (response.IsSuccess)
                                                {
                                                    UtmcFundInformation fund = (UtmcFundInformation)response.Data;
                                                    sub1.Append(@"<tr>
                                                                    <td style='width: 250px;'>" + fund.FundName.Capitalize() + @"</td>
                                                                    <td><span class='unitFormat text-left'>" + x.Units + @"</span></td>
                                                                </tr>");
                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', '" + response.Message + "', '');", true);
                                                }
                                            });
                                        }

                                        if (uo.OrderType == (int)OrderType.SwitchIn || uo.OrderType == (int)OrderType.SwitchOut)
                                        {
                                            uosubList.Where(a=>a.OrderType == (int)OrderType.SwitchIn).ToList().ForEach(x =>
                                            {
                                                    Response response = IUtmcFundInformationService.GetSingle(x.FundId);
                                                    if (response.IsSuccess)
                                                    {
                                                        UtmcFundInformation fund = (UtmcFundInformation)response.Data;
                                                        Response response2 = IUtmcFundInformationService.GetSingle(x.ToFundId);
                                                        if (response2.IsSuccess)
                                                        {
                                                            UtmcFundInformation toFund = (UtmcFundInformation)response2.Data;
                                                            sub1.Append(@"<tr>
                                                                    <td style='width: 250px;'>From - " + fund.FundCode + @"<br />To - " + toFund.FundCode + @"</td>
                                                                    <td><span class='unitFormat text-left'>" + x.Units + @"</span></td>
                                                                </tr>");
                                                        }
                                                        else
                                                        {
                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', '" + response2.Message + "', '');", true);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', '" + response.Message + "', '');", true);
                                                    }
                                            });
                                        }
                                        string amountorunit = string.Empty;
                                        if (uo.Amount == 0)
                                            amountorunit = "<span class='unitFormat text-left' style='padding-left:50px;'>" + uo.Units + @"</span>";
                                        else
                                            amountorunit = "<span class='currencyFormatNoSymbol text-left' style='padding-left:50px;'>" + uo.Amount + @"</span>";
                                        sb.Append(@"<tr>
                                                    <td>" + i + @"</td>
                                                    <td>" + uo.CreatedDate.ToString("dd/MM/yyyy") + @"</td>
                                                    <td>
                                                        <!-- OrderConfirmation.aspx?OrderNo=" + uo.OrderNo + @" -->
                                                        <a href='javascript:;' class='order-detail-link'>" + uo.OrderNo + @"</a>
                                                        <div class='order-detail-div hide'>
                                                            <div class='mb-10'><strong>" + uo.OrderNo + @"</strong></div> <span class='order-detail-div-close'>X</span>
                                                            <table class='table table-font-size-13 table-cell-pad-5 table-bordered'>
                                                                <thead>
                                                                    <tr>
                                                                        <th>Fund Name</th>
                                                                        <th>Amount/Units</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    " + sub1.ToString() + @"
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                    <td>" + ordertype + @"</td>
                                                    <td>"+ amountorunit +@"</td>
                                                    <td>" + (uo.PaymentMethod == "1" ? "FPX PAYMENT" : (uo.PaymentMethod == "3" ? "<a href='" + url + @"' target='_blank'>view</a>" : "-")) + @"</td>
                                                    <td>" + status + @"</td>
                                                    <td>" + actionsHTML + @"</td>
                                                </tr>");
                                        
                                        i++;

                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', '" + responseUOFList.Message + "', '');", true);
                                    }
                                }
                                tbodyUserOrders.InnerHtml = sb.ToString();

                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', '" + responseUOList.Message + "', '');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', '" + responseUAList.Message + "', '');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }


        protected void btnsearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("OrderList.aspx?Approvetype=" + ddlTransactionType.SelectedValue + "&period=" + ddlTransactionPeriod.SelectedValue + "&type=" + UserChoosetype + "&accNo=" + ddlUserAccountId.SelectedValue, false);
        }

        protected void btnreset_Click(object sender, EventArgs e)
        {
            Response.Redirect("OrderList.aspx", false);
        }

        protected void CancelOrder_Click(object sender, EventArgs e)
        {
            User user = (User)Session["user"];
            string orderNo = hdnCancelOrderNo.Value;
            Response responseUOList = IUserOrderService.GetDataByFilter(" order_no = '" + orderNo + "' and user_id=" + user.Id + " ", 0, 0, false);
            if (responseUOList.IsSuccess)
            {
                List<UserOrder> userOrders = (List<UserOrder>)responseUOList.Data;
                List<UserOrder> multipleOrders = new List<UserOrder>();

                userOrders.ForEach(x =>
                {
                    Response response = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), x.OrderNo, true, 0, 0, false);
                    if (response.IsSuccess)
                    {
                        List<UserOrder> ol = (List<UserOrder>)response.Data;
                        if (ol.Count > 0)
                        {
                            multipleOrders.AddRange(ol);
                        }
                    }
                });

                multipleOrders.ForEach(x =>
                {
                    x.OrderStatus = 18;
                });
                IUserOrderService.UpdateBulkData(multipleOrders);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Info', 'Order no: " + orderNo + " cancelled.', 'OrderList.aspx');", true);
            }
            // terminated RSP fund
            
        }

        public void FPXPayment(
          string orderNo, string transDate, string txn_amt,
          string buyer_mail_id, string buyer_id, string buyerName, string buyerBankBranch,
          string buyerAccNo, string makerName, string buyerIBAN,
          string buyerBankId, string msgType)
        {
            Controller c = new Controller();
            String checksum = "";
            Random RandomNumber = new Random();
            String fpx_msgType = msgType;
            String fpx_msgToken = "01";
            String fpx_sellerExId = "EX00011078";
            String fpx_sellerExOrderNo = orderNo;
            String fpx_sellerOrderNo = orderNo;
            String fpx_sellerTxnTime = transDate;
            String fpx_sellerId = "SE00043740";
            String fpx_sellerBankCode = "01";
            String fpx_txnCurrency = "MYR";
            String fpx_txnAmount = txn_amt;
            String fpx_buyerEmail = buyer_mail_id;
            String fpx_buyerId = buyer_id;
            String fpx_buyerName = buyerName;
            String fpx_buyerBankId = buyerBankId;
            String fpx_buyerBankBranch = buyerBankBranch;
            String fpx_buyerAccNo = buyerAccNo;
            String fpx_makerName = makerName;
            String fpx_buyerIban = buyerIBAN;
            String fpx_productDesc = "PD";
            String fpx_version = "6.0";
            String fpx_checkSum = "";
            fpx_checkSum = fpx_buyerAccNo + "|" + fpx_buyerBankBranch + "|" + fpx_buyerBankId + "|" + fpx_buyerEmail + "|" + fpx_buyerIban + "|" + fpx_buyerId + "|" + fpx_buyerName + "|";
            fpx_checkSum += fpx_makerName + "|" + fpx_msgToken + "|" + fpx_msgType + "|" + fpx_productDesc + "|" + fpx_sellerBankCode + "|" + fpx_sellerExId + "|";
            fpx_checkSum += fpx_sellerExOrderNo + "|" + fpx_sellerId + "|" + fpx_sellerOrderNo + "|" + fpx_sellerTxnTime + "|" + fpx_txnAmount + "|" + fpx_txnCurrency + "|" + fpx_version;
            fpx_checkSum = fpx_checkSum.Trim();

            checksum = c.RSASign(fpx_checkSum, Request.PhysicalApplicationPath + "EX00011078.key"); //Exchange Key name 
            String finalMsg = fpx_checkSum;
            string FPXPostPaymentUrl = ConfigurationManager.AppSettings["FPXPostPaymentUrl"].ToString();
            var formPostText = @"<html><body><div>
                    <form method='POST' action='" + FPXPostPaymentUrl + @"' name='FPXPaymentForm'>
                      <input type='hidden' name='txn_amt' value='" + fpx_txnAmount + @"' /> 
                      <input type='hidden' value='" + fpx_msgType + @"' name='fpx_msgType'>

                      <input type='hidden' value='" + fpx_msgToken + @"' name='fpx_msgToken'>
                      <input type='hidden' value='" + fpx_sellerExId + @"' name='fpx_sellerExId'>
                      <input type='hidden' value='" + fpx_sellerExOrderNo + @"' name='fpx_sellerExOrderNo'>
                      <input type='hidden' value='" + fpx_sellerTxnTime + @"' name='fpx_sellerTxnTime'>
                      <input type='hidden' value='" + fpx_sellerOrderNo + @"' name='fpx_sellerOrderNo'>
                      <input type='hidden' value='" + fpx_sellerBankCode + @"' name='fpx_sellerBankCode'>
                      <input type='hidden' value='" + fpx_txnCurrency + @"' name='fpx_txnCurrency'>
                      <input type='hidden' value='" + fpx_txnAmount + @"' name='fpx_txnAmount'>
                      <input type='hidden' value='" + fpx_buyerEmail + @"' name='fpx_buyerEmail'>
                      <input type='hidden' value='" + checksum + @"' name='fpx_checkSum'>
                      <input type='hidden' value='" + fpx_buyerName + @"' name='fpx_buyerName'>
                      <input type='hidden' value='" + fpx_buyerBankId + @"' name='fpx_buyerBankId'>
                      <input type='hidden' value='" + fpx_buyerBankBranch + @"' name='fpx_buyerBankBranch'>
                      <input type='hidden' value='" + fpx_buyerAccNo + @"' name='fpx_buyerAccNo'>
                      <input type='hidden' value='" + fpx_buyerId + @"' name='fpx_buyerId'>
                      <input type='hidden' value='" + fpx_makerName + @"' name='fpx_makerName'>
                      <input type='hidden' value='" + fpx_buyerIban + @"' name='fpx_buyerIban'>
                      <input type='hidden' value='" + fpx_productDesc + @"' name='fpx_productDesc'>
                      <input type='hidden' value='" + fpx_version + @"' name='fpx_version'>
                      <input type='hidden' value='" + fpx_sellerId + @"' name='fpx_sellerId'>
                      <input type='hidden' value='" + checksum + @"' name='checkSum_String'>
                      <input type='hidden' value='" + Request.PhysicalApplicationPath + @"'>
                    </form></div><script type='text/javascript'>document.FPXPaymentForm.submit();</script></body></html>
                    ";
            Response.Write(formPostText);
        }

    }
}