//Wizard Init

$("#wizard").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "none",
    titleTemplate: '#title#',
    enableKeyNavigation: false,
    //onFinished: function() {
    //    alert("Form successfully submitted!");
    //    location.reload();
    //}
    //titleTemplate: "<span class='number' data-toggle='tooltip' data-title='#title#' data-html='true'>#title#</span> ",
    onInit: function () {
        //$('.steps [data-toggle="tooltip"]').tooltip({
        //    trigger: 'hover focus'
        //});
        //alert('init');
    },
    //onContentLoaded: function () {
    //    $('.steps [data-toggle="tooltip"]').tooltip();
    //    alert('loaded');
    //}
});

var wizard = $("#wizard")

//Form control

$('[data-step="next"]').on('click', function() {
    wizard.steps('next');
});

$('[data-step="previous"]').on('click', function() {
    wizard.steps('previous');
});

//$('[data-step="finish"]').on('click', function() {
//    wizard.steps('finish');
//});

