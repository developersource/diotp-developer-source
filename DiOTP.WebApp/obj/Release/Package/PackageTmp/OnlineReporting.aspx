﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="OnlineReporting.aspx.cs" Inherits="DiOTP.WebApp.OnlineReporting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx">Home</a></li>
                            <li class="active">Online Reporting</li>
                        </ol>
                    </div>
                    <h3 class="hr-left mb-20 pb-12 d-ib mob-fund-head tab-fund-head">Online Reporting</h3>

                    <div class="tabs-horizontal my-tabs mob">
                        <ul class="nav nav-tabs  my-nav-tabs">
                            <li class="active">
                                <a href="#tab8" data-toggle="tab" class="mr-5">Transaction history</a>
                            </li>
                            <li>
                                <a href="#tab7" data-toggle="tab" class="mr-5 ml-5">Downloads</a>
                            </li>
                        </ul>

                        <div class="tab-content">

                            <div class="tab-pane fade active in" id="tab8">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-head">
                                            <h5 class="mt-0 ff-ls" style="margin-bottom: 2px !important;">Transaction history</h5>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                <asp:Label ID="lblFundAccount" runat="server" Text="Fund Account"></asp:Label><br /><br />
                                                <asp:Label ID="lblTransactionType" runat="server" Text="Transaction Type"></asp:Label><br /><br />
                                                <asp:Label ID="lblTransactionPeriod" runat="server" Text="Transaction Period"></asp:Label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <asp:DropDownList ID="ddlFundAccount" runat="server" ></asp:DropDownList><br /><br />
                                                <asp:DropDownList ID="ddlTransactionType" runat="server" ></asp:DropDownList><br /><br />
                                                <asp:TextBox ID="txtDateStart" runat="server" TextMode="Date" ></asp:TextBox>
                                                to
                                                <asp:TextBox ID="txtDateEnd" runat="server" TextMode="Date"></asp:TextBox>
                                                &nbsp&nbsp
                                                <asp:Button ID="btnSubmit" runat="server" Text="Generate" OnClick="btnSubmit_Click" />
                                            </div>
                        
                                        </div>

                                        <table class="table table-font-size-13 table-cell-pad-5">
                                            <tbody id="tbodyTransactionHistory" runat="server">
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="tab7">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-head">
                                            <h5 class="mt-0 ff-ls" style="margin-bottom: 2px !important;">Downloads</h5>
                                        </div>

                                        <table class="table">

                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
    <script>
        $('form').on('submit', function () {
            setTimeout(function () {
                $('.loader-bg').fadeIn();
            }, 500);
        });
    </script>
</asp:Content>
