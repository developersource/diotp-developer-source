﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="SwitchFunds.aspx.cs" Inherits="DiOTP.WebApp.SwitchFunds" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2">
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Portfolio.aspx">eApexIs</a></li>
                            <li class="active">Switch Funds</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Switch Fund</h3>
                    </div>
                </div>
            </div>
            <div style="margin-bottom: 5px; margin-left: 5px;">
                <img src="Content/MyImage/INFO.png" style="width: 15px; height: 15.25px; margin-right: 5px;" />
                <span style="font-weight: 600; color: #A0A0A0; font-size: 13px">You are currently viewing account</span>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4 mb-10" style="margin-left: 0px; width: 35.33%;">
                    <asp:DropDownList CssClass="form-control mobile-select selectpicker-acc" ID="ddlUserAccountId" runat="server" AutoPostBack="true" ClientIDMode="Static" OnSelectedIndexChanged="ddlUserAccountId_SelectedIndexChanged"></asp:DropDownList>
                    <asp:HiddenField ID="isUserAccountChange" Value="false" runat="server" ClientIDMode="Static" />
                </div>
            </div>
            <%--<div class="row">
                <div class="col-md-12">
                    <div class="selectedAccountDetails hide">
                        <div class="row">
                            <div class="col-md-5">
                                <strong>Account Name:</strong>
                                <span id="accName" runat="server"></span>
                            </div>
                            <div class="col-md-3">
                                <strong>Master Account Number:</strong>
                                <span id="maAccNumber" runat="server"></span>
                            </div>
                            <div class="col-md-3">
                                <strong>Account Plan:</strong>
                                <span id="accType" runat="server"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center form-box">
                    <div class="f1">
                        <div class="f1-steps">
                            <div class="f1-progress">
                                <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
                            </div>
                            <div class="f1-step active">
                                <div class="f1-step-icon"><i class="fa fa-universal-access"></i></div>
                                <p>Account Overview</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-money"></i></div>
                                <p>Fund Selection & Units</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-file-text-o"></i></div>
                                <p>Confirmation</p>
                            </div>
                        </div>
                        <small class="text-danger" id="errorMessage"></small>
                        <small class="text-success" id="successMessage"></small>
                        <fieldset class="fieldsetAccountFundsOverview">





                            <div class="row" id="accSummaryDiv">
                                <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                                    <table class="table table-cell-pad-5 table-font-size-13 table-condensed table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Fund</th>
                                                <th class="text-right">Total<br />
                                                    Units</th>
                                                <th class="text-right">NAV Price<br />
                                                    MYR</th>
                                                <th class="text-right">Market Value<br />
                                                    MYR</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbodyAccountFunds" runat="server">
                                            <tr>
                                                <td colspan="4" class="text-center">You have no unit trust holdings at this moment. Start investing with us today by clicking 'Transaction > Buy'.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="f1-buttons">
                                        <button type="button" class="btn btn-next">Next</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="fieldsetfundSelectionAndAmount">
                            <div class="row mt-10">
                                <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Switch Out Fund:</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:HiddenField ID="hdnFundId" runat="server" ClientIDMode="Static" />
                                                    <asp:DropDownList ID="ddlFundsList" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row mb-10">
                                        <div class="col-md-2" style="width: 40%;">
                                            <label>SAT Recommended Fund:</label>
                                        </div>
                                        <div class="col-md-3" style="width: 30%;">
                                            <asp:RadioButton ID="rdnRecommended" runat="server" GroupName="FundSelection" Text="Default" ClientIDMode="Static" />
                                        </div>
                                        <div class="col-md-4" style="width: 30%;">
                                            <asp:RadioButton ID="rdnNonRecommended" runat="server" GroupName="FundSelection" Text="Non-Default" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                    <p class="fs-12 text-justify mb-5" id="WarningLabel" style="color: red; line-height: 13px; font-size: 11px;">* Please be informed that you are investing in the product without a recommendation and certain products are only suitable for investors who have met the product issuers' minimum qualifying criteria.</p>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Switch In Fund:</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:DropDownList ID="ddlFundsList1" runat="server" ClientIDMode="Static" CssClass="form-control mb-10">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Switch Unit:</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:TextBox ID="txtUnits" runat="server" ClientIDMode="Static" placeholder="Enter switch units" CssClass="form-control" onkeypress="CheckNumeric(event);"></asp:TextBox>
                                                    <asp:CheckBox ID="chkSwitchAll" runat="server" ClientIDMode="Static" Text="Switch All" CssClass="checkbox-inline" />
                                                </div>
                                            </div>
                                        </div>
                                        <%--  <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6 col-md-offset-6">
                                                    <a href="javascript:;" id="btnAddFund" class="btn btn-infos1 btn-sm btn-block mb-20">Add</a>
                                                </div>
                                            </div>
                                        </div>--%>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <a href="#" class="btn btn-infos1 btn-sm btn-block mb-20" data-toggle="modal" data-target="#orderHistory">Order History</a>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-3">
                                            <a href="javascript:;" id="btnAddFund" class="btn btn-infos1 btn-sm btn-block mb-20">Add</a>
                                        </div>
                                    </div>
                                    <div class="form-group mb-10">
                                        <%--<p class="fs-12 text-justify mb-5" id="warningLabel" style="color:red;line-height : 13px; font-size: 11px; display:none;">* Please be informed that you are investing in the product without a recommendation and certain products are only suitable for investors who have met the product issuers' minimum qualifying criteria.</p>--%>
                                        <%-- <p class="fs-12 text-justify mb-5" id="WarningLabel" style="color:red; line-height:13px; font-size:11px; display: none">* Please be informed that you are investing in the product without a recommendation and certain products are only suitable for investors who have met the product issuers' minimum qualifying criteria.</p>--%>
                                    </div>


                                    <div id="divFundDetails" class="hide">
                                        <div class="loadingDiv">
                                            <div class="typing_loader"></div>
                                            <div class="text-center">Loading...</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="mt-15 cumulative" id="fundperformancetable1" style="display: none">
                                                    <h6 id="FromId" class="mb-2" style="color: #2071bf; font-weight: bold"></h6>
                                                    <div class="table-responsive">
                                                        <table class="table chart-table table-bordered myDataTable fund-table fundPerformanceCumulativeTable">
                                                            <thead>
                                                                <tr class="fs-13">
                                                                    <th style="width: 200px">Period <small class="pull-right">(Dividend date: <span id="Span1" runat="server"></span>)</small></th>
                                                                    <th style="width: 60px;">1 week </th>
                                                                    <th style="width: 60px;">1 month </th>
                                                                    <th style="width: 60px;">3 months</th>
                                                                    <th style="width: 60px;">6 months</th>
                                                                    <th style="width: 60px;">1 year</th>
                                                                    <th style="width: 60px;">2 years</th>
                                                                    <th style="width: 60px;">3 years</th>
                                                                    <th style="width: 60px;">5 years</th>
                                                                    <th style="width: 60px;">10 years</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="fs-13">
                                                                    <td>BID TO BID Returns (%) - MYR </td>
                                                                    <td><span id="weekCValue" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="monthCValue" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="month3CValue" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="month6CValue" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="yearCValue" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="year2CValue" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="year3CValue" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="year5CValue" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="year10CValue" runat="server" clientidmode="Static"></span></td>
                                                                </tr>
                                                                <tr class="fs-13">
                                                                    <td>Average </td>
                                                                    <td><span id="weekCValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="monthCValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="month3CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="month6CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="yearCValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="year2CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="year3CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="year5CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                    <td><span id="year10CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                </tr>
                                                            </tbody>
                                                            <tfoot>
                                                                <tr class="fs-12">
                                                                    <td colspan="10" style='font-family: "Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif;'>Performance figures are absolute returns based on the price of the fund as at <strong id="fundPricedate" runat="server" clientidmode="Static">WHEN?</strong> (Last updated on <strong id="fundPriceLastUpdatedDate" runat="server" clientidmode="Static">WHEN?</strong>), on NAV-to-NAV basis with dividends being 'reinvested' on the dividend date.</td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="mt-15 cumulative" id="fundperformancetable2" style="display: none">
                                                    <h6 id="ToId" class="mb-2" style="color: #2071bf; font-weight: bold">-</h6>
                                                    <div class="table-responsive">
                                                    <table class="table chart-table table-bordered myDataTable fund-table fundPerformanceCumulativeTable">
                                                        <thead>
                                                            <tr class="fs-13">
                                                                <th style="width: 200px">Period <small class="pull-right">(Dividend date: <span id="dividendDateValue" runat="server"></span>)</small></th>
                                                                <th style="width: 60px;">1 week </th>
                                                                <th style="width: 60px;">1 month </th>
                                                                <th style="width: 60px;">3 months</th>
                                                                <th style="width: 60px;">6 months</th>
                                                                <th style="width: 60px;">1 year</th>
                                                                <th style="width: 60px;">2 years</th>
                                                                <th style="width: 60px;">3 years</th>
                                                                <th style="width: 60px;">5 years</th>
                                                                <th style="width: 60px;">10 years</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr class="fs-13">
                                                                <td>BID TO BID Returns (%) - MYR </td>
                                                                <td><span id="weekCValue2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="monthCValue2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="month3CValue2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="month6CValue2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="yearCValue2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year2CValue2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year3CValue2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year5CValue2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year10CValue2" runat="server" clientidmode="Static"></span></td>
                                                            </tr>
                                                            <tr class="fs-13">
                                                                <td>Average </td>
                                                                <td><span id="weekCValueAvg2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="monthCValueAvg2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="month3CValueAvg2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="month6CValueAvg2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="yearCValueAvg2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year2CValueAvg2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year3CValueAvg2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year5CValueAvg2" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year10CValueAvg2" runat="server" clientidmode="Static"></span></td>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr class="fs-12">
                                                                <td colspan="10" style='font-family: "Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif;'>Performance figures are absolute returns based on the price of the fund as at <strong id="fundPricedate2" runat="server" clientidmode="Static">WHEN?</strong> <%--(Last updated on <strong id="fundPriceLastUpdatedDate2" runat="server" clientidmode="Static">WHEN?</strong>)--%>, on NAV-to-NAV basis with dividends being 'reinvested' on the dividend date.</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                                    </div>

                                                <div class="form-group mb-10">
                                                    <%--<p class="fs-12 text-justify mb-5" id="WarningLabel" style="color:red; display: none">* Please be informed that you are investing in the product without a recommendation and certain products are only suitable for investors who have met the product issuers' minimum qualifying criteria.</p>--%>
                                                </div>
                                                <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                    <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td>From</td>
                                                            <td>To</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Fund Name</td>
                                                            <td class="fundName">Fund Name</td>
                                                            <td class="TfundName" style="color: #2071bf; font-weight: bold">Fund Name</td>
                                                        </tr>
                                                        <tr class="bg-info">
                                                            <td>Units Held</td>
                                                            <td class="unitHolding">none</td>
                                                            <td class="TunitHolding">none</td>
                                                        </tr>
                                                        <tr class="bg-info">
                                                            <td>Units Available</td>
                                                            <td class="aUnits">Unit Available</td>
                                                            <td>-</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Fund - Risk Profile</td>
                                                            <td class="riskCriteria">Risk Profile</td>
                                                            <td class="TriskCriteria">Risk Profile</td>
                                                        </tr>
                                                        <tr>
                                                            <td id="salesChargeTitle">Sales Charge</td>
                                                            <td class="salesCharge">Sales Charge</td>
                                                            <td class="TsalesCharge">Sales Charge</td>
                                                        </tr>
                                                        <tr>
                                                            <td id="minimumInitialInvestementTitle">Minimum Initial Investement (CASH/EPF)</td>
                                                            <td class="minimumInitialInvestement">Minimum Initial Investement</td>
                                                            <td class="TminimumInitialInvestement">Minimum Initial Investement</td>
                                                        </tr>
                                                        <tr>
                                                            <td id="minimumSubsequentInvestementTitle">Minimum Subsequent Investement (CASH/EPF)</td>
                                                            <td class="minimumSubsequentInvestement">Minimum Subsequent Investement</td>
                                                            <td class="TminimumSubsequentInvestement">Minimum Subsequent Investement</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Minimum Holding Units</td>
                                                            <td class="mhUnits">Minimum Holding Units</td>
                                                            <td class="TmhUnits">Minimum Holding Units</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Master Prospectus</td>
                                                            <td class="masterProspectus">Master Prospectus</td>
                                                            <td class="TmasterProspectus">Master Prospectus</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Product Highlights Sheet</td>
                                                            <td class="productHighlightsSheet">Product Highlights Sheet</td>
                                                            <td class="TproductHighlightsSheet">Product Highlights Sheet</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Information Memorandum</td>
                                                            <td class="informationMemorandum">Information Memorandum</td>
                                                            <td class="TinformationMemorandum">Information Memorandum</td>
                                                        </tr>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="3">Note: <strong>You must READ the Fund Master Prospectus / Prospectus / Information Memorandum and its Supplementary(ies) / Replacement (if any) and Product Highlights Sheet before you proceed to the next step.</strong>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row hide" id="cart-table">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                <thead>
                                                    <tr>
                                                        <th>Selected Fund</th>
                                                        <th>To Fund</th>
                                                        <th class="text-right">Switch Units</th>
                                                        <th class="text-right">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="tbodySelectedFunds" id="tbodySelectedFunds" runat="server" clientidmode="static"></tbody>
                                                <%--<tfoot>
                                                    <tr>
                                                        <td></td>
                                                        <th class="text-right">Total</th>
                                                        <td class="unitFormat tdTotalSwitchingUnits text-right" id="tdTotalSwitchingUnits" runat="server" clientidmode="static"></td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>--%>
                                            </table>

                                            <div class="form-group mb-5">
                                                <asp:CheckBox ID="chkConfirmFundInformation" ClientIDMode="Static" runat="server" CssClass="checkbox-inline text-info text-bold" Text="I confirm that I have read and understood the Fund Prospectus/Information Memorandum and Product Highlights Sheet of" />
                                            </div>
                                            <div class="f1-buttons">
                                                <button type="button" class="btn btn-previous">Previous</button>
                                                <button type="button" class="btn btn-next">Next</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="fieldsetConfirmation">
                            <div class="row">
                                <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                <thead>
                                                    <tr>
                                                        <th>Selected Fund</th>
                                                        <th>To Fund</th>
                                                        <th class="text-right">Switch Units</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="tbodySelectedFunds" id="tbodySelectedFunds1" runat="server" clientidmode="static"></tbody>
                                                <%--<tfoot>
                                                    <tr>
                                                        <td></td>
                                                        <th class="text-right">Total</th>
                                                        <td class="unitFormat tdTotalSwitchingUnits text-right" id="tdTotalSwitchingUnits1" runat="server" clientidmode="static"></td>
                                                    </tr>
                                                </tfoot>--%>
                                            </table>

                                            <%--  <div class="text-center mb-10">
                                                <p>
                                                    Please enter transaction password for verification
                            <sup class="text-danger">*</sup>:
                            <asp:TextBox TextMode="Password" ID="txtTransPassword" runat="server" CssClass="form-control" Style="width: 200px; display: inline;" ClientIDMode="Static"></asp:TextBox>
                                                </p>
                                                <div>
                                                    <p>
                                                        <small><strong>Note:</strong>
                                                            <span>By default, transaction password will be your login password.</span>
                                                        </small>
                                                    </p>
                                                </div>
                                            </div>--%>

                                            <div class="f1-buttons">
                                                <button type="button" class="btn btn-previous">Previous</button>
                                                <asp:Button ID="btnSubmit" runat="server" ClientIDMode="Static" CssClass="btn btn-next btn-infos1" Text="Proceed" OnClick="btnSubmit_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <div class="text-center hide" id="loadAfterSubmitTransaction">
                            <h4>Please wait while we are processing your order...</h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <asp:HiddenField ID="hdnAccountPlan" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnUnitHoldingFrom" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnUnitHoldingTo" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnAvailableUnits" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnNavPrice" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnMinimumAmount" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSalesCharge" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnTSalesCharge" runat="server" ClientIDMode="Static" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
    <div class="modal fade" id="commonTermsPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="height: 92%;">
        <div class="modal-content trans">

            <div class="modal-body">
                <!-- style="height: calc(100vh - 250px); overflow-y: scroll;" -->
                <div id="commonTermsPopupModalBodyInner">
                    <h5 style="color: #059cce; font-size:16px;" class="text-justify">You are about to switch. Kindly make sure you have confirmed that:</h5>
                    <ul>
                        <li>you have read and agree to be bound by the terms and conditions set up on the facility;</li>
                        <li>you have read and fully understand the contents of the disclaimer, privacy policy, internet risk, transaction notice and below notice;</li>
                        <li>you are eligible to apply for the units in the fund(s), e.g. that you have attained 18 years of age unless otherwise allowed under the terms of the Master Prospectus / Prospectus / Information Memorandum and its Supplementary(ies) / Replacement;</li>
                        <li>you have given consent to Apex Investment Services Berhad (“AISB”) to disclose information pertaining to you, including but not limited to proprietary information, to the relevant entities involved in the fund(s) as well as to the Securities Commission Malaysia (“SC”).</li>
                    </ul>
                    <p>Neither AISB nor any of its directors, consultants, representatives or employees accept any liability whatsoever for any direct, indirect or consequences losses (in contract, tort or otherwise) arising from this transaction or its contents contained herein, except to the extent this would be prohibited by law or regulation.</p>
                    <p>The minimum amount for the switch is 1,000 units. The switching of units will be valued at the NAV per unit that would be ascertained at the end of the Business Day on which the application is accepted. Three free switches per account are allowed in each calendar year. Subsequent switches will be charged a 1% of redemption moneys for administrative purpose. Five (5) working days are required to process each switch. Therefore, the following switching will only be permitted after five (5) working days.</p>
                    <h5>NOTICE</h5>
                    <p>Apex Investment Services Berhad (“AISB”) is responsible for the issuance, circulation or dissemination of the electronic Master Prospectus / Prospectus, electronic Supplementary(ies) / Replacement Master Prospectus / Prospectus, electronic Information Memorandum, electronic Supplementary(ies) / Replacement Information Memorandum, electronic Product Highlights Sheet (collectively referred to as the “Disclosure Document”) and electronic application form. A copy of the Disclosure Document has been registered or lodged with the Securities Commission Malaysia (“SC”). The SC takes no responsibility for the contents and makes no representation on the accuracy or completeness of the Disclosure Document.</p>
                    <p>Investors are advised to read and understand the content of the Disclosure Document before completing the relevant application forms and making any investment decision. A copy of the Disclosure Document is available and investors have the right to request for it. The Disclosure Document can be obtained from our business office, our authorised distributors, our consultants or our representatives. You should also consider the fees and charges involved before investing. All fees and expenses incurred by the Fund is subject to any applicable taxes and / or duties as may be imposed by the government or other authorities from time to time. Prices of units and distributions payable, if any, may go down or up, and past performance of the Fund is not an indication of its future performance. Investors may not get back to the original cost incurred for the investment. Where a unit split / distribution is declared, the issue of additional units / distribution, the NAV per unit will be reduced from pre-unit split NAV / cum-distribution NAV to post-unit split NAV / ex-distribution NAV. Where a unit split is declared, the value of your investment in Malaysian Ringgit will remain unchanged after the distribution of the additional units. Where unit trust loan financing is available, investors are advised to read and understand the contents of the unit trust loan financing risk disclosure statement before deciding to borrow to purchase units. Investors should be aware of the specific risks for the Fund before investing and rely on their own evaluation to assess the merits and risks of the investment. Specific risks and general risks for the Fund is elaborated in the Disclosure Document. Units are issued upon receipt of duly completed relevant application forms referred to and accompanying a copy of the Disclosure Document. The Fund may not be suitable for all and if in doubt, investors should seek independent advice.</p>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-lg-8 col-md-7 text-left">
                        <label class="customcheck">
                            By clicking on the 'Accept' button, I confirm that I have read and accept the above Policy Statement and I wish to proceed.
                    <input type="checkbox" id="chkSwitchProceed">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="col-lg-4 col-md-5">
                        <asp:Button ID="btnAcceptTerms" runat="server" CssClass="btn btn-infos1" Text="Accept" OnClick="btnAcceptTerms_Click" ClientIDMode="Static" OnClientClick="return OnAcceptTerms()" />
                        <button type="button" class="btn btn-infos1-default hide" id="btnDismissPopup" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-infos1-default" data-dismiss="modal" onclick="javascript:window.location.href='Portfolio.aspx'">Decline</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="riskPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4>Disclaimer</h4>
            </div>
            <div class="modal-body">
                <p>You have decided to select a fund that is not recommended in accordance to your SAT. Please be advised of the risks associated with the products Apex Investments make available to you. You should not invest in or deal in any financial product unless you understand its nature and the extent of your exposure to risk. You should also be satisfied that it is suitable for you in the light of your circumstances and position. Different investment products have varied levels of exposure to risks and to different combinations of risks. Kindly speak to our team to find out more about our products.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-infos1" data-dismiss="modal">Accept</button>
                <button type="button" class="btn btn-infos1-default" data-dismiss="modal" onclick="javascript:window.location.href='Index.aspx'">Decline</button>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnTotalQuantity" runat="server" ClientIDMode="Static" />


    <button id="editFundModal" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="display: none">Open Modal</button>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Switching Units</h4>
                </div>
                <div class="modal-body">
                    <input type="text" id="FundAmount" name="FundAmount" class="form-control" placeholder="Enter your units amount here.">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="FundUpdate">Update</button>
                    <button type="button" class="btn btn-default" id="FundUpdateClose" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <%--Order History--%>
    <div class="modal fade" id="orderHistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog sett two">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h4>Order History</h4>
                </div>
                <div class="modal-body" style="height: calc(100vh - 250px); overflow-y: scroll;">
                    <div class="col-md-12">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-cell-pad-5 table-font-size-13 table-condensed table-bordered OrderHistoryTable">
                                        <thead id="theadUserOrders" runat="server">
                                            <tr>
                                                <th style="width: 50px"></th>
                                                <th style="width: 50px">S. No</th>
                                                <th>Account No</th>
                                                <th>Order No</th>
                                                <th>Order Type</th>
                                                <th>Order Date</th>
                                                <th>Order Status</th>
                                                <th class="text-right">Investment Amount(MYR)</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbodyUserOrders" runat="server" clientidmode="Static">
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-infos1" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <%--Order History End--%>
    <asp:HiddenField ID="hdnTermsPop" runat="server" ClientIDMode="Static" Value="1" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/4.1.0/autoNumeric.js"></script>

    <script type="text/javascript">
        var newFundEditId = 0;
        var newFundEditId1 = 0;
        var newFundEditId2 = 0;
        var hdnTermsPop = "0";
        (function ($) {
            hdnTermsPop = $('#hdnTermsPop').val();
            $.fn.inputFilter = function (inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
            };
        }(jQuery));
        function ConvertToCurrency(num) {
                return num.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            }
        $(document).ready(function () {

            $('#ddlUserAccountId').change(function () {
                $('#txtUnits').val('');
                $('#chkSwitchAll').prop('checked', false);
            });

            $('#commonTermsPopup .modal-body').scroll(function () {
                var disable =
                    (
                        $('#commonTermsPopupModalBodyInner').height() != parseInt($(this).scrollTop()) + $(this).height() - 32
                    )
                    &&
                    (
                        $('#commonTermsPopupModalBodyInner').height() - 1 != parseInt($(this).scrollTop()) + $(this).height() - 32
                    )
                    &&
                    (
                        $('#commonTermsPopupModalBodyInner').height() - 2 != parseInt($(this).scrollTop()) + $(this).height() - 32
                    );
                $('#chkSwitchProceed').prop('checked', false);
                $('#chkSwitchProceed').prop('disabled', disable);
                $('#btnAcceptSwitch').prop('disabled', disable);
            });

            //var tcAcceptSwitchSession = sessionStorage['tcAcceptSwitch'];
            //if (tcAcceptSwitchSession != undefined && tcAcceptSwitchSession != null && tcAcceptSwitchSession != "") {
            //    $('#btnDismissPopup').click();
            //}
            //$('#btnAcceptSwitch').click(function () {
            //    if ($('#chkSwitchProceed').is(':checked')) {
            //        $('#btnDismissPopup').click();
            //        var tcAcceptSwitch = 1;
            //        sessionStorage['tcAcceptSwitch'] = tcAcceptSwitch;
            //        $('.loader-bg').fadeIn();
            //        window.location.href = "SwitchFunds.aspx?isTermsModalPopup=0";
            //    }
            //    else {
            //        ShowCustomMessage('Alert', 'Please accept below highlighted statements.', '');
            //    }
            //});
        });

        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });

        function OpenCommonTermsPopup() {
            if ($('#ddlUserAccountId').val() != '') {

                $('#accSummaryDiv, .selectedAccountDetails').removeClass('hide');
                $('#commonTermsPopup').modal({
                    keyboard: false,
                    backdrop: "static",
                });
            }
            else {
                $('#accSummaryDiv, .selectedAccountDetails').addClass('hide');
            }
        }
        function openPopup() {
            $('#riskPopup').modal({
                keyboard: false,
                backdrop: "static",
            });
        }

        function OnAcceptTerms() {
            if ($('#chkSwitchProceed').is(':checked')) {
                $('.loader-bg').fadeIn();
                return true;
            }
            else {
                ShowCustomMessage('Alert', 'Please Read, Understand and Agree to proceed.', '');
                return false;
            }
        }
        if (hdnTermsPop == "1") {
            OpenCommonTermsPopup();
        }
        else {
            if ($('#ddlUserAccountId').val() != '') {
                $('#accSummaryDiv, .selectedAccountDetails').removeClass('hide');
            }
            else {
                $('#accSummaryDiv, .selectedAccountDetails').addClass('hide');
            }
        }

        var isCurFormat;
        function FormatAllCurrency() {
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ' });
            });
            $('.currencyFormatNoSymbol').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: '' });
            });
        }
        FormatAllCurrency();
        var isUniFormat;
        var isUniFormatNoSymbol;
        function FormatAllUnit() {
            $('.unitFormat').each(function () {
                isUniFormat = new AutoNumeric(this, { currencySymbol: '', currencySymbolPlacement: 's', decimalPlaces: '4' });
            });
            $('.unitFormatNoSymbol').each(function () {
                isUniFormatNoSymbol = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '4' });
            });
        }
        FormatAllUnit();
        function scroll_to_class(element_class, removed_height) {
            var scroll_to = $(element_class).offset().top - removed_height;
            if ($(window).scrollTop() != scroll_to) {
                $('html, body').stop().animate({ scrollTop: scroll_to }, 0);
            }
        }
        function bar_progress(progress_line_object, direction) {
            var number_of_steps = progress_line_object.data('number-of-steps');
            var now_value = progress_line_object.data('now-value');
            var new_value = 0;
            if (direction == 'right') {
                new_value = now_value + (100 / number_of_steps);
            }
            else if (direction == 'left') {
                new_value = now_value - (100 / number_of_steps);
            }
            progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
        }

        function ConvertToUnits(num) {
            var numm = num.toFixed(4);
            var prenum = numm.split('.')[0];
            var postnum = numm.split('.').length > 1 ? numm.split('.')[1] : '0';
            var cnum = prenum.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + (postnum == '0' ? "" : "." + postnum);
            return cnum;
        }

        $(document).ready(function () {

            $('#ddlSwitchPercentage').change(function () {
                $('#chkSwitchAll').removeAttr('checked');
                var hdnTotalQuantity = parseFloat($('#hdnUnitHoldingFrom').val());
                var switchPercentage = $('#ddlSwitchPercentage').val();
                var switchUnit = hdnTotalQuantity / 100 * switchPercentage;
                $('#txtUnits').val(switchUnit.toFixed(4));
            });


            //$('#chkSwitchAll').click(function () {
            //    var hdnTotalQuantity = parseFloat($('#hdnAvailableUnits').val());
            //    if ($(this).is(':checked')) {
            //        $('#txtUnits').val(hdnTotalQuantity.toFixed(4));
            //    }
            //    else {
            //        $('#txtUnits').val('0');
            //    }
            //});

            $('#txtUnits').keyup(function () {
                var hdnTotalQuantity = parseFloat($('#hdnAvailableUnits').val());
                var units = parseFloat($('#txtUnits').val());

                if (hdnTotalQuantity > units || hdnTotalQuantity < units)
                    $('#chkSwitchAll').removeAttr('checked');
                if (hdnTotalQuantity == units)
                    $('#chkSwitchAll').attr('checked', 'checked');
                // skip for arrow keys
                //if (event.which >= 37 && event.which <= 40) return;

                //// format number
                //$(this).val(function (index, value) {
                //    return value
                //        .replace(/\D/g, "")
                //        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                //        ;
                //});
            });

            $('.show-extended-row').click(function () {
                if ($(this).find('i').hasClass('fa-plus')) {
                    $('.show-extended-row i.fa-minus').each(function (idx, obj) {
                        var ele = $(obj).parents('.show-extended-row');
                        var extendedTRAll = $(ele).parents('tr').next();
                        var extendedRowAll = $(ele).parents('tr').next().find('.extended-row');
                        $(extendedRowAll).slideUp(500, function () {
                            $(extendedTRAll).addClass('hide');
                        });
                        $(ele).find('i').addClass('fa-plus');
                        $(ele).find('i').removeClass('fa-minus');
                    });
                }
                var extendedTR = $(this).parents('tr').next();
                var extendedRow = $(this).parents('tr').next().find('.extended-row');
                if ($(this).find('i').hasClass('fa-plus')) {
                    $(extendedTR).removeClass('hide');
                    $(extendedRow).slideDown();
                    $(this).find('i').removeClass('fa-plus');
                    $(this).find('i').addClass('fa-minus');
                }
                else {
                    $(extendedRow).slideUp(500, function () {
                        $(extendedTR).addClass('hide');
                    });
                    $(this).find('i').removeClass('fa-minus');
                    $(this).find('i').addClass('fa-plus');
                }
            });

            $('#txtUnits').inputFilter(function (value) {
                return /^\d*(\.)?(\d{0,4})$/.test(value);
            });
            $('#FundAmount').inputFilter(function (value) {
                return /^\d*(\.)?(\d{0,4})$/.test(value);
            });
        });

        var ispasswordValid = false;
        var isConfirmation = false;
        /* Form */
        $('.f1 fieldset:first').fadeIn('slow');
        $('.f1 input[type="text"], .f1 input[type="password"], .f1 input[type="checkbox"], .f1 textarea').on('focus', function () {
            $(this).removeClass('input-error');
        });
        // next step
        $('.f1 .btn-next').on('click', function () {
            if ($(this).attr('id') != 'btnSubmit') {
                var data = $(this).attr('id');
                $('#errorMessage').html('');
                $('#successMessage').html('');
                var parent_fieldset = $(this).parents('fieldset');
                var next_step = true;
                // navigation steps / progress steps
                var current_active_step = $(this).parents('.f1').find('.f1-step.active');
                var progress_line = $(this).parents('.f1').find('.f1-progress-line');
                // fields validation
                if (parent_fieldset.hasClass('fieldsetfundSelectionAndAmount')) {
                    if (!$('#chkConfirmFundInformation').is(":checked")) {
                        ShowCustomMessage('Alert', 'Please read, understand & agree to proceed. Thank you.', '');
                        next_step = false;
                    }
                    else
                        if (CartItems == undefined) {
                            ShowCustomMessage('Alert', 'Please add fund to proceed. Thank you.', '');
                            next_step = false;
                        }
                        else {
                            if (CartItems.length == 0) {
                                ShowCustomMessage('Alert', 'Please add fund to proceed. Thank you.', '');
                                next_step = false;
                            }
                        }
                }
                else
                    parent_fieldset.find('input[type="text"], input[type="password"], input[type="checkbox"], textarea, select').each(function () {
                        if ($(this).val() == "") {
                            $(this).addClass('input-error');
                            next_step = false;
                        }
                        else {
                            if ($(this).is(':checkbox')) {
                                if (!$(this).attr('checked')) {
                                    $(this).parents('.form-group').addClass('input-error');
                                    next_step = false;
                                }
                                else {
                                    $(this).parents('.form-group').removeClass('input-error');
                                }
                            }
                            else {
                                $(this).removeClass('input-error');
                            }
                        }
                    });
                // fields validation
                if (next_step) {
                    fundCodeSelected = $('#hdnFundId').val();
                    if (fundCodeSelected != "") {
                        if ($('#ddlFundsList option[data-code="' + fundCodeSelected + '"]').val() != undefined) {
                            $('#ddlFundsList').val($('#ddlFundsList option[data-code="' + fundCodeSelected + '"]').val());
                            $('#ddlFundsList').change();
                        }
                    }
                    parent_fieldset.fadeOut(400, function () {
                        // change icons
                        current_active_step.removeClass('active').addClass('activated').next().addClass('active');
                        // progress bar
                        bar_progress(progress_line, 'right');
                        // show next step
                        $(this).next().fadeIn();
                        // scroll window to beginning of the form
                        scroll_to_class($('.f1'), 20);
                    });
                }
            }
        });
        // previous step
        $('.f1 .btn-previous').on('click', function () {
            // navigation steps / progress steps
            var current_active_step = $(this).parents('.f1').find('.f1-step.active');
            var progress_line = $(this).parents('.f1').find('.f1-progress-line');
            $(this).parents('fieldset').fadeOut(400, function () {
                // change icons
                current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
                // progress bar
                bar_progress(progress_line, 'left');
                // show previous step
                $(this).prev().fadeIn();
                // scroll window to beginning of the form
                scroll_to_class($('.f1'), 20);
            });
        });
        // submit
        $('.f1').on('submit', function (e) {
            // fields validation
            $(this).find('input[type="text"], input[type="password"], input[type="checkbox"], textarea').each(function () {
                if ($(this).val() == "") {
                    e.preventDefault();
                    $(this).addClass('input-error');
                }
                else {
                    $(this).removeClass('input-error');
                    $('#loadAfterSubmitTransaction').removeClass('hide');
                    setTimeout(function () {
                        $('.loader-bg').fadeIn();
                    }, 500);
                }
            });
            // fields validation
        });
        // confirm button
        //$('#btnSubmit').click(function () {
        //    isConfirmation = true;
        //    if (!ispasswordValid) {
        //        $.ajax({
        //            url: "SwitchFunds.aspx/TransPasswordVerify",
        //            contentType: 'application/json; charset=utf-8',
        //            type: "GET",
        //            dataType: "JSON",
        //            async: true,
        //            data: { 'password': JSON.stringify(transPwd) },
        //            success: function (data) {
        //                if (data.d == true) {
        //                    ispasswordValid = true;
        //                    $('#btnSubmit').click();
        //                }
        //                else {
        //                    ispasswordValid = false;
        //                    ShowCustomMessage('Alert', 'Invalid password', '')
        //                }
        //            }
        //        });
        //        return false;
        //    }
        //});
        function BindCart() {
            $('.tdTotalSwitchingUnits').html(TotalUnits);
            $('#tbodySelectedFunds, #tbodySelectedFunds1').html('');
            var tbodySelectedFundsHtml = "";
            if (CartItems.length > 0) {
                $('#cart-table').removeClass('hide');
            }
            $.each(CartItems, function (idx, obj) {
                tbodySelectedFundsHtml += "<tr>";
                tbodySelectedFundsHtml += "<td>" + obj.UtmcFundInformation.FundName + "</td>"
                tbodySelectedFundsHtml += "<td>" + obj.UtmcFundInformation2.FundName + "</td>"
                tbodySelectedFundsHtml += "<td class='unitFormat text-right'>" + obj.Units + "</td>"
                tbodySelectedFundsHtml += "<td class='text-right'><div class='btn-group' role='group'><a href='javascript:;' data-id=" + obj.Id + " fund-id=" + obj.UtmcFundInformation.Id + " fund-id2=" + obj.UtmcFundInformation2.Id + " class='btn btn-sm btn-default editFund' data-fund-amount='" + obj.Units + "' data-toggle='tooltip' title='Edit' style='padding-left: 8px;'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a><a href='javascript:;' data-id=" + obj.Id + " class='btn btn-sm btn-default removeFund' data-toggle='tooltip' title='Remove' style='padding-left: 2px;'><i class='fa fa-trash-o' aria-hidden='true'></i></a></div></td>";
                tbodySelectedFundsHtml += "</tr>";
            });
            $('#tbodySelectedFunds').html(tbodySelectedFundsHtml);
            tbodySelectedFundsHtml = "";
            $.each(CartItems, function (idx, obj) {
                tbodySelectedFundsHtml += "<tr>";
                tbodySelectedFundsHtml += "<td>" + obj.UtmcFundInformation.FundName + "</td>"
                tbodySelectedFundsHtml += "<td>" + obj.UtmcFundInformation2.FundName + "</td>"
                tbodySelectedFundsHtml += "<td class='unitFormat text-right'>" + obj.Units + "</td>"
                tbodySelectedFundsHtml += "</tr>";
            });
            $('#tbodySelectedFunds1').html(tbodySelectedFundsHtml);
            ChangeTC();
            FormatAllCurrency();
            FormatAllUnit();
            $('#ddlFundsList').val('0');
            $('#ddlFundsList').change();
            $('#ddlFundsList1').val('0');
            $('#ddlFundsList1').change();
        }

        var Term;
        function ChangeTC() {
            Term = "I confirm that I have read and understood the Fund Prospectus/Information Memorandum and Product Highlights Sheet of ";
            $.each(CartItems, function (idx, obj) {
                Term += obj.UtmcFundInformation.FundName + ',' + obj.UtmcFundInformation2.FundName + ',';
            });
            Term = Term.substring(0, Term.length - 1);
            document.getElementById("chkConfirmFundInformation").nextSibling.innerHTML = Term;
        }

        var CartItems;
        var TotalUnits;
        $(document).ready(function () {

            $('#chkSwitchAll').click(function () {

                if (CartItems != undefined) {
                    var hdnTotalQuantityFrom = parseFloat($('#hdnUnitHoldingFrom').val());
                    var Id1 = parseInt($('#ddlFundsList').val());
                    var Id2 = parseInt($('#ddlFundsList1').val());
                    var items2 = $.grep(CartItems, function (obj, idx) {
                        return obj.UtmcFundInformation.Id == parseInt(Id1) && obj.UtmcFundInformation2.Id == parseInt(Id2);
                    });
                    if (items2.length > 0) {
                        $('#hdnAvailableUnits').val((hdnTotalQuantityFrom - fund1Total).toFixed(4));
                    }
                    var items = $.grep(CartItems, function (obj, idx) {
                        return obj.UtmcFundInformation.Id == parseInt(Id1) && obj.UtmcFundInformation2.Id != parseInt(Id2);
                    });
                    if (items.length > 0) {
                        var sumUnits = items.map(o => o.Units).reduce((a, c) => { return a + c });
                        $('#hdnAvailableUnits').val((hdnTotalQuantityFrom - fund1Total - sumUnits).toFixed(4));
                    }
                }

                var hdnTotalQuantity = parseFloat($('#hdnAvailableUnits').val());
                if ($(this).is(':checked')) {

                    var str = hdnTotalQuantity.toFixed(4).toString().split('.');
                    if (str[0].length >= 4)
                        str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');

                    $('#txtUnits').val(str.join('.'));
                }
                else {
                    $('#txtUnits').val('0');
                }
            });

            $.ajax({
                url: "SwitchFunds.aspx/BindFunds",
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: { userAccountNo: $('#ddlUserAccountId').val() },
                success: function (data) {
                    CartItems = data.d.CartItems;
                    TotalUnits = data.d.TotalUnits;
                    BindCart();
                }
            });

            $('.btn-previous').click(function () {
                $('#successMessage').html('');
                $('#errorMessage').html('');
            });

            $('#btnAddFund').click(function () {
                
                if (CartItems != undefined) {
                    var hdnTotalQuantityFrom = parseFloat($('#hdnUnitHoldingFrom').val());
                    var Id1 = parseInt($('#ddlFundsList').val());
                    var Id2 = parseInt($('#ddlFundsList1').val());
                    var items2 = $.grep(CartItems, function (obj, idx) {
                        return obj.UtmcFundInformation.Id == parseInt(Id1) && obj.UtmcFundInformation2.Id == parseInt(Id2);
                    });
                    if (items2.length > 0) {
                        $('#hdnAvailableUnits').val((hdnTotalQuantityFrom).toFixed(4));
                    }
                    var items = $.grep(CartItems, function (obj, idx) {
                        return obj.UtmcFundInformation.Id == parseInt(Id1) && obj.UtmcFundInformation2.Id != parseInt(Id2);
                    });
                    if (items.length > 0) {
                        var sumUnits = items.map(o => o.Units).reduce((a, c) => { return a + c });
                

                        $('#hdnAvailableUnits').val((hdnTotalQuantityFrom - fund1Total - sumUnits).toFixed(4));
                    }
                }

                $('#successMessage').html('');
                $('#errorMessage').html('');

                var fundId = $('#ddlFundsList').val();
                var fundId2 = $('#ddlFundsList1').val();
                var units = $('#txtUnits').val();
                var fundCodeSelected = '';

                if (fundId2 == "0" || fundId2 == "") {
                    ShowCustomMessage('Alert', 'Please select fund', '');
                    return false;
                }
                else if (units == "") {
                    ShowCustomMessage('Alert', 'Please enter units', '');
                    return false;
                }

                var availableUnits = parseFloat($('#hdnAvailableUnits').val());
                var minimumHoldingUnits = parseFloat($('.mhUnits').text().replace(',', ''));


                //if (CartItems != undefined) {
                //    var items = $.grep(CartItems, function (obj, idx) {
                //        return obj.UtmcFundInformation.Id == parseInt(fundId) && obj.UtmcFundInformation2.Id == parseInt(fundId2);
                //    });
                //    var isModify = 0;
                //    if (items.length > 0) {
                //        if (confirm('Do you want to modify existing fund units?')) {
                //            isModify = 1;
                //        }
                //        else {
                //            $('#errorMessage').html('Operation Cancelled by you.');
                //            return false;
                //        }
                //    }
                //}
                var message = "";
                units = parseFloat(units.replace(/,/g, ''));

                if (parseInt(units) < 1000) {
                    ShowCustomMessage('Alert', '1,000.0000 units is minimum for switching.', '');
                    return false;
                }
                else if (CartItems != undefined) {
                    var items = $.grep(CartItems, function (obj, idx) {
                        return obj.UtmcFundInformation.Id == parseInt(fundId);
                    });
                    var isModify = 0;
                }
                if (units > availableUnits) {
                    ShowCustomMessage('Alert', 'Available units insufficient.', '');
                    return false;
                }
                else if (units == availableUnits) {

                }
                else if (units > availableUnits - minimumHoldingUnits) {
                    ShowCustomMessage('Alert', 'Amount will exceed minimum fund holding after sell.', '');
                    return false;
                }
                else if (parseFloat((units * document.getElementById('hdnNavPrice').value).toFixed(2)) < document.getElementById('hdnMinimumAmount').value) {

                    if ($('#hdnSalesCharge').val() < $('#hdnTSalesCharge').val()) {
                        //message = "Estimated Sales Charge: " + (((units * document.getElementById('hdnNavPrice').value).toFixed(2)) * ($('#hdnTSalesCharge').val() / 100)).toFixed(2) + "(MYR) </br> Switching from " + $('#hdnSalesCharge').val() + "% sales charge to " + $('#hdnTSalesCharge').val() + "% sales charge"
                    }
                    else {

                    }
                    if ($('#hdnUnitHoldingTo') == 0 || $('#hdnUnitHoldingTo') == "0") {
                        ShowCustomMessage('Alert', 'Unit entered is not enough to invest into the fund with the minimum initial investment amount.', '');
                    }
                    else {
                        ShowCustomMessage('Alert', 'Unit entered is not enough to invest into the fund with the minimum subsequent investment amount.', '');
                    }
                    return false;
                }

                if (fundId2 == "0" || fundId2 == "") {
                    ShowCustomMessage('Alert', 'Please select fund', '')
                    return false;
                }
                else if (units == "") {
                    ShowCustomMessage('Alert', 'Please enter units', '')
                    return false;
                }
                else {
                    $('#errorMessage').html('');
                    $('#successMessage').html('');
                    var fundholdingUnits = $('#ddlFundsList option:selected').attr('data-holding-units');
                    var accountPlan = $('#hdnAccountPlan').val();
                    if (parseFloat(units) > parseFloat(fundholdingUnits.replace(',', ''))) {
                        ShowCustomMessage('Alert', 'Cannot exceed current holding: ' + ConvertToUnits(parseFloat(fundholdingUnits.replace(',', ''))), '');
                        return false;
                    }
                    $('#errorMessage').html('');
                    $.ajax({
                        url: "SwitchFunds.aspx/AddFund",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { fundId: fundId, fundId2: fundId2, units: units, userAccountNo: $('#ddlUserAccountId').val() },
                        success: function (data) {
                            CartItems = data.d.CartItems;
                            TotalUnits = data.d.TotalUnits;
                            BindCart();
                            if (isModify == 1) {
                                if ($('#hdnSalesCharge').val() < $('#hdnTSalesCharge').val()) {
                                    message = "Estimated Sales Charge: " + (((units * document.getElementById('hdnNavPrice').value).toFixed(2)) * ($('#hdnTSalesCharge').val() / 100)).toFixed(2) + "(MYR) </br> Switching from " + $('#hdnSalesCharge').val() + "% sales charge to " + $('#hdnTSalesCharge').val() + "% sales charge"
                                }
                                else {

                                }
                                $('#hdnAvailableUnits').val(availableUnits - units);
                                var n = units * document.getElementById('hdnNavPrice').value;
                                $('#successMessage').html('Successfully Modified with your confirmation. </br> Estimated amount when unit converted to amount: ' + ConvertToCurrency(n) + "(MYR) </br> " + message);
                            }
                            else {
                                if ($('#hdnSalesCharge').val() < $('#hdnTSalesCharge').val()) {
                                    message = "Estimated Sales Charge: " + (((units * document.getElementById('hdnNavPrice').value).toFixed(2)) * ($('#hdnTSalesCharge').val() / 100)).toFixed(2) + "(MYR) </br> Switching from " + $('#hdnSalesCharge').val() + "% sales charge to " + $('#hdnTSalesCharge').val() + "% sales charge";
                                }
                                else {

                                }
                                $('#hdnAvailableUnits').val(availableUnits - units);
                                var n = units * document.getElementById('hdnNavPrice').value;
                                $('#successMessage').html('Successfully Added. </br> Estimated amount when unit converted to amount: ' + ConvertToCurrency(n) + "(MYR) </br> " + message);
                            }

                            $('#FromId').addClass('hide');
                            $('#cart-table').removeClass('hide');
                        }
                    });
                }
            });

            $('#tbodySelectedFunds').on('click', '.removeFund', function () {
                $('#errorMessage').html('');
                $('#successMessage').html('');
                var Id = $(this).attr('data-id');
                $.ajax({
                    url: "SwitchFunds.aspx/RemoveFund",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { Id: Id, userAccountNo: $('#ddlUserAccountId').val() },
                    success: function (data) {
                        CartItems = data.d.CartItems;
                        TotalUnits = data.d.TotalUnits;
                        BindCart();
                        $('#successMessage').html('Successfully removed.');
                        $('#cart-table').addClass('hide');
                    }
                });
            });

            $('#FundUpdate').click(function () {

                //if (CartItems != undefined) {
                //    var hdnTotalQuantityFrom = parseFloat($('#hdnUnitHoldingFrom').val());
                //    var Id1 = parseInt(newFundEditId1);
                //    var Id2 = parseInt(newFundEditId2);
                //    var items2 = $.grep(CartItems, function (obj, idx) {
                //        return obj.UtmcFundInformation.Id == parseInt(Id1) && obj.UtmcFundInformation2.Id == parseInt(Id2);
                //    });
                //    if (items2.length > 0) {
                //        $('#hdnAvailableUnits').val((hdnTotalQuantityFrom - fund1Total).toFixed(4));
                //    }
                //    var items = $.grep(CartItems, function (obj, idx) {
                //        return obj.UtmcFundInformation.Id == parseInt(Id1) && obj.UtmcFundInformation2.Id != parseInt(Id2);
                //    });
                //    if (items.length > 0) {
                //        var sumUnits = items.map(o => o.Units).reduce((a, c) => { return a + c });
                

                //        $('#hdnAvailableUnits').val((hdnTotalQuantityFrom - fund1Total - sumUnits).toFixed(4));
                //    }
                //}

                var availableUnits = parseFloat($('#hdnAvailableUnits').val());
                var minimumHoldingUnits = parseFloat($('.mhUnits').text().replace(',', ''));
                var id = newFundEditId;
                var units2 = document.getElementById("FundAmount").value;
                var units = units2.replace(/,/g, "");
                var executeModify = false;
                if (units == "") {
                    ShowCustomMessage('Alert', 'Please enter amount!', '');
                }
                else if (units == "0") {
                    ShowCustomMessage('Alert', 'Unit is required', '');
                }
                else if (units < 1000) {
                    ShowCustomMessage('Alert', '1,000.0000 units is minimum for switching.', '');
                }
                else if (units > availableUnits) {
                    ShowCustomMessage('Alert', 'Available units insuficient', '');
                }
                else if (units > availableUnits - minimumHoldingUnits) {
                    ShowCustomMessage('Alert', 'Unit will exceed minimum fund holding after sell.', '');
                }
                else if (units * document.getElementById('hdnNavPrice').value < document.getElementById('hdnMinimumAmount').value) {
                    var n = units * document.getElementById('hdnNavPrice').value;
                    ShowCustomMessage('Alert', 'Unit entered is not enough to invest into the fund with the minimum investment amount. </br> Estimated amount when unit converted to amount: ' + ConvertToCurrency(n) + "(MYR)", '');
                }
                else if (units <= availableUnits) {
                    executeModify = true;
                }
                if(executeModify) {
                    $.ajax({
                        url: "SwitchFunds.aspx/EditFund",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { Id: id, Amount: units, userAccountNo: $('#ddlUserAccountId').val() },
                        success: function (data) {
                            newFundEditId = 0;
                            CartItems = data.d.CartItems;
                            TotalUnits = data.d.TotalUnits;
                            BindCart();
                            //$('#successMessage').html('Successfully Edit.');
                            $('#hdnAvailableUnits').val(availableUnits - units);
                            $('#ddlFundsList').change();
                            $('#ddlFundsList1').change();
                            $("#FundUpdateClose").click();
                        }
                    });
                }
            });


            $('#tbodySelectedFunds').on('click', '.editFund', function () {
                $('#errorMessage').html('');
                $('#successMessage').html('');
                var id = $(this).attr('data-id');
                var fundID = $(this).attr('fund-id');
                //var fundID2 = $(this).attr('fund-id2');
                var fundAmt = $(this).attr('data-fund-amount');
                var minfundAmt = $(this).attr('data-fund-min-amount');
                $('#FundAmount').val(fundAmt);
                $('#txtFundMinAmount').val(minfundAmt);
                newFundEditId = id;
                newFundEditId1 = fundID;
                //newFundEditId2 = fundID2;
                FundDetailsUnitOwned(fundID);
                //FundDetailsUnitOwnedT(fundID2);
                document.getElementById("editFundModal").click();
            });

            $('#txtUnits').inputFilter(function (value) {
                return /^\d*(\.)?(\d{0,4})$/.test(value);
            });
            $('#FundAmount').inputFilter(function (value) {
                return /^\d*(\.)?(\d{0,4})$/.test(value);
            });

        });

        //function FundDetailsUnitOwned(Id) {
        //    console.log('Id: ' + Id);
        //    $.ajax({
        //        type: "GET",
        //        url: "BuyFunds.aspx/GetTotalUnits",
        //        data: { Id: Id, userAccountNo: $('#ddlUserAccountId').val() },
        //        dataType: "JSON",
        //        contentType: "application/json; charset=utf-8",
        //        success: function (data) {
        //            console.log('data.d.totalUnit: '+data.d.totalUnit);
        //            $('#hdnTotalQuantity').val(data.d.totalUnit);
        //            if (data.d.EpfOrOther != "EPF") {
        //                if (data.d.instruction == 0) {
        //                    document.getElementById("ddlDistributionInstruction").disabled = false;
        //                    document.getElementById("ddlDistributionInstruction").value = 3;
        //                }
        //                else {
        //                    document.getElementById("ddlDistributionInstruction").disabled = false;
        //                    document.getElementById("ddlDistributionInstruction").value = (data.d.instruction == 0 ? 3 : data.d.instruction);
        //                }
        //            } else {
        //                document.getElementById("ddlDistributionInstruction").disabled = false;
        //                document.getElementById("ddlDistributionInstruction").value = 3;
        //            }
        //            FundDetails(Id);
        //            GetFundPerformance(Id);
        //        }
        //    });
        //}

        $('#rdnRecommended').click(function () {
            if ($(this).is(":checked")) {
                GetFundListByCategory(2);
                $('#WarningLabel').addClass('hide');
                $('#btnAddFund').attr('disabled', true);
                $('#txtUnits').val('');
                $('#txtUnits').attr('disabled', true);
                $('#chkSwitchAll').attr('disabled', true);
                $('#chkSwitchAll').removeAttr('checked');
            }
        });
        $('#rdnNonRecommended').click(function () {
            if ($(this).is(":checked")) {
                GetFundListByCategory(3);
                $('#WarningLabel').removeClass('hide');
                $('#btnAddFund').attr('disabled', true);
                $('#txtUnits').val('');
                $('#txtUnits').attr('disabled', true);
                $('#chkSwitchAll').attr('disabled', true);
                $('#chkSwitchAll').removeAttr('checked');
            }
        });

        $('#rdnRecommended').attr('checked', 'checked');
        $('#rdnRecommended').click();

        function GetFundListByCategory(Id) {
            $.ajax({
                type: "GET",
                url: "SwitchFunds.aspx/GetFundList",
                data: { Id: Id, userAccountNo: $('#ddlUserAccountId').val() },
                dataType: "JSON",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    document.getElementById("ddlFundsList1").options.length = 0;
                    var select = document.getElementById('ddlFundsList1');
                    //select[select.length] = new Option('Select To Fund', '0');

                    if (data.d.Name.length == 0) {
                        if (Id == 2)
                            ShowCustomMessage('Alert', 'There is no recommended funds.', '');
                        else {
                            ShowCustomMessage('Alert', 'There is no non-recommended funds.', '');
                            $('#rdnRecommended').attr('checked', 'checked');
                            $('#rdnRecommended').click();
                        }
                    }

                    //$('#ddlFundsList option').remove();
                    //$('#ddlFundsList').append('<option value="0">Select</option>');
                    //for (i = 0; i < data.d.Name.length; i++) {
                    //    $('#ddlFundsList').append('<option data-risk="' + data.d.Risk[i] + '" value="' + data.d.Id[i] + '">' + data.d.Name[i] + '</option>');
                    //}

                    var selectedFundId = $('#ddlFundsList').val();
                    $('#ddlFundsList1 option').remove();
                    $('#ddlFundsList1').append('<option value="0">Select</option>');
                    for (i = 0; i < data.d.Name.length; i++) {
                        if (parseInt(selectedFundId) != data.d.Id[i])
                            $('#ddlFundsList1').append('<option data-risk="' + data.d.Risk[i] + '" value="' + data.d.Id[i] + '">' + data.d.Name[i] + '</option>');
                    }

                    //for (i = 0; i < data.d.Name.length; i++) {
                    //    if () {
                    //        select[select.length] = new Option(data.d.Name[i], data.d.Id[i]);
                    //    }
                    //}
                    document.getElementById('txtUnits').disable = true;


                    //$('#hdnTotalQuantity').val(data.d);
                    //FundDetails(Id);
                    //GetFundPerformance(Id);
                }
            });
        }

        $('#ddlFundsList').change(function () {
            $('#divFundDetails').removeClass('hide');
            $('#btnAddFund').attr('disabled', true);
            $('#txtUnits').val('');
            $('#txtUnits').attr('disabled', true);
            $('#errorMessage').html('');
            $('#successMessage').html('');
            $('#chkSwitchAll').removeAttr('checked');
            $('#ddlFundsList1').val('0');
            //document.getElementById("txtUnits").disable = true;
            if ($(this).val() != "" && $(this).val() != null && $(this).val() != "0") {
                var Id = parseInt($(this).val());
                document.getElementById("chkSwitchAll").checked = false;
                //document.getElementById("txtUnits").value = "1,000";
                if ($('#rdnRecommended').is(':checked')) {
                    $('#rdnRecommended').click();
                }
                if ($('#rdnNonRecommended').is(':checked')) {
                    $('#rdnNonRecommended').click();
                }
                FundDetailsUnitOwned(Id);
                $('#ddlFundsList1').attr('disabled', false);

                var yourUl = document.getElementById("fundperformancetable1");
                yourUl.style.display = '';
            }
            else {
                $('#divFundDetails').addClass('hide');
                $('#ddlFundsList1').attr('disabled', true);
            }
        });

        $('#ddlFundsList1').change(function () {
            $('#btnAddFund').attr('disabled', true);
            if ($(this).val() != "" && $(this).val() != null && $(this).val() != "0") {
                //if (document.getElementById("rdnNonRecommended").checked)
                //    document.getElementById("WarningLabel").style.display = "block";
                //else
                //    document.getElementById("WarningLabel").style.display = "none";
                var Id = parseInt($(this).val());
                FundDetailsUnitOwnedT(Id);
                $('#chkSwitchAll').attr('disabled', false);
                var yourUl = document.getElementById("fundperformancetable2");
                yourUl.style.display = '';
            }
            else {
                $('#divFundDetails').addClass('hide');
                $('#btnAddFund').attr('disabled', true);
                $('#txtUnits').val('');
                $('#txtUnits').attr('disabled', true);
                $('#chkSwitchAll').attr('disabled', true);
                $('#chkSwitchAll').removeAttr('checked');
            }
        });

        var fundDetails;
        var minInitialInvestmentCashEPF = "0,0";
        var splits = '';
        var fund1Total = 0;
        function FundDetails(Id) {
            if (Id != 0) {
                var hdnTotalQuantity = parseFloat($('#hdnUnitHoldingFrom').val());
                var accountPlan = $('#hdnAccountPlan').val();
                console.log(hdnTotalQuantity);
                $('.loadingDiv').removeClass('hide');
                $('#divFundDetails').removeClass('hide');
                $.ajax({
                    url: "SwitchFunds.aspx/GetFundDetails",
                    async: true,
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { Id: Id, userAccountNo: $('#ddlUserAccountId').val() },
                    success: function (data) {
                        splits = data.d.splits;
                        var json = data.d.UtmcFundInformation;
                        fundDetails = json;
                        document.getElementById('hdnNavPrice').value = json.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice;
                        $('.fundName').html(json.FundName);

                        if (accountPlan == "EPF") {
                            $('#salesChargeTitle').text("EPF Sales Charge")
                            $('.salesCharge').html(json.UtmcFundInformationIdUtmcFundCharges[0].EpfSalesChargesPercent + "%");
                            $('#hdnSalesCharge').val(json.UtmcFundInformationIdUtmcFundCharges[0].EpfSalesChargesPercent);
                        }
                        else {
                            $('.salesCharge').html(json.UtmcFundInformationIdUtmcFundCharges[0].InitialSalesChargesPercent + "%");
                            $('#hdnSalesCharge').val(json.UtmcFundInformationIdUtmcFundCharges[0].InitialSalesChargesPercent);
                        }
                        if (json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentEpf == 0) {
                            $('.minimumInitialInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentCash + "</span>");
                            document.getElementById('minimumInitialInvestementTitle').innerHTML = "Minimum Initial Investement (CASH)";
                        }
                        else {
                            $('.minimumInitialInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentCash + "</span>/<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentEpf + "</span>");
                            document.getElementById('minimumInitialInvestementTitle').innerHTML = "Minimum Initial Investement (CASH/EPF)";
                        }
                        if (json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf == 0) {
                            $('.minimumSubsequentInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash + "</span>");
                            document.getElementById('minimumSubsequentInvestementTitle').innerHTML = "Minimum Subsequent Investement (CASH)";
                        }
                        else {
                            $('.minimumSubsequentInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash + "</span>/<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf + "</span>");
                            document.getElementById('minimumSubsequentInvestementTitle').innerHTML = "Minimum Subsequent Investement (CASH/EPF)";
                        }
                        $('.mhUnits').html("<span class='unitFormat'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinHoldingUnits + "</span>");
                        if (newFundEditId == 0) {
                            $('.riskCriteria').html($('#ddlFundsList option:selected').attr('risk-group').trim());
                        }
                        $('.unitHolding').html("<span class='unitFormat'>" + hdnTotalQuantity.toFixed(4) + "</span>");
                        $('#FromId').html(json.FundName);
                        //if ($('#ddlFundsList option:selected').text().split(":")[2].trim() == 'High Risk') {
                        //    openPopup();
                        //}
                        $('.aUnits').html("<span class='unitFormat'>" + (hdnTotalQuantity - data.d.total).toFixed(4) + "</span>");
                        $('#hdnAvailableUnits ').val((hdnTotalQuantity - data.d.total).toFixed(4));

                        var hdnAvailableUnits = parseFloat($('#hdnAvailableUnits').val());

                        if (newFundEditId == 0) {
                            fund1Total = data.d.total;
                            if (CartItems != undefined) {
                                var Id2 = parseInt($('#ddlFundsList1').val());
                                var items2 = $.grep(CartItems, function (obj, idx) {
                                    return obj.UtmcFundInformation.Id == parseInt(Id) && obj.UtmcFundInformation2.Id == parseInt(Id2);
                                });
                                if (items2.length > 0) {
                                    $('#hdnAvailableUnits ').val((hdnAvailableUnits).toFixed(4));
                                }
                                else {
                                    var items = $.grep(CartItems, function (obj, idx) {
                                        return obj.UtmcFundInformation.Id == parseInt(Id);
                                    });
                                    if (items.length > 0) {
                                        var sumUnits = items.map(o => o.Units).reduce((a, c) => { return a + c });
                                        $('#hdnAvailableUnits ').val((hdnAvailableUnits - sumUnits).toFixed(4));
                                    }
                                }
                            }
                        }

                        $('#hdnMinSubsequentInvestmentCashEPF').val(json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash + "," + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf);
                        var masterProspectus = $.grep(
                            json.UtmcFundInformationIdUtmcFundFiles,
                            function (obj) {
                                return obj.UtmcFundFileTypesDefId == 1
                            })[0];
                        if (masterProspectus != null) {
                            if (masterProspectus.Url != null) {
                                var a = $('<a />').attr('href', masterProspectus.Url).attr('target', '_blank').html('Click here to View');
                                $('.masterProspectus').html(a);
                            }
                            else {
                                $('.masterProspectus').html('-');
                            }
                        }
                        else {
                            $('.masterProspectus').html('-');
                        }

                        var productHighlightsSheet = $.grep(
                            json.UtmcFundInformationIdUtmcFundFiles,
                            function (obj) {
                                return obj.UtmcFundFileTypesDefId == 3
                            })[0];
                        if (productHighlightsSheet != null) {
                            if (productHighlightsSheet.Url != null) {
                                var a = $('<a />').attr('href', productHighlightsSheet.Url).attr('target', '_blank').html('Click here to View');
                                $('.productHighlightsSheet').html(a);
                            }
                            else {
                                $('.productHighlightsSheet').html('-');
                            }
                        }
                        else {
                            $('.productHighlightsSheet').html('-');
                        }

                        var informationMemorandum = $.grep(
                            json.UtmcFundInformationIdUtmcFundFiles,
                            function (obj) {
                                return obj.Name == "Information Memorandum"
                            })[0];
                        if (informationMemorandum != null) {
                            if (informationMemorandum.Url != null) {
                                var a = $('<a />').attr('href', informationMemorandum.Url).attr('target', '_blank').html('Click here to View');
                                $('.informationMemorandum').html(a);
                            }
                            else {
                                $('.informationMemorandum').html('-');
                            }
                        }
                        else {
                            $('.informationMemorandum').html('-');
                        }
                        $('#chkConfirmFundInformation').removeAttr('checked');
                        $('.loadingDiv').addClass('hide');
                        //$('#txtUnits').attr('disabled', false);
                        FormatAllCurrency();
                    }
                });
            }
        }

        function FundDetails2(Id) {
            $('#chkSwitchAll').hide();
            $('#txtUnits').attr('disabled', true);
            $('#txtUnits').val('');
            $('#chkSwitchAll').removeAttr('checked');
            var hdnTotalQuantityFrom = parseFloat($('#hdnUnitHoldingFrom').val());
            var hdnTotalQuantity = parseFloat($('#hdnUnitHoldingTo').val());
            $('.loadingDiv').removeClass('hide');
            $('#divFundDetails').removeClass('hide');
            $.ajax({
                url: "SwitchFunds.aspx/GetFundDetails",
                async: true,
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: { 'Id': Id, userAccountNo: $('#ddlUserAccountId').val() },
                success: function (data) {

                    if (newFundEditId == 0) {
                        if (CartItems != undefined) {
                            var Id1 = parseInt($('#ddlFundsList').val());
                            var Id2 = parseInt($('#ddlFundsList1').val());
                            var items2 = $.grep(CartItems, function (obj, idx) {
                                return obj.UtmcFundInformation.Id == parseInt(Id1) && obj.UtmcFundInformation2.Id == parseInt(Id2);
                            });
                            if (items2.length > 0) {
                                $('#hdnAvailableUnits').val((hdnTotalQuantityFrom - fund1Total).toFixed(4));
                            }
                            var items = $.grep(CartItems, function (obj, idx) {
                                return obj.UtmcFundInformation.Id == parseInt(Id1) && obj.UtmcFundInformation2.Id != parseInt(Id2);
                            });
                            if (items.length > 0) {
                                var sumUnits = items.map(o => o.Units).reduce((a, c) => { return a + c });
                                $('#hdnAvailableUnits').val((hdnTotalQuantityFrom - fund1Total - sumUnits).toFixed(4));
                            }
                        }
                    }
                    $('#txtUnits').attr('disabled', false);
                    $('#chkSwitchAll').show();
                    splits = data.d.splits;
                    var json = data.d.UtmcFundInformation;
                    fundDetails = json;
                    var accountPlan = $('#hdnAccountPlan').val();
                    if (accountPlan == "CASH") {
                        if (hdnTotalQuantity == 0) {
                            document.getElementById('hdnMinimumAmount').value = json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentCash;
                        }
                        else {
                            document.getElementById('hdnMinimumAmount').value = json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash;
                        }
                    }
                    else if (accountPlan == "EPF") {
                        if (hdnTotalQuantity == 0) {
                            document.getElementById('hdnMinimumAmount').value = json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentEpf;
                        }
                        else {
                            document.getElementById('hdnMinimumAmount').value = json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf;
                        }
                    }
                    $('.TfundName').html(json.FundName);
                    if (accountPlan == "EPF") {
                        $('.TsalesCharge').html(json.UtmcFundInformationIdUtmcFundCharges[0].EpfSalesChargesPercent + "%");
                        $('#hdnTSalesCharge').val(json.UtmcFundInformationIdUtmcFundCharges[0].EpfSalesChargesPercent);
                    }
                    else {
                        $('.TsalesCharge').html(json.UtmcFundInformationIdUtmcFundCharges[0].InitialSalesChargesPercent + "%");
                        $('#hdnTSalesCharge').val(json.UtmcFundInformationIdUtmcFundCharges[0].InitialSalesChargesPercent);
                    }

                    if (json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentEpf == 0) {
                        $('.TminimumInitialInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentCash + "</span>");
                    }
                    else {
                        $('.TminimumInitialInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentCash + "</span>/<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentEpf + "</span>");
                    }
                    if (json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf == 0) {
                        $('.TminimumSubsequentInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash + "</span>");
                    }
                    else {
                        $('.TminimumSubsequentInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash + "</span>/<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf + "</span> (EPF)");
                    }
                    $('.TmhUnits').html("<span class='unitFormat'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinHoldingUnits + "</span>");
                    $('.TriskCriteria').html($('#ddlFundsList1 option:selected').attr('data-risk').trim());
                    if (hdnTotalQuantity == 0) {
                        $(".TunitHolding").html("<span>-</span>");
                    }
                    else {
                        $('.TunitHolding').html("<span class='unitFormat'>" + hdnTotalQuantity.toFixed(4) + "</span>");
                    }
                    
                    $('#ToId').html(json.FundName);
                    if ($('#ddlFundsList1 option:selected').attr('data-risk').trim() == 'High Risk') {
                        openPopup();
                    }

                    $('#ThdnMinSubsequentInvestmentCashEPF').val(json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash + "," + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf);
                    var masterProspectus = $.grep(
                        json.UtmcFundInformationIdUtmcFundFiles,
                        function (obj) {
                            return obj.UtmcFundFileTypesDefId == 1
                        })[0];
                    if (masterProspectus != null) {
                        if (masterProspectus.Url != null) {
                            var a = $('<a />').attr('href', masterProspectus.Url).attr('target', '_blank').html('Click here to View');
                            $('.TmasterProspectus').html(a);
                        }
                        else {
                            $('.TmasterProspectus').html('-');
                        }
                    }
                    else {
                        $('.TmasterProspectus').html('-');
                    }

                    var productHighlightsSheet = $.grep(
                        json.UtmcFundInformationIdUtmcFundFiles,
                        function (obj) {
                            return obj.UtmcFundFileTypesDefId == 3
                        })[0];
                    if (productHighlightsSheet != null) {
                        if (productHighlightsSheet.Url != null) {
                            var a = $('<a />').attr('href', productHighlightsSheet.Url).attr('target', '_blank').html('Click here to View');
                            $('.TproductHighlightsSheet').html(a);
                        }
                        else {
                            $('.TproductHighlightsSheet').html('-');
                        }
                    }
                    else {
                        $('.TproductHighlightsSheet').html('-');
                    }

                    var informationMemorandum = $.grep(
                        json.UtmcFundInformationIdUtmcFundFiles,
                        function (obj) {
                            return obj.Name == "Information Memorandum"
                        })[0];
                    if (informationMemorandum != null) {
                        if (informationMemorandum.Url != null) {
                            var a = $('<a />').attr('href', informationMemorandum.Url).attr('target', '_blank').html('Click here to View');
                            $('.TinformationMemorandum').html(a);
                        }
                        else {
                            $('.TinformationMemorandum').html('-');
                        }
                    }
                    else {
                        $('.TinformationMemorandum').html('-');
                    }
                    $('#chkConfirmFundInformation').removeAttr('checked');
                    $('.loadingDiv').addClass('hide');
                    $('#btnAddFund').attr('disabled', false);
                    FormatAllCurrency();
                }
            });
        }

        var isCurFormat;
        function FormatAllCurrency() {
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ' });
            });
            $('.currencyFormatNoDecimal').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ', decimalPlaces: '0' });
            });
            $('.unitFormatNoSymbolMarketValue').each(function () {
                isUniFormatNoSymbol = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '2' });
            });
            $('.unitFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '4' });
            });
        }

        function FundDetailsUnitOwned(Id) {
            if (Id != 0) {
                $('#chkSwitchAll').hide();
                $.ajax({
                    type: "GET",
                    url: "SwitchFunds.aspx/GetTotalUnits",
                    data: { Id: Id, userAccountNo: $('#ddlUserAccountId').val() },
                    dataType: "JSON",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        $('#hdnUnitHoldingFrom').val(data.d);
                        $('#hdnAvailableUnits').val(data.d);
                        FundDetails(Id);
                        GetFundPerformance(Id);
                        document.getElementById('txtUnits').disable = false;
                        //$('#chkSwitchAll').show();
                    }
                });
            }
        }

        function FundDetailsUnitOwnedT(Id) {
            $.ajax({
                type: "GET",
                url: "SwitchFunds.aspx/GetTotalUnits",
                data: { Id: Id, userAccountNo: $('#ddlUserAccountId').val() },
                dataType: "JSON",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $('#hdnUnitHoldingTo').val(data.d);
                    FundDetails2(Id);
                    GetFundPerformance2(Id);
                }
            });
        }

        function CheckNumeric(e) {

            if (window.event) // IE 
            {
                if ((e.keyCode < 48 || e.keyCode > 57) && e.keyCode != 46) {
                    console.log("asadsadsa");
                    event.returnValue = false;
                    return false;
                }
            }
            else { // Fire Fox
                if ((e.keyCode < 48 || e.keyCode > 57) && e.keyCode != 46) {
                    console.log("asadsadsa");
                    event.returnValue = false;
                    return false;
                }
            }
            var txtvalue = txtUnits.value;
            var regexp = /^\d+(\.\d{1,2})?$/;
            console.log(regexp.test(txtvalue));
            if (!regexp.test(txtvalue)) {
                return false;
            }
        }

        function GetFundPerformance(Id) {
            $.ajax({
                type: "GET",
                url: "SwitchFunds.aspx/GetFundPerformance",
                data: { Id: Id },
                dataType: "JSON",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    //if ($.fn.DataTable.isDataTable('.fundPerformanceCumulativeTable')) {
                    //    $('.fundPerformanceCumulativeTable').DataTable().destroy();
                    //}
                    $('#weekCValue').html(data.d.w1);
                    $('#monthCValue').html(data.d.m1);
                    $('#month3CValue').html(data.d.m3);
                    $('#month6CValue').html(data.d.m6);
                    $('#yearCValue').html(data.d.y1);
                    $('#year2CValue').html(data.d.y2);
                    $('#year3CValue').html(data.d.y3);
                    $('#year5CValue').html(data.d.y5);
                    $('#year10CValue').html(data.d.y10);

                    $('#weekCValueAvg').html(data.d.w1a);
                    $('#monthCValueAvg').html(data.d.m1a);
                    $('#month3CValueAvg').html(data.d.m3a);
                    $('#month6CValueAvg').html(data.d.m6a);
                    $('#yearCValueAvg').html(data.d.y1a);
                    $('#year2CValueAvg').html(data.d.y2a);
                    $('#year3CValueAvg').html(data.d.y3a);
                    $('#year5CValueAvg').html(data.d.y5a);
                    $('#year10CValueAvg').html(data.d.y10a);

                    $('#fundPricedate').html(data.d.date);
                    $('#fundPriceLastUpdatedDate').html(data.d.date);

                    $('.loadingDiv').addClass('hide');
                    //$(".fundPerformanceCumulativeTable").DataTable({
                    //    dom: '',
                    //    responsive: true,
                    //    searching: false,
                    //    paging: false,
                    //    ordering: false,
                    //    length: false,
                    //    autoWidth: false
                    //});

                }
            });
        }
        $('#FundAmount').keyup(function (event) {

            // skip for arrow keys
            if (event.which >= 37 && event.which <= 40) return;

            // format number
            $(this).val(function (index, value) {
                return value
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    ;
            });
        });
        function GetFundPerformance2(Id) {
            $.ajax({
                type: "GET",
                url: "SwitchFunds.aspx/GetFundPerformance",
                data: { Id: Id },
                dataType: "JSON",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    //if ($.fn.DataTable.isDataTable('.fundPerformanceCumulativeTable')) {
                    //    $('.fundPerformanceCumulativeTable').DataTable().destroy();
                    //}
                    $('#weekCValue2').html(data.d.w1);
                    $('#monthCValue2').html(data.d.m1);
                    $('#month3CValue2').html(data.d.m3);
                    $('#month6CValue2').html(data.d.m6);
                    $('#yearCValue2').html(data.d.y1);
                    $('#year2CValue2').html(data.d.y2);
                    $('#year3CValue2').html(data.d.y3);
                    $('#year5CValue2').html(data.d.y5);
                    $('#year10CValue2').html(data.d.y10);

                    $('#weekCValueAvg2').html(data.d.w1a);
                    $('#monthCValueAvg2').html(data.d.m1a);
                    $('#month3CValueAvg2').html(data.d.m3a);
                    $('#month6CValueAvg2').html(data.d.m6a);
                    $('#yearCValueAvg2').html(data.d.y1a);
                    $('#year2CValueAvg2').html(data.d.y2a);
                    $('#year3CValueAvg2').html(data.d.y3a);
                    $('#year5CValueAvg2').html(data.d.y5a);
                    $('#year10CValueAvg2').html(data.d.y10a);

                    $('#fundPricedate2').html(data.d.date);
                    $('#fundPriceLastUpdatedDate2').html(data.d.date);

                    $('.loadingDiv').addClass('hide');

                    //$(".fundPerformanceCumulativeTable").DataTable({
                    //    dom: '',
                    //    responsive: true,
                    //    searching: false,
                    //    paging: false,
                    //    ordering: false,
                    //    length: false,
                    //    autoWidth: false
                    //});
                }
            });
        }



    </script>
</asp:Content>
