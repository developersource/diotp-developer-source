﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="RegularSavingsPlan.aspx.cs" Inherits="DiOTP.WebApp.RegularSavingsPlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2">
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Portfolio.aspx">eApexIs</a></li>
                            <li class="active">Regular Savings Plan</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Regular Savings Plan (RSP)</h3>
                    </div>
                </div>
            </div>

            <div class="row hide" id="userAccount">
                <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2" style="margin-left: 10px;">
                    <div class="form-group row rsp-account-number">
                        <div class="col-md-4 col-lg-4 mb-10 rsp-account-number">
                            <div class="rsp-account-selection-width">
                                <%--<label>Select Account:</label>--%>
                                <asp:DropDownList CssClass="form-control mobile-select" ID="ddlUserAccountId" AutoPostBack="true" runat="server" ClientIDMode="Static" OnSelectedIndexChanged="ddlUserAccountId_SelectedIndexChanged"></asp:DropDownList>

                                <asp:HiddenField ID="isUserAccountChange" Value="false" runat="server" ClientIDMode="Static" />

                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <small class="hide" id="userAccountTitle" style="font-size: 9px; margin-left: 10px;">* Only applicable to cash account</small>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center form-box">
                    <div class="f1">
                        <div class="f1-steps">
                            <div class="f1-progress">
                                <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
                            </div>
                            <div class="f1-step active">
                                <div class="f1-step-icon"><i class="fa fa-universal-access"></i></div>
                                <p>Introduction</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-money"></i></div>
                                <p>Regular Savings Authorization (RSP)</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-file-text-o"></i></div>
                                <p>Confirmation</p>
                            </div>
                        </div>

                        <small class="text-danger" id="errorMessage"></small>
                        <small class="text-success" id="successMessage"></small>

                        <fieldset class="fieldsetAccountFundsOverview RSP" id="rspIntro">
                            <div class="row">
                                <div class="col-md-12">




                                    <p>Now you can create a Regular Savings Plan for your unit trust investment.</p>
                                    <ul>
                                        <li>The Regular Savings Plan (RSP) is a monthly subscription plan.</li>
                                    </ul>
                                    <h4>Contribution Date and Payment Method</h4>
                                    <ul>
                                        <li>Payment for RSP can be made using the FPX e-mandate from your bank account.</li>
                                        <li>Below are the procedures to ALL RSP Plans</li>
                                    </ul>
                                    <h4>RSP BY e-Mandate via Financial Process Exchange (FPX)</h4>
                                    <ul>
                                        <li>For application via FPX, you are not required to submit any form. An email will be sent to notify you once the FPX has been approved.</li>
                                        <li>Clients are to ensure that there are sufficient balance in your bank account for successful creation of RSP subscription units.</li>
                                        <li>For investors who wish to change the Direct Debit Authorisation (“DDA”) limit, they are required to resubmit the My Clear DDA form to Apex Investment Services Bhd.</li>
                                        <li>For investors who wish to terminate the FPX e-mandate, they are required to submit the My Clear DDA form to Apex Investment Services Bhd.</li>
                                        <li>Change of amount request or termination request received from the 15th to 30th  of the month will only be effective from the next RSP cycle, as existing approved RSP-FPX e-mandate would have been processed for deduction on the 1st of the current month.</li>
                                        <li>RSP will be terminated automatically after three consecutive failed RSP deductions. An email will be sent to notify the investors of the voided RSP.</li>
                                    </ul>
                                    <h4>Transaction Date</h4>
                                    <ul>
                                        <li>The transaction date for the RSP subscription contracts will be the 1st of every month if it is a business day and fund-trading day. Otherwise, the transaction date will be the next fund-trading business day.</li>
                                    </ul>
                                    <h4>Redemption</h4>
                                    <ul>
                                        <li>RSP subscription contracts are treated as normal lump sum subscription contracts. There are no lock-in periods and investors may redeem anytime once the transactions have been completed. Holding and redemption of the funds will be subjected to the terms and conditions of the Master Prospectuses.</li>
                                        <li>RSP will be terminated after full redemption of RSP fund.</li>
                                    </ul>

                                    <div class="f1-buttons">
                                        <button type="button" id="btnIntroduction" class="btn btn-next" onclick="Proceed();">Next</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="fieldsetfundSelectionAndAmount">




                            <div id="accSummaryDiv">
                                <div id="fundTableDiv">
                                    <div class="row">
                                        <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                                            <table class="table table-cell-pad-5 table-font-size-13 table-condensed table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Fund</th>
                                                        <th class="text-right">Total<br />
                                                            Units</th>
                                                        <th class="text-right">NAV Price<br />
                                                            MYR</th>
                                                        <th class="text-right">Market Value<br />
                                                            MYR</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbodyAccountFunds" runat="server" clientidmode="Static">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">

                                        <div class="row mb-10">
                                            <div class="col-md-3" style="width: 40%;">
                                                <label>SAT Recommended Fund:</label>
                                            </div>
                                            <div class="col-md-3" style="width: 30%;">
                                                <asp:RadioButton ID="rdnRecommended" runat="server" GroupName="FundSelection" Text="Default" ClientIDMode="Static" />
                                            </div>
                                            <div class="col-md-4" style="width: 30%;">
                                                <asp:RadioButton ID="rdnNonRecommended" runat="server" GroupName="FundSelection" Text="Non-Default" ClientIDMode="Static" />
                                            </div>
                                        </div>
                                        <p class="fs-12 text-justify mb-5" id="warningLabel" style="color: red; line-height: 13px; font-size: 11px;">* Please be informed that you are investing in the product without a recommendation and certain products are only suitable for investors who have met the product issuers' minimum qualifying criteria.</p>


                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Select Fund:</label>

                                                        <asp:DropDownList ID="ddlFundsList" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                        </asp:DropDownList>
                                                        <asp:HiddenField ID="txtFundId" runat="server" ClientIDMode="Static" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>RSP Amount (MYR) :</label>
                                                        <asp:TextBox ID="txtAmount" runat="server" ClientIDMode="Static" placeholder="Enter amount" CssClass="form-control" onkeypress="CheckNumeric(event);" ReadOnly="true"></asp:TextBox>
                                                        <asp:HiddenField ID="txtIcNo" runat="server" />
                                                        <asp:HiddenField ID="txtTotalAmount" ClientIDMode="Static" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-10">
                                            <div class="form-group row">
                                                <div class="col-md-3">
                                                    <a href="#" class="btn btn-infos1 btn-sm btn-block mb-20" data-toggle="modal" data-target="#orderHistory">Order History</a>
                                                </div>
                                                <div class="col-md-6">
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="javascript:;" id="btnAddFund" class="btn btn-infos1 btn-sm btn-block mb-20">Add</a>
                                                </div>
                                            </div>

                                            <div class="row hide" id="fundNote">
                                                <div class="col-md-12">
                                                    <p class="fs-13"><strong>Note: </strong><span>This Regular Saving Authorization is a Monthly Subscription and the Min Subscription amount for <span class="fundName"></span> is MYR<span id="fundMinInvAmt">100</span>.</span></p>
                                                </div>
                                            </div>

                                            <div id="divFundDetails" class="hide">
                                                <div class="loadingDiv">
                                                    <div class="typing_loader"></div>
                                                    <div class="text-center">Loading...</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Fund Name</td>
                                                                    <td class="fundName">Fund Name</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Fund - Risk Profile</td>
                                                                    <td class="riskCriteria">Risk Profile</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Sales Charge</td>
                                                                    <td class="salesCharge">Sales Charge</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Master Prospectus</td>
                                                                    <td class="masterProspectus">Master Prospectus</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Product Highlights Sheet</td>
                                                                    <td class="productHighlightsSheet">Master Prospectus</td>
                                                                </tr>
                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <td colspan="2">Note: <strong>You must READ the fund prospectus and Product Highlights Sheet* before you proceed to the next step.</strong>
                                                                    </td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="mt-15 cumulative hide" id="fundperformancetable">
                                                <h6 class="mb-2" style="color: #3392b1;">FUND PERFORMANCE</h6>
                                                <div class="table-responsive">
                                                    <table class="table chart-table table-bordered myDataTable fund-table fundPerformanceCumulativeTable">
                                                        <thead>
                                                            <tr class="fs-13">
                                                                <th style="width: 35%">Period <small class="pull-right">(Dividend date: <span id="dividendDateValue" runat="server"></span>)</small></th>
                                                                <th>1 week </th>
                                                                <th>1 month </th>
                                                                <th>3 month</th>
                                                                <th>6 month</th>
                                                                <th>1 year</th>
                                                                <th>2 year</th>
                                                                <th>3 year</th>
                                                                <th>5 year</th>
                                                                <th>10 year</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr class="fs-13">
                                                                <td>BID TO BID Returns (%) - MYR </td>
                                                                <td><span id="weekCValue" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="monthCValue" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="month3CValue" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="month6CValue" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="yearCValue" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year2CValue" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year3CValue" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year5CValue" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year10CValue" runat="server" clientidmode="Static"></span></td>
                                                            </tr>
                                                            <tr class="fs-13">
                                                                <td>Average </td>
                                                                <td><span id="weekCValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="monthCValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="month3CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="month6CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="yearCValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year2CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year3CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year5CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                                <td><span id="year10CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr class="fs-12">
                                                                <td colspan="10" style='font-family: "Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif;'>Performance figures are absolute returns based on the price of the fund as at <strong id="fundPricedate" runat="server" clientidmode="Static">WHEN?</strong> <%--(Last updated on <strong id="fundPriceLastUpdatedDate" runat="server" clientidmode="Static">WHEN?</strong>)--%>, on NAV-to-NAV basis with dividends being 'reinvested' on the dividend date.</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="row" id="cart">
                                                <div class="col-md-12">
                                                    <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                        <thead>
                                                            <tr>
                                                                <th>Selected<br />
                                                                    Fund</th>
                                                                <th class="text-right">RSP Amount<br />
                                                                    MYR</th>
                                                                <th class="text-right">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="tbodySelectedFunds" id="tbodySelectedFunds" runat="server" clientidmode="static"></tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <th class="text-right">Total</th>
                                                                <td class="currencyFormatNoMYR tdTotalInvestmentAmount text-right" id="tdTotalInvestmentAmount" runat="server" clientidmode="static"></td>
                                                                <td></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                    <div class="form-group mb-5">
                                                        <asp:CheckBox ID="chkConfirmFundInformation" ClientIDMode="Static" runat="server" CssClass="checkbox-inline text-info text-bold" Text="I confirm that I have read and understood the electronic prospectus and Product Highlights Sheet of" />
                                                    </div>
                                                    <div class="f1-buttons">
                                                        <button type="button" class="btn btn-previous">Previous</button>
                                                        <button type="button" class="btn btn-next">Next</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </fieldset>

                        <fieldset class="fieldsetConfirmation">
                            <div class="row">
                                <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13" style="margin-bottom: 10px !important;">
                                                <thead>
                                                    <tr>
                                                        <th>Selected<br />
                                                            Fund</th>
                                                        <th class="text-right">RSP Amount
                                                            <br />
                                                            MYR</th>

                                                    </tr>
                                                </thead>
                                                <tbody class="tbodySelectedFunds" id="tbodySelectedFunds1" runat="server" clientidmode="static"></tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-right">Total</th>
                                                        <td class="unitFormatNoSymbolMarketValue tdTotalInvestmentAmount text-right" id="tdTotalInvestmentAmount1" runat="server" clientidmode="static"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <div class="mb-10">
                                                <p class="fs-12 mb-6 text-justify" style="font-size: 11px; line-height: 8px;">You must have Internet Banking Account in order to make transaction using FPX.</p>
                                                <p class="fs-12 mb-6 text-justify" style="font-size: 11px; line-height: 8px;">Please ensure that your browser’s pop up blocker has been disable to avoid any interruption during perform the transaction. </p>
                                                <p class="fs-12 mb-6 text-justify" style="font-size: 11px; line-height: 8px;">Do not close browser or refresh the page until you receive response.</p>
                                                <%--<a class="text-info" href="#" data-toggle="modal" data-target="#righttocancel">Important: Click here for information about your right to cancel.</a>--%>
                                            </div>
                                            <%--<div class="row">
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlPaymentMethodsList" runat="server" CssClass="form-control" ClientIDMode="Static">
                                                        <asp:ListItem Value="" Text="Select Payment Method"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="FPX Payment"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:FileUpload ID="fuPaymentMethod" runat="server" ClientIDMode="Static" />
                                                </div>
                                            </div>--%>
                                            <div class="f1-buttons">
                                                <button type="button" class="btn btn-previous">Previous</button>
                                                <asp:Button ID="btnSubmit" runat="server" ClientIDMode="Static" CssClass="btn btn-next btn-infos1" Text="Proceed" OnClick="btnSubmit_Click" EnableViewState="false" CausesValidation="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <asp:HiddenField ID="hdnAccountPlan" runat="server" ClientIDMode="Static" />

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">

    <button id="editFundModal" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="display: none">Open Modal</button>

    <div class="modal fade" id="righttocancel">
        <div class="modal-dialog sett two">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h4>Rights to cancel</h4>
                </div>
                <div class="modal-body">
                    <p>Rights to cancel</p>
                    <%-- <p>Investing in unit trust fund with borrowed money is more risky than investing with your own savings.</p>
                    <p><b>You should access if loan financing is suitable for you</b> in light of your objectives, attitude to risk and financial circumstances. You should be aware of risks, which would include the following:</p>
                    <ul>
                        <li>The higher the margin of financing (that is, the amount of money you borrow for every ringgit of your own money which you put in as deposit or down payment), the greater the loss or gain on your investment;</li>
                        <li>You should access whether you have the ability to service the repayments on the proposal loan. If your loan is a variable rate loan, and if interest rates rise, your total repayment amount will be increased;</li>
                        <li>If unit prices fall beyond a certain level, you may be asked to provide additional acceptable collateral (where units are used as collateral) or pay additional amounts on top of your normal installments. If you fail to comply within the time prescribed, your units may be sold towards the settlement of your loan;</li>
                        <li>Returns on unit trusts are not guaranteed and maynot be earned evenly over time. this means that there may be some years where returns are high and other years where losses are experienced. Whether you eventually realise a gain or loss may be affected by the timing of the sale of your units. The value of units may fall just when you want your money back even though the investment may have done well in the past.</li>
                    </ul>
                    <p><b>This brief statement cannot disclose all the risks and other aspects of loan financing. You should therefore careful study the terms and conditions before you decide to take the loan. If you are in doubt in respect of any aspect of this risk disclosure statement or the terms of the loan financing, you should consult the institution offering loan.</b></p>--%>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sett-cancel" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Amount</h4>
                </div>
                <div class="modal-body">
                    <input type="text" id="FundAmount" name="FundAmount" class="form-control" placeholder="Enter your amount here." />
                    <input type="hidden" id="txtFundMinAmount" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="FundUpdate">Update</button>
                    <button type="button" class="btn btn-default" id="FundUpdateClose" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <%--Order History--%>
    <div class="modal fade" id="orderHistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

        <div class="modal-dialog sett two">

            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h4>RSP History</h4>
                </div>
                <div class="modal-body" style="height: calc(100vh - 250px); overflow-y: scroll;">
                    <div class="col-md-12">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <table class="table table-cell-pad-5 table-font-size-13 table-condensed table-bordered OrderHistoryTable">
                                    <thead id="theadUserOrders" runat="server">
                                        <tr>
                                            <th style="width: 50px"></th>
                                            <th style="width: 50px">S. No</th>
                                            <th>Account No</th>
                                            <th>Order No</th>
                                            <th>Order Type</th>
                                            <th>Order Date</th>
                                            <th>Status</th>
                                            <th class="text-right">Investment Amount(MYR)</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyUserOrders" runat="server" clientidmode="Static">
                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-infos1" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <%--Order History End--%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/4.1.0/autoNumeric.js"></script>
    <script type="text/javascript">
        
        (function ($) {
            $.fn.inputFilter = function (inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
            };
        }(jQuery));

        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });

        function ConvertToCurrency(num) {
                return num.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            }
        $(document).ready(function () {
            $('#txtAmount').inputFilter(function (value) {
                return /^(\d{0,10})(\.(\d{0,2}))?$/.test(value);
            });
            $('#FundAmount').inputFilter(function (value) {
                return /^(\d{0,10})(\.(\d{0,2}))?$/.test(value);
                // return /^\d*(\.)?(\d{0,4})$/.test(value);
            });

            //document.getElementById('fuPaymentMethod').style.visibility = "hidden";
            $('#btnAcceptBuy').click(function () {
                if ($('#chkBuyProceed').is(':checked')) {
                    $('#btnDismissPopupBuy').click();
                }
                else {
                    ShowCustomMessage('Alert', 'Please Read, Understand and Agree to proceed.', '');
                }
            });
            $('.show-extended-row').click(function () {
                if ($(this).find('i').hasClass('fa-plus')) {
                    $('.show-extended-row i.fa-minus').each(function (idx, obj) {
                        var ele = $(obj).parents('.show-extended-row');
                        var extendedTRAll = $(ele).parents('tr').next();
                        var extendedRowAll = $(ele).parents('tr').next().find('.extended-row');
                        $(extendedRowAll).slideUp(500, function () {
                            $(extendedTRAll).addClass('hide');
                        });
                        $(ele).find('i').addClass('fa-plus');
                        $(ele).find('i').removeClass('fa-minus');
                    });
                }
                var extendedTR = $(this).parents('tr').next();
                var extendedRow = $(this).parents('tr').next().find('.extended-row');
                if ($(this).find('i').hasClass('fa-plus')) {
                    $(extendedTR).removeClass('hide');
                    $(extendedRow).slideDown();
                    $(this).find('i').removeClass('fa-plus');
                    $(this).find('i').addClass('fa-minus');
                }
                else {
                    $(extendedRow).slideUp(500, function () {
                        $(extendedTR).addClass('hide');
                    });
                    $(this).find('i').removeClass('fa-minus');
                    $(this).find('i').addClass('fa-plus');
                }
            });
            $('#txtUnits').inputFilter(function (value) {
                return /^\d*(\.)?(\d{0,2})$/.test(value);
            });
            $('#FundAmount').inputFilter(function (value) {
                return /^\d*(\.)?(\d{0,2})$/.test(value);
            });
        });
        function Proceed() {
            $('#userAccount').removeClass('hide');
            $('#userAccountTitle').removeClass('hide');

        }

        function OpenCommonTermsPopup() {
            if ($('#ddlUserAccountId').val() != '') {
                setTimeout(function () {
                    $('#btnIntroduction').click();
                }, 100);
                $('#accSummaryDiv, .selectedAccountDetails').removeClass('hide');
                //$('#commonTermsPopup').modal({
                //    keyboard: false,
                //    backdrop: "static",
                //});
            }
            else {
                $('#accSummaryDiv, .selectedAccountDetails').addClass('hide');
            }
        }
        OpenCommonTermsPopup();
        function openPopup() {
            $('#riskPopup').modal({
                keyboard: false,
                backdrop: "static",
            });
        }
        var isCurFormat;
        function FormatAllCurrency() {
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ' });
            });
            $('.currencyFormatNoSymbol').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: '' });
            });
        }
        FormatAllCurrency();
        var isUniFormat;
        var isUniFormatNoSymbol;
        function FormatAllUnit() {
            $('.unitFormat').each(function () {
                isUniFormat = new AutoNumeric(this, { currencySymbol: ' UNITS', currencySymbolPlacement: 's', decimalPlaces: '4' });
            });
            $('.unitFormatNoSymbol').each(function () {
                isUniFormatNoSymbol = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '4' });
            });
            $('.unitFormatNoSymbolMarketValue').each(function () {
                isUniFormatNoSymbol = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '2' });
            });
        }
        FormatAllUnit();
        function scroll_to_class(element_class, removed_height) {
            var scroll_to = $(element_class).offset().top - removed_height;
            if ($(window).scrollTop() != scroll_to) {
                $('html, body').stop().animate({ scrollTop: scroll_to }, 0);
            }
        }
        function bar_progress(progress_line_object, direction) {
            var number_of_steps = progress_line_object.data('number-of-steps');
            var now_value = progress_line_object.data('now-value');
            var new_value = 0;
            if (direction == 'right') {
                new_value = now_value + (100 / number_of_steps);
            }
            else if (direction == 'left') {
                new_value = now_value - (100 / number_of_steps);
            }
            progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
        }
        /* Form */
        $('.f1 fieldset:first').fadeIn('slow');
        $('.f1 input[type="text"], .f1 input[type="password"], .f1 input[type="checkbox"], .f1 textarea').on('focus', function () {
            $(this).removeClass('input-error');
        });
        // next step
        //$('#btnSubmit').on('click', function () {
        //    var payment = $('#ddlPaymentMethodsList').val();
        //    if (payment == "Select Payment Method") {
        //        ShowCustomMessage('Alert', 'Please select payment method. Thank you.', '');
        //    }
        //});
        $('.f1 .btn-next').on('click', function (e) {
            $('#errorMessage').html('');
            $('#successMessage').html('');
            var parent_fieldset = $(this).parents('fieldset');
            var next_step = true;
            // navigation steps / progress steps
            var current_active_step = $(this).parents('.f1').find('.f1-step.active');
            var progress_line = $(this).parents('.f1').find('.f1-progress-line');
            // fields validation
            if (parent_fieldset.hasClass('fieldsetfundSelectionAndAmount')) {
                if (!$('#chkConfirmFundInformation').is(":checked")) {
                    ShowCustomMessage('Alert', 'Please read, understand & agree to proceed. Thank you.', '');
                    next_step = false;
                }
                //else
                //    if (document.getElementById("ddlBank").value == "") {
                //        ShowCustomMessage('Alert', 'No bank is selected. Please proceed to add bank details', 'UserSettings.aspx');
                //        next_step = false;
                //    }
                //    if (CartItems == undefined) {
                //        ShowCustomMessage('Alert', 'Please add fund to proceed. Thank you.', '');
                //        next_step = false;
                //    }
                //    else {
                //        if (CartItems.length == 0) {
                //            ShowCustomMessage('Alert', 'Please add fund to proceed. Thank you.', '');
                //            next_step = false;
                //        }
                //    }
            }
            else
                parent_fieldset.find('input[type="text"], input[type="password"], input[type="checkbox"], textarea, select').each(function () {
                    if ($(this).val() == "") {
                        $(this).addClass('input-error');
                        next_step = false;
                    }
                    else {
                        if ($(this).is(':checkbox')) {
                            if (!$(this).attr('checked')) {
                                $(this).parents('.form-group').addClass('input-error');
                                next_step = false;
                            }
                            else {
                                $(this).parents('.form-group').removeClass('input-error');
                            }
                        }
                        else {
                            $(this).removeClass('input-error');
                        }
                    }
                });
            // fields validation
            if (next_step) {
                parent_fieldset.fadeOut(400, function () {
                    // change icons
                    current_active_step.removeClass('active').addClass('activated').next().addClass('active');
                    // progress bar
                    bar_progress(progress_line, 'right');
                    // show next step
                    $(this).next().fadeIn();
                    // scroll window to beginning of the form
                    scroll_to_class($('.f1'), 20);
                });
            }
            else {
                e.preventDefault();
            }
        });
        // previous step
        $('.f1 .btn-previous').on('click', function () {
            // navigation steps / progress steps
            $('#errorMessage').html('');
            $('#successMessage').html('');
            var current_active_step = $(this).parents('.f1').find('.f1-step.active');
            var progress_line = $(this).parents('.f1').find('.f1-progress-line');
            $(this).parents('fieldset').fadeOut(400, function () {
                // change icons
                current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
                // progress bar
                bar_progress(progress_line, 'left');
                // show previous step
                $(this).prev().fadeIn();
                // scroll window to beginning of the form
                scroll_to_class($('.f1'), 20);
            });
        });
        // submit
        $('.f1').on('submit', function (e) {
            // fields validation
            $(this).find('input[type="text"], input[type="password"], input[type="checkbox"], textarea, select').each(function () {
                if ($(this).val() == "") {
                    e.preventDefault();
                    $(this).addClass('input-error');
                }
                else {
                    $(this).removeClass('input-error');
                    setTimeout(function () {
                        $('.loader-bg').fadeIn();
                    }, 500);
                }
            });
            // fields validation
        });

        function BindCart() {
            $('.tdTotalInvestmentAmount').html(TotalAmount);
            $('#txtTotalAmount').val(TotalAmount);
            $('#tbodySelectedFunds, #tbodySelectedFunds1').html('');

            //if (document.getElementById('.fundName').text != null) {
            //    var fundname = document.getElementById('.fundName').text;
            //}
            var tbodySelectedFundsHtml = "";
            $.each(CartItems, function (idx, obj) {
                tbodySelectedFundsHtml += "<tr>";
                tbodySelectedFundsHtml += "<td>" + obj.UtmcFundInformation.FundCode + " - " + obj.UtmcFundInformation.FundName + "</td>"
                //tbodySelectedFundsHtml += "<td>" + obj.UtmcFundInformation.FundCode + " - " + obj.UtmcFundInformation.FundName + "</td>"
                tbodySelectedFundsHtml += "<td class='currencyFormatNoMYR text-right'>" + obj.Amount + "</td>"
                tbodySelectedFundsHtml += "<td class='text-right'><div class='btn-group' role='group'><a href='javascript:;' fund-id=" + obj.UtmcFundInformation.Id + " data-fund-min-amount='" + obj.UtmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].MinRspInvestmentInitialRm + "' data-fund-amount=" + obj.Amount + " data-id=" + obj.Id + " class='btn btn-sm btn-default editFund' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a><a href='javascript:;' data-id=" + obj.Id + " class='btn btn-sm btn-default removeFund' data-toggle='tooltip' title='Remove'><i class='fa fa-trash-o' aria-hidden='true'></i></a></div></td>";
            });
            $('#tbodySelectedFunds').html(tbodySelectedFundsHtml);
            tbodySelectedFundsHtml = "";
            $.each(CartItems, function (idx, obj) {
                tbodySelectedFundsHtml += "<tr>";
                tbodySelectedFundsHtml += "<td>" + obj.UtmcFundInformation.FundCode + " - " + obj.UtmcFundInformation.FundName + "</td>"
                tbodySelectedFundsHtml += "<td class='currencyFormatNoMYR text-right'>" + obj.Amount + "</td>"
                tbodySelectedFundsHtml += "</tr>";
            });
            $('#tbodySelectedFunds1').html(tbodySelectedFundsHtml);
            ChangeTC();
            FormatAllCurrency();
            FormatAllUnit();

            showCart();
        }

        function showFundDetail() {
            if ($('#tdTotalInvestmentAmount').attr('value') == 0) {//CartItems == undefined) {
                $('#divFundDetails').addClass('hide');
                $('#fundTableDiv').addClass('hide');
            }
            else {
                $('#divFundDetails').removeClass('hide');
            }
            $('.loadingDiv').addClass('hide');

        }

        var Term;
        function ChangeTC() {
            Term = "I confirm that I have read and understood the electronic prospectus and Product Highlights Sheet of";
            $.each(CartItems, function (idx, obj) {
                Term += " " + obj.UtmcFundInformation.FundName + ',';
            });
            Term = Term.substring(0, Term.length - 1);
            document.getElementById("chkConfirmFundInformation").nextSibling.innerHTML = Term;
        }

        var CartItems;
        var TotalAmount;
        $(document).ready(function () {
            $.ajax({
                url: "RegularSavingsPlan.aspx/BindFundsFromCartDB",
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: { userAccountNo: $('#ddlUserAccountId').val() },
                success: function (data) {
                    CartItems = data.d.CartItems;
                    TotalAmount = data.d.TotalAmount;
                    BindCart();
                }
            });

            $('#ddlPaymentMethodsList').change(function () {
                var selectedIndex = document.getElementById('ddlPaymentMethodsList').selectedIndex;
                if (selectedIndex == 0 || selectedIndex == 1)
                    document.getElementById('fuPaymentMethod').style.visibility = "hidden";
                else
                    document.getElementById('fuPaymentMethod').style.visibility = "visible";

            });

            //$('#txtAmount').keyup(function (event) {

            //    // skip for arrow keys
            //    if (event.which >= 37 && event.which <= 40) return;

            //    // format number
            //    $(this).val(function (index, value) {
            //        return value
            //            .replace(/\D/g, "")
            //            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            //            ;
            //    });
            //});


            $('#btnAddFund').click(function () {

                var fundId = $('#ddlFundsList').val();
                var amount2 = $('#txtAmount').val();
                //var distributionID = $('#ddlDistributionInstruction').val();
                //var consultantID = $('#ddlConsultant').val();
                if ($('#ddlFundsList').val() != 0) {
                    var amount = amount2.replace(/,/g, "");

                    if (CartItems != undefined) {
                        $('#successMessage').html('');
                        var items = $.grep(CartItems, function (obj, idx) {
                            return obj.UtmcFundInformation.Id == parseInt(fundId);
                        });
                        var isModify = 0;

                    }
                    if (amount == "") {
                        $('#successMessage').html('');
                        ShowCustomMessage('Alert', 'Amount is required.', '');

                        return false;
                    }
                    //if (distributionID == "0") {
                    //$('#errorMessage').html('Please choose a Distribution Instruction.');
                    //}
                    //else
                    {
                        $('#errorMessage').html('');
                        $('#successMessage').html('');
                        var fundMinInvestCash = minRspInvestmentInitialRm;
                        var fundMinInvestEPF = minRspInvestmentInitialRm;
                        var accountPlan = $('#hdnAccountPlan').val();
                        if (accountPlan == "EPF") {
                            if (parseFloat(amount) < parseFloat(fundMinInvestEPF)) {

                                ShowCustomMessage('Alert', 'Min Investment Amount is MYR ' + ConvertToCurrency(fundMinInvestEPF), '');
                                return false;
                            }
                        }
                        else if (accountPlan == "CASH") {
                            if (parseFloat(amount) < parseFloat(fundMinInvestCash)) {
                                ShowCustomMessage('Alert', 'Min Investment Amount is MYR ' + ConvertToCurrency(fundMinInvestCash), '');

                                return false;
                            }
                        }
                        $('#errorMessage').html('');
                        $('#successMessage').html('');
                        $.ajax({
                            url: "RegularSavingsPlan.aspx/AddFund",
                            contentType: 'application/json; charset=utf-8',
                            type: "GET",
                            dataType: "JSON",
                            data: { fundId: fundId, amount: amount, userAccountNo: $('#ddlUserAccountId').val() },
                            success: function (data) {
                                CartItems = data.d.CartItems;
                                TotalAmount = data.d.TotalAmount;
                                BindCart();
                                if (isModify == 1) {


                                    InsertIntoOrderCard(fundId, amount, 0, "");

                                    $('#txtAmount').val('');
                                    $('#ddlFundsList').val(0);
                                    $('#divFundDetails').addClass('hide');
                                }
                                else {


                                    InsertIntoOrderCard(fundId, amount, 0, "");

                                    $('#txtAmount').val('');
                                    $('#ddlFundsList').val(0);
                                    $('#divFundDetails').addClass('hide');

                                }
                            }
                        });

                    }
                } else {
                    $('#successMessage').html('');
                    ShowCustomMessage('Alert', 'Fund is required.', '');
                    $('#errorMessage').html('Fund is required.');
                    return false;
                }

            });

            function InsertIntoOrderCard(Id, Amount, distributionID, consultantID) {
                $.ajax({
                    type: "GET",
                    url: "RegularSavingsPlan.aspx/InsertOderCart",
                    data: { Id: Id, Amount: Amount, distributionID: distributionID, consultantID: consultantID, userAccountNo: $('#ddlUserAccountId').val() },
                    dataType: "JSON",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        //$('#hdnTotalQuantity').val(data.d);
                        //FundDetails(Id);
                        GetFundPerformance(Id);
                        GetHistoryCart();
                    }
                });
            }
            $('#FundUpdate').click(function () {
                var id = newFundEditId;
                var amount2 = $("#FundAmount").val();
                var minCash = $("#txtFundMinAmount").val();

                var amount = amount2.replace(/,/g, "");

                if (amount == "") {
                    ShowCustomMessage('Alert', 'Please enter amount!', '');
                    return false;
                }
                if (parseFloat(amount) < parseFloat(minCash)) {
                    ShowCustomMessage('Alert', 'Min Investment Amount is MYR <span class="currencyFormat2dp"> ' + ConvertToCurrency(parseFloat(minCash)) + '</span>', '');
                    return false;
                }
                $.ajax({
                    url: "RegularSavingsPlan.aspx/editFund",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { Id: id, Amount: amount, userAccountNo: $('#ddlUserAccountId').val() },
                    success: function (data) {
                        CartItems = data.d.CartItems;
                        TotalAmount = data.d.TotalAmount;
                        BindCart();
                        $('#errorMessage').html('');

                        document.getElementById("FundUpdateClose").click();
                    }
                });
            });

            var newFundEditId;

            $('#tbodySelectedFunds').on('click', '.editFund', function () {
                $('#errorMessage').html('');
                $('#successMessage').html('');
                var id = $(this).attr('data-id');
                var value = $(this).parents('td').parent('tr').find('td:nth-child(2)').attr('value');
                var minfundAmt = $(this).attr('data-fund-min-amount');
                //var value = $(this).parent('td').prev().attr('value');
                newFundEditId = id;
                $('#FundAmount').val(parseFloat(value).toFixed(2));
                $('#txtFundMinAmount').val(minfundAmt);
                document.getElementById("editFundModal").click();

                //$.ajax({
                //    url: "BuyFunds.aspx/RemoveFund",
                //    contentType: 'application/json; charset=utf-8',
                //    type: "GET",
                //    dataType: "JSON",
                //    data: { Id: id },
                //    success: function (data) {
                //        CartItems = data.d.CartItems;
                //        TotalAmount = data.d.TotalAmount;
                //        BindCart();
                //        $('#successMessage').html('Successfully removed.');
                //    }
                //});
            });
            $('#tbodySelectedFunds').on('click', '.removeFund', function () {
                $('#errorMessage').html('');
                $('#successMessage').html('');
                var id = $(this).attr('data-id');
                $.ajax({
                    url: "RegularSavingsPlan.aspx/RemoveFund",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { Id: id, userAccountNo: $('#ddlUserAccountId').val() },
                    success: function (data) {
                        CartItems = data.d.CartItems;
                        TotalAmount = data.d.TotalAmount;
                        BindCart();
                        $('#successMessage').html('Successfully removed.');

                    }
                });
            });
            showCart();

        });

        function showCart() {
            if ($('#ddlUserAccountId').val() != '') {

                if ($('#tdTotalInvestmentAmount').attr('value') == 0) {//CartItems == undefined) {
                    $('#cart').addClass('hide');

                }
                else {

                    $('#cart').removeClass('hide');
                }

            }
        }
        var fundDetails;
        var minRspInvestmentInitialRm = "0";
        var splits = '';
        function FundDetails(Id) {
            $('.loadingDiv').removeClass('hide');
            $('#divFundDetails').removeClass('hide');
            //var hdnTotalQuantity = parseFloat($('#hdnTotalQuantity').val());

            $.ajax({
                url: "RegularSavingsPlan.aspx/GetFundDetails",
                async: true,
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: { 'Id': Id, userAccountNo: $('#ddlUserAccountId').val() },
                success: function (data) {

                    splits = data.d.splits;
                    var json = data.d.UtmcFundInformation;
                    fundDetails = json;
                    $('.fundName').html(json.FundName);
                    $('.salesCharge').html(json.UtmcFundInformationIdUtmcFundCharges[0].InitialSalesChargesPercent + "%");

                    //$('.minimumInitialInvestement').html(json.UtmcFundInformationIdUtmcFundDetails[0].MinRspInvestmentInitialRm);
                    //$('.minimumInitialInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinRspInvestmentInitialRm + "</span>/<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentEpf + "</span>");
                    //$('#hdnMinRspInvestmentInitialRmEPF').val(json.UtmcFundInformationIdUtmcFundDetails[0].MinRspInvestmentInitialRm + "," + json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentEpf);
                    minRspInvestmentInitialRm = json.UtmcFundInformationIdUtmcFundDetails[0].MinRspInvestmentInitialRm;
                    $('#fundNote').removeClass('hide');
                    $('#fundMinInvAmt').html(minRspInvestmentInitialRm);
                    //$('.minimumSubsequentInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash + "</span>/<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf + "</span>");
                    if ($('#ddlFundsList').val() == '0') {
                        $('#ddlFundsList').val(Id);

                    }
                    $('.riskCriteria').html($('#ddlFundsList option:selected').attr('data-risk').trim());
                    //$('.unitholding').html("<span class='unitFormat'>" + hdnTotalQuantity.toFixed(4) + "</span>");
                    //console.log(hdnTotalQuantity);
                    if ($('#ddlFundsList option:selected').attr('data-risk').trim()) {

                        openPopup();
                    }

                    //$('#hdnMinSubsequentInvestmentCashEPF').val(json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash + "," + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf);
                    var masterProspectus = $.grep(
                        json.UtmcFundInformationIdUtmcFundFiles,
                        function (obj) {
                            return obj.UtmcFundFileTypesDefId == 1
                        })[0];

                    if (masterProspectus != null) {

                        if (masterProspectus.Url != null) {
                            var a = $('<a />').attr('href', masterProspectus.Url).attr('target', '_blank').html('Click here to View');
                            $('.masterProspectus').html(a);
                        }
                        else {
                            $('.masterProspectus').html('-');
                        }
                    }
                    else {
                        $('.masterProspectus').html('-');
                    }

                    var productHighlightsSheet = $.grep(
                        json.UtmcFundInformationIdUtmcFundFiles,
                        function (obj) {
                            return obj.UtmcFundFileTypesDefId == 3
                        })[0];
                    if (productHighlightsSheet != null) {
                        if (productHighlightsSheet.Url != null) {
                            var a = $('<a />').attr('href', productHighlightsSheet.Url).attr('target', '_blank').html('Click here to View');
                            $('.productHighlightsSheet').html(a);
                        }
                        else {
                            $('.productHighlightsSheet').html('-');
                        }
                    }
                    else {
                        $('.productHighlightsSheet').html('-');
                    }

                    $('#chkConfirmFundInformation').prop('checked',false);

                    FormatAllCurrency();

                    $('#txtAmount').attr('disabled', false);
                    $('#btnAddFund').attr('disabled', false);

                }
            });
        }

        $('#rdnRecommended').click(function () {
            if ($(this).is(":checked")) {
                GetFundListByCategory(2);
                $('#warningLabel').addClass('hide');
            }
        });
        $('#rdnNonRecommended').click(function () {
            if ($(this).is(":checked")) {
                GetFundListByCategory(3);
                $('#warningLabel').removeClass('hide');
            }
        });

        $('#rdnRecommended').prop('checked',true);
        $('#rdnRecommended').click();

        function GetFundListByCategory(Id) {
            $.ajax({
                type: "GET",
                url: "RegularSavingsPlan.aspx/GetFundList",
                data: { Id: Id, userAccountNo: $('#ddlUserAccountId').val() },
                dataType: "JSON",
                contentType: "application/json; charset=utf-8",
                success: function (data) {



                    //document.getElementById("ddlFundsList").options.length = 0;
                    //var select = document.getElementById('ddlFundsList');
                    //select[select.length] = new Option('Select Fund', '0');

                    if (data.d.Name.length == 0) {
                        if (Id == 2)
                            ShowCustomMessage('Alert', 'There is no recommended funds.', '');
                        else {
                            ShowCustomMessage('Alert', 'There is no non-recommended funds.', '');
                            //$('#rdnRecommended').prop('checked',true);
                            //$('#rdnRecommended').click();
                        }
                    }

                    $('#ddlFundsList option').remove();
                    $('#ddlFundsList').append('<option value="0">Select</option>');
                    for (i = 0; i < data.d.Name.length; i++) {
                        $('#ddlFundsList').append('<option data-risk="' + data.d.Risk[i] + '" value="' + data.d.Id[i] + '">' + data.d.Name[i] + '</option>');
                    }

                    if (data.d.UserSATGroup == "G4") {
                        $('#rdnNonRecommended').prop('disabled', 'disabled');
                    }
                    //$("txtAmount").readonly = true;
                    //$('#hdnTotalQuantity').val(data.d);
                    //FundDetails(Id);
                    //GetFundPerformance(Id);
                }
            });
        }

        function CheckNumeric(e) {

            if (window.event) // IE 
            {
                if ((e.keyCode < 48 || e.keyCode > 57) && e.keyCode != 46) {
                    event.returnValue = false;
                    return false;
                }
            }
            else { // Fire Fox
                if ((e.keyCode < 48 || e.keyCode > 57) && e.keyCode != 46) {
                    event.returnValue = false;
                    return false;
                }
            }
        }

        $('#ddlFundsList').change(function () {
            $('#errorMessage').html('');
            $('#successMessage').html('');
            $('#txtAmount').val('');
            $('#txtAmount').attr('disabled', true);
            $('#btnAddFund').attr('disabled', true);
            if ($(this).val() != "" && $(this).val() != null && $(this).val() != "0") {
                $("#txtAmount").removeAttr('readonly');
                var Id = parseInt($(this).val());

                FundDetailsUnitOwned(Id);
            }
            else {
                $('#divFundDetails').addClass('hide');
                $('#fundTableDiv').addClass('hide');
                $('#fundperformancetable').addClass('hide');
                $('#fundNote').addClass('hide');
            }
            $('#txtFundId').val($(this).val())
        });
        $('#ddlFundsList').change();

        var isCurFormat;
        function FormatAllCurrency() {
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ' });
            });
            $('.currencyFormatNoDecimal').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ', decimalPlaces: '0' });
            });
            $('.currencyFormatNoMYR').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '2' });
            });
            $('.currencyFormatNoSymbol').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '4' });
            });
            $('.unitFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '4' });
            });
        }

        function FundDetailsUnitOwned(Id) {

            $.ajax({
                type: "GET",
                url: "RegularSavingsPlan.aspx/GetTotalUnits",
                data: { Id: Id, userAccountNo: $('#ddlUserAccountId').val() },
                dataType: "JSON",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $('#hdnTotalQuantity').val(data.d.totalUnit);
                    if (data.d.EpfOrOther != "EPF") {

                        if (data.d.instruction == 0) {

                            //document.getElementById("ddlDistributionInstruction").disabled = false;
                            //document.getElementById("ddlDistributionInstruction").value = 0;

                        }
                        else {

                            //document.getElementById("ddlDistributionInstruction").disabled = true;
                            //document.getElementById("ddlDistributionInstruction").value = data.d.instruction;

                        }
                    }
                    else {
                        //document.getElementById("ddlDistributionInstruction").disabled = true;
                        //document.getElementById("ddlDistributionInstruction").value = 1;

                    }

                    FundDetails(Id);
                    GetFundPerformance(Id);
                }
            });
        }

        function GetHistoryCart() {
            $.ajax({
                url: "RegularSavingsPlan.aspx/BindFundsFromCartDB",
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: { userAccountNo: $('#ddlUserAccountId').val() },
                success: function (data) {
                    CartItems = data.d.CartItems;
                    TotalAmount = data.d.TotalAmount;
                    BindCart();
                }
            });
        }
        //$('#FundAmount').keyup(function (event) {

        //    // skip for arrow keys
        //    if (event.which >= 37 && event.which <= 40) return;

        //    // format number
        //    $(this).val(function (index, value) {
        //        return value
        //            .replace(/\D/g, "")
        //            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        //            ;
        //    });
        //});
        function GetFundPerformance(Id) {
            $.ajax({
                type: "GET",
                url: "RegularSavingsPlan.aspx/GetFundPerformance",
                data: { Id: Id },
                dataType: "JSON",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $("#fundperformancetable").removeClass('hide');

                    $('#weekCValue').html(data.d.w1);
                    $('#monthCValue').html(data.d.m1);
                    $('#month3CValue').html(data.d.m3);
                    $('#month6CValue').html(data.d.m6);
                    $('#yearCValue').html(data.d.y1);
                    $('#year2CValue').html(data.d.y2);
                    $('#year3CValue').html(data.d.y3);
                    $('#year5CValue').html(data.d.y5);
                    $('#year10CValue').html(data.d.y10);

                    $('#weekCValueAvg').html(data.d.w1a);
                    $('#monthCValueAvg').html(data.d.m1a);
                    $('#month3CValueAvg').html(data.d.m3a);
                    $('#month6CValueAvg').html(data.d.m6a);
                    $('#yearCValueAvg').html(data.d.y1a);
                    $('#year2CValueAvg').html(data.d.y2a);
                    $('#year3CValueAvg').html(data.d.y3a);
                    $('#year5CValueAvg').html(data.d.y5a);
                    $('#year10CValueAvg').html(data.d.y10a);

                    $('#fundPricedate').html(data.d.date);
                    $('#fundPriceLastUpdatedDate').html(data.d.date);

                    $('.loadingDiv').addClass('hide');
                }
            });
        }

    </script>
</asp:Content>
