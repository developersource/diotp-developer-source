﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="Alerts.aspx.cs" Inherits="DiOTP.WebApp.Alerts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css" id="style-resource-1">
    <style>
        #accountSelectedDropdown{
            display: none;
        }
        .checkbox-inline label {
            font-weight: 500;
            font-family: inherit !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper sat-form">

            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/Portfolio.aspx">eApexIs</a></li>
                    <li><a href="/Settings.aspx">Account Settings</a></li>
                    <li class="active">Alerts</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Alerts</h3>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <p>Master Account Number: </p>
                </div>
                <div class="col-md-3">
                    <asp:DropDownList CssClass="form-control" ID="ddlUserAccountId" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUserAccountId_SelectedIndexChanged"></asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row" id="notificationSettings" runat="server" clientidmode="static">
                    </div>
                    <div class="hide">
                        <br />
                        <asp:CheckBox ID="chkDistribution" runat="server" Text="Get notification when funds declare distribution." CssClass="checkbox-inline" />
                    </div>
                    <asp:Button ID="btnNotifySave" runat="server" CssClass="btn btn-sett-confirm mt-8" Text="Save Changes" OnClick="btnNotifySave_Click" ClientIDMode="Static" />
                    <asp:Button ID="btnback" CausesValidation="false" runat="server" Text="Back" CssClass="btn btn-sett-confirm mt-8" OnClick="btnback_Click" />
                </div>
            </div>
            <br />
            <div class="row">
                <table class="table table-bordered dt-responsive table-font-size-13 nowrap myTable" id="table-7">
                    <thead id="tblHeader" runat="server">
                        <tr>
                            <th>Fund Name</th>
                            <th>Fund Price Target (MYR)</th>
                            <th>Fund Performance Target (%)</th>
                        </tr>
                    </thead>

                    <tbody id="usersTbody" runat="server"></tbody>
                </table>
            </div>

        </div>
    </div>
    <asp:HiddenField ID="SettingId" runat="server" ClientIDMode="Static" />
    <asp:Button ID="Delete_Setting" runat="server" ClientIDMode="Static" CssClass="hide" OnClick="Delete_Setting_Click" />
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>


    <script type="text/javascript">
        var $ = jQuery;
        $(document).ready(function () {
            $('#form1').submit(function () {
                var btn = $(this).find("input[type=submit]:focus");
                if ($('#SettingId').val() == "" && $(btn).val() != 'Back') {
                    var isValid = [];
                    var count = 0;
                    $("#notificationSettings .row").each(function () {
                        debugger;
                        var Price = $(this).find('input[type=text]').val();
                        var Fund = $(this).find('select').val();
                        if ((Price != null && Price != '') && ($.isNumeric(Price)) && (parseFloat(Price) > 0) && (Fund != null && Fund != '')) {
                            isValid.push(true);
                        }
                        else {
                            if (Price != null && Price != '') {
                                $(this).find('input[type=text]').css({
                                    "borderBottom": "",
                                });
                                if ((Fund != null && Fund != '')) {
                                    $(this).find('select').css({
                                        "borderBottom": "",
                                    });
                                    if (!($.isNumeric(Price))) {
                                        $(this).find('input[type=text]').css({
                                            "borderBottom": "1px solid red",
                                            //"background": "#FFCECE"
                                        });
                                        ShowCustomMessage('Alert', 'Value must me numeric', '');
                                        //isValid = false;
                                        isValid.push(false);
                                    }
                                    else if (parseFloat(Price) <= 0) {
                                        $(this).find('input[type=text]').css({
                                            "borderBottom": "1px solid red",
                                            //"background": "#FFCECE"
                                        });
                                        ShowCustomMessage('Alert', 'Value cannot be negative or zero', '');
                                        //isValid = false;
                                        isValid.push(false);
                                    }
                                }
                                else {
                                    $(this).find('input[type=text]').css({
                                        "borderBottom": "",
                                    });
                                    $(this).find('select').css({
                                        "borderBottom": "1px solid red",
                                        //"background": "#FFCECE"
                                    });
                                    ShowCustomMessage('Alert', 'Please select a fund', '');
                                    isValid.push(false);
                                    //return false;
                                }
                            }
                            else {
                                $(this).find('input[type=text]').css({
                                    "borderBottom": "1px solid red",
                                });
                                if ((Fund != null && Fund != '')) {
                                    $(this).find('select').css({
                                        "borderBottom": "",
                                    });
                                    $(this).find('input[type=text]').css({
                                        "borderBottom": "1px solid red",
                                    });
                                    ShowCustomMessage('Alert', 'Please enter a value', '');
                                    isValid.push(false);
                                }
                                else {
                                    $(this).find('input[type=text]').css({
                                        "borderBottom": "",
                                    });
                                    $(this).find('select').css({
                                        "borderBottom": "1px solid red",
                                        //"background": "LightBlue"
                                    });
                                    //ShowCustomMessage('Alert', 'Please enter a value and select a fund', '');
                                    //return false;
                                    //isValid = true;
                                    count += 1;
                                }
                            }
                        }

                        if (Price == "" && Fund == "") {
                            $(this).find('select').css({
                                "borderBottom": "",
                            });
                            $(this).find('input[type=text]').css({
                                "borderBottom": "",
                            });
                        }
                    });
                    if (count == 2) {
                        $(this).find('select').css({
                            "borderBottom": "",
                        });
                        $(this).find('input[type=text]').css({
                            "borderBottom": "",
                        });
                        ShowCustomMessage('Alert', 'Please enter a value and select a fund', '');
                        isValid.push(false);
                    }
                    if (isValid.indexOf(false) == -1) {
                        setTimeout(function () {
                            $('.loader-bg').fadeIn();
                        }, 500);
                    }
                    return isValid.indexOf(false) == -1 ? true : false; 
                }
            });

            $('.delete-notify').click(function () {
                var id = $(this).data('id');
                $('#SettingId').val(id);
                $('#Delete_Setting').click();
            });
        });



    </script>
</asp:Content>
