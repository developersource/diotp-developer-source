﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="CoolingOff.aspx.cs" Inherits="DiOTP.WebApp.CoolingOff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">

            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/Portfolio.aspx">eApexIs</a></li>
                    <li class="active">Cooling Off</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Cooling Off</h3>
            </div>

            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="cooling">
                        <h3>For EPF:</h3>
                        <p>“Cooling-Off Period” or “Cooling-Off Right” does not apply to members' investments under the SPA KWSP.</p>
                        <h3>For CASH:</h3>
                        <p>
                            You may exercise cooling off right within six (6) business days from the date of investment and receive a full refund of the investment monies paid to the Manager.  This is applicable to the FIRST time investors (Those who invested for the first time in a fund managed by the manager).  Not withstanding the above, the following first time investors are not entitled to a cooling-off right:
                        </p>
                        <p>(a) corporation or institution.</p>
                        <p>(b) a staff of the manager</p>
                        <p>(c) person registered to deal in unit trusts of the Manager.</p>
                        <p>Please contact our Customer Service at +603 2095 9999 for further information and assistance.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
</asp:Content>
