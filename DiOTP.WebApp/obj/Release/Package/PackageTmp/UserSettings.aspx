﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="UserSettings.aspx.cs" Inherits="DiOTP.WebApp.UserSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">

    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css" rel="stylesheet" />

    <style>
        #accountSelectedDropdown {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">

            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/Portfolio.aspx">eApexIs</a></li>
                    <li class="active">User Settings</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">User Settings</h3>
            </div>


            <div class="row">

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="settings-part">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>Email - <small id="emailBindStatus" runat="server"></small></h5>
                                <p id="emailIdBindedHidden" runat="server"></p>
                                <a href="#" class="settings-bind" data-toggle="modal" data-target="#bindEmail">View</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="settings-part">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                <i class="fa fa-mobile-phone"></i>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>SMS Auth - <small id="mobileNoBindStatus" runat="server"></small></h5>
                                <p id="mobileNoBindedHidden" runat="server">+6011-********</p>
                                <a href="#" class="settings-bind" data-toggle="modal" data-target="#smsAuth">View</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="settings-part">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                <i class="fa fa-google"></i>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>Google Auth - <small id="googleAuthBindStatus" runat="server"></small></h5>
                                <p>For login authentication.</p>
                                <a href="#" class="settings-bind" data-toggle="modal" data-target="#googleAuth">View</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="settings-part">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                <i class="fa fa-lock"></i>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>Login Password <span></span></h5>
                                <p>For login purpose.</p>
                                <a href="#" class="settings-bind" data-toggle="modal" data-target="#loginPassword">Modify</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="transactionPasswordDiv" runat="server">
                    <div class="settings-part">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                <i class="fa fa-lock"></i>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>Transaction Password <span></span></h5>
                                <p>For transaction activities.</p>
                                <a href="#" class="settings-bind" data-toggle="modal" data-target="#transPassword">Modify</a>
                            </div>
                        </div>
                    </div>
                </div>

                <%-- <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="settings-part">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                <i class="fa fa-id-card"></i>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>ID Verification<span></span></h5>
                                <p>Used for security purpose.</p>
                                <a href="#" class="settings-bind" data-toggle="modal" data-target="#idVerify">View</a>
                            </div>
                        </div>
                    </div>
                </div>--%>

                <%--     <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="settings-part">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                <i class="fa fa-credit-card"></i>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>Bank Details - <small id="bankDetailsBindStatus" runat="server"></small></h5>
                                <p>Required for transaction activities.</p>
                                <a href="#" class="settings-bind" data-toggle="modal" data-target="#bankDetails">Modify</a>
                            </div>
                        </div>
                    </div>
                </div>--%>

                <%--<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="settings-part">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                <i class="fa fa-address-card"></i>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>Address</h5>
                                <p>Used for security purpose.</p>
                                <a href="#" class="settings-bind" data-toggle="modal" data-target="#address">Modify</a>
                            </div>
                        </div>
                    </div>
                </div>--%>

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 hide">
                    <div class="settings-part">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                <i class="fa fa-address-card"></i>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>Proof of Address - <small id="addressStatus" runat="server"></small></h5>
                                <p>For address verification.</p>
                                <a href="#" class="settings-bind" data-toggle="modal" data-target="#addressupload">Modify</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 hide">
                    <div class="settings-part">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                <i class="fa fa-address-card"></i>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>Know Your Client - <small id="KYCStatus" runat="server"></small></h5>
                                <p>For identity verification.</p>
                                <a href="#" class="settings-bind" data-toggle="modal" data-target="#KYC">Modify</a>
                            </div>
                        </div>
                    </div>
                </div>

                <%-- <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="settings-part">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                <i class="fa fa-address-card"></i>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>Profile</h5>
                                <p>Used for upload photo.</p>
                                <a href="#" class="settings-bind" data-toggle="modal" data-target="#profile">Modify</a>
                            </div>
                        </div>
                    </div>
                </div>--%>

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="hardcopyDiv" runat="server">
                    <div class="settings-part">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                <i class="fa fa-file-text"></i>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">

                                <h5>Hardcopy Setting - <small id="hardCopyStatus" runat="server"></small></h5>
                                <p>Activate / Deactivate</p>
                                <a href="#" class="settings-bind" data-toggle="modal" data-target="#hardcopySetting">View</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="settings-part">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                <i class="fa fa-address-card"></i>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">

                                <h5>Login History</h5>
                                <p>For login history.</p>
                                <a href="#" class="settings-bind" data-toggle="modal" data-target="#LoginHistory">View</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="bankDiv" runat="server">
                    <div class="settings-part">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">
                                <i class="fa fa-credit-card"></i>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>Bank Details - <small id="bankDetailsBindStatus" runat="server"></small></h5>
                                <p>Required for transaction activities.</p>
                                <a href="#" id="abank" runat="server" class="settings-bind" data-toggle="modal" data-target="#bankDetails">View</a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnSMSExpirationTimeInSeconds" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSMSLockTimeInSeconds" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnImpersonate" runat="server" Value="0" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
    <div class="modal fade" id="bindEmail">
        <div class="modal-dialog sett">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h2 class="text-white mb-0">Bind Email</h2>
                </div>
                <div class="modal-body">
                    <p class="text-center">Email Id: <strong id="emailIdBinded" runat="server"></strong>&nbsp; binded on <strong id="emailIdBindedDate" runat="server"></strong></p>
                    <%--<div class="text-center mb-10">
                        <small><strong>Note:</strong> You can only update email offline. Please visit our office to update.</small>
                    </div>
                    <div class="text-center">Please contact our customer service at +603 2095 9999 for further information and assistance.</div>--%>
                    <div class="text-center mb-10">
                        <small><strong>Note:</strong> If you required to update your e-mail, please <a href="Contact.aspx" style="color: #2b8be9; text-decoration: none">contact us</a> or visit our office if you need further assistance.</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sett-cancel" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="smsAuth">
        <div class="modal-dialog sett">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h2 class="text-white mb-0">SMS Authentication</h2>
                </div>
                <div class="modal-body">
                    <p class="text-center">Mobile Number: <strong id="mobileNoBinded" runat="server"></strong>&nbsp; binded on <strong id="mobileNoBindedDate" runat="server"></strong></p>
                    <%-- <div class="text-center mb-10">
                        <small><strong>Note:</strong> You can only update email offline. Please visit our office to update.</small>
                    </div>
                    <div class="text-center">Please contact our customer service at +603 2095 9999 for further information and assistance.</div>--%>
                    <div class="text-center mb-10">
                        <small><strong>Note:</strong> If you required to update your mobile number, please <a href="Contact.aspx" style="color: #2b8be9; text-decoration: none">contact us</a> or visit our office if you need further assistance.</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sett-cancel" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="googleAuth">
        <div class="modal-dialog sett">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h2 class="text-white mb-0">Google Authentication</h2>
                </div>
                <div class="modal-body">
                    <p class="text-center"><strong>Google Authenticator</strong> binded on <strong id="googleAuthBindedDate" runat="server"></strong></p>
                    <div class="text-center" id="googleAuthRebindButton" runat="server">
                        <asp:Button ID="btnRebindGoogleAuth" runat="server" ClientIDMode="Static" CssClass="btn btn-info" Text="Rebind" OnClientClick="return ShowConfirmationToRebindGoogleAuth()" />
                        <asp:Button ID="btnGoogleAuthRebindConfirm" runat="server" OnClick="btnRebindGoogleAuth_Click" CssClass="hide" ClientIDMode="Static" />
                        <asp:Button ID="btnUnbindGoogleAuth" runat="server" CssClass="btn btn-info" Text="Unbind" OnClientClick="return ShowConfirmationToUnbindGoogleAuth()" />
                        <asp:Button ID="btnGoogleAuthUnbindConfirm" runat="server" OnClick="btnUnbindGoogleAuth_Click" CssClass="hide" ClientIDMode="Static" />
                    </div>
                    <div class="google-authentication" id="googleAuthBindCode" runat="server" clientidmode="static">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <asp:Panel ID="googlePanel" runat="server" DefaultButton="btnCode">
                                    <div class="body-google">
                                        <div class="form-group row" id="divBindGoogleAuthenticator" runat="server">
                                            <div class="col-md-12 text-center" id="divGoogleAuthDisplay" runat="server">
                                                <asp:Image ID="imgQRCode" runat="server" ClientIDMode="Static" />
                                                <br />
                                                Manual Entry Key:
                                                <asp:Label ID="lblManualEntryKey" runat="server" Font-Bold="true" CssClass="fs-12 manual-label"></asp:Label>
                                            </div>
                                        </div>
                                        <label>Enter your Code:</label>
                                        <asp:TextBox ID="txtPin" runat="server" CssClass="form-control mb-10" placeholder="Enter Code" AutoCompleteType="Disabled" ClientIDMode="Static"></asp:TextBox>
                                        <button type="button" id="btnRequest" runat="server" clientidmode="static" class="btn btn-warning">Request OTP</button>
                                        <asp:TextBox ID="txtOTP" runat="server" CssClass="form-control mb-10" placeholder="Enter OTP" ClientIDMode="Static"></asp:TextBox>

                                        <div class="text-center mt-10">
                                            <small class="" id="lblGoogleAuthenticationInfo" runat="server" clientidmode="static"></small>
                                        </div>
                                        <div class="mt-10 countdown text-center">
                                            <span id="countdownLabelTextGoogle">&nbsp</span>
                                            <label class="label label-warning hide" style="display: inline-block; width: 70px; text-align: center;" id="countDownTimeGoogle" runat="server" clientidmode="static"></label>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">

                    <asp:Button ID="btnCode" runat="server" ClientIDMode="Static" CssClass="btn btn-sett-confirm" Text="Submit" OnClientClick="return clientGoogleValidation()" OnClick="btnCode_Click" />

                    <button id="btnGoogleAuthClose" type="button" class="btn btn-sett-cancel" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade mobile-height" id="loginPassword">
        <div class="modal-dialog sett two">
            <asp:Panel ID="Panel2" runat="server" DefaultButton="btnChangePassword">
                <div class="modal-content">
                    <div class="modal-header sett-modalhead">
                        <h2 class="text-white mb-0">Login Password</h2>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mb-10">
                                    <label>Your Current Password <sup class="text-danger">*</sup></label>
                                    <asp:TextBox ID="txtOldPassword" ClientIDMode="Static" TextMode="Password" runat="server" CssClass="form-control" placeholder="Enter Current Password"></asp:TextBox>
                                </div>
                                <div class="form-group mb-10">
                                    <label>Your New Password <sup class="text-danger">*</sup></label>
                                    <asp:TextBox ID="txtNewPassword" ClientIDMode="Static" TextMode="Password" runat="server" CssClass="form-control" placeholder="Enter New Password"></asp:TextBox>
                                </div>
                                <div class="form-group mb-10">
                                    <label>Confirm New Password <sup class="text-danger">*</sup></label>
                                    <asp:TextBox ID="txtConfirmNewPassword" ClientIDMode="Static" TextMode="Password" runat="server" CssClass="form-control" placeholder="Enter New Password"></asp:TextBox>
                                </div>
                                <div class="form-group mb-10">
                                    <label>Authentication<sup class="text-danger">*</sup></label>
                                    <div class="row mb-5">
                                        <div class="col-md-6">
                                            <asp:RadioButton ID="rdnGoogle" runat="server" Text=" Google Authentication" CssClass="fs-12" GroupName="pac2" ClientIDMode="Static" Checked="true" />
                                        </div>
                                        <div class="col-md-6">
                                            <asp:RadioButton ID="rdnMobile" runat="server" Text=" Mobile Authentication" CssClass="fs-12" GroupName="pac2" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                    <div class="form-group mb-5">
                                        <asp:TextBox ID="txtPacNo" runat="server" CssClass="form-control" placeholder="" ClientIDMode="Static"></asp:TextBox>
                                        <button type="button" id="btnPAC" runat="server" class="btn btn-sm btn-primary hide" clientidmode="static" style="position: absolute; margin-top: -31px; right: 19px;">Request OTP</button>
                                    </div>

                                    <small class="" id="lblInfo" runat="server" clientidmode="static"></small>
                                    <br />
                                    <small class="" id="countdownLabelText" runat="server" clientidmode="static"></small>
                                    <label class="label label-warning hide" style="display: inline-block; width: 70px; text-align: center;" id="countDownTime" runat="server" clientidmode="static"></label>
                                    <div id="LoginPasswordGA" runat="server" clientidmode="static"><small><strong>Note: If you choose Google Authentication, please make sure Google Authenticator is binded.</strong></small></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <ul>
                                    <li>Avoid using dictionary or generic passwords or a password that is easily identified with you (e.g. pass1234, birthday etc)</li>
                                    <li>Choose password that aren't easy to guess but easy to remember and use it as the basis of a password.</li>
                                    <li>Create smart password with using a combination of numbers, upper and lower case letters. (e.g. Smrt1zv3st0r).</li>
                                    <li>Do not share your password or write them down. Be wary of unexpected calls or emails requesting personal information, passwords or financial details. Apex will never send you an email or SMS asking you to verify or provide sensitive/confidential information</li>
                                    <li>Choose a different password for each online account. For example, using the same pasword on bank account and social media or email may increase risk of identity theft or fraud</li>
                                    <li>For security purpose, Google Code/ Mobile OTP must be keyed in for change of password.</li>
                                    <li>If you suspect your password has been compromised, please call Customer Service Hotline immediately at 03-20959999.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnChangePassword" runat="server" CssClass="btn btn-sett-confirm" Text="Submit" OnClientClick="return clientPasswordValidation()" OnClick="btnChangePassword_Click" ClientIDMode="Static" />
                        <button type="button" id="LoginPasswordClose" class="btn btn-sett-cancel" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>

    <div class="modal fade mobile-height" id="transPassword">


        <div class="modal-dialog sett two">
            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnTransactionPassword">
                <div class="modal-content">
                    <div class="modal-header sett-modalhead">
                        <h2 class="text-white mb-0">Transaction Password</h2>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <p>By default, transaction password will be your login password.</p>
                                <div class="form-group mb-10">
                                    <label>Your Login Password<sup class="text-danger">*</sup></label>
                                    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Your password"></asp:TextBox>
                                </div>
                                <div class="form-group mb-10">
                                    <label>New Transaction Password<sup class="text-danger">*</sup></label>
                                    <asp:TextBox ID="txtNewTransactionPassword" TextMode="Password" ClientIDMode="Static" runat="server" CssClass="form-control" placeholder="New transaction password"></asp:TextBox>
                                </div>
                                <div class="form-group mb-10">
                                    <label>Confirm Transaction Password<sup class="text-danger">*</sup></label>
                                    <asp:TextBox ID="txtConfirmTransactionPassword" TextMode="Password" ClientIDMode="Static" runat="server" CssClass="form-control" placeholder="Confirm transaction password"></asp:TextBox>
                                </div>
                                <div class="form-group mb-10">
                                    <label>Authentication<sup class="text-danger">*</sup></label>
                                    <div class="row mb-5">
                                        <div class="col-md-6">
                                            <asp:RadioButton ID="rdnGoogleTrans" runat="server" Text=" Google Authentication" CssClass="fs-12" GroupName="pac1" ClientIDMode="Static" Checked="true" />
                                        </div>
                                        <div class="col-md-6">
                                            <asp:RadioButton ID="rdnMobileTrans" runat="server" Text=" Mobile Authentication" CssClass="fs-12" GroupName="pac1" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-5">
                                    <asp:TextBox ID="txtPacNoTrans" runat="server" CssClass="form-control" placeholder="" ClientIDMode="Static"></asp:TextBox>
                                    <button type="button" id="btnPACTrans" runat="server" class="btn btn-sm btn-primary hide" clientidmode="static" style="position: absolute; margin-top: -31px; right: 19px;">Request OTP</button>
                                </div>

                                <small class="" id="lblInfoTrans" runat="server" clientidmode="static"></small>
                                <br />
                                <small class="" id="countdownLabelTextTrans" runat="server" clientidmode="static"></small>
                                <label class="label label-warning hide" style="display: inline-block; width: 70px; text-align: center;" id="countDownTimeTrans" runat="server" clientidmode="static"></label>
                                <div id="TransactionPasswordGA" runat="server" clientidmode="static"><small><strong>Note: If you choose Google Authentication, please make sure Google Authenticator is binded.</strong></small></div>
                            </div>
                            <div class="col-md-6">
                                <ul>
                                    <li>Avoid using dictionary or generic passwords or a password that is easily identified with you (e.g. pass1234, birthday etc)</li>
                                    <li>Choose password that aren't easy to guess but easy to remember and use it as the basis of a password.</li>
                                    <li>Create smart password with using a combination of numbers, upper and lower case letters. (e.g. Smrt1zv3st0r).</li>
                                    <li>Do not share your password or write them down. Be wary of unexpected calls or emails requesting personal information, passwords or financial details. Apex will never send you an email or SMS asking you to verify or provide sensitive/confidential information</li>
                                    <li>Choose a different password for each online account. For example, using the same pasword on bank account and social media or email may increase risk of identity theft or fraud</li>
                                    <li>For security purpose, Google Code/ Mobile OTP must be keyed in for change of password.</li>
                                    <li>If you suspect your password has been compromised, please call Customer Service Hotline immediately at 03-20959999.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnTransactionPassword" runat="server" CssClass="btn btn btn-sett-confirm" Text="Submit" OnClientClick="return clientTransactionValidation()" OnClick="btnTransactionPassword_Click" ClientIDMode="Static" />
                        <button type="button" class="btn btn-sett-cancel" onclick="clearTransaction()" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>

    <div class="modal fade" id="idVerify">
        <div class="modal-dialog sett">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h2 class="text-white mb-0">ID Verification</h2>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnIdVerify" runat="server" CssClass="btn btn-sett-confirm" Text="Submit" />
                    <button type="button" class="btn btn-sett-cancel" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade mobile-height" id="bankDetails">
        <div class="modal-dialog sett two">
            <asp:Panel ID="bankDetailsPanel" runat="server" DefaultButton="btnBankDetailsSubmit">
                <div class="modal-content">
                    <div class="modal-header sett-modalhead">
                        <h2 class="text-white mb-0">User Settings: Bank Details</h2>
                    </div>
                    <div class="modal-body" style="height: calc(100vh - 200px); overflow-y: scroll;">
                        <div id="divAddBank" class="row" runat="server" clientidmode="static">
                            <div class="col-md-7">
                                <div class="form-group mb-10">
                                    <label>Bank Name <sup class="text-danger">*</sup></label>
                                    <asp:HiddenField ID="hdnMaHolderBankId" runat="server" ClientIDMode="Static" Value="0" />
                                    <asp:DropDownList ID="ddlBank" ClientIDMode="Static" runat="server" CssClass="form-control" EnableViewState="true">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnBankNoFormat" runat="server" ClientIDMode="Static" />
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group mb-10">
                                            <label>Account Name <sup class="text-danger">*</sup></label>
                                            <asp:DropDownList ID="ddlAccountname" ClientIDMode="Static" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group mb-10">
                                            <label>Account Number <sup class="text-danger">*</sup></label>
                                            <asp:HiddenField ID="hdnAccountNumber" runat="server" ClientIDMode="Static" Value="0" />
                                            <div>
                                                <a href="javascript:;" data-toggle="tooltip" title="Click here to view bank account number guide" style="position: absolute; right: 23px; margin-top: 4px; z-index: 999;">
                                                    <span onclick="helpBankAccount();"><i class="fa fa-info-circle text-info fs-16"></i></span>
                                                </a>
                                                <asp:TextBox ID="txtAccountNumber" ClientIDMode="Static" runat="server" CssClass="form-control" placeholder="Enter Account Number"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div id="divBankAttachment" class="form-group mb-10">
                                            <label>Attachment file <sup class="text-danger">* <span style="font-size: 9px; font-weight: 100; top: 2px;">( jpeg / jpg / png / pdf )</span></sup></label>
                                            <div>
                                                <a href="javascript:;" data-toggle="tooltip" title="Click here to view guideline for attaching bank details file" style="position: absolute; right: 23px; margin-top: 4px; z-index: 999;">
                                                    <div onclick="helpBank();"><i class="fa fa-info-circle text-info fs-16"></i></div>
                                                </a>
                                                <asp:FileUpload ID="FileUpload1" CssClass="userKYC form-control" runat="server" ClientIDMode="Static" accept=".jpeg,.jpg,.png,.pdf" />
                                                <div id="helpBank" class="bank-attachment-modal overlay hide">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="guide-popup">
                                                                <p class="text-justify">To expedite the process of approve your valid bank account maintain with us, please take/save a colour version of your bank statement, ensure it’s in focus and we can read all the information as per the sample of bank statement is shown below:</p>
                                                                <img src="Content/MyImage/Guide for Bank Details.PNG" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divBankAttachment1" class="form-group mb-10 hide">
                                            <label>Attachment file :</label>
                                            <a href="#" target="_blank" id="attachemtFile">
                                                <asp:Label ID="lblBankAttachment" runat="server" ClientIDMode="Static" CssClass="hide"></asp:Label>
                                                Click here to view
                                            </a>
                                        </div>
                                    </div>


                                </div>
                                <div class="form-group mb-10">
                                    <label>Authentication<sup class="text-danger">*</sup></label>
                                    <div class="row mb-5">
                                        <div class="col-md-6">
                                            <asp:RadioButton ID="rdnGoogleBank" runat="server" Text="Google Authentication" CssClass="fs-12" GroupName="pac" ClientIDMode="Static" Checked="true" />
                                        </div>
                                        <div class="col-md-6">
                                            <asp:RadioButton ID="rdnMobileBank" runat="server" Text="Mobile Authentication" CssClass="fs-12" GroupName="pac" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                    <div class="form-group mb-5">
                                        <asp:TextBox ID="txtPacNoBank" runat="server" CssClass="form-control" placeholder="" ClientIDMode="Static"></asp:TextBox>
                                        <button type="button" id="btnPACBank" runat="server" class="btn btn-sm btn-primary hide" clientidmode="static" style="position: absolute; margin-top: -31px; right: 19px;">Request OTP</button>
                                    </div>
                                    <small class="" id="lblInfoBank" runat="server" clientidmode="static"></small>
                                    <br />
                                    <small class="" id="countdownLabelTextBank" runat="server" clientidmode="static"></small>
                                    <small class="" id="countDownTimeBank" runat="server" clientidmode="static"></small>
                                    <div id="divBank2" class="col-md-12">
                                        <label id="approvalBank" runat="server" visible="false">Approval:</label>
                                        <div id="banknotverified" runat="server" visible="false">
                                            <a class="text-warning"><i class="fa fa-exclamation-circle mr-6"></i>Bank Detail Approval is Pending</a><br />
                                        </div>
                                        <div id="bankverified" runat="server" visible="false">
                                            <a class="text-success"><i class="fa fa-check mr-6"></i>Bank Detail is Approved</a><br />
                                        </div>
                                        <div id="bankdenied" runat="server" visible="false">
                                            <a class="text-danger"><i class="fa fa-times mr-6"></i>Bank Detail is Denied</a><br />
                                        </div>
                                        <table>
                                            <tbody id="bankdetail" runat="server">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="BankDetailsGA" runat="server" clientidmode="static"><small><strong>Note: If you choose Google Authentication, please make sure Google Authenticator is binded.</strong></small></div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group mb-10">
                                    <p><b>Notes</b></p>
                                    <ul>
                                        <li>Your bank account number registered will be maintained by Apex for crediting of all future payments (distribution, redemption proceeds and other monies payable) to you.</li>
                                        <li>Kindly update your bank account number(s) and only One bank account number is allowed to be binded on each Master Account in User account setting. </li>
                                        <li>Remittance amount that you receive may be lesser than your initial redemption amount due to correspondence bank charges or intermediate bank charges (if any), which will be borne by investor and deducted directly from the redemption proceeds.</li>
                                        <li>For security purpose, Google Code/ Mobile OTP must be keyed in for registration of bank account.</li>
                                        <li>All payments will be made to the MAIN account holder. We will not make any payments to third-parties</li>
                                        <li>In the event that  any payment to your bank account is  unsuccessful, Apex will issue a cheque and send to your mailing address.</li>
                                        <li>To update your existing bank account details, kindly attach a copy of your latest bank statement (not later than 3 months).</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="divbanklist">
                            <div class="col-md-12">
                                <div class="table-responsive" style="border: none !important;">
                                    <table class="table table-font-size-13 table-cell-pad-5 table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Bank Name</th>
                                                <th>Account Number</th>
                                                <th>MA no</th>
                                                <th>File</th>
                                                <th>Created Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbodyUserBankAccounts" runat="server">
                                            <tr>
                                                <td colspan="7" class="text-center">No records found.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <asp:HiddenField ID="hdnBankId" runat="server" ClientIDMode="Static" />
                                    <asp:Button ID="btnDeleteBank" runat="server" ClientIDMode="Static" CssClass="hide" OnClick="btnDeleteBank_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="AddBank" class="btn btn-sett-confirm" runat="server" clientidmode="static">Add</button>
                            <asp:Button ID="btnBankDetailsSubmit" ClientIDMode="Static" OnClientClick="return clientBankValidation()" runat="server" CssClass="btn btn-sett-confirm" Text="Submit" OnClick="btnBankDetailsSubmit_Click" />
                            <button type="button" id="ViewBankList" class="btn btn-sett-cancel">Back</button>
                            <button type="button" class="btn btn-sett-cancel" onclick="return clearBank()" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>

    </div>

    <div class="modal fade" id="LoginHistory">
        <div class="modal-dialog sett">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h2 class="text-white mb-0">Login History</h2>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <h5>Recent Login History<span id="spanUsername" runat="server"></span></h5>
                                    <div class="table-responsive">
                                        <table class="table table-font-size-13 table-cell-pad-5 table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Login Date</th>
                                                    <th>Login Time</th>
                                                    <th>Login IP Address</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyUserLoginHistory" runat="server">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sett-cancel" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="hardcopySetting">
        <div class="modal-dialog sett">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h2 class="text-white mb-0">Hardcopy Settings</h2>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="hardcopyActivation" id="hardcopyActivation" runat="server" clientidmode="Static">
                                        <h5 class="mb-10">Requested for Hardcopy Activation: </h5>
                                        <p class="mb-8">Upon your request, statements/reports will be sent to your mailing address & softcopy for statements/reports will not be sent to you.</p>
                                        <p class="mb-10">However you can choose to conserve trees and fuel by replacing your hardcopy statements and reports with an electronic version by de-activate hardcopy.</p>
                                        <div class="row mb-20">
                                            <div class="col-lg-3 col-md-5">
                                                <%--<a href="javascript:;" id="btnhardcopyActivation" class="btn btn-infos1 btn-sm btn-block" style="width: 200px">Activate Hardcopy</a>--%>
                                                <asp:Button CssClass="btn btn-infos1 btn-block" ID="btnActivate" runat="server" ClientIDMode="Static" Text="Activate Hardcopy" Width="200px" OnClick="btnActivate_Click" OnClientClick="return clientLoad()" />
                                            </div>
                                        </div>
                                        <div>
                                            <h5 class="mb-8">NOTE:</h5>
                                            <p class="mb-8">Your request for hardcopy statements/reports will be made available in the next statement cycle.</p>

                                        </div>
                                    </div>

                                    <div class="hardcopyActivation" id="hardcopyDeactivation" runat="server" clientidmode="Static">
                                        <h5 class="mb-10">Requested for Hardcopy De-Activation:</h5>
                                        <p class="mb-8">
                                            Upon your request, Statements/Reports will be sent to your email address and hardcopy Statements/Reports WILL NOT be sent to you.  
                                        </p>
                                        <div class="row mb-20">
                                            <div class="col-lg-3 col-md-5">
                                                <%--<a href="javascript:;" id="btnhardcopyDeactivation" class="btn btn-infos1 btn-sm btn-block" style="width: 200px">Deactivate Hardcopy</a>--%>
                                                <asp:Button CssClass="btn btn-infos1 btn-block" ID="btnDeactivate" runat="server" ClientIDMode="Static" Text="De-Activate Hardcopy" Width="200px" OnClick="btnDeactivate_Click" OnClientClick="return clientLoad()" />
                                            </div>
                                        </div>
                                        <div>
                                            <h5 class="mb-8">NOTE:</h5>
                                            <p class="mb-8">Your request to de-activate hardcopy statements/reports will take place in next statement cycle.</p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sett-cancel" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addressupload">
        <div class="modal-dialog sett">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h2 class="text-white mb-0">Address Document</h2>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <asp:FileUpload ID="fuAddressupload" runat="server" ClientIDMode="Static" accept=".jpeg,.jpg,.png,.pdf" />
                                    <label for="fuAddressupload"></label>
                                </div>
                                <div class="avatar-preview2">
                                    <div id="imagePreview3" style="background-image: url(/Content/MyImage/11.png);"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label id="approvalAddress" runat="server" visible="false">Approval:</label>
                            <div id="addressnotverified" runat="server" visible="false">
                                <a class="text-warning"><i class="fa fa-times"></i>Address Proof is Pending</a><br />
                                <a href="#" target="_blank" id="addressnotverifiedlnk" runat="server">Click here to view document.</a>
                            </div>
                            <div id="addressverified" runat="server" visible="false">
                                <a class="text-success"><i class="fa fa-check"></i>Address Proof is Approved</a><br />
                                <a href="#" target="_blank" id="addressverifiedlnk" runat="server">Click here to view document.</a>
                            </div>
                            <div id="addressdenied" runat="server" visible="false">
                                <a class="text-danger"><i class="fa fa-times"></i>Address Proof is Rejected</a><br />
                                <a href="#" target="_blank" id="addressdeniedlnk" runat="server">Click here to view document.</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnFuAddressUpload" runat="server" ClientIDMode="Static" OnClientClick="return clientAddressValidation()" CssClass="btn btn-sett-confirm" Text="Submit" OnClick="btnFuAddressUpload_Click" />
                    <button type="button" class="btn btn-sett-cancel" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="KYC">
        <div class="modal-dialog sett">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h2 class="text-white mb-0">Know Your Client</h2>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <asp:FileUpload ID="fuKYC" CssClass="userKYC" runat="server" ClientIDMode="Static" accept=".jpeg,.jpg,.png,.pdf" />
                                    <label for="fuKYC"></label>
                                </div>
                                <div class="avatar-preview2">
                                    <div id="imagePreview2" style="background-image: url(/Content/MyImage/11.png);"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label id="approvalKyc" runat="server" visible="false">Approval:</label>
                            <div id="kycnotverified" runat="server" visible="false">
                                <a class="text-warning"><i class="fa fa-exclamation-circle mr-6"></i>KYC Approval is Pending</a><br />
                                <a href="#" target="_blank" id="kycnotverifiedlnk" runat="server">Click here to view document.</a>
                            </div>
                            <div id="kycverified" runat="server" visible="false">
                                <input type="hidden" id="lblKYC" value="1" />
                                <a class="text-success"><i class="fa fa-check mr-6"></i>KYC is Approved</a><br />
                                <a href="#" target="_blank" id="kycverifiedlnk" runat="server">Click here to view document.</a>
                            </div>
                            <div id="kycdenied" runat="server" visible="false">
                                <a class="text-danger"><i class="fa fa-times mr-6"></i>KYC is Rejected</a><br />
                                <a href="#" target="_blank" id="kycdeniedlnk" runat="server">Click here to view document.</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnFuKYC" runat="server" CssClass="btn btn-sett-confirm" Text="Submit" ClientIDMode="Static" OnClientClick="return clientKYCValidation()" OnClick="btnFuKYC_Click" />
                    <button type="button" class="btn btn-sett-cancel" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="profile">
        <div class="modal-dialog sett">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h2 class="text-white mb-0">Profile</h2>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <%--<div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                    <label for="imageUpload"></label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                                    </div>
                                </div>
                            </div>--%>
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <asp:FileUpload ID="imageUpload" runat="server" ClientIDMode="Static" />
                                    <label for="imageUpload"></label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url(/Content/MyImage/1.png);">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="namecard">
                                <h3 class="mb-10">Username: <span id="profileUsername" runat="server"></span></h3>
                                <h5>Email Id: <span id="profileEmailId" runat="server"></span></h5>
                                <h5>Contact Number: <span id="profileContactNumber" runat="server"></span></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnSubmitProfile" runat="server" CssClass="btn btn-sett-confirm" Text="Submit" OnClick="btnSubmitProfile_Click" OnClientClick="return clientLoad()" />
                    <button type="button" class="btn btn-sett-cancel" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnMobilePinRequested" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="hdnMobilePinRequestedTrans" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="hdnMobilePinRequestedBank" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="hdnMobilePinRequestedGoogle" runat="server" ClientIDMode="Static" Value="0" />

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>

    <script type="text/javascript">

        var hdnMobilePinRequested = parseInt($('#hdnMobilePinRequested').val());
        var hdnMobilePinRequestedTrans = parseInt($('#hdnMobilePinRequestedTrans').val());
        var hdnMobilePinRequestedBank = parseInt($('#hdnMobilePinRequestedBank').val());
        var hdnMobilePinRequestedGoogle = parseInt($('#hdnMobilePinRequestedGoogle').val());

        (function ($) {
            $.fn.inputFilter = function (inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
            };
        }(jQuery));

        $('#custom-popup-confirmation-ok').click(function () {
            var target = $('#custom-popup-confirmation-ok').attr('data-target');
            if (target == 'unbind')
                $('#btnGoogleAuthUnbindConfirm').click();
            else if (target == 'rebind')
                $('#btnGoogleAuthRebindConfirm').click();
            else if (target == 'deletebank')
                $('#btnDeleteBank').click();
            else if (target.indexOf('.aspx')) {
                window.location.href = target;
            }
        });

        function ShowConfirmationToUnbindGoogleAuth() {
            ShowCustomConfirmationMessage('Confirmation', 'Are you sure, you want to unbind?', 'unbind');
            return false;
        }

        function ShowConfirmationToRebindGoogleAuth() {
            ShowCustomConfirmationMessage('Confirmation', 'Are you sure, you want to rebind?', 'rebind');
            return false;
        }

        $(document).ready(function () {

            $('.deleteBank').click(function () {
                var id = $(this).data('id');
                $('#hdnBankId').val(id);
                ShowCustomConfirmationMessage('Confirmation', 'Are you sure?', 'deletebank')
            });

            //clearBank();
            $('#divAddBank').hide();
            $('#divbanklist').show();
            $('#btnBankDetailsSubmit').hide();
            $('#AddBank').show();
            $('#ViewBankList').hide();

            //$('#ddlBank').change(function () {
            //    $('#txtAccountNumber').val('');
            //    $('#txtAccountNumber').attr('readonly', 'readonly');
            //    if ($('#ddlBank').val() != "") {
            //        $.ajax({
            //            url: "UserSettings.aspx/GetBankAccNoLength",
            //            contentType: 'application/json; charset=utf-8',
            //            type: "GET",
            //            dataType: "JSON",
            //            async: true,
            //            data: { 'bankId': JSON.stringify($('#ddlBank').val()) },
            //            success: function (data) {
            //                $('#txtAccountNumber').removeAttr('readonly');
            //                if (data.d.IsSuccess == true) {
            //                    $('#hdnBankNoFormat').val(data.d.Data);
            //                }
            //                else {
            //                    $('#ddlBank').val('0');
            //                    ShowCustomMessage('Alert', data.d.Message, '');
            //                }
            //            }
            //        });
            //    }
            //});

            if ($('#imgQRCode').val() != undefined) {
                $('#btnCode').show();
            }
            else {
                $('#btnCode').hide();
            }

            $('#AddBank').click(function () {
                // clearBank();
                $('#divAddBank').show();
                $('#divbanklist').hide();
                $('#btnBankDetailsSubmit').show();
                $('#AddBank').hide();
                $('#ViewBankList').show();
            });

            $('#ViewBankList').click(function () {
                $('#divAddBank').hide();
                $('#divbanklist').show();
                $('#btnBankDetailsSubmit').hide();
                $('#AddBank').show();
                $('#ViewBankList').hide();
            });


            $('#txtAccountNumber').inputFilter(function (value) {
                //var bankcount = parseInt($('#hdnBankNoFormat').val()); //parseInt($('option:selected', '#ddlBank').attr('accountnumlength'));
                //if (value.length > bankcount) {
                //    return false;
                //}
                //else {
                return /^\d*$/.test(value);
                //}
                //return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 500);
            });


            $('#txtOTP').prop('readonly', true);
            if ($('#lblKYC').val() == "1") {
                $('#btnFuKYC').hide();
            }

            $('#rdnMobileBank').change(function () {
                if ($(this).is(':checked')) {
                    $('#txtPacNoBank').attr('placeholder', 'Enter OTP');
                    $('#btnPACBank').removeClass('hide');
                    $('#BankDetailsGA').addClass('hide');
                }
            });
            $('#rdnGoogleBank').change(function () {
                if ($(this).is(':checked')) {
                    $('#txtPacNoBank').attr('placeholder', 'Enter Code');
                    $('#btnPACBank').addClass('hide');
                    $('#BankDetailsGA').removeClass('hide');
                }
            });

            $('#btnGoogleAuthClose').click(function () {
                $('#txtPin,#txtOTP').val('');
                $('#txtOTP').prop('readonly', true);
                $('#txtPin,#txtOTP').each(function () {
                    $(this).css({
                        "borderBottom": "",
                        //"background": "LightBlue"
                    });
                });
                var hdnImpersonate = $('#hdnImpersonate').val();
                if (hdnImpersonate == "1") {

                }
                else {
                    if ($('#imgQRCode').val() != undefined) {
                        window.location.href = "UserSettings.aspx";
                    }
                    else {
                        if ($('#btnRebindGoogleAuth').val() != undefined)
                            window.location.href = "UserSettings.aspx";
                        else {




                            window.location.href = "UserSettings.aspx?isPopup=1&Popup=openPopup('googleAuth')";

                        }
                    }
                }

            });

            $('#LoginPasswordClose').click(function () {

                $('#txtOldPassword,#txtNewPassword,#txtConfirmNewPassword, #txtPacNo').each(function () {
                    $(this).css({
                        "borderBottom": "",
                        //"background": "LightBlue"
                    });
                });

                $('#txtOldPassword').val('');
                $('#txtNewPassword').val('');
                $('#txtConfirmNewPassword').val('');
                $('#txtPacNo').val('');
            });

            $('#btnPACBank').click(function () {

                if (hdnMobilePinRequestedBank == 0) {
                    $.ajax({
                        url: "UserSettings.aspx/PhoneVerify",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: function (data) {
                            return data;
                        },
                        success: function (data) {
                            var json = data.d;
                            //console.log(json);
                            $('#hdnMobilePinRequestedBank').val(1);
                            $('#txtPacNoBank').val('');
                            if (json == "sent") {
                                $('#lblInfoBank').html("OTP sent to mobile successfully.");
                                $('#countdownLabelTextBank').html('Please Enter your OTP in');
                                $('#btnPACBank').attr('disabled', 'disabled');
                                StartTimerBank();
                            }
                            else if (json == "already sent") {
                                $('#lblInfoBank').html("OTP already sent.");
                                $('#btnPACBank').attr('disabled', 'disabled');
                                StartTimerBank();
                            }
                            else if (json == "sms locked") {
                                $('#lblInfoBank').html("Too many attempts. Please try later.");
                                $('#countdownLabelTextBank').html('You can request new OTP in ');
                                //$('#requestNewPinLinkDiv').addClass('hide');
                                //$('#btnRequestNewPin').attr('disabled', 'disabled');
                                $('#btnPACBank').attr('disabled', 'disabled');
                                $('#hdnMobilePinRequestedBank').val(0);
                                SMSLockStartTimerBank();
                            }
                            else {
                                $('#lblInfoBank').html("No Account Registered with this Mobile Number.");
                            }

                        }
                    });
                }
                else {

                }
            });
            // End New code


        });

        function readKYC(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview2').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview2').hide();
                    $('#imagePreview2').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#fuKYC").change(function () {
            readKYC(this);
        });

        function readADDR(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview3').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview3').hide();
                    $('#imagePreview3').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#fuAddressupload").change(function () {
            readADDR(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function clientAddressValidation() {
            if ($('#fuAddressupload').val() == "") {
                ShowCustomMessage('Alert', 'Please choose file', '');
                return false;
            }
            else {
                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            }
        }

        function clientKYCValidation() {
            if ($('#fuKYC').val() == "") {
                ShowCustomMessage('Alert', 'Please choose file', '');
                return false;
            }
            else {
                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            }
        }

        function bankReadonly() {
            $('#txtAccountNumber').prop('readonly', true);
            $('#txtPacNoBank').prop('readonly', true);
            $('#ddlAccountname').prop('disabled', true);
            $('#ddlBank').prop('disabled', true);
            $('#btnBankDetailsSubmit').hide();
            $('#btnBankEdit').show();
            $('#divBankAttachment').hide();
            $('#divBankAttachment1').show();
            $('#attachemtFile').attr('href', $('#lblBankAttachment').text())
            $('#rdnGoogleBank').attr('disabled', true);
            $('#rdnMobileBank').attr('disabled', true)
        }

        function addBank() {
            setTimeout(function () {
                $('#AddBank').click();
            }, 100)
        }

        function clearBank() {
            $('#ddlBank').val('0');
            $('#txtAccountNumber').val('');
            $('#txtPacNoBank').val('');
            $('#FileUpload1').val('');
            //if ($('#hdnMaHolderBankId').val() != '') {
            //    $('#ddlBank').val($('#hdnMaHolderBankId').val());
            //}
            //$('#txtAccountNumber').val($('#hdnAccountNumber').val());
            //$('#divBank2').show();

            $('#ddlBank').css({
                "borderBottom": "",
            });
            $('#FileUpload1').css({
                "borderBottom": "",
            });
            $('#txtAccountNumber').css({
                "borderBottom": "",
            });
            $('#txtPacNoBank').css({
                "borderBottom": "",
            });

            $('#divAddBank').hide();
            $('#divbanklist').show();
            $('#btnBankDetailsSubmit').hide();
            $('#AddBank').show();
            $('#ViewBankList').hide();

            // bankReadonly();
        }


        function clientBankValidation() {
            var isValid = true;
            if ($('#ddlBank').val() == '' || $('#ddlBank').val() == '0') {
                $('#ddlBank').css({
                    "borderBottom": "1px solid red",
                });
                //ShowCustomMessage('Alert', 'Please Select bank', '');
                isValid = false;
            }
            else {
                $('#ddlBank').css({
                    "borderBottom": "",
                });
            }
            if (validation('txtAccountNumber') == false) {
                $('#txtAccountNumber').css({
                    "borderBottom": "1px solid red",
                });
                //ShowCustomMessage('Alert', 'Please enter Account Number', '');
                isValid = false;
            }
            else {
                $('#txtAccountNumber').css({
                    "borderBottom": "",
                });
                //var accountLength = $('#txtAccountNumber').val().length;
                //if ($('option:selected', '#ddlBank').attr('accountnumlength') != undefined) {
                //    var bankcount = parseInt($('option:selected', '#ddlBank').attr('accountnumlength'));
                //    if (accountLength != bankcount) {
                //        $('#txtAccountNumber').css({
                //            "borderBottom": "1px solid red",
                //        });
                //        ShowCustomMessage('Alert', 'Account number Invalid! Length must be ' + bankcount + '.', '');
                //        isValid = false;
                //    }
                //    else {
                //        $('#txtAccountNumber').css({
                //            "borderBottom": "",
                //        });
                //    }
                //}
            }
            if ($('#FileUpload1').val() == "") {
                $('#FileUpload1').css({
                    "borderBottom": "1px solid red",
                });
                //ShowCustomMessage('Alert', 'Please choose file', '');
                isValid = false;
            }
            else {
                $('#FileUpload1').css({
                    "borderBottom": "",
                });
            }
            if ($('#rdnGoogleBank').is(':checked') || $('#rdnMobileBank').is(':checked')) {
                if ($('#txtPacNoBank').val() == "") {
                    $('#txtPacNoBank').css({
                        "borderBottom": "1px solid red",
                    });
                    isValid = false;
                }
            }
            if (isValid) {
                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            }
            return isValid;
        }

        function validation(test) {
            if ($('#' + test).val() == '' || $('#' + test).val() == null) {
                return false;
            }
            return true;
        }
        var ispasswordValid = false;
        var isotpValid = false;
        function clientTransactionValidation() {
            var isValid = true;
            var message = "";

            $('#txtNewTransactionPassword,#txtConfirmTransactionPassword,#txtPassword, #txtPacNoTrans').each(function () {
                if ($.trim($(this).val()) == '') {
                    $(this).css({
                        "borderBottom": "1px solid red",
                        //"background": "#FFCECE"
                    });
                    isValid = false;
                }
                else {
                    $(this).css({
                        "borderBottom": "",
                        //"background": "LightBlue"
                    });
                }
            });
            if (!isValid) {
                return false;
            }
            // Validate capital letters
            var hasLowerChar = /[a-z]/g;
            var hasUpperChar = /[A-Z]/g;
            if (hasLowerChar.test($('#txtNewTransactionPassword').val()) == false) {
                message += "New Transaction Password must contain at least one lowercase alphabet letter.<br/>";
                isValid = false;
            }
            if (hasUpperChar.test($('#txtNewTransactionPassword').val()) == false) {
                message += "New Transaction Password must contain at least one uppercase alphabet letter.<br/>";
                isValid = false;
            }
            // Validate numbers
            var numbers = /[0-9]/g;
            if (!$('#txtNewTransactionPassword').val().match(numbers)) {
                message += "New Transaction Password must contain at least one number.<br/>";
                isValid = false;
            }
            // Validate length
            if (!($('#txtNewTransactionPassword').val().length >= 8 && $('#txtNewTransactionPassword').val().length <= 16)) {
                message += "New Transaction Password length must between 8 to 16 characters.<br/>";
                isValid = false;
            }
            if ($('#txtNewTransactionPassword').val() != $('#txtConfirmTransactionPassword').val()) {
                message += "New Transaction Password and Confirm Transaction Password must be the same.<br/>";
                isValid = false;
            }
            if (isValid == false) {
                ShowCustomMessage('Alert', message, '');
                return false;
            }
            else {

                if (!ispasswordValid) {
                    var password = $('#txtPassword').val();
                    var userdetails = { 'password': password }
                    $.ajax({
                        url: "Settings.aspx/PasswordVerify",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        async: true,
                        data: { 'password': JSON.stringify(password) },
                        success: function (data) {
                            if (data.d == true) {
                                ispasswordValid = true;
                                $('#btnTransactionPassword').click();
                            }
                            else {
                                ispasswordValid = false;
                                ShowCustomMessage('Alert', 'Invalid password', '')
                            }
                        }
                    });
                    return false;
                }

                if ($("#rdnMobileTrans").is(":checked") && !isotpValid) {
                    var otp = $('#txtPacNoTrans').val();
                    $.ajax({
                        url: "UserSettings.aspx/OtpVerify",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        async: true,
                        data: { 'otp': JSON.stringify(otp) },
                        success: function (data) {
                            if (data.d == 'success') {
                                isotpValid = true;
                                $('#btnTransactionPassword').click();
                            }
                            else {
                                isotpValid = false;
                                ShowCustomMessage('Alert', '' + data.d + '', '')
                            }
                        }
                    });
                    return false;
                }



                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            }
        }
        var isOldpasswordValid = false;
        var ispasswordotpValid = false;
        function clientPasswordValidation() {
            var isValid = true;
            var message = "";

            $('#txtOldPassword,#txtNewPassword,#txtConfirmNewPassword, #txtPacNo').each(function () {
                if ($.trim($(this).val()) == '') {
                    $(this).css({
                        "borderBottom": "1px solid red",
                        //"background": "#FFCECE"
                    });
                    isValid = false;
                }
                else {
                    $(this).css({
                        "borderBottom": "",
                        //"background": "LightBlue"
                    });
                }
            });
            if (!isValid) {
                return false;
            }
            // Validate capital letters
            var hasLowerChar = /[a-z]/g;
            var hasUpperChar = /[A-Z]/g;
            if (hasLowerChar.test($('#txtNewPassword').val()) == false) {
                message += "New Password must contain at least one lowercase alphabet letter.<br/>";
                isValid = false;
            }
            if (hasUpperChar.test($('#txtNewPassword').val()) == false) {
                message += "New Password must contain at least one uppercase alphabet letter.<br/>";
                isValid = false;
            }
            // Validate numbers
            var numbers = /[0-9]/g;
            if (!$('#txtNewPassword').val().match(numbers)) {
                message += "New Password must contain at least one number.<br/>";
                isValid = false;
            }
            // Validate length
            if (!($('#txtNewPassword').val().length >= 8 && $('#txtNewPassword').val().length <= 16)) {
                message += "New Password length must between 8 to 16 characters.<br/>";
                isValid = false;
            }
            if ($('#txtNewPassword').val() != $('#txtConfirmNewPassword').val()) {
                message += "New Password and Confirm New Password must be the same.<br/>";
                isValid = false;
            }

            if (isValid == false) {
                ShowCustomMessage('Alert', message, '');
                return false;
            }
            else {


                if (!isOldpasswordValid) {
                    var password = $('#txtOldPassword').val();
                    var userdetails = { 'password': password }
                    $.ajax({
                        url: "Settings.aspx/PasswordVerify",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        async: true,
                        data: { 'password': JSON.stringify(password) },
                        success: function (data) {
                            if (data.d == true) {
                                isOldpasswordValid = true;
                                $('#btnChangePassword').click();
                                //clientPasswordValidation();
                            }
                            else {
                                isOldpasswordValid = false;
                                ShowCustomMessage('Alert', 'Invalid password', '')
                            }
                        }
                    });
                    return false;
                }
                if ($("#rdnMobile").is(":checked") && !ispasswordotpValid && isOldpasswordValid) {
                    var otp = $('#txtPacNo').val();
                    $.ajax({
                        url: "UserSettings.aspx/OtpVerify",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        async: true,
                        data: { 'otp': JSON.stringify(otp) },
                        success: function (data) {
                            if (data.d == 'success') {
                                ispasswordotpValid = true;
                                $('#btnChangePassword').click();
                            }
                            else {
                                ispasswordotpValid = false;
                                ShowCustomMessage('Alert', '' + data.d + '', '')
                            }
                        }
                    });
                    return false;
                }


                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            }
        }

        function clientGoogleValidation() {
            var isValid = true;
            $('#txtPin,#txtOTP').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "borderBottom": "1px solid red",
                        //"background": "#FFCECE"
                    });
                }
                else {
                    $(this).css({
                        "borderBottom": "",
                        //"background": "LightBlue"
                    });
                }
            });
            setTimeout(function () { clientGoogleClear(); }, 50000);
            if (isValid == false) {
                return false;
            }
            else {
                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            }
        }

        function clientGoogleClear() {
            $('#txtPin,#txtOTP').each(function () {
                $(this).css({
                    "borderBottom": "",
                    //"background": "LightBlue"
                });
            });
        }
        function helpBank() {
            ShowCustomGuideMessage("Sample Bank Statement Attachment", $(".guide-popup").html(), "");
        }
        function closeHelpBank() {
            $('#helpBank').addClass('hide');
        }

        function closeHelpBank() {
            $('#helpBank').addClass('hide');
        }

        function clearTransaction() {

            $('#txtNewTransactionPassword,#txtConfirmTransactionPassword,#txtPassword, #txtPacNoTrans').each(function () {
                $(this).css({
                    "borderBottom": "",
                    //"background": "LightBlue"
                });
            });

            $('#txtNewTransactionPassword,#txtConfirmTransactionPassword,#txtPassword').val('');
        }

        $("#imageUpload").change(function () {
            readURL(this);
        });

        $(document).ready(function () {
            $('#rdnGoogle').change(function () {
                if ($(this).is(':checked')) {
                    $('#txtPacNo').attr('placeholder', 'Enter Code');
                    $('#btnPAC').addClass('hide');
                    $('#txtPacNo').attr('readonly', false);
                    $('#LoginPasswordGA').removeClass('hide');
                }
            });
            $('#rdnMobile').change(function () {
                if ($(this).is(':checked')) {
                    $('#txtPacNo').attr('placeholder', 'Enter OTP');
                    $('#txtPacNo').attr('readonly', true);
                    $('#btnPAC').removeClass('hide');
                    $('#LoginPasswordGA').addClass('hide');
                }
            });
            $('#rdnGoogleTrans').change(function () {
                if ($(this).is(':checked')) {
                    $('#txtPacNoTrans').attr('placeholder', 'Enter Code');
                    $('#btnPACTrans').addClass('hide');
                    $('#txtPacNoTrans').attr('readonly', false);
                    $('#TransactionPasswordGA').removeClass('hide');

                }
            });
            $('#rdnMobileTrans').change(function () {
                if ($(this).is(':checked')) {
                    $('#txtPacNoTrans').attr('placeholder', 'Enter OTP');
                    $('#btnPACTrans').removeClass('hide');
                    $('#txtPacNoTrans').attr('readonly', true);
                    $('#TransactionPasswordGA').addClass('hide');
                }
            });
            $('#rdnGoogleBank').change(function () {
                if ($(this).is(':checked')) {
                    $('#txtPacNoBank').attr('placeholder', 'Enter Code');
                    $('#btnPACBank').addClass('hide');
                    $('#txtPacNoBank').attr('readonly', false);
                }
            });
            $('#rdnMobileBank').change(function () {
                if ($(this).is(':checked')) {
                    $('#txtPacNoBank').attr('placeholder', 'Enter OTP');
                    $('#btnPACBank').removeClass('hide');
                    $('#txtPacNoBank').attr('readonly', true);
                }
            });
        });

        $(document).ready(function () {

            if ($('#rdnGoogle').is(':checked')) {
                $('#txtPacNo').attr('placeholder', 'Enter Code');
                $('#btnPAC').addClass('hide');
            }
            if ($('#rdnMobile').is(':checked')) {
                $('#txtPacNo').attr('placeholder', 'Enter OTP');
                $('#btnPAC').removeClass('hide');
            }
            if ($('#rdnGoogleTrans').is(':checked')) {
                $('#txtPacNoTrans').attr('placeholder', 'Enter Code');
                $('#btnPACTrans').addClass('hide');
            }
            if ($('#rdnMobileTrans').is(':checked')) {
                $('#txtPacNoTrans').attr('placeholder', 'Enter OTP');
                $('#btnPACTrans').removeClass('hide');
            }
            if ($('#rdnGoogleBank').is(':checked')) {
                $('#txtPacNoBank').attr('placeholder', 'Enter Code');
                $('#btnPACBank').addClass('hide');
            }
            if ($('#rdnMobileBank').is(':checked')) {
                $('#txtPacNoBank').attr('placeholder', 'Enter OTP');
                $('#btnPACBank').removeClass('hide');
            }
        });

    </script>
    <script type="text/javascript">
        function openPopup(popupid) {
            $('a[data-target="#' + popupid + '"]').click();
        }
        function secondsTimeSpanToHMS(s) {
            var h = Math.floor(s / 3600); //Get whole hours
            s -= h * 3600;
            var m = Math.floor(s / 60); //Get remaining minutes
            s -= m * 60;
            return h + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s); //zero padding on minutes and seconds
        }
        var hdnSMSExpirationTimeInSeconds = parseInt($('#hdnSMSExpirationTimeInSeconds').val());
        var hdnSMSLockTimeInSeconds = parseInt($('#hdnSMSLockTimeInSeconds').val());

        function StartTimer() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSExpirationTimeInSeconds);
            //var timer2 = "5:01";
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    $('#countdownLabelText').html('Pin Expired. Please Request Again.<br/>');
                    $('#countDownTime').addClass('hide');
                    $('#countDownTime').html('');
                    $('#btnPAC').removeAttr('disabled');
                    $('#hdnMobilePinRequested').val(0);
                }
                else {
                    $('#countdownLabelText').html('You can request new OTP in ');
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#countDownTime').html(hours + ':' + minutes + ':' + seconds + '<br/>');
                    $('#countDownTime').removeClass('hide');
                    timer2 = hours + ':' + minutes + ':' + seconds;
                }
            }, 1000);
        }

        function StartTimerTrans() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSExpirationTimeInSeconds);
            //var timer2 = "5:01";
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    $('#countdownLabelTextTrans').html('Pin Expired. Please Request Again.<br/>');
                    $('#countDownTimeTrans').html('');
                    $('#countDownTimeTrans').addClass('hide');
                    $('#btnPACTrans').removeAttr('disabled');
                    $('#hdnMobilePinRequestedTrans').val(0);
                }
                else {
                    $('#countdownLabelTextTrans').html('You can request new OTP in ');
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#countDownTimeTrans').html(hours + ':' + minutes + ':' + seconds + '<br/>');
                    $('#countDownTimeTrans').removeClass('hide');
                    timer2 = hours + ':' + minutes + ':' + seconds;
                }
            }, 1000);
        }

        function StartTimerBank() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSExpirationTimeInSeconds);
            //var timer2 = "5:01";
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    $('#countdownLabelTextBank').html('Pin Expired. Please Request Again.<br/>');
                    $('#countDownTimeBank').html('');
                    $('#countDownTimeBank').addClass('hide');
                    //$('#requestNewPinLinkDiv').removeClass('hide');
                    //$('#btnRequestNewPin').removeAttr('disabled');
                    $('#btnPACBank').removeAttr('disabled');
                    $('#hdnMobilePinRequestedBank').val(0);
                }
                else {
                    $('#countdownLabelTextBank').html('You can request new OTP in ');
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#countDownTimeBank').html(hours + ':' + minutes + ':' + seconds + '<br/>');
                    $('#countDownTimeBank').removeClass('hide');
                    timer2 = hours + ':' + minutes + ':' + seconds;
                }
            }, 1000);
        }

        function StartTimerGoogle() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSExpirationTimeInSeconds);
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    $('#countdownLabelTextGoogle').html('Pin Expired. Please Request Again.<br/>');
                    $('#countDownTimeGoogle').html('');
                    $('#countDownTimeGoogle').addClass('hide');
                    $('#btnRequest').removeAttr('disabled');
                    $('#hdnMobilePinRequestedGoogle').val(0);
                }
                else {
                    $('#countdownLabelTextGoogle').html('You can request new OTP in ');
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#countDownTimeGoogle').html(hours + ':' + minutes + ':' + seconds + '<br/>');
                    $('#countDownTimeGoogle').removeClass('hide');
                    timer2 = hours + ':' + minutes + ':' + seconds;
                }
            }, 1000);
        }

        function SMSLockStartTimer() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSLockTimeInSeconds);
            //var timer2 = "5:01";
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    //$('.countdown').html('Your Can Request Now.');
                    $('#countdownLabelText').html('You Can Request Now.<br/>');
                    $('#countDownTime').html('');
                    $('#countDownTime').addClass('hide');
                    $('#btnPAC').removeAttr('disabled');
                    $('#hdnMobilePinRequested').val(0);
                }
                else {
                    $('#countdownLabelText').html('You can request new OTP in ');
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#countDownTime').html(hours + ':' + minutes + ':' + seconds + '<br/>');
                    $('#countDownTime').removeClass('hide');
                    timer2 = hours + ':' + minutes + ':' + seconds;
                }
            }, 1000);
        }

        function SMSLockStartTimerTrans() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSLockTimeInSeconds);
            //var timer2 = "5:01";
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    //$('.countdown').html('Your Can Request Now.');
                    $('#countdownLabelTextTrans').html('You Can Request Now.<br/>');
                    $('#countDownTimeTrans').html('');
                    $('#countDownTimeTrans').addClass('hide');
                    $('#btnPACTrans').removeAttr('disabled');
                    $('#hdnMobilePinRequestedTrans').val(0);
                }
                else {
                    $('#countdownLabelTextTrans').html('You can request new OTP in ');
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#countDownTimeTrans').html(hours + ':' + minutes + ':' + seconds + '<br/>');
                    $('#countDownTimeTrans').removeClass('hide');
                    timer2 = hours + ':' + minutes + ':' + seconds;
                }
            }, 1000);
        }

        function SMSLockStartTimerBank() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSLockTimeInSeconds);
            //var timer2 = "5:01";
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    //$('.countdown').html('Your Can Request Now.');
                    $('#countdownLabelTextBank').html('You Can Request Now.<br/>');
                    $('#countDownTimeBank').html('');
                    $('#countDownTimeBank').addClass('hide');
                    //$('#requestNewPinLinkDiv').removeClass('hide');
                    //$('#btnRequestNewPin').removeAttr('disabled');
                    $('#btnPACBank').removeAttr('disabled');
                    $('#hdnMobilePinRequestedBank').val(0);
                }
                else {
                    $('#countdownLabelTextBank').html('You can request new OTP in ');
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#countDownTimeBank').html(hours + ':' + minutes + ':' + seconds + '<br/>');
                    $('#countDownTimeBank').removeClass('hide');
                    timer2 = hours + ':' + minutes + ':' + seconds;
                }
            }, 1000);
        }

        function SMSLockStartTimerGoogle() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSLockTimeInSeconds);
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    $('#countdownLabelTextGoogle').html('You Can Request Now.<br/>');
                    $('#countDownTimeGoogle').html('');
                    $('#countDownTimeGoogle').addClass('hide');
                    $('#btnRequest').removeAttr('disabled');
                    $('#hdnMobilePinRequestedGoogle').val(0);
                }
                else {
                    $('#countdownLabelTextGoogle').html('You can request new OTP in ');
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#countDownTimeGoogle').html(hours + ':' + minutes + ':' + seconds + '<br/>');
                    $('#countDownTimeGoogle').removeClass('hide');
                    timer2 = hours + ':' + minutes + ':' + seconds;
                }
            }, 1000);
        }
        $(document).ready(function () {
            $('#ddlCountry').change(function () {

            });
            var hdnMobilePinRequested = parseInt($('#hdnMobilePinRequested').val());
            var hdnMobilePinRequestedTrans = parseInt($('#hdnMobilePinRequestedTrans').val());
            var hdnMobilePinRequestedBank = parseInt($('#hdnMobilePinRequestedBank').val());

            if (hdnMobilePinRequested == 1) {
                $('#btnPAC').attr('disabled', 'disabled');
                $('#countdownLabelText').html('Your OTP expires in');
                StartTimer();
            }
            else {
                $('#btnPAC').removeAttr('disabled');
            }

            if (hdnMobilePinRequestedTrans == 1) {
                $('#btnPACTrans').attr('disabled', 'disabled');
                $('#countdownLabelTextTrans').html('Your OTP expires in');
                StartTimerTrans();
            }
            else {
                $('#btnPACTrans').removeAttr('disabled');
            }

            if (hdnMobilePinRequestedBank == 1) {
                $('#btnPACBank').attr('disabled', 'disabled');
                $('#countdownLabelTextBank').html('Your OTP expires in');
                StartTimerBank();
            }
            else {
                $('#btnPACBank').removeAttr('disabled');
            }

            if (hdnMobilePinRequestedGoogle == 1) {
                $('#btnRequest').attr('disabled', 'disabled');
                $('#countdownLabelTextGoogle').html('Your OTP expires in');
                StartTimerGoogle();
            }
            else {
                $('#btnRequest').removeAttr('disabled');
            }

            $('#btnPAC').click(function () {
                $('#lblInfo').html("");
                var isValid = true;
                var message = "";

                $('#txtOldPassword,#txtNewPassword,#txtConfirmNewPassword').each(function () {
                    if ($.trim($(this).val()) == '') {
                        $(this).css({
                            "borderBottom": "1px solid red",
                            //"background": "#FFCECE"
                        });
                        isValid = false;
                    }
                    else {
                        $(this).css({
                            "borderBottom": "",
                            //"background": "LightBlue"
                        });
                    }
                });
                if (!isValid) {
                    return false;
                }
                if ($('#txtNewPassword').val() != $('#txtConfirmNewPassword').val()) {
                    message += "New Password and Confirm New Password must be the same.<br/>";
                    isValid = false;
                }

                if (isValid == false) {
                    ShowCustomMessage('Alert', message, '');
                    return false;
                }
                else {

                }

                $('#txtPacNo').attr('readonly', false);

                if (hdnMobilePinRequested == 0) {
                    $.ajax({
                        url: "UserSettings.aspx/PhoneVerify",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { title: JSON.stringify("LOGIN P/W UPDATE"), password: JSON.stringify(document.getElementById("txtOldPassword").value) },
                        success: function (data) {
                            var jsons = data.d.split(',');
                            var json = jsons[0];
                            //console.log(json);
                            $('#hdnMobilePinRequested').val(1);
                            $('#txtPacNo').val('');
                            if (json == "sent") {
                                $('#lblInfo').html("OTP sent to mobile number " + jsons[1] + " successfully.");
                                $('#countdownLabelText').html('Please Enter your OTP in');
                                $('#btnPAC').attr('disabled', 'disabled');
                                StartTimer();
                            }
                            else if (json == "already sent") {
                                $('#lblInfo').html("OTP already sent to mobile number " + jsons[1] + " .");
                                $('#btnPAC').attr('disabled', 'disabled');
                                StartTimer();
                            }
                            else if (json == "sms locked") {
                                $('#lblInfo').html("Too many attempts. Please try later.");
                                $('#countdownLabelText').html('You can request new OTP in ');
                                //$('#requestNewPinLinkDiv').addClass('hide');
                                //$('#btnRequestNewPin').attr('disabled', 'disabled');
                                $('#btnPAC').attr('disabled', 'disabled');
                                $('#hdnMobilePinRequested').val(0);
                                SMSLockStartTimer();
                            }
                            else if (json == 'Wrong password') {
                                ShowCustomMessage('Alert', 'Wrong password', '');
                            }

                            else {
                                $('#lblInfo').html("No Account Registered with this Mobile Number.");
                            }

                        }
                    });
                }
                else {

                }

            });
            $('#btnPACTrans').click(function () {
                $('#lblInfoTrans').html("");
                var isValid = true;
                var message = "";

                $('#txtPassword,#txtNewTransactionPassword,#txtConfirmTransactionPassword').each(function () {
                    if ($.trim($(this).val()) == '') {
                        $(this).css({
                            "borderBottom": "1px solid red",
                            //"background": "#FFCECE"
                        });
                        isValid = false;
                    }
                    else {
                        $(this).css({
                            "borderBottom": "",
                            //"background": "LightBlue"
                        });
                    }
                });
                if (!isValid) {
                    return false;
                }
                if ($('#txtNewTransactionPassword').val() != $('#txtConfirmTransactionPassword').val()) {
                    message += "New Password and Confirm New Password must be the same.<br/>";
                    isValid = false;
                }

                if (isValid == false) {
                    ShowCustomMessage('Alert', message, '');
                    return false;
                }

                $('#txtPacNoTrans').attr('readonly', false);
                if (hdnMobilePinRequestedTrans == 0) {
                    $.ajax({
                        url: "UserSettings.aspx/PhoneVerify",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { title: JSON.stringify("TRANSACTION P/W UPDATE"), password: JSON.stringify(document.getElementById("txtPassword").value) },
                        success: function (data) {
                            var jsons = data.d.split(',');
                            var json = jsons[0];
                            //console.log(json);
                            $('#hdnMobilePinRequestedTrans').val(1);
                            $('#txtPacNoTrans').val('');
                            if (json == "sent") {
                                $('#lblInfoTrans').html("OTP sent to mobile number " + jsons[1] + " successfully.");
                                $('#countdownLabelTextTrans').html('Please Enter your OTP in');
                                $('#btnPACTrans').attr('disabled', 'disabled');
                                StartTimerTrans();
                            }
                            else if (json == "already sent") {
                                $('#lblInfoTrans').html("OTP already sent to mobile number " + jsons[1] + " .");
                                $('#btnPACTrans').attr('disabled', 'disabled');
                                StartTimerTrans();
                            }
                            else if (json == "sms locked") {
                                $('#lblInfoTrans').html("Too many attempts. Please try later.");
                                $('#countdownLabelTextTrans').html('You can request new OTP in ');
                                $('#btnPACTrans').attr('disabled', 'disabled');
                                $('#hdnMobilePinRequestedTrans').val(0);
                                SMSLockStartTimerTrans();
                            }
                            else if (json == 'Wrong password') {
                                ShowCustomMessage('Alert', 'Wrong password', '');
                            }
                            else {
                                $('#lblInfoTrans').html("No Account Registered with this Mobile Number.");
                            }

                        }
                    });
                }
                else {

                }
            });
            $('#btnPACBank').click(function () {
                var isValid = true;
                if ($('#ddlBank').val() == '' || $('#ddlBank').val() == '0') {
                    $('#ddlBank').css({
                        "borderBottom": "1px solid red",
                    });
                    //ShowCustomMessage('Alert', 'Please Select bank', '');
                    isValid = false;
                }
                else {
                    $('#ddlBank').css({
                        "borderBottom": "",
                    });
                }
                if (validation('txtAccountNumber') == false) {
                    $('#txtAccountNumber').css({
                        "borderBottom": "1px solid red",
                    });
                    //ShowCustomMessage('Alert', 'Please enter Account Number', '');
                    isValid = false;
                }
                else {
                    $('#txtAccountNumber').css({
                        "borderBottom": "",
                    });
                    //var accountLength = $('#txtAccountNumber').val().length;
                    //if ($('option:selected', '#ddlBank').attr('accountnumlength') != undefined) {
                    //    var bankcount = parseInt($('option:selected', '#ddlBank').attr('accountnumlength'));
                    //    if (accountLength != bankcount) {
                    //        $('#txtAccountNumber').css({
                    //            "borderBottom": "1px solid red",
                    //        });
                    //        ShowCustomMessage('Alert', 'Account number Invalid! Length must be ' + bankcount + '.', '');
                    //        isValid = false;
                    //    }
                    //    else {
                    //        $('#txtAccountNumber').css({
                    //            "borderBottom": "",
                    //        });
                    //    }
                    //}
                }
                if ($('#FileUpload1').val() == "") {
                    $('#FileUpload1').css({
                        "borderBottom": "1px solid red",
                    });
                    //ShowCustomMessage('Alert', 'Please choose file', '');
                    isValid = false;
                }
                else {
                    $('#FileUpload1').css({
                        "borderBottom": "",
                    });
                }

                if (isValid == false) {

                    return false;
                }
                $('#txtPacNoBank').attr('readonly', false);
                if (hdnMobilePinRequestedBank == 0) {
                    $('#txtPacNoBank').css({
                        "borderBottom": "",
                    });
                    $.ajax({
                        url: "UserSettings.aspx/PhoneVerify",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { title: JSON.stringify("ADD BANK DETAILS"), password: JSON.stringify("") },
                        success: function (data) {
                            var jsons = data.d.split(',');
                            var json = jsons[0];
                            //console.log(json);
                            $('#hdnMobilePinRequestedBank').val(1);
                            if (json == "sent") {
                                $('#lblInfoBank').html("OTP sent to mobile number " + jsons[1] + " successfully.");
                                $('#countdownLabelTextBank').html('Please Enter your OTP in');
                                $('#btnPACBank').attr('disabled', 'disabled');
                                StartTimerBank();
                            }
                            else if (json == "already sent") {
                                $('#lblInfoBank').html("OTP already sent to mobile number " + jsons[1] + " .");
                                $('#btnPACBank').attr('disabled', 'disabled');
                                StartTimerBank();
                            }
                            else if (json == "sms locked") {
                                $('#lblInfoBank').html("Too many attempts. Please try later.");
                                $('#countdownLabelTextBank').html('You can request new OTP in ');
                                //$('#requestNewPinLinkDiv').addClass('hide');
                                //$('#btnRequestNewPin').attr('disabled', 'disabled');
                                $('#btnPACBank').attr('disabled', 'disabled');
                                $('#hdnMobilePinRequestedBank').val(0);
                                SMSLockStartTimerBank();
                            }
                            else {
                                $('#lblInfoBank').html("No Account Registered with this Mobile Number.");
                            }

                        }
                    });
                }
                else {

                }
            });
            $('#btnRequest').click(function () {

                $('#txtOTP').prop('readonly', false);
                if (hdnMobilePinRequestedBank == 0) {
                    $.ajax({
                        url: "UserSettings.aspx/PhoneVerify",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { title: JSON.stringify("GOOGLE AUTH"), password: JSON.stringify("") },
                        success: function (data) {
                            var jsons = data.d.split(',');
                            var json = jsons[0];
                            //console.log(json);
                            $('#hdnMobilePinRequestedGoogle').val(1);
                            if (json == "sent") {
                                $('#lblGoogleAuthenticationInfo').html("OTP sent to mobile number " + jsons[1] + " successfully.");
                                $('#countdownLabelTextGoogle').html('Please Enter your OTP in');
                                $('#btnRequest').attr('disabled', 'disabled');
                                StartTimerGoogle();
                            }
                            else if (json == "already sent") {
                                $('#lblGoogleAuthenticationInfo').html("OTP already sent to mobile number " + jsons[1] + " .");
                                $('#btnRequest').attr('disabled', 'disabled');
                                StartTimerGoogle();
                            }
                            else if (json == "sms locked") {
                                $('#lblGoogleAuthenticationInfo').html("Too many attempts. Please try later.");
                                $('#countdownLabelTextGoogle').html('You can request new OTP in ');
                                $('#btnRequest').attr('disabled', 'disabled');
                                $('#hdnMobilePinRequestedGoogle').val(0);
                                SMSLockStartTimerGoogle();
                            }
                            else if (json == "se") {
                                ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');
                            }
                            else {
                                $('#lblGoogleAuthenticationInfo').html("No Account Registered with this Mobile Number.");
                            }

                        }
                    });
                }
            });
            $('#loginPassword').on('shown.bs.modal', function () {
                $('#txtOldPassword').focus();
            });
            $('#transPassword').on('shown.bs.modal', function () {
                $('#txtPassword').focus();
            });
        });
    </script>



    <script>
        function CheckNumeric(e) {
            if (window.event) // IE 
            {
                if ((e.keyCode < 48 || e.keyCode > 57) & e.keyCode != 8) {
                    event.returnValue = false;
                    return false;
                }
            }
            else { // Fire Fox
                if ((e.which < 48 || e.which > 57) & e.which != 8) {
                    e.preventDefault();
                    return false;
                }
            }
        }
    </script>
</asp:Content>
