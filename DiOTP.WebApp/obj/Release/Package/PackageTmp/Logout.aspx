﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="Logout.aspx.cs" Inherits="DiOTP.WebApp.Logout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="logoutmessage" id="securityLogout" runat="server" visible="false">
                                <h4>Security Logout</h4>
                                <p>Your previous session is not logged out. The system has reset your session. Please try to login again.</p>
                                <p>Please be reminded that as an added security measure, you should clean your cache after each internet transacted session.</p>
                                <a href="/Login.aspx" class="hidden-md hidden-lg">Login Here</a>
                                <a href="/Index.aspx" class="hidden-sm hidden-xs">Login Here</a>
                            </div>

                            <div class="logoutmessage" id="SuccessLogout" runat="server" visible="false">
                                <h4>Logout Successfully</h4>
                                <p>You have successfully logged out from eApexIs website.</p>
                                <p>Please be reminded that as an added security measure, you should clean your cache after each internet transacted session.</p>
                                <a href="/Login.aspx" class="hidden-md hidden-lg">Login Here</a>
                                <a href="/Index.aspx" class="hidden-sm hidden-xs">Login Here</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
    <script>
        sessionStorage.clear();
    </script>
</asp:Content>
