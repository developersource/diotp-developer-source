﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="Funds-listing.aspx.cs" Inherits="DiOTP.WebApp.Funds_listing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/fixedheader/3.1.3/css/fixedHeader.bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <%--<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx" id="homeLink" runat="server">Home</a></li>
                            <li> Fund Center</li>
                        </ol>
                    </div>
                    <%--<div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head"> Fund Center</h3>
                    </div>--%>


                    <div class="tabs-horizontal my-tabs">
                        <ul class="nav nav-tabs my-nav-tabs">
                            <li class="active">
                                <a href="#all" data-toggle="tab">All</a>
                            </li>
                            <li>
                                <a href="#conventional" data-toggle="tab"><span class="hidden-xs text-uppercase">Equity Fund</span> <span class="hidden-md hidden-lg hidden-sm">EE</span></a>
                            </li>
                            <li>
                                <a href="#shariah" data-toggle="tab"><span class="hidden-xs text-uppercase">Mixed Asset</span> <span class="hidden-md hidden-lg hidden-sm">MA</span> </a>
                            </li>
                            <li>
                                <a href="#shariahMoney" data-toggle="tab"><span class="hidden-xs text-uppercase">Money Market </span><span class="hidden-md hidden-lg hidden-sm">MM</span></a>
                            </li>

                        </ul>


                        <div class="tab-content">
                            <div class="loadingChartDiv">
                                <div class="typing_loader"></div>
                                <div class="text-center">Loading...</div>
                            </div>

                            <div class="table-responsive tab-pane fade active in" id="all">
                                <table id="fundListTable" class="fundListTable table nowrap table-striped compact table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="fs-14">
                                            <th>No.</th>
                                            <th>Name</th>
                                            <th>Unit Price</th>
                                            <th>Change</th>
                                            <th>Change %</th>
                                            <th>Currency</th>
                                            <th style="width: 200px !important">Category</th>
                                            <th>NAV Date </th>
                                            <th class="hide">Fund Class</th>
                                            <th class="hide">IsRetail </th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="fundListTableBody" runat="server">
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="11" class="text-right">
                                                <img src='/Content/MyImage/12.png' height='20' />
                                                - Wholesale Fund</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <div class="table-responsive tab-pane fade" id="conventional">
                                <table id="fundListTableEE" class="fundListTable table-striped table nowrap compact table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="fs-14">
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Unit Price</th>
                                            <th>Change</th>
                                            <th>Change %</th>
                                            <th>Currency</th>
                                            <th style="width: 200px !important">Category</th>
                                            <th>NAV Date </th>
                                            <th class="hide">Fund Class</th>
                                            <th class="hide">IsRetail </th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="11" class="text-right">
                                                <img src='/Content/MyImage/12.png' height='20' />
                                                - Wholesale Fund</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <div class="table-responsive tab-pane fade" id="shariah">
                                <table id="fundListTableMA" class="fundListTable table table-striped nowrap compact table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="fs-14">
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Unit Price</th>
                                            <th>Change</th>
                                            <th>Change %</th>
                                            <th>Currency</th>
                                            <th style="width: 200px !important">Category</th>
                                            <th>NAV Date </th>
                                            <th class="hide">Fund Class</th>
                                            <th class="hide">IsRetail </th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="11" class="text-right">
                                                <img src='/Content/MyImage/12.png' height='20' />
                                                - Wholesale Fund</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <div class="table-responsive stab-pane fade" id="shariahMoney">
                                <table id="fundListTableMM" class="fundListTable table table-striped nowrap compact table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="fs-14">
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Unit Price</th>
                                            <th>Change</th>
                                            <th>Change %</th>
                                            <th>Currency</th>
                                            <th style="width: 200px !important">Category</th>
                                            <th>NAV Date </th>
                                            <th class="hide">Fund Class</th>
                                            <th class="hide">IsRetail </th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="11" class="text-right">
                                                <img src='/Content/MyImage/12.png' height='20' />
                                                - Wholesale Fund</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </section>


    <asp:HiddenField ID="hdnFundID" runat="server" ClientIDMode="Static" />
    <asp:Button ID="btnFundDetails" runat="server" OnClick="btnFundDetails_Click" ClientIDMode="Static" CssClass="hide" />


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="server">

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>

    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            var buttonCommon = {
                exportOptions: {
                    format: {
                        body: function (data, row, column, node) {
                            if (column === 1) {
                                var dataHTML = $.parseHTML(data);
                                var fundName = $($(dataHTML)[0]).html();
                                var fundFullname = $($(dataHTML)[6]).html();
                                var fundType = $($(dataHTML)[3]).html();
                                var Image = $($(dataHTML)[2]).html() == "" ? "" : $($($(dataHTML)[2]).html()).attr('src');
                                console.log($(dataHTML));
                                var returnString = fundName + " - " + fundFullname;
                                if (fundType != "")
                                    returnString = returnString + " (" + fundType + ")";
                                if (Image != "")
                                    returnString = returnString + " (Wholesale)";
                                return returnString;
                            }
                            if (column === 3 || column === 4) {
                                var dataHTML = $.parseHTML(data);
                                var value = $($(dataHTML)[0]).html();
                                return value;
                            }
                            return column === 1 ?
                                data.replace(/[$,]/g, '') :
                                data;
                        }
                    }
                }
            };


            var trs = $('#fundListTable tbody tr').clone();

            var dataTable = $('#fundListTable').DataTable({
                dom: '',
                //responsive: true,
                order: [],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        if (column.index() == 1) {
                            var col = $('<div />').addClass('col-md-3 col-sm-4 col-xs-12').appendTo($('.fund-search > .row'));
                            var select = $('<input type="text" placeholder="Fund Name" />').addClass('form-control')
                                .appendTo(col)
                                .on('keyup', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '' + val + '' : '', true, false)
                                        .draw();
                                });
                        }

                        if (column.index() == 5) {
                            var col = $('<div />').addClass('col-md-3 col-sm-4 col-xs-5').appendTo($('.fund-search > .row'));
                            var select = $('<select class="selectpicker"><option value="">Currency</option></select>').addClass('form-control')
                                .appendTo(col)
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '' + val + '' : '', true, false)
                                        .draw();
                                });

                            column.data().unique().sort().each(function (d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>')
                            });
                        }

                        if (column.index() == 8) {
                            var col = $('<div />').addClass('col-md-3 col-sm-4 col-xs-7').appendTo($('.fund-search > .row'));
                            var select = $('<input type="text" placeholder="Fund Category" />').addClass('form-control')
                                .appendTo(col)
                                .on('keyup', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '' + val + '' : '', true, false)
                                        .draw();
                                });

                            column.data().unique().sort().each(function (d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>')
                            });
                        }
                    });
                }

            });

            new $.fn.dataTable.Buttons(dataTable, {
                name: 'commands',
                buttons: [
                    $.extend(true, {}, buttonCommon, {
                        extend: 'print',
                        orientation: 'landscape',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7],
                            modifier: {
                                selected: null
                            }
                        },
                        className: 'btn my-btn',
                        title: "Funds List",
                        customize: function (win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                                .prepend(
                                '<div style="width:100%;"><img src="https://www.fundsupermart.com.my/main/admin/amg/MY/en/MYAPEX_logo.gif" style="position:absolute; top:20px; right:20px;" width="100" /></div>'
                                );

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }),
                    $.extend(true, {}, buttonCommon, {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7],
                            modifier: {
                                selected: null
                            }
                        },
                        className: 'btn my-btn',
                        title: "Funds List"
                    }),
                    $.extend(true, {}, buttonCommon, {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7],
                            modifier: {
                                selected: null
                            }
                        },
                        className: 'btn my-btn',
                        title: "Funds List"
                    }),
                    $.extend(true, {}, buttonCommon, {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-copy"></i>',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7],
                            modifier: {
                                selected: null
                            }
                        },
                        className: 'btn my-btn',
                        title: "Funds List"
                    }),
                    //'colvis'
                ]
            });
            var div = $('<div />').addClass("form-control").html('<span class="pull-left" style="font-size: 10px;line-height: 20px;">Export Options: </span>').appendTo($('<div />').addClass('col-md-3 text-right col-sm-4 col-xs-12').appendTo($('.fund-search > .row')));
            dataTable.buttons(0, null).container().appendTo(div);


            var newTrs = $.map(trs, function (obj) {
                return $($(obj).find('td')[8]).text() == 'EE' ? obj : null;
            });
            for (i = 0; i < newTrs.length; i++) {
                $(newTrs[i]).find('td').first().text(i + 1)
            }
            $('#fundListTableEE tbody').html(newTrs);


            newTrs = $.map(trs, function (obj) {
                return $($(obj).find('td')[8]).text() == 'MA' ? obj : null;
            });
            for (i = 0; i < newTrs.length; i++) {
                $(newTrs[i]).find('td').first().text(i + 1)
            }
            $('#fundListTableMA tbody').html(newTrs);


            newTrs = $.map(trs, function (obj) {
                return $($(obj).find('td')[8]).text() == 'MM' ? obj : null;
            });
            for (i = 0; i < newTrs.length; i++) {
                $(newTrs[i]).find('td').first().text(i + 1)
            }
            $('#fundListTableMM tbody').html(newTrs);


            //newTrs = $.map(trs, function (obj) {
            //    return $($(obj).find('td')[9]).text() == '0' ? obj : null;
            //});
            //for (i = 0; i < newTrs.length; i++) {
            //    $(newTrs[i]).find('td').first().text(i + 1)
            //}
            //$('#fundListTableWF tbody').html(newTrs);


            var isMApplied = 0;
            function ApplyM() {
                if (isMApplied == 0) {
                    $('#fundListTableMM').DataTable({
                        dom: '',
                        //responsive: true,
                        order: []
                    });
                    isMApplied = 1;
                }
            }
            $('[href="#shariahMoney"]').click(function () {
                setTimeout(ApplyM, 200);
            });

            var isAApplied = 0;
            function ApplyA() {
                if (isAApplied == 0) {
                    $('#fundListTableMA').DataTable({
                        dom: '',
                        //responsive: true,
                        order: []
                    });
                    isAApplied = 1;
                }
            }
            $('[href="#shariah"]').click(function () {
                setTimeout(ApplyA, 200);
            });

            var isCApplied = 0;
            function ApplyC() {
                if (isCApplied == 0) {
                    $('#fundListTableEE').DataTable({
                        dom: '',
                        //responsive: true,
                        order: []
                    });
                    isCApplied = 1;
                }
            }
            $('[href="#conventional"]').click(function () {
                setTimeout(ApplyC, 200);
            });

            //var isWApplied = 0;
            //function ApplyW() {
            //    if (isWApplied == 0) {
            //        $('#fundListTableWF').DataTable({
            //            dom: '',
            //            responsive: true,
            //            order: [],

            //        });
            //        isWApplied = 1;
            //    }
            //}
            //$('[href="#wholesale"]').click(function () {
            //    setTimeout(ApplyW, 200);
            //});

        });

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.loadingChartDiv').fadeOut();
            $('.fundListTable tr td:not(:first-child)').click(function (e) {
                $(this).parent('tr').find('td').first().click();
            });
        });
    </script>


</asp:Content>
