﻿<%@ Page Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="UploadDocuments.aspx.cs" Inherits="DiOTP.WebApp.UploadDocuments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        iframe#embedid-module {
            height: 620px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx">Home</a></li>
                            <li class="active">Upload Documents</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Upload Documents</h3>
                    </div>
                    <div class="row" id="truliooDoc" runat="server">
                        <div class="col-md-12">
                            <div id="trulioo-embedid"></div>
                        </div>
                    </div>
                    <div class="row" id="uploadDocs" runat="server">
                        <div class="col-md-6 col-md-offset-2">
                            <asp:FileUpload ID="additionalDocuments" runat="server" AllowMultiple="true" accept="image/*" ClientIDMode="Static" CssClass="form-control" />
                        </div>
                        <div class="col-md-2">
                            <asp:Button CssClass="btn btn-infos1" ID="btnSubmit" runat="server" ClientIDMode="Static" Text="Submit" OnClick="btnSubmit_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="server">
    <script type="text/javascript" src="https://js.trulioo.com/latest/main.js"></script>
    <script>
        function handleResponse(e) {
            var optionurl = window.location.pathname;
            var origin = window.location.origin;
            var href = window.location.href;
            var url = href.replace(origin, "");

            console.log('handleResponse', e);
            var ExperienceTransactionId = e.experienceTransactionId;
            var status = e.status;
            var transactionResult = e.transactionResult;
            if (transactionResult == "match") {
                var steps = e.steps;
                var result = steps[0].result;
                var transactionRecordId = steps[0].transactionRecordId;
                var stepName = steps[0].stepName;
                var transactionId = steps[0].transactionId;
                var timestamp = steps[0].timestamp;
                var inputFields = steps[0].inputFields;

                var DocumentType = $.grep(inputFields, function (obj, idx) {
                    return obj.FieldName == 'DocumentType';
                })[0];
                var DocumentFrontImage = $.grep(inputFields, function (obj, idx) {
                    return obj.FieldName == 'DocumentFrontImage';
                })[0];
                var DocumentBackImage = $.grep(inputFields, function (obj, idx) {
                    return obj.FieldName == 'DocumentBackImage';
                })[0];
                var LivePhoto = $.grep(inputFields, function (obj, idx) {
                    return obj.FieldName == 'LivePhoto';
                })[0];
                var FirstName = $.grep(inputFields, function (obj, idx) {
                    return obj.FieldName == 'FirstName';
                })[0];
                var LastName = $.grep(inputFields, function (obj, idx) {
                    return obj.FieldName == 'LastName';
                })[0];
                var FullName = $.grep(inputFields, function (obj, idx) {
                    return obj.FieldName == 'FullName';
                })[0];
                var CountryCode = $.grep(inputFields, function (obj, idx) {
                    return obj.FieldName == 'CountryCode';
                })[0];

                $.ajax({
                    url: "UploadDocuments.aspx/UpdateStatus",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { ExperienceTransactionId: JSON.stringify(ExperienceTransactionId), TransactionRecordId: JSON.stringify(transactionRecordId) },
                    success: function (data) {
                        if (data.d.IsSuccess) {

                            var res = data.d.Data;
                            if (res.Record.RecordStatus == "match") {
                                ShowCustomMessage('Alert', 'Verified. Please proceed.', '');

                            }
                            else {
                                ShowCustomMessage('Alert', 'Verification failed. Please try again.', url);
                            }
                        }
                        else {
                            ShowCustomMessage('Alert', data.d.Message, '');
                        }
                    }
                });

                ShowCustomMessage('Alert', 'Verified. Please proceed.', '');

                //$.ajax({
                //    url: "AccountOpening.aspx/GetTransactionResponse",
                //    contentType: 'application/json; charset=utf-8',
                //    type: "GET",
                //    dataType: "JSON",
                //    data: { ExperienceTransactionId: JSON.stringify(ExperienceTransactionId), TransactionRecordId: JSON.stringify(transactionRecordId) },
                //    success: function (data) {
                //        if (data.d.IsSuccess) {
                //            var res = data.d.Data;
                //            if (res.Record.RecordStatus == "match") {
                //                ShowCustomMessage('Alert', 'Verified. Please proceed.', '');

                //            }
                //            else {
                //                ShowCustomMessage('Alert', 'Verification failed. Please try again.', '/UploadDocuments.aspx');
                //            }
                //        }
                //        else {
                //            ShowCustomMessage('Alert', data.d.Message, '');
                //        }
                //    }
                //});
            }
            else if (transactionResult == "nomatch") {
                ShowCustomMessage('Alert', 'Verification failed. Please try again.', url);
            }
            else {
                ShowCustomMessage('Alert', 'Verification failed. ' + transactionResult, url);
            }

        }
        const publicKey = '687d8ca416e543f8aca654bd42158014'; // Public Key
        //const publicKeyIdentity = '5e1731839ed74cd48d98d7365798b1cb'; // Public Key
        const accessTokenURL = 'http://localhost:3010/trulioo-api/embedids/tokens';
        var identityTrulioo = new TruliooClient({
            publicKey,
            accessTokenURL,
            handleResponse,
        });
    </script>
</asp:Content>
