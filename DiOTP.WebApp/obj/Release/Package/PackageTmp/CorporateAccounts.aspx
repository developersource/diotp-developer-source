﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="CorporateAccounts.aspx.cs" Inherits="DiOTP.WebApp.CorporateAccounts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .bg-white {
            background-color: white;
        }
        #corporate p {
            margin-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="section image">
        <div class="inner">
            <div class="container">
                <div class="row" style="margin-top:10%;">
                    <div class="col-md-8 col-md-offset-2 text-center bg-white mt-30" id="two-button" style="padding:20px;">
                        <h3>Open an Account</h3>
                        <h4>Please select type of Investment Account</h4>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                <a class="btn btn-readmore" href="AccountOpening.aspx" style="color: rgba(5, 156, 206, 1); background-color: white; border-color: white;"><div><i class="fa fa-user-o fa-4x mb-10"></i></div>Individual Account</a>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 text-left">
                                <a class="btn btn-readmore" href="javascript:;" id="corporate-doc" style="color: rgba(5, 156, 206, 1); background-color: white; border-color: white;"><div><i class="fa fa-building-o fa-4x mb-10"></i></div>Corporate Account</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="hide" id="corporate">
                            <div class="row">
                                <div class="col-md-12 text-center bg-white mt-30" style="margin-bottom: 10%;">
                                    <h3>Open Corporate Account</h3>
                                    <p class="" style="text-align: center; font-size: 17px; line-height: 25px;">
                                        All Corporate Investors who wish to register with us, please download and complete all the forms.<br/>
                                        <p style="text-align: left; margin-left: 19%;">Kindly mail it to:
                                        <br/>
                                        <br/>
                                        Attn: Customer Service Department<br/>
                                        Apex Investment Services Berhad<br/>
                                        3rd Floor, Menara MBSB,<br/>
                                        46 Jalan Dungun,<br/>
                                        Damansara Heights,<br/>
                                        50490 Kuala Lumpur, Malaysia.<br/>
                                            </p>
                                        <%--For security and compliance purposes, all Corporate Investors who wish to register with <a href="https://eapexis.apexis.com.my/">www.eApexis.apexis.com.my</a> are required to complete the Corporate form and email us at <a href="mailto:customer.aisb@apexis.com.my">customer.aisb@apexis.com.my</a>.--%>

<%--Would you have any enquiries, please contact our Hotline 03-2095 9999 or email us at <a href="mailto:customer.aisb@apexis.com.my">customer.aisb@apexis.com.my</a>--%><br />
                                    </p>
                                    
                                    <div class="row">
                                        <div class="filesBody" id="filesBody" runat="server">
                                        <%--<div class="col-md-6">
                                            <a target="_blank" href="/Downloads/Corporate Documents/MASTERACCOUNT FORM.pdf" class="row">
                                                <i class="fa fa-file-text-o col-md-3" style="font-size: 50px;"></i>
                                                <div class="col-md-6 text-left">
                                                    <p>Master Account form</p>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a target="_blank" href="/Downloads/Corporate Documents/SAT2017_Non_individual.pdf" class="row">
                                                <i class="fa fa-file-text-o col-md-3" style="font-size: 50px;"></i>
                                                <div class="col-md-6 text-left">
                                                    <p>SAT Non-individual</p>

                                                </div>
                                                <div class="col-md-3"></div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a target="_blank" href="/Downloads/Corporate Documents/FATCA corporate Opening Form_final.pdf" class="row">
                                                <i class="fa fa-file-text-o col-md-3" style="font-size: 50px;"></i>
                                                <div class="col-md-6 text-left">
                                                    <p>FATCA corporate Opening Form</p>
                                                </div>
                                            </a>

                                        </div>
                                        <div class="col-md-6">
                                            <a target="_blank" href="/Downloads/Corporate Documents/CRS FORM- Controlling Person_latest.pdf" class="row">
                                                <i class="fa fa-file-text-o col-md-3" style="font-size: 50px;"></i>
                                                <div class="col-md-6 text-left">
                                                    <p>Common Reporting Standard (CRS) - Controlling Person</p>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a target="_blank" href="/Downloads/Corporate Documents/CRS FORM- Non- Individual_latest.pdf" class="row">
                                                <i class="fa fa-file-text-o col-md-3" style="font-size: 50px;"></i>
                                                <div class="col-md-6 text-left">
                                                    <p>Common Reporting Standard (CRS) - Non-Individual</p>

                                                </div>
                                            </a>
                                        </div>--%>
                                    </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <asp:HiddenField ID="hdnLocalIPAddress" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnLoginLocked" runat="server" Value="0" ClientIDMode="Static" />
    <%--<asp:HiddenField ID="hdnLoginLockTimeInSeconds" runat="server" Value="0" ClientIDMode="Static" />--%>
    <asp:HiddenField ID="hdnLoginAttempts" runat="server" Value="0" ClientIDMode="Static" />
</asp:Content>

<asp:Content ContentPlaceHolderID="modalPlace" ID="modalHolder" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="Scripts" runat="server">
    <script src='https://www.google.com/recaptcha/api.js' type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('#corporate-doc').click(function () {
                $('#corporate').removeClass('hide');
                $('#two-button').addClass('hide');
            });
        });
    </script>

</asp:Content>
