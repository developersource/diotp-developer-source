﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="SAT-Form.aspx.cs" Inherits="DiOTP.WebApp.SAT_Form" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <style>
        #accountSelectedDropdown {
            display: none;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper sat">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/Portfolio.aspx">eApexIs</a></li>
                    <li><a href="/Settings.aspx">Account Settings</a></li>
                    <li class="active">SAT Form</li>
                </ol>
            </div>
            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">SAT Form</h3>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-10 fs-13">
                    <div class="sat-h">
                        <span id="formNameSpan" runat="server">INVESTOR SUITABILITY ASSESSMENT FORM (INDIVIDUAL)</span>
                    </div>
                    <%--<p class="mb-2"><b>NOTE: </b>Please complete in <b>CAPITAL LETTERS</b> and <b>BLACK INK</b> only. Any alteration made must be countersigned.</p>--%>

                    <div class="sat1">
                        <p><b>THE INVESTOR SUITABILITY ASSESSMENT FORM (NOT APPLICABLE FOR ACCREDIATED INVESTOR) WILL GUIDE YOU IN CHOOSING THE UNLISTED CAPITAL MARKET PRODUCTS THAT SUITE YOUR INVESTMENT OBJECTIVES, RISK TOLERANCE, FINANCIAL PROFILE AND INVESTMENT EXPERIENCE. THE INFORMATION YOU PROVIDE WILL FORM THE BASIS OF OUR RECOMMENDATION. IT IS IMPORTANT TO PROVIDE ACCURATE AND COMPLETE INFORMATION TO ENSURE THAT SUITABLE UNLISTED CAPITAL MARKET PRODUCTS ARE RECOMMENDED ACCORDING TO YOUR INVESTMENT NEEDS AND OBJECTIVES.</b></p>
                        <p class="mb-0"><b>WARNING: </b>The recommendation is made based on the information obtained from the suitability. investors are advised to exercise judgement in making an informed decision in relation to the unlisted capital market product.</p>
                    </div>

                    <div class="row mb-10">
                        <div class="col-md-2">
                            Master Account Number:
                        </div>
                        <div class="col-md-6" id="fieldSet1Div" runat="server">
                            <%--<asp:TextBox ID="txtExisting" runat="server" CssClass="bottomborder"></asp:TextBox>--%>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">

                                <table class="table table-bordered table-condensed">
                                    <thead>
                                        <tr style="background: #eee;">
                                            <th colspan="4">Part 1: Investor's Details</th>
                                        </tr>
                                    </thead>
                                    <tbody id="fieldSet2Tbody" runat="server" clientidmode="static">
                                    </tbody>
                                    <%--<tr>
                                    <td>Date</td>
                                    <td>
                                        <asp:TextBox ID="txtDate" runat="server" CssClass="noborder"></asp:TextBox></td>
                                    <td>NRIC/Passport/Others</td>
                                    <td>
                                        <asp:TextBox ID="txtNPO" runat="server" CssClass="noborder"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Client's Name</td>
                                    <td colspan="3">
                                        <asp:TextBox ID="txtClientName" runat="server" CssClass="noborder"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Source of Income</td>
                                    <td colspan="3">
                                        <asp:CheckBoxList ID="Checkboxlist1" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" TextAlign="Right">
                                            <asp:ListItem Value="1">Employement</asp:ListItem>
                                            <asp:ListItem Value="2">Inheritance</asp:ListItem>
                                            <asp:ListItem Value="3">Business</asp:ListItem>
                                            <asp:ListItem Value="4">Others, please specify</asp:ListItem>
                                        </asp:CheckBoxList>
                                    </td>

                                </tr>--%>
                                </table>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed">
                                    <thead>
                                        <tr style="background: #eee;">
                                            <th colspan="3">Part 2: Category of Investor</th>
                                        </tr>
                                        <tr style="background: #eee;">
                                            <th>Category</th>
                                            <th>Investor</th>
                                            <th>Tick</th>
                                        </tr>
                                    </thead>
                                    <tbody id="fieldSet4Tbody" runat="server" clientidmode="static" class="checkboxlist">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed">
                                    <thead>
                                        <tr style="background: #eee;">
                                            <th colspan="2">Part 3: Investor's Need Analysis/ Risk Profiling</th>
                                            <th>Score</th>
                                        </tr>
                                    </thead>
                                    <tbody id="fieldSet3Tbody" runat="server" clientidmode="static">
                                        <%--<tr>
                                        <td style="width: 3% !important;">1.</td>
                                        <td>Your Current age:
                                        <br />
                                            <asp:CheckBoxList ID="Checkboxlist2" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" TextAlign="Right">
                                                <asp:ListItem Value="1">sample</asp:ListItem>
                                                <asp:ListItem Value="2">sample</asp:ListItem>
                                                <asp:ListItem Value="3">sample</asp:ListItem>
                                            </asp:CheckBoxList>
                                        </td>
                                        <td></td>
                                    </tr>--%>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2" style="text-align: right">Grand Total</td>
                                            <td id="scoreGrandTotal"></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive" style="overflow-x:auto;">
                                <table class="table table-bordered table-condensed" id="toleranceMatrixTable">
                                <thead style="background: #eee;">
                                    <tr>
                                        <th colspan="5">Part 4: Investor’s Risk Tolerance Matrix</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <tr>
                                        <td class="text-left" width="20%">Risk Profile Total score</td>
                                        <td width="20%">8 - 14</td>
                                        <td width="20%">15 - 21</td>
                                        <td width="20%">22 - 27</td>
                                        <td width="20%">28 and above</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Risk Tolerance</td>
                                        <td>Conservative</td>
                                        <td>Moderate</td>
                                        <td>Moderately Aggressive</td>
                                        <td>Aggressive</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tick (√)</td>
                                        <td><input type="checkbox" id="score1" disabled="disabled" /></td>
                                        <td><input type="checkbox" id="score2" disabled="disabled" /></td>
                                        <td><input type="checkbox" id="score3" disabled="disabled" /></td>
                                        <td><input type="checkbox" id="score4" disabled="disabled" /></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Category of fund that match with the risk level</td>
                                        <td>Apex Dana Al-Kanz
                                            <br />
                                            Apex Dana Aman
                                        </td>
                                         <td>Apex Dana Aslah
                                            <br />
                                            Apex Quantum Fund
                                            <br />
                                            Apex Dana Al-Faiz-I</td>
                                        <td>Apex Dana Al-Sofi-I</td>
                                        <td>Apex Dynamic Fund
                                            <br />
                                            Apex Malaysia Growth Trust
                                            <br />
                                            Apex Asian (Ex Japan) Fund</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Description</td>
                                        <td>This portfolio seeks to preserve capital and generate a regular income stream over time, with capital growth being of secondary importance. It is expected to be more stable in portfolio value compared to other more aggressive portfolios.</td>
                                        <td>This portfolio seeks to provide stable income with some potential for capital growth. Short-term fluctuation is expected in anticipation of a higher return.</td>
                                        <td>This portfolio aims to generate capital growth. A fair level of fluctuation is expected in return of possible higher returns with some level of income (if any).</td>
                                        <td>This portfolio aims to generate long-term capital growth. Significant fluctuations may be expected in short-term in anticipation of the highest possible return over the long-term.</td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-40">
                        <div class="col-md-4 col-md-offset-4 text-center">
                            <button type="button" class="btn btn-primary hide" id="btnEdit" runat="server" clientidmode="static">Edit</button>
                            <asp:Button ID="btnSubmit" runat="server" ClientIDMode="Static" OnClick="btnSubmit_Click" CssClass="btn btn-primary" Text="Submit" />
                            <%--<asp:Button ID="btnBack" runat="server" CausesValidation="false" ClientIDMode="Static" OnClick="btnBack_Click" CssClass="btn btn-primary" Text="Back" />--%>
                            <button type="button" id="btnBack" runat="server" class="btn btn-primary" onclick="SATBack();">Back</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnScoreGrandTotal" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnIsEdit" runat="server" ClientIDMode="Static" />
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script>

        function RedirectToUrl(url) {

            //var tcAcceptBuySession = sessionStorage['tcAcceptBuy'];
            //if (tcAcceptBuySession != undefined && tcAcceptBuySession != null && tcAcceptBuySession != "" && url.indexOf('BuyFunds.aspx') > 0) {
            //    url = url + "&isTermsModalPopup=0";
            //}
            //var tcAcceptSellSession = sessionStorage['tcAcceptSell'];
            //if (tcAcceptSellSession != undefined && tcAcceptSellSession != null && tcAcceptSellSession != "" && url.indexOf('SellFunds.aspx') > 0) {
            //    url = url + "&isTermsModalPopup=0";
            //}
            //var tcAcceptSwitchSession = sessionStorage['tcAcceptSwitch'];
            //if (tcAcceptSwitchSession != undefined && tcAcceptSwitchSession != null && tcAcceptSwitchSession != "" && url.indexOf('SwitchFunds.aspx') > 0) {
            //    url = url + "&isTermsModalPopup=0";
            //}
            window.location.href = url;
        }

        function isEven(n) {
            return n % 2 == 0;
        }
        function getSum(total, num) {
            return total + num;
        }

        function SATBack() {
            window.location.href = "Settings.aspx?AccountNo=" + $('#AccNo').val();
        }

        function ValidateForm() {
            var f2 = $('#fieldSet2Tbody .checkboxlist');
            var f2checked = $('#fieldSet2Tbody .checkboxlist input[type=checkbox]:checked');
            var f4 = $('#fieldSet4Tbody.checkboxlist');
            var f4checked = $('#fieldSet4Tbody.checkboxlist input[type=checkbox]:checked');
            var f3 = $('#fieldSet3Tbody .checkboxlist');
            var f3checked = $('#fieldSet3Tbody .checkboxlist input[type=checkbox]:checked');

            if ((f2.length == f2checked.length && f3.length == f3checked.length - 3) && f4checked.length == 1) {
                
                $('.checkboxlist').removeClass('text-danger');
                $('.checkboxlist').find('td').removeClass('text-danger');
                return 1;
                
            }
            else {
                
                $('.checkboxlist').each(function () {
                    if ($(this).find('input[type=checkbox]:checked').length == 0 || $(this).find('input[type=checkbox]').length > 5 && $(this).find('input[type=checkbox]:checked').length < 4) {
                        if ($(this).find('input[type=checkbox]').length > 5) {
                            if ($(this).find('td:nth-child(1) input[type=checkbox]:checked').length == 0) {
                                $(this).find('td:nth-child(1)').addClass('text-danger');
                            }
                            else {
                                $(this).find('td:nth-child(1)').removeClass('text-danger');
                            }
                            if ($(this).find('td:nth-child(2) input[type=checkbox]:checked').length == 0) {
                                $(this).find('td:nth-child(2)').addClass('text-danger');
                            }
                            else {
                                $(this).find('td:nth-child(2)').removeClass('text-danger');
                            }
                            if ($(this).find('td:nth-child(3) input[type=checkbox]:checked').length == 0) {
                                $(this).find('td:nth-child(3)').addClass('text-danger');
                            }
                            else {
                                $(this).find('td:nth-child(3)').removeClass('text-danger');
                            }
                            if ($(this).find('td:nth-child(4) input[type=checkbox]:checked').length == 0) {
                                $(this).find('td:nth-child(4)').addClass('text-danger');
                            }
                            else {
                                $(this).find('td:nth-child(4)').removeClass('text-danger');
                            }
                        }
                        else
                            $(this).addClass('text-danger');
                    }
                    else {
                        $(this).removeClass('text-danger');
                        $(this).find('td').removeClass('text-danger');
                    }
                });
                return 0;
            }
        }
        var firstValidate = 0;
        $(document).ready(function () {

            $('#Source_Others').insertAfter('#Source_3 + label');

            $('#btnEdit').click(function () {
                $('.checkboxlist input').prop('disabled',false);

                
                $('#btnSubmit').removeClass('hide');
                $('#btnEdit').addClass('hide');
                $("html, body").animate({ scrollTop: 0 }, "slow");
            });

            $('form').submit(function () {
                var isValid = ValidateForm();
                firstValidate = 1;
                if (isValid == 0) {
                    ShowCustomMessage('Alert', 'Please fill the required fields.', '');
                }
                else {
                    setTimeout(function () {
                        $('.loader-bg').fadeIn();
                    }, 500);
                }
                return isValid == 1 ? true : false;
            });

            $('#fieldSet2Tbody .checkboxlist input[type=checkbox]').click(function () {
                if ($(this).is(":checked")) {
                    $(this).parents('.checkboxlist').find('input[type=checkbox]').prop('checked',false);
                    $(this).prop('checked', true);
                    $('#Source_Others').attr("readonly","readonly");
                    if ($(this).next().text().indexOf("Other") > -1) {
                        $('#Source_Others').removeAttr("readonly");
                        var thisid = $(this).attr('id');
                        var txtid = thisid.split('_')[0];
                        $('input[id*=' + txtid + '][type=text]').focus();
                    }
                }
                if (firstValidate == 1)
                    ValidateForm();
            });
            $('#fieldSet4Tbody.checkboxlist input[type=checkbox]').click(function () {
                if ($(this).is(":checked")) {
                    $(this).parents('.checkboxlist').find('input[type=checkbox]').prop('checked',false);
                    $(this).prop('checked', true);
                    if ($(this).next().text().indexOf("Other") > -1) {
                        var thisid = $(this).attr('id');
                        var txtid = thisid.split('_')[0];
                        $('input[id*=' + txtid + '][type=text]').focus();
                    }
                }
                if (firstValidate == 1)
                    ValidateForm();
            });
            if ($("#fieldSet3Tbody .checkboxlist").first().find('input[type=checkbox]').length == 5) {
                var currentAgeCB = $("#fieldSet3Tbody .checkboxlist").first().find('input[type=checkbox]');
                $.each(currentAgeCB, function (idx, obj) {
                    if ($(obj).is(':checked')) {
                        var score = 0;
                        score = parseInt($(obj).val());
                        $(this).parents('.checkboxlist').parents('tr').find('td').last().html(score);
                        var scores = $.map($('#fieldSet3Tbody>tr'), function (obj, idx) {
                            return parseInt($(obj).children('td').last().text() == "" ? "0" : $(obj).children('td').last().text());
                        });
                        grandTotal = scores.reduce(getSum);
                        $('#toleranceMatrixTable input[type="checkbox"]').prop('checked',false);
                        if (grandTotal >= 8 && grandTotal <= 14) {
                            $('#score1').prop('checked', 'checked');
                        }
                        else if (grandTotal >= 15 && grandTotal <= 21) {
                            $('#score2').prop('checked', true);
                        }
                        else if (grandTotal >= 22 && grandTotal <= 27) {
                            $('#score3').prop('checked', true);
                        }
                        else if (grandTotal >= 28) {
                            $('#score4').prop('checked', true);
                        }
                        $('#scoreGrandTotal').text(grandTotal);
                        $('#hdnScoreGrandTotal').val(grandTotal);
                    }
                });
            }
            $('#fieldSet3Tbody .checkboxlist input[type=checkbox]').click(function () {
                if ($(this).parents('.checkboxlist').find('input[type=checkbox]').length > 5) {
                    var thisTd = $(this).parent('td');
                    var thisTr = $(this).parent('td').parent('tr');
                    var thisId = $(this).attr('id');
                    var index = $(thisTr).find(thisTd).index();
                    $(this).parents('table#Exp').find('tr').each(function (idxTR, objTR) {
                        console.log($(objTR).find('td:eq(' + index + ')').html());
                        if ($(objTR).find('td:eq(' + index + ') input[type=checkbox]').attr('id') != thisId)
                            $(objTR).find('td:eq(' + index + ') input[type=checkbox]').prop('checked',false);
                    });
                    //if ($(this).is(":checked")) {
                        
                        //$(this).prop('checked', true);
                        //if (isEven(index)) {
                        //    $(this).parents('.checkboxlist').find('#' + id + '_' + (index + 1)).prop('checked',false);
                        //    $(this).prop('checked', true);
                        //}
                        //else {
                        //    $(this).parents('.checkboxlist').find('#' + id + '_' + (index - 1)).prop('checked',false);
                        //    $(this).prop('checked', true);
                        //}
                    //}
                }
                else {
                    if ($(this).is(":checked")) {
                        $(this).parents('.checkboxlist').find('input[type=checkbox]').prop('checked',false);
                        $(this).prop('checked', true);
                    }
                }
                var score = 0;
                if ($(this).parents('.checkboxlist').find('input[type=checkbox]:checked').length == 1) {
                    score = parseInt($(this).parents('.checkboxlist').find('input[type=checkbox]:checked').val());
                }
                else if ($(this).parents('.checkboxlist').find('input[type=checkbox]:checked').length > 1) {
                    var inscores = $.map($(this).parents('.checkboxlist').find('input[type=checkbox]:checked'), function (obj, idx) {
                        return parseInt($(obj).val());
                    });
                    score = inscores.reduce(getSum);
                }
                $(this).parents('.checkboxlist').parents('tr').find('td').last().html(score);
                var scores = $.map($('#fieldSet3Tbody>tr'), function (obj, idx) {
                    return parseInt($(obj).children('td').last().text() == "" ? "0" : $(obj).children('td').last().text());
                });
                grandTotal = scores.reduce(getSum);

                $('#toleranceMatrixTable input[type="checkbox"]').prop('checked',false);
                if (grandTotal >= 8 && grandTotal <= 14) {
                    $('#score1').prop('checked', 'checked');
                }
                else if (grandTotal >= 15 && grandTotal <= 21) {
                    $('#score2').prop('checked', true);
                }
                else if (grandTotal >= 22 && grandTotal <= 27) {
                    $('#score3').prop('checked', true);
                }
                else if (grandTotal >= 28) {
                    $('#score4').prop('checked', true);
                }
                $('#scoreGrandTotal').text(grandTotal);
                $('#hdnScoreGrandTotal').val(grandTotal);
                if (firstValidate == 1)
                    ValidateForm();
            });
            var checkboxsChecked = $('#fieldSet3Tbody input[type=checkbox]:checked');
            checkboxsChecked.click();
            checkboxsChecked.click();

            if ($('#hdnIsEdit').val() == "1") {
                $('.checkboxlist input').attr('disabled', 'disabled');
                $('#Source_Others').attr('readonly', 'readonly');
                $('#btnSubmit').addClass('hide');
                $('#btnEdit').removeClass('hide');
            }

        });
    </script>
</asp:Content>
