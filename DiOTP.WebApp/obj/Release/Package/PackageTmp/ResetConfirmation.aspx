﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="ResetConfirmation.aspx.cs" Inherits="DiOTP.WebApp.ResetConfirmation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="reset-confirm">
                        <img src="Content/MyImage/Google-Authenticator-icon.png" alt="" height="100" />
                        <h2>Google Authenticator Reset Key successfully sent to your email.</h2>
                        <a href="/Portfolio.aspx" class="btn btn-reset">Continue to Google Verification</a>
                        <hr class="g-hr" />
                        <p style="text-align:left; margin-bottom:10px;">If you haven't received the email, follow the below instructions:</p>
                        <ol style="text-align: left;">
                            <li> Check spam or other folders.</li>
                            <li>Refresh the email accout.</li>
                            <li>Check whether you have given right email id or not.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
