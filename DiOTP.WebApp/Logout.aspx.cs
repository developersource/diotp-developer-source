﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class Logout : System.Web.UI.Page
    {
        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sessionlogout = Request.QueryString["sessionlogout"];
                string browserlogout = Request.QueryString["browserlogout"];

                if (sessionlogout != null)
                {
                    if (sessionlogout == "1")
                    {
                        if (Session["User"] != null)
                        {
                            User user = (User)Session["User"];
                            UserLogMain ulm = new UserLogMain()
                            {
                                Description = "Security logout successful.",
                                TableName = "users",
                                UpdatedDate = DateTime.Now,
                                UserId = user.Id,
                                UserAccountId = 0,
                                RefId = user.Id,
                                RefValue = user.Username,
                                StatusType = 1
                            };
                            Response responseLog = IUserLogMainService.PostData(ulm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            Session.Clear();
                            Session.Abandon();
                            securityLogout.Visible = true;
                        }
                        else
                            securityLogout.Visible = true;
                    }
                }
                else if (browserlogout != null)
                {
                    if (browserlogout == "1")
                    {
                        if (Session["User"] != null)
                        {
                            User user = (User)Session["User"];
                            UserLogMain ulm = new UserLogMain()
                            {
                                Description = "Browser closed.",
                                TableName = "users",
                                UpdatedDate = DateTime.Now,
                                UserId = user.Id,
                                UserAccountId = 0,
                                RefId = user.Id,
                                RefValue = user.Username,
                                StatusType = 1
                            };
                            Response responseLog = IUserLogMainService.PostData(ulm);
                            if (!responseLog.IsSuccess)
                            {
                                //Audit log failed
                            }
                            Session.Clear();
                            Session.Abandon();
                            securityLogout.Visible = true;
                        }
                        else
                            securityLogout.Visible = true;
                    }
                }
                else if (Session["User"] != null)
                {
                    User user = (User)Session["User"];
                    UserLogMain ulm = new UserLogMain()
                    {
                        Description = "Logout successful.",
                        TableName = "users",
                        UpdatedDate = DateTime.Now,
                        UserId = user.Id,
                        UserAccountId = 0,
                        RefId = user.Id,
                        RefValue = user.Username,
                        StatusType = 1
                    };
                    Response responseLog = IUserLogMainService.PostData(ulm);
                    if (!responseLog.IsSuccess)
                    {
                        //Audit log failed
                    }

                    Session.Clear();
                    Session.Abandon();
                    SuccessLogout.Visible = true;
                }
                else
                {
                    Session.Clear();
                    Session.Abandon();
                    if (Request.Browser.IsMobileDevice)
                    {
                        Response.Redirect("Login.aspx", false);
                    }
                    else
                    {
                        Response.Redirect("Index.aspx", false);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object BrowserClose()
        {
            Response response = new Response();
            Logger.WriteLog("BrowserClose start");
            if (HttpContext.Current.Session["User"] != null)
            {
                response.Message = "has-session";
                Logger.WriteLog("BrowserClose: User " + ((User)HttpContext.Current.Session["User"]).EmailId);
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.Abandon();
                Logger.WriteLog("BrowserClose end");
            }
            else
            {
                response.Message = "no-session";
            }
            return response;
        }

    }

}