﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="SellFunds.aspx.cs" Inherits="DiOTP.WebApp.SellFunds" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2">
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Portfolio.aspx">eApexIs</a></li>
                            <li class="active">Sell Funds</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Sell Fund</h3>
                    </div>
                </div>
            </div>
            <div style="margin-bottom: 5px; margin-left: 5px;">
                <img src="Content/MyImage/INFO.png" style="width: 15px; height: 15.25px; margin-right: 5px;" />
                <span style="font-weight: 600; color: #A0A0A0; font-size: 13px">You are currently viewing account</span>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4 mb-10" style="margin-left: 0; width: 35.33%;">
                    <asp:DropDownList CssClass="form-control mobile-select selectpicker-acc" ID="ddlUserAccountId" runat="server" AutoPostBack="true" ClientIDMode="Static" OnSelectedIndexChanged="ddlUserAccountId_SelectedIndexChanged"></asp:DropDownList>
                    <asp:HiddenField ID="isUserAccountChange" Value="false" runat="server" ClientIDMode="Static" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <%--<div class="selectedAccountDetails hide">
                        <div class="row">
                            <div class="col-md-5">
                                <strong>Account Name:</strong>
                                <span id="accName" runat="server"></span>
                            </div>
                            <div class="col-md-3">
                                <strong>Master Account Number:</strong>
                                <span id="maAccNumber" runat="server"></span>
                            </div>
                            <div class="col-md-3">
                                <strong>Account Plan:</strong>
                                <span id="accType" runat="server"></span>
                            </div>
                        </div>
                    </div>--%>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center form-box">
                    <div class="f1">
                        <div class="f1-steps">
                            <div class="f1-progress">
                                <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
                            </div>
                            <div class="f1-step active">
                                <div class="f1-step-icon"><i class="fa fa-universal-access"></i></div>
                                <p>Account Overview</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-money"></i></div>
                                <p>Fund Selection & Units</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-file-text-o"></i></div>
                                <p>Confirmation</p>
                            </div>
                        </div>
                        <small class="text-danger" id="errorMessage"></small>
                        <small class="text-success" id="successMessage"></small>
                        <fieldset class="fieldsetAccountFundsOverview">




                            <div class="row" id="accSummaryDiv">
                                <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                                    <table class="table table-cell-pad-5 table-font-size-13 table-condensed table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Fund</th>
                                                <th class="text-right">Total<br />
                                                    Units</th>
                                                <th class="text-right">NAV Price<br />
                                                    MYR</th>
                                                <th class="text-right">Market Value<br />
                                                    MYR</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbodyAccountFunds" runat="server" class="tbodySummary">
                                            <tr>
                                                <td colspan="4" class="text-center">You have no unit trust holdings at this moment. Start investing with us today by clicking 'Transaction > Buy'.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="f1-buttons">
                                        <button id="btnSummary" type="button" class="btn btn-next">Next</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="fieldsetfundSelectionAndAmount">

                            <div class="row mt-10">
                                <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Select Fund:</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:HiddenField ID="hdnFundId" runat="server" ClientIDMode="Static" />
                                                    <asp:DropDownList ID="ddlFundsList" runat="server" ClientIDMode="Static" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Redemption Units:</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:TextBox ID="txtUnits" runat="server" ClientIDMode="Static" placeholder="Enter redemption units" CssClass="form-control"></asp:TextBox>
                                                    <asp:CheckBox ID="chkSellAll" runat="server" ClientIDMode="Static" Text="Sell All" CssClass="checkbox-inline" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <a href="#" class="btn btn-infos1 btn-sm mb-20" data-toggle="modal" data-target="#orderHistory">Order History</a>
                                            <a href="javascript:;" id="btnAddFund" class="btn btn-infos1 btn-sm mb-20 pull-right">Add</a>
                                        </div>
                                    </div>


                                    <div class="form-group row">



                                        <%--<div class="row">
                                        <div class="col-md-3 col-md-offset-9">
                                            <a href="javascript:;" id="btnAddFund" class="btn btn-infos1 btn-sm btn-block mb-20">Add</a>
                                        </div>
                                    </div>--%>

                                        <div id="divFundDetails" class="hide">
                                            <div class="loadingDiv">
                                                <div class="typing_loader"></div>
                                                <div class="text-center">Loading...</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                        <tbody>
                                                            <tr>
                                                                <td>Fund Name</td>
                                                                <td class="fundName">Fund Name</td>
                                                            </tr>
                                                            <tr class="bg-info">
                                                                <td>Units Held</td>
                                                                <td class="unitholding">Unit Holding</td>
                                                            </tr>
                                                            <tr class="bg-info">
                                                                <td>Available Units</td>
                                                                <td class="aUnits">Available Units</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Fund - Risk Profile</td>
                                                                <td class="riskCriteria">Risk Profile</td>
                                                            </tr>
                                                            <tr>
                                                                <td id="minimumInitialInvestementTitle">Minimum Initial Investement (CASH/EPF)</td>
                                                                <td class="minimumInitialInvestement">Minimum Initial Investement</td>
                                                            </tr>
                                                            <tr>
                                                                <td id="minimumSubsequentInvestementTitle">Minimum Subsequent Investement (CASH/EPF)</td>
                                                                <td class="minimumSubsequentInvestement">Minimum Subsequent Investement</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Minimum Holding Units</td>
                                                                <td class="mhUnits">Minimum Holding Units</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Master Prospectus</td>
                                                                <td class="masterProspectus">Master Prospectus</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Product Highlights Sheet</td>
                                                                <td class="productHighlightsSheet">Product Highlights Sheet</td>
                                                            </tr>
                                                            <tr id="infoMemo">
                                                                <td>Information Memorandum</td>
                                                                <td class="informationMemorandum">Information Memorandum</td>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="2">Note: <strong>You must READ the Fund Master Prospectus / Prospectus / Information Memorandum and its Supplementary(ies) / Replacement (if any) and Product Highlights Sheet before you proceed to the next step.</strong>
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row hide" id="cart-table">
                                            <div class="col-md-12">
                                                <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                    <thead>
                                                        <tr>
                                                            <th>Redemption Fund</th>
                                                            <th class="text-right">Redemption Units</th>
                                                            <th class="text-right">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="tbodySelectedFunds" id="tbodySelectedFunds" runat="server" clientidmode="static"></tbody>
                                                    <%--<tfoot>
                                                        <tr>
                                                            <th class="text-right">Total</th>
                                                            <td class="unitFormatNoSymbol tdTotalSalesUnits text-right" id="tdTotalSalesUnits" runat="server" clientidmode="static"></td>
                                                            <td></td>
                                                        </tr>
                                                    </tfoot>--%>
                                                </table>

                                                <div class="form-group mb-5">
                                                    <asp:CheckBox ID="chkConfirmFundInformation" ClientIDMode="Static" runat="server" CssClass="checkbox-inline text-info text-bold" Text="I confirm that I have read and understood the Fund Prospectus/Information Memorandum and Product Highlights Sheet of" />
                                                </div>
                                                <div class="f1-buttons">
                                                    <button type="button" class="btn btn-previous">Previous</button>
                                                    <button type="button" class="btn btn-next">Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </fieldset>

                        <fieldset class="fieldsetConfirmation">
                            <div class="row">
                                <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                <thead>
                                                    <tr>
                                                        <th>Redemption Fund</th>
                                                        <th class="text-right">Redemption Units</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="tbodySelectedFunds" id="tbodySelectedFunds1" runat="server" clientidmode="static"></tbody>
                                                <%--<tfoot>
                                                    <tr>
                                                        <th class="text-right">Total</th>
                                                        <td class="unitFormatNoSymbol tdTotalSalesUnits text-right" id="tdTotalSalesUnits1" runat="server" clientidmode="static"></td>
                                                    </tr>
                                                </tfoot>--%>
                                            </table>
                                            <div class="row" id="bankAccountDll" clientidmode="static" runat="server">
                                                <div class="col-md-8">
                                                    <small><small>Bank Account</small></small>
                                                    <asp:DropDownList ID="ddlBankAccountsList" runat="server" CssClass="form-control" ClientIDMode="Static">
                                                        <asp:ListItem Value="" Text="Select Bank Account"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="f1-buttons">
                                                <button type="button" class="btn btn-previous">Previous</button>
                                                <asp:Button ID="btnSubmit" runat="server" ClientIDMode="Static" CssClass="btn btn-next btn-infos1" Text="Proceed" OnClick="btnSubmit_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <div class="text-center hide" id="loadAfterSubmitTransaction">
                            <h4>Please wait while we are processing your order...</h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <asp:HiddenField ID="hdnTotalQuantity" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnTotalAvailableQuantity" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnAccountPlan" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnAvailableUnits" runat="server" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
    <div class="modal fade" id="commonTermsPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="height: 92%;">
        <div class="modal-content trans">
            <div class="modal-body">
                <!-- style="height: calc(100vh - 250px); overflow-y: scroll;" -->
                <div id="commonTermsPopupModalBodyInner">
                    <h5 style="color: #059cce; font-size:16px;" class="text-justify">You are about to sell. Kindly make sure you have confirmed that:</h5>
                    <ul>
                        <li>you have read and agree to be bound by the terms and conditions set up on the facility;</li>
                        <li>you have read and fully understand the contents of the disclaimer, privacy policy, internet risk, transaction notice and below notice;</li>
                        <li>you are eligible to apply for the units in the fund(s), e.g. that you have attained 18 years of age unless otherwise allowed under the terms of the Master Prospectus / Prospectus / Information Memorandum and its Supplementary(ies) / Replacement;</li>
                        <li>you have given consent to Apex Investment Services Berhad (“AISB”) to disclose information pertaining to you, including but not limited to proprietary information, to the relevant entities involved in the fund(s) as well as to the Securities Commission Malaysia (“SC”).</li>
                    </ul>
                    <p>Neither AISB nor any of its directors, consultants, representatives or employees accept any liability whatsoever for any direct, indirect or consequences losses (in contract, tort or otherwise) arising from this transaction or its contents contained herein, except to the extent this would be prohibited by law or regulation.</p>
                    <p>You may wish to consider switching instead of redemption. For redemption, you may lose your loaded units and incur service charge when you invest again in a fund(s).</p>
                    <h5>NOTICE</h5>
                    <p>Apex Investment Services Berhad (“AISB”) is responsible for the issuance, circulation or dissemination of the electronic Master Prospectus / Prospectus, electronic Supplementary(ies) / Replacement Master Prospectus / Prospectus, electronic Information Memorandum, electronic Supplementary(ies) / Replacement Information Memorandum, electronic Product Highlights Sheet (collectively referred to as the “Disclosure Document”) and electronic application form. A copy of the Disclosure Document has been registered or lodged with the Securities Commission Malaysia (“SC”). The SC takes no responsibility for the contents and makes no representation on the accuracy or completeness of the Disclosure Document.</p>
                    <p>Investors are advised to read and understand the content of the Disclosure Document before completing the relevant application forms and making any investment decision. A copy of the Disclosure Document is available and investors have the right to request for it. The Disclosure Document can be obtained from our business office, our authorised distributors, our consultants or our representatives. You should also consider the fees and charges involved before investing. All fees and expenses incurred by the Fund is subject to any applicable taxes and / or duties as may be imposed by the government or other authorities from time to time. Prices of units and distributions payable, if any, may go down or up, and past performance of the Fund is not an indication of its future performance. Investors may not get back to the original cost incurred for the investment. Where a unit split / distribution is declared, the issue of additional units / distribution, the NAV per unit will be reduced from pre-unit split NAV / cum-distribution NAV to post-unit split NAV / ex-distribution NAV. Where a unit split is declared, the value of your investment in Malaysian Ringgit will remain unchanged after the distribution of the additional units. Where unit trust loan financing is available, investors are advised to read and understand the contents of the unit trust loan financing risk disclosure statement before deciding to borrow to purchase units. Investors should be aware of the specific risks for the Fund before investing and rely on their own evaluation to assess the merits and risks of the investment. Specific risks and general risks for the Fund is elaborated in the Disclosure Document. Units are issued upon receipt of duly completed relevant application forms referred to and accompanying a copy of the Disclosure Document. The Fund may not be suitable for all and if in doubt, investors should seek independent advice.</p>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-lg-8 col-md-7 text-left">
                        <label class="customcheck">
                            By clicking on the 'Accept' button, I confirm that I have read and accept the above Policy Statement and I wish to proceed.
                    <input type="checkbox" id="chkSellProceed">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="col-lg-4 col-md-5">
                        <asp:Button ID="btnAcceptTerms" runat="server" CssClass="btn btn-infos1" Text="Accept" OnClick="btnAcceptTerms_Click" ClientIDMode="Static" OnClientClick="return OnAcceptTerms()" />
                        <button type="button" class="btn btn-infos1-default hide" id="btnDismissPopupSell" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-infos1-default" data-dismiss="modal" onclick="javascript:window.location.href='Portfolio.aspx'">Decline</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <button id="editFundModal" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="display: none">Open Modal</button>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Units</h4>
                </div>
                <div class="modal-body">
                    <input type="text" id="FundAmount" name="FundAmount" class="form-control" placeholder="Enter your units amount here." autocomplete="off" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="FundUpdate">Update</button>
                    <button type="button" class="btn btn-default" id="FundUpdateClose" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <button id="deleteFundModal" type="button" class="btn btn-info" data-toggle="modal" data-target="#deleteModal" style="display: none">Open Modal</button>

    <div class="modal fade" id="deleteModal" role="document">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Confirmation</h4>
                </div>
                <div class="modal-body">
                    <span>
                        Are you sure you would like to delete this order from your cart?
                    </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="ConfirmationYes">Confirm</button>
                    <button type="button" class="btn btn-secondary" id="ConfirmationCancel" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <%--Order History--%>
    <div class="modal fade" id="orderHistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog sett two">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h4>Order history</h4>
                </div>
                <%--<div class="modal-body" style="height: calc(100vh - 250px); overflow-y: scroll;">--%>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="border: none;">
                                    <table class="table table-cell-pad-5 table-font-size-13 table-condensed table-bordered OrderHistoryTable">
                                        <thead id="theadUserOrders" runat="server">
                                            <tr>
                                                <th style="width: 50px"></th>
                                                <th style="width: 50px">S. No</th>
                                                <th>Account No</th>
                                                <th>Order No</th>
                                                <th>Order Type</th>
                                                <th>Order Date</th>
                                                <th>Order Status</th>
                                                <th class="text-right">Investment Amount(MYR)</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbodyUserOrders" runat="server" clientidmode="Static">
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-infos1" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <%--Order History End--%>
    <asp:HiddenField ID="hdnTermsPop" runat="server" ClientIDMode="Static" Value="1" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/4.1.0/autoNumeric.js"></script>
    <script type="text/javascript">

        var hdnTermsPop = "0";
        (function ($) {
            hdnTermsPop = $('#hdnTermsPop').val();
            $.fn.inputFilter = function (inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
            };
        }(jQuery));

        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });

        $(document).ready(function () {
            $('#errorMessage').addClass('hide');
            $('#errorMessage').html('');
            $('#successMessage').html('');
            $('#btnSummary').show();
            $('#ddlUserAccountId').change(function () {
                $('#errorMessage').html('');
                $('#successMessage').html('');
                $('#isUserAccountChange').val('true');
                $('#txtUnits').val('');

                $('#chkSellAll').prop('checked', false);
                if ($('.tbodySummary tr').length == 0) {
                    $('#btnSummary').hide();
                }

            });

            $('.show-extended-row').click(function () {
                if ($(this).find('i').hasClass('fa-plus')) {
                    $('.show-extended-row i.fa-minus').each(function (idx, obj) {
                        var ele = $(obj).parents('.show-extended-row');
                        var extendedTRAll = $(ele).parents('tr').next();
                        var extendedRowAll = $(ele).parents('tr').next().find('.extended-row');
                        $(extendedRowAll).slideUp(500, function () {
                            $(extendedTRAll).addClass('hide');
                        });
                        $(ele).find('i').addClass('fa-plus');
                        $(ele).find('i').removeClass('fa-minus');
                    });
                }
                var extendedTR = $(this).parents('tr').next();
                var extendedRow = $(this).parents('tr').next().find('.extended-row');
                if ($(this).find('i').hasClass('fa-plus')) {
                    $(extendedTR).removeClass('hide');
                    $(extendedRow).slideDown();
                    $(this).find('i').removeClass('fa-plus');
                    $(this).find('i').addClass('fa-minus');
                }
                else {
                    $(extendedRow).slideUp(500, function () {
                        $(extendedTR).addClass('hide');
                    });
                    $(this).find('i').removeClass('fa-minus');
                    $(this).find('i').addClass('fa-plus');
                }
            });

            $('#commonTermsPopup .modal-body').scroll(function () {
                var disable =
                    (
                        $('#commonTermsPopupModalBodyInner').height() != parseInt($(this).scrollTop()) + $(this).height() - 32
                    )
                    &&
                    (
                        $('#commonTermsPopupModalBodyInner').height() - 1 != parseInt($(this).scrollTop()) + $(this).height() - 32
                    );
                $('#chkSellProceed').prop('checked', false);
                $('#chkSellProceed').prop('disabled', disable);
                $('#btnAcceptTerms').prop('disabled', disable);
            });
            //var tcAcceptSellSession = sessionStorage['tcAcceptSell'];
            //if (tcAcceptSellSession != undefined && tcAcceptSellSession != null && tcAcceptSellSession != "") {
            //    $('#btnDismissPopupSell').click();
            //}
            //$('#btnAcceptSell').click(function () {
            //    if ($('#chkSellProceed').is(':checked')) {
            //        $('#btnDismissPopupSell').click();
            //        var tcAcceptSell = 1;
            //        sessionStorage['tcAcceptSell'] = tcAcceptSell;
            //        $('.loader-bg').fadeIn();
            //        window.location.href = "SellFunds.aspx?isTermsModalPopup=0";
            //    }
            //    else {
            //        ShowCustomMessage('Alert', 'Please Read, Understand and Agree to proceed.', '');
            //    }
            //});
        });
        //function OpenCommonTermsPopup() {
        //    $('#commonTermsPopup').modal({
        //        keyboard: false,
        //        backdrop: "static",
        //    });
        //}
        var unitsAvailable = 0;
        function OpenCommonTermsPopup() {
            if ($('#ddlUserAccountId').val() != '') {

                $('#accSummaryDiv, .selectedAccountDetails').removeClass('hide');
                $('#commonTermsPopup').modal({
                    keyboard: false,
                    backdrop: "static",
                });
            }
            else {
                $('#accSummaryDiv, .selectedAccountDetails').addClass('hide');
            }
        }

        function OnAcceptTerms() {
            if ($('#chkSellProceed').is(':checked')) {
                $('.loader-bg').fadeIn();
                return true;
            }
            else {
                ShowCustomMessage('Alert', 'Please Read, Understand and Agree to proceed.', '');
                return false;
            }
        }
        if (hdnTermsPop == "1") {
            OpenCommonTermsPopup();
        }
        else {
            if ($('#ddlUserAccountId').val() != '') {
                $('#accSummaryDiv, .selectedAccountDetails').removeClass('hide');
            }
            else {
                $('#accSummaryDiv, .selectedAccountDetails').addClass('hide');
            }
        }
        var isCurFormat;
        function FormatAllCurrency() {
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ' });
            });
            $('.currencyFormatNoSymbol').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: '' });
            });
        }
        FormatAllCurrency();
        var isUniFormat;
        var isUniFormatNoSymbol;
        function FormatAllUnit() {
            $('.unitFormat').each(function () {
                isUniFormat = new AutoNumeric(this, { currencySymbol: ' UNITS', currencySymbolPlacement: 's', decimalPlaces: '4' });
            });
            $('.unitFormatNoSymbol').each(function () {
                isUniFormatNoSymbol = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '4' });
            });

        }
        FormatAllUnit();
        function scroll_to_class(element_class, removed_height) {
            var scroll_to = $(element_class).offset().top - removed_height;
            if ($(window).scrollTop() != scroll_to) {
                $('html, body').stop().animate({ scrollTop: scroll_to }, 0);
            }
        }
        function bar_progress(progress_line_object, direction) {
            var number_of_steps = progress_line_object.data('number-of-steps');
            var now_value = progress_line_object.data('now-value');
            var new_value = 0;
            if (direction == 'right') {
                new_value = now_value + (100 / number_of_steps);
            }
            else if (direction == 'left') {
                new_value = now_value - (100 / number_of_steps);
            }
            progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
        }
        function FundDetails(Id) {
            $('.loadingDiv').removeClass('hide');
            $.ajax({
                url: "SellFunds.aspx/GetFundDetails",
                async: true,
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: { Id: Id, userAccountNo: $('#ddlUserAccountId').val() },
                success: function (data) {
                    if (data.d.IsSuccess) {
                        var json = data.d.UtmcFundInformation;
                        var latestNavPU = parseFloat(json.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice).toFixed(4);
                        //console.log(data.d.utmcDetailedMemberInvestmentsByFund[0].utmcMemberInvestments[0]);
                        var Units = data.d.Units;
                        $('#hdnTotalQuantity').val(Units.toFixed(4));
                        $('.loadingDiv').addClass('hide');
                    }
                    else {
                        ShowCustomMessage('Error', data.d.Message, '');
                    }
                }
            });
        }
        $(document).ready(function () {
            $('#txtUnits').keyup(function (event) {

                // skip for arrow keys
                if (event.which >= 37 && event.which <= 40) return;

                // format number
                //$(this).val(function (index, value) {
                //    return value
                //        .replace(/\D/g, "")
                //        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                //        ;
                //});
            });
            $('.btn-previous').click(function () {
                $('#errorMessage').html('');
                $('#successMessage').html('');
            });

            $('#ddlFundsList').change(function () {
                $('#btnAddFund').attr('disabled', true);
                $('#txtUnits').val('');
                $('#txtUnits').attr('disabled', true);
                $('#chkSellAll').attr('disabled', true);
                $('#errorMessage').html('');
                $('#successMessage').html('');
                $('#hdnTotalAvailableQuantity').val('');
                var Id = $(this).val();
                if (Id != "0" && $(this).val() != "" && $(this).val() != null) {
                    FundDetails(Id);
                    $('#chkSellAll').prop('checked',false);
                    var Id1 = parseInt($(this).val());
                    FundDetailsGet(Id1);
                    GetFundPerformance(Id1);
                   
                } else {
                    $('#divFundDetails').addClass('hide');
                }
                //else {
                //    $('#errorMessage').html('Fund is required.');
                //    return false;
                //}
            });

            $('#chkSellAll').click(function () {
                var hdnTotalQuantity = parseFloat($('#hdnTotalAvailableQuantity').val());
                if ($(this).is(':checked')) {
                    var str = hdnTotalQuantity.toFixed(4).toString().split('.');
                    if (str[0].length >= 4)
                        str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');

                    $('#txtUnits').val(str.join('.'));
                    //$('.unitholding').html("<span class='unitFormat'>" + hdnTotalQuantity.toFixed(4) + "</span>");
                }
                else {
                    $('#txtUnits').val('0');
                }
            });

            $('#txtUnits').keyup(function () {
                var hdnTotalQuantity = parseFloat($('#hdnTotalQuantity').val());
                var units = parseFloat($('#txtUnits').val());

                if (hdnTotalQuantity > units || hdnTotalQuantity < units)
                    $('#chkSellAll').prop('checked',false);
                if (hdnTotalQuantity == units)
                    $('#chkSellAll').prop('checked',true);
            });
        });
        /* Form */
        $('.f1 fieldset:first').fadeIn('slow');
        $('.f1 input[type="text"], .f1 input[type="password"], .f1 input[type="checkbox"], .f1 textarea').on('focus', function () {
            $(this).removeClass('input-error');
        });
        // next step
        $('.f1 .btn-next').on('click', function (e) {
            $('#errorMessage').html('');
            $('#successMessage').html('');
            var parent_fieldset = $(this).parents('fieldset');
            var next_step = true;
            // navigation steps / progress steps
            var current_active_step = $(this).parents('.f1').find('.f1-step.active');
            var progress_line = $(this).parents('.f1').find('.f1-progress-line');
            // fields validation
            if (parent_fieldset.hasClass('fieldsetfundSelectionAndAmount')) {
                if (!$('#chkConfirmFundInformation').is(":checked")) {
                    ShowCustomMessage('Alert', 'Please read, understand & agree to proceed. Thank you.', '');
                    next_step = false;
                }
                else
                    if (CartItems == undefined) {
                        ShowCustomMessage('Alert', 'Please add fund to proceed. Thank you.', '');
                        next_step = false;
                    }
                    else {
                        if (CartItems.length == 0) {
                            ShowCustomMessage('Alert', 'Please add fund to proceed. Thank you.', '');
                            next_step = false;
                        }
                    }
            }
            else
                parent_fieldset.find('input[type="text"], input[type="password"], input[type="checkbox"], textarea, select').each(function () {
                    if ($(this).val() == "") {
                        $(this).addClass('input-error');
                        next_step = false;
                    }
                    else {
                        if ($(this).is(':checkbox')) {
                            if (!$(this).attr('checked')) {
                                $(this).parents('.form-group').addClass('input-error');
                                next_step = false;
                            }
                            else {
                                $(this).parents('.form-group').removeClass('input-error');
                            }
                        }
                        else {
                            $(this).removeClass('input-error');
                        }
                    }

                });
            // fields validation
            if (next_step) {
                fundCodeSelected = $('#hdnFundId').val();
                if (fundCodeSelected != "") {
                    if ($('#ddlFundsList option[data-code="' + fundCodeSelected + '"]').val() != undefined) {
                        $('#ddlFundsList').val($('#ddlFundsList option[data-code="' + fundCodeSelected + '"]').val());
                        $('#ddlFundsList').change();
                    }
                }
                parent_fieldset.fadeOut(400, function () {
                    // change icons
                    current_active_step.removeClass('active').addClass('activated').next().addClass('active');
                    // progress bar
                    bar_progress(progress_line, 'right');
                    // show next step
                    $(this).next().fadeIn();
                    // scroll window to beginning of the form
                    scroll_to_class($('.f1'), 20);
                });
            }
            else {
                e.preventDefault();
            }
        });
        // previous step
        $('.f1 .btn-previous').on('click', function () {
            // navigation steps / progress steps
            var current_active_step = $(this).parents('.f1').find('.f1-step.active');
            var progress_line = $(this).parents('.f1').find('.f1-progress-line');
            $(this).parents('fieldset').fadeOut(400, function () {
                // change icons
                current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
                // progress bar
                bar_progress(progress_line, 'left');
                // show previous step
                $(this).prev().fadeIn();
                // scroll window to beginning of the form
                scroll_to_class($('.f1'), 20);
            });
        });
        // submit
        $('.f1').on('submit', function (e) {
            // fields validation
            $(this).find('input[type="text"], input[type="password"], input[type="checkbox"], textarea, select').each(function () {
                if ($(this).val() == "") {
                    e.preventDefault();
                    $(this).addClass('input-error');
                }
                else {
                    $(this).removeClass('input-error');
                    $('#loadAfterSubmitTransaction').removeClass('hide');
                    setTimeout(function () {
                        $('.loader-bg').fadeIn();
                    }, 500);
                }
            });
            // fields validation
        });

        function BindCart() {
            $('.tdTotalSalesUnits').html(TotalUnits);
            $('#tbodySelectedFunds, #tbodySelectedFunds1').html('');
            var tbodySelectedFundsHtml = "";
            if (CartItems.length > 0) {
                $('#cart-table').removeClass('hide');
            }
            $.each(CartItems, function (idx, obj) {
                tbodySelectedFundsHtml += "<tr>";
                tbodySelectedFundsHtml += "<td>" + obj.UtmcFundInformation.FundName + "</td>"
                tbodySelectedFundsHtml += "<td class='unitFormatNoSymbol text-right'>" + obj.Units + "</td>"
                tbodySelectedFundsHtml += "<td class='text-right'><a href='javascript:;' data-id=" + obj.Id +
                    " class='btn btn-sm btn-default editFund' data-fund-amount='" + obj.Units + "' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a><a href='javascript:;' data-id=" + obj.Id +
                    " class='btn btn-sm btn-default removeFund' data-toggle='tooltip' title='Remove'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td > ";
                tbodySelectedFundsHtml += "</tr>";
            });
            $('#tbodySelectedFunds').html(tbodySelectedFundsHtml);
            tbodySelectedFundsHtml = "";
            $.each(CartItems, function (idx, obj) {
                tbodySelectedFundsHtml += "<tr>";
                tbodySelectedFundsHtml += "<td>" + obj.UtmcFundInformation.FundName + "</td>"
                tbodySelectedFundsHtml += "<td class='unitFormatNoSymbol text-right'>" + obj.Units + "</td>"
                tbodySelectedFundsHtml += "</tr>";
            });
            $('#tbodySelectedFunds1').html(tbodySelectedFundsHtml);
            ChangeTC();
            FormatAllCurrency();
            FormatAllUnit();
            $('#ddlFundsList').val('0');
            $('#ddlFundsList').change();
        }

        var Term;
        function ChangeTC() {
            Term = "I confirm that I have read and understood the Fund Prospectus/Information Memorandum and Product Highlights Sheet of ";
            $.each(CartItems, function (idx, obj) {
                Term += " " + obj.UtmcFundInformation.FundName + ',';
            });
            Term = Term.substring(0, Term.length - 1);
            document.getElementById("chkConfirmFundInformation").nextSibling.innerHTML = Term;
        }

        var CartItems;
        var TotalUnits;
        $(document).ready(function () {
            $.ajax({
                url: "SellFunds.aspx/BindFunds",
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: { userAccountNo: $('#ddlUserAccountId').val() },
                success: function (data) {
                    CartItems = data.d.CartItems;
                    TotalUnits = data.d.TotalUnits;
                    BindCart();
                }
            });

            $('#btnAddFund').click(function () {
                $('#errorMessage').html('');
                $('#successMessage').html('');
                var fundId = $('#ddlFundsList').val();
                var units = $('#txtUnits').val();
                if (fundId != "" && units != "") {
                    units = parseFloat(units.replace(/,/g, ''));
                    $.ajax({
                        url: "SellFunds.aspx/AddFund",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { fundId: fundId, units: units, userAccountNo: $('#ddlUserAccountId').val() },
                        success: function (data) {
                            if (data.d.IsSuccess) {
                                CartItems = data.d.Data.CartItems;
                                TotalAmount = data.d.Data.TotalAmount;
                                BindCart();
                                //if (isModify == 1) {
                                //    $('#errorMessage').addClass('hide');
                                //    $('#successMessage').html('Successfully Modified with your confirmation.');
                                //}
                                //else {
                                //    $('#errorMessage').addClass('hide');
                                //    $('#successMessage').html('Successfully Added.');
                                //}
                                //InsertIntoOrderCard(fundId, amount, distributionID, consultantID, bankId);
                                //}
                                $('#divFundDetails').addClass('hide');
                                $('#cart-table').removeClass('hide');
                            }
                            else {
                                ShowCustomMessage("Alert", data.d.Message, "");
                            }
                        }
                    });
                }
                else if (fundId == "") {
                    ShowCustomMessage("Alert", "Please select fund.", "");
                }
                else if (units == "") {
                    ShowCustomMessage("Alert", "Please enter units.", "");
                }
                //$('#errorMessage').removeClass('hide');
                //$('#errorMessage').html('');
                //$('#successMessage').html('');

                //var fundId = $('#ddlFundsList').val();
                //if ($('#txtUnits').val() == '') {
                //    $('#errorMessage').removeClass('hide');
                //    $('#errorMessage').html('Units cannot be null.');
                //    return false;
                //}
                //var units = $('#txtUnits').val();
                //var availableUnits = parseFloat($('#hdnTotalAvailableQuantity').val());
                //var minimumHoldingUnits = parseFloat($('.mhUnits').text().replace(',', ''));

                //if (fundId != "0") {

                //    units = parseFloat(units.replace(/,/g, ''));
                //    if (CartItems != undefined) {
                //        var items = $.grep(CartItems, function (obj, idx) {
                //            return obj.UtmcFundInformation.Id == parseInt(fundId);
                //        });
                //        console.log(items);
                //        var isModify = 0;
                //        if (items.length > 0) {
                //            if (confirm('Do you want to modify existing fund units?')) {
                //                isModify = 1;
                //            }
                //            else {
                //                $('#errorMessage').removeClass('hide');
                //                $('#errorMessage').html('Operation Cancelled by you.');
                //                return false;
                //            }
                //        }
                //    }
                //    if (units > availableUnits) {
                //        $('#errorMessage').removeClass('hide');
                //        $('#errorMessage').html('Available units insufficient.');
                //        return false;
                //    }
                //    else if (units == availableUnits) {

                //    }
                //    else if (units > availableUnits - minimumHoldingUnits) {
                //        $('#errorMessage').removeClass('hide');
                //        $('#errorMessage').html('Minimum holding unit must be not less than ' + $('.mhUnits').text());
                //        return false;
                //    }

                //    if (units == "") {
                //        $('#errorMessage').removeClass('hide');
                //        $('#errorMessage').html('Units is required.');
                //        return false;
                //    }
                //    else {
                //        $('#errorMessage').html('');
                //        var fundholdingUnits = $('#ddlFundsList option:selected').attr('data-holding-units');
                //        var accountPlan = $('#hdnAccountPlan').val();
                //        if (parseFloat(units) > parseFloat(fundholdingUnits)) {
                //            $('#errorMessage').removeClass('hide');
                //            $('#errorMessage').html('Cannot exceed current holding: ' + fundholdingUnits);
                //            return false;
                //        }
                //        $('#errorMessage').html('');
                //        $('#successMessage').html('');
                //        $.ajax({
                //            url: "SellFunds.aspx/AddFund",
                //            contentType: 'application/json; charset=utf-8',
                //            type: "GET",
                //            dataType: "JSON",
                //            data: { fundId: fundId, units: units, userAccountNo: $('#ddlUserAccountId').val() },
                //            success: function (data) {
                //                CartItems = data.d.CartItems;
                //                TotalUnits = data.d.TotalUnits;
                //                BindCart();

                //                if (isModify == 1) {
                //                    $('#errorMessage').addClass('hide');
                //                    $('#successMessage').html('Successfully Modified with your confirmation.');
                //                }
                //                else {
                //                    $('#errorMessage').addClass('hide');
                //                    $('#successMessage').html('Successfully Added.');
                //                }
                //                $('#divFundDetails').addClass('hide');
                //                $('#cart-table').removeClass('hide');
                //            }
                //        });
                //    }
                //} else {
                //    $('#errorMessage').removeClass('hide');
                //    $('#errorMessage').html("Fund is required");
                //}





            });

            $('#ConfirmationYes').click(function () {
                $('#errorMessage').html('');
                $('#successMessage').html('');
                var id = $('.removeFund').attr('data-id');
                $.ajax({
                    url: "SellFunds.aspx/RemoveFund",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { Id: id, userAccountNo: $('#ddlUserAccountId').val() },
                    success: function (data) {
                        CartItems = data.d.CartItems;
                        TotalAmount = data.d.TotalAmount;
                        BindCart();
                        $('#successMessage').html('Successfully removed.');
                        document.getElementById("ConfirmationCancel").click();
                    }
                });
            });

            $('#tbodySelectedFunds').on('click', '.removeFund', function () {

                document.getElementById("deleteFundModal").click();
            });
            //$('#tbodySelectedFunds').on('click', '.removeFund', function () {
            //    $('#errorMessage').html('');
            //    $('#successMessage').html('');
            //    var id = $(this).attr('data-id');
            //    $.ajax({
            //        url: "SellFunds.aspx/RemoveFund",
            //        contentType: 'application/json; charset=utf-8',
            //        type: "GET",
            //        dataType: "JSON",
            //        data: { Id: id, userAccountNo: $('#ddlUserAccountId').val() },
            //        success: function (data) {
            //            CartItems = data.d.CartItems;
            //            TotalUnits = data.d.TotalUnits;
            //            BindCart();
            //            $('#errorMessage').addClass('hide');
            //            $('#successMessage').html('Successfully removed.');
            //        }
            //    });
            //});

            $('#FundUpdate').click(function () {
                var availableUnits = parseFloat($('#hdnAvailableUnits').val());
                var minimumHoldingUnits = parseFloat($('.mhUnits').text().replace(',', ''));
                var id = newFundEditId;
                var amount2 = document.getElementById("FundAmount").value;

                var amount = amount2.replace(/,/g, "");
                if (amount == "") {
                    ShowCustomMessage('Alert', 'Please enter amount!', '');
                }
                else if (amount == "0") {
                    ShowCustomMessage('Alert', 'Unit is required', '');
                }
                else if (amount > availableUnits) {

                    ShowCustomMessage('Alert', 'Available units insuficient', '');
                }
                else if (amount > availableUnits - minimumHoldingUnits) {

                    ShowCustomMessage('Alert', 'Amount will exceed minimum fund holding after sell.', '');
                }
                //else if (amount.toFixed(4) < 0.0001 ) {
                //    ShowCustomMessage('Alert', 'Minimum unit is 0.0001', '');
                //}
                else {
                    $.ajax({
                        url: "SellFunds.aspx/EditFund",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { Id: id, Amount: amount, userAccountNo: $('#ddlUserAccountId').val() },
                        success: function (data) {
                            if (data.d.IsSuccess) {
                                CartItems = data.d.Data.CartItems;
                                TotalUnits = data.d.Data.TotalUnits;
                                BindCart();
                                $('#errorMessage').addClass('hide');
                                //$('#successMessage').html('Successfully Edit.');
                                document.getElementById("FundUpdateClose").click();
                            }
                            else {
                                ShowCustomMessage('Alert', data.d.Message, '');
                            }
                        }
                    });
                }
            });

            var newFundEditId;

            $('#tbodySelectedFunds').on('click', '.editFund', function () {
                $('#errorMessage').html('');
                $('#successMessage').html('');
                var id = $(this).attr('data-id');
                var fundID = $(this).attr('fund-id');
                var fundAmt = $(this).attr('data-fund-amount');
                var minfundAmt = $(this).attr('data-fund-min-amount');
                $('#FundAmount').val(fundAmt);
                $('#txtFundMinAmount').val(minfundAmt);
                newFundEditId = id;
                FundDetailsUnitOwned(fundID);
                document.getElementById("editFundModal").click();
            });

            $('#txtUnits').inputFilter(function (value) {
                return /^\d*(\.)?(\d{0,4})$/.test(value);
            });
            $('#FundAmount').inputFilter(function (value) {
                return /^\d*(\.)?(\d{0,4})$/.test(value);
            });
        });

        function FundDetailsUnitOwned(Id) {
            $.ajax({
                type: "GET",
                url: "BuyFunds.aspx/GetTotalUnits",
                data: { Id: Id },
                dataType: "JSON",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $('#hdnTotalQuantity').val(data.d.totalUnit);
                    if (data.d.EpfOrOther != "EPF") {
                        if (data.d.instruction == 0) {
                            document.getElementById("ddlDistributionInstruction").disabled = false;
                            document.getElementById("ddlDistributionInstruction").value = 3;
                        }
                        else {
                            document.getElementById("ddlDistributionInstruction").disabled = false;
                            document.getElementById("ddlDistributionInstruction").value = (data.d.instruction == 0 ? 3 : data.d.instruction);
                        }
                    } else {
                        document.getElementById("ddlDistributionInstruction").disabled = false;
                        document.getElementById("ddlDistributionInstruction").value = 3;
                    }
                    FundDetails(Id);
                    GetFundPerformance(Id);
                }
            });
        }

        function CheckNumeric(txt, e) {

            if (window.event) // IE 
            {
                if ((e.keyCode < 48 || e.keyCode > 57) && e.keyCode != 46) {
                    console.log("asadsadsa");
                    event.returnValue = false;
                    return false;
                }
            }
            else { // Fire Fox
                if ((e.keyCode < 48 || e.keyCode > 57) && e.keyCode != 46) {
                    console.log("asadsadsa");
                    event.returnValue = false;
                    return false;
                }
            }

            var txtvalue = txt.value;
            var regexp = /^\d+(\.\d{1,2})?$/;
            console.log(regexp.test(txtvalue));
            if (!regexp.test(txtvalue)) {
                return false;
            }
        }

        var fundDetails;
        var minInitialInvestmentCashEPF = "0,0";
        var splits = '';
        var fundCodeSelected = '';

        function FundDetailsGet(Id) {
            $('.loadingDiv').removeClass('hide');
            $('#divFundDetails').removeClass('hide');
            $('#chkSellAll').hide();
            $.ajax({
                url: "SellFunds.aspx/GetFundDetailsGet",
                async: true,
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: { 'Id': Id, userAccountNo: $('#ddlUserAccountId').val() },
                success: function (data) {
                    splits = data.d.splits;
                    var json = data.d.UtmcFundInformation;
                    var hdnTotalQuantity = parseFloat($('#hdnTotalQuantity').val());
                    fundDetails = json;
                    $('.fundName').html(json.FundName);
                    if (json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentEpf == 0) {
                        $('.minimumInitialInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentCash + "</span>");
                        document.getElementById('minimumInitialInvestementTitle').innerHTML = "Minimum Initial Investement (CASH)";
                    }
                    else {
                        $('.minimumInitialInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentCash + "</span>/<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentEpf + "</span>");
                        document.getElementById('minimumInitialInvestementTitle').innerHTML = "Minimum Initial Investement (CASH/EPF)";
                    }
                    if (json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf == 0) {
                        $('.minimumSubsequentInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash + "</span>");
                        document.getElementById('minimumSubsequentInvestementTitle').innerHTML = "Minimum Subsequent Investement (CASH)";
                    }
                    else {
                        $('.minimumSubsequentInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash + "</span>/<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf + "</span>");
                        document.getElementById('minimumSubsequentInvestementTitle').innerHTML = "Minimum Subsequent Investement (CASH/EPF)";
                    }
                    $('.riskCriteria').html($('#ddlFundsList option:selected').attr('risk-group').trim());
                    $('.unitholding').html("<span class='unitFormat'>" + hdnTotalQuantity.toFixed(4) + "</span>");
                    console.log(data.d.total);
                    $('.aUnits').html("<span class='unitFormat'>" + (hdnTotalQuantity - data.d.total).toFixed(4) + "</span>");
                    $('#hdnTotalAvailableQuantity').val((hdnTotalQuantity - data.d.total).toFixed(4));
                    $('.mhUnits').html("<span class='unitFormat'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinHoldingUnits + "</span>");
                    $('#hdnAvailableUnits ').val((hdnTotalQuantity - data.d.total).toFixed(4));

                    unitsAvailable = hdnTotalQuantity - data.d.total;
                    //.split(":")[1].trim() + " - " + $('#ddlFundsList option:selected').text().split(":")[2].trim()
                    //if ($('#ddlFundsList option:selected').text()) {
                    //    openPopup();
                    //}

                    $('#hdnMinSubsequentInvestmentCashEPF').val(json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash + "," + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf);
                    var masterProspectus = $.grep(
                        json.UtmcFundInformationIdUtmcFundFiles,
                        function (obj) {
                            return obj.UtmcFundFileTypesDefId == 1
                        })[0];
                    if (masterProspectus != null) {
                        if (masterProspectus.Url != null) {
                            var a = $('<a />').attr('href', masterProspectus.Url).attr('target', '_blank').html('Click here to View');
                            $('.masterProspectus').html(a);
                        }
                        else {
                            $('.masterProspectus').html('-');
                        }
                    }
                    else {
                        $('.masterProspectus').html('-');
                    }

                    var productHighlightsSheet = $.grep(
                        json.UtmcFundInformationIdUtmcFundFiles,
                        function (obj) {
                            return obj.UtmcFundFileTypesDefId == 3
                        })[0];
                    if (productHighlightsSheet != null) {
                        if (productHighlightsSheet.Url != null) {
                            var a = $('<a />').attr('href', productHighlightsSheet.Url).attr('target', '_blank').html('Click here to View');
                            $('.productHighlightsSheet').html(a);
                        }
                        else {
                            $('.productHighlightsSheet').html('-');
                        }
                    }
                    else {
                        $('.productHighlightsSheet').html('-');
                    }

                    var informationMemorandum = $.grep(
                        json.UtmcFundInformationIdUtmcFundFiles,
                        function (obj) {
                            return obj.Name == "Information Memorandum"
                        })[0];
                    if (informationMemorandum != null) {
                        if (informationMemorandum.Url != null) {
                            var a = $('<a />').attr('href', informationMemorandum.Url).attr('target', '_blank').html('Click here to View');
                            $('.informationMemorandum').html(a);
                            $('#infoMemo').show();
                        }
                        else {
                            $('.informationMemorandum').html('-');
                            $('#infoMemo').hide();
                        }
                    }
                    else {
                        $('.informationMemorandum').html('-');
                        $('#infoMemo').hide();
                    }

                    $('#chkConfirmFundInformation').prop('checked',false);
                    //$('.loadingDiv').addClass('hide');
                    $('#btnAddFund').attr('disabled', false);
                    $('#txtUnits').attr('disabled', false);
                    $('#chkSellAll').attr('disabled', false);
                    FormatAllCurrency();
                    $('#chkSellAll').show();
                }
            });
        }

        function GetFundPerformance(Id) {
            $.ajax({
                type: "GET",
                url: "SellFunds.aspx/GetFundPerformance",
                data: { Id: Id },
                dataType: "JSON",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    $('#weekCValue').html(data.d.w1);
                    $('#monthCValue').html(data.d.m1);
                    $('#month3CValue').html(data.d.m3);
                    $('#month6CValue').html(data.d.m6);
                    $('#yearCValue').html(data.d.y1);
                    $('#year2CValue').html(data.d.y2);
                    $('#year3CValue').html(data.d.y3);
                    $('#year5CValue').html(data.d.y5);
                    $('#year10CValue').html(data.d.y10);

                    $('#weekCValueAvg').html(data.d.w1a);
                    $('#monthCValueAvg').html(data.d.m1a);
                    $('#month3CValueAvg').html(data.d.m3a);
                    $('#month6CValueAvg').html(data.d.m6a);
                    $('#yearCValueAvg').html(data.d.y1a);
                    $('#year2CValueAvg').html(data.d.y2a);
                    $('#year3CValueAvg').html(data.d.y3a);
                    $('#year5CValueAvg').html(data.d.y5a);
                    $('#year10CValueAvg').html(data.d.y10a);

                    $('#fundPricedate').html(data.d.date);
                    $('#fundPriceLastUpdatedDate').html(data.d.date);

                    $('.loadingDiv').addClass('hide');
                }
            });
        }
        //$('#FundAmount').keyup(function (event) {

        //    // skip for arrow keys
        //    if (event.which >= 37 && event.which <= 40) return;

        //    // format number
        //    $(this).val(function (index, value) {
        //        return value
        //            .replace(/\D/g, "")
        //            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        //            ;
        //    });
        //});
        var isCurFormat;
        function FormatAllCurrency() {
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ' });
            });
            $('.currencyFormatNoDecimal').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ', decimalPlaces: '0' });
            });
            $('.unitFormatNoSymbolMarketValue').each(function () {
                isUniFormatNoSymbol = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '2' });
            });
            $('.unitFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '4' });
            });
        }


    </script>
</asp:Content>
