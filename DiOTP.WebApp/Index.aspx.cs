﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DiOTP.Service;
using DiOTP.Utility;
using DiOTP.Service.IService;
using System.Web.Services;
using System.Web.Script.Services;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using DiOTP.Utility.CustomClasses;
using System.Threading;
using System.Configuration;
using System.Text;
using System.Net;
using Newtonsoft.Json.Linq;
using DiOTP.WebApp.FPXLibary;
using DiOTP.Utility.OracleDTOs;
using System.Text.RegularExpressions;
using static DiOTP.PolicyAndRules.UserPolicy;
using DiOTP.PolicyAndRules;


namespace DiOTP.WebApp
{
    public partial class Index : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyObj.Value; } }

        private static readonly Lazy<ISiteContentService> lazySiteContentObj = new Lazy<ISiteContentService>(() => new SiteContentService());

        public static ISiteContentService ISiteContentService { get { return lazySiteContentObj.Value; } }

        private static readonly Lazy<IUserSecurityService> lazySecurityObj = new Lazy<IUserSecurityService>(() => new UserSecurityService());

        public static IUserSecurityService IUserSecurityService { get { return lazySecurityObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyObjFund = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObjFund.Value; } }

        private static readonly Lazy<IUserTypeService> lazyObjUserTypeService = new Lazy<IUserTypeService>(() => new UserTypeService());

        public static IUserTypeService IUserTypeService { get { return lazyObjUserTypeService.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IMaHolderRegService> lazyMaHolderRegObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());
        public static IMaHolderRegService IMaHolderRegService { get { return lazyMaHolderRegObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        public static string loginRedirectUrl = "";

        protected string GetApplicationVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        private static readonly Lazy<IUserOrderService> lazyUserOrderService = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderService.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            //DigitalizationServiceManager.Verify();
            try
            {
                Response responseCheckConn = ServicesManager.CheckConnection();
                if (responseCheckConn.IsSuccess)
                {
                    if (!responseCheckConn.IsApiAvailable)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"Portal is under maintainence.(1)\", '');", true);
                        Logger.WriteLog("Index Page_Load responseCheckConn !IsApiAvailable: " + responseCheckConn.Message);
                    }
                    else if (!responseCheckConn.IsDBAvailable)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"Portal is under maintainence.(2)\", '');", true);
                        Logger.WriteLog("Index Page_Load responseCheckConn !IsDBAvailable: " + responseCheckConn.Message);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"Portal is under maintainence.(3)\", '');", true);
                    Logger.WriteLog("Index Page_Load responseCheckConn !IsSuccess: " + responseCheckConn.Message);
                }

                Response responseSCList = ISiteContentService.GetDataByFilter("site_content_type_id=2 order by priority", 0, 10, false);
                if (responseSCList.IsSuccess)
                {
                    List<SiteContent> siteContents = (List<SiteContent>)responseSCList.Data;
                    StringBuilder sliderContents = new StringBuilder();
                    StringBuilder sliderThumbContents = new StringBuilder();
                    int i = 1;
                    foreach (SiteContent sc in siteContents.Where(x => x.Status == 1).ToList())
                    {
                        string classText = "";
                        //if (i == 0)
                        //classText = "active";
                        string classHide = "";
                        if (sc.IsContentDisplay == 0)
                        {
                            classHide = "hide";
                        }
                        foreach (SiteContent sc1 in siteContents.Where(x => x.Priority == 1).ToList())
                        { }
                        sliderContents.Append(@"<div class='item " + classText + @"' style='height: calc(100vh - 42px) !important'>
                        <img src='" + sc.ImageUrl + @"' height:86% weight:100%/>
                        <div class='carousel-caption " + classHide + @"'>
                            <div class='banner-head'>
                                <h3 style='color:" + sc.TitleColor + @"'>" + sc.Title + @"</h3>
                            </div>
                            <div class='banner-content'>
                                <p class='line-clamp' style='color:" + sc.ContentColor + @"'>
                                    " + (sc.Content.Length > 130 ? sc.Content.Substring(0, 130) + "..." : sc.Content) + @"
                                </p>
                                <a class='btn btn-readmore' href='News-Content.aspx?Id=" + sc.Id + @"'>Read more</a>
                            </div>
                        </div>

                    </div>");
                        sliderThumbContents.Append(@"<li data-target='#myCarousel' data-slide-to='" + i + @"' class='" + classText + @"'>
                        <a href='#'>
                            <span class='tab-head'>" + sc.SubTitle + @"</span><br />
                            <span class='tab-cont'>" + sc.ShortDisplayContent + @"</span>
                        </a></li>");
                        i++;
                    }
                    carouselInner.Controls.Add(new LiteralControl(sliderContents.ToString()));
                    carouselThumbs.Controls.Add(new LiteralControl(sliderThumbContents.ToString()));
                }
                Response responseSCList2 = ISiteContentService.GetDataByPropertyName(nameof(SiteContent.SiteContentTypeId), "1", true, 0, 0, true);
                if (responseSCList2.IsSuccess)
                {
                    List<SiteContent> announcementContents = (List<SiteContent>)responseSCList2.Data;
                    StringBuilder dbAnnouncementsText = new StringBuilder();
                    foreach (SiteContent sc in announcementContents.Where(x => x.Status == 1).ToList())
                    {
                        dbAnnouncementsText.Append(@"<a href='/News-Content.aspx?id=" + sc.Id + @"' class='announcement-slidetext'>" + sc.ShortDisplayContent + @" <small><u>Read more</u></small></a>");
                    }
                    announcementWrapper.InnerHtml = dbAnnouncementsText.ToString();
                }

                if (Session["User"] != null)
                {
                    HttpContext.Current.Session.Clear();
                    HttpContext.Current.Session.Abandon();
                }
                else
                {
                    if (Session["locked"] != null)
                    {
                        int locked = (int)Session["locked"];
                        if (locked == 1)
                        {
                            hdnAccountLockedPop.Value = "1";
                        }
                    }
                    if (Request.QueryString["accountHolderNo"] != null)
                    {
                        if (Request.QueryString["redirectUrl"] != null)
                        {
                            loginRedirectUrl = Request.QueryString["redirectUrl"].ToString();
                        }
                        string accNo = Request.QueryString["accountHolderNo"].ToString();
                        Response.Redirect(loginRedirectUrl, false);
                    }
                    if (Request.QueryString["languageSelected"] != null)
                    {
                        if (Request.QueryString["redirectUrl"] != null)
                        {
                            loginRedirectUrl = Request.QueryString["redirectUrl"].ToString();
                        }
                        string languageSelected = Request.QueryString["languageSelected"].ToString();
                        HttpCookie myCookie = new HttpCookie("DiOTPSettings");
                        myCookie["Language"] = languageSelected;
                        Response.Cookies.Add(myCookie);
                        Response.Redirect(loginRedirectUrl, false);
                    }
                    if (!IsPostBack)
                    {
                        //WB 8/5/2019
                        string q = Request.QueryString["q"];
                        if (q == "SetPassword")
                        {
                            Session.Clear();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Successfully updated password.', '');", true);
                        }
                        else if (q == "ResetPasswordEmail")
                        {
                            Session.Clear();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Successfully sent reset link to email.', '');", true);
                        }
                    }
                }

            }
            catch (ThreadAbortException ex1)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load ThreadAbortException: " + ex1.Message);
                // do nothing
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                Response message = VerifyLogin(txtEmailOrUsername.Text, txtPassword.Text, Request, hdnLocalIPAddress.Value);
                if (message.IsSuccess)
                {
                    if (string.IsNullOrEmpty(loginRedirectUrl))
                        Response.Redirect("/Portfolio.aspx", false);
                    else
                        Response.Redirect("/Portfolio.aspx?redirectUrl=" + loginRedirectUrl, false);
                }
                else
                {
                    divMessage.Attributes.Add("class", "alert-box error");
                    divMessage.InnerHtml = message.Message;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("index Page login click " + ex.Message);
            }
        }
        private static UserLoginStep responseUserStep1 { get; set; }

        public Response VerifyLogin(string emailOrUsername, string password, HttpRequest Request, String localIP)
        {
            User user = new User();
            emailOrUsername = emailOrUsername.ToLower();

            Response responseUser = (Response)UserPolicy.ValidateLogin(new UserPolicy.UserClass { email = emailOrUsername, Step = 1, password = password, httpContext = HttpContext.Current });
            try
            {
                if (responseUser.IsSuccess)
                {
                    responseUserStep1 = (UserLoginStep)responseUser.Data;
                    UserPolicyEnum userPolicyEnum = (UserPolicyEnum)Enum.Parse(typeof(UserPolicyEnum), responseUserStep1.Code);
                    string message = userPolicyEnum.ToDescriptionString();
                    if (message == "SUCCESS")
                    {
                        user = responseUserStep1.User;
                        //Session["user"] = user;
                        //Session["LastLoginIP"] = user.LastLoginIp;
                        //Session["LastLoginTime"] = user.LastLoginDate;
                        //user.IsLoginLocked = 0;
                        //user.LoginAttempts = 0;
                        //user.LastLoginDate = DateTime.Now;
                        //user.LastLoginIp = TrackIPAddress.GetUserPublicIP(localIP) + ", " + localIP;
                        //user.VerificationCode = 0;
                        //user.VerifyExpired = 1;
                        //Response responseUpdate = IUserService.UpdateData(user);

                        //if (!responseUpdate.IsSuccess)
                        //{
                        //    Session.Clear();
                        //    Session.Abandon();
                        //    return responseUpdate;
                        //}

                        UserLogMain ulm = new UserLogMain()
                        {
                            Description = "Login successful.",
                            TableName = "users",
                            UpdatedDate = DateTime.Now,
                            UserId = user.Id,
                            UserAccountId = 0,
                            RefId = user.Id,
                            RefValue = user.Username,
                            StatusType = 1
                        };
                        Response responseLog = IUserLogMainService.PostData(ulm);
                        if (!responseLog.IsSuccess)
                        {
                            //Audit log failed
                        }


                        //Session["CurrentLoginTime"] = user.LastLoginDate;

                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            List<UserAccount> uAs = (List<UserAccount>)responseUAList.Data;
                            UserAccount uA = ((List<UserAccount>)responseUAList.Data).Where(x => x.IsPrimary == 1).FirstOrDefault();
                            if (uA == null)
                                uA = uAs.FirstOrDefault();
                            if (uA.IsVerified == 0)
                            {
                                foreach (UserAccount ua in uAs)
                                {
                                    ua.VerificationCode = 0;
                                    ua.VerifyExpired = 1;
                                }
                                IUserAccountService.UpdateBulkData(uAs);
                            }
                        }

                        return responseUser;
                    }
                    else if (message == "EXCEPTION")
                    {
                        responseUser.Message = message;
                        responseUser.IsSuccess = false;
                        return responseUser;
                    }
                    else
                    {
                        responseUser.Message = message;
                        responseUser.IsSuccess = false;
                        if (userPolicyEnum == UserPolicyEnum.UA2 || userPolicyEnum == UserPolicyEnum.UA5)
                            hdnAccountLockedPop.Value = "1";
                        return responseUser;
                    }
                }
                else
                {
                    responseUser.IsSuccess = false;
                    return responseUser;
                }

            }
            catch (Exception ex)
            {
                responseUser.IsSuccess = false;
                responseUser.Message = ex.Message;
                Logger.WriteLog("index Page VerifyLogin " + ex.Message);
                return responseUser;
            }
        }

        private static readonly Lazy<ILanguageDirService> lazyLanguageDirService = new Lazy<ILanguageDirService>(() => new LanguageDirService());

        public static ILanguageDirService ILanguageDirService { get { return lazyLanguageDirService.Value; } }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetLanguageDirByModule(int module)
        {
            StringBuilder filter = new StringBuilder();
            filter.Append(" " + (Converter.GetColumnNameByPropertyName<LanguageDir>(nameof(LanguageDir.Module))) + "='" + module + "'");
            Response responseLDList = ILanguageDirService.GetDataByFilter(filter.ToString(), 0, 0, false);
            if (responseLDList.IsSuccess)
            {
                List<LanguageDir> languageDirectory = (List<LanguageDir>)responseLDList.Data;
                return languageDirectory;
            }
            return null;
        }

        protected void btnRequestCode_Click(object sender, EventArgs e)
        {
            try
            {
                string principleIC = "";
                String ICNo = txtICNo.Text;
                String MANumber = txtMANumber.Text;
                String EmailId = txtEmail.Text;

                Regex regex = new Regex(@"^\d+$");
                if (ICNo == "" || MANumber == "" || EmailId == "")
                {
                    msg.Style.Add("color", "#FF0000");
                    msg.InnerHtml = "<i class='fa fa-close icon'></i> Please enter required(*) fields.";
                }
                else if (!regex.IsMatch(MANumber))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'MA No must be numeric.', '');", true);
                }
                else
                {
                    StringBuilder filter = new StringBuilder();
                    filter.Append(" 1=1");
                    filter.Append(" and " + Converter.GetColumnNameByPropertyName<User>(nameof(Utility.User.EmailId)) + " = '" + EmailId + "'");
                    Response responseUList = IUserService.GetDataByFilter(filter.ToString(), 0, 0, false);
                    if (responseUList.IsSuccess)
                    {
                        List<User> users = (List<User>)responseUList.Data;
                        if (users.Count != 0)
                        {
                            foreach (User u in users)
                            {
                                Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + u.Id + "' and status='1' ", 0, 0, false);
                                if (responseUAList.IsSuccess)
                                {
                                    UserAccount userAccs = ((List<UserAccount>)responseUAList.Data).Where(x => x.AccountNo == MANumber).FirstOrDefault();

                                    if (userAccs != null)
                                    {
                                        Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(MANumber);
                                        MaHolderReg maHolderReg = new MaHolderReg();
                                        if (responseMHR.IsSuccess)
                                        {
                                            maHolderReg = (MaHolderReg)responseMHR.Data;
                                            if (maHolderReg != null)
                                            {
                                                if (maHolderReg.HolderStatus == "I" || maHolderReg.HolderStatus == "S")
                                                {
                                                    msg.Style.Add("color", "#FF0000");
                                                    msg.InnerHtml = "<i class='fa fa-close icon'></i>" + (maHolderReg.HolderStatus == "I" ? "MA Inactive." : (maHolderReg.HolderStatus == "S" ? "MA Suspended." : ""));
                                                }
                                                else
                                                {
                                                    bool isValidMANumber = false;
                                                    List<MaHolderReg> MaHolderRegDatas = new List<MaHolderReg>();

                                                    if (ICNo.Contains("-"))
                                                    {
                                                        ICNo = ICNo.Replace("-", "");
                                                    }
                                                    if (ICNo.Contains(" "))
                                                    {
                                                        ICNo = ICNo.Replace(" ", "");
                                                    }
                                                    if (ICNo.Length == 12 && ICNo.All(Char.IsDigit))
                                                    {
                                                        String IDNo = maHolderReg.IdNo;
                                                        String IDNo2 = maHolderReg.IdNo2;
                                                        String NewID = IDNo;
                                                        String NewID2 = (IDNo2 == null ? "" : IDNo2);
                                                        if (NewID.Contains("-"))
                                                        {
                                                            NewID = NewID.Replace("-", "");
                                                        }
                                                        if (NewID.Contains(" "))
                                                        {
                                                            NewID = NewID.Replace(" ", "");
                                                        }
                                                        if (NewID2.Contains("-"))
                                                        {
                                                            NewID2 = NewID2.Replace("-", "");
                                                        }
                                                        if (NewID2.Contains(" "))
                                                        {
                                                            NewID2 = NewID2.Replace(" ", "");
                                                        }
                                                        if (NewID == ICNo)
                                                        {
                                                            isValidMANumber = true;
                                                            Response response = ServicesManager.GetMaHolderRegsByIdNo(maHolderReg.IdNo);

                                                            if (response.IsSuccess)
                                                            {
                                                                principleIC = maHolderReg.IdNo;
                                                                MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                            }
                                                        }
                                                        else if (NewID2 == ICNo)
                                                        {
                                                            isValidMANumber = true;
                                                            Response response = ServicesManager.GetMaHolderRegsByIdNo(maHolderReg.IdNo2);
                                                            if (response.IsSuccess)
                                                            {
                                                                principleIC = maHolderReg.IdNo;
                                                                MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            isValidMANumber = false;
                                                        }
                                                    }
                                                    else if (ICNo.Length == 12 && !ICNo.All(Char.IsDigit))
                                                    {
                                                        msg.Style.Add("color", "#FF0000");
                                                        msg.InnerHtml = "<i class='fa fa-close icon'></i> Invalid NRIC / OLD NRIC / PASSPORT.";
                                                        return;
                                                    }
                                                    else if (ICNo.Length != 12 && ICNo.All(Char.IsDigit))
                                                    {
                                                        String IDNo = maHolderReg.IdNo;
                                                        String NewID = IDNo;
                                                        if (NewID.Contains("-"))
                                                        {
                                                            NewID = NewID.Replace("-", "");
                                                        }
                                                        if (NewID.Contains(" "))
                                                        {
                                                            NewID = NewID.Replace(" ", "");
                                                        }
                                                        if (NewID == ICNo)
                                                        {
                                                            isValidMANumber = true;
                                                            Response response = ServicesManager.GetMaHolderRegsByIdNo(maHolderReg.IdNo);
                                                            if (response.IsSuccess)
                                                            {
                                                                MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            isValidMANumber = false;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        String IDNo = maHolderReg.IdNoOld;
                                                        String NewID = IDNo;
                                                        if (NewID.Contains("-"))
                                                        {
                                                            NewID = NewID.Replace("-", "");
                                                        }
                                                        if (NewID.Contains(" "))
                                                        {
                                                            NewID = NewID.Replace(" ", "");
                                                        }
                                                        if (NewID == ICNo)
                                                        {
                                                            isValidMANumber = true;
                                                            Response response = ServicesManager.GetMaHolderRegsByIdNo(IDNo);
                                                            if (response.IsSuccess)
                                                            {
                                                                MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            isValidMANumber = false;
                                                            IDNo = maHolderReg.IdNo2;
                                                            NewID = IDNo;
                                                            if (NewID.Contains("-"))
                                                            {
                                                                NewID = NewID.Replace("-", "");
                                                            }
                                                            if (NewID.Contains(" "))
                                                            {
                                                                NewID = NewID.Replace(" ", "");
                                                            }
                                                            if (NewID == ICNo)
                                                            {
                                                                isValidMANumber = true;
                                                                Response response = ServicesManager.GetMaHolderRegsByIdNo(NewID);
                                                                if (response.IsSuccess)
                                                                {
                                                                    MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                isValidMANumber = false;
                                                            }
                                                        }
                                                    }
                                                    List<MaHolderReg> maHolderRegsNew = new List<MaHolderReg>();
                                                    if (MaHolderRegDatas.Count > 0)
                                                    {
                                                        MaHolderRegDatas = MaHolderRegDatas.Where(x => x.HolderStatus == "A").ToList();
                                                        MaHolderRegDatas.ForEach(x =>
                                                        {
                                                            bool isAdd = true;
                                                            if (x.IdNo2 != null)
                                                            {
                                                                if (x.IdNo2.Contains("STA"))
                                                                {
                                                                    isAdd = false;
                                                                }
                                                            }
                                                            if (x.IdNo3 != null)
                                                            {
                                                                if (x.IdNo3.Contains("STA") || x.IdNo3.Contains("10JF"))
                                                                {
                                                                    isAdd = false;
                                                                }
                                                            }
                                                            if (isAdd)
                                                            {
                                                                maHolderRegsNew.Add(x);
                                                            }
                                                        });
                                                        //Check
                                                        MaHolderRegDatas = maHolderRegsNew;

                                                    }
                                                    if (MaHolderRegDatas.Count == 0)
                                                    {
                                                        msg.Style.Add("color", "#FF0000");
                                                        msg.InnerHtml = "<i class='fa fa-close icon'></i> No MA record found.";
                                                    }
                                                    else
                                                    {
                                                        Response responseUAList1 = IUserAccountService.GetDataByFilter(" account_no='" + MANumber + "' and is_verified=1 and status=1 ", 0, 0, false);
                                                        List<UserAccount> userAccounts = new List<UserAccount>();
                                                        if (responseUAList1.IsSuccess)
                                                        {
                                                            userAccounts = (List<UserAccount>)responseUAList1.Data;
                                                            if (userAccounts.Count > 0)
                                                            {
                                                                string principleICNew = principleIC;
                                                                if (principleICNew.Contains("-"))
                                                                {
                                                                    principleICNew = principleICNew.Replace("-", "");
                                                                }
                                                                if (principleICNew.Contains(" "))
                                                                {
                                                                    principleICNew = principleICNew.Replace(" ", "");
                                                                }

                                                                string toMobileNumber = maHolderReg.HandPhoneNo;
                                                                if (principleICNew != ICNo)
                                                                {
                                                                    toMobileNumber = maHolderReg.JointTelNo.Trim();
                                                                }
                                                                if (toMobileNumber == null || toMobileNumber == "")
                                                                {
                                                                    msg.Style.Add("color", "#FF0000");
                                                                    msg.InnerHtml = "<i class='fa fa-close icon'></i> No mobile number for MA. Please contact our back office.";
                                                                }
                                                                else
                                                                {
                                                                    if (u.IsLoginLocked == 1)
                                                                    {
                                                                        if (IsReCaptchValid() == false)
                                                                        {
                                                                            msg.Style.Add("color", "#FF0000");
                                                                            msg.InnerHtml = "<i class='fa fa-close icon'></i> Please verify yourself.";
                                                                        }
                                                                        else
                                                                        {
                                                                            if (toMobileNumber.Contains("-"))
                                                                            {
                                                                                toMobileNumber = toMobileNumber.Replace("-", "");
                                                                            }
                                                                            if (toMobileNumber.Contains(" "))
                                                                            {
                                                                                toMobileNumber = toMobileNumber.Replace(" ", "");
                                                                            }

                                                                            int length = toMobileNumber.Length;
                                                                            string displayMoileNumber = new String('X', length - 4) + toMobileNumber.Substring(length - 4);
                                                                            string siteURL = ConfigurationManager.AppSettings["siteURL"];
                                                                            unlockMessage.Visible = false;
                                                                            Email email = new Email();
                                                                            email.user = u;
                                                                            email.link = siteURL + "ActivateAccount.aspx";

                                                                            var unique_code = CustomGenerator.GenerateSixDigitPin();
                                                                            User user = u;
                                                                            user.EmailCode = unique_code;

                                                                            user.ModifiedDate = DateTime.Now;
                                                                            IUserService.UpdateData(user);

                                                                            EmailService.SendUnlockAccLink(email, unique_code);
                                                                            unlockMessage.Visible = false;
                                                                            msg.Style.Add("color", "#5cb85c");
                                                                            msg.InnerHtml = "<i class='fa fa-check icon'></i>An activation link will be send to " + EmailId + " .<br/>If your mobile number is outdated, please contact our <a href='/Contact.aspx'>Customer Service</a> or visit our office if you need further assistance.";
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        msg.Style.Add("color", "#FF0000");
                                                                        msg.InnerHtml = "<i class='fa fa-close icon'></i> Account is not locked.";
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                msg.Style.Add("color", "#FF0000");
                                                                msg.InnerHtml = "<i class='fa fa-close icon'></i> This MA is not registered.";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            msg.Style.Add("color", "#FF0000");
                                                            msg.InnerHtml = "<i class='fa fa-close icon'></i> " + responseUAList1.Message;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                msg.Style.Add("color", "#FF0000");
                                                msg.InnerHtml = "<i class='fa fa-close icon'></i> No MA record found.";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        msg.Style.Add("color", "#FF0000");
                                        msg.InnerHtml = "<i class='fa fa-close icon'></i> MA is not registered with this email.";
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                                }
                            }
                        }
                        else
                        {
                            msg.Style.Add("color", "#FF0000");
                            msg.InnerHtml = "<i class='fa fa-close icon'></i> No user record found";
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUList.Message + "\", '');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Unlock Account Modal btnRequestCode_Click " + ex.Message);
            }
        }

        public bool IsReCaptchValid()
        {
            var response = Request["g-recaptcha-response"];
            string secretKey = "6LfXd58UAAAAACGUfwpdtRDFOlKFHjxCI5JSUeAd";
            var client = new WebClient();
            var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            var obj = JObject.Parse(result);
            var status = (bool)obj.SelectToken("success");

            if (status == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        public void FPXPaymentH(
            string orderNo, string transDate, string txn_amt,
            string buyer_mail_id, string buyerName, string buyerBankBranch,
            string buyerAccNo, string makerName, string buyerIBAN,
            string buyerBankId)
        {
            Controller c = new Controller();
            String checksum = "";
            Random RandomNumber = new Random();
            String fpx_msgType = "AE";
            String fpx_msgToken = "01";
            String fpx_sellerExId = "EX00011078";
            String fpx_sellerExOrderNo = orderNo;
            String fpx_sellerOrderNo = orderNo;
            String fpx_sellerTxnTime = transDate;
            String fpx_sellerId = "SE00043740";
            String fpx_sellerBankCode = "01";
            String fpx_txnCurrency = "MYR";
            String fpx_txnAmount = txn_amt;
            String fpx_buyerEmail = buyer_mail_id;
            String fpx_buyerId = buyer_mail_id;
            String fpx_buyerName = buyerName;
            String fpx_buyerBankId = buyerBankId;
            String fpx_buyerBankBranch = buyerBankBranch;
            String fpx_buyerAccNo = buyerAccNo;
            String fpx_makerName = makerName;
            String fpx_buyerIban = buyerIBAN;
            String fpx_productDesc = "PD";
            String fpx_version = "6.0";
            String fpx_checkSum = "";
            fpx_checkSum = fpx_buyerAccNo + "|" + fpx_buyerBankBranch + "|" + fpx_buyerBankId + "|" + fpx_buyerEmail + "|" + fpx_buyerIban + "|" + fpx_buyerId + "|" + fpx_buyerName + "|";
            fpx_checkSum += fpx_makerName + "|" + fpx_msgToken + "|" + fpx_msgType + "|" + fpx_productDesc + "|" + fpx_sellerBankCode + "|" + fpx_sellerExId + "|";
            fpx_checkSum += fpx_sellerExOrderNo + "|" + fpx_sellerId + "|" + fpx_sellerOrderNo + "|" + fpx_sellerTxnTime + "|" + fpx_txnAmount + "|" + fpx_txnCurrency + "|" + fpx_version;
            fpx_checkSum = fpx_checkSum.Trim();

            checksum = c.RSASign(fpx_checkSum, Request.PhysicalApplicationPath + "EX00011078.key"); //Exchange Key name 
            String finalMsg = fpx_checkSum;
            string FPXPostPaymentUrl = ConfigurationManager.AppSettings["FPXPaymentStatusUrl"].ToString();
            var formPostText = @"<html><body><div>
                    <form method='POST' action='" + FPXPostPaymentUrl + @"' name='FPXPaymentForm'>
                      <input type='hidden' name='txn_amt' value='" + fpx_txnAmount + @"' /> 
                      <input type='hidden' value='" + fpx_msgType + @"' name='fpx_msgType'>

                      <input type='hidden' value='" + fpx_msgToken + @"' name='fpx_msgToken'>
                      <input type='hidden' value='" + fpx_sellerExId + @"' name='fpx_sellerExId'>
                      <input type='hidden' value='" + fpx_sellerExOrderNo + @"' name='fpx_sellerExOrderNo'>
                      <input type='hidden' value='" + fpx_sellerTxnTime + @"' name='fpx_sellerTxnTime'>
                      <input type='hidden' value='" + fpx_sellerOrderNo + @"' name='fpx_sellerOrderNo'>
                      <input type='hidden' value='" + fpx_sellerBankCode + @"' name='fpx_sellerBankCode'>
                      <input type='hidden' value='" + fpx_txnCurrency + @"' name='fpx_txnCurrency'>
                      <input type='hidden' value='" + fpx_txnAmount + @"' name='fpx_txnAmount'>
                      <input type='hidden' value='" + fpx_buyerEmail + @"' name='fpx_buyerEmail'>
                      <input type='hidden' value='" + checksum + @"' name='fpx_checkSum'>
                      <input type='hidden' value='" + fpx_buyerName + @"' name='fpx_buyerName'>
                      <input type='hidden' value='" + fpx_buyerBankId + @"' name='fpx_buyerBankId'>
                      <input type='hidden' value='" + fpx_buyerBankBranch + @"' name='fpx_buyerBankBranch'>
                      <input type='hidden' value='" + fpx_buyerAccNo + @"' name='fpx_buyerAccNo'>
                      <input type='hidden' value='" + fpx_buyerId + @"' name='fpx_buyerId'>
                      <input type='hidden' value='" + fpx_makerName + @"' name='fpx_makerName'>
                      <input type='hidden' value='" + fpx_buyerIban + @"' name='fpx_buyerIban'>
                      <input type='hidden' value='" + fpx_productDesc + @"' name='fpx_productDesc'>
                      <input type='hidden' value='" + fpx_version + @"' name='fpx_version'>
                      <input type='hidden' value='" + fpx_sellerId + @"' name='fpx_sellerId'>
                      <input type='hidden' value='" + checksum + @"' name='checkSum_String'>
                     
                    </form></div><script type='text/javascript'>document.FPXPaymentForm.submit();</script></body></html>
                    ";
            Response.Write(formPostText);
        }

        protected void btnLoginOpen_Click(object sender, EventArgs e)
        {
            IndexWelcomeSection.Attributes.Remove("class");
            IndexWelcomeSection.Attributes.Add("class", "welcome-section hide");
            IndexLoginSection.Attributes.Remove("class");
            IndexLoginSection.Attributes.Add("class", "login-section");
        }
    }
}