﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MoreLinq;
using DiOTP.Utility.Helper;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.WebApp
{
    public partial class TaxInvoice : System.Web.UI.Page
    {
        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUserAccountObj2 = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUserAccountObj2.Value; } }

        private static readonly Lazy<IUserOrderFileService> lazyUserOrderFileObj = new Lazy<IUserOrderFileService>(() => new UserOrderFileService());

        public static IUserOrderFileService IUserOrderFileService { get { return lazyUserOrderFileObj.Value; } }

        private static readonly Lazy<IUserOrderStatementService> lazyUserStatementServiceObj = new Lazy<IUserOrderStatementService>(() => new UserOrderStatementService());
        public static IUserOrderStatementService IUserOrderStatementService { get { return lazyUserStatementServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                if (Session["isVerified"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                }
                if (!IsPostBack)
                {
                    if (Session["user"] != null)
                    {
                        User user = (User)Session["user"];

                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                            string all = String.Join(",", userAccounts.Select(x => "'" + x.Id + "'").ToArray());
                            ddlFundAccount.Items.Add(new ListItem("All", all));
                            foreach (UserAccount userAccount in userAccounts)
                            {
                                ddlFundAccount.Items.Add(new ListItem(userAccount.AccountNo, userAccount.Id.ToString()));
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }

        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            bindOrderFile();
        }

        public void bindOrderFile()
        {
            User user = (User)Session["user"];
            taxInvoiceTbody.InnerHtml = "";
            if (!(txtDateStart.Text.Equals("") && txtDateEnd.Text.Equals("")))
            {
                DateTime startDate = Convert.ToDateTime(txtDateStart.Text);
                DateTime endDate = Convert.ToDateTime(txtDateEnd.Text);
                string startDateString = startDate.ToString("yyyy/MM/dd");
                string endDateString = endDate.ToString("yyyy/MM/dd");
                string filter = " user_account_id in (" + ddlFundAccount.SelectedValue + ") and user_id='" + user.Id.ToString() + "' and created_date between '" + startDateString + " 00:00:00' and '" + endDateString + " 23:59:59'";
                filter += " and order_type != 6 and ((order_status = 3) or (payment_method = '3' and order_status = 2)) group by order_no";
                if (startDate < endDate)
                {
                    Response responseUOList = IUserOrderService.GetDataByFilter(filter, 0, 0, true);
                    if (responseUOList.IsSuccess)
                    {
                        List<UserOrder> userOrders = (List<UserOrder>)responseUOList.Data;
                        userOrders = userOrders.DistinctBy(z => z.OrderNo).ToList();

                        if (userOrders.Count > 0)
                        {
                            StringBuilder sb = new StringBuilder();
                            foreach (UserOrder x in userOrders)
                            {
                                UserOrderStatement tax = new UserOrderStatement();
                                UserOrderStatement cas = new UserOrderStatement();
                                UserOrderStatement creditnote = new UserOrderStatement();

                                Response responseUAList = IUserAccountService.GetDataByFilter(" user_id='" + x.UserId + "' and id = '" + x.UserAccountId + "' and status=1 ", 0, 0, true);
                                if (responseUAList.IsSuccess)
                                {
                                    UserAccount userAccount = ((List<UserAccount>)responseUAList.Data).FirstOrDefault();
                                    
                                    Response responseUOSList = IUserOrderStatementService.GetDataByPropertyName(nameof(UserOrderStatement.RefNo), x.OrderNo, true, 0, 0, true);
                                    if (responseUOSList.IsSuccess)
                                    {
                                        List<UserOrderStatement> userorderstatement = (List<UserOrderStatement>)responseUOSList.Data;

                                        if (userorderstatement != null)
                                        {
                                            if (x.IsTaxInvoice == 1)
                                                tax = userorderstatement.Where(a => a.UserOrderStatementTypeId == 1).FirstOrDefault();
                                            if (x.IsCas == 1)
                                                cas = userorderstatement.Where(a => a.UserOrderStatementTypeId == 2).FirstOrDefault();
                                            if (x.IsCreditNote == 1)
                                                creditnote = userorderstatement.Where(a => a.UserOrderStatementTypeId == 3).FirstOrDefault();
                                        }
                                    }
                                    sb.Append(
                                        @"<tr>
                                <td style='text-align: center'>" + x.OrderNo + @"</td>
                                <td style='text-align: center'>" + userAccount.AccountNo + @"</td>
                                <td style='text-align: center'>" + x.CreatedDate + @"</td>
                                <td style='text-align: center'>" + (x.IsTaxInvoice == 0 || tax == null ? "-" : ("<a href='" + tax.Url + @"' target='_blank'><i class='fa fa-search'></i></a>")) + @"</td>
                                <td style='text-align: center'>" + (x.IsCas == 0 || cas == null ? "-" : ("<a href='" + cas.Url + @"' target='_blank'><i class='fa fa-search'></i></a>")) + @"</td>
                                <td style='text-align: center'>" + (x.IsCreditNote == 0 || creditnote == null ? "-" : ("<a href='" + creditnote.Url + @"' target='_blank'><i class='fa fa-search'></i></a>")) + @"</td>
                            </tr>");
                                    
                                }
                            }
                            taxInvoiceTbody.InnerHtml = sb.ToString();
                        }
                        else
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append(
                                    @"
                            <tr>
                                <td colspan='6' align='center'>No transaction record found.</td>
                            </tr>"
                                    );

                            taxInvoiceTbody.InnerHtml = sb.ToString();
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUOList.Message + "\", '');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Date(From) cannot be greater than Date(To)', '');", true);
                }
            }
            else
            {
                if (txtDateStart.Text.ToString() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please select a Date(From)', '');", true);
                }
                else if (txtDateEnd.Text.ToString() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please select a Date(To)', '');", true);
                }
            }
        }
    }
}