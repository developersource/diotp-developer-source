﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class RSPHistory : System.Web.UI.Page
    {
        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUserAccountObj2 = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUserAccountObj2.Value; } }

        private static readonly Lazy<IUserOrderFileService> lazyUserOrderFileObj = new Lazy<IUserOrderFileService>(() => new UserOrderFileService());

        public static IUserOrderFileService IUserOrderFileService { get { return lazyUserOrderFileObj.Value; } }

        public int UserChoosetype;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Login.aspx'", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "alert('Please Login to Proceed.'); window.location.href='Index.aspx'", true);
                }

                if (Session["user"] != null)
                {
                    User user = (User)Session["user"];

                    string typeString = Request.QueryString["type"];
                    int type = 0;
                    if (typeString == null || typeString == "")
                    {
                        UserChoosetype = 1;
                        type = 6;
                    }
                    else
                    {
                        UserChoosetype = Convert.ToInt32(typeString);
                        type = Convert.ToInt32(typeString);
                    }
                    HtmlControl li = (HtmlControl)tabsDiv.FindControl("type" + type);
                    li.Attributes.Add("class", "active");
                    Response responseUAList = IUserAccountService.GetDataByPropertyName(nameof(UserAccount.UserId), user.Id.ToString(), true, 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                        UserAccount primaryAcc = new UserAccount();
                        MaHolderReg maHolderReg = new MaHolderReg();
                        if (Session["SelectedAccountHolderNo"] != null)
                        {
                            primaryAcc = userAccounts.Where(x => x.AccountNo == Session["SelectedAccountHolderNo"].ToString()).FirstOrDefault();

                            Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                            if (responseMHR.IsSuccess)
                            {
                                maHolderReg = (MaHolderReg)responseMHR.Data;
                            }
                        }
                        spanUsername.InnerHtml = primaryAcc.AccountNo + " - " + maHolderReg.Name1;

                        string columnNameUserAccountId = Converter.GetColumnNameByPropertyName<UserOrder>(nameof(UserOrder.UserAccountId));
                        string columnNameOrderType = Converter.GetColumnNameByPropertyName<UserOrder>(nameof(UserOrder.OrderType));
                        string columnNameCreatedDate = Converter.GetColumnNameByPropertyName<UserOrder>(nameof(UserOrder.CreatedDate));

                        string Approvetype = "";
                        string period = "";


                        StringBuilder filter = new StringBuilder();

                        if (Request.QueryString["Approvetype"] != null && Request.QueryString["period"] != null)
                        {
                            Approvetype = Request.QueryString["Approvetype"].ToString();
                            period = Request.QueryString["period"].ToString();

                            if (Approvetype != "10" && period != "0")
                                filter.Append(columnNameUserAccountId + " = '" + primaryAcc.Id + "' and " + columnNameOrderType + " = '" + type + "' and " + columnNameCreatedDate + " >= '" + DateTime.Now.AddMonths(Convert.ToInt32(period)).ToString("yyyy-MM-dd hh:mm:ss") + "' and " + columnNameCreatedDate + " <= '" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "' and status = '" + Approvetype + "'");
                            else if (Approvetype != "10" && period == "0")
                                filter.Append(columnNameUserAccountId + " = '" + primaryAcc.Id + "' and " + columnNameOrderType + " = '" + type + "' and status = '" + Approvetype + "'");
                            else if (Approvetype == "10" && period != "0")
                                filter.Append(columnNameUserAccountId + " = '" + primaryAcc.Id + "' and " + columnNameOrderType + " = '" + type + "' and " + columnNameCreatedDate + " >= '" + DateTime.Now.AddMonths(Convert.ToInt32(period)).ToString("yyyy-MM-dd hh:mm:ss") + "' and " + columnNameCreatedDate + " <= '" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "'");
                            else if (Approvetype == "10" && period == "0")
                                filter.Append(columnNameUserAccountId + " = '" + primaryAcc.Id + "' and " + columnNameOrderType + " = '" + type + "'");
                        }
                        else
                            filter.Append(columnNameUserAccountId + " = '" + primaryAcc.Id + "' and " + columnNameOrderType + " = '" + type + "'");

                        if (!IsPostBack)
                        {
                            ddlTransactionPeriod.SelectedIndex = ddlTransactionPeriod.Items.IndexOf(ddlTransactionPeriod.Items.FindByValue(period));
                            ddlTransactionType.SelectedIndex = ddlTransactionType.Items.IndexOf(ddlTransactionType.Items.FindByValue(Approvetype));
                        }
                        //if (ddlTransactionType.SelectedValue != "10")
                        //            //    filter.Append(columnNameUserAccountId + " = '" + primaryAcc.Id + "' and " + columnNameOrderType + " = '" + type + "' and " + columnNameCreatedDate + " >= '" + DateTime.Now.AddMonths(Convert.ToInt32(ddlTransactionPeriod.SelectedValue)).ToString("dd-MM-yyyy hh:mm:ss") + "' and " + columnNameCreatedDate + " <= '" + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss") + "' and status = '" + ddlTransactionType.SelectedValue.ToString() + "'");
                        //            //else
                        //            //    filter.Append(columnNameUserAccountId + " = '" + primaryAcc.Id + "' and " + columnNameOrderType + " = '" + type + "' and " + columnNameCreatedDate + " >= '" + DateTime.Now.AddMonths(Convert.ToInt32(ddlTransactionPeriod.SelectedValue)).ToString("dd-MM-yyyy hh:mm:ss") + "' and " + columnNameCreatedDate + " <= '" + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss") + "'");



                        //List<UserOrder> userOrders = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.UserAccountId), primaryAcc.Id.ToString(), true, 0, 0, true);
                        int skip = 0, take = 10;
                        if (hdnCurrentPageNo.Value == "")
                        {
                            skip = 0;
                            take = 10;
                            hdnNumberPerPage.Value = "10";
                            hdnCurrentPageNo.Value = "1";
                            hdnTotalRecordsCount.Value = IUserOrderService.GetCountByFilter(filter.ToString()).ToString();
                        }
                        else
                        {
                            skip = (Convert.ToInt32(hdnCurrentPageNo.Value) - 1) * 10;
                            take = 10;
                        }

                        StringBuilder sb = new StringBuilder();
                        if (type == 6)
                        {
                            sb.Append(@"<tr>
                                    <th>No</th>
                                    <th>Order Number</th>
                                    <th>Fund Name</th>
                                    <th>Order Date</th>
                                    <th class='text-right'>Investment Amount
                                        <br />
                                        MYR</th>
                                    <th class='text-right'>Order Status</th>
                                </tr>");
                        }
                        theadUserOrders.InnerHtml = sb.ToString();
                        Response responseUOList = IUserOrderService.GetDataByFilter(filter.ToString() + "group by order_no", skip, take, false);
                        if (responseUOList.IsSuccess)
                        {
                            List<UserOrder> userOrders = (List<UserOrder>)responseUOList.Data;

                            if (userOrders.Count > 0)
                            {
                                string status = "";
                                int i = 1;
                                sb = new StringBuilder();
                                if (type == 6)
                                {
                                    foreach (UserOrder uo in userOrders)
                                    {
                                        if (uo.Status == 0)
                                            status = "Pending";
                                        else if (uo.Status == 1)
                                            status = "Approved";
                                        else
                                            status = "Denied";
                                        
                                        Response response = IUtmcFundInformationService.GetSingle(uo.FundId);
                                        if (response.IsSuccess)
                                        {
                                            UtmcFundInformation fund = (UtmcFundInformation)response.Data;
                                            sb.Append(@"<tr>
                                                    <td>" + i + @"</td>
                                                    <td><a href='OrderConfirmation.aspx?OrderNo=" + uo.OrderNo + "' target='_blank'>" + uo.OrderNo + @"</a></td>
                                                    <td>" + fund.FundName + @"</td>
                                                    <td>" + uo.CreatedDate.ToString("dd/MM/yyyy") + @"</td>
                                                    <td class='text-right'><span class='currencyFormatNoSymbol'>" + uo.Amount + @"</span></td>
                                                    <td class='text-right'>" + status + @"</td>
                                                </tr>");
                                            i++;
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                                "alert('" + response.Message + "');", true);
                                        }
                                    }
                                }
                                tbodyUserOrders.InnerHtml = sb.ToString();
                            }
                            // }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                                "alert('" + responseUOList.Message + "');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure",
                            "alert('" + responseUAList.Message + "');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("OrderHistory Page load: " + ex.Message);
            }
        }
        protected void btnsearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("RSPHistory.aspx?Approvetype=" + ddlTransactionType.SelectedValue + "&period=" + ddlTransactionPeriod.SelectedValue);
        }

        protected void btnreset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RSPHistory.aspx");
        }
    }
}