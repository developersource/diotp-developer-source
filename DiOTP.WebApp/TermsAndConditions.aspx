﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="TermsAndConditions.aspx.cs" Inherits="DiOTP.WebApp.TermsAndConditions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Index.aspx">Home</a></li>
                            <li class="active">Terms & Conditions</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Terms & Conditions</h3>
                    </div>

                    <p>The following terms apply to your access and use of this portal (eapexis.apexis.com.my). By accessing to this portal, you agree to be bound by these terms without limitation and qualification. Apex Investment Services Berhad (“AISB”) may periodically modify these Terms of Use without prior notice. If you do not agree to or are not satisfied with the provision herein, you may choose to discontinue using this portal.</p>
                    <h4>Limited License and Restrictions of use</h4>
                    <p>AISB grants you a limited, revocable, non-exclusive, non-transferable license to view, save/store, bookmark, download, and print the pages within this portal solely for your personal, informational and non-commercial use or as expressly authorized by AISB herein.  You are responsible for obtaining and maintaining all equipment, services, and other materials that you need to access this portal.  AISB reserves all rights not expressly granted in these Terms of Use. Except as otherwise stated in these Terms of Use, you may not:</p>
                    <p>Modify, distribute, transmit, post, reproduce, republish, broadcast, create derivative works from, transfer, sell, or exploit any of the Materials;</p>
                    <p>Redeliver any page, text, image on this portal or any of the Materials using “framing” or other technology other than those made generally available by AISB;</p>
                    <p>Engage in any conduct that could damage, disable, or overburden this portal, any of the Materials, or any systems, networks or servers related to this portal, including without limitation, using devices or software that provide repeated automated access to this portal, other than those made generally available by AISB;</p>
                    <p>Probe, scan, or test the vulnerability of any Materials, systems, networks or servers ralted to this portal or attempt to gain unauthorized access to Materials, services, systems, networks, servers, or accounts connected or associated with this portal through hacking, password or data mining, or any other means of circumventing any access-limiting, user authentication or security device of any Materials, systems, networks or servers related to this portal;</p>
                    <p>Modify, copy, obscure, remove, or display AISB’s name, logo, trademarks, text, notices, or images.</p>
                    <h4>Information on the Portal</h4>
                    <p>The information provided in this portal is relevant to Malaysian residents only. References to currency shall mean Malaysian Ringgit unless specified otherwise. The value of your investment(s) as stated on the portal is based on the last published unit prices and on the unit balance currently recorded. The unit prices shown may differ from the actual unit price for your investment. The unit balance may not include recent transactions that have not yet been processed. Your actual unit price will be confirmed upon receipt of a completed transaction request. Prices for any particular business day will be available on the AISB’s website after 7:00 pm. However, delays may occur in updating the unit prices.</p>
                    <p>Prices for the AISB Foreign Fund will be one day behind as the fund's closing price cannot be determined at 7.00pm Malaysian time. The information contained on this portal has been derived from sources believed to be reliable and accurate and the same is given in good faith. Neither AISB nor any of their employees or directors gives any warranty of reliability or accuracy nor accepts any responsibility arising in any other way including by reason of negligence for errors or omissions herein. This portal contains general information only and should not be considered as a comprehensive statement on any matter and should not be relied upon as such. This information doesn't account for your investment objectives, particular needs or financial situation. These should be considered before investing and we recommend that you consult your banker, lawyer, stockbroker or an independent financial adviser. The information on this portal is based on current taxation laws and their interpretation. The levels and basis of taxation may change. You should, therefore, seek professional advice on the taxation implications of investing and should not rely on the information provided which should be used as a guide only.</p>
                    <h4>Online User</h4>
                    <p>For authentication purposes, certain features on this portal may require you to submit your personal code to access these features.  You agree to provide AISB with current, complete, and accurate information as prompted by the applicable registration process and agree to regularly update this information to maintain its completeness and accuracy.  You agree not to (i) obtain or attempt to obtain unauthorized access or (ii) gain access or use, any means not intentionally made available to you by AISB, to such parts of or features or to any other protected Materials or information on this portal.</p>
                    <p>You are responsible for maintaining the confidentiality of any account information, user name, logins and passwords that you use to access any page or feature on this portal, and for logging off your account and any protected areas of the portal.  Further, you are fully responsible for all activities occurring under our accounts, user names, logins and passwords that result from your omission, negligence, carelessness, misconduct, or failure to use or maintain appropriate security measures. If you become aware of any suspicious or unauthorized conduct concerning your accounts, user names, login or passwords, you agree to inform AISB immediately. AISB will not be liable for any loss or damage arising from your failure to meet your responsibilities stated in this paragraph.</p>
                    <p>Promotional and marketing materials may be electronically transmitted to the e-mail address you provide to us when you use the products and services on this portal.  You may opt to stop receiving such e-mails.</p>
                    <h4>Copyright</h4>
                    <p>AISB owns the copyright in the information contained on this portal. The information on this portal may be viewed on-line and be reproduced in hard copy only for your reference. No part of this portal may be commercially dealt with in any manner or linked to any other website / portal without the express prior written consent of AISB.</p>
                    <h4>Limitation of Liability</h4>
                    <p><b>AISB</b>, under no circumstances whatsoever, be liable for any loss or damage special, incidental or consequential arising directly or indirectly including but not limited to loss of use, profit, hardware or software failure or malfunction that arise in connection with this portal from any access, use or the inability to access or use of this portal.</p>
                    <h4>Termination</h4>
                    <p>The rights granted to you herein shall terminate immediately upon any violation by you of these Terms of Use.</p>
                    <p>AISB in its sole discretion, reserves the right to temporarily or permanently terminate your access to and use of the portal at any time and for any reason whatsoever, without notice or liability.  AISB will not be liable to you or any third party for any termination of your access to or use of the portal.</p>
                </div>


            </div>
        </div>
    </section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
