﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="Help-Center.aspx.cs" Inherits="DiOTP.WebApp.Help_Center" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Content/MyCJ/Help.css" rel="stylesheet" />
    <link href="Content/assets/css/custom.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

            

    <section class="Help">
        <div class="container-fluid">
            <div class="row">

                <aside class="sidebar sidebar-boxed col-md-3 hidden-sm hidden-xs">
                    <ul class="sidenav dropable">
                        <li>
                            <a href="#">Beginner guide</a>
                            <ul>
                                <li><a href="#OpenAccount">How to Open an Account?</a></li>
                                <li><a href="#RegisterExisting">How to register an account as an existing Apex Client?</a></li>
                                <li><a href="#MaActivation">MA Activation</a></li>
                                <li><a href="#AdditionalMa">Additional MA accounts</a></li>
                                <li><a href="#ForgotPassword">Forgot Password?</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Google Authenticator</a>
                            <ul>
                                <li><a href="#UseGoogleAuthenticator">How to use Google Authenticator</a></li>
                                <li><a href="#ResetGoogleAuthenticator">Reset Google Authenticator if lost</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Phone Verification</a>
                            <ul>
                                <li><a href="#MobileAuthentication">How to use phone verification</a></li>
                                <li><a href="#LostMobile">What if i lost mobile</a></li>
                            </ul>
                        </li>
                        <%--<li>
                            <a href="#">Fund Details</a>
                            <ul>
                                <li><a href="#">Where can I get entire fund details?</a></li>
                                <li><a href="#">How to compare funds for detailed information?</a></li>
                                <li><a href="#PricingDetails">Pricing Details</a></li>
                            </ul>
                        </li>--%>
                        <li>
                            <a href="#">Trading</a>
                            <ul>
                                <li><a href="#HowToBuy">How to buy?</a></li>
                                <li><a href="#HowToSell">How to sell?</a></li>
                                <li><a href="#HowToSwitch">How to switch?</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Edit details</a>
                            <ul>
                                <li><a href="#ChangePassword">How to change password?</a></li>
                                <li><a href="#UpdateBankDetails">How to update bank details?</a></li>
                                <li><a href="#ChangeAddress">How to update address?</a></li>
                                <li><a href="#ChangeOccupation">How to update occupation details?</a></li>
                            </ul>
                        </li>
                        <li><a href="#PrivacyPolicy">Privacy Policy</a></li>
                        <li><a href="#faq">FAQ's</a></li>
                    </ul>
                </aside>

                <div class="mobile-help hidden-lg hidden-md">
                    <select id="my-select" class="selectpicker">
                        <optgroup label="Beginner guide">
                            <option id="#OpenAccount">How to Open an Account?</option>
                            <li><a href="#RegisterExisting">How to register an account as an existing Apex Client?</a></li>
                            <option id="#MaActivation">MA Activation </option>
                            <option id="#AdditionalMa">Additional MA accounts</option>
                            <option id="#ForgotPassword">Forgot password?</option>
                        </optgroup>
                        <optgroup label="Google Authenticator">
                            <option id="#UseGoogleAuthenticator">How to use google authenticator</option>
                            <option id="#ResetGoogleAuthenticator">Reset Google Authenticator if lost</option>
                        </optgroup>
                        <optgroup label="Phone verification">
                            <option id="#MobileAuthentication">How to use verification</option>
                            <option id="#LostMobile">What if i lost mobile</option>
                        </optgroup>
                        <optgroup label="Fund details">
                            <%--<option id="">Where can I get entire fund details?</option>
                            <option id="">How to compare funds for detailed information?</option>--%>
                            <%--<option id="#PricingDetails">Pricing Details</option>--%>
                        </optgroup>
                        <optgroup label="Trading">
                            <option id="#HowToBuy">How to buy?</option>
                            <option id="#HowToSell">How to sell?</option>
                            <option id="#HowToSwitch">How to switch?</option>
                        </optgroup>
                        <optgroup label="Edit details">
                            <option id="#ChangePassword">How to change password?</option>
                            <option id="#UpdateBankDetails">How to update bank details?</option>
                            <option id="#ChangeAddress">How to update address?</option>
                            <option id="#ChangeOccupation">How to update occupation details?</option>
                        </optgroup>
                        <option id="#PrivacyPolicy">Privacy Policy</option>
                        <option id="#faq">FAQ's</option>
                    </select>
                </div>

                <div class="col-md-9 col-md-offset-3">
                        <div class="help-wrapper">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-right">
                                        <ol class="breadcrumb mb-5">
                                            <li><a href="/Index.aspx" id="homeLink" runat="server">Home</a></li>
                                            <li class="active">Help</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-12">

                                <div id="OpenAccount">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>How to Open An Account</h6>
                                        </div>
                                        <div class="help-content">
                                            <p><b>Step 1: </b>Browse <a href="https://eapexis.apexis.com.my">eapexis.apexis.com.my</a> website.</p>
                                            <p><b>Step 2: </b>In home page click <span class="help-highlight">Open an Account</span> button.</p>
                                            <img src="/Content/Help/1a.jpg" alt="How to Register" class="center-block img-responsive" height="300" />
                                            <p><b>Step 3: </b>In the next page click <span class="help-highlight">Individual Account</span> button.</p>
                                            <img src="/Content/Help/OAC1.jpg" alt="How to Register" class="center-block img-responsive" height="300" />
                                            <p><b>Step 4: </b>In the next page click <span class="help-highlight">Next</span> button to proceed to filling in your personal information.</p>
                                            <p><b>Step 5: </b>Take note that, there will be 2 verification methods required before you can proceed. This includes mobile and email verification as shown below.</p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <img src="/Content/Help/OACMobileVal.jpg" alt="How to Register" class="center-block img-responsive" height="300" />
                                                </div>
                                                <div class="col-md-6">
                                                        <img src="/Content/Help/OACEmailVal.jpg" alt="How to Register" class="center-block img-responsive" height="300" />
                                                    </div>
                                                </div>                                            
                                            <p><b>Step 6: </b>For mobile verification, you will receive a code via SMS. Meanwhile, you will receive a code via email as shown below.</p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <img src="/Content/Help/OACSMS.jpg" alt="How to Register" class="center-block img-responsive" height="300" style="margin-top:100px;"/>
                                                </div>
                                                <div class="col-md-6">
                                                    <img src="/Content/Help/OAC7.jpg" alt="How to Register" class="center-block img-responsive" height="300" />
                                                </div>
                                            </div>
                                            
                                            <p><b>Step 7: </b>After completing the account opening process you will receive emails regarding your completion of registration and is pending Apex Admin approval.</p>
                                            <img src="/Content/Help/OAC4.jpg" alt="How to Register" class="center-block img-responsive" height="300" />
                                            <p><b>Step 8: </b>Once the Apex admin has appoved the application, you will receive two more emails regarding your account information as shown below to complete your account opening.</p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <img src="/Content/Help/OAC5.jpg" alt="How to Register" class="center-block img-responsive" height="300"/>
                                                </div>
                                                <div class="col-md-6">
                                                    <img src="/Content/Help/OAC6.jpg" alt="How to Register" class="center-block img-responsive" height="300" />
                                                </div>
                                            </div>
                                            
                                            <p><b>Step 9: </b>You may now login to your investor account in the eApexis investor portal.</p>
                                        </div>
                                    </div>
                                </div>
                            
                                <div id="RegisterExisting">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>How to register an account as an existing Apex Client?</h6>
                                        </div>
                                        <div class="help-content">
                                            <p><b>Step 1: </b>Browse <a href="https://eapexis.apexis.com.my">eapexis.apexis.com.my</a> website.</p>
                                            <p><b>Step 2: </b>In home page click <span class="help-highlight">"Dont have an account? Register here"</span> button.</p>
                                            <img src="/Content/Help/1d.jpg" alt="How to Register" class="center-block img-responsive" height="300" />                                                                                        
                                            <p><b>Step 3: </b>Fill the corresponding details in the registration form & click on <span class="help-highlight">Request Code</span> button.</p>
                                            <img src="/Content/Help/1b.jpg" alt="Registration Page" class="center-block img-responsive" height="300" />
                                            <p><b>Step 4: </b>On a successful submission, the following confirmation message will be shown. Click proceed to send a verification email to your email entered in the form. Open your email and click the account activation link.</p>
                                            <img src="/Content/Help/1c.jpg" alt="Confirmation Page" class="center-block img-responsive" height="300" width="500" />
                                            <p><b>Step 5: </b>After clicking activate link in email, user will automatically send to <span class="help-highlight">Set up Password</span>. Click Submit and you are good to login.</p>
                                        </div>
                                    </div>
                                

                                <div id="MaActivation">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>First Time MA Activation</h6>
                                        </div>
                                        <div class="help-content">
                                            <p><b>Step 1: </b>Browse <a href="https://eapexis.apexis.com.my">eapexis.apexis.com.my</a> website.</p>
                                            <p><b>Step 2: </b>Login into your account.</p>
                                            <p><b>Step 3: </b>After successful login, there will be a MA activation form.</p>
                                            <img src="/Content/Help/2.jpg" alt="MA Activation" class="center-block img-responsive" height="300" />
                                            <p><b>Step 4: </b>Click <span class="help-highlight">Request OTP</span>. An OTP will be send to your phone, enter the OTP, and click <span class="help-highlight">Submit</span> to activate.</p>
                                        </div>
                                    </div>
                                </div>

                                <div id="AdditionalMa">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>Additional MA accounts</h6>
                                        </div>
                                        <div class="help-content">
                                            <p><b>Step 1: </b>Browse <a href="https://eapexis.apexis.com.my">eapexis.apexis.com.my</a> website.</p>
                                            <p><b>Step 2: </b>Login into your account.</p>
                                            <p><b>Step 3: </b>In your portfolio page, click <span class="help-highlight">+Add Account</span> in profile section.</p>
                                            <img src="/Content/Help/3a.jpg" alt="Additional MA Accounts" class="center-block img-responsive" height="300" />
                                            <p><b>Step 4: </b>Enter your MA number in the specified field. Click Submit, MA will be added to your account.</p>
                                            <img src="/Content/Help/3b.jpg" alt="Additional MA Accounts" class="center-block img-responsive" height="300" />
                                        </div>
                                    </div>
                                </div>

                                <div id="ForgotPassword">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>Forgot Password?</h6>
                                        </div>
                                        <div class="help-content">
                                            <p><b>Step 1: </b>Browse <a href="https://eapexis.apexis.com.my">eapexis.apexis.com.my</a> website.</p>
                                            <p><b>Step 2: </b>Enter your email id & Click on <span class="help-highlight">Forgot Password</span> in login section.</p>
                                            <img src="/Content/Help/4a.jpg" alt="Forgot Password" height="300" class="center-block img-responsive" />
                                            <p><b>Step 3: </b>You will be redirected to forgot password page. You will have two options i.e., by Email id & Mobie Number.</p>
                                            <p><b>Step 4: </b>Enter your credentials & Submit.</p>
                                            <img src="/Content/Help/4b.jpg" alt="Forgot Password" height="300" class="center-block img-responsive" />
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p><b>Step 4: </b>If you have choosed the email one, reset link will be sent to your email. Click the password reset mail, you will be redirected to password reset page.</p>
                                                    <img src="/Content/Help/6.png" alt="Forgot Password" height="300" class="center-block img-responsive" />
                                                </div>
                                                <div class="col-md-6">
                                                    <p><b>Step 4: </b>If you have choosed mobile number, enter your registered mobile number & submit. SMS will be sent to your registered number. Enter the PIN from SMS.</p>
                                                    <img src="/Content/Help/7.png" alt="Forgot Password" height="300" class="center-block img-responsive" />
                                                </div>
                                            </div>
                                            <p><b>Step 5: </b>Enter your new password, Confirm & Submit.</p>
                                        </div>
                                    </div>
                                </div>

                                <div id="UseGoogleAuthenticator">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>Use Google Authenticator</h6>
                                        </div>
                                        <div class="help-content">
                                            <p><b>Step 1: </b>Download Google Authenticator app. Open it and click <span class="help-highlight">Begin setup</span></p>
                                            <p><b>Step 2: </b>Select <span class="help-highlight">Scan Barcode</span> & scan your barcode for registration.</p>
                                            <p><b>Step 3: </b>Save manual key for future use because in case if you have lost authenticator, manual key helps to reset it.</p>
                                            <p><b>Step 4: </b>Enter the 6-digit code of google authenticator generated in your mobile for login.</p>
                                        </div>
                                    </div>
                                </div>

                                <div id="ResetGoogleAuthenticator">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>Reset Google Authenticator</h6>
                                        </div>
                                        <div class="help-content">
                                            <h6>For Android users:</h6>
                                            <p><b>Step 1: </b>Download the Google Authenticator app, if lost. Click on <span class="help-highlight">Begin Setup</span></p>
                                            <p><b>Step 2: </b>Select <span class="help-highlight">Manual Entry key</span> option.</p>
                                            <p><b>Step 3: </b>Enter details in the provided fields.</p>

                                        </div>
                                    </div>
                                </div>

                                <div id="MobileAuthentication">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>Mobile Authentication</h6>
                                        </div>
                                        <div class="help-content">
                                            <h6>For Android users:</h6>
                                            <p><b>Step 1: </b>Browse <a href="https://eapexis.apexis.com.my">eapexis.apexis.com.my</a> website.</p>
                                            <p><b>Step 2: </b>Login into your account.</p>
                                            <p><b>Step 3: </b>On Mobile section in two factor authentication, click on <span class="help-highlight">Request OTP</span>.</p>
                                            <p><b>Step 4: </b>The <span class="help-highlight">OTP</span> will be sent to your binded phone number, enter the <span class="help-highlight">OTP</span> into the web portal.</p>
                                            <p><b>Step 5: </b>Click Submit.</p>
                                        </div>
                                    </div>
                                </div>

                                <div id="LostMobile">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>Lost Mobile</h6>
                                        </div>
                                        <div class="help-content">
                                            <p>Please <a href="/Contact.aspx\" target=\"_blank\">Contact Us</a> or visit our office if you have lost your phone</p>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div id="HowToBuy">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>How to buy?</h6>
                                        </div>
                                        <div class="help-content">
                                            <p><b>Step 1: </b>Browse <a href="https://eapexis.apexis.com.my">eapexis.apexis.com.my</a> website.</p>
                                            <p><b>Step 2: </b>Login into your account.</p>
                                            <p><b>Step 3: </b>Go to Buy page.</p>
                                            <p><b>Step 4: </b>Select the MA account and the fund and amount you want to buy from. Proceed to Cart checkout</p>
                                            <img src="/Content/Help/11b.jpg" alt="How to Buy" height="300" class="center-block img-responsive" />
                                            <p><b>Step 5: </b>Select the method to pay (Attach Proof of Payment/FPX server) and enter your transaction password, proceed to Submit ths order.</p>
                                            <img src="/Content/Help/10c.jpg" alt="How to Buy" height="300" class="center-block img-responsive" />
                                            <p><b>Step 6: </b>Wait for Apex admin to approve your transaction. Once approved, you can see the status at the Order Status.</p>
                                        </div>
                                    </div>
                                </div>

                                <div id="HowToSell">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>How to sell?</h6>
                                        </div>
                                        <div class="help-content">
                                            <p><b>Step 1: </b>Browse <a href="https://eapexis.apexis.com.my">eapexis.apexis.com.my</a> website.</p>
                                            <p><b>Step 2: </b>Login into your account.</p>
                                            <p><b>Step 3: </b>Go to Sell page.</p>
                                            <p><b>Step 4: </b>Select the MA account. Make sure you have bind your bank details to your account.</p>
                                            <p><b>Step 5: </b>Select the fund you want to sell from, click Add and proceed to Cart checkout.</p>
                                            <img src="/Content/Help/10b.jpg" alt="How to Sell" height="300" class="center-block img-responsive" />
                                            <p><b>Step 6: </b>Enter your transaction password and submit your transaction.</p>
                                            <img src="/Content/Help/10c.jpg" alt="How to Sell" height="300" class="center-block img-responsive" />
                                            <p><b>Step 7: </b>Wait for Apex admin to approve your transaction. Once approved, you can see the status at the Order Status.</p>
                                        </div>
                                    </div>
                                </div>

                                <div id="HowToSwitch">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>How to switch?</h6>
                                        </div>
                                        <div class="help-content">
                                            <p><b>Step 1: </b>Browse <a href="https://eapexis.apexis.com.my">eapexis.apexis.com.my</a> website.</p>
                                            <p><b>Step 2: </b>Login into your account.</p>
                                            <p><b>Step 3: </b>Go to Switch page.</p>
                                            <p><b>Step 4: </b>Select the MA account and the fund you want to switch from and the fund you want to switch to.</p>
                                            <p><b>Step 5: </b>Enter the units you want to switch, click Add and proceed to Cart checkout.</p>
                                            <img src="/Content/Help/12b.jpg" alt="How to Switch" height="300" class="center-block img-responsive" />
                                            <p><b>Step 6: </b>Enter your transaction password and submit your transaction.</p>
                                            <img src="/Content/Help/12c.jpg" alt="How to Switch" height="300" class="center-block img-responsive" />
                                            <p><b>Step 7: </b>Wait for Apex admin to approve your transaction. Once approved, you can see the status at the Order Status.</p>
                                        </div>
                                    </div>
                                </div>

                                <div id="ChangePassword">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>Change password</h6>
                                        </div>
                                        <div class="help-content">
                                            <p><b>Step 1: </b>Browse <a href="https://eapexis.apexis.com.my">eapexis.apexis.com.my</a> website.</p>
                                            <p><b>Step 2: </b>Login into your account.</p>
                                            <p><b>Step 3: </b>Click <span class="help-highlight">User Settings</span> in the left bar once successfully login.</p>
                                            <img src="/Content/Help/7a.jpg" alt="Forgot Password" height="300" class="center-block img-responsive" />
                                            <p><b>Step 4: </b>In User Settings Click <span class="help-highlight">Modify</span> on Change Login Password/Transaction Passwords.</p>
                                            <img src="/Content/Help/7b1.jpg" alt="Change Password" height="300" class="center-block img-responsive" />
                                            <p><b>Step 5: </b>Type in your current login password, enter your new login password and confirm login new password. After that, choose either google authentication or mobile authentication to validate change. At last, click submit to change login password.</p>
                                            <img src="/Content/Help/7c1.jpg" alt="Change Password" height="300" class="center-block img-responsive" />
                                            <p><b>Step 6: </b>Type in your current login password, enter your new transaction password and confirm new transaction password. After that, choose either google authentication or mobile authentication to validate change. At last, click submit to change transaction password.</p>
                                            <img src="/Content/Help/7c2.jpg" alt="Change Password" height="300" class="center-block img-responsive" />
                                        </div>
                                    </div>
                                </div>

                                <div id="UpdateBankDetails">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>How to update bank details?</h6>
                                        </div>
                                        <div class="help-content">
                                            <p><b>Step 1: </b>Browse <a href="https://eapexis.apexis.com.my">eapexis.apexis.com.my</a> website.</p>
                                            <p><b>Step 2: </b>Login into your account.</p>
                                            <p><b>Step 3: </b>Click <span class="help-highlight">User Settings</span> in the left bar once successfully login.</p>
                                            <img src="/Content/Help/7a.jpg" alt="Bank Details" height="300" class="center-block img-responsive" />
                                            <p><b>Step 4: </b>In User Settings Click <span class="help-highlight">View</span> on Bank Details and click Add.</p>
                                            <p><b>Step 5: </b>Select bank, enter your bank number, upload your bank statement file, and authenticate submission either by Google authenticator or mobile authenticator. Click Submit</p>
                                            <img src="/Content/Help/8a.jpg" alt="Bank Details" height="300" class="center-block img-responsive" />
                                            <p><b>Step 6: </b>Wait for Apex admin to approve your submission. Once approved, you can see the status at the User Settings Bank Details.</p>
                                            <p><b>Step 7: </b>Go to Account Settings, select the account you wish to bind to.</p>
                                            <p><b>Step 8: </b>Click View on Bank Details, and click on Bind and you have successfully binded your bank to your account.</p>
                                            <img src="/Content/Help/8b.jpg" alt="Bank Details" height="300" class="center-block img-responsive" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div id="ChangeAddress">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>How to change address?</h6>
                                        </div>
                                        <div class="help-content">
                                            <p><b>Step 1: </b>Browse <a href="https://eapexis.apexis.com.my">eapexis.apexis.com.my</a> website.</p>
                                            <p><b>Step 2: </b>Login into your account.</p>
                                            <p><b>Step 3: </b>Click <span class="help-highlight">Account Settings</span> in the left bar once successfully login.</p>
                                            <p><b>Step 4: </b>In Account Settings Click <span class="help-highlight">View</span> on MA Address and click <span class="help-highlight">Edit</span>.</p>
                                            <img src="/Content/Help/9a.jpg" alt="Change Address" height="300" class="center-block img-responsive" />
                                            <p><b>Step 5: </b>Change on your relevant row of address, enter your password and click <span class="help-highlight">Submit</span>.</p>
                                            <img src="/Content/Help/9b.jpg" alt="Change Address" height="300" class="center-block img-responsive" />
                                            <p><b>Step 6: </b>Wait for Apex admin to approve your submission. Once approved, you can see the status at the Account Settings MA Address</p>
                                            <img src="/Content/Help/9c.jpg" alt="Change Address" height="300" class="center-block img-responsive" />
                                        </div>
                                    </div>
                                </div>

                                <div id="ChangeOccupation">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>How to change occupation details?</h6>
                                        </div>
                                        <div class="help-content">
                                            <p><b>Step 1: </b>Browse <a href="https://eapexis.apexis.com.my">eapexis.apexis.com.my</a> website.</p>
                                            <p><b>Step 2: </b>Login into your account.</p>
                                            <p><b>Step 3: </b>Click <span class="help-highlight">Account Settings</span> in the left bar once successfully login.</p>
                                            <p><b>Step 4: </b>In Account Settings Click <span class="help-highlight">View</span> on Occupation Information and click <span class="help-highlight">View</span>.</p>
                                            <img src="/Content/Help/Occ1.jpg" alt="Change Address" height="300" class="center-block img-responsive" />
                                            <p><b>Step 5: </b>To change the occupational details, click <span class="help-highlight">Edit</span>.</p>
                                            <img src="/Content/Help/Occ2.jpg" alt="Change Address" height="300" class="center-block img-responsive" />
                                            <p><b>Step 5: </b>Change the relevant details for your ocucpation, enter your password and click <span class="help-highlight">Submit</span>.</p>
                                            <img src="/Content/Help/Occ3.jpg" alt="Change Address" height="300" class="center-block img-responsive" />
                                            <p><b>Step 6: </b>Wait for Apex admin to approve your submission. Once approved, you can see the status at the Account Settings for Occupation Information</p>
                                            <img src="/Content/Help/Occ4.jpg" alt="Change Address" height="300" class="center-block img-responsive" />
                                        </div>
                                    </div>
                                </div>

                                <div id="PrivacyPolicy">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>Privacy Policy</h6>
                                        </div>
                                        <div class="help-content">
                                            <p class="text-justify">Apex Investment Services Berhad (AISB) (“the Company”, “our”, “us”, or “we”) is a holder of a Capital Markets Services Licence issued under the Capital Market and Services Act 2007 permitting the Company to carry out regulated activities of fund management and dealings in securities (restricted to unit trust).</p>
                                            <h5 class="text-justify">THIS MEANS, MOST IMPORTANTLY, THAT WE DO NOT SELL CLIENT INFORMATION-WHETHER IT IS YOUR PERSONAL INFORMATION OR THE FACT THAT YOU ARE AN APEX INVESTMENT SERVICES BERHAD (AISB) CLIENT-TO ANYONE.</h5>
                                            <p class="text-justify">In the course of the Company carrying out the aforesaid permitted activities, the Company may collect, record, hold, store or process your Personal Data (as defined in the Personal Data Protection Act 2010). We have always (and will continue to do so) respected the privacy and confidentiality of all the personal information we have received and collected in the course of the provision of our services to and/or our dealings with you and taken all reasonable steps to ensure the proper safeguard of such information.</p>
                                            <h5>YOUR CONSENT</h5>
                                            <p>By proceeding to access and use this Site, you are deemed to have consented to the collection and use of your personal information including for the disclosure of such personal information to the relevant entities and/or regulatory bodies.</p>
                                            <h5>SAFEGUARDING OF YOUR PERSONAL DATA</h5>
                                            <p>Please take note that your Personal Data may be stored or processed to or in locations or systems in jurisdictions outside Malaysia (where necessary to facilitate the provision of our services and products to you) subject to those jurisdictions having similar data protection laws in place and/or our securing reciprocal confidentiality undertakings.</p>
                                            <p>Please be assured that we will take all necessary practical steps including but not limited to incorporating reasonable security measures into any equipment in which your Personal Data is stored, to protect your Personal Data from any loss, misuse or unauthorised access or disclosure.</p>
                                            <h5>DESCRIPTION OF THE PERSONAL DATA WE COLLECT AND PROCESS</h5>
                                            <p>You provide personal information when you complete an AISB account application. (If you enter information in an online application, we may store the information even if you don't complete or submit the application.). You also required providing personal information when you request a transaction that involves AISB or one of the AISB affiliated companies.</p>
                                            <p>In addition to personal information you provide to us, we may receive information about you that you have authorised third parties to provide to us. We also may obtain personal information from third parties in order for us to verify your identity, prevent fraud, and to help us identify products and services that may benefit you.</p>
                                            <p>Personal information collected from any source may include your:</p>
                                            <ul>
                                                <li>Name and address</li>
                                                <li>Identification Card Number/Passport Number</li>
                                                <li>Assets</li>
                                                <li>Income</li>
                                                <li>Account balance</li>
                                                <li>Investment activity</li>
                                                <li>Accounts at other institutions</li>
                                            </ul>
                                            <br/>
                                            <p>We will not disclose any of your personal information except where required by regulatory bodies, regulation of legal process, judicial order or as required or permitted by law or for lawful purposes only. Further, we may use your personal information within AISB, professional advisors, consultants and/or third party's performing services on our behalf and such confidential information will only be used by the said parties for the purpose for which it is provided whereby the said parties have undertaken to treat such information confidential.</p>
                                            <h5>SECURITY OF YOUR PERSONAL DATA</h5>
                                            <ul>
                                                <li>AISB protects your Personal Data by ensuring we have sufficient security measures in place and shall ensure that your Personal Data is stored and handled in such a way as to prevent any unauthorised disclosure.</li>
                                                <li>AISB shall take all reasonable action to prevent unauthorised use, access or disclosure of and to protect the confidentiality of your Personal Data in connection with the purpose for which the Personal Data, has been disclosed to, or has been collected by us.</li>
                                                <li>In order to alert you to other financial products and services that AISB offers or sponsors, we may share your information within the AISB affiliated companies. This would include, for example, sharing your information within AISB to make you aware of new AISB funds or other investment offerings and trust services offered through AISB’s trust companies and registered investment advisors.</li>
                                                <li>In certain instances, we may contract with non-affiliated companies to perform services for us. Where necessary, we will disclose information we have about you to these third parties. In all such cases, we provide the third party with only the information necessary to carry out its assigned responsibilities and only for that purpose. And, we require these third parties to treat your private information with the same high degree of confidentiality that we do.</li>
                                                <li>Finally, we will release information about you if you direct us to do so, if we are compelled by law to do so, or in other legally limited circumstances (for example, to prevent fraud).</li>
                                            </ul>
                                            <br />
                                            <h5>WHAT YOU CAN DO</h5>
                                            <p>For your protection, if you have an online account with us, we advise you NOT to reveal any information relating to your account to anyone. You are responsible to maintain the confidentiality of your account information, user names, login IDs, password, security questions and answers to access your account in this Site. DO NOT reveal your account information, user names, login IDs, password, security questions and answers to access your account in this Site, to anyone at any time under any circumstances. You acknowledge that you are fully responsible for your account and activities and should you become aware of any suspicious and unauthorised activities or any other breach of security, please act immediately and notify us as soon as possible. You should also aware that there shall be risk factors involved in sending confidential information over the Internet Sites, e-mails or other electronic means.</p>
                                            <p>By using this Site, you consent and authorise for such transfer of information and you agree that we may contact you through any of the means stated herein. You further consent and authorise that you will receive information from us through the electronic means from time to time and/or other means as determined by us and/or as required by the regulatory bodies and/or by the law.</p>
                                            <p>Online safety tips:-</p>
                                            <ul>
                                                <li>You should keep your confidential information in a safe place;</li>
                                                <li>Always protect your PINs and other passwords. You should not reveal the PINs and passwords to anyone unless it's for a service or transaction you request;</li>
                                                <li>Check your account regularly. Contact us immediately should there any error or dispute a transaction in your account.</li>
                                                <li>If you believe you are a victim of identity theft, take immediate action and keep records of your conversations and correspondence.</li>
                                            </ul>
                                            <br/>
                                            <p>Do contact us immediately. File a report with your local police station and keep a copy of the police report for records.</p>
                                            <h5>RIGHTS TO MODIFY</h5>
                                            <p>We reserve the right to modify this policy at any time without prior notice to you. Any such amendment shall be effective once the revised changes have been posted on the Site. Since we may update the Site from time to time, you should constantly check the contents herein from time to time so that you are aware of any changes or amendments to the policy.</p>
                                            <h5>YOU NEED TO UPDATE YOUR CURRENT INFORMATION</h5>
                                            <p>You should always ensure that you provide us with your most updated and current personal particulars and information in order to ensure that your records with us are kept up to date, complete and accurate. If any information supplied by you changes during the course of your account with us, you should notify us immediately to enable us to update your information.</p>
                                            <h5>ANY ENQUIRIES?</h5>
                                            <p>If you have any enquiries or concerns on the Privacy Policy, please <a href="Contact.aspx">contact us</a> and we will respond to your enquiries as soon as possible.</p>
                                        </div>
                                    </div>
                                </div>


                                <div id="faq">
                                    <div class="help-section">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="faq-section">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default faq-panel">
                                                            <div class="panel-heading faq-panel-heading">
                                                                <h6 data-toggle="collapse" data-parent="#accordion" href="#general" class="panel-title expand">
                                                                    <span class="right-arrow pull-right">+</span>
                                                                    <a href="#">General</a>
                                                                </h6>
                                                            </div>
                                                            <div id="general" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div id="General">
                                                                        <div class="faq">
                                                                            <div class="panel-group faqAccordion" id="faqAccordion">
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question0">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What is eApexIs?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question0" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>eApexIs is an online facility which allows unitholders to make investment and transactions requests ( including redemption and switching). And enquire on account balances, transactions and statements.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question1">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What are the operational hours for eApexIs?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question1" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>eApexIs is available 24 hours a day, 7 days a week.  Online investments are subject to availability of e-banking services.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question2">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Who is eligible to register for eApexIs?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question2" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>eApexIs is open to all Individuals  who are the firstholders and Corporate Investors who has an Investment accounts with Apex Investment. </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question3">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: How do I ensure I am accessing a genuine eApexIs website?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question3" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>eApexIs is a secured website which uses TLS (Transport Layer Security) encryption technology enabled by SSL certificate to protect our information and secure your online transactions.  Please ensure that you are accessing eApexis web address that starts with https:// and you will usually see a security padlock when you are accessing a secured website.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question4">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can I make investments and transaction requests in eApexIs outside of Malaysia?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question4" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>You can access to eApexIs Services outside of Malaysia to view information and perform investments and transaction requests provided that your local mobile phone has access to international call roaming service (for receipt of TAC) and Malaysian bank account under FPX Myclear network (for investment transactions).</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question5">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Do I need to clear browser cache (internet browsing history/information) after I have completed my eApexIs services session?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question5" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Yes, as an added privacy measure, you should clear the browser cache (internet browsing history/information that is stored in internet files) after each online transaction session.  This is to prevent others from viewing confidential transactional information done over eApexIs.</p>
                                                                                            <p>What can I do to protect my online information</p>
                                                                                            <ul>
                                                                                                <li>Do not disclose your password or account details to any unauthorized persons.</li>
                                                                                                <li>Do not click on links in an e-mail from senders whom you are not familiar with. These emails often contain misspelled words in the sender’s e-mail address or fill out forms in e-mails messages and un-trusted websites.</li>
                                                                                                <li>Be wary of unexpected calls or emails requesting personal information, passwords or financial details.  Apex Investment will never send you an email or SMS asking you to very or provide sensitive/confidential information.</li>
                                                                                                <li>Create smart passwords by using combination of numbers, upper and lower case letter.  Avoid using dictionary or generic passwords or a password that is easily identified with you (eg. family name, birthday, pass1234, etc.)</li>
                                                                                                <li>Do not share your password or write it down. Prevent browser from storing your login id and password. Passwords should never be automatically filled in by a computer.</li>
                                                                                                <li>Change passwords regularly. Choose a different password for each online account. For example, using the same password on bank accounts and social media or email may increase risk of identity theft or fraud.</li>
                                                                                                <li>When you log in to eApexIs you are in a secure session. Make sure to check the web address - it should start with https://.</li>
                                                                                                <li>Remember to log out if you step away from your computer or you have completed your tasks.</li>
                                                                                                <li>Avoid using public Wi-Fi or public computers to perform online transactions.</li>
                                                                                                <li>Only use online services on computers with up-to-date anti-virus and anti-spam software.</li>
                                                                                                <li>Clear browser cache (internet browsing history/information) after each internet transaction session.</li>
                                                                                            </ul>
                                                                                            <br/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question6">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What should I do if my computer crashes or I get disconnected from the Internet in middle of an investment or a transaction request?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question6" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Reboot and connect your computer or use another computer to login into your account to verify your transaction status.</p>
                                                                                            <ul>
                                                                                                <li>If you have an incomplete payment transaction at FPX, please contact your respective bank to check the status of the payment transaction at bank.</li>
                                                                                                <li>b.	If you have completed payment but still cannot find your transaction request under ‘Transaction > Order Status’, please contact our Customer Hotline at +603 2095 9999 for assistance.</li>
                                                                                                <li>If you have yet to submit your transaction request, you will need to key in the transaction again.</li>
                                                                                            </ul>
                                                                                            <br/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default faq-panel">
                                                            <div class="panel-heading faq-panel-heading">
                                                                <h6 data-toggle="collapse" data-parent="#accordion" href="#nonepfinvestment" class="panel-title expand">
                                                                    <span class="right-arrow pull-right">+</span>
                                                                    <a href="#">Non-EPF Investment</a>
                                                                </h6>
                                                            </div>
                                                            <div id="nonepfinvestment" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div id="NonEpfInvestment">
                                                                        <div class="faq">
                                                                            <div class="panel-group faqAccordion" id="faqAccordion2">
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion2" data-target="#question7">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Who can invest online?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question7" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Individuals aged 18 and above on their last birthday.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion2" data-target="#question8">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can an account have more than one unit holder?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question8" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Yes, but only limited to one joint holder.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion2" data-target="#question9">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Are there any circumstances in which an individual is not allowed to invest?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question9" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Yes, a minor below 18 years old. He / she must invest as a designated account holder together with a first holder who is 18 years old or above.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion2" data-target="#question10">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: If the Joint Holder is below 18, must the Joint Holder sign on the investment form?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question10" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>No.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion2" data-target="#question11">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: How does an individual invest online via eApexIs? What are the documents required for invest online via eApexIs?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question11" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>A first time investor can now open an account through eApexIs, please refer to “Beginner guide” under “How to Open an Account”.</p>
                                                                                            <p>For subsequent investments, the investor can perform additional top-up via eApexIs.</p>
                                                                                            <p>The prospective investor may contact our Customer Service Hotline at +603 2095 9999 for assistance.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion2" data-target="#question12">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: How do I determine which fund(s) are suitable for me?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question12" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>The Suitability Assessment Test will be invoked when if you are first time register for eApexIs.  The Suitability Assessment Test serves as an advisory tool to identify your risk tolerance level. Please note that while we hope that these recommendations would be useful for you, you are advised to look at the fund's Master Prospectus and Product Highlight Sheet(s) and do your own further research before making your investment decisions.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion2" data-target="#question13">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What is the minimum and maximum amount for my fund transaction( purchase/redeem/switch)</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question13" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Investors are advised to read and understand the Master Prospectus and the respective Product Highlights Sheet(s) for minimum transaction amount.</p>
                                                                                            <p>There is no maximum limit to invest, however, any investment amount in excess of 10% of the fund size would require the approval of Apex's senior management.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion2" data-target="#question14">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: How to make payment for investment with via eApexIs?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question14" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Payment may be made through the following method:</p>
                                                                                            <ul>
                                                                                                <li>Financial Process Exchange (FPX)</li>
                                                                                                <li>Bank in slips for cheque/bank draft/cashiers order or online banking/direct deposit receipts of the below should be uploaded as a proof of payment documents in the eApexIs.
                                                                 <ul>
                                                                     <li>Cheques, bank drafts or cashiers order made payable to ‘ Apex Investment Services Berhad-Clients Trust Account’ and bank in to Maybank Islamic Berhad (Account No: 5640-1662-7254).</li>
                                                                     <li>Online banking or direct deposits should be remitted in Ringgit Malaysia to Maybank Islamic Berhad (Account No: 5640-1662-7254).</li>
                                                                 </ul>
                                                                                                </li>
                                                                                                <li>All payment should be issued by the Unitholders and not by any 3rd party individual</li>
                                                                                            </ul>
                                                                                            <br/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion2" data-target="#question15">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Apart from online transactions of unit trusts via eApexIs, how else can I subscribe, redeem and switch units?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question15" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>To purchase, redeem or switch units, you can also contact:</p>
                                                                                            <p>Your Unit Trust Consultant or our customer service representative at +603 20959999</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion2" data-target="#question16">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What is the cut-off time for investment?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question16" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>The cut-off time for submission of investment on any Business Day, before 4.00pm on a business day for Equity Funds and before 11am for Money Market Fund, and before 11.30am for Equity Funds on a Bursa Malaysia half day trading day.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion2" data-target="#question17">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: How do I know if my BUY orders have been approved?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question17" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Your BUY orders will only be processed once we received your payment before the cut-off time. The approved BUY orders will be displayed in the “Order Status” under the “Transaction” in the eApexIs.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion2" data-target="#question18">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What will I receive once my BUY orders have been transacted?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question18" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>You will able to view the Order Status as Completed and you will be received Confirmation Advice Slip from eApexIs.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default faq-panel">
                                                            <div class="panel-heading faq-panel-heading">
                                                                <h6 data-toggle="collapse" data-parent="#accordion" href="#epfinvestment" class="panel-title expand">
                                                                    <span class="right-arrow pull-right">+</span>
                                                                    <a href="#">EPF Investment</a>
                                                                </h6>
                                                            </div>
                                                            <div id="epfinvestment" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div id="EpfInvestment">
                                                                        <div class="faq">
                                                                            <div class="panel-group faqAccordion" id="faqAccordion3">
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion3" data-target="#question19">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can I invest via EPF withdrawal? What is the procedure?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question19" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>No. Currently EPF withdrawal is not available at eApexIs.</p>
                                                                                            <p>You may contact your Unit Trust Consultant or our Customer Service Representative at +603 2095 9999.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion3" data-target="#question20">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What are the documents required for investing in the EPF Members Investment Scheme?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question20" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>A duly completed and signed Master Account Opening Form or Additional Quickform, KWSP 9N (AHL) Form, a certified true copy of MyKad (front and back on the same page of an A4-sized paper).</p>
                                                                                            <p>A first time investor is required to complete a Suitability Assessment (SA) Form, FATCA Form (Foreign Account Tax Compliance Act), CRS Form (Common Reporting Standard) and other related forms which are available at our office.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion3" data-target="#question21">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What will happen if the investment is rejected by EPF?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question21" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>The company will notify the unit holder and his/her Unit TrustConsultant and the investment will be cancelled accordingly.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion3" data-target="#question22">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What are the documents issued to me once an investment has been made?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question22" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>A Confirmation Advice Slip.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion3" data-target="#question23">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can I transfer my EPF account to another person?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question23" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>No.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion3" data-target="#question24">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What couldI do if I have damaged ridges on both thumbs / disfigured thumbs or have had both thumbs amputated?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question24" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>You may submit a photocopy of duly verified Borang JPN KP12 which can be obtained from Jabatan Pendaftaran Negara (JPN). The KWSP 9N (AHL) Form, a certified true copy of MyKad and the Borang JPN KP12 will be forwarded to EPF for processing.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <%--<div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion3" data-target="#question25">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: If I reach 55 years old and continues to contribute to EPF, can I invest under the EPF Scheme?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question25" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>No.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>--%>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion3" data-target="#question26">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Will I be informed of the status of my account after EPF has released its control to Apex?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question26" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Yes. A letter will be sent to you after the account has been converted to a Non-EPF account.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion3" data-target="#question27">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What will happen to my account if I have pass away?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question27" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Your next of kin needs to inform the EPF that you have passed away. EPF will then send a release control letter to Apex. After the account has been transferred to a non-EPF account, the beneficiary may apply to transfer or redeem the units from your account by submitting relevant documents such as Letter of Administration or Grant of Probate.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion3" data-target="#question28">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What is the release control letter for?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question28" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>The purpose of the release control letter, issued by EPF, is for EPF to relinquish control of the Member's EPF investment to Apex. Thereafter, the unit holder has the right to redeem the units, payment will be payable to his/her bank account or transfer the units to a third party.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default faq-panel">
                                                            <div class="panel-heading faq-panel-heading">
                                                                <h6 data-toggle="collapse" data-parent="#accordion" href="#corporateinvestment" class="panel-title expand">
                                                                    <span class="right-arrow pull-right">+</span>
                                                                    <a href="#">Corporate Investment</a>
                                                                </h6>
                                                            </div>
                                                            <div id="corporateinvestment" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div id="CorporateInvestment">
                                                                        <div class="faq">
                                                                            <div class="panel-group faqAccordion" id="faqAccordion4">
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion4" data-target="#question29">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Who is eligible to invest?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question29" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Any corporation, society, association, government and non-government body that is registered in Malaysia.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion4" data-target="#question30">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can Corporate Investment able to do additional of investment via eApexIs?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question30" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>No. Currently eApexIs is opened for corporate authorized person for viewing purpose.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion4" data-target="#question31">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What are the documents required for Corporate Investment?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question31" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>The corporation must submit:-</p>
                                                                                            <ul>
                                                                                                <li>Master Account Application form</li>
                                                                                                <li>Suitability Assessment (SA) Form</li>
                                                                                                <li>FATCA Form (Foreign Account Tax Compliance Act)</li>
                                                                                                <li>CRS Form (Common Reporting Standard)</li>
                                                                                            </ul>
                                                                                            <br/>
                                                                                            <p>For SDN BHD Company:-</p>
                                                                                            <ul>
                                                                                                <li>Form 9 (Certificate of incorporation)/ Section 15 - Notice of Registration & Section 14 - Application for Registration or Section 17 (SSM Registration) [Certificate of Incorporation of Private Company]</li>
                                                                                                <li>Constitution/Declaration of Account Opening</li>
                                                                                                <li>Form 24/Section 78  - Return of Allotment of Shares (if applicable)</li>
                                                                                                <li>Form 49 - Particulars of Directors/Section 58 - Notification of Change in the Register of Directors, Managers and Secretaries (if available)/Section 58 & 236(2) - Notification Of Appointment Of The First Company Secretary (if available)</li>
                                                                                                <li>Form 13 – Certificate of Change of Name/Section 28(3)(b) -Notice of Registration of New Name (if applicable)</li>
                                                                                                <li>Form 44 (Notice of Situation of Registered Office and of Office Hours and Particulars of Changes)/ Section 46 – Registered Office (if applicable)</li>
                                                                                                <li>Resolutions passed by the Board of Directors or Extract of Resolution (List of Board of Directors)</li>
                                                                                                <li>Section 51 (Register of Member) - sdnbhd</li>
                                                                                                <li>SSM Registration Copy</li>
                                                                                                <li>Photocopy of Identity Cards of Shareholders of Individuals (with more than 25% controlling interest), to provide certified copy of Form 9 (Certificate of Incorporation) Constitution / Declaration of Account Opening,  Form 24 & Form 49 </li>
                                                                                            </ul>
                                                                                            <br/>
                                                                                            <p class="text-info">Note: If the appointed signatory is a foreign national, please present valid passport and residency pass (i.e.; employment pass / work permit issued by Malaysian Authorities).</p>
                                                                                            <p>For BHD Company:-</p>
                                                                                            <ul>
                                                                                                <li>Form 20 / Section 41 (Certificate of incorporation on Conversion to Public Company) [BHD]</li>
                                                                                                <li>Form 23 (Certificate of Commencement of Business for Public Company) / Section 190 Statutory  Declaration On Entitlement to Commerce Business by a Public Company  [BHD]</li>
                                                                                                <li>Constitution / Declaration of Account Opening</li>
                                                                                                <li>Form 24/ Section 78  - Return of Allotment of shares (if applicable)</li>
                                                                                                <li>Form 49 - Particulars of Director / Section 58 - Notification of Change in the Register of Directors, Managers and Secretaries (if available) / Section 58 & 236(2) - Notification Of Appointment Of The First Company Secretary (if available)</li>
                                                                                                <li>Form 13 – Certificate of Change of Name / Section 28(3)(b) -Notice of Registration of New Name (if applicable)</li>
                                                                                                <li>Form 44 / Section 46 – Registered Office (if applicable)</li>
                                                                                                <li>Resolutions passed by the Board of Directors or Extract of Resolution (List of Board of Directors)</li>
                                                                                                <li>Photocopy of Identity Cards of Shareholders of Individuals (with more than 25% controlling interest) / if Shareholder is a company (with more than 25% controlling interest), to provide certified copy of Form 9 (Certificate of Incorporation, Constitution / Declaration of Account Opening, Form 24 & Form 49</li>
                                                                                            </ul>
                                                                                            <br/>
                                                                                            <p class="text-info">Note: If the appointed signatory is a foreign national, please present valid passport and residency pass (ie; employment pass / work permit issued by Malaysian Authorities)</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default faq-panel">
                                                            <div class="panel-heading faq-panel-heading">
                                                                <h6 data-toggle="collapse" data-parent="#accordion" href="#coolingoff" class="panel-title expand">
                                                                    <span class="right-arrow pull-right">+</span>
                                                                    <a href="#">Cooling Off</a>
                                                                </h6>
                                                            </div>
                                                            <div id="coolingoff" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div id="CoolingOff">
                                                                        <div class="faq">
                                                                            <div class="panel-group faqAccordion" id="faqAccordion5">
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion5" data-target="#question34">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What is cooling-off period?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question34" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>You have 6 (six) business days after your initial investment to reconsider its appropriateness for your needs. Within this period, you may withdraw your investment at the Net Asset Value (NAV) per unit on the day the units were first purchased and have the Application Fee (if any) repaid. Please note that the cooling-off right is only given to an investor who is investing with Apex for the first time and is not applicable for corporate or institutional investors.</p>
                                                                                            <p>EPF investments' investors are not entitled to cooling-off rights.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion5" data-target="#question35">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: How do I exercise my cooling-off right?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question35" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>You can exercise your cooling-off right by visiting our business office within 6 (six) business days from the date of the transaction.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default faq-panel">
                                                            <div class="panel-heading faq-panel-heading">
                                                                <h6 data-toggle="collapse" data-parent="#accordion" href="#redemption" class="panel-title expand">
                                                                    <span class="right-arrow pull-right">+</span>
                                                                    <a href="#">Redemption</a>
                                                                </h6>
                                                            </div>
                                                            <div id="redemption" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div id="Redemption">
                                                                        <div class="faq">
                                                                            <div class="panel-group faqAccordion" id="faqAccordion6">
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion6" data-target="#question36">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can I book the redemption price?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question36" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>No.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion6" data-target="#question37">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can I request for the redemption proceed to be made payable to a third party?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question37" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>No.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion6" data-target="#question38">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What are the conditions for a partial redemption?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question38" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>You are advised to read and understand the Master Prospectus and the respective Product Highlights Sheet(s) for minimum transaction units and the minimum holding of the fund.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion6" data-target="#question39">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: For redemption of units, can I request for cash payment?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question39" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>No.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion6" data-target="#question40">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: How long does APEX take to settle the redemption payment?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question40" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>For Money Market investment, the redemption proceed be issued within 1 business day from the transaction date of the redemption.</p>
                                                                                            <p>For Non Money Market investment, the redemption proceed be issued within 5 business days from the transaction date of the redemption.</p>
                                                                                            <p>However, the industry practice is 10 calendar days from the date of receipt of the duly completed repurchase documents to issue the repurchase cheques.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion6" data-target="#question41">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can I request for the redemption to be credited into my bank account?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question41" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Yes, but only credit into the Bank Account that maintain in eApexIs.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion6" data-target="#question42">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What should l do if I requests for a full redemption of the investment which has a monthly RSP?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question42" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Once your particular fund unit holdings is fully redeemed, the monthly Regular Saving Plan will then be terminated.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default faq-panel">
                                                            <div class="panel-heading faq-panel-heading">
                                                                <h6 data-toggle="collapse" data-parent="#accordion" href="#switching" class="panel-title expand">
                                                                    <span class="right-arrow pull-right">+</span>
                                                                    <a href="#">Switching</a>
                                                                </h6>
                                                            </div>
                                                            <div id="switching" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div id="Switching">
                                                                        <div class="faq">
                                                                            <div class="panel-group faqAccordion" id="faqAccordion7">
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion7" data-target="#question43">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What is switching of fund?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question43" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Switching of fund is a facility which enables a unit holder to convert units of a particular fund for the units of other funds managed by Apex.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion7" data-target="#question44">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can Switching be done through eApexIs?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question44" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Yes. </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion7" data-target="#question45">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Are there any administrative charges for switching of funds?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question45" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Three free switches per account are allowed in each calendar year. Subsequent switches will be charged a 1% of redemption moneys for administrative purpose.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion7" data-target="#question46">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What are the conditions for partial switching of fund?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question46" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>You are advised to read and understand the Master Prospectus and the respective Product Highlights Sheet(s) for minimum transaction units / amount and the minimum holding of the fund.</p>
                                                                                            <p>If the switching is made to an initial (new) fund, the net amount switched after deduction of sales charge (if any) must meet the minimum requirement of the fund's initial investment amount.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion7" data-target="#question47">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What should I do ifI would like to request for a full switching from Fund A to Fund B and the Fund A has a monthly Regular Saving Plan?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question47" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>The monthly standing instruction will be credited into the new fund (switched to new fund). For investors do not wish to switch the monthly Regular Saving Plan to the new fund, you are required to submit the MyClear DDA termination form to us.</p><br/><br />
                                                                                            <p>For further inquiries, please contact Apex Customer Representative at +603 2095 9999</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion7" data-target="#question48">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What are the conditions for switching into a new fund?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question48" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>If the Switching request is made to an initial (new) investment, the net amount switched after deduction of sales charge (if any) must meet the minimum initial investment required by the switch-to fund.</p>
                                                                                            <p>A Suitability Assessment (SA) Form is required if you have not completed one previously.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion7" data-target="#question49">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can I switch from a single fund to multiple funds?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question49" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Yes. You are allowed to switch from one fund to multiple funds.However, if the account is under EPF account and on Islamic type status. You are only allow to switch to Shariah funds.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default faq-panel">
                                                            <div class="panel-heading faq-panel-heading">
                                                                <h6 data-toggle="collapse" data-parent="#accordion" href="#transfer" class="panel-title expand">
                                                                    <span class="right-arrow pull-right">+</span>
                                                                    <a href="#">Transfer</a>
                                                                </h6>
                                                            </div>
                                                            <div id="transfer" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div id="Transfer">
                                                                        <div class="faq">
                                                                            <div class="panel-group faqAccordion" id="faqAccordion8">
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion8" data-target="#question50">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can the transfer of Units be done via eApexIs?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question50" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>No. For Transfer of units, please contact Apex customer service centre at 03 2095 9999.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion8" data-target="#question51">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What are the documents required for full / partial transfer of units?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question51" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>A completed Transfer Form, a photocopy of NRIC of transferor and transferee, documentations to support relationship, an Account Opening Form and other related forms which are available at our office.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion8" data-target="#question52">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Is it compulsory that the transferor's signature in the "Transfer Form" be the same as the one in APEX's records?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question52" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Yes.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion8" data-target="#question53">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What is the minimum number of units for transfer?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question53" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Full transfer of units is required for the transfer of accounts</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion8" data-target="#question54">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Are there any administrative charges for transfer of units?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question54" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>A RM5.00 charge will be deducted from the transfer. This charge is waived for transfer of units for a deceased unit holder.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion8" data-target="#question55">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can I transfer my units to another fund in another account?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question55" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>No. Transfer must be made within the same fund to a different accounts.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion8" data-target="#question56">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What are the documents required for transfer of units for a deceased unit holder?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question56" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>A Transaction Request Form for Deceased Account, a copy of Letter of Administration or Grant of Probate, photocopy of NRIC of beneficiary(s), an Account Opening Form, FATCA Form (Foreign Account Tax Compliance Act) and CRS Form (Common Reporting Standard) and other related forms which are available at our office.</p><br/><br />
                                                                                            <p>please contact our Customer Service Representative +603 2095 9999 for assistance.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default faq-panel">
                                                            <div class="panel-heading faq-panel-heading">
                                                                <h6 data-toggle="collapse" data-parent="#accordion" href="#amendment" class="panel-title expand">
                                                                    <span class="right-arrow pull-right">+</span>
                                                                    <a href="#">Amendment of Account Holder's Particulars</a>
                                                                </h6>
                                                            </div>
                                                            <div id="amendment" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div id="Amendment">
                                                                        <div class="faq">
                                                                            <div class="panel-group faqAccordion" id="faqAccordion9">
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion9" data-target="#question57">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: How do I update the company if there is changes in of my particulars  via eApexIs?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question57" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>You may update the changes in the Account Settings in eApexIs.</p>
                                                                                            <p>Alternatively, you may write to us or complete the Master Account Amendment Form and submit the form to Apex offices.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion9" data-target="#question58">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can I request for a change of signature? How do I update the company?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question58" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Yes. You are required to complete the Master Account Amendment Form and witnessed by your Unit Trust Consultant or any Apex Customer Service staff.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion9" data-target="#question59">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Who can authorise a change of signature in the case of a joint account?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question59" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>For change of signature(s) in the case of joint account, both holders must sign the Master Account Amendment Form and witnessed by your Unit Trust Consultant or any Apex Customer Service staff.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion9" data-target="#question60">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can a unit holder request to change the Authority to operate the account in the case of a joint account?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question60" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Yes. Both holders must sign for a request to change the Authority to operate an account by completing the Master Account Amendment Form.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default faq-panel">
                                                            <div class="panel-heading faq-panel-heading">
                                                                <h6 data-toggle="collapse" data-parent="#accordion" href="#rsp" class="panel-title expand">
                                                                    <span class="right-arrow pull-right">+</span>
                                                                    <a href="#">Regular Savings Plan (RSP)</a>
                                                                </h6>
                                                            </div>
                                                            <div id="rsp" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div id="RSP">
                                                                        <div class="faq">
                                                                            <div class="panel-group faqAccordion" id="faqAccordion10">
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question61">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What is a Regular Savings Plan?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question61" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Regular Savings plan (RSP) allows you to make regular monthly investments of RM100 or more directly from your account via Intra Bank and interbank Direct Debit collection system.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question80">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What is the frequency of the RSP investment?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question80" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>The RSP frequency is <b>ONE</b> time in a month.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question62">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: How can I make regular investments into the RSP?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question62" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>You may apply for RSP through FPX service via eApexIs.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question63">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: What is the minimum Direct debit amount?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question63" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>You are advised to read and understand the Master Prospectus and the respective Product Highlights Sheet(s) for minimum transaction units / amount and the minimum holding of the fund.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question64">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: When is the effective date for deduction?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question64" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>The effective date is on the 1st day of each month. If the 1st of the month falls on a non-business day, the price will be based on the valuation determined at the end of the next Bursa Malaysia business day.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question66">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Are there any administrative charges for Autodebit?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question66" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Upon successful completion of the online application, processing fee of MYR 1.00 will be deducted from your selected bank account. However, eApexIs will refund the MYR 1.00 into your selected bank account within 14 business days. If you any enquiries, please contact our Customer Service Representative at 03-2095999 for assistance.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question67">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can a unit holder request to increase, decrease, change funds or cancel Autodebit?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question67" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>A unit holder may request to increase, decrease, change funds or cancel Autodebit by submitting the "MyClear DDA Form" to APEX. Change of request received from 15th to 30th/31st of the month will only be effective from the next RSP cycle, as existing approved RSP-FPX e-mandate would have been processed for deduction on the 1st of the current month.</p>
                                                                                            <p>However, the Fund Manager reserves the right to terminate your Autodebit request for payments that are unsuccessful for three (3) consecutive months.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question68">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Who shall I call if the Autodebit Instruction is not effected?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question68" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>You may contact our Customer Service Hotline at 03-2095 9999 for enquiry.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question69">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can I request to increase the existing Autodebit amount if the Fund has been closed?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question69" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>No.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question70">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can I have more than one (1) Autodebit for the same account from the same bank account?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question70" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>No. You are advised to have (1) Autodebit for same account from same bank account, however, you may invest multiple funds under same monthly Regular Savings Plan (RSP-FPX e-mandate).</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question71">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can a unit holder have more than one (1) Autodebit for the same account from a different Maybank bank account?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question71" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>No. You are advised to have (1) Autodebit for same account from same bank account, however, you may invest multiple funds under same monthly Regular Savings Plan (RSP-FPX e-mandate).</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question72">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Will my RSP subscription contract be created if there is an insufficient balance in my fund holdings?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question72" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>No.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question73">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: How can I change or terminate my RSP instructions?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question73" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Any change of amount request or termination request received from 15th to end of the month will be effective on the next RSP cycle as existing approved RSP-DDA or RSP-FPX E-Mandate have been processed for deduction on the 1st of the current month. If 1st is a non business day, the order will processed on the next business day.<br/><br/>Should you wish to change the DDA limit or terminate the MyClear DDA Form, you are required to submit a copy of DDA from to us.</p><br/>
                                                                                            <p>For further inquiries, please contact Apex Customer Representative at +603 2095 9999</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question74">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: If I sell or switch all the units for the particular fund, will my RSP be automatically terminated?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question74" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <%--<h6><span class="label label-primary">Answer</span></h6>--%>
                                                                                            <p>If switch all the units: </p><br/>
                                                                                            <p>The monthly standing instruction will be credited into the new fund (switched to new fund). For investors do not wish to switch the monthly Regular Saving Plan to the new fund, you are required to submit the MyClear DDA termination form to us。</p><br/>
                                                                                            <p>If sell all the units:</p><br/>
                                                                                            <p>Once your particular fund unit holdings is fully redeemed, the monthly Regular Saving Plan will then be terminated.</p><br/>
                                                                                            <p>For further inquiries, please contact Apex Customer Representative at +603 2095 9999</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question75">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Can I switch The RSP between different funds?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question75" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>No, switching of the RSP between different funds is not available.<br/>For example: If you currently have the RSP for Fund A and would like to switch the RSP to Fund B, you need to terminate the RSP for Fund A and apply for a RSP for Fund B. You are required to submit the MyClear DDA Maintenance form to us.</p><br/>
                                                                                            <p>For further inquiries, please contact Apex Customer Representative at +603 2095 9999</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question76">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: When can I redeem the funds subscribed via RSP?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question76" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>There is no lock-in period and you can redeem/switch-sell the funds anytime once your RSP subscription contracts have been completed.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question77">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Will my RSP subscription contracts be created if there is an insufficient balance in my DDA bank account for my RSP instructions?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question77" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>In the event of insufficient balance in your bank account, eApexIs will not create any RSP subscription contract.</p><br/>
                                                                                            <p>Unsuccessful RSP instructions will be put on hold till next month's contribution date. Therefore, please ensure that you will have sufficient balance for the RSP purchase each month.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question78">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: How will I be informed of my RSP investments?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question78" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>Similar to normal lump sum investments, we will inform you of the trade details and the status of the transaction by email. You can also view details of your RSP subscriptions by clicking on <b>Regular Savings Plan >> RSP Enrollment Status.</b></p><br/>
                                                                                            <p>Generally, FPX E-Mandate will takes approximately 7 business to process and approved by the bank. We will send you an email once your application has been approved by the bank. Meanwhile, an email which indicating the first RSP investment will as well send to you.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel panel-default ">
                                                                                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion10" data-target="#question79">
                                                                                        <h6 class="panel-title">
                                                                                            <a class="ing">Q: Do I need to sign any forms for RSP?</a>
                                                                                        </h6>
                                                                                    </div>
                                                                                    <div id="question79" class="panel-collapse collapse" style="height: 0px;">
                                                                                        <div class="panel-body">
                                                                                            <p>RSP application via FPX E-Mandate is paperless, you are not required to submit any form. The RSP will commence once the approval is obtained. A confirmation email will be sent to you to inform of the first contribution date for the RSP upon the approval.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="OnlineProviders">
                                    <div class="help-section">
                                        <div class="help-heading">
                                            <h6>List of Online Providers</h6>
                                        </div>
                                        <div class="help-content">
                                            <p class="text-justify">1. <a href="https://www.fundsupermart.com.my/fsmone/home">iFAST Capital Sdn.Bhd.</a></p>
                                            <p class="text-justify">2. <a href="https://www.phillipmutual.com">Phillip Mutual Berhad</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="server">
    <script src="/Content/assets/js/theDocs.all.min.js"></script>
    <script>
        $(function () {
            $('.selectpicker').selectpicker({
                style: 'btn-info',
                size: 4
            });

            $('.sidebar a').click(function () {
                var target = $(this).attr('href');

                $('html,body').animate({
                    scrollTop: $(target).offset().top - 114
                });
            });
            $('#my-select').change(function () {
                var target = $(this).find('option:selected').attr('id');
                $('html,body').animate({
                    scrollTop: $(target).offset().top - 114
                });
            });
            $('#my-select2').change(function () {
                var target = $(this).find('option:selected').attr('id');
                $('html,body').animate({
                    scrollTop: $(target).offset().top - 114
                });
            });
        });
    </script>
    <script>
        $(function () {
            $(".expand").on("click", function () {
                // $(this).next().slideToggle(200);
                $expand = $(this).find(">:first-child");

                if ($expand.text() == "+") {
                    $expand.text("-");
                } else {
                    $expand.text("+");
                }
            });
        });
    </script>
    <script src="Content/MyCJ/MyScripts.js"></script>
</asp:Content>
