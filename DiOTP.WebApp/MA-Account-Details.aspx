﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="MA-Account-Details.aspx.cs" Inherits="DiOTP.WebApp.MA_Account_Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .dataTable{
            width:100% !important;
        }
        .dataTable tr {
            cursor:pointer;
        }
        .MAAccountDetails [class*=col-]{
            display:table;
            min-height: 25px;
        }
        .MAAccountDetails [class*=col-] strong {
            display:table-cell;
            width:150px;
        }
        .MAAccountDetails [class*=col-] div {
            display:table-cell;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="p-head">
                            <h5>MA Account Details</h5>
                        </div>
                        <div class="panel-body">
                            <div class="row MAAccountDetails" id="MAAccountDetails" runat="server">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="p-head">
                            <h5>Investments</h5>
                        </div>
                        <div class="panel-body tableInvestments">
                            <table class="table table-condensed table-bordereds dataTable">
                                    <thead>
                                        <tr class="fs-14">
                                            <th>No</th>
                                            <th>EffectiveDate</th>
                                            <th>ReportDate</th>
                                            <th>ActualTransferredFromEpfRm</th>
                                            <th>ActualCost</th>
                                            <th>Units</th>
                                            <th>BookValue</th>
                                            <th>MarketValue</th>
                                            <th>RedemptionCost</th>
                                            <th>MemberEpfNo</th>
                                            <th>EpfIpdCode</th>
                                            <th>IpdFundCode</th>
                                            <th>IpdMemberAccNo</th>
                                            <th>Isactive</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyInvestments" runat="server">

                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="server">
     <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
    <script>
        var selectFunds;
        var fundDetails;

        function ApplyDataTable() {
            $('.dataTable').DataTable({
                dom: "",
                responsive: true,
                order: [],
                ordering: false
            });
            $('.dataTable tr td:not(:first-child)').click(function (e) {
                $(this).parent('tr').find('td').first().click();
            });
        }
        ApplyDataTable();
        function FundDetails() {
            $.ajax({
                url: "Fund-Comparison.aspx/GetFunds",
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: function (data) {
                    return data;
                },
                success: function (data) {
                    var json = data.d;
                    if (json.IsSuccess) {
                        fundDetails = json.Data;
                        $('.fundsSelect option').each(function (idx, obj) {
                            var fundCode = $(obj).attr('value');
                            if (fundCode != "") {
                                console.log(fundCode);
                                var fund = $.grep(fundDetails, function (o, i) {
                                    return o.UtmcFundInformation.IpdFundCode == fundCode;
                                })[0];
                                $(this).html(fund.UtmcFundInformation.FundName + " - " + fund.UtmcFundInformation.FundCode)
                            }
                        });
                    }
                    else {
                        ShowCustomMessage('Exception', json.Message, '');
                    }
                }
            });
        }
        FundDetails();
    </script>
</asp:Content>
