﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class Login : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyObj.Value; } }

        private static readonly Lazy<IUserTypeService> lazyObjUserTypeService = new Lazy<IUserTypeService>(() => new UserTypeService());

        public static IUserTypeService IUserTypeService { get { return lazyObjUserTypeService.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        public static string loginRedirectUrl = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["redirectUrl"] != null)
                {

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string message = VerifyLogin(txtEmailOrUsername.Text.Trim(), txtPassword.Text, Request, hdnLocalIPAddress.Value);
            if (message == "1")
                Response.Redirect("/Portfolio.aspx?redirectUrl=" + loginRedirectUrl, false);
            else
            {
                divMessage.Attributes.Add("class", "alert-box error");
                divMessage.InnerHtml = message;
            }
        }
        private static User user = new User();

        public string VerifyLogin(string emailOrUsername, string password, HttpRequest Request, String localIP)
        {
            try
            {
                if (!string.IsNullOrEmpty(emailOrUsername) && !string.IsNullOrEmpty(password))
                {
                    if (Utility.Helper.CustomValidator.IsValidEmail(emailOrUsername))
                    {
                        Response responseUList = IUserService.GetDataByPropertyName(nameof(Utility.User.EmailId), emailOrUsername, true, 0, 0, false);
                        if (responseUList.IsSuccess)
                        {
                            user = ((List<User>)responseUList.Data).FirstOrDefault();
                        }
                    }
                    else
                    {
                        return "<i class='fa fa-close icon'></i> Invalid Email ID.";
                    }
                    if (user != null)
                    {
                        Response responseUTList = IUserTypeService.GetDataByPropertyName(nameof(UserType.UserId), user.Id.ToString(), true, 0, 0, false);
                        if (user.Status == 1)
                        {
                            if (responseUTList.IsSuccess)
                            {
                                List<UserType> userTypes = (List<UserType>)responseUTList.Data;
                                if (user.Password != CustomEncryptorDecryptor.EncryptPassword(password))
                                {
                                    user.LoginAttempts = user.LoginAttempts + 1;

                                    hdnLoginAttempts.Value = user.LoginAttempts.ToString();
                                    hdnLoginLocked.Value = user.IsLoginLocked.ToString();
                                    
                                    IUserService.UpdateData(user);
                                    if (user.LoginAttempts >= 3)
                                    {
                                        user.IsLoginLocked = 1;
                                        hdnAccountLockedPop.Value = "1";
                                        IUserService.UpdateData(user);
                                        return "<i class='fa fa-close icon'></i> Too many attempts. Login Locked.";
                                    }
                                    else
                                    {
                                        return "<i class='fa fa-close icon'></i> Invalid Password.";
                                    }
                                }
                                else
                                {
                                    if (user.IsLoginLocked == 0)
                                    {
                                        if (userTypes.Count > 0)
                                        {
                                            UserType ut = userTypes.Where(x => x.UserTypeId == 1 && x.IsVerified == 1 && x.Status == 1).FirstOrDefault();
                                            if (ut != null)
                                            {
                                                Session["user"] = user;
                                                Session["LastLoginIP"] = user.LastLoginIp;
                                                Session["LastLoginTime"] = user.LastLoginDate;
                                                user.IsLoginLocked = 0;
                                                user.LoginAttempts = 0;
                                                user.LastLoginDate = DateTime.Now;
                                                user.LastLoginIp = TrackIPAddress.GetUserPublicIP(localIP) + ", " + localIP;
                                                user.VerificationCode = 0;
                                                user.VerifyExpired = 1;
                                                IUserService.UpdateData(user);
                                                Session["CurrentLoginTime"] = user.LastLoginDate;

                                                Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                                                if (responseUAList.IsSuccess)
                                                {
                                                    List<UserAccount> uAs = (List<UserAccount>)responseUAList.Data;
                                                    UserAccount uA = ((List<UserAccount>)responseUAList.Data).Where(x => x.IsPrimary == 1).FirstOrDefault();
                                                    if (uA.IsVerified == 0)
                                                    {
                                                        foreach (UserAccount ua in uAs)
                                                        {
                                                            ua.VerificationCode = 0;
                                                            ua.VerifyExpired = 1;
                                                        }
                                                        IUserAccountService.UpdateBulkData(uAs);
                                                    }
                                                }

                                                return "1";
                                            }
                                            else
                                            {
                                                return "<i class='fa fa-close icon'></i> Invalid user type.";
                                            }
                                        }
                                        else
                                        {
                                            return "<i class='fa fa-close icon'></i> No user type.";
                                        }
                                    }
                                    else
                                    {
                                        hdnAccountLockedPop.Value = "1";
                                        return "<i class='fa fa-close icon'></i> Account is Locked.";
                                    }
                                }
                            }
                            else
                            {
                                return "<i class='fa fa-close icon'></i> " + responseUTList.Message + ".";
                            }
                        }
                        else
                        {
                            return "<i class='fa fa-close icon'></i> This account is deavtivated .";
                        }
                    }
                    else
                    {
                        return "<i class='fa fa-close icon'></i> Invalid Email ID.";
                    }
                }
                else
                {
                    return "<i class='fa fa-close icon'></i> Please enter (<sup>*</sup>) required fields.";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("index Page VerifyLogin " + ex.Message);
                return "<i class='fa fa-close icon'></i> " + ex.Message;
            }
        }
    }
}