﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using DiOTP.WebApp.ServiceCalls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class OTP_Activation : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUserSecurityService> lazyUserSecurityObj = new Lazy<IUserSecurityService>(() => new UserSecurityService());

        public static IUserSecurityService IUserSecurityService { get { return lazyUserSecurityObj.Value; } }

        private static readonly Lazy<IMaHolderRegService> lazyMaHolderRegObj = new Lazy<IMaHolderRegService>(() => new MaHolderRegService());

        public static IMaHolderRegService IMaHolderRegService { get { return lazyMaHolderRegObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainServiceObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());
        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainServiceObj.Value; } }

        private static readonly Lazy<IUserLogSubService> lazyIUserLogSuberviceObj = new Lazy<IUserLogSubService>(() => new UserLogSubService());
        public static IUserLogSubService IUserLogSubService { get { return lazyIUserLogSuberviceObj.Value; } }

        String ic = "";
        bool valid = false;
        public static List<MaHolderReg> MaHolderRegDatas = new List<MaHolderReg>();
        public static MaHolderReg maHolderReg = new MaHolderReg();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["ActivateAccount"] != null)
                    {
                        int id = Convert.ToInt32(Request.QueryString["ActivateAccount"].ToString());
                        Response response = IUserService.GetSingle(id);
                        if (response.IsSuccess)
                        {

                            User x = (User)response.Data;
                            if (x != null)
                            {
                                x.Status = 1;
                                Response response2 = IUserService.UpdateData(x);
                                x = (User)response2.Data;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Alert', 'Account Activated!', '" + Request.ApplicationPath + "Index.aspx');", true);
                            }

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                        }
                    }
                }
                if (Session["user"] != null)
                {
                    captcha.Visible = false;
                    btnRequestCode.Visible = false;
                    btnRequestCodeForAdditionalAcc.Visible = true;
                    User user = (User)Session["user"];
                    txtEmail.Text = user.EmailId;
                    txtEmail.ReadOnly = true;
                    //##CHECK##
                    Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(user.UserIdUserAccounts.FirstOrDefault(x => x.IsPrimary == 1).AccountNo);
                    MaHolderReg maHolderReg = new MaHolderReg();
                    if (responseMHR.IsSuccess)
                    {
                        maHolderReg = (MaHolderReg)responseMHR.Data;
                    }
                    txtICNo.Text = maHolderReg.IdNo;
                    ic = maHolderReg.IdNo;
                    if (user.UserIdUserAccounts.FirstOrDefault(x => x.IsPrimary == 1).IsPrinciple == 0)
                    {
                        txtICNo.Text = maHolderReg.IdNo2;
                        ic = maHolderReg.IdNo2;
                    }
                    txtICNo.ReadOnly = true;
                    unregisteredUser.Visible = false;
                    cancelBtn.Attributes.Remove("href");
                    cancelBtn.Attributes.Add("href", "Portfolio.aspx");
                }
                else
                {
                    captcha.Visible = true;
                    btnRequestCode.Visible = true;
                    btnRequestCodeForAdditionalAcc.Visible = false;
                    unregisteredUser.Visible = true;
                    cancelBtn.Attributes.Remove("href");
                    cancelBtn.Attributes.Add("href", "Index.aspx");
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        protected void btnRequestCode_Click(object sender, EventArgs e)
        {
            try
            {
                string principleIC = "";
                String MANumber = txtMANumber.Text;
                String EmailId = txtEmail.Text;
                String ICNo = txtICNo.Text;
                User user = new User();


                if (valid == false)
                {
                    Regex regex = new Regex(@"^\d+$");
                    if (MANumber == "" || ICNo == "" || EmailId == "")
                    {
                        divMessage.Attributes.Add("class", "alert text-danger");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please enter required(*) fields.', '');", true);
                    }
                    else if (!regex.IsMatch(MANumber))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'MA No must be numeric.', '');", true);
                    }
                    else
                    {
                        if (ICNo.Contains("-"))
                        {
                            ICNo = ICNo.Replace("-", "");
                        }
                        if (ICNo.Contains(" "))
                        {
                            ICNo = ICNo.Replace(" ", "");
                        }
                        StringBuilder queryJoin = new StringBuilder();
                        StringBuilder queryCount = new StringBuilder();
                        StringBuilder queryData = new StringBuilder();
                        StringBuilder queryJoin2ndID = new StringBuilder();
                        StringBuilder filter = new StringBuilder();
                        queryData.Append(@"select * from ");
                        queryCount.Append(@"select count(*) from ");
                        queryJoin.Append(@" users u
                                                join user_accounts a on u.id = a.user_id where 1=1");
                        queryJoin2ndID.Append(@" ma_holder_reg where 1=1");
                        String filterID = " and u.id_no = '" + ICNo + "'";
                        String filterMA = " and a.account_no = '" + MANumber + "'";
                        String filterEmail = " and u.email_id = '" + EmailId + "'";
                        String filterStatus0 = " and u.status = '0'";
                        String filterStatus1 = " and u.status = '1'";
                        String filterPassword = " and u.password = ''";
                        String filterAltID = " and ID_NO='" + ICNo + "' or ID_NO_OLD='" + ICNo + "' ";
                        String filterUAStatus = " and a.is_verified = '0'";
                        String filterIDNO2 = " or ID_NO_2='" + ICNo + "'";
                        String filterMAHolderMA = " and holder_no ='" + MANumber + "'";


                        filter.Append(filterID);
                        Response responseUserExists = GenericService.GetCountByQuery(queryCount.ToString() + queryJoin.ToString() + filter.ToString());
                        if (responseUserExists.IsSuccess)
                        {
                            Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(MANumber);
                            if (responseMHR.IsSuccess)
                            {
                                maHolderReg = (MaHolderReg)responseMHR.Data;
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + responseMHR.Message + "\", '');", true);
                            }
                            var UsersIDCountDyn = responseUserExists.Data;
                            var responseJSON = JsonConvert.SerializeObject(UsersIDCountDyn);
                            int UsersIDCount = Newtonsoft.Json.JsonConvert.DeserializeObject<int>(responseJSON);
                            if (UsersIDCount > 0)//Condition A Is ID entered exist?
                            { //ID does exist
                                filter.Clear();
                                filter.Append(filterID + filterMA);
                                Response responseMA = GenericService.GetCountByQuery(queryCount.ToString() + queryJoin.ToString() + filter.ToString());
                                if (responseMA.IsSuccess)
                                {
                                    var UsersMACountDyn = responseMA.Data;
                                    var responseMAJSON = JsonConvert.SerializeObject(UsersMACountDyn);
                                    int UsersMACount = Newtonsoft.Json.JsonConvert.DeserializeObject<int>(responseMAJSON);
                                    if (UsersMACount > 0)//Condition B Is MA entered match with ID entered?
                                    {//MA does exist
                                        filter.Clear();
                                        filter.Append(filterID + filterMA + filterStatus0);
                                        Response responseStatus = GenericService.GetCountByQuery(queryCount.ToString() + queryJoin.ToString() + filter.ToString());
                                        if (responseStatus.IsSuccess)
                                        {
                                            var UsersStatusCountDyn = responseStatus.Data;
                                            var responseStatusJSON = JsonConvert.SerializeObject(UsersStatusCountDyn);
                                            int UsersStatusCount = Newtonsoft.Json.JsonConvert.DeserializeObject<int>(responseStatusJSON);
                                            if (UsersStatusCount > 0)//Condition D Is status == 0 ?
                                            {// status == 0
                                                filter.Clear();
                                                filter.Append(filterID + filterMA + filterStatus0 + filterPassword);
                                                Response responsePassword = GenericService.GetCountByQuery(queryCount.ToString() + queryJoin.ToString() + filter.ToString());
                                                if (responsePassword.IsSuccess)
                                                {
                                                    var UsersPasswordCountDyn = responsePassword.Data;
                                                    var responsePasswordJSON = JsonConvert.SerializeObject(UsersPasswordCountDyn);
                                                    int UsersPasswordCount = Newtonsoft.Json.JsonConvert.DeserializeObject<int>(responsePasswordJSON);
                                                    if (UsersPasswordCount > 0)//Condition F Is password == "" ?
                                                    {// password == ""
                                                        string siteURL = ConfigurationManager.AppSettings["siteURL"];
                                                        var unique_code = CustomGenerator.GenerateSixDigitPin();
                                                        filter.Clear();
                                                        filter.Append(filterID + filterMA + filterStatus0 + filterPassword);
                                                        Response responseEmail = GenericService.GetDataByQuery(queryData.ToString() + queryJoin.ToString() + filter.ToString(), 0, 20, false, null, false, null, false);
                                                        if (responseEmail.IsSuccess)
                                                        {
                                                            var UsersEmailDyn = responseEmail.Data;
                                                            var responseEmailJSON = JsonConvert.SerializeObject(UsersEmailDyn);
                                                            List<User> UsersEmail = Newtonsoft.Json.JsonConvert.DeserializeObject<List<User>>(responseEmailJSON);
                                                            List<User> users = new List<User>();
                                                            users = (List<User>)UsersEmail;
                                                            user = users.FirstOrDefault();
                                                            //Outcome 3 Update new email and send email
                                                            bool isNewEmail = false;
                                                            string oldEmail = user.EmailId;
                                                            if (user.EmailId != EmailId)
                                                            {
                                                                isNewEmail = true;
                                                            }
                                                            user.EmailId = EmailId;
                                                            user.EmailCode = unique_code;
                                                            user.ModifiedBy = 0;
                                                            user.ModifiedDate = DateTime.Now;
                                                            Response response = IUserService.UpdateData(user);
                                                            if (response.IsSuccess)
                                                            {
                                                                string selectedAccountHolderNo = "";
                                                                Response responseUAList = IUserAccountService.GetDataByPropertyName(nameof(UserAccount.UserId), user.Id.ToString(), true, 0, 0, false);
                                                                List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                                                                UserAccount primaryAccount = userAccounts.Where(x => x.AccountNo == selectedAccountHolderNo).FirstOrDefault();
                                                                Email email = new Email();
                                                                email.user = user;
                                                                email.link = siteURL + "ActivateAccount.aspx";
                                                                EmailService.SendVerificationLink(email, unique_code);

                                                                UserLogMain ulm = new UserLogMain()
                                                                {
                                                                    Description = isNewEmail ? "Requested for resend activation link to email: " + user.EmailId : "Requested for activation link to email: " + user.EmailId,
                                                                    TableName = "users",
                                                                    UpdatedDate = DateTime.Now,
                                                                    UserId = user.Id,
                                                                    UserAccountId = 0,
                                                                    RefId = user.Id,
                                                                    RefValue = user.EmailId,
                                                                    StatusType = 1
                                                                };
                                                                Response responseLog = IUserLogMainService.PostData(ulm);
                                                                if (responseLog.IsSuccess)
                                                                {
                                                                    ulm = (UserLogMain)responseLog.Data;
                                                                }
                                                                else
                                                                {
                                                                    //Audit log failed
                                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                                                                }
                                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Info', 'You will receive an email to activate your account.<br/><br/>Your mobile number is " + maHolderReg.HandPhoneNo + ".<br/><br/>If you required to update your mobile number, please <a href=\"/Contact.aspx\" target=\"_blank\">Contact Us</a> or visit our office if you need further assistance.', 'Index.aspx');", true);
                                                            }
                                                            else
                                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                                                        }
                                                        else
                                                        {
                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + responseEmail.Message + "\", '');", true);
                                                        }
                                                    }
                                                    else//Outcome 4 password != ""
                                                    {
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Your account has been deactivated by admin<br/><br/>Unfortunately, you cannot resend activation link to your email to activate your account.<br/><br/>Please contact Apex back office for support', 'OTP-Activation.aspx');", true);
                                                    }
                                                }
                                                else
                                                {
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + responsePassword.Message + "\", '');", true);
                                                }
                                            }
                                            else //Outcome 5 status == 1
                                            {
                                                filter.Clear();
                                                filter.Append(filterID + filterMA + filterStatus1 + filterUAStatus);
                                                Response responseUAStatus = GenericService.GetCountByQuery(queryCount.ToString() + queryJoin.ToString() + filter.ToString());
                                                if (responseUAStatus.IsSuccess)
                                                {
                                                    var UsersUAStatusCountDyn = responseUAStatus.Data;
                                                    var responseUAStatussJSON = JsonConvert.SerializeObject(UsersUAStatusCountDyn);
                                                    int UsersUAStatusCount = Newtonsoft.Json.JsonConvert.DeserializeObject<int>(responseUAStatussJSON);
                                                    if (UsersUAStatusCount > 0)
                                                    {
                                                        string siteURL = ConfigurationManager.AppSettings["siteURL"];
                                                        var unique_code = CustomGenerator.GenerateSixDigitPin();
                                                        filter.Clear();
                                                        filter.Append(filterID + filterMA + filterStatus1 + filterUAStatus);
                                                        Response responseEmail = GenericService.GetDataByQuery(queryData.ToString() + queryJoin.ToString() + filter.ToString(), 0, 20, false, null, false, null, false);
                                                        if (responseEmail.IsSuccess)
                                                        {
                                                            var UsersEmailDyn = responseEmail.Data;
                                                            var responseEmailJSON = JsonConvert.SerializeObject(UsersEmailDyn);
                                                            List<User> UsersEmail = Newtonsoft.Json.JsonConvert.DeserializeObject<List<User>>(responseEmailJSON);
                                                            List<User> users = new List<User>();
                                                            users = (List<User>)UsersEmail;
                                                            user = users.FirstOrDefault();
                                                            //Outcome 2 Update new email and send email
                                                            bool isNewEmail = false;
                                                            string oldEmail = user.EmailId;
                                                            if (user.EmailId != EmailId)
                                                            {
                                                                isNewEmail = true;
                                                            }
                                                            user.EmailId = EmailId;
                                                            user.EmailCode = unique_code;
                                                            user.ModifiedBy = 0;
                                                            user.ModifiedDate = DateTime.Now;
                                                            user.Password = "";
                                                            user.TransPwd = "";
                                                            user.IsActive = 0;
                                                            Response response = IUserService.UpdateData(user);
                                                            if (response.IsSuccess)
                                                            {
                                                                string selectedAccountHolderNo = "";
                                                                Response responseUAList = IUserAccountService.GetDataByPropertyName(nameof(UserAccount.UserId), user.Id.ToString(), true, 0, 0, false);
                                                                List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                                                                UserAccount primaryAccount = userAccounts.Where(x => x.AccountNo == selectedAccountHolderNo).FirstOrDefault();
                                                                Email email = new Email();
                                                                email.user = user;
                                                                email.link = siteURL + "ActivateAccount.aspx";
                                                                EmailService.SendVerificationLink(email, unique_code);

                                                                UserLogMain ulm = new UserLogMain()
                                                                {
                                                                    Description = isNewEmail ? "Requested for resend activation link to email: " + user.EmailId : "Requested for activation link to email: " + user.EmailId,
                                                                    TableName = "users",
                                                                    UpdatedDate = DateTime.Now,
                                                                    UserId = user.Id,
                                                                    UserAccountId = 0,
                                                                    RefId = user.Id,
                                                                    RefValue = user.EmailId,
                                                                    StatusType= 1
                                                                };
                                                                Response responseLog = IUserLogMainService.PostData(ulm);
                                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Info', 'You will receive an email to activate your account.<br/><br/>Your mobile number is " + maHolderReg.HandPhoneNo + ".<br/><br/>If you required to update your mobile number, please <a href=\"/Contact.aspx\" target=\"_blank\">Contact Us</a> or visit our office if you need further assistance.', 'Index.aspx');", true);

                                                            }
                                                            else
                                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                                                        }
                                                        else
                                                        {
                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + responseEmail.Message + "\", '');", true);
                                                        }
                                                    }
                                                    else //Outcome 1 User have already registered
                                                    {
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'MA already registered with the Identity No. you entered', 'OTP-Activation.aspx');", true);
                                                    }
                                                }
                                                else
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + responseUAStatus + "\", '');", true);
                                            }
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + responseStatus.Message + "\", '');", true);
                                        }
                                    }
                                    else //Outcome 5 MA does not exist
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'This MA cannot be registered<br/><br/>Please login to your existing user account to add your new MA', 'Index.aspx');", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + responseMA.Message + "\", '');", true);
                                }
                            }
                            else //ID does not exist 
                            {
                                filter.Clear();
                                filter.Append(filterMA);
                                Response responseMACount = GenericService.GetCountByQuery(queryCount.ToString() + queryJoin.ToString() + filter.ToString());
                                if (responseMACount.IsSuccess)
                                {
                                    var UsersMADyn = responseMACount.Data;
                                    var responseMAJSON = JsonConvert.SerializeObject(UsersMADyn);
                                    int UsersMA = Newtonsoft.Json.JsonConvert.DeserializeObject<int>(responseMAJSON);
                                    if (UsersMA > 0)//Condition C Is MA entered match with ID entered?
                                    {//MA does exist

                                        filter.Clear();
                                        filter.Append(filterMAHolderMA);
                                        Response responseGetAltIDandIDNO2 = GenericService.GetDataByQuery(queryData.ToString() + queryJoin2ndID.ToString() + filter.ToString(), 0, 20, false, null, false, null, false);
                                        if (responseGetAltIDandIDNO2.IsSuccess)
                                        {
                                            String maUserIDNO = "";
                                            String maUserOldIDNO = "";
                                            String maUserIDNO2 = "";
                                            var AltIDandIDNO2DataDyn = responseGetAltIDandIDNO2.Data;
                                            var responseGetAltIDandIDNO2JSON = JsonConvert.SerializeObject(AltIDandIDNO2DataDyn);
                                            List<MaHolderReg> MAUsersJSON = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MaHolderReg>>(responseGetAltIDandIDNO2JSON);
                                            List<MaHolderReg> MAUsers = new List<MaHolderReg>();
                                            MAUsers = (List<MaHolderReg>)MAUsersJSON;
                                            foreach (MaHolderReg x in MAUsers)
                                            {
                                                maUserIDNO = x.IdNo.Replace("-", "");
                                                maUserOldIDNO = x.IdNoOld;
                                                maUserIDNO2 = x.IdNo2.Replace("-", "");
                                            }


                                            if (ICNo == maUserOldIDNO || ICNo == maUserIDNO2)// NEW condition Is Entered MA NO. registered with other ID?
                                            {// IsAlternateID?
                                                filter.Clear();
                                                filter.Append(filterAltID);
                                                Response responseGetAltID = GenericService.GetCountByQuery(queryCount.ToString() + queryJoin2ndID.ToString() + filter.ToString());
                                                if (responseGetAltID.IsSuccess)
                                                {
                                                    var AltIDDataDyn = responseGetAltID.Data;
                                                    var responseGetAltIDJSON = JsonConvert.SerializeObject(AltIDDataDyn);
                                                    int GetAltID = Newtonsoft.Json.JsonConvert.DeserializeObject<int>(responseGetAltIDJSON);
                                                    if (GetAltID > 0)// Condition E Is Entered ID No. match in existing maHolder_reg record with ID_NO or ID_NO_OLD?
                                                    {//Outcome 6
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'MA already registered with your old ID or new ID before', 'OTP-Activation.aspx');", true);
                                                    }
                                                    else// NEW REGISTRATION FOR SECONDARY HOLDER
                                                    {
                                                        if (Int64.TryParse(MANumber.Trim(), out long result))
                                                        {
                                                            if (maHolderReg != null)
                                                            {
                                                                if (maHolderReg.HolderStatus == "I" || maHolderReg.HolderStatus == "S")
                                                                {
                                                                    divMessage.Attributes.Add("class", "alert text-danger");
                                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', '" + (maHolderReg.HolderStatus == "I" ? "MA Inactive." : (maHolderReg.HolderStatus == "S" ? "MA Suspended." : "")) + "', '');", true);
                                                                }
                                                                else if (CustomValues.GetAccounPlan(maHolderReg.HolderCls.Trim()) == "CORP")
                                                                {
                                                                    divMessage.Attributes.Add("class", "alert text-danger");
                                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Corporate Account can only be registered by admin.', '');", true);

                                                                }
                                                                else
                                                                {
                                                                    bool isValidMANumber = false;
                                                                    string reasonInvalid = "";

                                                                    {

                                                                        if (maHolderReg != null)
                                                                        {
                                                                            if (ICNo.Length == 12 && ICNo.All(Char.IsDigit))
                                                                            {
                                                                                String IDNo = maHolderReg.IdNo;
                                                                                String IDNo2 = maHolderReg.IdNo2;
                                                                                String NewID = IDNo;
                                                                                String NewID2 = (IDNo2 == null ? "" : IDNo2);
                                                                                if (NewID.Contains("-"))
                                                                                {
                                                                                    NewID = NewID.Replace("-", "");
                                                                                }
                                                                                if (NewID.Contains(" "))
                                                                                {
                                                                                    NewID = NewID.Replace(" ", "");
                                                                                }
                                                                                if (NewID2.Contains("-"))
                                                                                {
                                                                                    NewID2 = NewID2.Replace("-", "");
                                                                                }
                                                                                if (NewID2.Contains(" "))
                                                                                {
                                                                                    NewID2 = NewID2.Replace(" ", "");
                                                                                }
                                                                                if (NewID == ICNo)
                                                                                {
                                                                                    isValidMANumber = true;
                                                                                    Response response = ServicesManager.GetMaHolderRegsByIdNo(maHolderReg.IdNo);
                                                                                    if (response.IsSuccess)
                                                                                    {
                                                                                        principleIC = maHolderReg.IdNo;
                                                                                        MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                                                    }
                                                                                }
                                                                                else if (NewID2 == ICNo)
                                                                                {
                                                                                    isValidMANumber = true;
                                                                                    Response response = ServicesManager.GetMaHolderRegsByIdNo(maHolderReg.IdNo2);
                                                                                    if (response.IsSuccess)
                                                                                    {
                                                                                        principleIC = maHolderReg.IdNo;
                                                                                        MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    isValidMANumber = false;
                                                                                }
                                                                            }
                                                                            else if (ICNo.Length != 12 && ICNo.All(Char.IsDigit))
                                                                            {
                                                                                String IDNo = maHolderReg.IdNo;
                                                                                String IDNo2 = maHolderReg.IdNo2;
                                                                                String NewID = IDNo;
                                                                                String NewID2 = (IDNo2 == null ? "" : IDNo2);
                                                                                if (NewID.Contains("-"))
                                                                                {
                                                                                    NewID = NewID.Replace("-", "");
                                                                                }
                                                                                if (NewID.Contains(" "))
                                                                                {
                                                                                    NewID = NewID.Replace(" ", "");
                                                                                }
                                                                                if (NewID2.Contains("-"))
                                                                                {
                                                                                    NewID2 = NewID2.Replace("-", "");
                                                                                }
                                                                                if (NewID2.Contains(" "))
                                                                                {
                                                                                    NewID2 = NewID2.Replace(" ", "");
                                                                                }
                                                                                if (NewID == ICNo)
                                                                                {
                                                                                    isValidMANumber = true;
                                                                                    Response response = ServicesManager.GetMaHolderRegsByIdNo(maHolderReg.IdNo);
                                                                                    if (response.IsSuccess)
                                                                                    {
                                                                                        principleIC = maHolderReg.IdNo;
                                                                                        MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                                                    }
                                                                                }
                                                                                else if (NewID2 == ICNo)
                                                                                {
                                                                                    isValidMANumber = true;
                                                                                    Response response = ServicesManager.GetMaHolderRegsByIdNo(maHolderReg.IdNo2);
                                                                                    if (response.IsSuccess)
                                                                                    {
                                                                                        principleIC = maHolderReg.IdNo;
                                                                                        MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    isValidMANumber = false;
                                                                                }
                                                                            }
                                                                            else if (ICNo.Length == 12 && !ICNo.All(Char.IsDigit))
                                                                            {
                                                                                divMessage.Attributes.Add("class", "alert text-danger");
                                                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid NRIC / OLD NRIC / PASSPORT.', '');", true);
                                                                                return;
                                                                            }
                                                                            else
                                                                            {
                                                                                String IDNo = maHolderReg.IdNoOld;
                                                                                String NewID = IDNo;
                                                                                if (NewID.Contains("-"))
                                                                                {
                                                                                    NewID = NewID.Replace("-", "");
                                                                                }
                                                                                if (NewID.Contains(" "))
                                                                                {
                                                                                    NewID = NewID.Replace(" ", "");
                                                                                }
                                                                                if (NewID == ICNo)
                                                                                {
                                                                                    isValidMANumber = true;
                                                                                    Response response = ServicesManager.GetMaHolderRegsByIdNo(IDNo);
                                                                                    if (response.IsSuccess)
                                                                                    {
                                                                                        principleIC = maHolderReg.IdNo;
                                                                                        MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    isValidMANumber = false;
                                                                                    if (maHolderReg.IdNo2 != null)
                                                                                    {
                                                                                        IDNo = maHolderReg.IdNo2;
                                                                                        NewID = IDNo;
                                                                                        if (NewID.Contains("-"))
                                                                                        {
                                                                                            NewID = NewID.Replace("-", "");
                                                                                        }
                                                                                        if (NewID.Contains(" "))
                                                                                        {
                                                                                            NewID = NewID.Replace(" ", "");
                                                                                        }
                                                                                        if (NewID == ICNo)
                                                                                        {
                                                                                            isValidMANumber = true;
                                                                                            Response response = ServicesManager.GetMaHolderRegsByIdNo(IDNo);
                                                                                            if (response.IsSuccess)
                                                                                            {
                                                                                                MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            isValidMANumber = false;
                                                                                        }
                                                                                    }

                                                                                }
                                                                            }
                                                                        }
                                                                        if (isValidMANumber)
                                                                        {
                                                                            List<MaHolderReg> maHolderRegsNew = new List<MaHolderReg>();
                                                                            if (MaHolderRegDatas.Count > 0)
                                                                            {
                                                                                MaHolderRegDatas = MaHolderRegDatas.Where(x => x.HolderStatus == "A").ToList();
                                                                                MaHolderRegDatas.ForEach(x =>
                                                                                {
                                                                                    bool isAdd = true;
                                                                                    if (x.IdNo2 != null)
                                                                                    {
                                                                                        if (x.IdNo2.Contains("STA"))
                                                                                        {
                                                                                            isAdd = false;
                                                                                        }
                                                                                    }
                                                                                    if (x.IdNo3 != null)
                                                                                    {
                                                                                        if (x.IdNo3.Contains("STA") || x.IdNo3.Contains("10JF"))
                                                                                        {
                                                                                            isAdd = false;
                                                                                        }
                                                                                    }
                                                                                    if (isAdd)
                                                                                    {
                                                                                        x.OtpActSt = "2";
                                                                                        maHolderRegsNew.Add(x);
                                                                                    }
                                                                                });
                                                                                //Check
                                                                                MaHolderRegDatas = maHolderRegsNew;
                                                                            }

                                                                            if (Utility.Helper.CustomValidator.IsValidEmail(EmailId))
                                                                            {
                                                                                string principleICNew = principleIC;
                                                                                if (principleICNew.Contains("-"))
                                                                                {
                                                                                    principleICNew = principleICNew.Replace("-", "");
                                                                                }
                                                                                if (principleICNew.Contains(" "))
                                                                                {
                                                                                    principleICNew = principleICNew.Replace(" ", "");
                                                                                }
                                                                                Response responseUAList = IUserAccountService.GetDataByFilter(" account_no='" + MANumber + "' and is_verified=1 and is_principle=" + (principleICNew != ICNo ? 0 : 1) + " ", 0, 0, false);
                                                                                List<UserAccount> userAccounts = new List<UserAccount>();
                                                                                if (responseUAList.IsSuccess)
                                                                                {
                                                                                    userAccounts = (List<UserAccount>)responseUAList.Data;
                                                                                    if (userAccounts.Count == 0)
                                                                                    {
                                                                                        string toMobileNumber = maHolderReg.HandPhoneNo.Trim();
                                                                                        if (principleICNew != ICNo)
                                                                                        {
                                                                                            toMobileNumber = maHolderReg.JointTelNo.Trim();
                                                                                        }
                                                                                        if (toMobileNumber == null || toMobileNumber == "")
                                                                                        {
                                                                                            divMessage.Attributes.Add("class", "alert text-danger");
                                                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'No mobile number for MA. Please contact our back office.', '');", true);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            {
                                                                                                {
                                                                                                    if (toMobileNumber.Contains("-"))
                                                                                                    {
                                                                                                        toMobileNumber = toMobileNumber.Replace("-", "");
                                                                                                    }
                                                                                                    if (toMobileNumber.Contains(" "))
                                                                                                    {
                                                                                                        toMobileNumber = toMobileNumber.Replace(" ", "");
                                                                                                    }
                                                                                                    StringBuilder filterR = new StringBuilder();
                                                                                                    filterR.Append(" 1=1");
                                                                                                    filterR.Append(" and " + Converter.GetColumnNameByPropertyName<User>(nameof(Utility.User.EmailId)) + " = '" + EmailId + "'");
                                                                                                    Response responseUList = IUserService.GetDataByFilter(filterR.ToString(), 0, 0, false);
                                                                                                    if (responseUList.IsSuccess)
                                                                                                    {
                                                                                                        List<User> userMatches = (List<User>)responseUList.Data;

                                                                                                        if (userMatches.Count > 0)
                                                                                                        {
                                                                                                            divMessage.Attributes.Add("class", "alert text-danger");
                                                                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Account with this email already exists.', '');", true);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            if (IsReCaptchValid() == true)
                                                                                                            {
                                                                                                                hdnMobileNoPop.Value = "1";
                                                                                                                string regDetails = "<tr><td>MA Number</td><td>" + MANumber + "</td></tr>";
                                                                                                                regDetails += "<tr><td>Email Id</td><td>" + EmailId + "</td></tr>";
                                                                                                                regDetails += "<tr><td>IC No</td><td>" + ICNo + "</td></tr>";
                                                                                                                registrationDetailsTbody.InnerHtml = regDetails;
                                                                                                                // (1) success scenario
                                                                                                                lblMobileNo.InnerHtml = "Your mobile number is " + (principleICNew != ICNo ? maHolderReg.JointTelNo : maHolderReg.HandPhoneNo) + ".<br/><br/>If you required to update your mobile number, please <a href=\"/Contact.aspx\" target=\"_blank\">Contact Us</a> or visit our office if you need further assistance..";

                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                divMessage.Attributes.Add("class", "alert text-danger");
                                                                                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please verify yourself.', '');", true);
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUList.Message + "\", '');", true);
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        divMessage.Attributes.Add("class", "alert text-danger");
                                                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'This MA already registered.', '');", true);
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    divMessage.Attributes.Add("class", "alert text-danger");
                                                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                divMessage.Attributes.Add("class", "alert text-danger");
                                                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid Email.', '');", true);
                                                                            }

                                                                        }
                                                                        else if (MaHolderRegDatas.Count == 0 || !isValidMANumber) // This condition means 
                                                                        {
                                                                            divMessage.Attributes.Add("class", "alert text-danger");
                                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid IC/Passport Number or MA Number.', '');", true);
                                                                        }
                                                                        else
                                                                        {

                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                //This condition is when the input MA number is not found in database
                                                                divMessage.Attributes.Add("class", "alert text-danger");
                                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid IC/Passport Number or MA Number.', '');", true);
                                                            }



                                                        }
                                                        else
                                                        {
                                                            divMessage.Attributes.Add("class", "alert text-danger");
                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'MA No must be Numeric.', '');", true);
                                                        }
                                                    }
                                                }
                                                else
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseGetAltID.Message + "\", '');", true);
                                            }
                                            else// Outcome 7
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'This MA has been registered with other ID No.<br/><br/>Please contact to Apex back office for support', 'OTP-Activation.aspx');", true);
                                        }
                                        else
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseGetAltIDandIDNO2.Message + "\", '');", true);


                                    }
                                    else //MA does not exist
                                    {
                                        int usersExist = Convert.ToInt32(responseUserExists.Data.ToString());
                                        if (usersExist == 0)// NEW REGISTRATION Outcome No.1
                                        {
                                            if (Int64.TryParse(MANumber.Trim(), out long result))
                                            {
                                                if (maHolderReg != null)
                                                {
                                                    if (maHolderReg.HolderStatus == "I" || maHolderReg.HolderStatus == "S")
                                                    {
                                                        divMessage.Attributes.Add("class", "alert text-danger");
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', '" + (maHolderReg.HolderStatus == "I" ? "MA Inactive." : (maHolderReg.HolderStatus == "S" ? "MA Suspended." : "")) + "', '');", true);
                                                    }
                                                    else if (CustomValues.GetAccounPlan(maHolderReg.HolderCls.Trim()) == "CORP")
                                                    {
                                                        divMessage.Attributes.Add("class", "alert text-danger");
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Corporate Account can only be registered by admin.', '');", true);

                                                    }
                                                    else
                                                    {
                                                        bool isValidMANumber = false;
                                                        string reasonInvalid = "";

                                                        {

                                                            if (maHolderReg != null)
                                                            {
                                                                if (ICNo.Length == 12 && ICNo.All(Char.IsDigit))
                                                                {
                                                                    String IDNo = maHolderReg.IdNo;
                                                                    String IDNo2 = maHolderReg.IdNo2;
                                                                    String NewID = IDNo;
                                                                    String NewID2 = (IDNo2 == null ? "" : IDNo2);
                                                                    if (NewID.Contains("-"))
                                                                    {
                                                                        NewID = NewID.Replace("-", "");
                                                                    }
                                                                    if (NewID.Contains(" "))
                                                                    {
                                                                        NewID = NewID.Replace(" ", "");
                                                                    }
                                                                    if (NewID2.Contains("-"))
                                                                    {
                                                                        NewID2 = NewID2.Replace("-", "");
                                                                    }
                                                                    if (NewID2.Contains(" "))
                                                                    {
                                                                        NewID2 = NewID2.Replace(" ", "");
                                                                    }
                                                                    if (NewID == ICNo)
                                                                    {
                                                                        isValidMANumber = true;
                                                                        Response response = ServicesManager.GetMaHolderRegsByIdNo(maHolderReg.IdNo);
                                                                        if (response.IsSuccess)
                                                                        {
                                                                            principleIC = maHolderReg.IdNo;
                                                                            MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                                        }
                                                                    }
                                                                    else if (NewID2 == ICNo)
                                                                    {
                                                                        isValidMANumber = true;
                                                                        Response response = ServicesManager.GetMaHolderRegsByIdNo(maHolderReg.IdNo2);
                                                                        if (response.IsSuccess)
                                                                        {
                                                                            principleIC = maHolderReg.IdNo;
                                                                            MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        isValidMANumber = false;
                                                                    }
                                                                }
                                                                else if (ICNo.Length != 12 && ICNo.All(Char.IsDigit))
                                                                {
                                                                    String IDNo = maHolderReg.IdNo;
                                                                    String IDNo2 = maHolderReg.IdNo2;
                                                                    String NewID = IDNo;
                                                                    String NewID2 = (IDNo2 == null ? "" : IDNo2);
                                                                    if (NewID.Contains("-"))
                                                                    {
                                                                        NewID = NewID.Replace("-", "");
                                                                    }
                                                                    if (NewID.Contains(" "))
                                                                    {
                                                                        NewID = NewID.Replace(" ", "");
                                                                    }
                                                                    if (NewID2.Contains("-"))
                                                                    {
                                                                        NewID2 = NewID2.Replace("-", "");
                                                                    }
                                                                    if (NewID2.Contains(" "))
                                                                    {
                                                                        NewID2 = NewID2.Replace(" ", "");
                                                                    }
                                                                    if (NewID == ICNo)
                                                                    {
                                                                        isValidMANumber = true;
                                                                        Response response = ServicesManager.GetMaHolderRegsByIdNo(maHolderReg.IdNo);
                                                                        if (response.IsSuccess)
                                                                        {
                                                                            principleIC = maHolderReg.IdNo;
                                                                            MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                                        }
                                                                    }
                                                                    else if (NewID2 == ICNo)
                                                                    {
                                                                        isValidMANumber = true;
                                                                        Response response = ServicesManager.GetMaHolderRegsByIdNo(maHolderReg.IdNo2);
                                                                        if (response.IsSuccess)
                                                                        {
                                                                            principleIC = maHolderReg.IdNo;
                                                                            MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        isValidMANumber = false;
                                                                    }
                                                                }
                                                                else if (ICNo.Length == 12 && !ICNo.All(Char.IsDigit))
                                                                {
                                                                    divMessage.Attributes.Add("class", "alert text-danger");
                                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid NRIC / OLD NRIC / PASSPORT.', '');", true);
                                                                    return;
                                                                }
                                                                else
                                                                {
                                                                    String IDNo = maHolderReg.IdNoOld;
                                                                    String NewID = IDNo;
                                                                    if (NewID.Contains("-"))
                                                                    {
                                                                        NewID = NewID.Replace("-", "");
                                                                    }
                                                                    if (NewID.Contains(" "))
                                                                    {
                                                                        NewID = NewID.Replace(" ", "");
                                                                    }
                                                                    if (NewID == ICNo)
                                                                    {
                                                                        isValidMANumber = true;
                                                                        Response response = ServicesManager.GetMaHolderRegsByIdNo(IDNo);
                                                                        if (response.IsSuccess)
                                                                        {
                                                                            principleIC = maHolderReg.IdNo;
                                                                            MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        isValidMANumber = false;
                                                                        if (maHolderReg.IdNo2 != null)
                                                                        {
                                                                            IDNo = maHolderReg.IdNo2;
                                                                            NewID = IDNo;
                                                                            if (NewID.Contains("-"))
                                                                            {
                                                                                NewID = NewID.Replace("-", "");
                                                                            }
                                                                            if (NewID.Contains(" "))
                                                                            {
                                                                                NewID = NewID.Replace(" ", "");
                                                                            }
                                                                            if (NewID == ICNo)
                                                                            {
                                                                                isValidMANumber = true;
                                                                                Response response = ServicesManager.GetMaHolderRegsByIdNo(IDNo);
                                                                                if (response.IsSuccess)
                                                                                {
                                                                                    MaHolderRegDatas = (List<MaHolderReg>)response.Data;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                isValidMANumber = false;
                                                                            }
                                                                        }

                                                                    }
                                                                }
                                                            }
                                                            if (isValidMANumber)
                                                            {
                                                                List<MaHolderReg> maHolderRegsNew = new List<MaHolderReg>();
                                                                if (MaHolderRegDatas.Count > 0)
                                                                {
                                                                    MaHolderRegDatas = MaHolderRegDatas.Where(x => x.HolderStatus == "A").ToList();
                                                                    MaHolderRegDatas.ForEach(x =>
                                                                    {
                                                                        bool isAdd = true;
                                                                        if (x.IdNo2 != null)
                                                                        {
                                                                            if (x.IdNo2.Contains("STA"))
                                                                            {
                                                                                isAdd = false;
                                                                            }
                                                                        }
                                                                        if (x.IdNo3 != null)
                                                                        {
                                                                            if (x.IdNo3.Contains("STA") || x.IdNo3.Contains("10JF"))
                                                                            {
                                                                                isAdd = false;
                                                                            }
                                                                        }
                                                                        if (isAdd)
                                                                        {
                                                                            x.OtpActSt = "2";
                                                                            maHolderRegsNew.Add(x);
                                                                        }
                                                                    });
                                                                    //Check
                                                                    MaHolderRegDatas = maHolderRegsNew;
                                                                }

                                                                if (Utility.Helper.CustomValidator.IsValidEmail(EmailId))
                                                                {
                                                                    string principleICNew = principleIC;
                                                                    if (principleICNew.Contains("-"))
                                                                    {
                                                                        principleICNew = principleICNew.Replace("-", "");
                                                                    }
                                                                    if (principleICNew.Contains(" "))
                                                                    {
                                                                        principleICNew = principleICNew.Replace(" ", "");
                                                                    }
                                                                    Response responseUAList = IUserAccountService.GetDataByFilter(" account_no='" + MANumber + "' and is_verified=1 and is_principle=" + (principleICNew != ICNo ? 0 : 1) + " ", 0, 0, false);
                                                                    List<UserAccount> userAccounts = new List<UserAccount>();
                                                                    if (responseUAList.IsSuccess)
                                                                    {
                                                                        userAccounts = (List<UserAccount>)responseUAList.Data;
                                                                        if (userAccounts.Count == 0)
                                                                        {
                                                                            string toMobileNumber = maHolderReg.HandPhoneNo;
                                                                            if (principleICNew != ICNo)
                                                                            {
                                                                                toMobileNumber = maHolderReg.JointTelNo.Trim();
                                                                            }
                                                                            if (toMobileNumber == null || toMobileNumber == "")
                                                                            {
                                                                                divMessage.Attributes.Add("class", "alert text-danger");
                                                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'No mobile number for MA. Please contact our back office.', '');", true);
                                                                            }

                                                                            else
                                                                            {




                                                                                if (toMobileNumber.Contains("-"))
                                                                                {
                                                                                    toMobileNumber = toMobileNumber.Replace("-", "");
                                                                                }
                                                                                if (toMobileNumber.Contains(" "))
                                                                                {
                                                                                    toMobileNumber = toMobileNumber.Replace(" ", "");
                                                                                }
                                                                                StringBuilder filterR = new StringBuilder();
                                                                                filterR.Append(" 1=1");
                                                                                filterR.Append(" and " + Converter.GetColumnNameByPropertyName<User>(nameof(Utility.User.EmailId)) + " = '" + EmailId + "'");
                                                                                Response responseUList = IUserService.GetDataByFilter(filterR.ToString(), 0, 0, false);
                                                                                if (responseUList.IsSuccess)
                                                                                {
                                                                                    List<User> userMatches = (List<User>)responseUList.Data;

                                                                                    if (userMatches.Count > 0)
                                                                                    {
                                                                                        divMessage.Attributes.Add("class", "alert text-danger");
                                                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Account with this email already exists.', '');", true);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if (IsReCaptchValid() == true)
                                                                                        {
                                                                                            hdnMobileNoPop.Value = "1";
                                                                                            string regDetails = "<tr><td>MA Number</td><td>" + MANumber + "</td></tr>";
                                                                                            regDetails += "<tr><td>Email Id</td><td>" + EmailId + "</td></tr>";
                                                                                            regDetails += "<tr><td>IC No</td><td>" + ICNo + "</td></tr>";
                                                                                            registrationDetailsTbody.InnerHtml = regDetails;
                                                                                            // (1) success scenario
                                                                                            lblMobileNo.InnerHtml = "Your mobile number is " + maHolderReg.HandPhoneNo + ".<br/><br/>If you required to update your mobile number, please <a href=\"/Contact.aspx\" target=\"_blank\">Contact Us</a> or visit our office if you need further assistance.";

                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            divMessage.Attributes.Add("class", "alert text-danger");
                                                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please verify yourself.', '');", true);
                                                                                        }
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUList.Message + "\", '');", true);
                                                                                }


                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            divMessage.Attributes.Add("class", "alert text-danger");
                                                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'This MA already registered.', '');", true);
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        divMessage.Attributes.Add("class", "alert text-danger");
                                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    divMessage.Attributes.Add("class", "alert text-danger");
                                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid Email.', '');", true);
                                                                }

                                                            }
                                                            else if (MaHolderRegDatas.Count == 0 || !isValidMANumber) // This condition means 
                                                            {
                                                                divMessage.Attributes.Add("class", "alert text-danger");
                                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid IC/Passport Number or MA Number.', '');", true);
                                                            }
                                                            else
                                                            {

                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //This condition is when the input MA number is not found in database
                                                    divMessage.Attributes.Add("class", "alert text-danger");
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid IC/Passport Number or MA Number.', '');", true);
                                                }



                                            }
                                            else
                                            {
                                                divMessage.Attributes.Add("class", "alert text-danger");
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'MA No must be Numeric.', '');", true);
                                            }
                                        }
                                        else
                                        {
                                            divMessage.Attributes.Add("class", "alert text-danger");
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid IC/Passport Number or MA Number.', '');", true);
                                        }
                                    }

                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + responseMACount.Message + "\", '');", true);
                                }

                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + responseUserExists.Message + "\", '');", true);
                        }
                    }
                }
                else if (valid == true)
                {
                    principleIC = txtICNo.Text;
                    if (principleIC.Contains("-"))
                    {
                        principleIC = principleIC.Replace("-", "");
                    }
                    if (principleIC.Contains(" "))
                    {
                        principleIC = principleIC.Replace(" ", "");
                    }
                    string idno = maHolderReg.IdNo;
                    if (idno.Contains("-"))
                    {
                        idno = idno.Replace("-", "");
                    }
                    if (idno.Contains(" "))
                    {
                        idno = idno.Replace(" ", "");
                    }
                    string idno2 = (maHolderReg.IdNo2 == null ? "" : maHolderReg.IdNo2);

                    if (idno2.Contains("-"))
                    {
                        idno2 = idno2.Replace("-", "");
                    }
                    if (idno2.Contains(" "))
                    {
                        idno2 = idno2.Replace(" ", "");
                    }

                    user = new User
                    {
                        Username = (principleIC == idno ? maHolderReg.Name1 : maHolderReg.Name2),
                        EmailId = EmailId.ToLower(),
                        Password = "",
                        MobileNumber = (principleIC == idno ? maHolderReg.HandPhoneNo : maHolderReg.JointTelNo),
                        IdNo = principleIC,
                        CreatedDate = DateTime.Now,
                        IsActive = 0,
                        IsSecurityChecked = 0,
                        IsSatChecked = 0,
                        Status = 0,
                        RegisterIp = TrackIPAddress.GetUserPublicIP(hdnLocalIPAddress.Value) + ", " + hdnLocalIPAddress.Value,
                        ModifiedBy = 0,
                        VerificationCode = 0,
                        VerifyExpired = 1,
                        ResetExpired = 1
                    };
                    user.UserIdUserAccounts = new List<UserAccount>();
                    List<MaHolderReg> maHolderRegCash = MaHolderRegDatas.Where(x => CustomValues.GetAccounPlan(x.HolderCls.Trim()) == "CASH").ToList();
                    if (maHolderRegCash.Count > 0)
                    {
                        MaHolderReg ma = maHolderRegCash.FirstOrDefault();
                        user.UserIdUserAccounts.Add(new UserAccount
                        {
                            UserId = user.Id,
                            AccountNo = ma.HolderNo.ToString(),
                            IdNo = principleIC,
                            CreatedBy = 0,
                            CreatedDate = DateTime.Now,
                            Status = 1,
                            IsVerified = 0,
                            MaHolderRegId = ma.Id,
                            ModifiedBy = 0,
                            HolderClass = ma.HolderCls,
                            MaHolderRegIdMaHolderReg = ma,
                            IsPrinciple = 1
                        });
                    }
                    List<MaHolderReg> maHolderRegEpf = MaHolderRegDatas.Where(x => CustomValues.GetAccounPlan(x.HolderCls.Trim()) == "EPF").ToList();
                    if (maHolderRegEpf.Count > 0)
                    {
                        MaHolderReg ma = maHolderRegEpf.FirstOrDefault();
                        user.UserIdUserAccounts.Add(new UserAccount
                        {
                            UserId = user.Id,
                            AccountNo = ma.HolderNo.ToString(),
                            IdNo = principleIC,
                            CreatedBy = 0,
                            CreatedDate = DateTime.Now,
                            Status = 1,
                            IsVerified = 0,
                            MaHolderRegId = ma.Id,
                            ModifiedBy = 0,
                            HolderClass = ma.HolderCls,
                            MaHolderRegIdMaHolderReg = ma,
                            IsPrinciple = 1
                        });
                    }
                    List<MaHolderReg> maHolderRegJoints = MaHolderRegDatas.Where(x => CustomValues.GetAccounPlan(x.HolderCls.Trim()) == "JOINT").ToList();
                    if (maHolderRegJoints.Count > 0)
                    {
                        List<MaHolderReg> mas = maHolderRegJoints;
                        List<UserAccount> userAccountJoints = new List<UserAccount>();
                        mas.ForEach(ma =>
                        {
                            string IDNo = ma.IdNo;
                            if (IDNo.Contains("-"))
                            {
                                IDNo = IDNo.Replace("-", "");
                            }
                            if (principleIC.Contains(" "))
                            {
                                IDNo = IDNo.Replace(" ", "");
                            }
                            userAccountJoints.Add(new UserAccount
                            {
                                UserId = user.Id,
                                AccountNo = ma.HolderNo.ToString(),
                                IdNo = principleIC,
                                CreatedBy = 0,
                                CreatedDate = DateTime.Now,
                                Status = 1,
                                IsVerified = 0,
                                MaHolderRegId = ma.Id,
                                ModifiedBy = 0,
                                HolderClass = ma.HolderCls,
                                MaHolderRegIdMaHolderReg = ma,
                                IsPrinciple = (principleIC == IDNo ? 1 : 0)
                            });
                        });
                        userAccountJoints = userAccountJoints.OrderByDescending(x => x.IsPrinciple).ToList();
                        user.UserIdUserAccounts.AddRange(userAccountJoints);
                    }
                    string primaryPlan = "";
                    List<UserAccount> uAs = user.UserIdUserAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "CASH").ToList();
                    if (uAs.Count > 0)
                    {
                        primaryPlan = "CASH";
                    }
                    else
                    {
                        uAs = user.UserIdUserAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "EPF").ToList();
                        if (uAs.Count > 0)
                        {
                            primaryPlan = "EPF";
                        }
                        else
                        {
                            uAs = user.UserIdUserAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "JOINT").ToList();
                            if (uAs.Count > 0)
                            {
                                primaryPlan = "JOINT";
                            }
                        }
                    }
                    if (primaryPlan == "JOINT")
                    {
                        int i = 0;
                        user.UserIdUserAccounts.ForEach(x =>
                        {
                            string plan = CustomValues.GetAccounPlan(x.HolderClass);
                            string idno11 = x.MaHolderRegIdMaHolderReg.IdNo;
                            if (idno11.Contains("-"))
                                idno11 = idno11.Replace("-", "");
                            if (idno11.Contains(" "))
                                idno11 = idno11.Replace(" ", "");
                            string idno22 = x.MaHolderRegIdMaHolderReg.IdNo2;
                            if (idno22.Contains("-"))
                                idno22 = idno22.Replace("-", "");
                            if (idno22.Contains(" "))
                                idno22 = idno22.Replace(" ", "");
                            if (plan == primaryPlan && x.IsPrinciple == 1 && principleIC == idno11)
                            {
                                if (i == 0)
                                    x.IsPrimary = 1;
                                i++;
                            }
                            else if (plan == primaryPlan && x.IsPrinciple == 0 && principleIC == idno22)
                            {
                                if (i == 0)
                                    x.IsPrimary = 1;
                                i++;
                            }
                        });
                    }
                    else
                    {
                        user.UserIdUserAccounts.ForEach(x =>
                        {
                            string plan = CustomValues.GetAccounPlan(x.HolderClass);
                            if (plan == primaryPlan)
                            {
                                x.IsPrimary = 1;
                            }
                        });
                    }


                    Response response = IUserService.PostData(user);
                    if (response.IsSuccess)
                    {
                        User returnUser = (User)response.Data;

                        UserLogMain ulm = new UserLogMain()
                        {
                            Description = "User registered and activation link sent to email",
                            TableName = "users",
                            UpdatedDate = DateTime.Now,
                            UserId = returnUser.Id,
                            UserAccountId = 0,
                            RefId = returnUser.Id,
                            RefValue = returnUser.EmailId,
                            StatusType = 1
                        };
                        Response responseLog = IUserLogMainService.PostData(ulm);
                        if (!responseLog.IsSuccess)
                        {
                            //Audit log failed
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                        }

                        try
                        {
                            int count = IMaHolderRegService.PostBulkData(MaHolderRegDatas);
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(ex.Message);

                        }
                        //SMSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
                        divMessage.Attributes.Add("class", "alert alert-success");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'An activation link will be send to " + EmailId + " .<br/>If you required to update your mobile number, please <a href=\"/Contact.aspx\" target=\"_blank\">Contact Us</a> or visit our office if you need further assistance.', 'Index.aspx');", true);
                        // An OTP will sent to mobile number " + displayMoileNumber + ".<br/>
                    }
                    else
                    {
                        divMessage.Attributes.Add("class", "alert alert-danger");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                    }

                    //sent activation email
                    string siteURL = ConfigurationManager.AppSettings["siteURL"];


                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("OTP-Activation Page btnRequestCode_Click " + ex.Message);

            }
        }

        public void function(string principleIC, User user, string EmailId, string ICNoCompare)
        {
            principleIC = txtICNo.Text;
            if (principleIC.Contains("-"))
            {
                principleIC = principleIC.Replace("-", "");
            }
            if (principleIC.Contains(" "))
            {
                principleIC = principleIC.Replace(" ", "");
            }
            string idno = maHolderReg.IdNo;
            if (idno.Contains("-"))
            {
                idno = idno.Replace("-", "");
            }
            if (idno.Contains(" "))
            {
                idno = idno.Replace(" ", "");
            }
            string idno2 = (maHolderReg.IdNo2 == null ? "" : maHolderReg.IdNo2);

            if (idno2.Contains("-"))
            {
                idno2 = idno2.Replace("-", "");
            }
            if (idno2.Contains(" "))
            {
                idno2 = idno2.Replace(" ", "");
            }

            user = new User
            {
                Username = (principleIC == idno ? maHolderReg.Name1 : maHolderReg.Name2),
                EmailId = EmailId.ToLower(),
                Password = "",
                MobileNumber = (principleIC == idno ? maHolderReg.HandPhoneNo : maHolderReg.JointTelNo),
                IdNo = principleIC,
                CreatedDate = DateTime.Now,
                IsActive = 0,
                IsSecurityChecked = 0,
                IsSatChecked = 0,
                Status = 0,
                RegisterIp = TrackIPAddress.GetUserPublicIP(hdnLocalIPAddress.Value) + ", " + hdnLocalIPAddress.Value,
                ModifiedBy = 0,
                VerificationCode = 0,
                VerifyExpired = 1,
                ResetExpired = 1
            };
            user.UserIdUserAccounts = new List<UserAccount>();
            //                                           CASH HOLDER
            List<MaHolderReg> maHolderRegCash = MaHolderRegDatas.Where(x => CustomValues.GetAccounPlan(x.HolderCls.Trim()) == "CASH").ToList();
            if (maHolderRegCash.Count > 0)
            {
                MaHolderReg ma = maHolderRegCash.FirstOrDefault();
                user.UserIdUserAccounts.Add(new UserAccount
                {
                    UserId = user.Id,
                    AccountNo = ma.HolderNo.ToString(),
                    IdNo = principleIC,
                    CreatedBy = 0,
                    CreatedDate = DateTime.Now,
                    Status = 1,
                    IsVerified = 0,
                    MaHolderRegId = ma.Id,
                    ModifiedBy = 0,
                    HolderClass = ma.HolderCls,
                    MaHolderRegIdMaHolderReg = ma,
                    IsPrinciple = 1
                });
            }
            //                                              EPF HOLDER
            List<MaHolderReg> maHolderRegEpf = MaHolderRegDatas.Where(x => CustomValues.GetAccounPlan(x.HolderCls.Trim()) == "EPF").ToList();
            if (maHolderRegEpf.Count > 0)
            {
                MaHolderReg ma = maHolderRegEpf.FirstOrDefault();
                user.UserIdUserAccounts.Add(new UserAccount
                {
                    UserId = user.Id,
                    AccountNo = ma.HolderNo.ToString(),
                    IdNo = principleIC,
                    CreatedBy = 0,
                    CreatedDate = DateTime.Now,
                    Status = 1,
                    IsVerified = 0,
                    MaHolderRegId = ma.Id,
                    ModifiedBy = 0,
                    HolderClass = ma.HolderCls,
                    MaHolderRegIdMaHolderReg = ma,
                    IsPrinciple = 1
                });
            }
            //                                                JOINT HOLDER
            List<MaHolderReg> maHolderRegJoints = MaHolderRegDatas.Where(x => CustomValues.GetAccounPlan(x.HolderCls.Trim()) == "JOINT").ToList();
            if (maHolderRegJoints.Count > 0)
            {
                List<MaHolderReg> mas = maHolderRegJoints;
                List<UserAccount> userAccountJoints = new List<UserAccount>();
                mas.ForEach(ma =>
                {
                    string IDNo = ma.IdNo;
                    if (IDNo.Contains("-"))
                    {
                        IDNo = IDNo.Replace("-", "");
                    }
                    if (principleIC.Contains(" "))
                    {
                        IDNo = IDNo.Replace(" ", "");
                    }
                    userAccountJoints.Add(new UserAccount
                    {
                        UserId = user.Id,
                        AccountNo = ma.HolderNo.ToString(),
                        IdNo = principleIC,
                        CreatedBy = 0,
                        CreatedDate = DateTime.Now,
                        Status = 1,
                        IsVerified = 0,
                        MaHolderRegId = ma.Id,
                        ModifiedBy = 0,
                        HolderClass = ma.HolderCls,
                        MaHolderRegIdMaHolderReg = ma,
                        IsPrinciple = (principleIC == IDNo ? 1 : 0)
                    });
                });
                userAccountJoints = userAccountJoints.OrderByDescending(x => x.IsPrinciple).ToList();
                user.UserIdUserAccounts.AddRange(userAccountJoints);
            }
            string primaryPlan = "";
            List<UserAccount> uAs = user.UserIdUserAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "CASH").ToList();
            if (uAs.Count > 0)
            {
                primaryPlan = "CASH";
            }
            else
            {
                uAs = user.UserIdUserAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "EPF").ToList();
                if (uAs.Count > 0)
                {
                    primaryPlan = "EPF";
                }
                else
                {
                    uAs = user.UserIdUserAccounts.Where(x => CustomValues.GetAccounPlan(x.HolderClass) == "JOINT").ToList();
                    if (uAs.Count > 0)
                    {
                        primaryPlan = "JOINT";
                    }
                }
            }
            if (primaryPlan == "JOINT")
            {
                int i = 0;
                user.UserIdUserAccounts.ForEach(x =>
                {
                    string plan = CustomValues.GetAccounPlan(x.HolderClass);
                    string idno11 = x.MaHolderRegIdMaHolderReg.IdNo;
                    if (idno11.Contains("-"))
                        idno11 = idno11.Replace("-", "");
                    if (idno11.Contains(" "))
                        idno11 = idno11.Replace(" ", "");
                    string idno22 = x.MaHolderRegIdMaHolderReg.IdNo2;
                    if (idno22.Contains("-"))
                        idno22 = idno22.Replace("-", "");
                    if (idno22.Contains(" "))
                        idno22 = idno22.Replace(" ", "");
                    if (plan == primaryPlan && x.IsPrinciple == 1 && principleIC == idno11)
                    {
                        if (i == 0)
                            x.IsPrimary = 1;
                        i++;
                    }
                    else if (plan == primaryPlan && x.IsPrinciple == 0 && principleIC == idno22)
                    {
                        if (i == 0)
                            x.IsPrimary = 1;
                        i++;
                    }
                });
            }
            else
            {
                user.UserIdUserAccounts.ForEach(x =>
                {
                    string plan = CustomValues.GetAccounPlan(x.HolderClass);
                    if (plan == primaryPlan)
                    {
                        x.IsPrimary = 1;
                    }
                });
            }


            Response response = IUserService.PostData(user);
            if (response.IsSuccess)
            {
                User returnUser = (User)response.Data;

                UserLogMain ulm = new UserLogMain()
                {
                    Description = "User registered and activation link sent to email",
                    TableName = "users",
                    UpdatedDate = DateTime.Now,
                    UserId = returnUser.Id,
                    UserAccountId = 0,
                    RefId = returnUser.Id,
                    RefValue = returnUser.EmailId,
                    StatusType = 1
                };
                Response responseLog = IUserLogMainService.PostData(ulm);
                if (!responseLog.IsSuccess)
                {
                    //Audit log failed
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Warning', \"Audit log failed. " + responseLog.Message + "\", 'Index.aspx');", true);
                }

                try
                {
                    int count = IMaHolderRegService.PostBulkData(MaHolderRegDatas);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(ex.Message);

                }
                //SMSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
                divMessage.Attributes.Add("class", "alert alert-success");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'An activation link will be send to " + EmailId + " .<br/>If you required to update your mobile number, please <a href=\"/Contact.aspx\" target=\"_blank\">Contact Us</a> or visit our office if you need further assistance.', 'Index.aspx');", true);
                // An OTP will sent to mobile number " + displayMoileNumber + ".<br/>
            }
            else
            {
                divMessage.Attributes.Add("class", "alert alert-danger");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
            }

            //sent activation email
            string siteURL = ConfigurationManager.AppSettings["siteURL"];
        }

        protected void btnRequestCodeForAdditionalAcc_Click(object sender, EventArgs e)
        {
            try
            {
                String EmailId = txtEmail.Text;
                String MANumber = txtMANumber.Text.Trim();
                String ICNo = txtICNo.Text;
                ICNo = ICNo.Replace("-", "");
                ICNo = ICNo.Replace(" ", "");


                if (MANumber == "")
                {
                    divMessage.Attributes.Add("class", "alert text-danger");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please enter required(*) fields.', '');", true);

                }
                else
                {
                    if (!(Int32.TryParse(MANumber.Trim(), out int result)))
                    {
                        divMessage.Attributes.Add("class", "alert text-danger");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'MA No must be Numeric.', '');", true);
                    }
                    else
                    {
                        Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(MANumber);
                        if (responseMHR.IsSuccess)
                        {
                            MaHolderReg maHolderReg = (MaHolderReg)responseMHR.Data;
                            if (maHolderReg == null)
                            {
                                divMessage.Attributes.Add("class", "alert text-danger");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid MA Number.', '');", true);
                            }
                            else
                            {
                                if (maHolderReg.IdNo == ic || maHolderReg.IdNo2 == ic || maHolderReg.IdNo3 == ic || maHolderReg.IdNo4 == ic || maHolderReg.IdNo5 == ic)
                                {
                                    if (maHolderReg.HolderStatus == "I" || maHolderReg.HolderStatus == "S")
                                    {
                                        divMessage.Attributes.Add("class", "alert text-danger");
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', '" + (maHolderReg.HolderStatus == "I" ? "MA Inactive." : (maHolderReg.HolderStatus == "S" ? "MA Suspended." : "")) + "', '');", true);
                                    }
                                    else if (CustomValues.GetAccounPlan(maHolderReg.HolderCls.Trim()) == "CORP")
                                    {
                                        divMessage.Attributes.Add("class", "alert text-danger");
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Corporate Account can only be registered by admin.', '');", true);
                                    }
                                    else
                                    {
                                        User user = (User)Session["user"];
                                        Response responseUser = IUserService.GetDataByFilter("ID = '" + user.Id + "' ", 0, 0, false);
                                        if (responseUser.IsSuccess)
                                        {
                                            User userMatch = ((List<User>)responseUser.Data).FirstOrDefault();
                                            if (EmailId != userMatch.EmailId)
                                            {
                                                divMessage.Attributes.Add("class", "alert text-danger");
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'This is not the email that registered before.', '');", true);
                                            }
                                            else
                                            {
                                                List<UserAccount> userAccounts = new List<UserAccount>();
                                                Response response = IUserAccountService.GetDataByFilter(" user_id='" + user.Id + "' and account_no='" + MANumber + "' ", 0, 0, false);
                                                if (response.IsSuccess)
                                                {
                                                    ic = ic.Replace("-", "");
                                                    ic = ic.Replace(" ", "");
                                                    userAccounts = (List<UserAccount>)response.Data;
                                                    if (userAccounts.Count == 0)
                                                    {
                                                        int IsPrinciple = 1;
                                                        String maIDNo = maHolderReg.IdNo;
                                                        maIDNo = maIDNo.Replace("-", "");
                                                        maIDNo = maIDNo.Replace(" ", "");
                                                        if (maIDNo != ic)
                                                        {
                                                            IsPrinciple = 0;
                                                            maIDNo = maHolderReg.IdNo2;
                                                            maIDNo = maIDNo.Replace("-", "");
                                                            maIDNo = maIDNo.Replace(" ", "");
                                                        }

                                                        UserAccount uA = new UserAccount
                                                        {
                                                            UserId = user.Id,
                                                            AccountNo = MANumber,
                                                            IdNo = maIDNo,
                                                            HolderClass = maHolderReg.HolderCls,
                                                            CreatedBy = 0,
                                                            CreatedDate = DateTime.Now,
                                                            Status = 1,
                                                            IsVerified = 0,
                                                            VerificationCode = 0,
                                                            VerifyExpired = 1,
                                                            MaHolderRegId = maHolderReg.Id,
                                                            ModifiedBy = 0,
                                                            IsPrinciple = IsPrinciple
                                                        };
                                                        if (IsPrinciple == 1 && user.UserIdUserAccounts.FirstOrDefault(x => x.IsPrimary == 1) != null && user.UserIdUserAccounts.FirstOrDefault(x => x.IsPrimary == 1).IsPrinciple == 0)
                                                        {
                                                            uA.IsPrimary = 1;
                                                            Response responseUAPrimary = IUserAccountService.GetSingle(user.UserIdUserAccounts.FirstOrDefault(x => x.IsPrimary == 1).Id);
                                                            if (responseUAPrimary.IsSuccess)
                                                            {
                                                                UserAccount userAccountPrimary = (UserAccount)responseUAPrimary.Data;
                                                                userAccountPrimary.IsPrimary = 0;
                                                                IUserAccountService.UpdateData(userAccountPrimary);

                                                                Response userDBResponse = IUserService.GetSingle(user.Id);
                                                                if (userDBResponse.IsSuccess)
                                                                {
                                                                    User userDB = (User)userDBResponse.Data;
                                                                    userDB.MobileNumber = maHolderReg.HandPhoneNo;
                                                                    IUserService.UpdateData(userDB);
                                                                }
                                                            }
                                                        }
                                                        IUserAccountService.PostData(uA);
                                                        maHolderReg.OtpActSt = "2";
                                                        IMaHolderRegService.PostData(maHolderReg);

                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "ShowCustomMessage('Alert', 'Additional account is added successfully. Please proceed to activate.', 'Portfolio.aspx');", true);
                                                    }
                                                    else
                                                    {
                                                        divMessage.Attributes.Add("class", "alert text-danger");
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Account already exist.', '');", true);
                                                    }
                                                }
                                                else
                                                {
                                                    divMessage.Attributes.Add("class", "alert text-danger");
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            divMessage.Attributes.Add("class", "alert text-danger");
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUser.Message + "\", '');", true);
                                        }
                                    }
                                }
                                else
                                {
                                    divMessage.Attributes.Add("class", "alert text-danger");
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'This MA not belong to this user', '');", true);
                                }
                            }
                        }
                        else
                        {
                            divMessage.Attributes.Add("class", "alert text-danger");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHR.Message + "\", '');", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("OTP-Activation Page btnRequestCodeForAdditionalAcc_Click " + ex.Message);
            }
        }



        public bool IsReCaptchValid()
        {
            var response = Request["g-recaptcha-response"];
            string secretKey = "6LfXd58UAAAAACGUfwpdtRDFOlKFHjxCI5JSUeAd";
            var client = new WebClient();
            var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            var obj = JObject.Parse(result);
            var status = (bool)obj.SelectToken("success");

            if (status == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void btnProceed_Click(object sender, EventArgs e)
        {
            valid = true;
            hdnMobileNoPop.Value = "0";
            this.btnRequestCode_Click(sender, e);
        }
        protected void btnExit_Click(object sender, EventArgs e)
        {
            Response.Redirect("OTP-Activation.aspx", false);
        }
    }
}