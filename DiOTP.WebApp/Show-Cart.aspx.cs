﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.FPXLibary;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class Show_Cart : System.Web.UI.Page
    {
        public static List<Cart> cartItems = new List<Cart>();

        private static readonly Lazy<IUserService> lazyIUserService = new Lazy<IUserService>(() => new UserService());

        public static IUserService IUserService { get { return lazyIUserService.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountServiceObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountServiceObj.Value; } }

        private static readonly Lazy<IMaHolderBankService> lazyMaHolderBankServiceObj = new Lazy<IMaHolderBankService>(() => new MaHolderBankService());

        public static IMaHolderBankService IMaHolderBankService { get { return lazyMaHolderBankServiceObj.Value; } }

        private static readonly Lazy<IBanksDefService> lazyBanksDefServiceObj = new Lazy<IBanksDefService>(() => new BanksDefService());

        public static IBanksDefService IBanksDefService { get { return lazyBanksDefServiceObj.Value; } }

        private static readonly Lazy<IUserOrderService> lazyUserOrderService = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderService.Value; } }

        private static readonly Lazy<IUserOrderCartService> lazyUserOrderCartObj = new Lazy<IUserOrderCartService>(() => new UserOrderCartService());

        public static IUserOrderCartService IUserOrderCartService { get { return lazyUserOrderCartObj.Value; } }

        private static readonly Lazy<IDvDistributionIntructionService> lazyDvDistributionIntructionObj = new Lazy<IDvDistributionIntructionService>(() => new DvDistributionIntructionService());

        public static IDvDistributionIntructionService IDvDistributionIntructionService { get { return lazyDvDistributionIntructionObj.Value; } }

        private static readonly Lazy<IUserOrderFileService> lazyUserOrderFileObj = new Lazy<IUserOrderFileService>(() => new UserOrderFileService());

        public static IUserOrderFileService IUserOrderFileService { get { return lazyUserOrderFileObj.Value; } }

        private static readonly Lazy<IUserLogMainService> lazyUserLogMainObj = new Lazy<IUserLogMainService>(() => new UserLogMainService());

        public static IUserLogMainService IUserLogMainService { get { return lazyUserLogMainObj.Value; } }


        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        public string orderNo { get; set; }

        public static int RSPFee = 1;

        public List<UserOrder> UserOrders { get; set; }
        public string accountId = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] != null)
                {
                    orderNo = Request.QueryString["UON"] != null ? Request.QueryString["UON"].ToString() : "";
                    if (!string.IsNullOrEmpty(orderNo))
                    {
                        User user = (User)Session["user"];
                        if (!IsPostBack)
                        {
                            if (Request.QueryString["OrderType"] != null && Request.QueryString["OrderType"].ToString() == "RSP")
                            {
                                ddlPaymentMethodsList.Items.Clear();
                                ddlPaymentMethodsList.Items.Add(new ListItem("Select Payment Method", ""));
                                ddlPaymentMethodsList.Items.Add(new ListItem("FPX Payment", "1"));
                            }
                        }


                        {

                            Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                            if (responseUAList.IsSuccess)
                            {
                                List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;

                                Response responseO = IUserOrderService.GetDataByFilter(" order_no='" + (orderNo.Replace("'", "''")) + "' and order_status=1 and user_id=" + user.Id + " ", 0, 0, false);
                                if (responseO.IsSuccess)
                                {
                                    UserOrders = (List<UserOrder>)responseO.Data;

                                    string selectedAccountHolderNo = "";
                                    selectedAccountHolderNo = userAccounts.FirstOrDefault(x => x.Id == UserOrders.FirstOrDefault().UserAccountId).AccountNo;

                                    UserAccount primaryAccount = userAccounts.Where(x => x.AccountNo == selectedAccountHolderNo).FirstOrDefault();
                                    accountId = primaryAccount.AccountNo;


                                    if (UserOrders.Count > 0)
                                    {
                                        Response responseAllItems = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), orderNo.Replace("'", "''"), true, 0, 0, false);
                                        if (responseAllItems.IsSuccess)
                                        {
                                            UserOrders = (List<UserOrder>)responseAllItems.Data;
                                            if (UserOrders.Count > 0)
                                            {
                                                BindCart(UserOrders);
                                                Response response = IUserService.GetSingle(user.Id);
                                                if (response.IsSuccess)
                                                {
                                                    user = (User)response.Data;
                                                    maAccNo.InnerHtml = primaryAccount.AccountNo;

                                                    Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAccount.AccountNo);
                                                    MaHolderReg maHolderReg = new MaHolderReg();
                                                    if (responseMHR.IsSuccess)
                                                    {
                                                        maHolderReg = (MaHolderReg)responseMHR.Data;
                                                        if (!string.IsNullOrEmpty(maHolderReg.IdNo))
                                                            lblUserIC.Value = maHolderReg.IdNo.Replace("-", "");
                                                    }
                                                    bankDetailsDiv.Visible = false;

                                                    cartTotalAmount.Attributes.Remove("class");
                                                    if (UserOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.Buy || UserOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.RSP)
                                                    {
                                                        cartTotalAmount.Attributes.Add("class", "text-right currencyFormat");

                                                        thSalesCharge.Visible = true;
                                                        totalTfootTH.ColSpan = 6;
                                                        if (UserOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.Buy)
                                                        {
                                                            TCofTransaction.InnerHtml = "Terms & Conditions of this Investment";
                                                            TCofBuy.Visible = true;
                                                            TCofSell.Visible = false;
                                                            TCofSwitch.Visible = false;
                                                            //TCofRSP.Visible = false;
                                                            cartType.InnerText = "Cart - Buy Fund";
                                                            transType.InnerHtml = "Investment Amount <br /> MYR";
                                                        }
                                                        else
                                                        {
                                                            TCofTransaction.InnerHtml = "Terms & Conditions of this Investment";
                                                            TCofSell.Visible = false;
                                                            TCofBuy.Visible = true;
                                                            //TCofRSP.Visible = true;
                                                            cartType.InnerText = "Cart - Regular Savings Plan";
                                                            cartTotalAmount.Visible = false;
                                                            cartTotalAmount.InnerText = "1";
                                                            totalTfootTH.ColSpan = 6;
                                                            totalTfootTH.InnerText = "Subscription Fee (MYR)";
                                                            totalTfootTH.Visible = false;
                                                            transType.InnerHtml = "RSP Amount <br /> MYR";
                                                            ddlPaymentMethodsList.SelectedIndex = ddlPaymentMethodsList.Items.IndexOf(ddlPaymentMethodsList.Items.FindByText("FPX Payment"));
                                                            ddlPaymentMethodsList.Enabled = false;

                                                        }
                                                    }
                                                    if (UserOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.Sell)
                                                    {
                                                        TCofTransaction.InnerHtml = "Terms & Conditions of this Redemption";
                                                        TCofBuy.Visible = false;
                                                        TCofSell.Visible = true;
                                                        TCofSwitch.Visible = false;
                                                        //TCofRSP.Visible = false;
                                                        cartType.InnerText = "Cart - Sell Fund";
                                                        thSalesCharge.Visible = false;
                                                        cartTotalAmount.Attributes.Add("class", "text-right unitFormatNoSymbol");
                                                        transType.InnerHtml = "Redemption Units";
                                                        if (CustomValues.GetAccounPlan(maHolderReg.HolderCls) != "EPF")
                                                        {
                                                            bankDetailsDiv.Visible = true;
                                                            Response responseMHBList = IMaHolderBankService.GetDataByFilter(" user_id=" + primaryAccount.UserId + " and id=" + UserOrders.FirstOrDefault().BankCode + " ", 0, 0, true);
                                                            if (responseMHBList.IsSuccess)
                                                            {
                                                                MaHolderBank hb = ((List<MaHolderBank>)responseMHBList.Data).FirstOrDefault();
                                                                StringBuilder sb = new StringBuilder();
                                                                if (hb != null)
                                                                {
                                                                    Response response2 = IBanksDefService.GetSingle(hb.BankDefId);
                                                                    if (response2.IsSuccess)
                                                                    {
                                                                        BanksDef b = (BanksDef)response2.Data;
                                                                        if (b.Status == 1)
                                                                        {
                                                                            sb.Append(@"<tr>
                                                                                <td>" + (b.ShortName != "" ? b.ShortName + " - " : "") + b.Name + @"</td>
                                                                                <td>" + hb.AccountName + @"</td>
                                                                                <td>" + hb.BankAccountNo + @"</td>
                                                                            </tr>");
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response2.Message + "\", '');", true);
                                                                    }
                                                                }
                                                                bankDetailsTbody.InnerHtml = sb.ToString();
                                                                totalTfootTH.ColSpan = 5;
                                                            }
                                                            else
                                                            {
                                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMHBList.Message + "\", '');", true);
                                                            }
                                                        }
                                                        else
                                                        {

                                                        }
                                                    }
                                                    if (UserOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.Buy)
                                                    {
                                                        transactionType.InnerText = "purchased";
                                                    }
                                                    else if (UserOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.Sell)
                                                    {
                                                        totalTfoot.Visible = false;
                                                        transactionType.InnerText = "redempt";
                                                        cartTotalAmount.Visible = false;
                                                    }
                                                    else if (UserOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.SwitchOut)
                                                    {
                                                        TCofTransaction.InnerHtml = "Terms & Conditions of this Switching";
                                                        TCofBuy.Visible = false;
                                                        TCofSell.Visible = false;
                                                        TCofSwitch.Visible = true;
                                                        //TCofRSP.Visible = false;
                                                        totalTfoot.Visible = false;
                                                        thSalesCharge.Visible = false;
                                                        cartTotalAmount.Visible = false;
                                                        cartTotalAmount.Attributes.Add("class", "text-right unitFormatNoSymbol");
                                                        fund1TD.InnerHtml = "Switching Fund";
                                                        fund2TD.InnerHtml = "To Fund";
                                                        fund2TD.Visible = true;
                                                        totalTfootTH.ColSpan = 6;
                                                        transType.InnerHtml = "Switched Unit";
                                                        cartType.InnerText = "Cart - Switching";
                                                    }
                                                    else if (UserOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.RSP)
                                                    {
                                                        transactionType.InnerText = "direct debited";

                                                        subscriptionFee.InnerHtml = "Subscription Fee <br /> MYR";

                                                        rspNote.Visible = true;
                                                        btnPayLater.Visible = false;
                                                    }




                                                    ddlUserAccountId.Items.Clear();
                                                    ddlUserAccountId.Items.Add(new ListItem(CustomValues.GetAccounPlan(primaryAccount.HolderClass) + " - " + primaryAccount.AccountNo.ToString(), primaryAccount.AccountNo.ToString()));
                                                    ddlUserAccountId.Enabled = false;
                                                }
                                                else
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + response.Message + "\", '');", true);
                                            }
                                            else
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid Order!', 'OrderList.aspx');", true);
                                        }
                                        else
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseAllItems.Message + "\", '');", true);
                                    }
                                    else
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid Order!', 'OrderList.aspx');", true);
                                }
                                else
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseO.Message + "\", '');", true);
                            }
                            else
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                        }
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid Order!', 'OrderList.aspx');", true);
                    if (!Page.IsPostBack)
                    {
                        //Payment
                        if (UserOrders.Count != 0 && UserOrders.FirstOrDefault().OrderType == 1 || UserOrders.FirstOrDefault().OrderType == 6)
                        {
                            buyPayDiv.Attributes.Remove("class");
                            buyPayDiv.Attributes.Add("class", "row mb-10 mt-10");
                            {
                                String checksum = "";
                                String fpx_msgType = "BE";
                                String fpx_msgToken = "01";
                                String fpx_sellerExId = "EX00008813"; //Production is EX00011078
                                String fpx_version = "7.0";
                                String fpx_checkSum = "";
                                fpx_checkSum = fpx_msgToken + "|" + fpx_msgType + "|" + fpx_sellerExId + "|" + fpx_version;
                                fpx_checkSum = fpx_checkSum.Trim();
                                Controller c = new Controller();
                                checksum = c.RSASign(fpx_checkSum, Request.PhysicalApplicationPath + "EX00008813.key"); //Exchange Key //Production is EX00011078
                                String finalMsg = fpx_checkSum;

                                Logger.WriteLog("fpx_msgToken=" + fpx_msgToken + "\n" +
                                    "fpx_msgType=" + fpx_msgType + "\n" +
                                    "fpx_sellerExId=" + fpx_sellerExId + "");
                                Logger.WriteLog("fpx_version="+ fpx_version + "");
                                Logger.WriteLog("fpx_checkSum="+ checksum + "");


                                string dlc_content = "fpxmsgToken=" + fpx_msgToken + "&fpx_msgToken=" + fpx_msgToken + "&fpx_msgType=" + fpx_msgType + "&fpx_sellerExId=" + fpx_sellerExId + "&fpx_version=" + fpx_version + "&fpx_checkSum=" + checksum;

                                string resp = "";
                                string error = "";
                                var resps = new Dictionary<string, string>();
                                string posting_data = "";
                                try
                                {
                                    posting_data = dlc_content;
                                    System.Net.ServicePointManager.ServerCertificateValidationCallback += (s, cert, chain, sslPolicyErrors) => true;
                                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                    string url = ConfigurationManager.AppSettings["FPXBankListUrl"].ToString();   //URL for send HTTP request
                                    System.Net.HttpWebRequest objRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                                    objRequest.Method = "POST";
                                    objRequest.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
                                    objRequest.Accept = "application/json";
                                    objRequest.AllowAutoRedirect = true;
                                    objRequest.KeepAlive = false;
                                    objRequest.Timeout = 300000;
                                    byte[] _byteVersion = Encoding.ASCII.GetBytes(string.Concat("content=", posting_data));
                                    objRequest.ContentLength = _byteVersion.Length;
                                    Stream stream = objRequest.GetRequestStream();
                                    stream.Write(_byteVersion, 0, _byteVersion.Length);
                                    stream.Close();

                                    using (System.Net.HttpWebResponse objResponse = (System.Net.HttpWebResponse)objRequest.GetResponse())
                                    using (StreamReader reader = new StreamReader(objResponse.GetResponseStream()))
                                    {
                                        resp = ((reader.ReadToEnd()).Trim());
                                    }
                                    NameValueCollection qscoll;
                                    qscoll = System.Web.HttpUtility.ParseQueryString(resp);
                                    if (qscoll.AllKeys[0] != null)
                                    {
                                        foreach (String key in qscoll.AllKeys)
                                            resps.Add(key, qscoll[key]);
                                        string bankListString = (qscoll["fpx_bankList"].Replace("%7E", "~")).Replace("%2C", ", ");
                                        List<string> bankList = bankListString.Split(',').ToList<string>();
                                        List<ListItem> banksList = new List<ListItem>();
                                        bankList.ForEach(x =>
                                        {
                                            Type t = GetType("FPXBank");
                                            string BankName = x.Split('~')[0];
                                            FPXBank b = FPXEnumeration.GetByBankId(t, BankName);
                                            string BankStatus = x.Split('~')[1];
                                            banksList.Add(new ListItem
                                            {
                                                Text = (b == null ? BankName : b.DisplayName) + (BankStatus == "B" ? " (Offline)" : ""),
                                                Value = BankName,
                                                Enabled = (BankStatus == "B" ? false : true)
                                            });
                                        });
                                        ddlBankList.DataSource = banksList.OrderBy(a => a.Text).ToList();
                                        ddlBankList.DataTextField = "Text";
                                        ddlBankList.DataValueField = "Value";
                                        ddlBankList.DataBind();
                                        ddlBankList.Items.Insert(0, new ListItem { Text = "Select Bank", Value = "" });
                                    }
                                    else
                                    {
                                        ddlBankList.Items.Insert(0, new ListItem { Text = resp, Value = "" });
                                    }
                                }
                                catch (Exception exception)
                                {
                                    resp = "ERROR";
                                    error = exception.ToString();
                                }

                            }
                        }
                        //Payment
                        else
                        {
                            buyPayDiv.Attributes.Remove("class");
                            buyPayDiv.Attributes.Add("class", "row mb-10 mt-10 hide");
                        }
                    }

                }
                else
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
            //}
        }

        public void BindCart(List<UserOrder> userOrders)
        {
            try
            {
                if (userOrders.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (UserOrder uo in userOrders)
                    {
                        UtmcFundInformation utmcFundInformation1 = new UtmcFundInformation();
                        Response responseUFI1 = IUtmcFundInformationService.GetDataByFilter(" id=" + uo.FundId + " ", 0, 0, true);
                        if (responseUFI1.IsSuccess)
                        {
                            List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)responseUFI1.Data;
                            if (utmcFundInformations.Count > 0)
                            {
                                utmcFundInformation1 = utmcFundInformations.FirstOrDefault();
                            }
                        }

                        UtmcFundInformation utmcFundInformation2 = new UtmcFundInformation();
                        if (uo.ToFundId != 0)
                        {
                            Response responseUFI2 = IUtmcFundInformationService.GetDataByFilter(" id=" + uo.ToFundId + " ", 0, 0, true);
                            if (responseUFI2.IsSuccess)
                            {
                                List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)responseUFI2.Data;
                                if (utmcFundInformations.Count > 0)
                                {
                                    utmcFundInformation2 = utmcFundInformations.FirstOrDefault();
                                }
                            }
                        }

                        UserAccount userAccount2 = new UserAccount();
                        if (uo.ToAccountId != 0)
                        {
                            Response responseUFI2 = IUserAccountService.GetDataByFilter(" id=" + uo.ToAccountId + " and status=1 ", 0, 0, true);
                            if (responseUFI2.IsSuccess)
                            {
                                List<UserAccount> userAccounts = (List<UserAccount>)responseUFI2.Data;
                                if (userAccounts.Count > 0)
                                {
                                    userAccount2 = userAccounts.FirstOrDefault();
                                }
                            }
                        }

                        UtmcFundFile masterProspectus = utmcFundInformation1.UtmcFundInformationIdUtmcFundFiles.Where(x => x.UtmcFundFileTypesDefId == 1).FirstOrDefault();
                        UtmcFundFile phs = utmcFundInformation1.UtmcFundInformationIdUtmcFundFiles.Where(x => x.UtmcFundFileTypesDefId == 3).FirstOrDefault();
                        UtmcFundFile im = utmcFundInformation1.UtmcFundInformationIdUtmcFundFiles.Where(x => x.Name == "Information Memorandum").FirstOrDefault();
                        if (uo.OrderType != 4)
                            sb.Append(@"<tr>
                                    <td><span class='fundName mobile'>" + utmcFundInformation1.FundName.Capitalize() + @" (" + utmcFundInformation1.FundCode + @")</span></td>
                                    " + (uo.OrderType == 3 ? "<td class='fundName'>" + utmcFundInformation2.FundName.Capitalize() + @" (" + utmcFundInformation2.FundCode + @")</td>" : "") + @"
                                    <td>" + (masterProspectus == null ? " - " : (masterProspectus.Url == null ? " - " : "<a href='" + masterProspectus.Url + @"' target='_blank'>Click here to View</a>")) + @"</td>
                                    <td>" + (phs == null ? " - " : (phs.Url == null ? " - " : "<a href='" + phs.Url + @"' target='_blank'>Click here to View</a>")) + @"</td>
                                    <td>" + (im == null ? " - " : (im.Url == null ? " - " : "<a href='" + im.Url + @"' target='_blank'>Click here to View</a>")) + @"</td>
                                    <td>" + DateTime.Now.ToString("dd-MM-yyyy hh:mm") + @"</td>
                                    " + (uo.OrderType == 1 || uo.OrderType == 6 ? "<td class='text-right'>" + utmcFundInformation1.UtmcFundInformationIdUtmcFundCharges.FirstOrDefault().InitialSalesChargesPercent + @"</td>" : "") + @"
                                    <td class='text-right'>" + (uo.OrderType == 1 || uo.OrderType == 6 ? "<span class='currencyFormatNoSymbol'>" + uo.Amount + @"</span>" : "<span class='unitFormatNoSymbol'>" + uo.Units + @"</span>") + @"
                                    </td>" +

                               @"</tr>");
                    }
                    tbodyCart.InnerHtml = sb.ToString();
                    if (userOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.Buy || userOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.RSP)
                        cartTotalAmount.InnerHtml = userOrders.Sum(x => x.Amount).ToString();
                    else
                        cartTotalAmount.InnerHtml = userOrders.Sum(x => x.Units).ToString();
                    if (userOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.Sell || userOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.SwitchOut)
                    {
                        btnPayLater.Text = "Request Later";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Show-Cart Page BindCart: " + ex.Message);
            }
        }

        public class orderfile
        {
            public int paymentType { get; set; }
            public string url { get; set; }
            public string filename { get; set; }
        }

        public void RSPFund(UserRspeModel userRspeModel)
        {
            User user = (User)Session["user"];

            string regGuid = string.Empty;
            regGuid = Guid.NewGuid().ToString();

            DateTime createdDate = DateTime.Now;
            Response responseLastOrder = IUserOrderService.GetData(0, 1, true);
            UserOrder lastUO = ((List<UserOrder>)responseLastOrder.Data).FirstOrDefault();
            int lastOrderNo = Convert.ToInt32(lastUO.OrderNo.Substring(4, lastUO.OrderNo.Length - 4));
            string orderNo = (lastOrderNo + 1).ToString().PadLeft(8, '0');
            orderNo = "RS18" + orderNo;
            string idNo = string.Empty;

            Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
            if (responseUAList.IsSuccess)
            {
                List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                UserAccount primaryAcc = new UserAccount();
                MaHolderReg maHolderReg = new MaHolderReg();
                {
                    primaryAcc = userAccounts.Where(x => x.AccountNo == "").FirstOrDefault();
                    if (primaryAcc.IsPrinciple == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Alert', 'Only PRINCIPAL account can do transaction.','" + Request.ApplicationPath + "Portfolio.aspx');", true);
                    }
                    Response responseMHR = ServicesManager.GetMaHolderRegByAccountNo(primaryAcc.AccountNo);
                    if (responseMHR.IsSuccess)
                    {
                        maHolderReg = (MaHolderReg)responseMHR.Data;
                        if (!string.IsNullOrEmpty(maHolderReg.IdNo))
                        {
                            idNo = maHolderReg.IdNo.Replace("-", "");
                        }
                    }
                }
            }

            //insert order onfo
            UserOrder userOrder = new UserOrder();
            userOrder.FundId = userRspeModel.Fund_ID;
            userOrder.RefNo = regGuid;
            userOrder.OrderType = (int)CustomStatus.OrderType.RSP;
            userOrder.ToFundId = 0;
            userOrder.ToAccountId = 0;
            userOrder.UserId = user.Id;
            userOrder.UserAccountId = user.UserIdUserAccounts.Count != 0 ? user.UserIdUserAccounts.FirstOrDefault().Id : 0;
            userOrder.OrderNo = orderNo;
            userOrder.PaymentMethod = ddlPaymentMethodsList.SelectedValue;
            userOrder.Amount = userRspeModel.Rsp_Amount;
            userOrder.Units = 0;
            userOrder.TransId = 0;
            userOrder.CreatedBy = user.Id;
            userOrder.CreatedDate = createdDate;
            userOrder.UpdatedDate = createdDate;
            userOrder.UpdatedBy = 0;
            userOrder.Status = 1;
            userOrder.ConsultantId = "";
            userOrder.TransNo = createdDate.ToString("yyyyMMddHHmmss");
            userOrder.OrderStatus = 1;
            userOrder.BankCode = "";
            Response responseOrder = IUserOrderService.PostData(userOrder);
            if (responseOrder.IsSuccess)
            {

                string month = DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();

                string startDate = DateTime.Now.Day.ToString() + month + DateTime.Now.Year.ToString().Substring(DateTime.Now.Year.ToString().Length - 2);
                string endDate = DateTime.Now.Day.ToString() + month + DateTime.Now.AddYears(1).Year.ToString().Substring(DateTime.Now.Year.ToString().Length - 2);
                Session["RSPFund"] = null;
                FPXPayment(orderNo, userOrder.TransNo, userOrder.Amount.ToString(), user.EmailId, "" + userRspeModel.IDNo + ",1", user.Username, "0", userRspeModel.UserAccount, "APEX", "01," + user.MobileNumber + ",1,MT," + startDate + "," + endDate + "", ddlBankList.SelectedItem.Value, "AD");
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            User user = (User)Session["user"];
            bool agreed = chkAgree.Checked;
            string transPwd = txtTransPassword.Text;
            string ddlPaymentMode = ddlPaymentMethodsList.SelectedValue;
            string ddlBank = ddlBankList.SelectedValue;



            {
                if ((UserOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.Buy || UserOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.RSP) && ddlPaymentMode != "" && transPwd != "" && agreed)
                {
                    orderfile uf = new orderfile();
                    if (ddlPaymentMode == "2")
                    {
                        string Date = DateTime.Now.ToString("ddMMyyyy-mmsshh");
                        string G = Guid.NewGuid().ToString();
                        if (fuPaymentMethod.HasFile)
                        {
                            string subdir = Server.MapPath("/UserOrderFiles/");
                            string dbpath = "/UserOrderFiles/";
                            if (!Directory.Exists(subdir))
                                Directory.CreateDirectory(subdir);

                            string filename = "ID" + UserOrders.FirstOrDefault().UserAccountId + "_" + Date + (Path.GetFileName(fuPaymentMethod.FileName));
                            subdir = subdir + filename;
                            dbpath = dbpath + filename;

                            //saves into local
                            fuPaymentMethod.SaveAs(subdir);

                            uf.filename = Path.GetFileName(fuPaymentMethod.FileName);
                            uf.paymentType = Convert.ToInt32(ddlPaymentMethodsList.SelectedValue);
                            uf.url = dbpath;

                            UserOrderFile uof = new UserOrderFile();
                            uof.OrderFileTypeId = uf.paymentType;
                            uof.Name = uf.filename;
                            uof.Url = uf.url;
                            uof.OrderNo = UserOrders.FirstOrDefault().OrderNo;
                            uof.UserId = user.Id;
                            uof.UserAccountId = UserOrders.FirstOrDefault().UserAccountId;
                            uof.CreatedBy = user.Id;
                            uof.CreatedDate = DateTime.Now;
                            uof.UpdatedBy = user.Id;
                            uof.UpdatedDate = DateTime.Now;
                            uof.Status = 1;
                            IUserOrderFileService.PostData(uof);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please choose file.', '');", true);
                        }
                    }
                    UserAccount primaryAcc = new UserAccount();
                    Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;

                        if (ddlUserAccountId.SelectedValue != "")
                        {
                            primaryAcc = userAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();
                        }
                    }

                    if (ddlPaymentMode == "1")
                    {
                        string pw = CustomEncryptorDecryptor.EncryptPassword(transPwd);
                        if (user.TransPwd == CustomEncryptorDecryptor.EncryptPassword(txtTransPassword.Text))
                        {
                            UserOrders.ForEach(uo =>
                            {
                                uo.PaymentMethod = ddlPaymentMode;
                                uo.OrderStatus = (int)CustomStatus.Order_Status.Payment_Pending;
                                uo.UpdatedDate = DateTime.Now;
                                uo.BankCode = (ddlPaymentMode == "1" ? ddlBank : "");
                                uo.ConfirmationDate = DateTime.Now;
                            });


                            List<UtmcFundInformation> funds = new List<UtmcFundInformation>();
                            List<UtmcFundInformation> funds2 = new List<UtmcFundInformation>();

                            foreach (UserOrder uo in UserOrders)
                            {
                                UtmcFundInformation utmcFundInformation1 = new UtmcFundInformation();
                                Response responseUFI1 = IUtmcFundInformationService.GetDataByFilter(" id=" + uo.FundId + " ", 0, 0, true);
                                if (responseUFI1.IsSuccess)
                                {
                                    List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)responseUFI1.Data;
                                    if (utmcFundInformations.Count > 0)
                                    {
                                        utmcFundInformation1 = utmcFundInformations.FirstOrDefault();
                                    }
                                }

                                funds.Add(utmcFundInformation1);
                            }



                            IUserOrderService.UpdateBulkData(UserOrders);
                            if (UserOrders.FirstOrDefault().OrderType == 6) //RSP
                            {
                                string month = DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();
                                string day = DateTime.Now.Day < 10 ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString();
                                string startDate = day + month + DateTime.Now.Year.ToString().Substring(DateTime.Now.Year.ToString().Length - 2);
                                string endDate = day + month + DateTime.Now.AddYears(1).Year.ToString().Substring(DateTime.Now.Year.ToString().Length - 2);
                                string mobileno = user.MobileNumber.Replace(" ", "").Replace("-", "");
                                HttpCookie userInfo = new HttpCookie("userInfo");
                                userInfo["user_id"] = user.Id.ToString();
                                userInfo["LastLoginIP"] = Session["LastLoginIP"].ToString();
                                userInfo["LastLoginTime"] = Session["LastLoginTime"].ToString();
                                userInfo["CurrentLoginTime"] = Session["CurrentLoginTime"].ToString();
                                userInfo["isVerified"] = Session["isVerified"].ToString();
                                userInfo.SameSite = SameSiteMode.None;
                                userInfo.Expires.Add(new TimeSpan(0, 20, 0));
                                Response.Cookies.Add(userInfo);
                                FPXPayment(orderNo, UserOrders.FirstOrDefault().TransNo, (Math.Round(UserOrders.Sum(x => x.Amount), 2).ToString()), user.EmailId, "" + lblUserIC.Value + ",1", user.Username, "0", user.Username, "APEX", "01," + mobileno + ",1,MT," + startDate + "," + endDate + "", ddlBank, "AD");
                            }
                            else
                            {
                                HttpCookie userInfo = new HttpCookie("userInfo");
                                userInfo["user_id"] = user.Id.ToString();
                                userInfo["LastLoginIP"] = Session["LastLoginIP"].ToString();
                                userInfo["LastLoginTime"] = Session["LastLoginTime"].ToString();
                                userInfo["CurrentLoginTime"] = Session["CurrentLoginTime"].ToString();
                                userInfo["isVerified"] = Session["isVerified"].ToString();
                                userInfo.SameSite = SameSiteMode.None;
                                userInfo.Expires.Add(new TimeSpan(0, 20, 0));
                                Response.Cookies.Add(userInfo);
                                FPXPayment(orderNo, UserOrders.FirstOrDefault().TransNo, Math.Round(UserOrders.Sum(x => x.Amount), 2).ToString(), user.EmailId, user.EmailId, user.Username, "0", "0", "APEX", "0", ddlBank, "AR");
                            }

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Wrong password.', '');", true);
                        }

                    }
                    else if (ddlPaymentMode == "2")
                    {
                        var allowedExtensions = new string[] { "jpg", "jpeg", "pdf", "png" };
                        var extension = Path.GetExtension(fuPaymentMethod.PostedFile.FileName).ToLower().Replace(".", "");

                        if (allowedExtensions.Contains(extension))
                        {
                            string pw = CustomEncryptorDecryptor.EncryptPassword(transPwd);
                            if (user.TransPwd == CustomEncryptorDecryptor.EncryptPassword(txtTransPassword.Text))
                            {
                                UserOrders.ForEach(uo =>
                                {
                                    uo.PaymentMethod = ddlPaymentMode;
                                    uo.OrderStatus = (int)CustomStatus.Order_Status.Payment_Pending;
                                    uo.UpdatedDate = DateTime.Now;
                                    uo.BankCode = (ddlPaymentMode == "1" ? ddlBank : "");
                                    uo.ConfirmationDate = DateTime.Now;
                                });

                                List<UtmcFundInformation> funds = new List<UtmcFundInformation>();
                                List<UtmcFundInformation> funds2 = new List<UtmcFundInformation>();

                                foreach (UserOrder uo in UserOrders)
                                {
                                    UtmcFundInformation utmcFundInformation1 = new UtmcFundInformation();
                                    Response responseUFI1 = IUtmcFundInformationService.GetDataByFilter(" id=" + uo.FundId + " ", 0, 0, true);
                                    if (responseUFI1.IsSuccess)
                                    {
                                        List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)responseUFI1.Data;
                                        if (utmcFundInformations.Count > 0)
                                        {
                                            utmcFundInformation1 = utmcFundInformations.FirstOrDefault();
                                        }
                                    }

                                    funds.Add(utmcFundInformation1);
                                }
                                IUserOrderService.UpdateBulkData(UserOrders);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Info', 'Your order placed and is pending for approval.', 'OrderList.aspx?type=" + UserOrders.FirstOrDefault().OrderType + "&accNo=" + accountId + "');", true);

                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Wrong password.', '');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please select no other than jpg/jpeg/png/pdf file to upload.', '');", true);
                        }
                    }
                }
                else if ((UserOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.Sell || UserOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.SwitchOut) && transPwd != "" && agreed)
                {
                    UserAccount primaryAcc = new UserAccount();
                    Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                    if (responseUAList.IsSuccess)
                    {
                        List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;

                        if (ddlUserAccountId.SelectedValue != "")
                        {
                            primaryAcc = userAccounts.Where(x => x.AccountNo == ddlUserAccountId.SelectedValue).FirstOrDefault();
                        }
                    }
                    string pw = CustomEncryptorDecryptor.EncryptPassword(transPwd);
                    if (user.TransPwd == CustomEncryptorDecryptor.EncryptPassword(txtTransPassword.Text))
                    {
                        UserOrders.ForEach(uo =>
                        {
                            uo.PaymentMethod = ddlPaymentMode;
                            uo.OrderStatus = (int)CustomStatus.Order_Status.Order_Verified;
                            uo.UpdatedDate = DateTime.Now;

                            uo.ConfirmationDate = DateTime.Now;
                        });
                        List<UtmcFundInformation> funds = new List<UtmcFundInformation>();
                        List<UtmcFundInformation> funds2 = new List<UtmcFundInformation>();

                        foreach (UserOrder uo in UserOrders)
                        {
                            UtmcFundInformation utmcFundInformation1 = new UtmcFundInformation();
                            Response responseUFI1 = IUtmcFundInformationService.GetDataByFilter(" id=" + uo.FundId + " ", 0, 0, true);
                            if (responseUFI1.IsSuccess)
                            {
                                List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)responseUFI1.Data;
                                if (utmcFundInformations.Count > 0)
                                {
                                    utmcFundInformation1 = utmcFundInformations.FirstOrDefault();
                                }
                            }
                            UtmcFundInformation utmcFundInformation2 = new UtmcFundInformation();
                            Response responseUFI2 = IUtmcFundInformationService.GetDataByFilter(" id=" + uo.ToFundId + " ", 0, 0, true);
                            if (responseUFI2.IsSuccess)
                            {
                                List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)responseUFI2.Data;
                                if (utmcFundInformations.Count > 0)
                                {
                                    utmcFundInformation2 = utmcFundInformations.FirstOrDefault();
                                }
                            }

                            funds.Add(utmcFundInformation1);
                            funds2.Add(utmcFundInformation2);
                        }
                        IUserOrderService.UpdateBulkData(UserOrders);
                        Email email = new Email();
                        email.user = user;
                        EmailService.SendOrderEmail(UserOrders, email, funds, funds2, primaryAcc.AccountNo);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Info', 'Your order placed and is pending for approval.', 'OrderList.aspx?type=" + UserOrders.FirstOrDefault().OrderType + "&accNo=" + accountId + "');", true);
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Invalid password.', '');", true);

                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please fill the required fields.', '');", true);
                }
            }


        }

        public void FPXPayment(
            string orderNo, string transDate, string txn_amt,
            string buyer_mail_id, string buyer_id, string buyerName, string buyerBankBranch,
            string buyerAccNo, string makerName, string buyerIBAN,
            string buyerBankId, string msgType)
        {
            Controller c = new Controller();
            String checksum = "";
            Random RandomNumber = new Random();
            String fpx_msgType = msgType;
            String fpx_msgToken = "01";
            String fpx_sellerExId = "EX00008813";  //Production is EX00011078
             String fpx_sellerExOrderNo = orderNo;
            String fpx_sellerOrderNo = orderNo;
            String fpx_sellerTxnTime = transDate;
            String fpx_sellerId = "SE00043740";
            String fpx_sellerBankCode = "01";
            String fpx_txnCurrency = "MYR";
            String fpx_txnAmount = txn_amt;
            String fpx_buyerEmail = buyer_mail_id;
            String fpx_buyerId = buyer_id;
            String fpx_buyerName = buyerName;
            String fpx_buyerBankId = buyerBankId;
            String fpx_buyerBankBranch = buyerBankBranch;
            String fpx_buyerAccNo = buyerAccNo;
            String fpx_makerName = makerName;
            String fpx_buyerIban = buyerIBAN;
            String fpx_productDesc = "PD";
            String fpx_version = "7.0";
            String fpx_checkSum = "";
            fpx_checkSum = fpx_buyerAccNo + "|" + fpx_buyerBankBranch + "|" + fpx_buyerBankId + "|" + fpx_buyerEmail + "|" + fpx_buyerIban + "|" + fpx_buyerId + "|" + fpx_buyerName + "|";
            fpx_checkSum += fpx_makerName + "|" + fpx_msgToken + "|" + fpx_msgType + "|" + fpx_productDesc + "|" + fpx_sellerBankCode + "|" + fpx_sellerExId + "|";
            fpx_checkSum += fpx_sellerExOrderNo + "|" + fpx_sellerId + "|" + fpx_sellerOrderNo + "|" + fpx_sellerTxnTime + "|" + fpx_txnAmount + "|" + fpx_txnCurrency + "|" + fpx_version;
            fpx_checkSum = fpx_checkSum.Trim();

            checksum = c.RSASign(fpx_checkSum, Request.PhysicalApplicationPath + "EX00008813.key"); //Exchange Key name //Production is EX00011078
            String finalMsg = fpx_checkSum;
            string FPXPostPaymentUrl = ConfigurationManager.AppSettings["FPXPostPaymentUrl"].ToString();
            string exOrderNoStr = "<input type='hidden' value=\"" + fpx_sellerExOrderNo + "\" name='fpx_sellerExOrderNo'>";
            string sellerOrderNoStr = "<input type='hidden' value=\"" + fpx_sellerOrderNo + "\" name='fpx_sellerOrderNo'>";

            var formPostText = @"<html><body><div>
                    <form method='POST' action='" + FPXPostPaymentUrl + @"' name='FPXPaymentForm'>
                      <input type='hidden' name='txn_amt' value='" + fpx_txnAmount + @"' /> 
                      <input type='hidden' value='" + fpx_msgType + @"' name='fpx_msgType'>

                      <input type='hidden' value='" + fpx_msgToken + @"' name='fpx_msgToken'>
                      <input type='hidden' value='" + fpx_sellerExId + @"' name='fpx_sellerExId'>
                       " + exOrderNoStr + @"
                      <input type='hidden' value='" + fpx_sellerTxnTime + @"' name='fpx_sellerTxnTime'>
                       " + sellerOrderNoStr + @"
                      <input type='hidden' value='" + fpx_sellerBankCode + @"' name='fpx_sellerBankCode'>
                      <input type='hidden' value='" + fpx_txnCurrency + @"' name='fpx_txnCurrency'>
                      <input type='hidden' value='" + fpx_txnAmount + @"' name='fpx_txnAmount'>
                      <input type='hidden' value='" + fpx_buyerEmail + @"' name='fpx_buyerEmail'>
                      <input type='hidden' value='" + checksum + @"' name='fpx_checkSum'>
                      <input type='hidden' value='" + fpx_buyerName + @"' name='fpx_buyerName'>
                      <input type='hidden' value='" + fpx_buyerBankId + @"' name='fpx_buyerBankId'>
                      <input type='hidden' value='" + fpx_buyerBankBranch + @"' name='fpx_buyerBankBranch'>
                      <input type='hidden' value='" + fpx_buyerAccNo + @"' name='fpx_buyerAccNo'>
                      <input type='hidden' value='" + fpx_buyerId + @"' name='fpx_buyerId'>
                      <input type='hidden' value='" + fpx_makerName + @"' name='fpx_makerName'>
                      <input type='hidden' value='" + fpx_buyerIban + @"' name='fpx_buyerIban'>
                      <input type='hidden' value='" + fpx_productDesc + @"' name='fpx_productDesc'>
                      <input type='hidden' value='" + fpx_version + @"' name='fpx_version'>
                      <input type='hidden' value='" + fpx_sellerId + @"' name='fpx_sellerId'>
                      <input type='hidden' value='" + checksum + @"' name='checkSum_String'>
                      <input type='hidden' value='" + Request.PhysicalApplicationPath + @"'>
                    </form></div><script type='text/javascript'>document.FPXPaymentForm.submit();</script></body></html>
                    ";
            //Logger.WriteLog("Sender Form Details:" + formPostText);
            Response.Write(formPostText);
        }

        protected void btnPayCanCel_Click(object sender, EventArgs e)
        {
            User user = (User)Session["user"];
            Response responseUOList = IUserOrderService.GetDataByFilter(" order_no = '" + orderNo + "' and user_id=" + user.Id + " ", 0, 0, false);
            if (responseUOList.IsSuccess)
            {
                List<UserOrder> userOrders = (List<UserOrder>)responseUOList.Data;
                List<UserOrder> multipleOrders = new List<UserOrder>();

                userOrders.ForEach(x =>
                {
                    Response response = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), x.OrderNo, true, 0, 0, false);
                    if (response.IsSuccess)
                    {
                        List<UserOrder> ol = (List<UserOrder>)response.Data;
                        if (ol.Count > 0)
                        {
                            multipleOrders.AddRange(ol);
                        }
                    }
                });

                multipleOrders.ForEach(x =>
                {
                    x.OrderStatus = (int)CustomStatus.Order_Status.Order_Canceld;
                });
                IUserOrderService.UpdateBulkData(multipleOrders);
                if (userOrders.Count != 0)
                {
                    string userAccountId = user.UserIdUserAccounts.FirstOrDefault(a => a.Id == userOrders.FirstOrDefault().UserAccountId).AccountNo;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", ((userOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.RSP) ? "ShowCustomMessage('Info', 'Order no: " + orderNo + " cancelled.', 'RSPList.aspx?type=" + multipleOrders.FirstOrDefault().OrderType : "ShowCustomMessage('Info', 'Order no: " + orderNo + " cancelled.', 'OrderList.aspx?type=" + multipleOrders.FirstOrDefault().OrderType) + "&accNo=" + userAccountId + "');", true);
                }
            }
        }

        protected void btnPayLater_Click(object sender, EventArgs e)
        {
            User user = (User)Session["user"];
            Response responseUOList = IUserOrderService.GetDataByFilter(" order_no = '" + orderNo + "' and user_id=" + user.Id + " ", 0, 0, false);
            if (responseUOList.IsSuccess)
            {
                List<UserOrder> userOrders = (List<UserOrder>)responseUOList.Data;
                if (userOrders.Count != 0)
                {
                    string userAccountId = user.UserIdUserAccounts.FirstOrDefault(a => a.Id == userOrders.FirstOrDefault().UserAccountId).AccountNo;
                    if (userOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.Sell)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Info', 'Order no: " + orderNo + " request later.', 'OrderList.aspx?type=" + userOrders.FirstOrDefault().OrderType + "&accNo=" + userAccountId + "');", true);
                    else if (userOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.SwitchIn || userOrders.FirstOrDefault().OrderType == (int)CustomStatus.OrderType.SwitchOut)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Info', 'Order no: " + orderNo + " switch later.', 'OrderList.aspx?type=" + userOrders.FirstOrDefault().OrderType + "&accNo=" + userAccountId + "');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Info', 'Order no: " + orderNo + " pay later.', 'OrderList.aspx?type=" + userOrders.FirstOrDefault().OrderType + "&accNo=" + userAccountId + "');", true);
                }
            }
        }


        protected void checkTypeofPayment(object sender, EventArgs e)
        {
            if (ddlPaymentMethodsList.SelectedIndex == 0)
            {
                guideProof.Visible = false;
            }
            else if (ddlPaymentMethodsList.SelectedIndex == 1)
            {
                guideProof.Visible = false;
            }
            else if (ddlPaymentMethodsList.SelectedIndex == 2)
            {
                guideProof.Visible = true;
            }
        }


        public static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType("DiOTP.Utility.CustomClasses." + typeName);
                if (type != null)
                    return type;
            }
            return null;
        }

    }
}