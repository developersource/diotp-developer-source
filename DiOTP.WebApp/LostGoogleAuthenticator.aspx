﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="LostGoogleAuthenticator.aspx.cs" Inherits="DiOTP.WebApp.LostGoogleAuthenticator" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="section lostauth">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="mb-5">Reset Google Authenticator</h3>
                    <p class="mb-12">If you have lost your google authenticator, just follow the below steps for resetting app. This will be different for Android and IPhone users.</p>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="req-label">
                                <label>NOTE: </label>
                                If you have lost your email that has key, request here. The requested key will be sent to your email. 
                                    <a href="/ResetConfirmation.aspx" role="button" class="btn reqkey">Request Key</a>
                            </p>
                        </div>

                        <div class="col-md-6">
                            <div class="lostgoogle">
                                <h4>For Android Users:</h4>
                                <p>
                                    <label>Step 1:</label>If you have uninstalled Google Authenticator app, reinstall it for resetting.
                                </p>
                                <p>
                                    <label>Step 2:</label>Open your Google Authenticator app, click <a class="add-icon">+</a> icon at the bottom of your page, for adding your eApexIs account.
                                </p>
                                <img src="Content/MyImage/14.png" alt="" height="300" class="mt-8 mb-8" />
                                <p>
                                    <label>Step 3:</label>Add <a class="add-icon">+</a> shows two options. Scan a barcode & Enter a provided key. Click on <b>Enter a provided key</b>.
                                </p>
                                <img src="Content/MyImage/15.png" alt="" height="400" class="mt-8 mb-8" />
                                <p>
                                    <label>Step 4:</label>You will be asked Account details. Enter Account name and your key (that we have provided after scanning barcode. Check your email for Reset Key.)
                                    <br />
                                    <b>Account name : eApexIs</b><br />
                                    <b>Your key: _ _ _ _ _ _ _ _ _ _ _ _ _ _</b>
                                </p>
                                <img src="Content/MyImage/13.png" alt="" height="350" class="mt-8 mb-8" />
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="lostgoogle">
                                <h4>For IPhone Users:</h4>
                                <p>
                                    <label>Step 1:</label>If you have uninstalled Google Authenticator app, reinstall it for resetting.
                                </p>
                                <p>
                                    <label>Step 2:</label>Open your Google Authenticator app, click on (<span class="icon-i">+</span>) symbol on top right corner. If you are new to app, then click Begin setup.
                                </p>
                                <img src="Content/MyImage/1.jpg" alt="" height="200" class="mt-8 mb-8" />
                                <p>
                                    <label>Step 3:</label>After step 2, you will have two options showing Scan barcode & Manual entry. Click on <b>Manual entry.</b>
                                </p>
                                <img src="Content/MyImage/3.jpg" alt="" height="400" class="mt-8 mb-8" />
                                <p>
                                    <label>Step 4:</label>You will be asked Account details. Enter Account name and your key (that we have provided after scanning barcode. Check your email for Reset Key.)
                                    <br />
                                    <b>Account : eApexIs</b><br />
                                    <b>Your key: _ _ _ _ _ _ _ _ _ _ _ _ _ _</b>
                                </p>
                                <img src="Content/MyImage/2.jpg" alt="" height="300" class="mt-8 mb-8" />
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </section>


</asp:Content>



<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
