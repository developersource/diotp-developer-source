﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="TransactionHistory.aspx.cs" Inherits="DiOTP.WebApp.TransactionHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <link href="/Content/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="/Content/css/fixedHeader.bootstrap.min.css" rel="stylesheet" />
    <link href="/Content/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <link href="Content/js/bootstrap-datepicker.css" rel="stylesheet" />
    <style>
        #accountSelectedDropdown {
            display: none;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2 pr-0 pl-0">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/Portfolio.aspx">eApexIs</a></li>
                    <li class="active">Transaction History</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Transaction History</h3>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="statement">
                        <h5 class="hidden-lg hidden-md">Generate below for selected account:</h5>
                        <div class="row">
                            <div class="col-md-3 mb-8">
                                <label>Account Number:</label>
                                <asp:DropDownList ID="ddlFundAccount" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <div class="col-md-3 mb-8">
                                <label>Transaction Type:</label>
                                <asp:DropDownList ID="ddlTransactionType" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Purchase"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Redemption"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Switching"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-2 mb-8">
                                <label>From</label>
                                <asp:TextBox ID="txtDateStart" runat="server" CssClass="form-control" ClientIDMode="Static" autocomplete="off"></asp:TextBox>
                            </div>
                            <div class="col-md-2 mb-8">
                                <label>To</label>
                                <asp:TextBox ID="txtDateEnd" runat="server" CssClass="form-control" ClientIDMode="Static" autocomplete="off"></asp:TextBox>
                            </div>
                            <div class="col-md-2 mb-8">
                                <asp:Button ID="btnGenerate" runat="server" Text="Submit" CssClass="btn btn-block btn-infos1 mt-30" OnClientClick="return clientStatementValidation()" OnClick="btnGenerate_Click" />
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-font-size-13 table-cell-pad-5 table-striped table-bordered" id="TransactionHistory">
                            <thead>
                                <tr>
                                    <th style="text-align: left">Transaction<br />
                                        Date</th>
                                    <th style="text-align: left">MasterAccount<br />
                                        No</th>
                                    <th style="text-align: left">Transaction<br />
                                        Type</th>
                                    <th style="text-align: left">Fund<br />
                                        Name</th>
                                    <th style="text-align: left">Transaction<br />
                                        Number</th>
                                    <th style="text-align: right">No. of<br />
                                        Units</th>
                                    <th style="text-align: right">NAV Price<br />
                                        MYR</th>
                                    <th style="text-align: right">Transaction Amount<br />
                                        MYR</th>
                                    <th style="text-align: left">Payment<br />
                                        Method</th>
                                </tr>
                            </thead>
                            <tbody id="tbodyTransactionHistory" runat="server">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script src="/Content/js/jquery.dataTables.min.js"></script>
    <script src="/Content/js/dataTables.bootstrap.min.js"></script>
    <script src="/Content/js/dataTables.responsive.min.js"></script>
    <script src="/Content/js/responsive.bootstrap.min.js"></script>
    <script src="Content/js/bootstrap-datepicker.min.js"></script>
    <script>
        $(document).ready(function () {


            $("#txtDateStart").datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                startView: 2
            }).on('changeDate', function (selected) {
                var minDate = new Date(selected.date.valueOf());
                $('#txtDateEnd').datepicker('setStartDate', minDate);
            });

            $("#txtDateEnd").datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                startView: 2
            })
                .on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf());
                    $('#startdate').datepicker('setEndDate', minDate);
                });

            $("#TransactionHistory").DataTable({
                //responsive: true,
                ordering: false,
                autoWidth: true,
                "oLanguage": {
                    "sEmptyTable": "No transaction record found"
                }
            });

        });

        function clientStatementValidation() {
            if ($('#txtDateStart').val() == "") {
                ShowCustomMessage('Alert', 'Please select a Date(From)', '');
                return false;
            }
            else if ($('#txtDateEnd').val() == "") {
                ShowCustomMessage('Alert', 'Please select a Date(To)', '');
                return false;
            }
            //else if ($('#txtDateStart').val() > $('#txtDateEnd').val()) {
            //    ShowCustomMessage('Alert', 'Date(From) cannot be greater than Date(To)', '');
            //    return false;
            //}
            else {
                setTimeout(function () {
                    $('.loader-bg').fadeIn();
                }, 500);
            }
        }
    </script>

</asp:Content>
