﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.FPXLibary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class FPXResponse : System.Web.UI.Page
    {
        private static readonly Lazy<IUserService> lazyUserObj = new Lazy<IUserService>(() => new UserService());
        public static IUserService IUserService { get { return lazyUserObj.Value; } }

        private static readonly Lazy<IUserOrderService> lazyUserOrderService = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderService.Value; } }

        private static readonly Lazy<IPaymentDetailService> lazyIPaymentDetailServiceObj = new Lazy<IPaymentDetailService>(() => new PaymentDetailService());
        public static IPaymentDetailService IPaymentDetailService { get { return lazyIPaymentDetailServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());
        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObj.Value; } }

        public static List<Cart> cartItems = new List<Cart>();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                {
                    {
                        //Retrieve Data From FPX Form Response
                        Controller c = new Controller();
                        String fpx_buyerBankBranch = Request.Form["fpx_buyerBankBranch"];
                        String fpx_buyerBankId = Request.Form["fpx_buyerBankId"];
                        String fpx_buyerIban = Request.Form["fpx_buyerIban"];
                        String fpx_buyerId = Request.Form["fpx_buyerId"];
                        String fpx_buyerName = Request.Form["fpx_buyerName"];
                        String fpx_creditAuthCode = Request.Form["fpx_creditAuthCode"];
                        String fpx_creditAuthNo = Request.Form["fpx_creditAuthNo"];
                        String fpx_debitAuthCode = Request.Form["fpx_debitAuthCode"];
                        String fpx_debitAuthNo = Request.Form["fpx_debitAuthNo"];
                        String fpx_fpxTxnId = Request.Form["fpx_fpxTxnId"];
                        String fpx_fpxTxnTime = Request.Form["fpx_fpxTxnTime"];
                        String fpx_makerName = Request.Form["fpx_makerName"];
                        String fpx_msgToken = Request.Form["fpx_msgToken"];
                        String fpx_msgType = Request.Form["fpx_msgType"];
                        String fpx_sellerExId = Request.Form["fpx_sellerExId"];
                        String fpx_sellerExOrderNo = Request.Form["fpx_sellerExOrderNo"];
                        String fpx_sellerId = Request.Form["fpx_sellerId"];
                        String fpx_sellerOrderNo = Request.Form["fpx_sellerOrderNo"];
                        String fpx_sellerTxnTime = Request.Form["fpx_sellerTxnTime"];
                        String fpx_txnAmount = Request.Form["fpx_txnAmount"];
                        String fpx_txnCurrency = Request.Form["fpx_txnCurrency"];
                        String fpx_checkSum = Request.Form["fpx_checkSum"];
                        String fpx_productDesc = Request.Form["fpx_txnCurrency"];
                        String fpx_checkSumString = "";

                        fpx_checkSumString = fpx_buyerBankBranch + "|" + fpx_buyerBankId + "|" + fpx_buyerIban + "|" + fpx_buyerId + "|" + fpx_buyerName + "|" + fpx_creditAuthCode + "|" + fpx_creditAuthNo + "|" + fpx_debitAuthCode + "|" + fpx_debitAuthNo + "|" + fpx_fpxTxnId + "|" + fpx_fpxTxnTime + "|" + fpx_makerName + "|" + fpx_msgToken + "|" + fpx_msgType + "|";
                        fpx_checkSumString += fpx_sellerExId + "|" + fpx_sellerExOrderNo + "|" + fpx_sellerId + "|" + fpx_sellerOrderNo + "|" + fpx_sellerTxnTime + "|" + fpx_txnAmount + "|" + fpx_txnCurrency;


                        String finalVerifiMsg = "ERROR";
                        String xmlMessage = "";
                        xmlMessage = fpx_checkSumString;
                        finalVerifiMsg = c.nvl_VerifiMsg(xmlMessage, fpx_checkSum, Request.PhysicalApplicationPath); //Certificate Path

                        //Get Payment Details from FPX form response
                        PaymentDetail paymentDetail = new PaymentDetail();
                        paymentDetail.BuyerAccNo = fpx_buyerName;
                        paymentDetail.BuyerBankBranch = fpx_buyerBankBranch;
                        paymentDetail.BuyerBankId = fpx_buyerBankId;
                        paymentDetail.BuyerEmail = fpx_buyerId;
                        paymentDetail.BuyerIban = fpx_buyerIban;
                        paymentDetail.BuyerId = fpx_buyerId;
                        paymentDetail.BuyerName = fpx_buyerName;
                        paymentDetail.CreatedOn = DateTime.Now;
                        paymentDetail.MakerName = fpx_makerName;
                        paymentDetail.MsgToken = fpx_msgToken;
                        paymentDetail.MsgType = fpx_msgType;
                        paymentDetail.SellerBankCode = fpx_sellerExId;
                        paymentDetail.SellerExId = fpx_sellerExId;
                        paymentDetail.SellerExOrderNo = fpx_sellerExOrderNo;
                        paymentDetail.SellerId = fpx_sellerId;
                        paymentDetail.SellerOrderNo = fpx_sellerOrderNo;
                        paymentDetail.SellerTxnTime = fpx_sellerTxnTime;
                        paymentDetail.FpxTxnTime = fpx_fpxTxnTime;
                        paymentDetail.TransactionId = fpx_fpxTxnId;
                        paymentDetail.TxnAmount = fpx_txnAmount;
                        paymentDetail.TxnCurrency = fpx_txnCurrency;
                        paymentDetail.ProductDesc = fpx_productDesc;

                        string fpx_response_code = fpx_debitAuthCode;
                        string query = "SELECT * FROM fpx_response_codes where code = '" + fpx_response_code + "' ";
                        FpxResponseCode fpxResponseCode = new FpxResponseCode();
                        Response responseCodesList = GenericService.GetDataByQuery(query, 0, 0, false, null, false, null, false);

                        var uoDyn = responseCodesList.Data;
                        var responseJSON = JsonConvert.SerializeObject(uoDyn);
                        List<FpxResponseCode> responseCodes = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FpxResponseCode>>(responseJSON);
                        string fpxResponseDesc = "";
                        if (responseCodes.Count > 0)
                        {
                            fpxResponseCode = responseCodes.FirstOrDefault();
                            fpxResponseCodeDesc.InnerHtml = " (" + fpxResponseCode.Name + " <sub> - by FPX</sub>)";
                            transDesc.InnerHtml = fpxResponseCode.Name;
                            fpxResponseDesc = fpxResponseCode.Name;
                        }
                        int OrderStatus = 1;
                        if (fpx_debitAuthCode.CompareTo("00") == 0) //code 00 = approved
                        {
                            OrderStatus = 2; //Order Verified
                            txtStatus.InnerText = "Thank you for investing!";
                        }
                        else if (fpx_debitAuthCode.CompareTo("09") == 0) //code 09 = Transaction Pending
                        {
                            OrderStatus = 22; //Order Payment Pending
                            txtStatus.InnerText = "Thank you for investing! Your payment is pending.";
                        }
                        else if (fpx_debitAuthCode.CompareTo("99") == 0) //code 99 = Pending Authorization
                        {
                            OrderStatus = 22; //Order Payment Pending
                            txtStatus.InnerText = "Thank you for investing! Authorization is pending.";
                        }
                        else
                        {
                            OrderStatus = 29; //Order Payment_Rejected
                            txtStatus.InnerText = "Sorry your payment has failed!";
                        }

                        //Check if record exist or not.
                        Response responseUOList = IUserOrderService.GetDataByFilter(" trans_no='" + fpx_sellerTxnTime + "' and order_no='" + (fpx_sellerOrderNo.Replace("'", "''")) + "' ", 0, 0, true);
                        List<UserOrder> userOrders = new List<UserOrder>();
                        if (responseUOList.IsSuccess)
                        {
                            userOrders = (List<UserOrder>)responseUOList.Data;

                            Response response = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), fpx_sellerOrderNo.Replace("'", "''"), true, 0, 0, false);
                            if (response.IsSuccess)
                            {
                                List<UserOrder> ol = (List<UserOrder>)response.Data;
                                if (ol.Count > 0)
                                {
                                    userOrders = ol;
                                }
                            }

                            //If userorder exist
                            if (userOrders.Count > 0)
                            {

                                string fundName = "";
                                string fundName2 = "";
                                List<UtmcFundInformation> funds = new List<UtmcFundInformation>();
                                List<UtmcFundInformation> funds2 = new List<UtmcFundInformation>();

                                foreach (UserOrder uo in userOrders)
                                {
                                    UtmcFundInformation utmcFundInformation1 = new UtmcFundInformation();
                                    Response responseUFI1 = IUtmcFundInformationService.GetDataByFilter(" id=" + uo.FundId + " ", 0, 0, true);
                                    if (responseUFI1.IsSuccess)
                                    {
                                        List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)responseUFI1.Data;
                                        if (utmcFundInformations.Count > 0)
                                        {
                                            utmcFundInformation1 = utmcFundInformations.FirstOrDefault();
                                        }
                                    }
                                    lblOrderType.Value = uo.OrderType.ToString();
                                    UtmcFundInformation utmcFundInformation2 = new UtmcFundInformation();
                                    if (uo.ToFundId != 0)
                                    {
                                        Response responseUFI2 = IUtmcFundInformationService.GetDataByFilter(" id=" + uo.ToFundId + " ", 0, 0, true);
                                        if (responseUFI2.IsSuccess)
                                        {
                                            List<UtmcFundInformation> utmcFundInformations = (List<UtmcFundInformation>)responseUFI2.Data;
                                            if (utmcFundInformations.Count > 0)
                                            {
                                                utmcFundInformation2 = utmcFundInformations.FirstOrDefault();
                                            }
                                        }
                                    }

                                    UserAccount userAccount2 = new UserAccount();
                                    if (uo.ToAccountId != 0)
                                    {
                                        Response responseUFI2 = IUserAccountService.GetDataByFilter(" id=" + uo.ToAccountId + " and status='1' ", 0, 0, true);
                                        if (responseUFI2.IsSuccess)
                                        {
                                            List<UserAccount> userAccounts = (List<UserAccount>)responseUFI2.Data;
                                            if (userAccounts.Count > 0)
                                            {
                                                userAccount2 = userAccounts.FirstOrDefault();
                                            }
                                        }
                                    }

                                    fundName = utmcFundInformation1.FundName.Capitalize();
                                    if (uo.ToFundId != 0)
                                        fundName2 = utmcFundInformation2.FundName.Capitalize();
                                    funds.Add(utmcFundInformation1);
                                    funds2.Add(utmcFundInformation2);
                                }
                                Logger.WriteLog("OrderNo: " + userOrders.FirstOrDefault().OrderNo);

                                Response responseUAList = IUserAccountService.GetDataByFilter(" id=" + userOrders.FirstOrDefault().UserAccountId + " and user_id=" + userOrders.FirstOrDefault().UserId + " and status=1 ", 0, 0, true);
                                if (responseUAList.IsSuccess)
                                {
                                    List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                                    UserAccount primaryAccount = new UserAccount();
                                    if (userAccounts.Count > 0)
                                    {
                                        primaryAccount = userAccounts.FirstOrDefault();
                                        lblaccNo.Value = primaryAccount.AccountNo;

                                        Logger.WriteLog("userOrders.Count: " + userOrders.Count);

                                        userOrders.ForEach(x =>
                                        {
                                            Logger.WriteLog("userOrders: " + x.FundId + " - " + x.Amount + ", " + x.Units);
                                            x.OrderStatus = OrderStatus;
                                            x.FpxTransactionId = fpx_fpxTxnId;
                                            x.FpxStatus = fpx_debitAuthCode;
                                            x.BankCode = fpx_buyerBankId;
                                            x.PaymentDate = DateTime.Now;
                                        });
                                        Type t = Type.GetType("FPXBank");
                                        if (t == null)
                                        {
                                            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
                                            {
                                                t = a.GetType("DiOTP.Utility.CustomClasses." + "FPXBank");
                                                if (t != null)
                                                    break;
                                            }
                                        }
                                        FPXBank b = null;
                                        if (t != null)
                                            b = FPXEnumeration.GetByBankId(t, fpx_buyerBankId);
                                        if (userOrders.FirstOrDefault().OrderType == 6) //FOR RSP
                                        {
                                            accNo.InnerHtml = primaryAccount.AccountNo;
                                            bankName.InnerHtml = "Maybank";
                                            bankName2.InnerHtml = (b == null ? fpx_buyerBankId : b.DisplayName);
                                            orderNo.InnerHtml = userOrders.FirstOrDefault().OrderNo;
                                            amount.InnerHtml = userOrders.Sum(x => x.Amount).ToString("N2");
                                            transDate.InnerHtml = DateTime.ParseExact(fpx_fpxTxnTime, "yyyyMMddHHmmss", null).ToString("dd/MM/yyyy HH:mm");

                                            transID.InnerHtml = fpx_fpxTxnId;
                                            charges.InnerHtml = fpx_txnAmount;

                                        }
                                        else
                                        {
                                        }


                                        //update user order records
                                        int updatedRecs = IUserOrderService.UpdateBulkData(userOrders);
                                        Logger.WriteLog("updatedRecs: " + updatedRecs);

                                        //send success mail
                                        if (OrderStatus == 2)
                                        {
                                            Email email = new Email();
                                            User user = (User)Session["user"];
                                            if (user == null)
                                            {
                                                Response responseUser = IUserService.GetSingle(userOrders.FirstOrDefault().UserId);
                                                if (responseUser.IsSuccess)
                                                {
                                                    user = (User)responseUser.Data;
                                                    HttpCookie reqCookies = Request.Cookies["userInfo"];
                                                    if (reqCookies != null)
                                                    {
                                                        string user_id = reqCookies["user_id"].ToString();
                                                        string LastLoginIP = reqCookies["LastLoginIP"].ToString();
                                                        string LastLoginTime = reqCookies["LastLoginTime"].ToString();
                                                        string CurrentLoginTime = reqCookies["CurrentLoginTime"].ToString();

                                                        Session["user"] = user;
                                                        Session["LastLoginIP"] = LastLoginIP;
                                                        Session["LastLoginTime"] = LastLoginTime;
                                                        Session["CurrentLoginTime"] = CurrentLoginTime;
                                                    }
                                                }
                                            }
                                            email.user = user;
                                            EmailService.SendOrderEmail(userOrders, email, funds, funds2, primaryAccount.AccountNo, null, false, "", paymentDetail, fpxResponseCode.Name, (b == null ? fpx_buyerBankId : b.DisplayName));
                                        }
                                        else if (OrderStatus == 29) //send fail mail
                                        {
                                            Email email = new Email();
                                            User user = (User)Session["user"];
                                            if (user == null)
                                            {
                                                Response responseUser = IUserService.GetSingle(userOrders.FirstOrDefault().UserId);
                                                if (responseUser.IsSuccess)
                                                {
                                                    user = (User)responseUser.Data;
                                                    HttpCookie reqCookies = Request.Cookies["userInfo"];
                                                    if (reqCookies != null)
                                                    {
                                                        string user_id = reqCookies["user_id"].ToString();
                                                        string LastLoginIP = reqCookies["LastLoginIP"].ToString();
                                                        string LastLoginTime = reqCookies["LastLoginTime"].ToString();
                                                        string CurrentLoginTime = reqCookies["CurrentLoginTime"].ToString();
                                                        string isVerified = reqCookies["isVerified"].ToString();

                                                        Session["user"] = user;
                                                        Session["LastLoginIP"] = LastLoginIP;
                                                        Session["LastLoginTime"] = LastLoginTime;
                                                        Session["CurrentLoginTime"] = CurrentLoginTime;
                                                        Session["isVerified"] = isVerified;
                                                    }
                                                }
                                            }
                                            email.user = user;
                                            email.fpxResponseDesc = fpxResponseDesc;
                                            EmailService.SendOrderEmail(userOrders, email, funds, funds2, primaryAccount.AccountNo);
                                        }

                                        //if transaction succeed
                                        if (!string.IsNullOrEmpty(fpx_fpxTxnId))
                                        {
                                            Response responsePaymentDetails = GenericService.PullData<PaymentDetail>(" transaction_id = '" + fpx_fpxTxnId + "' ", 0, 0, true, nameof(PaymentDetail.Id));
                                            if (responsePaymentDetails.IsSuccess)
                                            {
                                                List<PaymentDetail> paymentDetails = (List<PaymentDetail>)responsePaymentDetails.Data;
                                                //PaymentDetail paymentDetail = new PaymentDetail();
                                                //paymentDetail.BuyerAccNo = fpx_buyerName;
                                                //paymentDetail.BuyerBankBranch = fpx_buyerBankBranch;
                                                //paymentDetail.BuyerBankId = fpx_buyerBankId;
                                                //paymentDetail.BuyerEmail = fpx_buyerId;
                                                //paymentDetail.BuyerIban = fpx_buyerIban;
                                                //paymentDetail.BuyerId = fpx_buyerId;
                                                //paymentDetail.BuyerName = fpx_buyerName;
                                                //paymentDetail.CreatedOn = DateTime.Now;
                                                //paymentDetail.MakerName = fpx_makerName;
                                                //paymentDetail.MsgToken = fpx_msgToken;
                                                //paymentDetail.MsgType = fpx_msgType;
                                                //paymentDetail.SellerBankCode = fpx_sellerExId;
                                                //paymentDetail.SellerExId = fpx_sellerExId;
                                                //paymentDetail.SellerExOrderNo = fpx_sellerExOrderNo;
                                                //paymentDetail.SellerId = fpx_sellerId;
                                                //paymentDetail.SellerOrderNo = fpx_sellerOrderNo;
                                                //paymentDetail.SellerTxnTime = fpx_sellerTxnTime;
                                                //paymentDetail.FpxTxnTime = fpx_fpxTxnTime;
                                                //paymentDetail.TransactionId = fpx_fpxTxnId;
                                                //paymentDetail.TxnAmount = fpx_txnAmount;
                                                //paymentDetail.TxnCurrency = fpx_txnCurrency;
                                                //paymentDetail.Version = "6.0";

                                                //if payment not exist > add
                                                if (paymentDetails.Count == 0)
                                                {
                                                    IPaymentDetailService.PostData(paymentDetail);
                                                    Logger.WriteLog("Payment Inserted Successfully");
                                                }
                                                else // if payment exist >update details
                                                {
                                                    paymentDetail.Id = paymentDetails.FirstOrDefault().Id;
                                                    IPaymentDetailService.UpdateData(paymentDetail);
                                                    Logger.WriteLog("Payment Updated Successfully");
                                                }
                                            }

                                        }
                                        else
                                            txtStatus.InnerText = "Sorry your payment has failed!";


                                    }
                                }


                            }
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUOList.Message + "\", '');", true);
                    }
                }
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx?redirectUrl=BuyFunds.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx?redirectUrl=BuyFunds.aspx');", true);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }
    }
}