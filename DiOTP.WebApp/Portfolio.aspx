﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.Master" AutoEventWireup="true" CodeBehind="Portfolio.aspx.cs" Inherits="DiOTP.WebApp.Portfolio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
    <%--<style>
        .account-selected {
            display: none;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2">
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="">eApexIs</a></li>
                            <li class="active">Portfolio</li>
                        </ol>
                    </div>

                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Portfolio</h3>
                    </div>
                    <span class="label label-success pull-right" id="serviceMessage" runat="server" style="margin-top: -20px; position: relative;"></span>
                    <div class="login-message" id="loginMessage">
                        <p id="ploginDateTime" runat="server">
                            Your last login was: <strong id="loginDateTime" runat="server"></strong>.
                            Check below for cut-off time for Buy/Sell/Switch. Current time: <strong id="presentTime"></strong>
                        </p>
                    </div>

                    <div class="row mt-m-40">
                        <div class="col-md-12">
                            <a href="javascript:;" class="btn btn-infos1 btn-active portfolio-btn" id="ConsolidatedAccount"><i class="fa fa-caret-down"></i>Consolidated Account</a>
                            <span id="portfolioAccountsMenu" runat="server" clientidmode="static" class="hidden-xs"></span>
                            <asp:DropDownList CssClass="form-control hidden-md hidden-lg" ID="ddlPorfolioAccountsMenuMobile" runat="server" ClientIDMode="Static">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <hr class="my-hr" />
                </div>
                <div class="col-md-12" id="consolidatedAccountSummary">
                    <div class="loadingDiv hide" id="consolidatedAccountSummaryLoading">
                        <div class="typing_loader"></div>
                        <div class="text-center">Loading...</div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default" id="consolidatedAccountSummaryPanel">
                                <div class="table-head" style="background-color: rgba(5, 156, 206, 1);">
                                    <h5 class="mt-0 ff-ls text-uppercase display-inline-block" style="margin-bottom: 2px !important;"><i class="fa fa-universal-access mr-5"></i>Consolidated Account Summary
                                    </h5>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="maAccount-box">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="profile">
                                                            <!--<div class="panel panel-default">-->
                                                            <div class="table-head hide">
                                                                <h5 class="mt-0 ff-ls text-center text-uppercase" style="margin-bottom: 2px !important;">Accounts
                                                                </h5>
                                                                <small class="pull-right text-right hide" style="margin-top: -3.5px;" data-toggle="tooltip" data-placement="bottom" title="Last Login">
                                                                    <small class="display-block" id="lastLoginTime" runat="server"></small>
                                                                    <small class="display-block" id="lastLoginIP" runat="server"></small>
                                                                </small>
                                                            </div>
                                                            <!--<div> class="panel-body" -->
                                                            <!-- <div>class="profile-body text-center" -->
                                                            <div class="loadingDiv hide" id="profileLoading">
                                                                <div class="typing_loader"></div>
                                                                <div class="text-center">Loading...</div>
                                                            </div>
                                                            <div class="row hide">
                                                                <div class="col-md-offset-3 col-md-6">
                                                                    <img src="/Content/MyImage/1.png" alt="User" class="img-responsive" />
                                                                </div>
                                                            </div>
                                                            <h5 class="text-uppercase text-info mb-0 hide" id="username" runat="server"></h5>
                                                            <div class="text-muted mb-5 hide">
                                                                <span id="email" runat="server" class="fs-13"></span>
                                                                <small id="userStatus" runat="server" class="text-danger ml-4 position-absolute"></small>
                                                            </div>
                                                            <div class="text-muted hide">
                                                                <span id="mobile" runat="server"></span>
                                                                <small id="mobileStatus" runat="server" class="position-absolute"></small>
                                                            </div>
                                                            <h5 class="text-uppercase underline mb-5 text-muted">Accounts (<span id="consolidatedAccountsCount"><strong>0</strong></span>)</h5>
                                                            <div class="bg1">
                                                                <table class="table table-condensed table-bordered mt-0 ma-account-table">
                                                                    <tr>
                                                                        <th></th>
                                                                        <th>Acc</th>
                                                                        <th>Plan</th>
                                                                    </tr>
                                                                    <tbody id="maAccounts" runat="server" clientidmode="static" class="fs-12">
                                                                    </tbody>
                                                                    <tfoot id="addAccountFunction" runat="server">
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <a href="javascript:;" class="btn btn-sm pull-right addAccountLink">
                                                                                    <i class="fa fa-plus mr-5"></i>Add Account
                                                                                </a>
                                                                                <a href="/AccountOpeningType.aspx" class="btn btn-sm pull-right openAccountLink">
                                                                                    <i class="fa fa-plus-circle mr-5"></i>Open Account
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                            <!-- </div>-->
                                                            <!--  </div>-->
                                                            <!--  </div>-->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h5 class="text-uppercase underline mb-5 text-muted">Current Status <small><strong>as at <span id="asAtDate"></span></strong></small></h5>
                                                        <div class="row mb-20">
                                                            <div class="col-md-12">
                                                                <h5 class="text-uppercase mb-5 text-primary">Total Current Investment Value <small></small></h5>
                                                                <span id="consolidatedAccountTotalInvestmentLabel"><strong>0.00</strong></span>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <hr class="mt-10 mb-5" />
                                                            </div>
                                                            <div class="col-md-12">
                                                                <h5 class="text-uppercase mb-5 text-primary">Unrealised Profit/Loss MYR(%)</h5>
                                                                <span id="consolidatedAccountUnrealisedGainRM">- </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="operating-hrs">
                                                            <h5 class="text-uppercase text-center underline mb-10 text-primary">Operating Hours</h5>
                                                            <div class="fs-13 mb-5 text-justify">
                                                                9am -6pm Monday to Friday (Except public holidays).<br />
                                                                <b>Cut off time for Buy, Sell and Switch :</b>
                                                                <table class="table table-bordered table-timings mb-6 mt-8">
                                                                    <thead>
                                                                        <tr>
                                                                            <th></th>
                                                                            <th>All Funds</th>
                                                                            <th>Money Market</th>
                                                                            <th>Other</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td><b>CASH</b></td>
                                                                            <td>-</td>
                                                                            <td>Before-11am</td>
                                                                            <td>Before-4pm</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>EPF</b></td>
                                                                            <td>Before-12pm</td>
                                                                            <td>-</td>
                                                                            <td>-</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <span class="text-danger" style="color: #ff7676 !important;">Transaction transacted after the cut-off time will be treated for the next Business Day.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="text-center fs-16 mt-10 no-data-available hide">
                                                <strong>--- No data available ---</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="table-head">
                                    <h5 class="mt-0 ff-ls text-uppercase display-inline-block" style="margin-bottom: 2px !important;"><i class="fa fa-bar-chart mr-5"></i>Investment Performance
                                    </h5>
                                </div>
                                <div class="panel-body">
                                    <div class="row mt-20">
                                        <div class="col-md-12">
                                            <div class="stackedBarChartInvestmentPerformanceloadingChartDiv">
                                                <div class="typing_loader"></div>
                                                <div class="text-center">Loading...</div>
                                            </div>
                                            <div id="stackedBarChartInvestmentPerformance"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="table-head">
                                    <h5 class="mt-0 ff-ls text-uppercase display-inline-block" style="margin-bottom: 2px !important;"><i class="fa fa-line-chart mr-5"></i>Realized Profit
                                    </h5>
                                </div>
                                <div class="panel-body">
                                    <div class="row mt-20">
                                        <div class="col-md-12">
                                            <div class="lineChartPortfolioMarketValueMovementloadingChartDiv">
                                                <div class="typing_loader"></div>
                                                <div class="text-center">Loading...</div>
                                            </div>
                                            <div id="lineChartPortfolioMarketValueMovement"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="panel panel-default">
                                <div class="table-head grey-200">
                                    <h5 class="mt-0 ff-ls text-uppercase display-inline-block" style="margin-bottom: 2px !important;"><i class="fa fa-pie-chart mr-5"></i><span>Account Allocations</span>
                                    </h5>
                                </div>
                                <div class="panel-body">
                                    <div class="row mt-20">
                                        <div class="col-md-12">
                                            <div class="donutChartAccountAllocationloadingChartDiv">
                                                <div class="typing_loader"></div>
                                                <div class="text-center">Loading...</div>
                                            </div>
                                            <div id="donutChartAccountAllocation" class="pieChart-highchart"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="panel panel-default">
                                <div class="table-head grey-200">
                                    <h5 class="mt-0 ff-ls text-uppercase display-inline-block" style="margin-bottom: 2px !important;"><i class="fa fa-pie-chart mr-5"></i><span>Fund Asset Allocations</span>
                                    </h5>
                                </div>
                                <div class="panel-body">
                                    <div class="row mt-20">
                                        <div class="col-md-12">
                                            <div class="donutChartFundCategoryAllocationloadingChartDiv">
                                                <div class="typing_loader"></div>
                                                <div class="text-center">Loading...</div>
                                            </div>
                                            <div id="donutChartFundCategoryAllocation" class="pieChart-highchart"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 hide">
                            <div class="panel panel-default">
                                <div class="table-head">
                                    <h5 class="mt-0 ff-ls text-uppercase display-inline-block" style="margin-bottom: 2px !important;"><i class="fa fa-pie-chart mr-5"></i><span>Portfolio Allocations</span>
                                    </h5>
                                </div>
                                <div class="panel-body">
                                    <div class="row mt-20">
                                        <div class="col-md-12">
                                            <div class="pieChartPortfolioAllocationloadingChartDiv">
                                                <div class="typing_loader"></div>
                                                <div class="text-center">Loading...</div>
                                            </div>
                                            <div id="pieChartPortfolioAllocation" class="pieChart-highchart"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="panel panel-default">
                                <div class="table-head grey-200">
                                    <h5 class="mt-0 ff-ls text-uppercase display-inline-block" style="margin-bottom: 2px !important;"><i class="fa fa-bar-chart mr-5"></i>Fund Allocations
                                    </h5>
                                </div>
                                <div class="panel-body">
                                    <div class="row mt-20">
                                        <div class="col-md-12">
                                            <div class="stackedBarChartFundAllocationsloadingChartDiv">
                                                <div class="typing_loader"></div>
                                                <div class="text-center">Loading...</div>
                                            </div>
                                            <div id="stackedBarChartFundAllocations"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 hide" id="consolidatedFundSummaryByAccount">
                    <div class="loadingDiv" id="transactionSummaryLoading">
                        <div class="typing_loader"></div>
                        <div class="text-center">Loading...</div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center hide" id="consolidatedSummaryNoData" runat="server" clientidmode="static" style="position: absolute;z-index: 999;min-height: 321px;background: rgba(255, 255, 255, 0.8);margin-left: 16px;margin-top: 33px;width: calc(100% - 32px);">
                            <a href="/BuyFunds.aspx" class="btn btn-infos1" style="margin-top: 130px;">Start Investing</a>
                        </div>
                        <div class="col-md-8">
                            <div class="panel panel-default" id="consolidateSummaryforAccount">
                                <div class="table-head" style="background-color: rgb(5, 156, 206);">
                                    <h5 class="mt-0 ff-ls text-uppercase display-inline-block" style="margin-bottom: 2px !important;">Account Summary <span id="holderType"></span><span id="holderPlan"></span>
                                        <small class="pull-right text-right hide">
                                            <span class="display-block ma-account-summary-head-label text-capitalize">
                                                <span class="maHolderRegNoLabel"></span>
                                            </span>
                                        </small>
                                    </h5>
                                </div>
                                <div class="panel-body tableInvestments">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="maAccount-box">
                                                <h5 class="text-uppercase underline mb-5 text-muted">Current Status  <small><strong>AS AT <span id="CurrentDate"></span></strong></small></h5>
                                                <div class="row mb-20">
                                                    <div class="col-md-6">
                                                        <h5 class="text-uppercase mb-5 text-primary">Total Current Investment Value<small></small></h5>
                                                        <span id="maAccountRMTotalInvestmentLabel"><strong>0.00</strong></span>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h5 class="text-uppercase mb-5 text-primary" id="EpfTypeHead">EPF TYPE</h5>
                                                        <span id="epfType"><strong>0.0000</strong></span>
                                                        <%--<span id="fundsCount"><strong>0.0000</strong></span>--%>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <hr class="mt-10 mb-5" />
                                                    </div>
                                                    <div class="col-md-6" id="divSAT">
                                                        <h5 class="text-uppercase mb-5 text-primary" title="“SAT” -  Suitability Assessment Test, this is based on investor’s need analysis/risk profiling to assess and compute your risk scoring and thereafter it will group into different category of fund that match with your risk level.">SAT Group & Score</h5>
                                                        <span id="SATScoreLabel" runat="server" clientidmode="static"><strong>0</strong></span>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h5 class="text-uppercase mb-5 text-primary">Unrealised Profit/Loss MYR(%)</h5>
                                                        <span id="unrealisedGain"><strong>- </strong></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="text-center fs-16 mt-10 no-data-available hide">
                                                <strong>--- No data available ---</strong>
                                            </div>
                                        </div>

                                    </div>



                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="table-head" style="background-color: rgb(5, 156, 206);">
                                    <h5 class="mt-0 ff-ls text-uppercase display-inline-block" style="margin-bottom: 2px !important;">Fund Allocations</h5>
                                </div>
                                <div class="panel-body">
                                    <div class="mt-0 text-uppercase position-absolute text-center fs-12 hide" style="width: calc(100% - 30px);">
                                        Total Current Value <small></small>
                                        <div class="fs-14"><strong id="totalRMValueLabel"></strong></div>
                                    </div>
                                    <div id="pieChart" class="pieChart-highchart"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row" id="viewscroll">
                        <div class="col-md-12">

                            <div class="tabs-horizontal my-tabs">
                                <ul class="nav nav-tabs my-nav-tabs">
                                    <li class="active" id="InvestmentTab">
                                        <a href="#InvestmentHoldings" data-toggle="tab">Investment holdings</a>
                                    </li>
                                    <li>
                                        <a href="#AccountDetails" data-toggle="tab">Account Details</a>
                                    </li>
                                    <li id="Accountbank" runat="server" clientidmode="static">
                                        <a href="#BankDetails" data-toggle="tab">Bank Details</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="InvestmentHoldings">
                                        <div class="loadingDiv" id="transactionLoading">
                                            <div class="typing_loader"></div>
                                            <div class="text-center">Loading...</div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-condensed table-bordered dataTable table-cell-pad-5" id="investmentTable">
                                                <thead>
                                                    <tr class="fs-13" style="background-color: #b0bec5 !important;">
                                                        <th class="grey-200">No</th>
                                                        <th class="grey-200">Fund<br />
                                                            Name</th>
                                                        <th class="grey-200">Distribution<br />
                                                            Instruction</th>
                                                        <th class="text-right grey-200">Total<br />
                                                            Units</th>
                                                        <th class="text-right grey-200">Actual Cost<br />
                                                            MYR</th>
                                                        <th class="text-right grey-200">Avg. Cost<br />
                                                            MYR</th>
                                                        <th class="text-right grey-200">Market Value<br />
                                                            MYR</th>
                                                        <th class="text-right grey-200">NAV Price<br />
                                                            MYR</th>
                                                        <th class="text-right grey-200">Unrealised Profit/Loss<br />
                                                            MYR</th>
                                                        <th class="text-right grey-200">Unrealised Profit/Loss<br />
                                                            %</th>
                                                        <th class="text-right grey-200"></th>
                                                    </tr>
                                                </thead>
                                                <tbody class="fs-12" id="investmentPortfolioTbody">
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>

                                    <div class="table-responsive tab-pane fade" id="AccountDetails">
                                        <table class="table table-condensed table-bordered dataTable table-cell-pad-5" id="accountDetailsTable">
                                            <tbody class="fs-12" id="accountDetailsTbody">
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="tab-pane fade" id="BankDetails">
                                        <table class="table table-condensed table-bordered dataTable table-cell-pad-5" id="bankDetailsTable">
                                            <thead>
                                                <tr>
                                                    <th class="grey-200">S.No</th>
                                                    <th class="grey-200">Bank Name</th>
                                                    <th class="grey-200">Account Name</th>
                                                    <th class="grey-200">Account Number</th>
                                                    <th class="grey-200">File</th>
                                                    <th class="grey-200">Updated Date</th>
                                                </tr>
                                            </thead>
                                            <tbody class="fs-12" id="bankDetailsTbody">
                                            </tbody>
                                        </table>


                                    </div>

                                </div>


                            </div>

                        </div>
                    </div>

                    <hr class="mb-10 mt-5" />

                    <div class="row">
                        <!--User Account-->
                        <%-- <div class="col-md-12">
                          
                            <div class="panel panel-default" id="">
                                <div class="table-head">
                                    <h5 class="mt-0 ff-ls text-uppercase display-inline-block" style="margin-bottom: 2px !important;">User Account Setting 
                                            <small class="pull-right text-right hide">
                                                <span class="display-block ma-account-summary-head-label text-capitalize">
                                                    <span class="maHolderRegNoLabel"></span>
                                                </span>
                                            </small>
                                    </h5>
                                </div>

                                <div class="panel-body tableInvestments">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="operating-hrs">
                                                <h5 class="text-uppercase text-center underline mb-10 text-primary">Bank Details</h5>
                                                <div class="fs-13 mb-4 text-justify">
                                                    <table class="table">
                                                        <tbody id="tblBank">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="operating-hrs">
                                                <h5 class="text-uppercase text-center underline mb-10 text-primary">Address</h5>
                                                <div class="fs-13 mb-4 text-justify">
                                                    <p id="divMaAddress">
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="operating-hrs">
                                                <h5 class="text-uppercase text-center underline mb-10 text-primary">Hardcopy Setting</h5>
                                                <div class="fs-13 mb-4 text-justify">
                                                    <div class="fs-13 mb-4 text-justify">
                                                        <p id="divHardCopy">Hardcopy Setting - Activated</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                            </div>
                        </div>--%>
                        <!-- User acoount end -->


                        <div class="col-md-12">
                            <div class="loadingDiv" id="transactionSummaryLoading1">
                                <div class="typing_loader"></div>
                                <div class="text-center">Loading...</div>
                            </div>
                            <div class="panel panel-default" id="investmentPortfolio">
                                <div class="table-head grey-300">
                                    <h5 class="mt-0 ff-ls text-uppercase display-inline-block" style="margin-bottom: 2px !important;">Transaction details by Fund 
                                            <small class="pull-right text-right hide">
                                                <span class="display-block ma-account-summary-head-label text-capitalize">
                                                    <span class="maHolderRegNoLabel"></span>
                                                </span>
                                            </small>
                                    </h5>
                                </div>
                                <div class="panel-body tableTransactionDetails apex-blue-bg-transparent hide pt-0" id="divTransactionDetailsByFund">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5><a target="_blank" id="fundName" style="color: white; font-size: 16px; border-radius: 3px;"></a></h5>
                                            <h6 class="fs-14 hide">Transaction Details By Fund</h6>
                                        </div>
                                        <div class="col-md-6 text-right">
                                        </div>
                                    </div>

                                    <div class="table-responsive">
                                        <table class="table table-condensed table-bordered dataTable table-cell-pad-5 transactionTable" id="transactionTable">
                                            <thead>
                                                <tr class="fs-14" style="background-color: #b0bec5;">
                                                    <th class="grey-200">No</th>
                                                    <th class="grey-200">Date of<br />
                                                        Transaction</th>
                                                    <th class="grey-200">Type</th>
                                                    <th class="text-right grey-200">Total<br />
                                                        Units</th>
                                                    <th class="text-right grey-200">NAV Price<br />
                                                        MYR</th>
                                                    <th class="text-right grey-200">Gross Amount<br />
                                                        MYR</th>
                                                    <th class="text-right grey-200">Sales Charge<br />
                                                        %</th>
                                                    <th class="text-right grey-200">Sales Charge<br />
                                                        MYR</th>
                                                    <th class="text-right grey-200">Tax<br />
                                                        MYR</th>
                                                    <%--<th class="text-right">GST<br />
                                                    MYR</th>--%>
                                                    <th class="text-right grey-200">Net Amount<br />
                                                        MYR</th>
                                                    <th class="text-right grey-200">Cost<br />
                                                        MYR</th>
                                                    <%--<th class="text-right grey-200">Avg. Cost<br />
                                                    MYR</th>--%>
                                                    <th class="text-right grey-200">Unit<br />
                                                        Holdings</th>
                                                    <%--<th class="text-right">Unrealised
                                                    <br />
                                                    Profit/Loss</th>--%>
                                                </tr>

                                            </thead>
                                            <tbody id="tbodyTransaction" class="fs-12">
                                            </tbody>
                                            <%--<tfoot>
                                            <tr>
                                                <td colspan="12">
                                                    <small><strong>* Note:</strong> Avg. Cost is including <code>DIVIDEND</code>.</small>
                                                </td>
                                            </tr>
                                        </tfoot>--%>
                                        </table>
                                    </div>
                                    <div class="legend-position hide">
                                        <div class="legend-title-position">
                                            <div class="legend-title-wording">Legend</div>

                                        </div>
                                        <div class="transactiondetails-first-row">
                                            <span>BI - Unit Split</span>
                                            <span>SA - Purchase</span>
                                            <span>CO - Collateral</span>
                                            <span>BU - Bonus Unit</span>
                                            <span>RD - Redemption</span>
                                            <span>UC - Uncollateral</span>
                                            <span>RC - Replace Cert</span>
                                            <%--</div>
                                        <div class="transactiondetails-second-row">--%>
                                            <span>SWI - Switching In</span>
                                            <span>TR - Transfer</span>
                                            <span>SC - Split Cert</span>
                                            <span>SWO - Switching Out</span>
                                            <span>DD - Dividend</span>
                                            <span>MC - Merge Cert</span>
                                            <span>X - Cancellation</span>
                                        </div>

                                    </div>
                                </div>
                                <div class="panel-body tableInvestmentDetails apex-blue-bg-transparent pt-0 hide">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6 class="fs-14">Historical Investment Details By Fund (Movement)</h6>
                                        </div>
                                        <div class="col-md-6 text-right">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="lineChartMarketValueMovementByFundloadingChartDiv">
                                                <div class="typing_loader"></div>
                                                <div class="text-center">Loading...</div>
                                            </div>
                                            <div id="lineChartMarketValueMovementByFund"></div>

                                        </div>

                                    </div>
                                    <%--<table class="table table-condensed table-bordered dataTable table-cell-pad-5" id="detailedTable">
                                        <thead>
                                            <tr class="fs-14">
                                                <th>No</th>
                                                <th class="text-right">From Epf<br />
                                                    MYR</th>
                                                <th class="text-right">Cost<br />
                                                    MYR</th>
                                                <th class="text-right">Units</th>
                                                <th class="text-right">Market Value<br />
                                                    MYR</th>
                                                <th class="text-right">Redemption Cost<br />
                                                    MYR</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbodyInvestmentDetails" class="fs-12">
                                        </tbody>
                                    </table>--%>
                                </div>
                                <%--<button type="button" class="btn btn-next pull-right" id="backbutton">Back</button>--%>
                            </div>

                        </div>


                    </div>

                </div>

            </div>
        </section>
        <asp:HiddenField ID="hdnSelectedAccountNo" runat="server" ClientIDMode="Static" />
    </div>

</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="AccountModal" runat="server">

    <div class="modal fade" id="modaladdAccountPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm sett" role="document" style="width: 75%; max-width: 500px">
            <div class="modal-content">
                <div class="modal-header sett-modalhead text-white">
                    <strong>Add Account Confirmation</strong>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p class="text-justify">Are you sure to add your new MA account to Online Portal?</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" style="margin-top: -1px;" class="btn login-btn pull-right" onclick="javascript:window.location.href='OTP-Activation.aspx';">Proceed</button>
                    <button type="button" style="margin-top: -1px;" class="btn pull-right" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalSecondSecurity" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content google-modal">
                <div class="modal-body">
                    <div class="row">
                        <div id="mobileTwoFactorAuth1" style="width: 10%;">
                            <asp:Button ID="btnLogout" runat="server" CssClass="btn btn-info" Text="Cancel" OnClick="btnLogout_Click" Style="position: absolute; right: 15px;" />
                        </div>
                        <div id="mobileTwoFactorAuth2" class="fs-14 text-center text-white mb-5" style="width: 90%;">
                            <div>You can select either "Google Authentication" or "Mobile Authentication" to login.</div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="google-authentication">
                                <div class="row">
                                    <div class="col-md-12">
                                        <img src="/Content/MyImage/Google-Authenticator-icon.png" height="40" /><br />
                                        <label style="font-size: 14px; font-weight: 600;">Google Authentication</label>
                                        <hr />
                                        <asp:Panel ID="googlePanel" runat="server" DefaultButton="btnCode">
                                            <div class="body-google">
                                                <div class="form-group row" id="divBindGoogleAuthenticator" runat="server" visible="false">
                                                    <div class="col-md-12 text-center" id="divGoogleAuthDisplay" runat="server">
                                                        <asp:Image ID="imgQRCode" runat="server" />
                                                        <br />
                                                        Manual Entry Key:
                                                    <asp:Label ID="lblManualEntryKey" runat="server" Font-Bold="true" CssClass="fs-12 manual-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <label>Enter Your Code:</label>
                                                <asp:TextBox ID="txtPin" runat="server" ClientIDMode="Static" CssClass="form-control mb-10" placeholder="Enter Code" AutoCompleteType="Disabled"></asp:TextBox>
                                                <asp:Button ID="btnCode" runat="server" ClientIDMode="Static" CssClass="btn btn-block btn-info" Text="Submit" OnClick="btnCode_Click" OnClientClick="return googleValidation()" />
                                                <div class="text-center mt-10">
                                                    <label class="label label-info" id="lblGoogleAuthenticationInfo" runat="server"></label>
                                                </div>
                                                <div class="mt-10" id="ShowGoogleAuthLostLink" runat="server">
                                                    <a href="/LostGoogleAuthenticator.aspx" target="_blank">Lost Google Authenticator?</a>
                                                </div>
                                                <div class="mt-10" id="ShowGoogleAuthBindMessage" runat="server">
                                                    Please login with OTP and bind in user center.
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="/TwoAuthenticatorFactorHelp.aspx" target="_blank">Learn how to use.</a><br />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="phone-verification">
                                <div class="row">
                                    <div class="col-md-12">
                                        <img src="/Content/MyImage/10.png" height="40" /><br />
                                        <label style="font-size: 14px; font-weight: 600;">Mobile Authentication</label>
                                        <hr />
                                        <asp:Panel ID="phonePanel" runat="server" DefaultButton="btnVerifyByMobile">
                                            <div class="body-google">
                                                <label>Enter Your OTP:</label>
                                                <div>
                                                    <button type="button" id="btnRequest" runat="server" clientidmode="static" class="btn btn-warning">Request OTP</button>
                                                    <asp:TextBox Text="" ID="txtMobilePin" ClientIDMode="Static" runat="server" CssClass="form-control mb-10" placeholder="Enter OTP" AutoCompleteType="Disabled" Enabled="false"></asp:TextBox>
                                                </div>
                                                <asp:Button ID="btnVerifyByMobile" ClientIDMode="Static" runat="server" CssClass="btn btn-block btn-info" Text="Submit" OnClick="btnVerifyByMobile_Click" OnClientClick="return mobileValidation()" disabled="disabled" />
                                                <div class="mt-10 text-center">
                                                    <asp:HiddenField ID="hdOtpStatus" Value="0" ClientIDMode="Static" runat="server" />
                                                    <small class="" id="lblInfo" runat="server" clientidmode="static"></small>
                                                </div>
                                                <div class="mt-10 countdown text-center">
                                                    <span id="countdownLabelText">&nbsp</span>
                                                    <label class="label label-warning hide" style="width: 70px; text-align: center; display: inline-block;" id="countDownTime" runat="server" clientidmode="static"></label>
                                                </div>
                                                <div class="mt-10 text-center hide" id="requestNewPinLinkDiv" runat="server" clientidmode="static">
                                                    
                                                    
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="/TwoAuthenticatorFactorHelp.aspx" target="_blank">Learn how to use.</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fs-14 text-center text-white">please contact our Customer Service Representative +603 2095 9999 for assistance.</div>

                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="modalMAActivation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm sett" role="document">
            <div class="modal-content" style="width: 350px;">
                <asp:Panel ID="maActivationPanel" runat="server" DefaultButton="btnMAActivation">
                    <asp:Button ID="btnExit" runat="server" Text="x" OnClick="btnExit_Click" Visible="true" Style="position: relative; float: right; background-color: transparent; border: none;" />
                    <div class="modal-header sett-modalhead text-white">
                        <strong>Activate your MasterAccount</strong>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="button" id="btnRequestMAActivation" runat="server" clientidmode="static" class="btn btn-warning" style="position: absolute; right: 15px;">Request OTP</button>
                                <asp:TextBox ID="txtMAActivationPin" ClientIDMode="Static" runat="server" CssClass="form-control mb-10" placeholder="Enter OTP" autocomplete="off"></asp:TextBox>
                                <div class="text-center mt-10">
                                    <small class="" id="lblInfoMAActivation" runat="server" clientidmode="static"></small>
                                </div>
                                <div class="mt-10 countdown text-center">
                                    <span id="countdownMAActivation" runat="server" clientidmode="static">&nbsp</span>
                                    <label class="label label-warning hide" style="width: 70px; text-align: center; display: inline-block;" id="countdownTimeMAActivation" runat="server" clientidmode="static"></label>
                                </div>
                                <div class="text-center"><small><strong>Note: </strong>An OTP will be sent to your mobile.</small></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnMAActivation" runat="server" CssClass="btn login-btn pull-right" Text="Submit" OnClick="btnMAActivation_Click" />
                        <asp:Button ID="Button1" runat="server" CssClass="btn pull-right" Text="Cancel" OnClick="btnLogout_Click" />
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalExpiryOfPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog sett" role="document">
            <div class="modal-content">
                <div class="modal-header sett-modalhead text-white">
                    <strong>Expiry of Password</strong>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Your Password will be expired within <span class="passwordExpiryDaysCount">15</span> days</h6>
                            <p>Please change your password within <span class="passwordExpiryDaysCount">15</span> days.</p>
                            <div class="text-center mt-10">
                                <label class="label label-info" id="Label1" runat="server"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalChangePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog sett" role="document">
            <div class="modal-content">
                <div class="modal-header sett-modalhead text-white">
                    <strong>Expiry of Password</strong>
                </div>
                <div class="modal-body">
                    <asp:Panel ID="changePasswordPanel" runat="server" DefaultButton="btnSubmitChangePw">
                        <div class="row">
                            <div class="col-md-12">
                                <h6>Your password has expired. For security reason, you are required to change your password immediately User ID: </h6>
                                <asp:RadioButton ID="rdbChangePw" runat="server" GroupName="rdbEOP" Text="Yes, I wish to change my password :" Checked="true" />
                                <div class="row">
                                    <div class="col-md-6">
                                        Key-in existing password
                                    </div>

                                    <div class="col-md-6">
                                        <asp:TextBox TextMode="Password" ID="txtEOPExisting1" runat="server" CssClass="form-control mb-10" placeholder="Existing Password" autocomplete="off"></asp:TextBox>
                                    </div>

                                    <div class="col-md-6">
                                        Key-in new password
                                    </div>

                                    <div class="col-md-6">
                                        <asp:TextBox TextMode="Password" ID="txtEOPNewPw" runat="server" CssClass="form-control mb-10" placeholder="New Password" autocomplete="off"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6">
                                        Confirm new password
                                    </div>

                                    <div class="col-md-6">
                                        <asp:TextBox TextMode="Password" ID="txtEOPCfNewPw" runat="server" CssClass="form-control mb-10" placeholder="Confirm New Password" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <asp:RadioButton ID="rdbKeepPw" runat="server" GroupName="rdbEOP" Text="No, I do not wish to change my password :" />
                                <div class="row">
                                    <div class="col-md-6">
                                        Key-in existing password
                                    </div>

                                    <div class="col-md-6">
                                        <asp:TextBox TextMode="Password" ID="txtEOPExisting2" runat="server" CssClass="form-control mb-10" placeholder="Existing Password" autocomplete="off"></asp:TextBox>
                                    </div>

                                    <div class="col-md-6">
                                        Confirm existing password
                                    </div>

                                    <div class="col-md-6">
                                        <asp:TextBox TextMode="Password" ID="txtEOPCfExisting" runat="server" CssClass="form-control mb-10" placeholder="New Password" autocomplete="off"></asp:TextBox>
                                    </div>

                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-12">
                                        <br />
                                        <div style="height: 2px; width: 100%; background-color: black"></div>
                                        <br />
                                    </div>
                                    <div class="col-md-12">
                                        <h5>Notes: </h5>
                                    </div>
                                    <div class="col-md-12">
                                        <ul>
                                            <li>Password must be between 8 to 16 characters.</li>
                                            <li>It must contain a minimum of 1 aplhabet and 1 numeric character.</li>
                                            <li>It is case sensitive.</li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnSubmitChangePw" runat="server" Text="Confirm" OnClick="btnSubmitChangePw_Click" />
                    <asp:Button ID="Button2" runat="server" CssClass="btn pull-right" Text="Cancel" OnClick="btnLogout_Click" />
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnOption" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnIsMAActive" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnIsVerified" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSMSExpirationTimeInSeconds" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnMobileNumber" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnMobilePinRequested" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnGoogleAuthAttempts" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSMSAttempts" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSMSLockTimeInSeconds" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnIsSMSLocked" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnMAToBeActivated" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnPasswordExpiredDate" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnTotalUnits" runat="server" Value="0" ClientIDMode="Static" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="AccountScripts" runat="server">
    <script src="Content/js/autoNumeric.js"></script>
    <%-- <script>
        $(document).ready(function () {
            setTimeout(function () {
                $('#loginMessage').fadeOut(3000);
            }, 5000);
        });
    </script>--%>
    <script src="/Content/js/highcharts.js"></script>
    <%--<script src="https://code.highcharts.com/highcharts-3d.js"></script>--%>
    <script type="text/javascript">
        $('form').on('submit', function () {
            setTimeout(function () {
                $('.loader-bg').fadeIn();
            }, 500);
        });

        $('.addAccountLink').click(function () {
            $('#modaladdAccountPop').modal({
                keyboard: false,
                backdrop: "static"
            });
        });

        function googleValidation() {
            $('#txtMobilePin').css({
                "borderBottom": "",
                //"background": "LightBlue"
            });
            var isValid = true;
            if ($('#txtPin').val() == "") {
                isValid = false;
                $('#txtPin').css({
                    "borderBottom": "1px solid red",
                    //"background": "#FFCECE"
                });
            }
            else {
                $('#txtPin').css({
                    "borderBottom": "",
                    //"background": "LightBlue"
                });
            }
            return isValid;
        }

        function mobileValidation() {
            $('#txtPin').css({
                "borderBottom": "",
                //"background": "LightBlue"
            });
            var isValid = true;
            if ($('#txtMobilePin').val() == "") {
                isValid = false;
                $('#txtMobilePin').css({
                    "borderBottom": "1px solid red",
                    //"background": "#FFCECE"
                });
            }
            else {
                $('#txtMobilePin').css({
                    "borderBottom": "",
                    //"background": "LightBlue"
                });
            }
            return isValid;
        }


        function secondsTimeSpanToHMS(s) {
            var h = Math.floor(s / 3600); //Get whole hours
            s -= h * 3600;
            var m = Math.floor(s / 60); //Get remaining minutes
            s -= m * 60;
            return h + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s); //zero padding on minutes and seconds
        }
        var pin = 0;
        var hdnIsMAActive = parseInt($('#hdnIsMAActive').val());
        var hdnIsVerified = parseInt($('#hdnIsVerified').val());
        var hdnMobilePinRequested = parseInt($('#hdnMobilePinRequested').val());
        var hdnSMSExpirationTimeInSeconds = parseInt($('#hdnSMSExpirationTimeInSeconds').val());
        var hdnSMSLockTimeInSeconds = parseInt($('#hdnSMSLockTimeInSeconds').val());
        var hdnIsSMSLocked = parseInt($('#hdnIsSMSLocked').val());
        var hdnPasswordExpiredDate = parseInt($('#hdnPasswordExpiredDate').val());


        function openPopup(popupid) {
            $('#' + popupid).modal({
                keyboard: false,
                backdrop: "static",
            })
        }
        function updateClock() {
            var time = new Date();
            $("#presentTime").html(time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: true }));
        }
        $(document).ready(function () {
            if ($('#hdOtpStatus').val() == "1") {
                $('#btnVerifyByMobile').prop('disabled', false);
            }
            setInterval('updateClock()', 1000);
        });
        var isCurFormat;
        var isCurFormatNoSymbol;
        function FormatAllCurrency() {
            $('.currencyFormat, .currencyFormatNoSymbol').each(function (i) {
                var self = $(this);
                try {
                    var v = self.autoNumeric('get');
                    self.autoNumeric('destroy');
                    self.val(v);
                } catch (err) {
                    //console.log("Not an autonumeric field: " + self.attr("name"));
                }
            });
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ' });
            });
            $('.currencyFormatNoSymbol').each(function () {
                isCurFormatNoSymbol = new AutoNumeric(this, { currencySymbol: '' });
            });
        }
        FormatAllCurrency();
        var isUniFormat;
        function FormatAllUnit() {
            $('.unitFormat, .unitFormatNoDecimal').each(function (i) {
                var self = $(this);
                try {
                    var v = self.autoNumeric('get');
                    self.autoNumeric('destroy');
                    self.val(v);
                } catch (err) {
                    //console.log("Not an autonumeric field: " + self.attr("name"));
                }
            });
            $('.unitFormat').each(function () {
                isUniFormat = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: 4 });
            });
            $('.unitFormatNoDecimal').each(function () {
                isUniFormat = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: 0 });
            });
        }
        FormatAllUnit();
        if (hdnIsMAActive == 1 && hdnIsVerified == 1) {
            var chart = Highcharts.chart('pieChart', {
                chart: {
                    height: '300px',
                    type: 'pie',
                    className: 'fund-bg-color',
                    style: {
                        fontFamily: "'Montserrat', sans-serif"
                    }
                },
                title: {
                    useHTML: true,
                    text: ''
                    //text: '<p class="fs-12">Based on Investment</p>'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>',
                    followPointer: true,
                },
                credits: {
                    href: '/Index.aspx',
                    text: 'eapexis.apexis.com.my',
                    position: {
                        align: 'right',
                        verticalAlign: 'bottom',
                    }
                },
                plotOptions: {
                    pie: {
                        size: 130,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        //depth: 25,
                        //dataLabels: {
                        //    enabled: true,
                        //    format: '{point.name}',
                        //    style: {
                        //        textOutline: false
                        //    }
                        //},
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                layout: 'horizontal'
                            },
                            yAxis: {
                                labels: {
                                    align: 'left',
                                    x: 0,
                                    y: -5
                                },
                                title: {
                                    text: null
                                }
                            },
                            subtitle: {
                                text: null
                            },
                            credits: {
                                enabled: false
                            }
                        }
                    }],
                }
            });
            var lineChartMarketValueMovementByFundXAxisLength = 0;
            var lineChartMarketValueMovementByFund = Highcharts.chart('lineChartMarketValueMovementByFund', {
                chart: {
                    height: '300px',
                    type: 'line',
                    events:
                    {
                        addSeries: function () {
                            $('.lineChartMarketValueMovementByFundloadingChartDiv').fadeOut();
                            var label = this.renderer.label('A series was added', 30, 30)
                                .attr({
                                    fill: Highcharts.getOptions().colors[0],
                                    padding: 10,
                                    zIndex: 8
                                })
                                .css({
                                    color: '#FFFFFF'
                                })
                                .add();

                            setTimeout(function () {
                                label.fadeOut();
                            }, 1000);
                        },
                        load: function () {
                        }
                    },
                    borderColor: '#000000',
                    borderWidth: 0,
                    inverted: false,
                    zoomType: 'x',
                    spacingRight: 20,
                    style: {
                        fontFamily: "'Montserrat', sans-serif"
                    }
                },
                title: {
                    useHTML: true,
                    text: '<p style="font-size:12px; margin-bottom:5px; color:#aaaaaa;">Realized Profit </p>'
                },
                subtitle: {
                    useHTML: true,
                    text: (document.ontouchstart === undefined ?
                        'Click and drag in the plot area to zoom in' :
                        '<div class="pinchZoom">Pinch the chart to zoom in</div>')
                },
                xAxis: {
                    allowDecimals: true,
                    labels: {
                        rotation: -45,
                        step: 30,
                        showLastLabel: true,
                        endOnTick: true,
                        x: -10,
                        formatter: function () {
                            var x = this.value;

                            if (lineChartMarketValueMovementByFundXAxisLength <= 100) {
                                lineChartMarketValueMovementByFund.xAxis[0].options.labels.step = 4;
                                return Highcharts.dateFormat("%b '%y", new Date(x));
                            }
                            else {
                                lineChartMarketValueMovementByFund.xAxis[0].options.labels.step = 200;
                                return Highcharts.dateFormat("%b '%y", new Date(x));
                            }
                            return Highcharts.dateFormat("%b '%y", new Date(x));
                        },
                        style: {
                            fontSize: '10px'
                        },
                        padding: 2,
                        distance: 5
                    },
                    categories: []
                },
                yAxis: {
                    endOnTick: false,
                    allowDecimals: true,
                    title: {
                        text: 'MYR'
                    }
                },
                tooltip: {
                    shared: true,
                    pointFormat: '{series.name} produced <b>{point.y} </b><br/>warheads in {point.x}',
                    formatter: function () {
                        var s = [];
                        $.each(this.points, function (i, point) {
                            var seriesName = point.series.name;
                            var y = point.y;
                            var x = point.x;

                            var str = y.toString().split('.');
                            if (str[0].length >= 4) {
                                str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
                            }

                            s.push('<span style="color:' + point.color + '">Date: ' + Highcharts.dateFormat('%e. %b %Y', new Date(x)) + '</span><br/><span style="color:' + point.color + '"> ' + this.series.name + ': ' + y.toFixed(2) + ' </span>');
                        });

                        return s.join('<br />');
                    },
                    followPointer: true,
                    //positioner: { x: 100, y: 100 },
                    crosshairs: [true, true],
                    crosshairs: {
                        color: 'rgba(0,0,0,0.3)',
                        dashStyle: 'Solid',
                        width: 2
                    },
                },
                credits: {
                    href: '/Index.aspx',
                    text: 'eapexis.apexis.com.my',
                    position: {
                        align: 'right',
                        verticalAlign: 'bottom',
                    }
                },
                plotOptions: {
                    line: {
                        marker: {
                            enabled: true,
                            symbol: 'circle',
                            radius: 2,
                            states: {
                                hover: {
                                    enabled: true
                                },
                            }
                        }
                    }
                },
                series: [

                ],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                layout: 'horizontal'
                            },
                            yAxis: {
                                labels: {
                                    align: 'left',
                                    x: 0,
                                    y: -5
                                },
                                title: {
                                    text: null
                                }
                            },

                            subtitle: {
                                text: null
                            },
                            credits: {
                                enabled: false
                            },
                        }
                    }],
                }
            });
            function ConvertToCurrency(num) {
                return num.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            }
        }

        if (hdnMobilePinRequested == 1) {
            if (hdnIsSMSLocked == 1) {
                $('#countdownLabelText').html('You can request new OTP in ');
                $('#countdownMAActivation').html('You can request new OTP in ');
                SMSLockStartTimer();
            }
            else {
                $('#countdownLabelText').html('Your OTP expires in');
                $('#countdownMAActivation').html('Your OTP expires in');
                StartTimer();
                StartTimer1();
            }
        }
        else {
            $('#countdownMAActivation').html('');
            $('#countdownTimeMAActivation').html('');
            $('#countdownTimeMAActivation').addClass('hide');
        }

        function StartTimer() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSExpirationTimeInSeconds);
            //var timer2 = "5:01";
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    $('#countdownLabelText').html('OTP Expired. Please Request Again.');
                    $('#countDownTime').addClass('hide');
                    $('#countDownTime').html('');
                    $('#lblInfo').html('');
                    $('#btnRequest').removeAttr('disabled');
                    $('#hdnMobilePinRequested').val(0);
                    hdnMobilePinRequested = parseInt($('#hdnMobilePinRequested').val());
                }
                else {
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#countDownTime').html(hours + ':' + minutes + ':' + seconds);
                    timer2 = hours + ':' + minutes + ':' + seconds;
                    $('#countDownTime').removeClass('hide');
                }
            }, 1000);
        }

        function StartTimer1() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSExpirationTimeInSeconds);
            //var timer2 = "5:01";
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    $('#countdownMAActivation').html('OTP Expired. Please Request Again.');
                    $('#countdownTimeMAActivation').html('');
                    $('#countdownTimeMAActivation').addClass('hide');
                    $('#lblInfoMAActivation').html('');
                    $('#btnRequestMAActivation').removeAttr('disabled');
                    $('#hdnMobilePinRequested').val(0);
                    hdnMobilePinRequested = parseInt($('#hdnMobilePinRequested').val());
                }
                else {
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#countdownTimeMAActivation').html(hours + ':' + minutes + ':' + seconds);
                    $('#countdownTimeMAActivation').removeClass('hide');
                    timer2 = hours + ':' + minutes + ':' + seconds;
                }
            }, 1000);
        }

        function SMSLockStartTimer() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSLockTimeInSeconds);
            //var timer2 = "5:01";
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    //$('.countdown').html('Your Can Request Now.');
                    $('#countdownLabelText').html('Your Can Request Now.');
                    $('#countDownTime').html('');
                    $('#countDownTime').addClass('hide');
                    $('#btnRequest').removeAttr('disabled');
                    $('#hdnMobilePinRequested').val(0);
                }
                else {
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#countDownTime').html(hours + ':' + minutes + ':' + seconds);
                    $('#countDownTime').removeClass('hide');
                    timer2 = hours + ':' + minutes + ':' + seconds;
                }
            }, 1000);
        }

        function SMSLockStartTimer1() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSLockTimeInSeconds);
            //var timer2 = "5:01";
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    $('#countdownMAActivation').html('Your Can Request Now.');
                    $('#countdownTimeMAActivation').html('');
                    $('#countdownTimeMAActivation').addClass('hide');
                    $('#btnRequestMAActivation').removeAttr('disabled');
                    $('#hdnMobilePinRequested').val(0);
                }
                else {
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#countdownTimeMAActivation').html(hours + ':' + minutes + ':' + seconds);
                    $('#countdownTimeMAActivation').removeClass('hide');
                    timer2 = hours + ':' + minutes + ':' + seconds;
                }
            }, 1000);
        }

        $(document).ready(function () {
            if (!$('#btnRequestMAActivation').attr('disabled')) {
                $('#txtMAActivationPin').attr('readonly', 'readonly');
            }
            else {
                StartTimer1();
            }
            if (hdnIsMAActive == 0) {
                $('#modalMAActivation').modal({
                    keyboard: false,
                    backdrop: "static",
                });
            }
            else if (hdnIsVerified == 0) {
                $('#modalSecondSecurity').modal({
                    keyboard: false,
                    backdrop: "static",
                });
                var hdnOption = $('#hdnOption').val();
                if (hdnOption == "G") {
                    if ($('[href="#collapseOne"]').hasClass("collapsed")) {

                    }
                    else {
                        $('[href="#collapseOne"]').addClass("collapsed")
                        //$('[href="#collapseOne"]').click();
                    }
                }
                if (hdnOption == "M") {
                    $('[href="#collapseTwo"]').click();
                }
            }

            $('#backbutton').click(function () {
                document.getElementById('viewscroll').scrollIntoView();
            });
            if (hdnMobilePinRequested == 1) {
                $('#txtMobilePin').removeAttr('disabled');
            }
            $('#btnRequest').click(function () {
                if (hdnMobilePinRequested == 0) {
                    $.ajax({
                        url: "Portfolio.aspx/PhoneVerify",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: function (data) {
                            return data;
                        },
                        success: function (data) {
                            if (data.d.IsSuccess) {
                                var jsons = data.d.Message.split(',');
                                var json = jsons[0];
                                //console.log(json);
                                $('#hdnMobilePinRequested').val(1);
                                $('#txtMobilePin').val('');
                                $('#btnVerifyByMobile').removeAttr('disabled');
                                if (json == "sent") {
                                    $('#txtMobilePin').removeAttr('disabled');
                                    $('#lblInfo').html("Authentication OTP sent to mobile number<br>" + jsons[1] + " successfully.");
                                    $('#countdownLabelText').html('Please Enter your OTP in');
                                    $('#countDownTime').removeClass('hide');
                                    $('#btnRequest').attr('disabled', 'disabled');
                                    StartTimer();
                                }
                                else if (json == "already sent") {
                                    $('#txtMobilePin').removeAttr('disabled');
                                    $('#lblInfo').html("OTP already sent to mobile number " + jsons[1] + " .");
                                    $('#btnRequest').attr('disabled', 'disabled');
                                    StartTimer();
                                }
                                else if (json == "sms locked") {
                                    $('#countdownLabelText').html('You can request new OTP in ');
                                    $('#countDownTime').removeClass('hide');
                                    $('#lblInfo').html("Too many attempts. Please try later.");
                                    //$('#requestNewPinLinkDiv').addClass('hide');
                                    //$('#btnRequestNewPin').attr('disabled', 'disabled');
                                    $('#btnRequest').attr('disabled', 'disabled');
                                    $('#hdnMobilePinRequested').val(0);
                                    SMSLockStartTimer();
                                }
                                else {
                                    $('#lblInfo').html("No Account Registered with this Mobile Number.");
                                }
                            }
                            else {
                                $('#lblInfo').html(data.d.Message);
                            }
                        }
                    });
                }
                else {
                    //$('#lblInfo').html("OTP already sent.");
                    //$('#btnRequest').attr('disabled', 'disabled');
                    //StartTimer();
                }
            });

            $('#btnRequestMAActivation').click(function () {
                $('#countdownMAActivation').html('');
                if (hdnMobilePinRequested == 0) {
                    $.ajax({
                        url: "Portfolio.aspx/PhoneVerifyMAActivation",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: function (data) {
                            return data;
                        },
                        success: function (data) {
                            var jsons = data.d.split(',');
                            var json = jsons[0];
                            //console.log(json);
                            $('#hdnMobilePinRequested').val(1);
                            if (json == "sent") {
                                $('#lblInfoMAActivation').html("Authentication OTP sent to mobile number<br>" + jsons[1] + " successfully.");
                                $('#countdownMAActivation').html('Please Enter your OTP in');
                                $('#btnRequestMAActivation').attr('disabled', 'disabled');
                                $('#txtMAActivationPin').removeAttr('readonly');
                                StartTimer1();
                            }
                            else if (json == "already sent") {
                                $('#lblInfoMAActivation').html("OTP already sent to mobile number " + jsons[1] + " .");
                                $('#btnRequestMAActivation').attr('disabled', 'disabled');
                                $('#txtMAActivationPin').removeAttr('readonly');
                                StartTimer1();
                            }
                            else if (json == "sms locked") {
                                $('#countdownMAActivation').html('You can request new OTP in ');
                                $('#lblInfoMAActivation').html("Too many attempts. Please try later.");
                                $('#btnRequestMAActivation').attr('disabled', 'disabled');
                                $('#hdnMobilePinRequested').val(0);
                                SMSLockStartTimer1();
                            }
                            else if (json == "no account") {
                                $('#lblInfoMAActivation').html("No Account Registered with this Mobile Number.");
                            }
                            else {
                                $('#hdnSMSExpirationTimeInSeconds').val(data.d);
                                hdnSMSExpirationTimeInSeconds = data.d;
                                $('#lblInfoMAActivation').html("Authentication OTP sent to mobile number<br  >" + jsons[1] + " successfully.");
                                $('#countdownMAActivation').html('Please Enter your OTP in');
                                $('#btnRequestMAActivation').attr('disabled', 'disabled');
                                $('#txtMAActivationPin').removeAttr('readonly');
                                StartTimer1();
                            }
                        }
                    });
                }
            });
        });
    </script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <%--<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>--%>
    <script src="/Content/js/date.format.js"></script>
    <script>

        function pad(str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }

        if (hdnIsMAActive == 1 && hdnIsVerified == 1) {
            var portfolioDetailed;
            var portfolioDetailedForChart;
            function pad(num, size) {
                var s = num + "";
                while (s.length < size) s = "0" + s;
                return s;
            }
            function parseJsonDate(jsonDateString) {
                var formattedDate = new Date(parseInt(jsonDateString.replace('/Date(', '')));
                var newDate = pad(formattedDate.getDate(), 2) + "/" + pad(formattedDate.getMonth() + 1, 2) + "/" + formattedDate.getFullYear();
                return newDate;
            }
            function parseJsonDateToChartDate(jsonDateString) {
                var formattedDate = new Date(parseInt(jsonDateString.replace('/Date(', '')));
                var newDate = formattedDate.getFullYear() + "-" + pad(formattedDate.getMonth() + 1, 2) + "-" + pad(formattedDate.getDate(), 2);
                return newDate;
            }
            function parseJsonDateToLongDate(jsonDateString) {
                var nowDate = new Date(parseInt(jsonDateString.substr(6)));
                var newDate = nowDate.format("dddd, mmmm d, yyyy");
                return newDate;
            }
            var isClickBinded = 0;
            var maAccountRMTotalInvestment = 0;
            var maAccountUNITSTotalInvestment = 0;
            var dataClass;
            var detailedTableFundName;
            function ApplyScripts() {
                if (isClickBinded == 0) {
                    isClickBinded = 1;

                    var IpdFundCode;
                    //$('#investmentPortfolioTbody').on('click', 'tr.movement', function () {
                    //    if (IpdFundCode == $(this).attr('data-value')) {
                    //        //$('.tableInvestmentDetails').toggleClass('hide');
                    //        //document.getElementById('lineChartMarketValueMovementByFund').scrollIntoView();
                    //    }
                    //    else {
                    //        IpdFundCode = $(this).attr('data-value');
                    //        //$('.tableInvestmentDetails').toggleClass('hide');
                    //        $('a[data-value="' + IpdFundCode + '"].details').click();
                    //        //document.getElementById('lineChartMarketValueMovementByFund').scrollIntoView();
                    //    }
                    //});
                    //$('#investmentPortfolioTbody tr.details').unbind('click');
                    $('#investmentPortfolioTbody').on('click', 'tr.details a', function () {
                        $('#transactionLoading').show();
                        $('#transactionSummaryLoading1').removeClass('hide');
                        $('#investmentPortfolioTbody tr.details').removeClass('detailed');
                        $('#investmentPortfolioTbody tr.detailed').addClass('details');
                        var thisO = $(this);
                        //document.getElementById('divTransactionDetailsByFund').scrollIntoView();

                        //$(this).parents('tr').removeClass('details');
                        $(thisO).parents('tr').addClass('detailed');
                        //$('#transactionTable thead tr th').removeClass(dataClass);
                        $('#fundName').removeClass(dataClass);
                        setTimeout(function () {


                            dataClass = $(thisO).parents('tr').attr('data-class');
                            $('#transactionLoading').show();
                            $('.tableTransactionDetails').addClass('hide');
                            IpdFundCode = $(thisO).parents('tr').attr('data-value');
                            var maInvestDetailedByFund = $.grep(portfolioDetailed, function (o, i) {
                                return o.utmcFundInformation.IpdFundCode == IpdFundCode;
                            })[0];
                            //var maInvestDetailedByFundForChart = $.grep(portfolioDetailedForChart, function (o, i) {
                            //    return o.utmcFundInformation.IpdFundCode == IpdFundCode;
                            //})[0];

                            $('#fundName').addClass(dataClass);
                            //$('#transactionTable thead tr th').addClass(dataClass);
                            $('#fundName').html(maInvestDetailedByFund.utmcFundInformation.FundName);
                            $('#fundName').attr('href', "/Fund-Information.aspx?fundCode=" + maInvestDetailedByFund.utmcFundInformation.FundCode);
                            var utmcMemberInvestments = maInvestDetailedByFund.utmcMemberInvestments;
                            //var utmcMemberInvestmentsForChart = maInvestDetailedByFundForChart.utmcMemberInvestments;

                            //var utmcCompositionalTransactions = maInvestDetailedByFund.utmcCompositionalTransactions;
                            var holderLedgers = maInvestDetailedByFund.holderLedgers;
                            //console.log(utmcCompositionalTransactions);
                            //if ($.fn.DataTable.isDataTable('#detailedTable')) {
                            //    $('#detailedTable').DataTable().clear();
                            //    $('#detailedTable').DataTable().destroy();
                            //}
                            var MarketValueMovementByFundReportDates = $.map(holderLedgers, function (obj, idx) {
                                return parseJsonDateToChartDate(obj.TransDt).toString();
                            });
                            var MarketValueMovementByFundUnits = $.map(holderLedgers, function (obj, idx) {
                                return obj.TransUnits;
                            });
                            var MarketValueMovementByFundUnitsMarketValue = $.map(holderLedgers, function (obj, idx) {
                                //console.log(parseJsonDate(obj.DateOfTransaction) + " :: " + obj.IpdUniqueTransactionId + " - " + obj.TransPR + " : " + obj.ActualUnits + " - " + (obj.ActualUnits * obj.TransPR));
                                return parseFloat((obj.TransPr == 0 ? holderLedgers[idx + 1].TransUnits * obj.TransPr : obj.TransUnits * obj.TransPr).toFixed(2));
                                //sreturn parseFloat((obj.Units * maInvestDetailedByFund.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice).toFixed(2));
                            });
                            var MarketValueMovementByFundCost = $.map(holderLedgers, function (obj, idx) {
                                return obj.CostRm;
                            });
                            //Movement
                            while (lineChartMarketValueMovementByFund.series.length > 0) lineChartMarketValueMovementByFund.series[0].remove(true);
                            lineChartMarketValueMovementByFund.xAxis[0].setCategories(MarketValueMovementByFundReportDates.reverse());
                            lineChartMarketValueMovementByFundXAxisLength = MarketValueMovementByFundReportDates.length;
                            lineChartMarketValueMovementByFund.addSeries({
                                name: 'Market Value',
                                data: MarketValueMovementByFundUnitsMarketValue.reverse(),
                                legendIndex: 0,
                                index: 0,
                            });
                            lineChartMarketValueMovementByFund.addSeries({
                                name: 'Actual Cost',
                                data: MarketValueMovementByFundCost.reverse(),
                                legendIndex: 1,
                                index: 1,
                                className: dataClass
                            });
                            //$.each(utmcMemberInvestments, function (idx, u) {
                            //    var tr = $("<tr />").addClass('fs-12').appendTo($('#tbodyInvestmentDetails'));
                            //    $("<td />").html(idx + 1).appendTo(tr);
                            //    //var EffectiveDate = parseJsonDate(u.utmcMemberInvestments[0].EffectiveDate);
                            //    //$("<td />").html(EffectiveDate).appendTo(tr);
                            //    $("<td />").addClass('text-right currencyFormatNoSymbol').html(u.ActualTransferredFromEpfRm.toFixed(2)).appendTo(tr);
                            //    $("<td />").addClass('text-right currencyFormatNoSymbol').html(u.ActualCost.toFixed(2)).appendTo(tr);
                            //    $("<td />").addClass('text-right unitFormat').html(u.Units.toFixed(4)).appendTo(tr);
                            //    //$("<td />").addClass('text-right currencyFormatNoSymbol').html(u.BookValue.toFixed(2)).appendTo(tr);
                            //    $("<td />").addClass('text-right unitFormat').html(u.MarketValue.toFixed(2)).appendTo(tr);
                            //    $("<td />").addClass('text-right currencyFormatNoSymbol').html(u.RedemptionCost.toFixed(2)).appendTo(tr);
                            //    //var ReportDate = parseJsonDate(u.ReportDate);
                            //    //$("<td />").html(ReportDate).appendTo(tr);
                            //    //$("<td />").html(u.utmcMemberInvestments[0].MemberEpfNo).appendTo(tr);
                            //    //$("<td />").html(u.utmcMemberInvestments[0].EpfIpdCode).appendTo(tr);
                            //    //$("<td />").html(u.utmcMemberInvestments[0].IpdMemberAccNo).appendTo(tr);
                            //    //$("<td />").html(u.Isactive).appendTo(tr);
                            //});
                            if ($.fn.DataTable.isDataTable('#transactionTable')) {
                                $('#transactionTable').DataTable().clear();
                                $('#transactionTable').DataTable().destroy();
                            }

                            //var totalUnit = 0;

                            //$.each(utmcCompositionalTransactions, function (idx, u) {
                            //totalUnit = totalUnit + u.Units;
                            //});
                            $.each(holderLedgers, function (idx, u) {
                                //console.log(u);
                                var tr = $("<tr />").addClass('fs-12').appendTo($('#tbodyTransaction'));
                                $("<td />").html(idx + 1).appendTo(tr);
                                var DateOfTransaction = parseJsonDate(u.TransDt);
                                $("<td />").html(DateOfTransaction).appendTo(tr);
                                $("<td />").html(u.TransType).appendTo(tr);
                                $("<td />").addClass('text-right unitFormat').html((u.TransUnits != null ? u.TransUnits : 0).toFixed(4)).appendTo(tr);
                                $("<td />").addClass('text-right unitFormat').html((u.TransPr != null ? u.TransPr : 0).toFixed(4)).appendTo(tr);
                                $("<td />").addClass('text-right currencyFormatNoSymbol').html((u.TransAmt != null ? u.TransAmt : 0).toFixed(2)).appendTo(tr);
                                $("<td />").addClass('text-right unitFormat').html((u.AppFee != null ? u.AppFee : 0).toFixed(4)).appendTo(tr);
                                $("<td />").addClass('text-right currencyFormatNoSymbol').html((u.FeeAMT != null ? u.FeeAMT : 0).toFixed(2)).appendTo(tr);
                                $("<td />").addClass('text-right currencyFormatNoSymbol').html((u.GstRm != null ? u.GstRm : 0).toFixed(2)).appendTo(tr);
                                $("<td />").addClass('text-right currencyFormatNoSymbol').html((u.UnitValue != null ? u.UnitValue : 0).toFixed(2)).appendTo(tr);
                                //var costRM = (u.TransAmt - u.UnitValue);
                                //$("<td />").addClass('text-right currencyFormatNoSymbol').html(costRM.toFixed(2)).appendTo(tr);
                                $("<td />").addClass('text-right currencyFormatNoSymbol').html(u.CostRM != null ? Math.abs(u.CostRM.toFixed(2)) : 0).appendTo(tr);
                                //$("<td />").addClass('text-right currencyFormatNoSymbol').html((u.TransAmt != null ? u.TransAmt : 0).toFixed(2)).appendTo(tr);
                                //$("<td />").addClass('text-right unitFormat').html((u.HLDRAveCost != null ? u.HLDRAveCost : 0).toFixed(4)).appendTo(tr);
                                $("<td />").addClass('text-right unitFormat').html((u.CurUnitHLDG != null ? u.CurUnitHLDG : 0).toFixed(4)).appendTo(tr);
                            });

                            //$.each(utmcCompositionalTransactions, function (idx, u) {
                            //    var tr = $("<tr />").addClass('fs-12').appendTo($('#tbodyTransaction'));
                            //    $("<td />").html(idx + 1).appendTo(tr);

                            //    var DateOfTransaction = parseJsonDate(u.DateOfTransaction);
                            //    $("<td />").html(DateOfTransaction).appendTo(tr);


                            //    //$("<td />").html(u.TransType).appendTo(tr);
                            //    $("<td />").html(u.TransactionCode).appendTo(tr);
                            //    $("<td />").addClass('text-right unitFormat').html(u.Units.toFixed(4)).appendTo(tr);
                            //    $("<td />").addClass('text-right unitFormat').html(u.TransPR.toFixed(4)).appendTo(tr);
                            //    $("<td />").addClass('text-right currencyFormatNoSymbol').html(u.GrossAmountRm.toFixed(2)).appendTo(tr);
                            //    $("<td />").addClass('text-right unitFormat').html(u.SalesChargePer.toFixed(4)).appendTo(tr);
                            //    $("<td />").addClass('text-right currencyFormatNoSymbol').html(u.SalesChargeAmt != null ? u.SalesChargeAmt.toFixed(2) : '0.00').appendTo(tr);
                            //    $("<td />").addClass('text-right ' + (u.GstRm == 0 ? '' : 'currencyFormatNoSymbol')).html((u.GstRm == 0 ? "-" : u.GstRm)).appendTo(tr);
                            //    $("<td />").addClass('text-right currencyFormatNoSymbol').html(u.NetAmountRm.toFixed(2)).appendTo(tr);
                            //    $("<td />").addClass('text-right currencyFormatNoSymbol').html(u.CostRm.toFixed(2)).appendTo(tr);
                            //    $("<td />").addClass('text-right unitFormat').html(u.AverageCost.toFixed(4)).appendTo(tr);
                            //    $("<td />").addClass('text-right unitFormat').html(u.CurUnitHldg.toFixed(4)).appendTo(tr);
                            //    //$("<td />").addClass('text-right currencyFormatNoSymbol').html(u.RealisedGainLoss).appendTo(tr);

                            //    totalUnit = totalUnit - (u.Units);

                            //});
                            FormatAllUnit();
                            FormatAllCurrency();
                            $('.tableTransactionDetails').removeClass('hide');

                            //var detailedTable = $('#detailedTable').DataTable({
                            //    dom: "t<'row'<'col-md-6'i><'col-md-6 text-right'p>>",
                            //    select: true,
                            //    destroy: true,
                            //    responsive: true,
                            //    order: [],
                            //    ordering: false,
                            //    searching: false,
                            //    length: false,
                            //    autoWidth: false
                            //});

                            var transactionTable = $('#transactionTable').DataTable({
                                dom: "t<'row'<'col-md-6'i><'col-md-6 text-right'p>>",
                                select: true,
                                destroy: true,
                                //responsive: true,
                                order: [],
                                ordering: false,
                                searching: false,
                                length: false,
                                autoWidth: false
                            });
                            $('.dataTable tr td:not(:first-child)').click(function (e) {
                                $(this).parent('tr').find('td').first().click();
                            });
                            detailedTableFundName = maInvestDetailedByFund.utmcFundInformation.FundName;
                            //new $.fn.dataTable.Buttons(detailedTable, {
                            //    name: 'commands',
                            //    buttons: [
                            //        {
                            //            extend: 'print',
                            //            text: '<i class="fa fa-print"></i>',
                            //            exportOptions: {
                            //                modifier: {
                            //                    selected: null
                            //                }
                            //            },
                            //            className: 'btn my-btn',
                            //            title: detailedTableFundName
                            //        },
                            //        {
                            //            extend: 'excelHtml5',
                            //            text: '<i class="fa fa-file-excel-o"></i>',
                            //            exportOptions: {
                            //                modifier: {
                            //                    selected: null
                            //                }
                            //            },
                            //            className: 'btn my-btn',
                            //            title: detailedTableFundName
                            //        },
                            //        {
                            //            extend: 'pdfHtml5',
                            //            text: '<i class="fa fa-file-pdf-o"></i>',
                            //            exportOptions: {
                            //                modifier: {
                            //                    selected: null
                            //                }
                            //            },
                            //            className: 'btn my-btn',
                            //            title: detailedTableFundName
                            //        },
                            //        {
                            //            extend: 'copyHtml5',
                            //            text: '<i class="fa fa-copy"></i>',
                            //            exportOptions: {
                            //                modifier: {
                            //                    selected: null
                            //                }
                            //            },
                            //            className: 'btn my-btn',
                            //            title: detailedTableFundName
                            //        },
                            //        //'colvis'
                            //    ]
                            //});
                            new $.fn.dataTable.Buttons(transactionTable, {
                                name: 'commands',
                                buttons: [
                                    {
                                        extend: 'print',
                                        text: '<i class="fa fa-print"></i>',
                                        exportOptions: {
                                            modifier: {
                                                selected: null
                                            }
                                        },
                                        className: 'btn my-btn',
                                        titleAttr: "PRINT",
                                        title: function () {
                                            var fundName = $('#fundName').text();
                                            return fundName;
                                        },
                                        filename: function () {
                                            var now = new Date();
                                            var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
                                            var fundName = $('#fundName').text();
                                            return fundName + " " + jsDate;
                                        },
                                    },
                                    {
                                        extend: 'excelHtml5',
                                        text: '<i class="fa fa-file-excel-o"></i>',
                                        exportOptions: {
                                            format: {
                                                header: function (data, columnIdx) {
                                                    data = data.replace('<br>', '');
                                                    data = $.trim(data).replace(/[ ]{2,}/, ' ');
                                                    //data = data.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
                                                    return data;
                                                },
                                                //body: function ( data, row, column, node ) {
                                                //    //column: select the index of the column you want to apply the formatting
                                                //    return column === 5 ?
                                                //         data.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") :
                                                //        data;
                                                //}
                                            },
                                            modifier: {
                                                selected: null
                                            }
                                        },
                                        className: 'btn my-btn',
                                        title: "",
                                        titleAttr: "Excel",
                                        filename: function () {
                                            var now = new Date();
                                            var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
                                            var fundName = $('#fundName').text();
                                            return fundName + " " + jsDate;
                                        },
                                    },
                                    {
                                        extend: 'pdfHtml5',
                                        orientation: 'landscape',
                                        text: '<i class="fa fa-file-pdf-o"></i>',
                                        exportOptions: {
                                            modifier: {
                                                selected: null
                                            },
                                            header: false,
                                            title: function () {
                                                var fundName = $('#fundName').text();
                                                return fundName;
                                            },
                                            //format: {
                                            //    header: function (data, columnIdx) {
                                            //        var fundName = $('#fundName').text();
                                            //        return columnIdx + ': ' + data;
                                            //    }
                                            //}
                                        },
                                        className: 'btn my-btn',
                                        titleAttr: "PDF",
                                        title: "",
                                        filename: function () {
                                            var now = new Date();
                                            var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
                                            var fundName = $('#fundName').text();
                                            return fundName + " " + jsDate;
                                        },
                                        customize: function (doc) {
                                            var fundName = $('#fundName').text();
                                            doc['header'] = (function () {
                                                return {
                                                    columns: [
                                                        {
                                                            alignment: 'left',
                                                            fontSize: 16,
                                                            text: fundName,
                                                            margin: [40, 20]
                                                        },
                                                        {
                                                            alignment: 'right',
                                                            fontSize: 10,
                                                            text: 'APEX INVESTMENT SERVICES BHD',
                                                            margin: [40, 20]
                                                        }
                                                    ]
                                                }
                                            });
                                            var now = new Date();
                                            var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();

                                            doc['footer'] = (function (page, pages) {
                                                return {
                                                    columns: [
                                                        {
                                                            alignment: 'left',
                                                            text: ['Created on: ', { text: jsDate.toString() }]
                                                        },
                                                        {
                                                            alignment: 'right',
                                                            text: ['page ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                                        }
                                                    ],
                                                    margin: 20
                                                }
                                            });

                                            return doc;
                                        }
                                    },
                                    //{
                                    //    extend: 'copyHtml5',
                                    //    text: '<i class="fa fa-copy"></i>',
                                    //    exportOptions: {
                                    //        modifier: {
                                    //            selected: null
                                    //        }
                                    //    },
                                    //    className: 'btn my-btn',
                                    //    titleAttr: "COPY"
                                    //},
                                    //'colvis'
                                ]
                            });
                            //detailedTable.buttons(0, null).container().appendTo($('.tableInvestmentDetails > .row').find('.col-md-6').last());
                            transactionTable.buttons(0, null).container().appendTo($('.tableTransactionDetails > .row').find('.col-md-6').last());
                            //$('.dataTable thead tr th').removeAttr('style');
                            //$('html, body').animate({
                            //    scrollTop: $("#investmentPortfolioRow").offset().top - 90
                            //},
                            //    'slow');
                            //$('.tableTransactionDetails .paginate_button a').removeClass(dataClass);
                            //$('.tableTransactionDetails .paginate_button.active a').addClass(dataClass);
                            transactionTable.on('page', function () {
                                //setTimeout(function () {
                                //    $('.tableTransactionDetails .paginate_button a').removeClass(dataClass);
                                //    $('.tableTransactionDetails .paginate_button.active a').addClass(dataClass);
                                //}, 100);
                                FormatAllCurrency();
                                FormatAllUnit();
                            });
                            //$('.tableTransactionDetails .dt-button').removeClass(dataClass);
                            //$('.tableTransactionDetails .dt-button').addClass(dataClass);
                            $('#transactionLoading').hide();
                            $('#transactionSummaryLoading1').addClass('hide');
                            setTimeout(function () {
                                FormatAllCurrency();
                                FormatAllUnit();
                            }, 1000);
                        }, 3000);

                    });
                }
                while (chart.series.length > 0) chart.series[0].remove(true);
                var pieChartSeriesData = [];
                $.each(portfolioDetailed, function (idx, u) {
                    var totalRM = maAccountRMTotalInvestment;
                    var totalUNITS = maAccountUNITSTotalInvestment;
                    var Units = u.holderInv.CurrUnitHldg;
                    var UnitsMV = u.holderInv.CurrUnitHldg * u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice;
                    var percent = parseFloat((UnitsMV / totalRM) * 100);
                    if (percent.toFixed(2) > 0) {
                        pieChartSeriesData.push({
                            name: u.utmcFundInformation.FundCode + "<br />(" + percent.toFixed(2) + "%)",
                            y: percent,
                            className: "fund-color-" + u.utmcFundInformation.FundCode,
                        });
                    }
                });
                $('#totalRMValueLabel').html('<span class="unitFormat">' + maAccountUNITSTotalInvestment.toFixed(4) + '</span> <sup><small>UNITS</small></sup> <small>(<span class="currencyFormat">' + ConvertToCurrency(maAccountRMTotalInvestment) + '</span>)</small>');
                chart.addSeries({
                    data: pieChartSeriesData,
                    name: "Investment",
                    type: 'pie',
                    useHTML: true
                });
                setTimeout(function () {
                    $('#investmentPortfolioTbody').find('tr.details a').first().click();
                    FormatAllCurrency();
                    FormatAllUnit();
                }, 1000);
                FormatAllCurrency();
                FormatAllUnit();
            }
            function GetMAAccountUtmcMemberInvestments(HolderNo) {
                $('#maAccountRMTotalInvestmentLabel').removeAttr('data-title');
                //$('[data-toggle="tooltip"]').tooltip('disable');
                $('#maAccountRMTotalInvestmentLabel').removeAttr('data-toggle');

                $('#maAccountRMTotalInvestmentLabel').html('<strong>0.00</strong>');
                $('#unrealisedGain').html('<strong>-</strong>');
                $('#SATScoreLabel').html('<strong>-</strong>');

                if ($.fn.DataTable.isDataTable('#investmentTable')) {
                    $('#investmentTable').DataTable().clear();
                    $('#investmentTable').DataTable().destroy();
                }
                $('#transactionSummaryLoading, #transactionSummaryLoading1').removeClass('hide');
                //$('#investmentPortfolio').addClass('hide');
                //console.log('started GetMAAccountUtmcMemberInvestments' + new Date().toUTCString())
                $.ajax({
                    url: "Portfolio.aspx/GetMAAccountUtmcMemberInvestments",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { HolderNo: HolderNo },
                    success: function (data) {
                        //console.log('response GetMAAccountUtmcMemberInvestments' + new Date().toUTCString())
                        if (hdnIsMAActive == 0) {
                            //if (json.length == 0 && hdnIsMAActive == 1) {
                            $('#hdnMAToBeActivated').val(HolderNo);
                            $('#modalMAActivation').modal();
                        }
                        else {

                            $('#investmentPortfolioRow').html('');
                            $('#accountDetailsTbody').html('');
                            $('#bankDetailsTbody').html('');
                            $('#Accountbank a').removeClass('text-danger');
                            if (data.d.bankDetails == "Not Binded") {
                                $('#Accountbank').addClass('hide');
                                $('#Accountbank a').addClass('text-danger');
                                $('#bankDetailsTbody').html("<tr><td colspan='6' class='text-danger text-center'>" + data.d.bankDetails + "</td></tr>");
                            }
                            else {
                                $('#Accountbank').removeClass('hide');
                                $('#bankDetailsTbody').html(data.d.bankDetails);
                            }

                            var userAccount = data.d.userAccount;

                            if (userAccount.IsVerified == 0) {
                                $('#hdnMAToBeActivated').val(HolderNo);
                                $('#modalMAActivation').modal();
                            }
                            else {

                                var accountPlan = data.d.accounPlan;
                                var maHolderReg = data.d.maHolderReg;


                                if (userAccount.IsVerified == 0) {
                                    $('#hdnMAToBeActivated').val(HolderID);
                                    $('#modalMAActivation').modal();
                                }

                                $('#primaryAccountClass').attr('data-title', HolderNo);
                                $('#primaryAccountClass').html(accountPlan);
                                $('#maNumber').html(HolderNo);
                                $('#userAccountsList a').removeClass('active');
                                $('[data-account-no="' + HolderNo + '"] a').addClass('active');

                                $('#aAccountName').html(maHolderReg.Name1);
                                if (accountPlan == 'JOINT') {
                                    if (userAccount.IsPrinciple == 0) {
                                        $('#consolidatedSummaryNoData').addClass('hide');
                                        $('#aAccountName').html(maHolderReg.Name2 + "<div class='aAccountName-joint'>(" + maHolderReg.Name1 + ")</div>");
                                    }
                                    else {
                                        $('#consolidatedSummaryNoData').removeClass('hide');
                                        $('#aAccountName').html(maHolderReg.Name1 + "<div class='aAccountName-joint'>(" + maHolderReg.Name2 + ")</div>");
                                    }
                                }

                                var accountDetailsTbodyTR = $("<tr />");
                                var accountDetailsTbodyTH = $("<th />");
                                $(accountDetailsTbodyTH).html("Name");
                                var accountDetailsTbodyTD = $("<td />");
                                $('#Accountbank').show();
                                $('#InvestmentTab a').click();
                                var Name = "";
                                var Id = "";
                                //var Salutation = maHolderReg.Salutation;
                                if (accountPlan == "CASH") {
                                    Name = maHolderReg.Name1;
                                    Id = maHolderReg.IdNo;
                                }
                                else if (accountPlan == "EPF" || accountPlan == "CORP") {
                                    Name = maHolderReg.Name1;
                                    $('#Accountbank').hide();
                                    Id = maHolderReg.IdNo;
                                }
                                else if (accountPlan == "JOINT") {
                                    if (userAccount.IsPrinciple == 0) {
                                        Name = maHolderReg.Name2 + " <small>(Primary: " + maHolderReg.Name1 + ")</small>";
                                        Id = maHolderReg.IdNo2 + " (Primary: " + maHolderReg.IdNo + ")";
                                    }
                                    else {
                                        Name = maHolderReg.Name1 + " <small>(Secondary: " + maHolderReg.Name2 + ")</small>";
                                        Id = maHolderReg.IdNo + " (Secondary: " + maHolderReg.IdNo2 + ")";
                                    }

                                }

                                $(accountDetailsTbodyTD).html('<strong>' + Name + '</strong>');
                                $(accountDetailsTbodyTH).appendTo(accountDetailsTbodyTR);
                                $(accountDetailsTbodyTD).appendTo(accountDetailsTbodyTR);

                                accountDetailsTbodyTH = $("<th />");
                                $(accountDetailsTbodyTH).html("Registered Date");
                                accountDetailsTbodyTD = $("<td />");
                                var regDt = maHolderReg.RegDt;
                                $(accountDetailsTbodyTD).html('<strong>' + pad(regDt.split('/')[1], 2) + '/' + pad(regDt.split('/')[0], 2) + '/' + regDt.split('/')[2] + '</strong>');

                                $(accountDetailsTbodyTH).appendTo(accountDetailsTbodyTR);
                                $(accountDetailsTbodyTD).appendTo(accountDetailsTbodyTR);


                                accountDetailsTbodyTH = $("<th />");
                                $(accountDetailsTbodyTH).html("ID No");
                                accountDetailsTbodyTD = $("<td />");
                                $(accountDetailsTbodyTD).html('<strong>' + Id + '</strong>');

                                $(accountDetailsTbodyTH).appendTo(accountDetailsTbodyTR);
                                $(accountDetailsTbodyTD).appendTo(accountDetailsTbodyTR);

                                $(accountDetailsTbodyTR).appendTo($('#accountDetailsTbody'));

                                var accountDetailsTbodyTR = $("<tr />");

                                accountDetailsTbodyTH = $("<th />");
                                $(accountDetailsTbodyTH).html("Old IC");
                                accountDetailsTbodyTD = $("<td />");
                                $(accountDetailsTbodyTD).html('<strong>' + (accountPlan == "CASH" || accountPlan == "EPF" || accountPlan == "CORP" ? (maHolderReg.IdNoOld == null ? "-" : maHolderReg.IdNoOld) : (accountPlan == "JOINT" ? (userAccount.IsPrinciple == 0 ? (maHolderReg.IdNoOld2 == null ? "-" : maHolderReg.IdNoOld2) : (maHolderReg.IdNoOld == null ? "-" : maHolderReg.IdNoOld)) : "")) + '</strong>');
                                $(accountDetailsTbodyTH).appendTo(accountDetailsTbodyTR);
                                $(accountDetailsTbodyTD).appendTo(accountDetailsTbodyTR);


                                accountDetailsTbodyTH = $("<th />");
                                $(accountDetailsTbodyTH).html("Birth Date");
                                accountDetailsTbodyTD = $("<td />");
                                var BirthDt = maHolderReg.BirthDt;
                                var JointBirthDt = maHolderReg.JointBirthDt;
                                var bd = '-';
                                if (BirthDt != "") {
                                    bd = pad(BirthDt.split('/')[1], 2) + '/' + pad(BirthDt.split('/')[0], 2) + '/' + BirthDt.split('/')[2];
                                }
                                var jbd = '-';
                                if (JointBirthDt != "") {
                                    jbd = pad(JointBirthDt.split('/')[1], 2) + '/' + pad(JointBirthDt.split('/')[0], 2) + '/' + JointBirthDt.split('/')[2];
                                }

                                var today = new Date();
                                var currentYear = parseInt(today.getFullYear().toString().substr(0, 2));
                                var IdYear = parseInt(Id.toString().substr(0, 2));
                                var YearA = (currentYear).toString();
                                var YearB = (currentYear - 1).toString();

                                $(accountDetailsTbodyTD).html('<strong>' + (accountPlan == "CORP" ? "-" : (accountPlan == "CASH" || accountPlan == "EPF" ? (bd != "" || bd != null ? bd : Id.toString().substr(4, 2) + "/" + Id.toString().substr(2, 2) + "/" + (IdYear > currentYear ? YearB : YearA) + Id.toString().substr(0, 2)) : (accountPlan == "JOINT" ? (userAccount.IsPrinciple == 0 ? (jbd != "-" ? jbd : Id.toString().substr(4, 2) + "/" + Id.toString().substr(2, 2) + "/" + (IdYear > currentYear ? YearB : YearA) + Id.toString().substr(0, 2)) : (bd != "" || bd != null ? bd : Id.toString().substr(4, 2) + "/" + Id.toString().substr(2, 2) + "/" + (IdYear > currentYear ? YearB : YearA) + Id.toString().substr(0, 2))) : ""))) + '</strong>');

                                $(accountDetailsTbodyTH).appendTo(accountDetailsTbodyTR);
                                $(accountDetailsTbodyTD).appendTo(accountDetailsTbodyTR);


                                accountDetailsTbodyTH = $("<th />");
                                $(accountDetailsTbodyTH).html("Telephone No");
                                accountDetailsTbodyTD = $("<td />");
                                $(accountDetailsTbodyTD).html('<strong>' + (maHolderReg.TelNo == null ? "-" : maHolderReg.TelNo) + '</strong>');

                                $(accountDetailsTbodyTH).appendTo(accountDetailsTbodyTR);
                                $(accountDetailsTbodyTD).appendTo(accountDetailsTbodyTR);

                                $(accountDetailsTbodyTR).appendTo($('#accountDetailsTbody'));

                                var accountDetailsTbodyTR = $("<tr />");

                                accountDetailsTbodyTH = $("<th />");
                                $(accountDetailsTbodyTH).html("Handphone No");
                                accountDetailsTbodyTD = $("<td />");
                                var HandPhone = maHolderReg.HandPhoneNo;
                                var JointHandPhone = maHolderReg.JointTelNo;

                                var hp = '-';
                                if (HandPhone != "") {
                                    hp = HandPhone;
                                }
                                var jhp = '-';
                                if (JointHandPhone != "" || JointHandPhone != null) {
                                    jhp = maHolderReg.JointTelNo;
                                }

                                $(accountDetailsTbodyTD).html('<strong>' + (accountPlan == "CORP" ? "" : (accountPlan == "CASH" || accountPlan == "EPF" ? hp : (accountPlan == "JOINT" ? (userAccount.IsPrinciple == 0 ? jhp : hp) : ""))) + '</strong>');

                                $(accountDetailsTbodyTH).appendTo(accountDetailsTbodyTR);
                                $(accountDetailsTbodyTD).appendTo(accountDetailsTbodyTR);

                                accountDetailsTbodyTH = $("<th />");
                                $(accountDetailsTbodyTH).html("Gender");
                                accountDetailsTbodyTD = $("<td />");
                                $(accountDetailsTbodyTD).html('<strong>' + (accountPlan == "CORP" ? "" : (accountPlan == "CASH" || accountPlan == "EPF" ? (maHolderReg.Sex == "M" ? "Male" : (maHolderReg.Sex == "F" ? "Female" : "-")) : (accountPlan == "JOINT" ? (userAccount.IsPrinciple == 0 ? (maHolderReg.JointSex == "M" ? "Male" : (maHolderReg.JointSex == "F" ? "Female" : "-")) : (maHolderReg.Sex == "M" ? "Male" : (maHolderReg.Sex == "F" ? "Female" : "-"))) : ""))) + '</strong>');

                                $(accountDetailsTbodyTH).appendTo(accountDetailsTbodyTR);
                                $(accountDetailsTbodyTD).appendTo(accountDetailsTbodyTR);

                                accountDetailsTbodyTH = $("<th />");
                                $(accountDetailsTbodyTH).html("No Of Dependants");
                                accountDetailsTbodyTD = $("<td />");
                                $(accountDetailsTbodyTD).html('<strong>' + (maHolderReg.NoOfDpndnt == 0 ? "-" : maHolderReg.NoOfDpndnt) + '</strong>');

                                $(accountDetailsTbodyTH).appendTo(accountDetailsTbodyTR);
                                $(accountDetailsTbodyTD).appendTo(accountDetailsTbodyTR);


                                $(accountDetailsTbodyTR).appendTo($('#accountDetailsTbody'));



                                $('#smallAccountPlan').html(accountPlan);

                                $('#holderPlan').html(" " + accountPlan + " SCHEME");
                                if (accountPlan == "EPF") {
                                    $('#EpfTypeHead').html("EPF TYPE");
                                    $('#holderType').html("- " + (maHolderReg.EpfIStatus == 'I' ? "Islamic" : "Conventional"));
                                    $('#epfType').html(maHolderReg.EpfIStatus == 'I' ? "Islamic" : "Conventional");
                                }
                                else {
                                    $('#EpfTypeHead').html("CASH ACCOUNT");
                                    if (accountPlan == "JOINT") {
                                        $('#EpfTypeHead').html("JOINT ACCOUNT");
                                    }

                                    $('#holderType').html('');
                                    $('#epfType').html("");
                                }
                                if (data.d.utmcDetailedMemberInvestments.length == 0) {
                                        $('#consolidatedSummaryNoData').removeClass('hide');
                                        $('[href="#AccountDetails"]').click();
                                    $('.loadingDiv').addClass('hide');
                                    $('#pieChart .highcharts-container').addClass('hide');
                                    $('#pieChart').css({ 'height': '300px' });
                                    if ($('#pieChart .no-data-div') != undefined)
                                        $('#pieChart .no-data-div').remove();
                                    $('#pieChart').append('<div class="no-data-div text-center">No data</div>');
                                    if ($('#investmentPortfolioTbody .no-data-div') != undefined)
                                        $('#investmentPortfolioTbody .no-data-div').remove();
                                    $('#investmentPortfolioTbody').append('<tr class="no-data-div"><td colspan="9" class="text-center">No data</td></tr>');
                                    $('#investmentPortfolio').hide();
                                    if ($.fn.DataTable.isDataTable('#transactionTable')) {
                                        $('#transactionTable').DataTable().clear();
                                        $('#transactionTable').DataTable().destroy();
                                    }
                                    //$('#consolidatedFundSummaryByAccount > div:not(:first)').addClass('hide');
                                    //$('.no-data-available').removeClass('hide');
                                    $('#portfolioAccountsMenu a, .portfolio-btn').removeClass("disabled");
                                }
                                else {
                                    $('#consolidatedSummaryNoData').addClass('hide');
                                    $('#investmentPortfolio').show();
                                    $('.no-data-div').remove();
                                    $('#pieChart .highcharts-container').removeClass('hide');
                                    $('#pieChart').css({ 'height': 'auto' });
                                    $('.no-data-available').addClass('hide');
                                    $('#consolidatedFundSummaryByAccount > div:not(:first)').removeClass('hide');
                                    var json = data.d.utmcDetailedMemberInvestments;
                                    //var jsonForChart = data.d.utmcDetailedMemberInvestmentsForChart;
                                    var UnrealisedGain = data.d.UnrealisedGain;
                                    var UnrealisedGainPer = data.d.UnrealisedGainPer;
                                    var totalInvestmentUNITS = data.d.totalInvestmentUNITS;
                                    var totalInvestmentRM = data.d.totalInvestmentRM;
                                    $('#unrealisedGain').removeClass('text-danger');
                                    $('#unrealisedGain').removeClass('text-success');
                                    if (UnrealisedGainPer < 0) {
                                        $('#unrealisedGain').addClass('text-danger');
                                    }
                                    else if (UnrealisedGainPer >= 0) {
                                        $('#unrealisedGain').addClass('text-success');
                                    }
                                    $('#unrealisedGain').html('<strong>MYR </strong><strong class="currencyFormatNoSymbol">' + UnrealisedGain.toFixed(2) + '</strong> (' + UnrealisedGainPer.toFixed(2) + ' %)');


                                    portfolioDetailed = json;
                                    //portfolioDetailedForChart = jsonForChart;
                                    //$('.maHolderRegNoLabel').html(HolderNo);


                                    //$('investmentPortfolioTbody').html('');
                                    maAccountRMTotalInvestment = totalInvestmentRM;
                                    maAccountUNITSTotalInvestment = totalInvestmentUNITS;
                                    var fundInvests = "<table class='table table-condensed table-bordered table-cell-pad-5 mb-0'><tbody>";
                                    
                                    $.each(json, function (idx, u) {
                                        if (u.holderInv.CurrUnitHldg > 0) {
                                            var commaNum = numberWithCommas(u.holderInv.CurrUnitHldg.toFixed(4));
                                            fundInvests += "<tr><td class='text-left'>" + u.utmcFundInformation.FundCode + "</td><td class='text-right'>" + commaNum + "</td></tr>";
                                        }
                                        //index = idx + 1
                                        //maAccountRMTotalInvestment += parseFloat((u.utmcMemberInvestments[0].Units * u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice).toFixed(2));
                                        //maAccountUNITSTotalInvestment += parseFloat(u.utmcMemberInvestments[0].Units.toFixed(4));
                                        //NEW
                                        //if (u.holderInv.CurrUnitHldg > 0) {
                                        //var ReportDate = parseJsonDate(u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavDate);
                                        //var div = $("<div />").addClass("col-lg-4 col-md-6 col-sm-6").appendTo($('#investmentPortfolioRow'));
                                        //var divFundBoxWrapper = $('<div />').addClass('fund-box-wrapper').appendTo(div);
                                        //var divFundBox = $('<div />').addClass('fund-box fund-color-' + u.utmcFundInformation.FundCode).appendTo(divFundBoxWrapper);
                                        //var divFundNavPU = $('<div />').addClass('fund-navpu').html('NAV MYR: ' + u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice.toFixed(4)).appendTo(divFundBox);
                                        //var divFundName = $('<div data-toggle="tooltip" title="' + u.utmcFundInformation.FundName + '" />').addClass('fund-name line-clamp2').html(u.utmcFundInformation.FundName).appendTo(divFundBox);
                                        //var divFundRM = $('<div />').addClass('fund-rm').html('<strong class="currencyFormatNoSymbol">' + (u.holderInv.CurrUnitHldg * u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice).toFixed(2) + '</strong> <sup>MYR </sup>').appendTo(divFundBox);
                                        //var spanFundUnits = $('<div />').addClass('fund-units').html('<strong class="unitFormat">' + u.holderInv.CurrUnitHldg.toFixed(4) + '</strong> <sup>UNITS</sup>').appendTo(divFundBox);
                                        //var span = $('<span />').addClass('fund-date').html('<small><small>AS AT</small></small> <strong>' + ReportDate + '</strong>').appendTo(divFundBox);
                                        //var btnGroup = $('<div />').addClass('btn-group-justified').appendTo(divFundBox);
                                        //var a = $('<a title="See Details" href="javascript:;" data-class="fund-color-' + u.utmcFundInformation.FundCode + '" data-value="' + u.utmcFundInformation.IpdFundCode + '"/>').addClass('btn btn-sm btn-default details').html('Details').appendTo(btnGroup);
                                        //var a = $('<a title="See Movement" href="javascript:;" data-class="fund-color-' + u.utmcFundInformation.FundCode + '" data-value="' + u.utmcFundInformation.IpdFundCode + '"/>').addClass('btn btn-sm btn-default movement').html('Movement').appendTo(btnGroup);
                                        //if (userAccount.IsPrinciple == 1) {
                                        //var a = $('<a title="Buy Now" href="/BuyFunds.aspx?fundCode=' + u.utmcFundInformation.FundCode + '&AccountNo=' + HolderNo + '" data-value="' + u.utmcFundInformation.IpdFundCode + '"/>').addClass('btn btn-sm btn-default').html('Buy Now').appendTo(btnGroup);
                                        //}
                                        //}
                                        //NEW

                                        //NEW
                                        var ac = 0;
                                        if (u.holderInv.CurrUnitHldg > 0) {
                                            var tr = $("<tr data-class='fund-color-" + u.utmcFundInformation.FundCode + "' data-value='" + u.utmcFundInformation.IpdFundCode + "' />").addClass("fs-12 details").appendTo($('#investmentPortfolioTbody'));
                                            var td = $("<td />").addClass("fs-12").html(idx + 1).appendTo(tr);
                                            var td = $("<td />").addClass("fs-12").html('<a href="javascript:;">' + u.utmcFundInformation.FundName + '</a>').appendTo(tr);
                                            var distributionInstruction = u.holderInv.DistributionInsString;
                                            var td = $("<td />").addClass("fs-12").html(distributionInstruction).appendTo(tr);
                                            var units = u.holderInv.CurrUnitHldg.toFixed(4);
                                            var afterDecimal = units.substr(units.length - 4);
                                            var beforeDecimal = units.substr(0, units.length - 4);
                                            var td = $("<td />").addClass("fs-12 text-right").html('<span class="unitFormatNoDecimal">' + beforeDecimal + '</span>.<small>' + afterDecimal + '</small>').appendTo(tr);
                                            //u.utmcCompositionalTransactions.forEach(function (item) {
                                            //    //ac += item.CostRm;
                                            //    console.log(item.ReportDate + " : " + item.CostRm);
                                            //    ac = item.CostRm;
                                            //});
                                            ac = u.utmcCompositionalTransactions[0].ActualCost;
                                            var td = $("<td />").addClass("fs-12 text-right currencyFormatNoSymbol").html(ConvertToCurrency(ac)).appendTo(tr);
                                            var avgCost = u.utmcCompositionalTransactions[0].ActualCost / u.holderInv.CurrUnitHldg;
                                            var td = $("<td />").addClass("fs-12 text-right unitFormat").html(avgCost).appendTo(tr);
                                            //var td = $("<td />").addClass("fs-12 text-right currencyFormatNoSymbol").html(u.utmcMemberInvestments[0].BookValue.toFixed(4)).appendTo(tr);
                                            var td = $("<td />").addClass("fs-12 text-right currencyFormatNoSymbol").html((u.holderInv.CurrUnitHldg * u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice)).appendTo(tr);
                                            var td = $("<td />").addClass("fs-12 text-right").html("<span data-toggle='tooltip' title='" + parseJsonDate(u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavDate) + "' class='unitFormat'>" + u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice + "</span>").appendTo(tr);
                                        }
                                        var mv = (u.holderInv.CurrUnitHldg * u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice);
                                        var rgl = (mv - ac);
                                        var rglPer = ((mv - ac) / ac) * 100;
                                        var td =
                                            $("<td />")
                                                .addClass("fs-12 text-right " + (rgl >= 0 ? "text-success" : "text-danger"))
                                                .html("<span class='currencyFormatNoSymbol'>" + rgl + "</span>")
                                                .appendTo(tr);
                                        var td =
                                            $("<td />")
                                                .addClass("fs-12 text-right " + (rgl >= 0 ? "text-success" : "text-danger"))
                                                .html("<span class='currencyFormatNoSymbol'>" + rglPer + "</span>")
                                                .appendTo(tr);
                                        //if ((u.holderInv.CurrUnitHldg * u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice) >= (u.utmcMemberInvestments[0].ActualCost)) {
                                        //    //gain
                                        //    var td = $("<td />").addClass("fs-12 text-right text-success currencyFormatNoSymbol").html(
                                        //        (
                                        //            (
                                        //                ((u.utmcMemberInvestments[0].Units * u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice) - (ac)) / (ac)
                                        //            ) * 100
                                        //        ).toFixed(2)
                                        //    ).appendTo(tr);
                                        //} else {
                                        //    //loss
                                        //    var td = $("<td />").addClass("fs-12 text-right text-danger currencyFormatNoSymbol").html(
                                        //        (
                                        //            (
                                        //                ((u.utmcMemberInvestments[0].Units * u.utmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].LatestNavPrice) - (ac)) / (ac)
                                        //            ) * 100
                                        //        ).toFixed(2)
                                        //    ).appendTo(tr);
                                        //}
                                        var td = $("<td />").addClass("fs-12 text-right").appendTo(tr);
                                        if (userAccount.IsPrinciple == 1 && accountPlan != "EPF" && accountPlan != "CORP" && u.utmcFundInformation.Status == "Active") {
                                            var url = "/BuyFunds.aspx?fundCode=" + u.utmcFundInformation.FundCode + "&AccountNo=" + HolderNo;
                                            //var isPopup = 1;
                                            //var tcAcceptBuySession = sessionStorage['tcAcceptBuy'];
                                            //if (tcAcceptBuySession != undefined && tcAcceptBuySession != null && tcAcceptBuySession != "") {
                                            //    isPopup = 0;
                                            //    url += "&isTermsModalPopup=0";
                                            //}

                                            var a = $('<a href="' + url + '" />').addClass('btn btn-sm btn-primary').html('Buy Now').appendTo(td);
                                        }

                                        //NEW
                                    });
                                    var maAccountUNITSTotalInvestmentUNITS = maAccountUNITSTotalInvestment.toFixed(4);


                                    fundInvests += "</tbody></table>";
                                    $('#maAccountRMTotalInvestmentLabel').attr('data-title', fundInvests);
                                    $('#maAccountRMTotalInvestmentLabel').attr('data-toggle', 'tooltip');
                                    $('[data-toggle="tooltip"]').tooltip({
                                        html: true
                                    });


                                    var afterDecimal = maAccountUNITSTotalInvestmentUNITS.substr(maAccountUNITSTotalInvestmentUNITS.length - 4);
                                    var beforeDecimal = maAccountUNITSTotalInvestmentUNITS.substr(0, maAccountUNITSTotalInvestmentUNITS.length - 4);
                                    $('#maAccountRMTotalInvestmentLabel').html('<strong><span class="currencyFormat">' + ConvertToCurrency(maAccountRMTotalInvestment) + '</span></strong> (<span class="unitFormatNoDecimal">' + beforeDecimal + '</span><small><small>.' + afterDecimal + '</small></small> <sup><small>UNITS</small></sup> <small>)</small>');
                                    //$('#maAccountRMTotalInvestmentLabel').html('<span class="unitFormatNoDecimal">' + beforeDecimal + '</span><small><small>.' + afterDecimal + '</small></small> <sup><small>UNITS</small></sup> <small>(<strong><span class="currencyFormat">' + ConvertToCurrency(maAccountRMTotalInvestment) + '</span></strong>)</small>');
                                    //$('#maAccountRMTotalInvestmentLabel').html('<span class="unitFormat">' + maAccountUNITSTotalInvestment.toFixed(4) + '</span> <sup><small>UNITS</small></sup> <small>(<strong><span class="currencyFormat">' + ConvertToCurrency(maAccountRMTotalInvestment) + '</span></strong>)</small>');
                                    //$('#fundsCount').html('<strong>' + json.length + '</strong>');
                                    if (userAccount.IsPrinciple == 1) {
                                        if (accountPlan == "CORP") {
                                            $('#divSAT').hide();
                                        }
                                        else {
                                            $('#divSAT').show();
                                            if (userAccount.SatScore == 0)
                                                $('#SATScoreLabel').html("<strong></strong><sup><a data-toggle='tooltip' title='Update SAT' href='/SAT-Form.aspx?redirectUrl=Portfolio.aspx&AccountNo=" + userAccount.AccountNo + "'>Update Now</a></sup>");
                                            else
                                                $('#SATScoreLabel').html("<strong> " + maHolderReg.FieldDesc1 + " </strong> - " + userAccount.SatScore + " <sup><br/>(<a data-toggle='tooltip' title='View' href='/SAT-Form.aspx?redirectUrl=Portfolio.aspx&AccountNo=" + userAccount.AccountNo + "'>Last Updated: " + parseJsonDate(userAccount.SatUpdatedDate) + "</a>)</sup>");
                                        }
                                    }
                                    else {
                                        $('#divSAT').hide();
                                    }
                                    //$('#investmentPortfolio').removeClass('hide');
                                    $('a[data-toggle="tooltip"]').tooltip({ html: true });
                                    //$('.dataTable thead tr th').removeAttr('style');
                                    $('#transactionSummaryLoading, #transactionSummaryLoading1').addClass('hide');

                                    var investmentTable = $('#investmentTable').DataTable({
                                        dom: "t<'row'<'col-md-6'i><'col-md-6 text-right'p>>",
                                        //select: true,
                                        select: {
                                            info: false
                                        },
                                        destroy: true,
                                        //responsive: true,
                                        order: [],
                                        ordering: false,
                                        searching: false,
                                        length: false,
                                        autoWidth: false,
                                        paging: false,
                                        info: false
                                    });
                                    if ($.fn.DataTable.isDataTable('#transactionTable')) {
                                        $('#transactionTable').DataTable().clear();
                                        $('#transactionTable').DataTable().destroy();
                                    }
                                    while (lineChartMarketValueMovementByFund.series.length > 0) lineChartMarketValueMovementByFund.series[0].remove(true);
                                    FormatAllCurrency();
                                    FormatAllUnit();
                                    ApplyScripts();
                                }
                            }

                            //$('#tblBank').html(data.d.BankDetails);
                            //$('#divMaAddress').html(data.d.MAAddress);
                            //$('#divHardCopy').html(data.d.HardCopyStatus);

                        }
                        $('[data-toggle="tooltip"]').tooltip({ html: true });
                        $('#portfolioAccountsMenu a, .portfolio-btn').removeClass("disabled");
                        //console.log('ended GetMAAccountUtmcMemberInvestments' + new Date().toUTCString())
                    },
                    error: function () {
                        $('#portfolioAccountsMenu a, .portfolio-btn').removeClass("disabled");
                    }
                });
            }

            function numberWithCommas(number) {
                var parts = number.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return parts.join(".");
            }

            $(document).ready(function () {
                //CHART
                //var stackedBarChartInvestmentPerformance = Highcharts.chart('stackedBarChartInvestmentPerformance', {
                //    chart: {
                //        height: '300px',
                //        type: 'column',
                //        events:
                //        {
                //            addSeries: function () {
                //                $('.stackedBarChartInvestmentPerformanceloadingChartDiv').fadeOut();
                //                var label = this.renderer.label('A series was added', 30, 30)
                //                    .attr({
                //                        fill: Highcharts.getOptions().colors[0],
                //                        padding: 10,
                //                        zIndex: 8
                //                    })
                //                    .css({
                //                        color: '#FFFFFF'
                //                    })
                //                    .add();

                //                setTimeout(function () {
                //                    label.fadeOut();
                //                }, 1000);
                //            },
                //            load: function () {
                //            }
                //        },
                //        borderColor: '#000000',
                //        borderWidth: 0,
                //        inverted: false,
                //        zoomType: 'x',
                //        spacingRight: 20
                //    },
                //    title: {
                //        useHTML: true,
                //        text: '<p style="font-size:14px; margin-bottom:5px; color:#aaaaaa;">Investment Performance</p>'
                //    },
                //    subtitle: {
                //        useHTML: true,
                //        text: (document.ontouchstart === undefined ?
                //            'Click and drag in the plot area to zoom in' :
                //            '<div class="pinchZoom">Pinch the chart to zoom in</div>')
                //    },
                //    xAxis: {
                //        allowDecimals: true,
                //        labels: {
                //            showLastLabel: true,
                //            endOnTick: true,
                //            x: -10,
                //            formatter: function () {
                //                var x = this.value;
                //                return Highcharts.dateFormat("%Y", new Date(x));
                //                //return Highcharts.dateFormat('%d/%m/%Y', new Date(x));
                //            },
                //            style: {
                //                fontSize: '10px'
                //            },
                //            padding: 2,
                //            distance: 5
                //        },
                //        categories: []
                //    },
                //    //xAxis: {
                //    //    //categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
                //    //},
                //    yAxis: {
                //        endOnTick: false,
                //        allowDecimals: true,
                //        title: {
                //            text: 'MYR'
                //        }
                //    },
                //    tooltip: {
                //        shared: true,
                //        pointFormat: '{series.name}: <b>{point.y} </b><br />',
                //        followPointer: true,
                //        crosshairs: [true, true],
                //        crosshairs: {
                //            color: 'rgba(0,0,0,0.3)',
                //            dashStyle: 'Solid',
                //            width: 2
                //        },
                //        formatter: function () {
                //            var s = [];
                //            var x;
                //            var l = (this.points).length;
                //            $.each(this.points, function (i, point) {
                //                var seriesName = point.series.name;
                //                var y = point.y;
                //                x = point.x;

                //                //var str = y.toString().split('.');
                //                //if (str[0].length >= 4) {
                //                //    str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
                //                //}

                //                //Highcharts.dateFormat('%b %Y', x)
                //                if (i == 0) {
                //                    s.push('<h6 style="text-decoration:underline;">Date: ' + Highcharts.dateFormat("%Y", new Date(x)) + '</h6><br/><span style="color:' + point.color + '">' + seriesName + ': ' + (y.toFixed(2)) + ' </span>');
                //                }
                //                else {
                //                    s.push('<span style="color:' + point.color + '">' + seriesName + ': ' + (y.toFixed(2)) + ' </span>');
                //                }
                //            });


                //            return s.join('<br />');
                //        }
                //    },
                //    credits: {
                //        href: '/Index.aspx',
                //        text: 'eapexis.apexis.com.my',
                //        position: {
                //            align: 'right',
                //            verticalAlign: 'bottom',
                //        }
                //    },
                //    plotOptions: {
                //        column: {
                //            //stacking: 'normal',
                //            //dataLabels: {
                //            //    enabled: true,
                //            //    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                //            //}
                //        },
                //        //line: {
                //        //    marker: {
                //        //        enabled: false,
                //        //        symbol: 'circle',
                //        //        radius: 2,
                //        //        states: {
                //        //            hover: {
                //        //                enabled: true
                //        //            },
                //        //        }
                //        //    }
                //        //}
                //    },
                //    //series: [{
                //    //    name: 'John',
                //    //    data: [5, 3, 4, 7, 2]
                //    //}, {
                //    //    name: 'Jane',
                //    //    data: [2, 2, 3, 2, 1]
                //    //}, {
                //    //    name: 'Joe',
                //    //    data: [3, 4, 4, 2, 5]
                //    //}],
                //    responsive: {
                //        rules: [{
                //            condition: {
                //                maxWidth: 500
                //            },
                //            chartOptions: {
                //                legend: {
                //                    align: 'center',
                //                    verticalAlign: 'bottom',
                //                    layout: 'horizontal'
                //                },
                //                yAxis: {
                //                    labels: {
                //                        align: 'left',
                //                        x: 0,
                //                        y: -5
                //                    },
                //                    title: {
                //                        text: null
                //                    }
                //                },
                //                subtitle: {
                //                    text: null
                //                },
                //                credits: {
                //                    enabled: false
                //                }
                //            }
                //        }],
                //    }
                //});

                var lineChartPortfolioMarketValueMovement = Highcharts.chart('lineChartPortfolioMarketValueMovement', {
                    chart: {
                        resetZoomButton: {
                            position: {
                                // align: 'right', // by default
                                // verticalAlign: 'top', // by default
                                x: 0,
                                y: -80
                            }
                        },
                        height: '300px',
                        type: 'line',
                        events:
                        {
                            addSeries: function () {
                                $('.lineChartPortfolioMarketValueMovementloadingChartDiv').fadeOut();
                                var label = this.renderer.label('A series was added', 30, 30)
                                    .attr({
                                        fill: Highcharts.getOptions().colors[0],
                                        padding: 10,
                                        zIndex: 8
                                    })
                                    .css({
                                        color: '#FFFFFF'
                                    })
                                    .add();

                                setTimeout(function () {
                                    label.fadeOut();
                                }, 1000);
                            },
                            load: function () {
                            }
                        },
                        borderColor: '#000000',
                        borderWidth: 0,
                        inverted: false,
                        zoomType: 'x',
                        spacingRight: 20,
                        style: {
                            fontFamily: "'Montserrat', sans-serif"
                        }
                    },
                    title: {
                        useHTML: true,
                        text: '<p style="font-size:14px; margin-bottom:5px; color:#aaaaaa;">Based on Market Value </p>'
                    },
                    subtitle: {
                        useHTML: true,
                        text: (document.ontouchstart === undefined ?
                            'Click and drag in the plot area to zoom in' :
                            '<div class="pinchZoom">Pinch the chart to zoom in</div>')
                    },
                    xAxis: {
                        allowDecimals: true,
                        labels: {
                            rotation: -45,
                            step: 30,
                            showLastLabel: true,
                            endOnTick: true,
                            x: -10,
                            formatter: function () {
                                var x = this.value;
                                //if (lineChartXAxisLength >= 12) {
                                //    lineChartPortfolioMarketValueMovement.xAxis[0].options.labels.step = 6;
                                //    return Highcharts.dateFormat("%b '%y", new Date(x));
                                //    //return Highcharts.dateFormat('%b %Y', new Date(x));
                                //}
                                //else if (lineChartXAxisLength >= 12 && lineChartXAxisLength <= 100) {
                                //    lineChartPortfolioMarketValueMovement.xAxis[0].options.labels.step = 14;
                                //    return Highcharts.dateFormat("%b '%y", new Date(x));
                                //    //return Highcharts.dateFormat('%e. %b %Y', new Date(navDate));
                                //}
                                lineChartPortfolioMarketValueMovement.xAxis[0].options.labels.step = 1;
                                //return Highcharts.dateFormat("%b %Y", new Date(x));
                                //return Highcharts.dateFormat("%b '%y", new Date(x));
                                return Highcharts.dateFormat("%Y", new Date(x));
                                //return Highcharts.dateFormat('%d/%m/%Y', new Date(x));
                                //return Highcharts.dateFormat('%b %Y', new Date(x));
                            },
                            style: {
                                fontSize: '10px'
                            },
                            padding: 2,
                            distance: 5
                        },
                        categories: []
                    },
                    yAxis: {
                        endOnTick: false,
                        allowDecimals: true,
                        title: {
                            text: ''
                        }
                    },
                    tooltip: {
                        shared: true,
                        pointFormat: '{series.name} produced <b>{point.y} </b><br/>warheads in {point.x}',
                        formatter: function () {
                            var s = [];
                            $.each(this.points, function (i, point) {
                                var seriesName = point.series.name;
                                var y = point.y;
                                var x = point.x;

                                var str = y.toString().split('.');
                                //console.log(str[0]);
                                //console.log(str[1]);
                                if (str[0].length >= 4) {
                                    str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
                                }

                                //s.push('<span style="text-decoration:underline;">Date: ' + Highcharts.dateFormat('%d/%m/%Y', new Date(x)) + '</span><br/><span style="color:' + point.color + '">Realized Profit: ' + (str.join('.')) + ' </span>');
                                s.push('<span style="text-decoration:underline;">Date: ' + Highcharts.dateFormat('%Y', new Date(x)) + '</span><br/><span style="color:' + point.color + '">Realized Profit: ' + (str.join('.')) + ' </span>');
                            });

                            return s.join('<br />');
                        },
                        followPointer: true,
                        //positioner: { x: 100, y: 100 },
                        crosshairs: [true, true],
                        crosshairs: {
                            color: 'rgba(0,0,0,0.3)',
                            dashStyle: 'Solid',
                            width: 2
                        },
                    },
                    credits: {
                        href: '/Index.aspx',
                        text: 'eapexis.apexis.com.my',
                        position: {
                            align: 'right',
                            verticalAlign: 'bottom',
                        }
                    },
                    plotOptions: {
                        line: {
                            marker: {
                                enabled: false,
                                symbol: 'circle',
                                radius: 2,
                                states: {
                                    hover: {
                                        enabled: true
                                    },
                                }
                            }
                        }
                    },
                    series: [
                        //{
                        //    name: 'Nav Price',
                        //    data: data11
                        //},
                        //{
                        //    name: 'Nav',
                        //    data: [null, null, null, null, null, null, null, null, null, null,
                        //        5, 25, 50, 120, 150, 200, 426, 660, 869, 1060, 1605, 2471, 3322,
                        //        4238, 5221, 6129, 7089, 8339, 9399, 10538, 11643, 13092, 14478,
                        //        15915, 17385, 19055, 21205, 23044, 25393, 27935, 30062, 32049,
                        //        33952, 35804, 37431, 39197, 45000, 43000, 41000, 39000, 37000,
                        //        35000, 33000, 31000, 29000, 27000, 25000, 24000, 23000, 22000,
                        //        21000, 20000, 19000, 18000, 18000, 17000, 16000]
                        //}
                    ],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal'
                                },
                                yAxis: {
                                    labels: {
                                        align: 'left',
                                        x: 0,
                                        y: -5
                                    },
                                    title: {
                                        text: null
                                    }
                                },

                                subtitle: {
                                    text: null
                                },
                                credits: {
                                    enabled: false
                                },
                            }
                        }],
                    }
                });

                var pieChartPortfolioAllocation = Highcharts.chart('pieChartPortfolioAllocation', {
                    chart: {
                        resetZoomButton: {
                            position: {
                                // align: 'right', // by default
                                // verticalAlign: 'top', // by default
                                x: 0,
                                y: -80
                            }
                        },
                        height: '300px',
                        type: 'pie',
                        className: 'fund-bg-color',
                        //options3d: {
                        //    enabled: true,
                        //    alpha: 60,
                        //    beta: 0
                        //},
                        events: {
                            addSeries: function () {
                                $('.pieChartPortfolioAllocationloadingChartDiv').fadeOut();
                                var label = this.renderer.label('A series was added', 30, 30)
                                    .attr({
                                        fill: Highcharts.getOptions().colors[0],
                                        padding: 10,
                                        zIndex: 8
                                    })
                                    .css({
                                        color: '#FFFFFF'
                                    })
                                    .add();

                                setTimeout(function () {
                                    label.fadeOut();
                                }, 1000);
                            },
                            load: function () {
                            }
                        },
                        style: {
                            fontFamily: "'Montserrat', sans-serif"
                        }
                    },
                    title: {
                        useHTML: true,
                        text: ''
                        //text: '<p class="fs-12">Based on Investment</p>'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>',
                        followPointer: true,
                    },
                    credits: {
                        href: '/Index.aspx',
                        text: 'eapexis.apexis.com.my',
                        position: {
                            align: 'right',
                            verticalAlign: 'bottom',
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 25,
                            dataLabels: {
                                enabled: true,
                                //format: '{y} %',
                                formatter: function () {
                                    var y = this.y;
                                    return y.toFixed(2) + ' %';
                                },
                                color: 'white',
                                padding: 2,
                                borderWidth: 0,
                                distance: -50,
                                padding: 2,
                                style: {
                                    textOutline: false
                                }
                            },
                            showInLegend: true,
                            size: "100%"
                        }
                    },
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal'
                                },
                                yAxis: {
                                    labels: {
                                        align: 'left',
                                        x: 0,
                                        y: -5
                                    },
                                    title: {
                                        text: null
                                    }
                                },
                                subtitle: {
                                    text: null
                                },
                                credits: {
                                    enabled: false
                                }
                            }
                        }],
                    }
                });

                var donutChartAccountAllocation = Highcharts.chart('donutChartAccountAllocation', {
                    chart: {
                        resetZoomButton: {
                            position: {
                                // align: 'right', // by default
                                // verticalAlign: 'top', // by default
                                x: 0,
                                y: -80
                            }
                        },
                        //height: '300px',
                        type: 'pie',
                        className: 'fund-bg-color',
                        //options3d: {
                        //    enabled: true,
                        //    alpha: 60,
                        //    beta: 0
                        //},
                        events: {
                            addSeries: function () {
                                $('.donutChartAccountAllocationloadingChartDiv').fadeOut();
                                var label = this.renderer.label('A series was added', 30, 30)
                                    .attr({
                                        fill: Highcharts.getOptions().colors[1],
                                        padding: 10,
                                        zIndex: 8
                                    })
                                    .css({
                                        color: '#FFFFFF'
                                    })
                                    .add();

                                setTimeout(function () {
                                    label.fadeOut();
                                }, 1000);
                            },
                            load: function () {
                            }
                        },
                        style: {
                            fontFamily: "'Montserrat', sans-serif"
                        }
                    },
                    title: {
                        useHTML: true,
                        text: ''
                        //text: '<p class="fs-12">Based on Investment</p>'
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'middle',
                        layout: 'vertical',
                        useHTML: true, labelFormatter: function () {
                            return '<div style="width:200px;"><span style="float:left;">' + this.name + '</span><span style="float:right;">MYR ' + ConvertToCurrency(this.y) + '</span></div>';
                        },
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>',
                        followPointer: true,
                    },
                    credits: {
                        href: '/Index.aspx',
                        text: 'eapexis.apexis.com.my',
                        position: {
                            align: 'right',
                            verticalAlign: 'bottom',
                        }
                    },
                    plotOptions: {
                        pie: {
                            size: 250,
                            innerSize: 150,
                            shadow: false,
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 45,
                            dataLabels: {
                                enabled: false,
                                //format: '{y} %',
                                formatter: function () {
                                    var y = this.y;
                                    return y.toFixed(2) + ' MYR';
                                },
                                color: 'black',
                                padding: 2,
                                borderWidth: 0,
                                distance: 0,
                                padding: 2,
                                style: {
                                    textOutline: false
                                }
                            },
                            showInLegend: true,
                            size: "100%"
                        }
                    },
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal'
                                },
                                yAxis: {
                                    labels: {
                                        align: 'left',
                                        x: 0,
                                        y: -5
                                    },
                                    title: {
                                        text: null
                                    }
                                },
                                subtitle: {
                                    text: null
                                },
                                credits: {
                                    enabled: false
                                }
                            }
                        }],
                    }
                });

                var donutChartFundCategoryAllocation = Highcharts.chart('donutChartFundCategoryAllocation', {
                    chart: {
                        resetZoomButton: {
                            position: {
                                // align: 'right', // by default
                                // verticalAlign: 'top', // by default
                                x: 0,
                                y: -80
                            }
                        },
                        //height: '300px',
                        type: 'pie',
                        className: 'fund-bg-color',
                        //options3d: {
                        //    enabled: true,
                        //    alpha: 60,
                        //    beta: 0
                        //},
                        events: {
                            addSeries: function () {
                                $('.donutChartFundCategoryAllocationloadingChartDiv').fadeOut();
                                var label = this.renderer.label('A series was added', 30, 30)
                                    .attr({
                                        fill: Highcharts.getOptions().colors[1],
                                        padding: 10,
                                        zIndex: 8
                                    })
                                    .css({
                                        color: '#FFFFFF'
                                    })
                                    .add();

                                setTimeout(function () {
                                    label.fadeOut();
                                }, 1000);
                            },
                            load: function () {
                            }
                        },
                        style: {
                            fontFamily: "'Montserrat', sans-serif"
                        }
                    },
                    title: {
                        useHTML: true,
                        text: ''
                        //text: '<p class="fs-12">Based on Investment</p>'
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'middle',
                        layout: 'vertical',
                        useHTML: true, labelFormatter: function () {
                            return '<div style="width:200px;"><span style="float:left;">' + this.name + '</span><span style="float:right;"> MYR ' + ConvertToCurrency(this.y) + '</span></div>';
                        },
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>',
                        followPointer: true,
                    },
                    credits: {
                        href: '/Index.aspx',
                        text: 'eapexis.apexis.com.my',
                        position: {
                            align: 'right',
                            verticalAlign: 'bottom',
                        }
                    },
                    plotOptions: {
                        pie: {
                            size: 250,
                            innerSize: 150,
                            shadow: false,
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 45,
                            dataLabels: {
                                enabled: false,
                                //format: '{y} %',
                                formatter: function () {
                                    var y = this.y;
                                    return y.toFixed(2) + ' MYR';
                                },
                                color: 'black',
                                padding: 2,
                                borderWidth: 0,
                                distance: 0,
                                padding: 2,
                                style: {
                                    textOutline: false
                                }
                            },
                            showInLegend: true,
                            size: "100%"
                        }
                    },
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal'
                                },
                                yAxis: {
                                    labels: {
                                        align: 'left',
                                        x: 0,
                                        y: -5
                                    },
                                    title: {
                                        text: null
                                    }
                                },
                                subtitle: {
                                    text: null
                                },
                                credits: {
                                    enabled: false
                                }
                            }
                        }],
                    }
                });

                var stackedBarChartFundAllocations = Highcharts.chart('stackedBarChartFundAllocations', {
                    chart: {
                        resetZoomButton: {
                            position: {
                                // align: 'right', // by default
                                // verticalAlign: 'top', // by default
                                x: 0,
                                y: -80
                            }
                        },
                        height: '300px',
                        type: 'column',
                        events:
                        {
                            addSeries: function () {
                                $('.stackedBarChartFundAllocationsloadingChartDiv').fadeOut();
                                var label = this.renderer.label('A series was added', 30, 30)
                                    .attr({
                                        fill: Highcharts.getOptions().colors[0],
                                        padding: 10,
                                        zIndex: 8
                                    })
                                    .css({
                                        color: '#FFFFFF'
                                    })
                                    .add();
                                setTimeout(function () {
                                    label.fadeOut();
                                }, 1000);
                            },
                            load: function () {
                            }
                        },
                        borderColor: '#000000',
                        borderWidth: 0,
                        inverted: false,
                        zoomType: 'x',
                        spacingRight: 20,
                        style: {
                            fontFamily: "'Montserrat', sans-serif"
                        }
                    },
                    title: {
                        useHTML: true,
                        text: '<p style="font-size:14px; margin-bottom:5px; color:#aaaaaa;">Fund Allocations</p>'
                    },
                    subtitle: {
                        useHTML: true,
                        text: (document.ontouchstart === undefined ?
                            'Click and drag in the plot area to zoom in' :
                            '<div class="pinchZoom">Pinch the chart to zoom in</div>')
                    },
                    //xAxis: {
                    //    //categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
                    //},
                    yAxis: {
                        endOnTick: false,
                        allowDecimals: true,
                        title: {
                            text: 'Allocations (%)'
                        }
                    },
                    tooltip: {
                        shared: true,
                        pointFormat: '{series.name}: <b>{point.y} </b><br />',
                        followPointer: true,
                        crosshairs: [true, true],
                        crosshairs: {
                            color: 'rgba(0,0,0,0.3)',
                            dashStyle: 'Solid',
                            width: 2
                        },
                        formatter: function () {
                            var s = [];
                            var x;
                            var l = (this.points).length;
                            $.each(this.points, function (i, point) {
                                var seriesName = point.series.name;
                                var y = point.y;
                                x = point.x;
                                //Highcharts.dateFormat('%b %Y', x)
                                if (i == 0) {
                                    s.push('<h6 style="text-decoration:underline;">' + point.key + '</h6><br/><span style="color:' + point.color + '">' + seriesName + ': ' + y.toFixed(2) + ' % </span>');
                                }
                                else {
                                    s.push('<span style="color:' + point.color + '">' + seriesName + ': ' + y.toFixed(2) + ' % </span>');
                                }
                            });


                            return s.join('<br />');
                        }
                    },
                    credits: {
                        href: '/Index.aspx',
                        text: 'eapexis.apexis.com.my',
                        position: {
                            align: 'right',
                            verticalAlign: 'bottom',
                        }
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true,
                                //format: '{y} %',
                                formatter: function () {
                                    var y = this.y;
                                    var max = this.series.yAxis.max,
                                        color = this.y / max < 0.05 ? 'black' : 'white'; // 5% width
                                    return '<span style="color: ' + color + '">' + y.toFixed(2) + ' %</span>';
                                },
                                crop: false,
                                overflow: 'none',
                                //rotation: 270,
                                color: 'white',
                                padding: 0,
                                borderWidth: 0,
                                style: {
                                    textOutline: false
                                }
                            },
                            textOtLine: 0
                        }
                        //line: {
                        //    marker: {
                        //        enabled: false,
                        //        symbol: 'circle',
                        //        radius: 2,
                        //        states: {
                        //            hover: {
                        //                enabled: true
                        //            },
                        //        }
                        //    }
                        //}
                    },
                    //series: [{
                    //    name: 'John',
                    //    data: [5, 3, 4, 7, 2]
                    //}, {
                    //    name: 'Jane',
                    //    data: [2, 2, 3, 2, 1]
                    //}, {
                    //    name: 'Joe',
                    //    data: [3, 4, 4, 2, 5]
                    //}],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal'
                                },
                                yAxis: {
                                    labels: {
                                        align: 'left',
                                        x: 0,
                                        y: -5
                                    },
                                    title: {
                                        text: null
                                    }
                                },
                                subtitle: {
                                    text: null
                                },
                                credits: {
                                    enabled: false
                                }
                            }
                        }],
                    }
                });

                Highcharts.setOptions({
                    lang: {
                        resetZoom: 'Reset zoom', resetZoomTitle: 'Reset zoom level 1:1'
                    }
                });



                //CHART END
                var lineChartXAxisLength = 0;
                $('.portfolio-btn').click(function () {
                    //console.log('started')
                    $('#ddlPorfolioAccountsMenuMobile').val('');
                    $('#consolidatedAccountSummaryLoading').removeClass('hide');
                    $('#portfolioAccountsMenu a, .portfolio-btn').removeClass("btn-active");
                    $('#portfolioAccountsMenu a, .portfolio-btn').removeClass("btn-infos1");
                    $('#portfolioAccountsMenu a, .portfolio-btn').addClass("btn-infos1-default disabled");
                    $('#consolidatedAccountSummary').removeClass('hide');
                    $('#consolidatedFundSummaryByAccount').addClass('hide');
                    $(this).removeClass("btn-infos1-default");
                    $(this).removeClass("text-success");
                    $(this).addClass("btn-active");
                    $(this).addClass("btn-infos1");
                    //Get Consolidated Account Summary
                    //console.log('started GetConsolidatedAccountSummary' + new Date().toUTCString())
                    $.ajax({
                        url: "Portfolio.aspx/GetConsolidatedAccountSummary",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        //data: { HolderID: HolderID },
                        success: function (data) {
                            //console.log('GetConsolidatedAccountSummary response' + new Date().toUTCString())
                            $('#primaryAccountClass').attr('data-title', '');
                            $('#primaryAccountClass').html('All');
                            $('#maNumber').html('-');
                            $('#userAccountsList a').removeClass('active');
                            //$('[data-account-no="' + HolderNo + '"] a').addClass('active');
                            $('#smallAccountPlan').html("CONSOLIDATED ACCOUNT");

                            var json = data.d;
                            if (json.consolidatedTotalInvestmentRM == 0) {
                                $('.loadingDiv').addClass('hide');
                                $('#consolidatedAccountSummary > div:not(:nth-child(2))').addClass('hide');
                                $('.no-data-available').removeClass('hide');
                                $('#portfolioAccountsMenu a, .portfolio-btn').removeClass("disabled");
                                $('#asAtDate').html(parseJsonDate(json.asAtDate));
                                $('#CurrentDate').html(parseJsonDate(json.asAtDate));
                            }
                            else {
                                var NoOfAccounts = json.NoOfAccounts;
                                var consolidatedTotalInvestmentRM = json.consolidatedTotalInvestmentRM;
                                var consolidatedTotalInvestmentUNITS = json.consolidatedTotalInvestmentUNITS.toFixed(4);
                                var consolidatedAccountUnrealisedGainRM = json.consolidatedAccountUnrealisedGainRM;
                                var consolidatedAccountUnrealisedGainRMPer = json.consolidatedAccountUnrealisedGainRMPer;
                                //var InvestmentPerformances = json.InvestmentPerformances;
                                var PortfolioMarketValueMovements = json.PortfolioMarketValueMovements;
                                var PortfolioAllocations = json.PortfolioAllocations;
                                var AccountAllocations = json.AccountAllocations;
                                var FCAllocations = json.FCAllocations;
                                var FundAllocations = json.FundAllocations;
                                var afterDecimal = consolidatedTotalInvestmentUNITS.substr(consolidatedTotalInvestmentUNITS.length - 4);
                                var beforeDecimal = consolidatedTotalInvestmentUNITS.substr(0, consolidatedTotalInvestmentUNITS.length - 4);
                                $('#asAtDate').html(parseJsonDate(json.asAtDate));
                                $('#CurrentDate').html(parseJsonDate(json.asAtDate));
                                $('#consolidatedAccountTotalInvestmentLabel').html('<strong><span class="currencyFormat">' + consolidatedTotalInvestmentRM + '</span><small><strong> (<span class="unitFormatNoDecimal">' + beforeDecimal + '</span><small><small>.' + afterDecimal + '</small></small>' + ' <sup><small>UNITS</small></sup>)</strong></small></strong>');

                                var fundInvests = "<table class='table table-condensed table-bordered table-cell-pad-5 mb-0'><tbody>";
                                $.each(FundAllocations, function (idx, obj) {
                                    var commaNum = numberWithCommas(obj.Investment.toFixed(2));
                                    fundInvests += "<tr><td class='text-left'>" + obj.FundCode + "</td><td class='text-right'><span class='currencyFormatNoSymbol'>" + commaNum + "</span></td></tr>";
                                });
                                fundInvests += "</tbody></table>";
                                $('#consolidatedAccountTotalInvestmentLabel').attr('data-title', fundInvests);
                                $('#consolidatedAccountTotalInvestmentLabel').attr('data-toggle', 'tooltip');
                                $('[data-toggle="tooltip"]').tooltip({ html: true });
                                $('#consolidatedAccountsCount').html('<strong>' + NoOfAccounts + '</strong>');
                                if (consolidatedAccountUnrealisedGainRM >= 0) {
                                    $('#consolidatedAccountUnrealisedGainRM').html('<strong class="text-success">MYR </strong><strong><span class="text-success currencyFormatNoSymbol"> ' + consolidatedAccountUnrealisedGainRM.toFixed(2) + '</span><span class="text-success"> (' + consolidatedAccountUnrealisedGainRMPer.toFixed(2) + ' %)</span></strong>');
                                } else {
                                    $('#consolidatedAccountUnrealisedGainRM').html('<strong class="text-danger">MYR </strong><strong><span class="text-danger currencyFormatNoSymbol"> ' + consolidatedAccountUnrealisedGainRM.toFixed(2) + '</span><span class="text-danger"> (' + consolidatedAccountUnrealisedGainRMPer.toFixed(2) + ' %) </span></strong>');
                                }
                                $('#portfolioAccountsMenu a, .portfolio-btn').removeClass("disabled");
                                FormatAllCurrency();
                                FormatAllUnit();
                                //var InvestmentPerformanceReportDates = $.map(InvestmentPerformances, function (obj, idx) {
                                //    //console.log(obj.ReportDate);
                                //    return parseJsonDateToChartDate(obj.ReportDate).toString();
                                //});
                                //var InvestmentPerformanceInvestUnits = $.map(InvestmentPerformances, function (obj, idx) {
                                //    //var num = parseFloat(obj.InvestUnits.toFixed(4));
                                //    //var str = num.toString().split('.');
                                //    //console.log(str[0]);
                                //    //console.log(str[1]);
                                //    //if (str[0].length >= 4) {
                                //    //    str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
                                //    //}
                                //    //console.log (str.join('.'));

                                //    return parseFloat(obj.InvestUnits.toFixed(4));
                                //});
                                //var InvestmentPerformanceInvestRMs = $.map(InvestmentPerformances, function (obj, idx) {
                                //    return parseFloat(obj.InvestMarketValue.toFixed(2));
                                //});
                                //var InvestmentPerformanceInvestCostRMs = $.map(InvestmentPerformances, function (obj, idx) {
                                //    return parseFloat(obj.InvestCost.toFixed(2));
                                //});
                                //var InvestmentPerformanceGainLoss = $.map(InvestmentPerformances, function (obj, idx) {
                                //    return parseFloat(obj.GainLoss.toFixed(2));
                                //});
                                //stackedBarChartInvestmentPerformance
                                //while (stackedBarChartInvestmentPerformance.series.length > 0) stackedBarChartInvestmentPerformance.series[0].remove(true);
                                //stackedBarChartInvestmentPerformance.xAxis[0].setCategories(InvestmentPerformanceReportDates);

                                //stackedBarChartInvestmentPerformance.addSeries({
                                //    name: 'Net Investment Market Value',
                                //    data: InvestmentPerformanceInvestRMs,
                                //    legendIndex: 1,
                                //    index: 1,
                                //    className: "fund-color-1"
                                //});
                                //stackedBarChartInvestmentPerformance.addSeries({
                                //    name: 'Net Investment Cost',
                                //    data: InvestmentPerformanceInvestCostRMs,
                                //    legendIndex: 0,
                                //    index: 0,
                                //    //className: "fund-color-1"
                                //});
                                //stackedBarChartInvestmentPerformance.addSeries({
                                //    name: 'Gain/Loss',
                                //    data: InvestmentPerformanceGainLoss,
                                //    legendIndex: 1,
                                //    index: 1,
                                //    //className: "fund-color-0"
                                //});
                                //stackedBarChartInvestmentPerformance.addSeries({
                                //    name: 'Total Dividend Payout',
                                //    data: InvestmentPerformanceInvestRMs,
                                //    legendIndex: 2,
                                //    index: 0
                                //});

                                var PortfolioMarketValueMovementReportDates = $.map(PortfolioMarketValueMovements, function (obj, idx) {
                                    //console.log(obj.ReportDate);
                                    return parseJsonDateToChartDate(obj.ReportDate).toString();
                                });
                                var PortfolioMarketValueMovementInvestUnits = $.map(PortfolioMarketValueMovements, function (obj, idx) {
                                    return parseFloat(obj.InvestUnits.toFixed(4));
                                });
                                var PortfolioMarketValueMovementInvestRMs = $.map(PortfolioMarketValueMovements, function (obj, idx) {
                                    return parseFloat(obj.InvestMarketValue.toFixed(2));
                                });
                                var PortfolioMarketValueMovementRealizedProfits = $.map(PortfolioMarketValueMovements, function (obj, idx) {
                                    return parseFloat(obj.RealizedProfit.toFixed(2));
                                });
                                var PortfolioMarketValueMovementGainLoss = $.map(PortfolioMarketValueMovements, function (obj, idx) {
                                    return parseFloat(obj.GainLoss.toFixed(2));
                                });
                                lineChartXAxisLength = PortfolioMarketValueMovementReportDates.length;
                                //lineChartPortfolioMarketValueMovement
                                while (lineChartPortfolioMarketValueMovement.series.length > 0) lineChartPortfolioMarketValueMovement.series[0].remove(true);
                                lineChartPortfolioMarketValueMovement.xAxis[0].setCategories(PortfolioMarketValueMovementReportDates);
                                //lineChartPortfolioMarketValueMovement.addSeries({
                                //    name: 'Total Investment Units',
                                //    data: PortfolioMarketValueMovementInvestUnits,
                                //    legendIndex: 0,
                                //    index: 0
                                //});
                                lineChartPortfolioMarketValueMovement.addSeries({
                                    name: 'Realized Profit',
                                    data: PortfolioMarketValueMovementRealizedProfits,
                                    legendIndex: 0,
                                    index: 0
                                });
                                //pieChartPortfolioAllocations
                                var pieChartPortfolioAllocationsSeriesData = [];
                                $.each(PortfolioAllocations, function (idx, u) {
                                    var Percent = parseFloat(u.Percent.toFixed(2));
                                    pieChartPortfolioAllocationsSeriesData.push([u.AccountPlan, Percent]);
                                });
                                while (pieChartPortfolioAllocation.series.length > 0) pieChartPortfolioAllocation.series[0].remove(true);
                                pieChartPortfolioAllocation.addSeries({
                                    data: pieChartPortfolioAllocationsSeriesData,
                                    name: "Portfolio Allocations",
                                    type: 'pie',
                                    useHTML: true
                                });

                                //donutChartAccountAllocations
                                var donutChartAccountAllocationsSeriesData = [];
                                $.each(AccountAllocations, function (idx, u) {
                                    var Percent = parseFloat(u.Investment.toFixed(2));
                                    donutChartAccountAllocationsSeriesData.push([u.AccountNo + " - " + u.AccountPlan, Percent]);
                                });
                                while (donutChartAccountAllocation.series.length > 0) donutChartAccountAllocation.series[0].remove(true);
                                donutChartAccountAllocation.addSeries({
                                    data: donutChartAccountAllocationsSeriesData,
                                    name: "Account Allocations",
                                    type: 'pie',
                                    useHTML: true
                                });

                                //donutChartFundCategoryAllocations
                                var donutChartFundCategoryAllocationsSeriesData = [];
                                $.each(FCAllocations, function (idx, u) {
                                    var Percent = parseFloat(u.Investment.toFixed(2));
                                    donutChartFundCategoryAllocationsSeriesData.push([u.FundCategoryName, Percent]);
                                });
                                while (donutChartFundCategoryAllocation.series.length > 0) donutChartFundCategoryAllocation.series[0].remove(true);
                                donutChartFundCategoryAllocation.addSeries({
                                    data: donutChartFundCategoryAllocationsSeriesData,
                                    name: "Market Allocations",
                                    type: 'pie',
                                    useHTML: true
                                });

                                //stackedBarChartFundAllocations
                                var FundAllocationsFundCodes = $.map(FundAllocations, function (obj, idx) {
                                    return obj.FundCode;
                                });

                                var FundAllocationsFundCodesData = $.map(FundAllocations, function (obj, idx) {
                                    return { name: obj.FundCode, y: parseFloat(obj.Percent.toFixed(2)), className: "fund-color-" + obj.FundCode };
                                });


                                var FundAllocationsPercent = $.map(FundAllocations, function (obj, idx) {
                                    return parseFloat(obj.Percent.toFixed(2));
                                });
                                while (stackedBarChartFundAllocations.series.length > 0) stackedBarChartFundAllocations.series[0].remove(true);
                                stackedBarChartFundAllocations.xAxis[0].setCategories(FundAllocationsFundCodes);

                                //console.log(FundAllocationsFundCodesData);

                                stackedBarChartFundAllocations.addSeries({
                                    name: 'Fund Allocations',
                                    data: FundAllocationsFundCodesData,
                                });
                                $('#consolidatedAccountSummaryLoading').addClass('hide');
                            }
                        }
                    });

                });

                $('#portfolioAccountsMenu').on('click', '.maHolderRegNo', function () {
                    $('#portfolioAccountsMenu a, .portfolio-btn').removeClass("btn-active");
                    $('#portfolioAccountsMenu a, .portfolio-btn').removeClass("btn-infos1");
                    $('#portfolioAccountsMenu a, .portfolio-btn').addClass("btn-infos1-default disabled");
                    $(this).removeClass("btn-infos1-default");
                    $(this).removeClass("text-success");
                    $(this).addClass("btn-active");
                    $(this).addClass("btn-infos1");
                    var MAHolderRegNo = $(this).attr('data-accNo');
                    GetMAAccountUtmcMemberInvestments(MAHolderRegNo);
                    $('#consolidatedAccountSummary').addClass('hide');
                    $('#consolidatedFundSummaryByAccount').removeClass('hide');
                    //$('#profileLoading').fadeOut(1000);

                });
                if (hdnIsVerified == 1) {
                    //var selectedAccId = $('#hdnSelectedAccountId').val();
                    //console.log(selectedAccId);
                    //if (selectedAccId != "" && selectedAccId != "0") {
                    //    $('#portfolioAccountsMenu a[data-id="' + selectedAccId + '"]').click();
                    //}
                    var hdnSelectedAccountNo = $('#hdnSelectedAccountNo').val();
                    //$('[data-accno="' + hdnSelectedAccountNo + '"').click();
                    $('.portfolio-btn').click();
                    //$('#profileLoading').fadeOut(1000);

                    if (hdnPasswordExpiredDate >= 165 && hdnPasswordExpiredDate < 180) {
                        var passwordExpiryDaysCount = 180 - hdnPasswordExpiredDate;
                        $('.passwordExpiryDaysCount').html(passwordExpiryDaysCount);
                        $('#modalExpiryOfPassword').modal({
                            keyboard: false,
                            backdrop: "static",
                        })
                    } else if (hdnPasswordExpiredDate >= 180) {
                        $('#modalChangePassword').modal({
                            keyboard: false,
                            backdrop: "static",
                        })
                    }
                }

                $('#maAccounts').on('click', '.makePrimary', function () {
                    var MAHolderRegID = $(this).attr('data-id');
                    //$('#profileLoading').fadeIn(1000);
                    $.ajax({
                        url: "Portfolio.aspx/MakePrimary",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { HolderID: MAHolderRegID },
                        success: function (data) {
                            var userAccounts = data.d;
                            if (userAccounts.length > 0) {
                                $('#maAccounts').html('');
                                $.each(userAccounts, function (idx, uA) {
                                    var maHolderReg = uA.MaHolderRegIdMaHolderReg;
                                    var tr = $('<tr />').appendTo($('#maAccounts'));
                                    var td1 = $('<td />').html((idx + 1) + " " + (uA.IsPrimary == 1 ? "<sup data-toggle='tooltip' title='Primary Account'><i class='fa fa-star text-primary'></i></sup>" : "<sup data-id='" + maHolderReg.Id + "' class='makePrimary cursor-pointer' data-toggle='tooltip' title='Make Primary'><i class='fa fa-star-o'></i></sup>")).appendTo(tr);
                                    var td2 = $('<td />').html("<a href='javascript:;' class='maHolderRegNo " + (uA.IsVerified == 0 ? "text-danger" : "text-success") + "' data-id='" + maHolderReg.Id + "' data-toggle='tooltip' title='" + (uA.IsVerified == 0 ? "Click to Activate" : "Click to See Details") + "'>" + maHolderReg.HolderNo + "</a>").appendTo(tr);
                                    var td3 = $('<td />').html(maHolderReg.HolderCls).appendTo(tr);
                                });
                                $("[data-toggle='tooltip']").tooltip();
                                var customNotify = $('<div class="custom-notification bottom left well well-sm bg-success"></div>').html('Primary account updated. <i class="fa fa-times hide"></i>');
                                $('body').prepend(customNotify);
                                setTimeout(function () {
                                    $('.custom-notification').fadeOut(2000, function () {
                                        $('.custom-notification').remove();
                                    });
                                }, 5000);
                                //$('#profileLoading').fadeOut(1000);
                            }
                        }
                    });
                });

                $('#ddlPorfolioAccountsMenuMobile').change(function () {
                    var No = $(this).val();
                    $('[data-accNo="' + No + '"].maHolderRegNo').click();
                });
            });
        }
    </script>

</asp:Content>
