﻿using DiOTP.PolicyAndRules;
using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using static DiOTP.PolicyAndRules.FundPolicy;

namespace DiOTP.WebApp
{
    public partial class Fund_Comparison : System.Web.UI.Page
    {
        private static readonly Lazy<IUtmcFundInformationService> lazyObjFund = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyObjFund.Value; } }

        private static readonly Lazy<IFundChartInfoService> lazyIFundChartInfoServiceObj = new Lazy<IFundChartInfoService>(() => new FundChartInfoService());

        public static IFundChartInfoService IFundChartInfoService { get { return lazyIFundChartInfoServiceObj.Value; } }
        private static ResponseFundStep1 responseFundStep1 { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] != null)
                {
                    homeLink.HRef = "/Portfolio.aspx";
                }
                String fundCode = Request.QueryString["fundCode"];
                if (fundCode != null)
                {
                    Response responseFunds = (Response)FundPolicy.Get(new FundPolicy.FundClass { Step = 1, httpContext = Context });

                    if (responseFunds.IsSuccess)
                    {
                        responseFundStep1 = (ResponseFundStep1)responseFunds.Data;
                        FundPolicyEnum fundPolicyEnum = (FundPolicyEnum)Enum.Parse(typeof(FundPolicyEnum), responseFundStep1.Code);
                        string message = fundPolicyEnum.ToDescriptionString();
                        if (message == "SUCCESS")
                        {
                            List<UtmcFundInformation> UtmcFundInformations = responseFundStep1.utmcFundInformations;
                            UtmcFundInformation utmcFundInformation = UtmcFundInformations.Where(x => x.FundCode == fundCode).FirstOrDefault();
                            hdnFundId.Value = utmcFundInformation.Id.ToString();
                            fundInformation.InnerHtml = "<a href='/Fund-Information.aspx?fundCode=" + fundCode + "'>Fund Information</a>";
                        }
                        else if (message == "EXCEPTION")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + responseFundStep1.Message + "','');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + message + "','');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "ShowCustomMessage('Exception', '" + responseFunds.Message + "','');", true);
                    }
                }
                else
                {
                    Response.Redirect("Funds-listing.aspx");
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetFunds()
        {
            Response response = new Response();
            Response responseFunds = (Response)FundPolicy.Get(new FundPolicy.FundClass { Step = 1, httpContext = HttpContext.Current });

            if (responseFunds.IsSuccess)
            {
                responseFundStep1 = (ResponseFundStep1)responseFunds.Data;
                FundPolicyEnum fundPolicyEnum = (FundPolicyEnum)Enum.Parse(typeof(FundPolicyEnum), responseFundStep1.Code);
                string message = fundPolicyEnum.ToDescriptionString();
                if (message == "SUCCESS")
                {
                    List<UtmcFundInformation> UtmcFundInformations = responseFundStep1.utmcFundInformations;
                    response.IsSuccess = true;
                    response.Data = UtmcFundInformations;
                }
                else if (message == "EXCEPTION")
                {
                    response.IsSuccess = false;
                    response.Message = responseFundStep1.Message;
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = message;
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Message = responseFunds.Message;
            }
            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetFundChartSeries(Int32[] selectedFunds, Int32 dataValue)
        {
            Response response = new Response();
            FundPolicyEnum fundPolicyEnum = (FundPolicyEnum)Enum.Parse(typeof(FundPolicyEnum), responseFundStep1.Code);
            string message = fundPolicyEnum.ToDescriptionString();
            if (message == "SUCCESS")
            {
                List<UtmcFundInformation> UtmcFundInformations = responseFundStep1.utmcFundInformations;

                Response responseFundChartInfo = (Response)FundPolicy.Get(new FundPolicy.FundClass { Step = 3, httpContext = HttpContext.Current, selectedFunds = selectedFunds, dataValue = dataValue, utmcFundInformations = UtmcFundInformations });

                if (responseFundChartInfo.IsSuccess)
                {
                    ResponseFundStep3 responseFundStep3 = (ResponseFundStep3)responseFundChartInfo.Data;
                    FundPolicyEnum fundPolicyEnum1 = (FundPolicyEnum)Enum.Parse(typeof(FundPolicyEnum), responseFundStep3.Code);
                    string message1 = fundPolicyEnum1.ToDescriptionString();
                    if (message1 == "SUCCESS")
                    {
                        response.IsSuccess = true;
                        response.Data = responseFundStep3.FundChartInformations;
                    }
                    else if (message1 == "EXCEPTION")
                    {
                        response.IsSuccess = false;
                        response.Message = responseFundStep3.Message;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = message1;
                    }
                }
            }
            else if (message == "EXCEPTION")
            {
                response.IsSuccess = false;
                response.Message = responseFundStep1.Message;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = message;
            }
            return response;
        }
    }
}