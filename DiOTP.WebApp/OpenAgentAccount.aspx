﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" CodeBehind="OpenAgentAccount.aspx.cs" Inherits="DiOTP.WebApp.OpenAgentAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2">
        <div class="settings-wrapper">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/Portfolio.aspx">eApexIs</a></li>
                    <li class="active">Open Agent Account</li>
                </ol>
            </div>

            <div style="text-align: left;">
                <h5 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Open Agent Account</h5>
            </div>

            <div class="row mb-20 mt-20">
                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                    Account Type
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <asp:DropDownList CssClass="form-control" ID="ddlAccountType" runat="server" Enabled="false">
                        <asp:ListItem>AGENT</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="row mb-20">
                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                    Introducer Code
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <asp:TextBox ID="txtIntroCode" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter Introducer Code"></asp:TextBox>
                </div>
            </div>
            <div class="row mb-20">
                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                    Upline Code
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <asp:TextBox ID="txtUplineCode" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="Enter Upline Code" ReadOnly="true"></asp:TextBox>
                </div>
            </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <asp:Button ID="btnNext" runat="server" ClientIDMode="Static" CssClass="btn btn-infos1 pull-right" Text="Next" OnClick="btnNext_Click" />
            </div>
        </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
</asp:Content>
