﻿<%@ Page Language="C#" MasterPageFile="~/DiOTPMaster.master" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="DiOTP.WebApp.Error.ErrorPage" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/Error/ErrorCss.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
    <section id="Error">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="error-template">
                        <h1>
                            Oops!</h1>
                        <h2>
                            ERROR 404 Not Found</h2>
                        <div class="error-details">
                            Sorry, an error has occured, Requested page not found!
                        </div>
                        <div class="error-actions">
                            <a href="/Index.aspx" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
                                Take Me Home </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

<asp:Content ContentPlaceHolderID="modalPlace" ID="modalHolder" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="Scripts" runat="server">

</asp:Content>
