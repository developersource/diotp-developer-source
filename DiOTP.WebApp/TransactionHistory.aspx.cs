﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiOTP.WebApp
{
    public partial class TransactionHistory : System.Web.UI.Page
    {
        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<ITransactionCodeService> lazyTransactionCodeServiceObj = new Lazy<ITransactionCodeService>(() => new TransactionCodeService());

        public static ITransactionCodeService ITransactionCodeService { get { return lazyTransactionCodeServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUtmcFundInformationServiceObj = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());
        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUtmcFundInformationServiceObj.Value; } }

        private static readonly Lazy<IUtmcFundDetailService> lazyUtmcFundDetailServiceObj = new Lazy<IUtmcFundDetailService>(() => new UtmcFundDetailService());
        public static IUtmcFundDetailService IUtmcFundDetailService { get { return lazyUtmcFundDetailServiceObj.Value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                if (Session["isVerified"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                }
                if (!IsPostBack)
                {
                    if (Session["user"] != null)
                    {
                        User user = (User)Session["user"];

                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {
                            List<UserAccount> userAccounts = (List<UserAccount>)responseUAList.Data;
                            string all = String.Join(",", userAccounts.Select(x => "'" + x.AccountNo + "'").ToArray());
                            ddlFundAccount.Items.Add(new ListItem("All Accounts", all));
                            foreach (UserAccount userAccount in userAccounts)
                            {
                                ddlFundAccount.Items.Add(new ListItem(CustomValues.GetAccounPlan(userAccount.HolderClass) + " - " + userAccount.AccountNo, userAccount.AccountNo));
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUAList.Message + "\", '');", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            GenerateTransactionHistory();
        }

        public void GenerateTransactionHistory()
        {
            try
            {
                if (!(txtDateStart.Text.Equals("") && txtDateEnd.Text.Equals("")))
                {
                    DateTime startDate = DateTime.ParseExact(txtDateStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime endDate = DateTime.ParseExact(txtDateEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    if (startDate < endDate)
                    {
                        string startDateString = startDate.ToString("yyyy-MM-dd");
                        string endDateString = endDate.ToString("yyyy-MM-dd");
                        string selectedAccount = ddlFundAccount.SelectedItem.Value;
                        string type = ddlTransactionType.SelectedItem.Text.ToString().ToLower();
                        
                        List<UtmcCompositionalTransaction> utmcCompositionalTransactions = new List<UtmcCompositionalTransaction>();
                        List<UtmcFundInformation> UTMCFundInformations = new List<UtmcFundInformation>();
                        Response responseUFIList = IUtmcFundInformationService.GetDataByFilter(" Status != 'Inactive' ", 0, 0, false);
                        if (responseUFIList.IsSuccess)
                        {
                            UTMCFundInformations = (List<UtmcFundInformation>)responseUFIList.Data;
                        }
                        string fundIds = String.Join(",", UTMCFundInformations.Select(x => "'" + x.IpdFundCode + "'").ToList());

                        List<HolderLedger> holderLedgers = new List<HolderLedger>();
                        string query = @" select h.*, t.* from uts.holder_ledger h left join uts.UT_RCPT_PYMT t on 
                                        t.TRANS_NO = h.TRANS_NO and
                                        t.FUND_ID = h.FUND_ID and
                                        t.TRANS_TYPE = h.TRANS_TYPE
                                        where h.TRANS_TYPE NOT IN ('CO','UC','RC','SC','MC') and h.FUND_ID in (" + fundIds + @")
                                        and h.HOLDER_NO in (" + selectedAccount + ") and h.TRANS_DT >= to_date('" + startDateString + @"', 'yyyy-MM-dd') and h.TRANS_DT <= to_date('" + endDateString + @"', 'yyyy-MM-dd') ";
                        if (type.Equals("all", StringComparison.InvariantCultureIgnoreCase))
                        {
                            
                        }
                        if (type.Equals("purchase", StringComparison.InvariantCultureIgnoreCase))
                        {
                            query += " and h.TRANS_TYPE = 'SA' and (h.ACC_TYPE IS NULL OR h.ACC_TYPE != 'SW') ";
                            
                        }
                        else if (type.Equals("redemption", StringComparison.InvariantCultureIgnoreCase))
                        {
                            query += " and h.TRANS_TYPE = 'RD' and (h.ACC_TYPE IS NULL OR h.ACC_TYPE != 'SW') ";
                            
                        }
                        else if (type.Equals("switching", StringComparison.InvariantCultureIgnoreCase))
                        {
                            query += " and h.TRANS_TYPE in ('SA', 'RD') and h.ACC_TYPE = 'SW' ";
                            
                        }
                        else if (type.Equals("transfer", StringComparison.InvariantCultureIgnoreCase))
                        {
                            
                        }
                        else if (type.Equals("cooling off", StringComparison.InvariantCultureIgnoreCase))
                        {
                            
                        }
                        query += " ORDER By h.TRANS_DT desc";

                        holderLedgers = ServicesManager.GetHolderLedgerByHolderNo(query);
                        List<HolderLedger> holderLedgersFiltered = holderLedgers;
                        holderLedgersFiltered.ForEach(checkX =>
                        {
                            if (checkX.TransNO.Contains('X'))
                            {
                                string transNoX = checkX.TransNO;
                                string transNo = checkX.TransNO.TrimEnd('X');
                                holderLedgers = holderLedgers.Where(x => x.TransNO != transNo && x.TransNO != transNoX).ToList();
                            }
                        });
                        holderLedgers = holderLedgers.OrderByDescending(x => x.TransDt).ThenByDescending(x => Convert.ToInt32(x.TransNO)).ToList();

                        Response responseTC = ITransactionCodeService.GetDataByFilter(" 1=1 ", 0, 0, false);
                        List<TransactionCode> TCs = new List<TransactionCode>();
                        if (responseTC.IsSuccess)
                        {
                            TCs = (List<TransactionCode>)responseTC.Data;
                        }

                        List<UtmcFundDetail> utmcfunddetails = UTMCFundInformations.Select(x => x.UtmcFundInformationIdUtmcFundDetails.FirstOrDefault()).ToList();

                        StringBuilder sb = new StringBuilder();
                        holderLedgersFiltered = holderLedgersFiltered.OrderByDescending(x => x.TransDt).ThenByDescending(x => x.TransNO).ToList();
                        holderLedgersFiltered.ForEach(transaction =>
                        {
                            TransactionCode transactionCode = TCs.FirstOrDefault(x => x.Code == transaction.TransType);
                            string transactionName = transactionCode.Name;
                            if (transaction.AccType == "SW")
                            {
                                transactionName = transaction.TransType == "SA" ? "Switch In" : (transaction.TransType == "RD" ? "Switch Out" : (transactionCode != null ? transactionCode.Name : transaction.TransType));
                            }
                            UtmcFundInformation utmcfundinfo = UTMCFundInformations.FirstOrDefault(x => x.IpdFundCode == transaction.FundID);
                            UtmcFundDetail utmcfunddetail = utmcfunddetails.FirstOrDefault(x => x.UtmcFundInformationId == utmcfundinfo.Id);

                            sb.Append(
                                    @"<tr>
                                        <td style='text-align: left'>" + transaction.TransDt.Value.ToString("dd/MM/yyyy") + @"</td>
                                        <td style='text-align: left'>" + transaction.HolderNo + @"</td>
                                        <td style='text-align: left'>" + (transactionName == "" ? transaction.TransType : transactionName) + @"</td>
                                        <td style='text-align: left;'>" + utmcfundinfo.FundName.Capitalize() + @"</td>
                                        <td style='text-align: left'>" + transaction.TransNO + @"</td>
                                        <td style='text-align: right'>" + transaction.TransUnits.Value.ToString("#,##0.0000") + @"</td>
                                        <td style='text-align: right'>" + transaction.TransPr.Value.ToString("#,##0.0000") + @"</td>
                                        <td style='text-align: right'>" + transaction.TransAmt.Value.ToString("#,##0.00") + @"</td>
                                        <td style='text-align: left'>" + (transaction.PymtRef == "MBBAD" || transaction.PymtRef == "DIRECT DEBIT" ? "RSP" : (transactionName == "Switch In" || transactionName == "Switch Out" ? "Switching" : (transaction.PymtMode != null ? CustomValues.GetPaymentMethodByCode(transaction.PymtMode) : "-"))) + @"</td>
                                    </tr>");

                        });
                        //if (holderLedgersFiltered.Count == 0)
                        //{
                        //    sb.Append(
                        //            @"<tr>
                        //                <td colspan='9' class='dataTables_empty'>No transaction record found.</td>
                        //            </tr>");
                        //}

                        tbodyTransactionHistory.InnerHtml = sb.ToString();
                        
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Date(From) cannot be greater than Date(To)', '');", true);
                    }
                }
                else
                {
                    if (txtDateStart.Text.ToString() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please select a Date(From)', '');", true);
                    }
                    else if (txtDateEnd.Text.ToString() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', 'Please select a Date(To)', '');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("TransctionHistory Page GenerateTransactionHistory: " + ex.Message);
            }

        }
    }
}