﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="BuyFunds.aspx.cs" Inherits="DiOTP.WebApp.BuyFunds" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">
    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2">
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Portfolio.aspx">eApexIs</a></li>
                            <li class="active">Buy Funds</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Buy Fund</h3>
                    </div>
                </div>
            </div>
            <div style="margin-bottom: 5px; margin-left: 5px;">
                <img src="Content/MyImage/INFO.png" style="width: 15px; height: 15.25px; margin-right: 5px;" />
                <span style="font-weight: 600; color: #A0A0A0; font-size: 13px">You are currently viewing account</span>
            </div>
            <div class="row">

                <div class="col-sm-12 col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4 mb-10" style="margin-left: 0; width: 35.33%;">
                    <asp:DropDownList CssClass="form-control mobile-select selectpicker-acc" ID="ddlUserAccountId" runat="server" AutoPostBack="true" ClientIDMode="Static"  OnSelectedIndexChanged="ddlUserAccountId_SelectedIndexChanged"></asp:DropDownList>

                    <small style="font-size: 9px">* Only applicable to Cash Account</small>
                    <asp:HiddenField ID="isUserAccountChange" Value="false" runat="server" ClientIDMode="Static" />
                </div>
            </div>
            <%--<div class="row">
                <div class="col-md-12">
                    <div class="selectedAccountDetails hide">
                        <div class="row">
                            <div class="col-md-5">
                                <strong>Account Name:</strong>
                                <span id="accName" runat="server"></span>
                            </div>
                            <div class="col-md-3">
                                <strong>Master Account Number:</strong>
                                <span id="maAccNumber" runat="server"></span>
                            </div>
                            <div class="col-md-3">
                                <strong>Account Plan:</strong>
                                <span id="accType" runat="server" clientidclient="Static"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center form-box">
                    <div class="f1">
                        <div class="f1-steps">
                            <div class="f1-progress">
                                <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
                            </div>
                            <div class="test" style="width: 100%; height: 100%; background-color: #232312;"></div>
                            <div class="f1-step active">
                                <div class="test" style="width: 100%; height: 100%;"></div>
                                <div class="f1-step-icon"><i class="fa fa-universal-access"></i></div>
                                <p>Account Overview</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-money"></i></div>
                                <p>Fund Selection & Amount</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-file-text-o"></i></div>
                                <p>Confirmation</p>
                            </div>
                        </div>
                        <small class="text-danger" id="errorMessage"></small>
                        <small class="text-success" id="successMessage"></small>
                        <fieldset class="fieldsetAccountFundsOverview">

                            <div class="row hide" id="accSummaryDiv">
                                <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                                    <table class="table table-cell-pad-5 table-font-size-13 table-condensed table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Fund</th>
                                                <th class="text-right">Total<br />
                                                    Units</th>
                                                <th class="text-right">NAV Price<br />
                                                    MYR</th>
                                                <th class="text-right">Market Value<br />
                                                    MYR</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbodyAccountFunds" runat="server" clientidmode="Static">
                                            <tr>
                                                <td colspan="4" class="text-center">You have no unit trust holdings at this moment.  Start investing with us today by clicking 'Next'.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="f1-buttons">
                                        <button type="button" class="btn btn-next">Next</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="fieldsetfundSelectionAndAmount">
                            <div class="row mt-10">
                                <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">

                                    <div class="row mb-10">
                                        <div class="col-md-3" style="width: 40%;">
                                            <label>SAT Recommended Fund:</label>
                                        </div>
                                        <div class="col-md-3" style="width: 30%;">
                                            <asp:RadioButton ID="rdnRecommended" runat="server" GroupName="FundSelection" Text="Default" ClientIDMode="Static" />
                                        </div>
                                        <div class="col-md-4" style="width: 30%;">
                                            <asp:RadioButton ID="rdnNonRecommended" runat="server" GroupName="FundSelection" Text="Non-Default" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                    <p class="fs-12 text-justify mb-5" id="warningLabel" style="color: red; line-height: 13px; font-size: 11px;">* Please be informed that you are investing in the product without a recommendation and certain products are only suitable for investors who have met the product issuers' minimum qualifying criteria.</p>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label style="font-size: 12px; line-height: 12px;">Select Fund:</label>
                                            <asp:HiddenField ID="hdnFundId" runat="server" ClientIDMode="Static" />
                                            <asp:DropDownList ID="ddlFundsList" runat="server" ClientIDMode="Static" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-6">
                                            <label style="font-size: 12px; line-height: 12px;">Investment Amount (MYR) :</label>
                                            <asp:TextBox ID="txtAmount" runat="server" ClientIDMode="Static" placeholder="Enter investment amount" CssClass="form-control" onkeypress="CheckNumeric(this,event);"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6">
                                            <label style="font-size: 12px; line-height: 12px;">Distribution Instruction:</label>
                                            <asp:DropDownList ID="ddlDistributionInstruction" runat="server" CssClass="form-control" ClientIDMode="Static">
                                                <asp:ListItem Value="3">Reinvest</asp:ListItem>
                                                <asp:ListItem Value="1">Payout</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hdnBankId" runat="server" ClientIDMode="Static" />
                                        </div>
                                        <div class="col-md-6 mb-10">
                                            <label style="font-size: 12px; line-height: 12px;">Servicing Agent:</label>
                                            <asp:DropDownList ID="ddlConsultant" runat="server" CssClass="form-control" ClientIDMode="Static">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="row hide" id="bankDetailsDiv">
                                        <div class="col-md-6">
                                            <table class="table table-condensed table-bordered dataTable table-cell-pad-5" id="bankDetailsTable">
                                                <tr class="bg-info">
                                                    <th>Bank Name</th>
                                                    <th>Account Name</th>
                                                    <th>Account Number</th>
                                                </tr>
                                                <tbody class="fs-12" id="bankDetailsTbody">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <%--     <div class="row">
                                        <div class="col-md-3 col-md-offset-9">
                                            <a href="javascript:;" id="btnAddFund" class="btn btn-infos1 btn-sm btn-block mb-20">Add</a>
                                        </div>
                                    </div>--%>


                                    <div class="form-group row mt-10">
                                        <div class="col-md-3">
                                            <a href="#" class="btn btn-infos1 btn-sm btn-block mb-20" data-toggle="modal" data-target="#orderHistory">Order History</a>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-3">
                                            <a href="javascript:;" id="btnAddFund" class="btn btn-infos1 btn-sm btn-block mb-20">Add</a>
                                        </div>
                                    </div>


                                    <div id="divFundDetails" class="hide">
                                        <div class="loadingDiv">
                                            <div class="typing_loader"></div>
                                            <div class="text-center">Loading...</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group mb-10">
                                                    <%--<p class="fs-12 text-justify mb-5" id="warningLabel" style="color:red;line-height : 13px; font-size: 11px;">* Please be informed that you are investing in the product without a recommendation and certain products are only suitable for investors who have met the product issuers' minimum qualifying criteria.</p>--%>
                                                </div>
                                                <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                    <tbody>
                                                        <tr>
                                                            <td>Fund Name</td>
                                                            <td class="fundName">Fund Name</td>
                                                        </tr>
                                                        <tr class="bg-info">
                                                            <td>Units Held</td>
                                                            <td class="unitholding">Unit Holding</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Fund - Risk Profile</td>
                                                            <td class="riskCriteria">Risk Profile</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Sales Charge</td>
                                                            <td class="salesCharge">Sales Charge</td>
                                                        </tr>
                                                        <tr>
                                                            <td id="minimumInitialInvestementTitle">Minimum Initial Investement (CASH/EPF)</td>
                                                            <td class="minimumInitialInvestement">Minimum Initial Investement</td>
                                                        </tr>
                                                        <tr>
                                                            <td id="minimumSubsequentInvestementTitle">Minimum Subsequent Investement (CASH/EPF)</td>
                                                            <td class="minimumSubsequentInvestement">Minimum Subsequent Investement</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Master Prospectus</td>
                                                            <td class="masterProspectus">Master Prospectus</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Product Highlights Sheet</td>
                                                            <td class="productHighlightsSheet">Product Highlights Sheet</td>
                                                        </tr>
                                                        <tr id="infoMemo">
                                                            <td>Information Memorandum</td>
                                                            <td class="informationMemorandum">Information Memorandum</td>
                                                        </tr>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="2">Note: <strong>You must READ the Fund Prospectus/Information Memorandum and Product Highlights Sheet* before you proceed to the next step.</strong>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mt-15 cumulative" style="display: none" id="fundperformancetable">
                                        <h6 class="mb-2" style="color: #3392b1;">FUND PERFORMANCE</h6>
                                        <div class="table-responsive">
                                            <table class="table chart-table table-bordered myDataTable fund-table fundPerformanceCumulativeTable">
                                                <thead>
                                                    <tr class="fs-13">
                                                        <th style="width: 200px">Period <small class="pull-right">(Dividend date: <span id="dividendDateValue" runat="server"></span>)</small></th>
                                                        <th style="width: 60px;">1 week </th>
                                                        <th style="width: 60px;">1 month </th>
                                                        <th style="width: 60px;">3 month</th>
                                                        <th style="width: 60px;">6 month</th>
                                                        <th style="width: 60px;">1 year</th>
                                                        <th style="width: 60px;">2 year</th>
                                                        <th style="width: 60px;">3 year</th>
                                                        <th style="width: 60px;">5 year</th>
                                                        <th style="width: 60px;">10 year</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="fs-13">
                                                        <td>BID TO BID Returns (%) - MYR </td>
                                                        <td><span id="weekCValue" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="monthCValue" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="month3CValue" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="month6CValue" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="yearCValue" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="year2CValue" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="year3CValue" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="year5CValue" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="year10CValue" runat="server" clientidmode="Static"></span></td>
                                                    </tr>
                                                    <tr class="fs-13">
                                                        <td>Average </td>
                                                        <td><span id="weekCValueAvg" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="monthCValueAvg" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="month3CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="month6CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="yearCValueAvg" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="year2CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="year3CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="year5CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                        <td><span id="year10CValueAvg" runat="server" clientidmode="Static"></span></td>
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <tr class="fs-12">
                                                        <td colspan="10" style='font-family: "Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif;'>Performance figures are absolute returns based on the price of the fund as at <strong id="fundPricedate" runat="server" clientidmode="Static">WHEN?</strong> <%--(Last updated on <strong id="fundPriceLastUpdatedDate" runat="server" clientidmode="Static">WHEN?</strong>)--%>, on NAV-to-NAV basis with dividends being 'reinvested' on the dividend date.</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="row hide" id="cart-table">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                <thead>
                                                    <tr>
                                                        <th>Selected<br />
                                                            Fund</th>
                                                        <th class="text-right">Investment Amount<br />
                                                            MYR</th>
                                                        <th class="text-right">
                                                            <div style="margin-left: 65%; width: 40px">Action</div>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody class="tbodySelectedFunds" id="tbodySelectedFunds" runat="server" clientidmode="static"></tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-right">Total</th>
                                                        <td class="currencyFormatNoMYR tdTotalInvestmentAmount text-right" id="tdTotalInvestmentAmount" runat="server" clientidmode="static"></td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </table>



                                            <div class="form-group mb-5">
                                                <asp:CheckBox ID="chkConfirmFundInformation" ClientIDMode="Static" runat="server" CssClass="checkbox-inline text-info text-bold" Text="I confirm that I have read and understood the Fund Prospectus/Information Memorandum and Product Highlights Sheet of" />
                                            </div>
                                            <div class="f1-buttons">
                                                <button type="button" class="btn btn-previous">Previous</button>
                                                <button type="button" class="btn btn-next">Next</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="fieldsetConfirmation">
                            <div class="row">
                                <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-condensed table-cell-pad-5 table-font-size-13">
                                                <thead>
                                                    <tr>
                                                        <th>Selected<br />
                                                            Fund</th>
                                                        <th class="text-right">Investment Amount
                                                            <br />
                                                            MYR</th>

                                                    </tr>
                                                </thead>
                                                <tbody class="tbodySelectedFunds" id="tbodySelectedFunds1" runat="server" clientidmode="static"></tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-right">Total</th>
                                                        <td class="unitFormatNoSymbolMarketValue tdTotalInvestmentAmount text-right" id="tdTotalInvestmentAmount1" runat="server" clientidmode="static"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <%--<div class="row">
                                                <div class="col-md-4 mb-10">
                                                    <asp:DropDownList ID="ddlPaymentMethodsList" runat="server" CssClass="form-control" ClientIDMode="Static">
                                                        <asp:ListItem Value="" Text="Select Payment Method"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="FPX Payment"></asp:ListItem>
                                                        <!-- <asp:ListItem Value="2" Text="Cheque"></asp:ListItem> -->
                                                        <asp:ListItem Value="3" Text="Proof of Payment"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-4 mb-10">
                                                    <asp:FileUpload ID="fuPaymentMethod" runat="server" CssClass="form-control" ClientIDMode="Static" />
                                                </div>
                                            </div>--%>
                                            <div class="f1-buttons">
                                                <button type="button" class="btn btn-previous">Previous</button>
                                                <asp:Button ID="btnSubmit" runat="server" ClientIDMode="Static" CssClass="btn btn-next btn-infos1" Text="Proceed" OnClick="btnSubmit_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <div class="text-center hide" id="loadAfterSubmitTransaction">
                            <h4>Please wait while we are processing your order...</h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <asp:HiddenField ID="hdnAccountPlan" runat="server" ClientIDMode="Static" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="AccountModal" runat="server">
    <div class="modal fade" id="commonTermsPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="height: 92%;">
        <div class="modal-content trans">
            <div class="modal-body">
                <!-- style="height: calc(100vh - 250px); overflow-y: scroll;" -->
                <div id="commonTermsPopupModalBodyInner">
                    <h5 style="color: #059cce; font-size:16px;" class="text-justify">You are about to buy.  Kindly make sure you have confirmed that:</h5>
                    <ul>
                        <li>you have read and agree to be bound by the terms and conditions set up on the facility;</li>
                        <li>you have read and fully understand the contents of the disclaimer, privacy policy, internet risk, transaction notice and below notice; </li>
                        <li>you are eligible to apply for the units in the fund(s), e.g.  that you have attained 18 years of age unless otherwise allowed under the terms of the Master Prospectus / Prospectus / Information Memorandum and its Supplementary(ies) / Replacement;</li>
                        <li>you have given consent to Apex Investment Services Berhad (“AISB”) to disclose information pertaining to you, including but not limited to proprietary information, to the relevant entities involved in the fund(s) as well as to the Securities Commission Malaysia (“SC”).</li>
                    </ul>
                    <p>Neither AISB nor any of its directors, consultants, representatives or employees accept any liability whatsoever for any direct, indirect or consequences losses (in contract, tort or otherwise) arising from this transaction or its contents contained herein, except to the extent this would be prohibited by law or regulation.</p>
                    <h5>NOTICE</h5>
                    <p>Apex Investment Services Berhad (“AISB”) is responsible for the issuance, circulation or dissemination of the electronic Master Prospectus / Prospectus, electronic Supplementary(ies) / Replacement Master Prospectus / Prospectus, electronic Information Memorandum, electronic Supplementary(ies) / Replacement Information Memorandum, electronic Product Highlights Sheet (collectively referred to as the “Disclosure Document”) and electronic application form. A copy of the Disclosure Document has been registered or lodged with the Securities Commission Malaysia (“SC”). The SC takes no responsibility for the contents and makes no representation on the accuracy or completeness of the Disclosure Document.</p>
                    <p>Investors are advised to read and understand the content of the Disclosure Document before completing the relevant application forms and making any investment decision. A copy of the Disclosure Document is available and investors have the right to request for it. The Disclosure Document can be obtained from our business office, our authorised distributors, our consultants or our representatives. You should also consider the fees and charges involved before investing. All fees and expenses incurred by the Fund is subject to any applicable taxes and / or duties as may be imposed by the government or other authorities from time to time. Prices of units and distributions payable, if any, may go down or up, and past performance of the Fund is not an indication of its future performance. Investors may not get back to the original cost incurred for the investment. Where a unit split / distribution is declared, the issue of additional units / distribution, the NAV per unit will be reduced from pre-unit split NAV / cum-distribution NAV to post-unit split NAV / ex-distribution NAV. Where a unit split is declared, the value of your investment in Malaysian Ringgit will remain unchanged after the distribution of the additional units. Where unit trust loan financing is available, investors are advised to read and understand the contents of the unit trust loan financing risk disclosure statement before deciding to borrow to purchase units. Investors should be aware of the specific risks for the Fund before investing and rely on their own evaluation to assess the merits and risks of the investment. Specific risks and general risks for the Fund is elaborated in the Disclosure Document. Units are issued upon receipt of duly completed relevant application forms referred to and accompanying a copy of the Disclosure Document. The Fund may not be suitable for all and if in doubt, investors should seek independent advice.</p>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-lg-8 col-md-7 text-left">
                        <label class="customcheck">
                            By clicking on the 'Accept' button, I confirm that I have read and accept the above Policy Statement and I wish to proceed.
                    <input type="checkbox" id="chkBuyProceed">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="col-lg-4 col-md-5">
                        <asp:Button ID="btnAcceptTerms" runat="server" CssClass="btn btn-infos1" Text="Accept" OnClick="btnAcceptTerms_Click" ClientIDMode="Static" OnClientClick="return OnAcceptTerms()" />
                        <%--<button type="button" class="btn btn-infos1" id="btnAcceptBuy">Accept</button>--%>
                        <button type="button" class="btn btn-infos1-default hide" id="btnDismissPopupBuy" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-infos1-default" data-dismiss="modal" onclick="javascript:window.location.href='Portfolio.aspx'">Decline</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="riskPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4>Disclaimer</h4>
            </div>
            <div class="modal-body">
                <p>You have decided to select a fund that is not recommended in accordance to your SAT. Please be advised of the risks associated with the products Apex Investments make available to you. You should not invest in or deal in any financial product unless you understand its nature and the extent of your exposure to risk. You should also be satisfied that it is suitable for you in the light of your circumstances and position. Different investment products have varied levels of exposure to risks and to different combinations of risks. Kindly speak to our team to find out more about our products.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-infos1" data-dismiss="modal">Accept</button>
                <button type="button" class="btn btn-infos1-default" data-dismiss="modal" onclick="javascript:window.location.href='Index.aspx'">Decline</button>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hdnTotalQuantity" runat="server" ClientIDMode="Static" />

    <button id="editFundModal" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="display: none">Open Modal</button>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Amount</h4>
                </div>
                <div class="modal-body">
                    <input type="text" id="FundAmount" name="FundAmount" class="form-control" placeholder="Enter your amount here.">
                    <input type="hidden" value="" id="txtFundMinAmount" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="FundUpdate">Update</button>
                    <button type="button" class="btn btn-default" id="FundUpdateClose" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <button id="deleteFundModal" type="button" class="btn btn-info" data-toggle="modal" data-target="#deleteModal" style="display: none">Open Modal</button>

    <div class="modal fade" id="deleteModal" role="document">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Confirmation</h4>
                </div>
                <div class="modal-body">
                    <span>
                        Are you sure you would like to delete this order from your cart?
                    </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="ConfirmationYes">Confirm</button>
                    <button type="button" class="btn btn-secondary" id="ConfirmationCancel" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!--Order Model -->
    <div class="modal fade" id="orderHistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog sett two">
            <div class="modal-content">
                <div class="modal-header sett-modalhead">
                    <h4>Order history</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive" style="border: none!important;">
                                <table class="table table-cell-pad-5 table-font-size-13 table-condensed table-bordered OrderHistoryTable">
                                    <thead id="theadUserOrders" runat="server">
                                        <tr>
                                            <th style="width: 50px"></th>
                                            <th style="width: 50px">S. No</th>
                                            <th>Account No</th>
                                            <th>Order No</th>
                                            <th>Order Type</th>
                                            <th>Order Date</th>
                                            <th>Order Status</th>
                                            <th class="text-right">Investment Amount(MYR)</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyUserOrders" runat="server" clientidmode="Static">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-infos1" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--Order Model end -->
    <asp:HiddenField ID="hdnTermsPop" runat="server" ClientIDMode="Static" Value="1" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="AccountScripts" runat="server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/4.1.0/autoNumeric.js"></script>
    <script type="text/javascript">

        var hdnTermsPop = "0";
        (function ($) {
            hdnTermsPop = $('#hdnTermsPop').val();
            $.fn.inputFilter = function (inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
            };
        }(jQuery));

        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });
        $(document).ready(function () {
            //$('#ddlUserAccountId').change(function () {
            //    if($(this).val() != "")
            //        $('#isUserAccountChange').val('true');
            //});
            $('#txtUnits').keyup(function (event) {

                // skip for arrow keys
                if (event.which >= 37 && event.which <= 40) return;

                // format number
                //$(this).val(function (index, value) {
                //    return value
                //        .replace(/\D/g, "")
                //        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                //        ;
                //});
            });

            $('#commonTermsPopup .modal-body').scroll(function () {
                var disable =
                    (
                        $('#commonTermsPopupModalBodyInner').height() != parseInt($(this).scrollTop()) + $(this).height() - 32
                    )
                    &&
                    (
                        $('#commonTermsPopupModalBodyInner').height() - 1 != parseInt($(this).scrollTop()) + $(this).height() - 32
                    );
                $('#chkBuyProceed').prop('checked', false);
                $('#chkBuyProceed').prop('disabled', disable);
                $('#btnAcceptTerms').prop('disabled', disable);
            });

            document.getElementById("txtAmount").disabled = true;
            //document.getElementById('fuPaymentMethod').style.visibility = "hidden";
            //var tcAcceptBuySession = sessionStorage['tcAcceptBuy'];
            //if (tcAcceptBuySession != undefined && tcAcceptBuySession != null && tcAcceptBuySession != "") {
            //    $('#btnDismissPopupBuy').click();
            //}


            $('#txtAmount').inputFilter(function (value) {
                return /^(\d{0,10})(\.(\d{0,2}))?$/.test(value);
            });
            $('#FundAmount').inputFilter(function (value) {
                return /^(\d{0,10})(\.(\d{0,2}))?$/.test(value);
            });
        });
        $(document).ready(function () {
            $('.show-extended-row').click(function () {
                if ($(this).find('i').hasClass('fa-plus')) {
                    $('.show-extended-row i.fa-minus').each(function (idx, obj) {
                        var ele = $(obj).parents('.show-extended-row');
                        var extendedTRAll = $(ele).parents('tr').next();
                        var extendedRowAll = $(ele).parents('tr').next().find('.extended-row');
                        $(extendedRowAll).slideUp(500, function () {
                            $(extendedTRAll).addClass('hide');
                        });
                        $(ele).find('i').addClass('fa-plus');
                        $(ele).find('i').removeClass('fa-minus');
                    });
                }
                var extendedTR = $(this).parents('tr').next();
                var extendedRow = $(this).parents('tr').next().find('.extended-row');
                if ($(this).find('i').hasClass('fa-plus')) {
                    $(extendedTR).removeClass('hide');
                    $(extendedRow).slideDown();
                    $(this).find('i').removeClass('fa-plus');
                    $(this).find('i').addClass('fa-minus');
                }
                else {
                    $(extendedRow).slideUp(500, function () {
                        $(extendedTR).addClass('hide');
                    });
                    $(this).find('i').removeClass('fa-minus');
                    $(this).find('i').addClass('fa-plus');
                }
            });
        });

        function OpenCommonTermsPopup() {
            if ($('#ddlUserAccountId').val() != '') {

                $('#accSummaryDiv, .selectedAccountDetails').removeClass('hide');
                $('#commonTermsPopup').modal({
                    keyboard: false,
                    backdrop: "static",
                });
            }
            else {
                $('#accSummaryDiv, .selectedAccountDetails').addClass('hide');
            }
        }

        function OnAcceptTerms() {
            if ($('#chkBuyProceed').is(':checked')) {
                $('.loader-bg').fadeIn();
                return true;
            }
            else {
                ShowCustomMessage('Alert', 'Please Read, Understand and Agree to proceed.', '');
                return false;
            }
        }

        if (hdnTermsPop == "1") {
            OpenCommonTermsPopup();
        }
        else {
            if ($('#ddlUserAccountId').val() != '') {
                $('#accSummaryDiv, .selectedAccountDetails').removeClass('hide');
            }
            else {
                $('#accSummaryDiv, .selectedAccountDetails').addClass('hide');
            }
        }
        function openPopup() {
            $('#riskPopup').modal({
                keyboard: false,
                backdrop: "static",
            });
        }
        var isCurFormat;
        function FormatAllCurrency() {
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ' });
            });
            $('.currencyFormatNoSymbol').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: '' });
            });
        }
        FormatAllCurrency();
        var isUniFormat;
        var isUniFormatNoSymbol;
        function FormatAllUnit() {
            $('.unitFormat').each(function () {
                isUniFormat = new AutoNumeric(this, { currencySymbol: '', currencySymbolPlacement: 's', decimalPlaces: '4' });
            });
            $('.unitFormatNoSymbol').each(function () {
                isUniFormatNoSymbol = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '4' });
            });
            $('.unitFormatNoSymbolMarketValue').each(function () {
                isUniFormatNoSymbol = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '2' });
            });
        }
        FormatAllUnit();
        function scroll_to_class(element_class, removed_height) {
            var scroll_to = $(element_class).offset().top - removed_height;
            if ($(window).scrollTop() != scroll_to) {
                $('html, body').stop().animate({ scrollTop: scroll_to }, 0);
            }
        }
        function bar_progress(progress_line_object, direction) {
            var number_of_steps = progress_line_object.data('number-of-steps');
            var now_value = progress_line_object.data('now-value');
            var new_value = 0;
            if (direction == 'right') {
                new_value = now_value + (100 / number_of_steps);
            }
            else if (direction == 'left') {
                new_value = now_value - (100 / number_of_steps);
            }
            progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
        }
        /* Form */
        $('.f1 fieldset:first').fadeIn('slow');
        $('.f1 input[type="text"], .f1 input[type="password"], .f1 input[type="checkbox"], .f1 textarea').on('focus', function () {
            $(this).removeClass('input-error');
        });
        // next step
        $('.f1 .btn-next').on('click', function (e) {
            $('#errorMessage').html('');
            $('#successMessage').html('');
            var parent_fieldset = $(this).parents('fieldset');
            var next_step = true;
            // navigation steps / progress steps
            var current_active_step = $(this).parents('.f1').find('.f1-step.active');
            var progress_line = $(this).parents('.f1').find('.f1-progress-line');
            // fields validation
            if (parent_fieldset.hasClass('fieldsetfundSelectionAndAmount')) {
                if (!$('#chkConfirmFundInformation').is(":checked")) {
                    ShowCustomMessage('Alert', 'Please read, understand & agree to proceed. Thank you.', '');
                    next_step = false;
                }
                else
                    if (CartItems == undefined) {
                        ShowCustomMessage('Alert', 'Please add fund to proceed. Thank you.', '');
                        next_step = false;
                    }
                    else {
                        if (CartItems.length == 0) {
                            ShowCustomMessage('Alert', 'Please add fund to proceed. Thank you.', '');
                            next_step = false;
                        }
                    }
            }
            else
                parent_fieldset.find('input[type="text"], input[type="password"], input[type="checkbox"], textarea, select').each(function () {
                    if ($(this).val() == "") {
                        $(this).addClass('input-error');
                        next_step = false;
                    }
                    //else if ($(this).val() == "3") {
                    //    if ($("#fuPaymentMethod").val() == "") {
                    //        $(this).addClass('input-error');
                    //        next_step = false;
                    //    }
                    //    else {
                    //        $(this).removeClass('input-error');
                    //    }
                    //}
                    else {
                        if ($(this).is(':checkbox')) {
                            if (!$(this).attr('checked')) {
                                $(this).parents('.form-group').addClass('input-error');
                                next_step = false;
                            }
                            else {
                                $(this).parents('.form-group').removeClass('input-error');
                            }
                        }
                        else {
                            $(this).removeClass('input-error');
                        }
                    }
                });
            if (next_step) {
                if (fundCodeSelected != "") {
                    if ($('#ddlFundsList option[data-code="' + fundCodeSelected + '"]').val() != undefined) {
                        $('#ddlFundsList').val($('#ddlFundsList option[data-code="' + fundCodeSelected + '"]').val());
                    }
                    else {
                        $('#rdnNonRecommended[value="rdnNonRecommended"]').prop('checked', true);
                        $('#rdnNonRecommended').click();
                        $('#ddlFundsList').val($('#ddlFundsList option[data-code="' + fundCodeSelected + '"]').val());
                    }

                }
                else {
                    document.getElementById("txtAmount").disabled = true;
                }
                parent_fieldset.fadeOut(400, function () {
                    current_active_step.removeClass('active').addClass('activated').next().addClass('active');
                    bar_progress(progress_line, 'right');
                    $(this).next().fadeIn('slow', function () {
                        var interval = setInterval(function () {
                            console.log('In interval ddlFundsList: ' + $('#ddlFundsList').val());
                            if ($('#ddlFundsList').val() != null && $('#ddlFundsList').val() != "0") {
                                console.log('In interval ddlFundsList: In' + $('#ddlFundsList').val());
                                $('#ddlFundsList').change();
                                clearInterval(interval);
                            }
                        }, 500);
                    });
                    scroll_to_class($('.f1'), 20);
                });
            }
            else {
                e.preventDefault();
            }
        });
        $('.f1 .btn-previous').on('click', function () {
            var current_active_step = $(this).parents('.f1').find('.f1-step.active');
            var progress_line = $(this).parents('.f1').find('.f1-progress-line');
            $(this).parents('fieldset').fadeOut(400, function () {
                current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
                bar_progress(progress_line, 'left');
                $(this).prev().fadeIn();
                scroll_to_class($('.f1'), 20);
            });
        });
        $('.f1').on('submit', function (e) {
            // fields validation
            $(this).find('input[type="text"], input[type="password"], input[type="checkbox"], input[type="file"], textarea').each(function () {
                if ($(this).val() == "") {
                    e.preventDefault();
                    $(this).addClass('input-error');
                }
                else {
                    $(this).removeClass('input-error');
                    $('#loadAfterSubmitTransaction').removeClass('hide');
                    setTimeout(function () {
                        $('.loader-bg').fadeIn();
                    }, 500);
                }
            });
            // fields validation
        });

        function BindCart() {

            $('.tdTotalInvestmentAmount').html(TotalAmount);
            $('#tbodySelectedFunds, #tbodySelectedFunds1').html('');
            //if (document.getElementById('.fundName').text != null) {
            //    var fundname = document.getElementById('.fundName').text;
            //}
            var tbodySelectedFundsHtml = "";
            var trCount = 1;
            if (CartItems.length > 0) {
                $('#cart-table').removeClass('hide');
            }
            $.each(CartItems, function (idx, obj) {
                tbodySelectedFundsHtml += "<tr id='td_" + trCount + "'>";
                tbodySelectedFundsHtml += "<td>" + obj.UtmcFundInformation.FundName + "</td>"
                //tbodySelectedFundsHtml += "<td>" + obj.UtmcFundInformation.FundCode + " - " + obj.UtmcFundInformation.FundName + "</td>"
                tbodySelectedFundsHtml += "<td class='currencyFormatNoMYR text-right'>" + obj.Amount + "</td>"
                tbodySelectedFundsHtml += "<td class='text-right'><div class='btn-group' role='group'><a href='javascript:;' fund-id=" + obj.UtmcFundInformation.Id + " data-fund-min-amount='" + (obj.isExisting == 0 ? obj.UtmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentCash : obj.UtmcFundInformation.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash) + "' data-fund-amount=" + obj.Amount + " data-id=" + obj.Id + " class='btn btn-sm btn-default editFund' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a><a href='javascript:;' data-id=" + obj.Id + " class='btn btn-sm btn-default removeFund' data-toggle='tooltip' title='Remove'><i class='fa fa-trash-o' aria-hidden='true'></i></a></div></td>";
                tbodySelectedFundsHtml += "</tr>";
                trCount++;
            });
            $('#tbodySelectedFunds').html(tbodySelectedFundsHtml);
            tbodySelectedFundsHtml = "";
            $.each(CartItems, function (idx, obj) {
                tbodySelectedFundsHtml += "<tr>";
                tbodySelectedFundsHtml += "<td>" + obj.UtmcFundInformation.FundName + "</td>"
                tbodySelectedFundsHtml += "<td class='currencyFormatNoMYR text-right'>" + obj.Amount + "</td>"
                tbodySelectedFundsHtml += "</tr>";
            });
            $('#tbodySelectedFunds1').html(tbodySelectedFundsHtml);
            ChangeTC();
            FormatAllCurrency();
            FormatAllUnit();
            $('#ddlFundsList').val('0');
            $('#ddlFundsList').change();
            $('#ddlDistributionInstruction').val('3');
            $('#ddlDistributionInstruction').change();
        }
        function ConvertToCurrency(num) {
            return num.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }

        var Term;
        function ChangeTC() {
            Term = "I confirm that I have read and understood the Fund Prospectus/Information Memorandum and Product Highlights Sheet of ";
            $.each(CartItems, function (idx, obj) {
                Term += " " + obj.UtmcFundInformation.FundName + ',';
            });
            Term = Term.substring(0, Term.length - 1);
            document.getElementById("chkConfirmFundInformation").nextSibling.innerHTML = Term;

        }

        var CartItems;
        var TotalAmount;
        $(document).ready(function () {
            if ($('#ddlUserAccountId').val() != null && $('#ddlUserAccountId').val() != "") {
                GetHistoryCart();
            }

            //$('#ddlPaymentMethodsList').change(function () {
            //    var selectedIndex = document.getElementById('ddlPaymentMethodsList').selectedIndex;
            //    if (selectedIndex == 0 || selectedIndex == 1)
            //        document.getElementById('fuPaymentMethod').style.visibility = "hidden";
            //    else
            //        document.getElementById('fuPaymentMethod').style.visibility = "visible";

            //});

            //$('#txtAmount').keyup(function (event) {

            //    // skip for arrow keys
            //    if (event.which >= 37 && event.which <= 40) return;

            //    // format number
            //    $(this).val(function (index, value) {
            //        return value
            //            .replace(/\D/g, "")
            //            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            //            ;
            //    });
            //});

            //$('#FundAmount').keyup(function (event) {

            //    // skip for arrow keys
            //    if (event.which >= 37 && event.which <= 40) return;

            //    // format number
            //    $(this).val(function (index, value) {
            //        return value
            //            .replace(/\D/g, "")
            //            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            //            ;
            //    });
            //});

            $('.btn-previous').click(function () {
                $('#errorMessage').html('');
                $('#successMessage').html('');
            });

            $('#btnAddFund').click(function () {
                $('#errorMessage').html('');
                $('#successMessage').html('');
                var fundId = $('#ddlFundsList').val();
                var amount2 = $('#txtAmount').val();
                var distributionID = $('#ddlDistributionInstruction').val();
                var consultantID = $('#ddlConsultant').val();
                var amount = amount2.replace(/,/g, "");
                var bankId = "0";
                if (distributionID == "1") {
                    bankId = $('#hdnBankId').val();
                }
                if (fundId != "" && amount2 != "" && distributionID != "" && consultantID != "") {
                    $.ajax({
                        url: "BuyFunds.aspx/AddFund",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { fundId: fundId, amount: amount, distributionID: distributionID, bankId: bankId, consultantID: consultantID, userAccountNo: $('#ddlUserAccountId').val() },
                        success: function (data) {
                            if (data.d.IsSuccess) {
                                CartItems = data.d.Data.CartItems;
                                TotalAmount = data.d.Data.TotalAmount;
                                BindCart();
                                //if (isModify == 1) {
                                //$('#successMessage').html('Successfully Modified with your confirmation.');
                                //InsertIntoOrderCard(fundId, amount, distributionID, consultantID, bankId);
                                //}
                                //else {
                                //$('#successMessage').html('Successfully Added.');
                                InsertIntoOrderCard(fundId, amount, distributionID, consultantID, bankId);
                                //}
                                $('#divFundDetails').addClass('hide');
                                $('#cart-table').removeClass('hide');
                            }
                            else {
                                ShowCustomMessage("Error", data.d.Message, "");
                            }
                        }
                    });
                }
                else if (fundId == "") {
                    ShowCustomMessage("Error", "Please select fund.", "");
                }
                else if (amount2 == "") {
                    ShowCustomMessage("Error", "Please enter amount.", "");
                }
                else if (distributionID == "") {
                    ShowCustomMessage("Error", "Please select distribution instruction.", "");
                }
                else if (consultantID == "") {
                    ShowCustomMessage("Error", "Please select agent.", "");
                }
            });

            function InsertIntoOrderCard(Id, Amount, distributionID, consultantID, bankId) {
                $.ajax({
                    type: "GET",
                    url: "BuyFunds.aspx/InsertOderCart",
                    data: { Id: Id, Amount: Amount, distributionID: distributionID, bankId: bankId, consultantID: consultantID, userAccountNo: $('#ddlUserAccountId').val() },
                    dataType: "JSON",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        //$('#hdnTotalQuantity').val(data.d);
                        //console.log("hdnTotalQuantity--------------------------------------");
                        FundDetails(Id);
                        GetFundPerformance(Id);
                        GetHistoryCart();
                    }
                });
            }

            //$('#tbodySelectedFunds').on('click', '.removeFund', function () {
            //    $('#errorMessage').html('');
            //    $('#successMessage').html('');
            //    var id = $(this).attr('data-id');
            //    $.ajax({
            //        url: "BuyFunds.aspx/RemoveFund",
            //        contentType: 'application/json; charset=utf-8',
            //        type: "GET",
            //        dataType: "JSON",
            //        data: { Id: id, userAccountNo: $('#ddlUserAccountId').val() },
            //        success: function (data) {
            //            CartItems = data.d.CartItems;
            //            TotalAmount = data.d.TotalAmount;
            //            BindCart();
            //            $('#successMessage').html('Successfully removed.');
            //        }
            //    });
            //});

            $('#FundUpdate').click(function () {
                $('#errorMessage').html('');
                $('#successMessage').html('');
                var id = newFundEditId;
                var amount2 = document.getElementById("FundAmount").value;
                var minCash = document.getElementById("txtFundMinAmount").value;
                var amount = amount2.replace(/,/g, "");

                if (amount == "") {
                    ShowCustomMessage('Alert', 'Please enter amount!', '');
                    return false;
                }
                if (parseFloat(amount) < parseFloat(minCash)) {
                    ShowCustomMessage('Alert', 'Min Investment Amount is MYR <span class="currencyFormat2dp"> ' + ConvertToCurrency(parseFloat(minCash)) + '</span>', '');
                    return false;
                }
                $.ajax({
                    url: "BuyFunds.aspx/EditFund",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { Id: id, Amount: amount, userAccountNo: $('#ddlUserAccountId').val() },
                    success: function (data) {
                        CartItems = data.d.CartItems;
                        TotalAmount = data.d.TotalAmount;
                        BindCart();
                        //$('#successMessage').html('Successfully Edit.');
                        document.getElementById("FundUpdateClose").click();
                    }
                });
            });


            $('#ConfirmationYes').click(function () {
                $('#errorMessage').html('');
                $('#successMessage').html('');
                var id = $('.removeFund').attr('data-id');
                $.ajax({
                    url: "BuyFunds.aspx/RemoveFund",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { Id: id, userAccountNo: $('#ddlUserAccountId').val() },
                    success: function (data) {
                        CartItems = data.d.CartItems;
                        TotalAmount = data.d.TotalAmount;
                        BindCart();
                        $('#successMessage').html('Successfully removed.');
                        document.getElementById("ConfirmationCancel").click();
                    }
                });
            });

            var newFundEditId;

            $('#tbodySelectedFunds').on('click', '.editFund', function () {
                $('#errorMessage').html('');
                $('#successMessage').html('');
                var id = $(this).attr('data-id');
                var fundID = $(this).attr('fund-id');
                var fundAmt = $(this).attr('data-fund-amount');
                var minfundAmt = $(this).attr('data-fund-min-amount');
                $('#FundAmount').val(fundAmt);
                $('#txtFundMinAmount').val(minfundAmt);
                newFundEditId = id;
                FundDetailsUnitOwned(fundID);
                document.getElementById("editFundModal").click();
            });

            $('#tbodySelectedFunds').on('click', '.removeFund', function () {

                document.getElementById("deleteFundModal").click();
            });

            $('#ddlDistributionInstruction').change(function () {
                var value = $(this).val();
                if (value == "1") {
                    $.ajax({
                        url: "BuyFunds.aspx/CheckBankDetails",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { userAccountNo: $('#ddlUserAccountId').val() },
                        success: function (data) {

                            var maHolderRegRes = data.d;
                            if (maHolderRegRes.IsSuccess) {
                                var td = '<td>' + maHolderRegRes.Data.BankDefIdBanksDef.Name + '</td>';
                                td += '<td>' + maHolderRegRes.Data.AccountName + '</td>';
                                td += '<td>' + maHolderRegRes.Data.BankAccountNo + '</td>';
                                $('#bankDetailsTbody').html('<tr>' + td + '</tr>');
                                //ShowCustomMessage('Info', maHolderRegRes.Data.BankAccountNo, '');
                                $('#bankDetailsDiv').removeClass('hide');
                                $('#hdnBankId').val(maHolderRegRes.Data.Id);
                            }
                            else {
                                $('#bankDetailsTbody').html('');
                                if (data.d.Message == 'Bank details not found') {
                                    ShowCustomMessage('Alert', 'Please bind bank details before proceeding.', "Settings.aspx?AccountNo=" + $("#ddlUserAccountId").val() + "&isPopup=1&Popup=openPopup('bankDetails')&redirectBack=BuyFunds.aspx");
                                }
                                else {
                                    $('#ddlDistributionInstruction').val('3');
                                    ShowCustomMessage('Alert', data.d.Message, '');
                                }
                            }
                        }
                    });

                    //ShowCustomMessage('Alert', '', '');
                }
                else {
                    $('#bankDetailsTbody').html('');
                    $('#bankDetailsDiv').addClass('hide');
                }
            });
        });

        var fundDetails;
        var minInitialInvestmentCashEPF = "0,0";
        var minEpf;
        var minCash;
        var minSubEpf;
        var minSubCash;
        var splits = '';
        var fundCodeSelected = '';

        function CheckNumeric(txt, e) {

            if (window.event) // IE 
            {
                if ((e.keyCode < 48 || e.keyCode > 57) && e.keyCode != 46) {
                    event.returnValue = false;
                    return false;
                }
            }
            else { // Fire Fox
                if ((e.keyCode < 48 || e.keyCode > 57) && e.keyCode != 46) {
                    event.returnValue = false;
                    return false;
                }
            }
            var txtvalue = txt.value;
            var regexp = /^\d+(\.\d{1,2})?$/;
            if (!regexp.test(txtvalue)) {
                return false;
            }
        }

        function FundDetails(Id) {
            $('.loadingDiv').removeClass('hide');
            $('#divFundDetails').removeClass('hide');
            $('#fundperformancetable').removeClass('hide');
            var hdnTotalQuantity = parseFloat($('#hdnTotalQuantity').val());

            if (document.getElementById("rdnNonRecommended").checked)
                document.getElementById("warningLabel").style.display = "block";
            else
                document.getElementById("warningLabel").style.display = "none";


            $.ajax({
                url: "BuyFunds.aspx/GetFundDetails",
                async: true,
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: { 'Id': Id, userAccountNo: $('#ddlUserAccountId').val() },
                success: function (data) {
                    splits = data.d.splits;
                    var json = data.d.UtmcFundInformation;
                    fundDetails = json;
                    $('.fundName').html(json.FundName);
                    $('.salesCharge').html(json.UtmcFundInformationIdUtmcFundCharges[0].InitialSalesChargesPercent + "%");
                    if (json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentEpf == 0) {
                        $('.minimumInitialInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentCash + "</span>");
                        document.getElementById('minimumInitialInvestementTitle').innerHTML = "Minimum Initial Investement (CASH)";
                    }
                    else {
                        $('.minimumInitialInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentCash + "</span>/<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentEpf + "</span>");
                        document.getElementById('minimumInitialInvestementTitle').innerHTML = "Minimum Initial Investement (CASH/EPF)";
                    }
                    if (json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf == 0) {
                        $('.minimumSubsequentInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash + "</span>");
                        document.getElementById('minimumSubsequentInvestementTitle').innerHTML = "Minimum Subsequent Investement (CASH)";
                    }
                    else {
                        $('.minimumSubsequentInvestement').html("<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash + "</span>/<span class='currencyFormatNoDecimal'>" + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf + "</span>");
                        document.getElementById('minimumSubsequentInvestementTitle').innerHTML = "Minimum Subsequent Investement (CASH/EPF)";
                    }
                    minInitialInvestmentCashEPF = json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentCash + "," + json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentEpf;
                    minEpf = json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentEpf;
                    minCash = json.UtmcFundInformationIdUtmcFundDetails[0].MinInitialInvestmentCash;
                    minSubEpf = json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf;
                    minSubCash = json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash;

                    var x = $('#ddlFundsList').val();
                    if (x != 0) {
                        $('.riskCriteria').html($('#ddlFundsList option:selected').attr('data-risk').trim());
                        $('.unitholding').html("<span class='unitFormat'>" + hdnTotalQuantity.toFixed(4) + "</span>");
                        if ($('#ddlFundsList option:selected').attr('data-risk').trim() == 'High Risk') {
                            openPopup();
                        }
                    }
                    $('#hdnMinSubsequentInvestmentCashEPF').val(json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentCash + "," + json.UtmcFundInformationIdUtmcFundDetails[0].MinSubsequentInvestmentEpf);
                    var masterProspectus = $.grep(
                        json.UtmcFundInformationIdUtmcFundFiles,
                        function (obj) {
                            return obj.UtmcFundFileTypesDefId == 1
                        })[0];
                    if (masterProspectus != null) {
                        if (masterProspectus.Url != null) {
                            var a = $('<a />').attr('href', masterProspectus.Url).attr('target', '_blank').html('Click here to View');
                            $('.masterProspectus').html(a);
                        }
                        else {
                            $('.masterProspectus').html('-');
                        }
                    }
                    else {
                        $('.masterProspectus').html('-');
                    }

                    var productHighlightsSheet = $.grep(
                        json.UtmcFundInformationIdUtmcFundFiles,
                        function (obj) {
                            return obj.UtmcFundFileTypesDefId == 3
                        })[0];
                    if (productHighlightsSheet != null) {
                        if (productHighlightsSheet.Url != null) {
                            var a = $('<a />').attr('href', productHighlightsSheet.Url).attr('target', '_blank').html('Click here to View');
                            $('.productHighlightsSheet').html(a);
                        }
                        else {
                            $('.productHighlightsSheet').html('-');
                        }
                    }
                    else {
                        $('.productHighlightsSheet').html('-');
                    }

                    var informationMemorandum = $.grep(
                        json.UtmcFundInformationIdUtmcFundFiles,
                        function (obj) {
                            return obj.Name == "Information Memorandum"
                        })[0];
                    if (informationMemorandum != null) {
                        if (informationMemorandum.Url != null) {
                            var a = $('<a />').attr('href', informationMemorandum.Url).attr('target', '_blank').html('Click here to View');
                            $('.informationMemorandum').html(a);
                            $('#infoMemo').show();
                        }
                        else {
                            $('.informationMemorandum').html('-');
                            $('#infoMemo').hide();
                        }
                    }
                    else {
                        $('.informationMemorandum').html('-');
                        $('#infoMemo').hide();
                    }
                    $('#chkConfirmFundInformation').prop('checked',false);
                    $('#txtAmount').attr('disabled', false);
                    $('#btnAddFund').attr('disabled', false);

                    FormatAllCurrency();
                }
            });
        }
        //$('#FundAmount').keyup(function (event) {

        //    // skip for arrow keys
        //    if (event.which >= 37 && event.which <= 40) return;

        //    // format number
        //    $(this).val(function (index, value) {
        //        return value
        //            .replace(/\D/g, "")
        //            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        //            ;
        //    });
        //});
        $('#rdnRecommended').click(function () {
            if ($(this).is(":checked")) {
                GetFundListByCategory(2);
                $('#warningLabel').addClass('hide');
            }
        });
        $('#rdnNonRecommended').click(function () {
            if ($(this).is(":checked")) {
                GetFundListByCategory(3);
                $('#warningLabel').removeClass('hide');
            }
        });

        $('#rdnRecommended').prop('checked',true);
        $('#rdnRecommended').click();

        $('#ddlFundCategory').change(function () {
            var Id = $('#ddlFundCategory').val();
            GetFundListByCategory(Id);
        });
        function GetFundListByCategory(Id) {
            if ($('#ddlUserAccountId').val() != null && $('#ddlUserAccountId').val() != "") {

                $.ajax({
                    type: "GET",
                    url: "BuyFunds.aspx/GetFundList",
                    data: { Id: Id, userAccountNo: $('#ddlUserAccountId').val() },
                    dataType: "JSON",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {

                        //document.getElementById("ddlFundsList").options.length = 0;
                        //var select = document.getElementById('ddlFundsList');
                        //select[select.length] = new Option('Select Fund', '0');
                        if (data.d.IsSuccess) {
                            if (data.d.Data.Names.length == 0) {
                                if (Id == 2)
                                    ShowCustomMessage('Alert', 'There is no recommended funds.', '');
                                else {
                                    ShowCustomMessage('Alert', 'There is no non-recommended funds.', '');
                                    $('#rdnRecommended').prop('checked',true);
                                    $('#rdnRecommended').click();
                                }
                            }
                            else {
                                $('#ddlFundsList option').remove();
                                $('#ddlFundsList').append('<option value="0">Select</option>');
                                for (i = 0; i < data.d.Data.Names.length; i++) {
                                    $('#ddlFundsList').append('<option data-code="' + data.d.Data.Codes[i] + '" data-risk="' + data.d.Data.Risks[i] + '" value="' + data.d.Data.Ids[i] + '">' + data.d.Data.Names[i] + '</option>');
                                    if ($('#hdnFundId').val() != '' || $('#hdnFundId').val() != null) {
                                        if ($('#hdnFundId').val() == data.d.Data.Codes[i])
                                            $('#ddlFundsList').val(data.d.Data.Ids[i]);
                                    }
                                }
                            }
                            fundCodeSelected = data.d.Data.fundCodeSelected;
                        }
                        else {
                            if (data.d.Message.indexOf('SAT') == -1)
                                ShowCustomMessage('Alert', data.d.Message, '');
                        }
                        //if (data.d.Name.length == 0) {
                        //    if (Id == 2)
                        //        ShowCustomMessage('Alert', 'There is no recommended funds.', '');
                        //    else {
                        //        ShowCustomMessage('Alert', 'There is no non-recommended funds.', '');
                        //        $('#rdnRecommended').prop('checked',true);
                        //        $('#rdnRecommended').click();
                        //    }
                        //}
                        //$('#ddlFundsList option').remove();
                        //$('#ddlFundsList').append('<option value="0">Select</option>');
                        //for (i = 0; i < data.d.Name.length; i++) {
                        //    $('#ddlFundsList').append('<option data-code="' + data.d.Code[i] + '" data-risk="' + data.d.Risk[i] + '" value="' + data.d.Id[i] + '">' + data.d.Name[i] + '</option>');
                        //    if ($('#hdnFundId').val() != '' || $('#hdnFundId').val() != null) {
                        //        if ($('#hdnFundId').val() == data.d.Code[i])
                        //            $('#ddlFundsList').val(data.d.Id[i]);
                        //    }
                        //}

                        //fundCodeSelected = data.d.fundCodeSelected;

                        //$('#hdnTotalQuantity').val(data.d);
                        //FundDetails(Id);
                        //GetFundPerformance(Id);
                    }
                });
            }
        }

        $('#ddlFundsList').change(function () {
            $('#btnAddFund').attr('disabled', true);
            $('#txtAmount').val('');
            $('#txtAmount').attr('disabled', true);
            $('#successMessage').html('');
            $('#errorMessage').html('');
            if ($(this).val() != "" && $(this).val() != null && $(this).val() != "0") {
                var Id = parseInt($(this).val());
                FundDetailsUnitOwned(Id);
                var yourUl = document.getElementById("fundperformancetable");
                yourUl.style.display = '';
            }
            else {
                $('#divFundDetails').addClass('hide');
                $('#fundperformancetable').addClass('hide');
            }
        });
        $('#ddlFundsList').change();
        var isCurFormat;
        function FormatAllCurrency() {
            $('.currencyFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ' });
            });
            $('.currencyFormatNoDecimal').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ', decimalPlaces: '0' });
            });
            $('.currencyFormatNoMYR').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '2' });
            });
            $('.currencyFormatNoSymbol').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '4' });
            });
            $('.unitFormat').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: '', decimalPlaces: '4' });
            });
            $('.currencyFormat2dp').each(function () {
                isCurFormat = new AutoNumeric(this, { currencySymbol: 'MYR ', decimalPlaces: '2' });
            });
        }
        var FundDetailsUnitOwnedCount = 0;
        function FundDetailsUnitOwned(Id) {
            FundDetailsUnitOwnedCount++;
            $.ajax({
                type: "GET",
                url: "BuyFunds.aspx/GetTotalUnits",
                data: { Id: Id, userAccountNo: $('#ddlUserAccountId').val() },
                dataType: "JSON",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d.IsSuccess == 1 || data.d.IsSuccess == '1') {
                        $('#hdnTotalQuantity').val(data.d.totalUnit);
                        if (data.d.EpfOrOther != "EPF") {
                            if (data.d.instruction == 0) {
                                document.getElementById("ddlDistributionInstruction").disabled = false;
                                document.getElementById("ddlDistributionInstruction").value = 3;
                            }
                            else {
                                document.getElementById("ddlDistributionInstruction").disabled = false;
                                document.getElementById("ddlDistributionInstruction").value = (data.d.instruction == 0 ? 3 : data.d.instruction);
                            }
                        } else {
                            document.getElementById("ddlDistributionInstruction").disabled = false;
                            document.getElementById("ddlDistributionInstruction").value = 3;
                        }
                        $('#ddlDistributionInstruction').change();
                        FundDetails(Id);
                        GetFundPerformance(Id);
                    }
                    else {
                        if (FundDetailsUnitOwnedCount <= 3)
                            FundDetailsUnitOwned(Id);
                        else {
                            ShowCustomMessage('Alert', "! Please try again. " + data.d.Message, '');
                            $('#ddlFundsList').val('0');
                        }
                    }
                }
            });
        }

        function GetHistoryCart() {
            $.ajax({
                url: "BuyFunds.aspx/BindFundsFromCartDB",
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: { userAccountNo: $('#ddlUserAccountId').val() },
                success: function (data) {
                    CartItems = data.d.CartItems;
                    TotalAmount = data.d.TotalAmount;
                    //console.log('Cartitems and total amount of investment.');
                    //console.log(data.d.CartItems);
                    //console.log(data.d.TotalAmount);
                    BindCart();
                }
            });
        }

        function GetFundPerformance(Id) {
            $.ajax({
                type: "GET",
                url: "BuyFunds.aspx/GetFundPerformance",
                data: { Id: Id },
                dataType: "JSON",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    $('#weekCValue').html(data.d.w1);
                    $('#monthCValue').html(data.d.m1);
                    $('#month3CValue').html(data.d.m3);
                    $('#month6CValue').html(data.d.m6);
                    $('#yearCValue').html(data.d.y1);
                    $('#year2CValue').html(data.d.y2);
                    $('#year3CValue').html(data.d.y3);
                    $('#year5CValue').html(data.d.y5);
                    $('#year10CValue').html(data.d.y10);

                    $('#weekCValueAvg').html(data.d.w1a);
                    $('#monthCValueAvg').html(data.d.m1a);
                    $('#month3CValueAvg').html(data.d.m3a);
                    $('#month6CValueAvg').html(data.d.m6a);
                    $('#yearCValueAvg').html(data.d.y1a);
                    $('#year2CValueAvg').html(data.d.y2a);
                    $('#year3CValueAvg').html(data.d.y3a);
                    $('#year5CValueAvg').html(data.d.y5a);
                    $('#year10CValueAvg').html(data.d.y10a);

                    $('#fundPricedate').html(data.d.date);
                    //$('#fundPriceLastUpdatedDate').html(data.d.date);

                    $('.loadingDiv').addClass('hide');
                }
            });
        }

    </script>
</asp:Content>
