﻿<%@ Page Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="Warning.aspx.cs" Inherits="DiOTP.WebApp.Warning" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="logoutmessage" id="deactiveteLogout" runat="server" visible="false">
                                <h4>Warning</h4>
                                <p>Account have not activated yet, Please contact the user for more information.</p>
                                <a href="/Login.aspx" class="hidden-md hidden-lg">Login Here</a>
                                <a href="/Index.aspx" class="hidden-sm hidden-xs">Login Here</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="modalPlace" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>