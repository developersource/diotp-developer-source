﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AccountMaster.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="DiOTP.WebApp.Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AccountStyles" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="AccountContent" runat="server">

    <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2">
        <section class="section profile-section">

            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <ol class="breadcrumb">
                            <li><a href="/Portfolio.aspx">eApexIs</a></li>
                            <li class="active">Profile</li>
                        </ol>
                    </div>
                    <div style="text-align: left;">
                        <h3 class="mb-8 pb-12 d-ib mob-fund-head tab-fund-head">Profile</h3>
                    </div>

                    <div class="profile">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="table-head">
                                        <h5 class="mt-0 ff-ls text-center text-uppercase" style="margin-bottom: 2px !important;">eApexIs Profile
                                        </h5>
                                        <small class="pull-right text-right" style="margin-top: -3.5px;" data-toggle="tooltip" data-placement="bottom" title="Last Login">
                                            <small class="pull-right text-right" id="lastLoginSpan" runat="server">
                                                <small id="lastLoginTime" runat="server">2018-02-12 10:01:15</small>
                                                <br />
                                                <small id="lastLoginIP" runat="server">192.168.0.33</small>
                                            </small>
                                        </small>
                                    </div>

                                    <div class="panel-body">
                                        <h5 class="text-uppercase text-primary">Account</h5>
                                        <div class="row profile-details">
                                            <div class="col-md-12">
                                                <strong>Username</strong>
                                                <asp:TextBox ID="txtUsername" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>

                                            <div class="col-md-12">
                                                <strong>Email</strong>
                                                <asp:TextBox ID="txtEmail" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>

                                            <div class="col-md-12">
                                                <strong>Mobile Number</strong>
                                                <asp:TextBox ID="txtMobileNumber" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-head">
                                                <h5 class="mt-0 ff-ls text-uppercase display-inline-block" style="margin-bottom: 2px !important;">Bank Info
                                                </h5>
                                                <a href="/Settings.aspx" class="pull-right text-white" data-toggle="tooltip" data-placement="bottom" title="Edit">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </div>
                                            <div class="panel-body">
                                                <h5 class="text-uppercase text-primary">Bank Details</h5>
                                                <div class="row profile-details" id="bankDetailsRow" runat="server">

                                                    <div class="col-md-12">
                                                        <strong>Bank Name</strong>
                                                        <asp:TextBox ID="txtBankname" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <strong>Account Name</strong>
                                                        <asp:TextBox ID="txtAccountname" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <strong>Account Number</strong>
                                                        <asp:TextBox ID="txtAccountnumber" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                    </div>

                                                </div>
                                                <div class="row profile-details" id="noBankDetailsRow" runat="server">
                                                    <div class="col-md-12">
                                                        <a href="/Settings.aspx" class="btn btn-infos1">Update Bank Details</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="table-head">
                                        <h5 class="mt-0 ff-ls text-uppercase display-inline-block" style="margin-bottom: 2px !important;">MA Profile
                                            <small class="pull-right text-right">
                                                <span class="display-block ma-account-summary-head-label text-capitalize">Acc No. 
                                                    <span class="maHolderRegNoLabel" id="maHolderRegNoLabel" runat="server"></span>
                                                </span>
                                            </small>
                                        </h5>
                                        <a href="/Settings.aspx" class="text-white" data-toggle="tooltip" data-placement="bottom" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </div>


                                    <div class="panel-body">
                                        <h5 class="text-uppercase text-primary">Account</h5>

                                        <div class="row profile-details">
                                            <div class="col-md-6 hide">
                                                <strong>Holder Acc Type</strong>
                                                <div>
                                                    <asp:TextBox ID="txtHolderAccType" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <strong>Holder Class</strong>
                                                <div>
                                                    <asp:TextBox ID="txtHolderClass" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6 hide">
                                                <strong>Holder Ind</strong>
                                                <div>
                                                    <asp:TextBox ID="txtHolerInd" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6 hide">
                                                <strong>Holder Status</strong>
                                                <div>
                                                    <asp:TextBox ID="txtHolderStatus" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <strong>Registered Date</strong>
                                                <div>
                                                    <asp:TextBox ID="txtRegDate" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <h5 class="text-uppercase text-primary">Identity</h5>
                                        <div class="row profile-details">
                                            <div class="col-md-6 hide">
                                                <strong>Identity Ind</strong>
                                                <div>
                                                    <asp:TextBox ID="txtIdentityInd" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <strong id="labelIdentityInd" runat="server">IdNo</strong>
                                                <div>
                                                    <asp:TextBox ID="txtIdNo" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <strong>Old IC</strong>
                                                <div>
                                                    <asp:TextBox ID="txtIdNoOld" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <h5 class="text-uppercase text-primary">Personal</h5>
                                        <div class="row profile-details">
                                            <div class="col-md-6">
                                                <strong>Salutation</strong>
                                                <div>
                                                    <asp:TextBox ID="txtSalutation" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <strong>Name</strong>
                                                <div>
                                                    <asp:TextBox ID="txtName" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <strong>Birth Date</strong>
                                                <div>
                                                    <asp:TextBox ID="txtBirthDate" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <strong>Gender</strong>
                                                <div>
                                                    <asp:TextBox ID="txtSex" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <strong>Telephone No</strong>
                                                <div>
                                                    <asp:TextBox ID="txtTelNo" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <strong>Handphone No</strong>
                                                <div>
                                                    <asp:TextBox ID="txtHandphoneNo" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <strong>Nationality</strong>
                                                <div>
                                                    <asp:TextBox ID="txtNationality" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <strong>Race</strong>
                                                <div>
                                                    <asp:TextBox ID="txtRace" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <strong>Occupation Code</strong>
                                                <div>
                                                    <asp:TextBox ID="txtOccCode" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <strong>No Of Dependants</strong>
                                                <div>
                                                    <asp:TextBox ID="txtNoOfDependants" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <h5 class="text-uppercase text-primary">Residential Address</h5>
                                        <div class="row profile-details">
                                            <div class="col-md-6">
                                                <strong>Address</strong>
                                                <div>
                                                    <asp:TextBox TextMode="MultiLine" ID="txtResAddr" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <h5 class="text-uppercase text-primary">Permanent Address</h5>
                                        <div class="row profile-details">
                                            <div class="col-md-6">
                                                <strong>Permanent Address</strong>
                                                <div>
                                                    <asp:TextBox TextMode="MultiLine" ID="txtPerAddr" runat="server" Text="CS" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="AccountScripts" runat="server">
    <script>
        function GetMAAccountDetails(HolderID, HolderNo) {
            $('#profileLoading').show();
            $.ajax({
                url: "Portfolio.aspx/GetMAAccountDetails",
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: { HolderID: HolderID },
                success: function (data) {
                    var json = data.d;
                    portfolioDetailed = json;
                    $('.maHolderRegNoLabel').html(HolderNo);

                    $('#profileLoading').hide();
                }
            });
        }
        $(document).ready(function () {
            $('#maAccounts').on('click', '.maHolderRegNo', function () {
                var MAHolderRegID = $(this).attr('data-id');
                var MAHolderRegNo = $(this).text();
                GetMAAccountDetails(MAHolderRegID, MAHolderRegNo);
            });
        });
    </script>
</asp:Content>
