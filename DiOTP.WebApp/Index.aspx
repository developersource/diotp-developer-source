﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DiOTPMaster.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="DiOTP.WebApp.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" autocomplete="off">
    <link href="Content/MyCJ/index-styles.css" rel="stylesheet" />

    <section class="announcement">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="announcement-wrapper" id="announcementWrapper" runat="server">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="login">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-5">
                    <div class="welcome-section hide" id="IndexWelcomeSection" runat="server" clientidmode="static">
                        <div>
                            <h3 class="welcome-note">Welcome to eApexIs</h3>
                        </div>
                        <div class="mb-30">
                            <h4 class="welcome-note-2">CREATE WEALTH BY INVESTING.</h4>
                            <h5 class="welcome-note-3">Begin with the right fund manager.</h5>
                        </div>
                        <div>
                            <a href="CorporateAccounts.aspx" class="btn btn-infos1">Open an Account</a>
                            <asp:LinkButton ID="btnLoginOpen" runat="server" ClientIDMode="Static" OnClick="btnLoginOpen_Click" Text="Login/Register" CssClass="btn btn-infos1"></asp:LinkButton>
                        </div>
                    </div>
                    <div class="login-section" id="IndexLoginSection" runat="server" clientidmode="static">
                        <div class="login-head">
                            <h4><strong>LOGIN</strong></h4>
                        </div>
                        <div class="login-content">

                            <div id="divMessage" runat="server" clientidmode="static"></div>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                <asp:TextBox ID="txtEmailOrUsername" ClientIDMode="Static" runat="server" CssClass="form-control" placeholder="Enter your Email ID" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock fa" aria-hidden="true"></i></span>
                                <asp:TextBox ID="txtPassword" runat="server"  TextMode="Password" ClientIDMode="Static" CssClass="form-control" placeholder="Enter your password" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                            <div class="form-group mb-5">
                                <asp:Button ClientIDMode="Static" ID="btnLogin" runat="server" CssClass="btn login-btn btn-block" Text="Login" OnClick="btnLogin_Click" OnClientClick="return loginValidation()" />
                            </div>
                            <div class="mt-5 countdown text-center text-white hide">

                                <label class="label label-warning" id="countDownTime" runat="server" clientidmode="static"></label>
                            </div>
                            <div class="login-fp mb-5" style="height: 20px">
                                <a href="/ForgotPassword.aspx" style="font-size: 12px;"><strong>Forgot Email ID/Password?</strong></a>
                            </div>

                            <%--<%= GetApplicationVersion() %>--%>
                        </div>

                        <div class="login-bottom" style="display: none">
                            <p class="fs-14 mb-2"><a href="#" style="color: #e89928;"><i class="fa fa-caret-right mr-6"></i>General information.</a></p>
                            <p class="fs-14 mb-2"><a href="#" style="color: #e89928;"><i class="fa fa-caret-right mr-6"></i>Whats new here?</a></p>
                            <p class="fs-14 mb-2"><a href="#" style="color: #e89928;"><i class="fa fa-caret-right mr-6"></i>Apex Thank you rewards.</a></p>
                        </div>
                        <div class="login-apply">
                            <div>
                                <p style="color: #fff; font-size: 14px;" class="mb-0"></p>
                                <a href="/OTP-Activation.aspx" style="color: #cca100; width: 105px; text-align: center; font-size: 14px" title="Join OTP">
                                    <b>Existing holder but don't have an account? Register here</b> <%--Don't have an account? Register here --%>
                                </a>

                            </div>
                            <div style="height: 25px;">
                                <a href="/PrivacyPolicy.aspx" style="font-size: 13px">Privacy Policy</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="slider">
        <div class="container-fluid">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" id="carouselInner" runat="server" clientidmode="static">
                    <div class="item active" style="height: calc(100vh - 42px) !important">
                        <img src="/Content/Banner/office-583841_1920.jpg" height:86% weight:100%/>
                        <div class="carousel-caption">
                            <div class="banner-head">
                                <h3 style="color : white;">Account Opening</h3>
                            </div>
                            <div class="banner-content">
                                <p class="line-clamp" style="color: black;">
                                    You can open Account online. <br />
                                    We enabled Digital Onboarding to open account online.
                                </p>
                                <a class="btn btn-readmore" href="CorporateAccounts.aspx">Open now</a>
                            </div>
                        </div>

                    </div>
                    <%--<div class="item active" style="height: calc(100vh - 42px) !important">
                        <img src="Content/Banner/14.png" />
                        <div class="carousel-caption">
                            <div class="banner-head">
                                <h3>Launch OTP</h3>
                            </div>
                            <div class="banner-content">
                                <p class="line-clamp">
                                    Open your world of trading with the grand launching of
                                    eApexIs, Online Trading Platform.
                                    Gear up, start trading beginning Mid 2018.<br />

                                </p>
                                <button type="button" class="btn btn-readmore" onclick="javascript:window.location.href='News-Content.aspx'">Read more</button>
                            </div>
                        </div>

                    </div>
                    <div class="item" style="height: calc(100vh - 42px) !important">
                        <img src="Content/Banner/BANNER-2-Banner.jpg" />
                        <div class="carousel-caption">
                            <div class="banner-head">
                                <h3>Launch FPX</h3>
                            </div>
                            <div class="banner-content">
                                <p class="line-clamp" style="color: #000;">
                                    Trading never been so easy with FPX,
                                    Real time payment partner.
                                    All you need is your Internet Banking Account.
                                    
                                </p>
                                <button type="button" class="btn btn-readmore">Read more</button>

                            </div>

                        </div>
                    </div>
                    <div class="item" style="height: calc(100vh - 42px) !important">
                        <img src="/Content/Banner/9.jpg" />
                        <div class="carousel-caption hidden-lg hidden-md hidden-sm">
                            <div class="banner-head">
                                <h3>Security</h3>
                            </div>
                            <div class="banner-content">
                                <p class="line-clamp">
                                    Taking encrypted security to the next level,
                                    with Two level Authentication, via SMS & Google.
                                    SO you vault over any threat.
                                </p>
                                <button type="button" class="btn btn-readmore">Read more</button>

                            </div>
                        </div>
                    </div>
                    <div class="item" style="height: calc(100vh - 42px) !important">
                        <img src="/Content/Banner/2.jpg" />
                        <div class="carousel-caption">
                            <div class="banner-head">
                                <h3 style="color: #000;">Trading</h3>
                            </div>
                            <div class="banner-content">
                                <p class="line-clamp" style="color: #000;">
                                    Powerful analysis tool with round the clock updates.
                                    View charts, compare funds, review prices and manymore.
                                </p>
                                <button type="button" class="btn btn-readmore">Read more</button>

                            </div>
                        </div>
                    </div>--%>
                </div>

                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

                <ul class="nav nav-pills nav-justified nav-my" id="carouselThumbs" runat="server">
                    <li data-target="#myCarousel" data-slide-to="0" class="active">
                        <a href="#">
                            <span class="tab-head">Account Opening</span><br />
                            <span class="tab-cont">You can open Account online.</span>
                        </a>
                    </li>
                    <%--<li data-target="#myCarousel" data-slide-to="0" class="active">
                        <a href="#">
                            <span class="tab-head">Launch OTP</span><br />
                            <span class="tab-cont">Open your world of trading with the grand launching of eApexIs.</span>
                        </a></li>
                    <li data-target="#myCarousel" data-slide-to="1">
                        <a href="#">
                            <span class="tab-head">Launch FPX</span><br />
                            <span class="tab-cont">Trading never been so easy with FPx, your real time payment partner.</span>
                        </a></li>
                    <li data-target="#myCarousel" data-slide-to="2">
                        <a href="#">
                            <span class="tab-head">Security</span><br />
                            <span class="tab-cont">Taking encrypted security to the next level with Two Level Authentication.</span>
                        </a></li>
                    <li data-target="#myCarousel" data-slide-to="3">
                        <a href="#">
                            <span class="tab-head">Trading</span><br />
                            <span class="tab-cont">Powerful analysis tool with round the clock updates.</span>
                        </a></li>--%>
                </ul>
            </div>
        </div>
    </section>



    <asp:HiddenField ID="hdnLocalIPAddress" runat="server" Value="0" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnLoginLocked" runat="server" Value="0" ClientIDMode="Static" />
    <%--<asp:HiddenField ID="hdnLoginLockTimeInSeconds" runat="server" Value="0" ClientIDMode="Static" />--%>
    <asp:HiddenField ID="hdnLoginAttempts" runat="server" Value="0" ClientIDMode="Static" />
</asp:Content>

<asp:Content ContentPlaceHolderID="modalPlace" ID="modalHolder" runat="server">
    <div class="modal fade" id="modalAccountLockedPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document" style="width: 350px">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Unlock Your Account</h4>
                </div>
                <div class="modal-body">
                    <div class="text-center text-danger mb-10">
                        <small id="msg" runat="server" clientidmode="static"><i class='fa fa-close icon'></i>Your account is locked.</small>
                    </div>
                    <div id="unlockMessage" runat="server" clientidmode="static">
                        <div class="mb-10">
                            Too many invalid login attempts. Unlock your account to Proceed.
                        </div>
                        <div class="text-center mb-5">
                            <small>You can click 'Proceed' below to unlock.</small>
                        </div>
                        <button id="unlock-btn" type="button" class="btn btn-primary btn-block">Proceed</button>
                    </div>
                    <div class="row hide" id="unlock-input">
                        <div class="col-md-12">
                            <div id="divIC" class="input-group">
                                <span class="input-group-addon"><i class="fa fa-keyboard-o fa" aria-hidden="true"></i></span>
                                <asp:TextBox ID="txtICNo" runat="server" CssClass="form-control" placeholder="NRIC / OLD NRIC / PASSPORT *" ClientIDMode="Static"></asp:TextBox>
                            </div>

                            <div id="divMA" class="input-group">
                                <span class="input-group-addon"><i class="fa fa-keyboard-o fa" aria-hidden="true"></i></span>
                                <asp:TextBox ID="txtMANumber" runat="server" CssClass="form-control" placeholder="Enter your MasterAccount Number *" ClientIDMode="Static"></asp:TextBox>
                            </div>
                            <div id="divEmail" class="input-group">
                                <span class="input-group-addon"><i class="fa fa-keyboard-o fa" aria-hidden="true"></i></span>
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="Enter your Email *" ClientIDMode="Static"></asp:TextBox>
                            </div>
                            <div class="form-group mb-10">
                                <br />
                                <div id="captcha" class="g-recaptcha text-center" data-sitekey="6LfXd58UAAAAAFneK1MXlm-Z5k9Y_rA0wC5VV4X7" runat="server"></div>
                                <br />
                                <asp:Button ID="btnRequestCode" runat="server" CssClass="btn login-btn btn-block" OnClientClick="return clientValidation()" OnClick="btnRequestCode_Click" Text="Submit" Enabled="true" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnAccountLockedPop" runat="server" Value="0" ClientIDMode="Static" />
</asp:Content>

<asp:Content ContentPlaceHolderID="Scripts" runat="server">
    <script src='https://www.google.com/recaptcha/api.js' type="text/javascript"></script>
    <script src="Content/MyCJ/index-scripts.js"></script>

</asp:Content>
