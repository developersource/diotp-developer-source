﻿
function StopLoading() {
    $('.loader-bg').fadeOut();
}
function helpBankAccount() {
    ShowCustomGuideMessage("Acceptable bank account number length", $(".bank-guide-popup").html(), "");
}
//function setCustomCookie(cname, cvalue, exdays) {
//    var d = new Date();
//    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
//    var expires = "expires=" + d.toUTCString();
//    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
//}
//function getCustomCookie(cname) {
//    var name = cname + "=";
//    var decodedCookie = decodeURIComponent(document.cookie);
//    var ca = decodedCookie.split(';');
//    for (var i = 0; i < ca.length; i++) {
//        var c = ca[i];
//        while (c.charAt(0) == ' ') {
//            c = c.substring(1);
//        }
//        if (c.indexOf(name) == 0) {
//            return c.substring(name.length, c.length);
//        }
//    }
//    return "";
//}




//var closed = getCustomCookie('closed');
//alert(document.cookie);
//alert(closed);
//if (closed != "" && closed == '1') {
//    alert("closed");
//} else {
//    alert("not closed");
//}
//window.addEventListener('beforeunload', function (e) {
//    setCustomCookie('closed', '1', 365);
//}); 

if (window.performance) {
    //console.info("window.performance works fine on this browser");
}
if (performance.navigation.type == 1) {
    //console.info("This page is reloaded");
} else {
    //console.info("This page is not reloaded");
}

function print_nav_timing_data() {


    // Use getEntriesByType() to just get the "navigation" events
    //console.log("event.currentTarget.performance.navigation.type: " + performance.navigation.type);
    var perfEntries = performance.getEntriesByType("navigation");

    for (var i = 0; i < perfEntries.length; i++) {
        //console.log("= Navigation entry[" + i + "]");
        var p = perfEntries[i];
        // dom Properties
        //console.log("DOM content loaded = " + (p.domContentLoadedEventEnd - p.domContentLoadedEventStart));
        //console.log("DOM complete = " + p.domComplete);
        //console.log("DOM interactive = " + p.interactive);

        // document load and unload time
        //console.log("document load = " + (p.loadEventEnd - p.loadEventStart));
        //console.log("document unload = " + (p.unloadEventEnd - p.unloadEventStart));

        // other properties
        //console.log("type = " + p.type);
        //console.log("redirectCount = " + p.redirectCount);


        if (p.type == 'navigate') {

        }
        else if (p.type == 'reload') {

        }
        else if (p.type == 'back_forward') {
            $.ajax({
                url: "Logout.aspx/BrowserClose",
                contentType: 'application/json; charset=utf-8',
                type: "GET",
                dataType: "JSON",
                data: function (data) {
                    return data;
                },
                success: function (data) {
                    var pageName = window.location.pathname;
                    if (pageName.indexOf('/') != -1)
                        pageName = pageName.replace('/', '');
                    if (data.d.Message == 'has-session') {
                        //ShowCustomMessage('Your Session has expired', 'For security reasons, we have disabled double clicks and Back, Forward and Refresh tabs of the browser. Also, the session will expire automatically, if the browser window is idle for a long time.<br/>If the problem persists, please try again after clearing the Temporary Files from your web browser.', '/Index.aspx');
                        ShowCustomMessage('Your Session has expired', 'For security reasons, we have disabled Refresh tabs of the browser. Also, the session will expire automatically, if the browser window is idle for a long time.<br/>If the problem persists, please try again after clearing the Temporary Files from your web browser.', '/Index.aspx');
                    }
                }
            });
        }
        else if (p.type == 'prerender') {

        }

    }
}
print_nav_timing_data();



//window.history.pushState(null, "", window.location.href);
//window.onpopstate = function () {
//    window.history.pushState(null, "", window.location.href);
//};

//var checkCloseX = 0;
//$(document).ready(function () {

//    $(document).mousemove(function (e) {
//        console.log(e.pageY);
//        if (e.pageY <= 5) {
//            checkCloseX = 1;
//        }
//        else {
//            checkCloseX = 0;
//        }
//    });
//    $(document).mousedown(function (e) {
//        if (e.pageY <= 5) {
//            checkCloseX = 1;
//        }
//        else {
//            checkCloseX = 0;
//        }
//    });
//    $(document).mouseup(function (e) {
//        if (e.pageY <= 5) {
//            checkCloseX = 1;
//        }
//        else {
//            checkCloseX = 0;
//        }
//    });
//});
//$(window).unload(function () {
//    if (checkCloseX == 1) {
//        checkCloseX = 0;
//        $(window).unbind('unload');
//        $.ajax({
//            url: "Logout.aspx/BrowserClose",
//            contentType: 'application/json; charset=utf-8',
//            type: "GET",
//            dataType: "JSON",
//            data: function (data) {
//                return data;
//            },
//            success: function (data) {
//                checkCloseX = 0;
//            }
//        });
//    }
//});  

$('body').keypress(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if ((keycode == '13' || keycode == '32') && !$('#popup1').hasClass('hide')) {
        if ($('#custom-popup-close') != undefined)
            $('#custom-popup-close').click();
        //if ($('#custom-popup-confirmation-ok') != undefined)
        //    $('#custom-popup-confirmation-ok').click();
    }
});

//function getCookie(cname) {
//    var name = cname + "=";
//    var decodedCookie = decodeURIComponent(document.cookie);
//    var ca = decodedCookie.split(';');
//    for (var i = 0; i < ca.length; i++) {
//        var c = ca[i];
//        while (c.charAt(0) == ' ') {
//            c = c.substring(1);
//        }
//        if (c.indexOf(name) == 0) {
//            return c.substring(name.length, c.length);
//        }
//    }
//    return "";
//}
//function setCookie(cname, cvalue, exdays) {
//    var d = new Date();
//    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
//    var expires = "expires=" + d.toUTCString();
//    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
//}
//var tabCount = 0;
//var tabCountCookie = getCookie("tabs_count");
//if (tabCountCookie != "")
//    tabCount = parseInt(tabCountCookie);
//tabCount++;
//setCookie("tabs_count", tabCount, 1);
//if (tabCount == 2) {
//    setCookie("tabs_count", tabCount - 1, 1);
//    ShowCustomMessage('Alert', 'Concurrent tab detected.', 'close-tab');
//    //window.close();
//}
//window.addEventListener("beforeunload", function (e) {
//    if (tabCount < 2) {
//        var tabCount = 0;
//        var tabCountCookie = getCookie("tabs_count");
//        if (tabCountCookie != "")
//            tabCount = parseInt(tabCountCookie);
//        setCookie("tabs_count", tabCount - 1, 1);
//    }
//});

function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

function filterFunction() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    div = document.getElementById("myDropdown");
    a = div.getElementsByTagName("a");
    for (i = 0; i < a.length; i++) {
        if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
            a[i].style.display = "";
        } else {
            a[i].style.display = "none";
        }
    }
}


function clientLoad() {
    setTimeout(function () {
        $('.loader-bg').fadeIn();
    }, 500);
}

function TranslateLanguage(selectedLanguage) {
    var pageName = window.location.pathname;
    if (pageName.indexOf('/') != -1)
        pageName = pageName.replace('/', '');
    var date = new Date();
    $.getJSON("/language/langDir.json?" + date.getTime(), function (data) {
        var langDir = data;
        $.each(langDir, function (idx, obj) {
            if (obj.TextId == "0") {
                $("*").each(function (idx, tag) {
                    var tagType = $(tag).attr('type');
                    if (tagType == undefined || tagType == "button") {
                        if ($(tag).html().indexOf('<') == -1) {
                            //if (!$(tag).hasClass('text-uppercase'))
                            //    $(tag).addClass('text-capitalize');
                            if ($(tag).text().toLowerCase() == obj.TextEn.trim().toLowerCase()) {
                                if (selectedLanguage == 'en')
                                    $(tag).text(obj.TextEn);
                                if (selectedLanguage == 'bm')
                                    $(tag).text(obj.TextMs);
                                if (selectedLanguage == 'zh')
                                    $(tag).text(obj.TextZh);
                            }
                        }
                    }
                    if (tagType == "submit") {
                        if ($(tag).val().toLowerCase() == obj.TextEn.toLowerCase()) {
                            if (selectedLanguage == 'en')
                                $(tag).val(obj.TextEn);
                            if (selectedLanguage == 'bm')
                                $(tag).val(obj.TextMs);
                            if (selectedLanguage == 'zh')
                                $(tag).val(obj.TextZh);
                        }
                    }
                });
            }

            if (obj.TextPage == pageName) {
                $("*").each(function (idx, tag) {
                    var tagType = $(tag).attr('type');
                    if (tagType == undefined || tagType == "button") {
                        if ($(tag).html().indexOf('<') == -1) {
                            //if (!$(tag).hasClass('text-uppercase'))
                            //    $(tag).addClass('text-capitalize');
                            if ($(tag).text().toLowerCase() == obj.TextEn.trim().toLowerCase()) {
                                if (selectedLanguage == 'en')
                                    $(tag).text(obj.TextEn);
                                if (selectedLanguage == 'bm')
                                    $(tag).text(obj.TextMs);
                                if (selectedLanguage == 'zh')
                                    $(tag).text(obj.TextZh);
                            }
                        }
                    }
                    if (tagType == "submit") {
                        if ($(tag).val().toLowerCase() == obj.TextEn.toLowerCase()) {
                            if (selectedLanguage == 'en')
                                $(tag).val(obj.TextEn);
                            if (selectedLanguage == 'bm')
                                $(tag).val(obj.TextMs);
                            if (selectedLanguage == 'zh')
                                $(tag).val(obj.TextZh);
                        }
                    }
                });
            }
        });
    });
}

$(document).ready(function () {
    $("body").dblclick(function (e) {
        //console.log('double-click');
        e.preventDefault();
        e.stopPropagation();
    });
    $('body').on('selectstart', function (event) {
        event.preventDefault();
    });
    var selectedLanguage = $('#languageCode').text().toLowerCase();
    $('a[href="/Index.aspx?languageSelected=' + selectedLanguage + '"]').addClass('active');
    TranslateLanguage(selectedLanguage);

    //console.log($('.navbar-nav li a[href="' + window.location.pathname + '"]').text().indexOf('Portfolio'));
    if ($('.navbar-nav li a[href="' + window.location.pathname + '"]').text().indexOf('Portfolio') == -1) {
        $('.navbar-nav li a[href="' + window.location.pathname + '"]').parent('li').addClass('active');
    }
    $('body').click(function () {
        $("#myDropdown").removeClass("show");
    });

    //$('body').on('click', '.custom-notification>i.fa', function () {
    //    $(this).parent('.custom-notification').fadeOut(1000, function () {
    //        $(this).parent('.custom-notification').remove();
    //    });
    //});
    $.ajax({
        url: "Fund-Comparison.aspx/GetFunds",
        contentType: 'application/json; charset=utf-8',
        type: "GET",
        dataType: "JSON",
        data: function (data) {
            return data;
        },
        success: function (data) {
            var json = data.d;
            var html = "";
            var fundsHTML = '';
            if (json.IsSuccess) {
                $.each(json.Data, function (idx, y) {
                    fundsHTML += '<li value="' + y.Id + '"><a href="/Fund-Information.aspx?fundCode=' + y.FundCode + '">' + y.FundName + '</a></li>';
                });
                $('#fundSearchList').html(fundsHTML);
                $('[data-value="6"]').click();
            }
            else {
                ShowCustomMessage('Exception', json.Message, '');
            }
        }
    });

    $('#custom-popup-close').click(function () {
        $('.custom-popup').addClass('hide');
        $('#custom-popup-heading').html('');
        $('#custom-popup-content').html('');
        var target = $('#custom-popup-close').attr('data-target');
        if (target != '') {
            if (target == 'close-tab')
                window.close();
            else {
                $('.custom-popup').removeClass('hide');
                $('.custom-popup .popup').addClass('hide');
                $('.loader-bg').fadeIn();
                window.location.href = target;
            }
        }
        var focusId = $('#custom-popup-close').attr('data-focusId');
        if (focusId != undefined && focusId != '') {
            $('#' + focusId).focus();
        }

    });

    $('#custom-popup-guide-close').click(function () {
        $('.custom-popup-guide').addClass('hide');
        $('#custom-popup-guide-heading').html('');
        $('#custom-popup-guide-content').html('');
    });

    $('#custom-popup-confirmation-cancel').click(function () {
        $('.custom-popup-confirmation').addClass('hide');
        $('#custom-popup-confirmation-heading').html('');
        $('#custom-popup-confirmation-content').html('');
    });

});

function ShowCustomMessage(title, message, target, btnName, focusId) {
    $('.custom-popup').removeClass('hide');
    $('#custom-popup-heading').html(title);
    $('#custom-popup-content').html(message);
    $('#custom-popup-close').attr('data-target', target);
    $('#custom-popup-close').attr('data-focusId', focusId);
    $('#custom-popup-close').focus();
    if (btnName != undefined && btnName == 'Accept') {
        $('#custom-popup-close').html(btnName);
    }
    else {
        $('#custom-popup-close').html('Ok');
    }
}

function ShowCustomConfirmationMessage(title, message, target) {
    $('.custom-popup-confirmation').removeClass('hide');
    $('#custom-popup-confirmation-heading').html(title);
    $('#custom-popup-confirmation-content').html(message);
    $('#custom-popup-confirmation-ok').attr('data-target', target);
    $('#custom-popup-confirmation-ok').focus();
}

function ShowCustomGuideMessage(title, message, target) {
    $('.custom-popup-guide').removeClass('hide');
    $('#custom-popup-guide-heading').html(title);
    $('#custom-popup-guide-content').html(message);
    $('#custom-popup-guide-close').attr('data-target', target);
    $('#custom-popup-guide-close').focus();
}



var idleTime = 0;
$(document).ready(function () {
    if ($('#liLogin').text() == "") {
        //Increment the idle time counter every minute.
        if ($('#impersonatedUserLabel').html() == undefined) {
            //console.log('session started');
            var idleInterval = setInterval(timerIncrement, 60000); // 1 minute
        }
        //Zero the idle timer on mouse movement.
        $(this).mousemove(function (e) {
            idleTime = 0;
            //console.log('session time set to ' + idleTime);
        });
        $(this).keypress(function (e) {
            idleTime = 0;
            //console.log('session time set to ' + idleTime);
        });
    }

});
function timerIncrement() {
    idleTime = idleTime + 1;
    //console.log('session time increased to ' + idleTime);
    if (idleTime > 19) { // 20 minutes
        //console.log('session time ended ' + idleTime);
        if ($('#impersonatedUserLabel').html() == undefined) {
            window.location.href = "Logout.aspx?sessionlogout=1";
        }
    }
}