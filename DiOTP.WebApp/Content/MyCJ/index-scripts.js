﻿    function loginValidation() {
            var isValid = true;
            var txtEmailOrUsername = $('#txtEmailOrUsername').val();
            var txtPassword = $('#txtPassword').val();
            if (!txtEmailOrUsername) {
        $('#txtEmailOrUsername').css({
            "borderBottom": "1px solid red",
            //"background": "#FFCECE"
        });
                isValid = false;
            } else {
        $('#txtEmailOrUsername').css({
            "borderBottom": "",
            //"background": "LightBlue"
        });

            }
            if (!txtPassword) {
        $('#txtPassword').css({
            "borderBottom": "1px solid red",
            //"background": "#FFCECE"
        });
                isValid = false;
            } else {
        $('#txtPassword').css({
            "borderBottom": "",
            //"background": "LightBlue"
        });

            }
            if (isValid) {
        setTimeout(function () {
            $('.loader-bg').fadeIn();
        }, 500);
            }
            return isValid;
        }
        function clientValidation() {
            var isValid = true;
            var txtICNo = $('#txtICNo').val();
            if (!txtICNo) {
        $('#divIC').addClass("required-input");
                isValid = false;
            } else {
        $('#divIC').removeClass("required-input");
            }
            var txtMANumber = $('#txtMANumber').val();
            if (!txtMANumber) {
        $('#divMA').addClass("required-input");
                isValid = false;
            } else {
        $('#divMA').removeClass("required-input");
            }
            var txtEmail = $('#txtEmail').val();
            if (!txtEmail) {
        $('#divEmail').addClass("required-input");
                isValid = false;
            } else {
        $('#divEmail').removeClass("required-input");
            }
            if (!isValid) {
        $('#msg').addClass('text-danger').html("<i class='fa fa-exclamation-triangle'></i> Please enter required(*) fields.");

            }
            else {
        setTimeout(function () {
            $('.loader-bg').fadeIn();
        }, 500);
            }
            return isValid;
        }


    function secondsTimeSpanToHMS(s) {
            var h = Math.floor(s / 3600); //Get whole hours
            s -= h * 3600;
            var m = Math.floor(s / 60); //Get remaining minutes
            s -= m * 60;
            return h + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s); //zero padding on minutes and seconds
        }
        //var hdnLoginLockTimeInSeconds = parseInt($('#hdnLoginLockTimeInSeconds').val());
        var hdnAccountLockedPop = parseInt($('#hdnAccountLockedPop').val());
        var hdnLoginAttempts = parseInt($('#hdnLoginAttempts').val());
        var hdnLoginLocked = parseInt($('#hdnLoginLocked').val());
        //function StartTimer() {
        //    //var timer2 = secondsTimeSpanToHMS(hdnLoginLockTimeInSeconds);
        //    //var timer2 = "5:01";
        //    var interval = setInterval(function () {
        //        //var timer = timer2.split(':');
        //        //by parsing integer, I avoid all extra string processing
        //        var hours = parseInt(timer[0], 10);
        //        var minutes = parseInt(timer[1], 10);
        //        var seconds = parseInt(timer[2], 10);
        //        --seconds;
        //        minutes = (seconds < 0) ? --minutes : minutes;
        //        hours = (minutes < 0) ? --hours : hours;
        //        if (hours < 0) {
        //            clearInterval(interval);
        //            $('.countdown').removeClass('hide');
        //            $('.countdown').html('Your Can Login Now.');
        //            $('#btnLogin').removeAttr('disabled');
        //            $('#divMessage').html("");
        //            $('#hdnMobilePinRequested').val(0);
        //        }
        //        else {
        //            seconds = (seconds < 0) ? 59 : seconds;
        //            seconds = (seconds < 10) ? '0' + seconds : seconds;
        //            minutes = (minutes < 0) ? 59 : minutes;
        //            minutes = (minutes < 10) ? '0' + minutes : minutes;
        //            hours = (hours < 10) ? '0' + hours : hours;
        //            $('#countDownTime').html(hours + ':' + minutes + ':' + seconds);
        //            timer2 = hours + ':' + minutes + ':' + seconds;
        //        }
        //    }, 1000);
        //}


        $(document).ready(function () {
            $("#txtPassword").keyup(function () {
                $("#divMessage").html("");
            });

            $('#unlock-btn').click(function () {
                $('#msg').html('');
                $('#unlockMessage').addClass('hide');
                $('#unlock-input').removeClass('hide');
            });

            if (hdnAccountLockedPop == 1) {
                $('#modalAccountLockedPop').modal({
                    keyboard: false,
                    backdrop: "static"
                });
            }
            if (hdnLoginAttempts >= 3 && hdnLoginLocked == 1) {
                //$('#btnLogin').attr('disabled', 'disabled');
                $('#divMessage').html("<i class='fa fa-close icon'></i> Too many attempts. Account is Locked.");
                //StartTimer();
            }
            $('#myCarousel').carousel({
                interval: 3000
            });

            var clickEvent = false;
            $('#myCarousel').on('click', '.nav-my a', function () {
                clickEvent = true;
                $('.nav-my li').removeClass('active');
                $(this).parent().addClass('active');
            }).on('slid.bs.carousel', function (e) {
                if (!clickEvent) {
                    var count = $('.nav-my').children().length - 1;
                    var current = $('.nav-my li.active');
                    current.removeClass('active').next().addClass('active');
                    var id = parseInt(current.data('slide-to'));
                    if (count == id) {
                        $('.nav-my li').first().addClass('active');
                    }
                }
                clickEvent = false;
            });
            //announcement
            $('.announcement-wrapper .announcement-slidetext:gt(0)').hide();
            setInterval(function () {
                console.log($('.announcement-wrapper .announcement-slidetext').length);
                if ($('.announcement-wrapper .announcement-slidetext').length > 1) {
                    $('.announcement-wrapper .announcement-slidetext:first-child').fadeOut(3000).next('.announcement-slidetext').fadeIn(3000)
                        .end().appendTo('.announcement-wrapper');
                }
                else {
                    $('.announcement-wrapper .announcement-slidetext:first-child').fadeOut(3000);
                    $('.announcement-wrapper .announcement-slidetext:first-child').fadeIn(3000)
                    //.end().appendTo('.announcement-wrapper');
                }
            }, 5000);
            //setTimeout(fadeOutAnnouncement, 5000000);
        });
        //function fadeOutAnnouncement() {
        //    $(".announcement").fadeOut(1000000);
        //    setTimeout(fadeInAnnouncement, 4000000);
        //}
        //function fadeInAnnouncement() {
        //    $(".announcement").fadeIn(100000000);
        //    setTimeout(fadeOutAnnouncement, 400000000);
        //}
        /**
        * Get the user IP throught the webkitRTCPeerConnection
        * @param onNewIP {Function} listener function to expose the IP locally
        * @return undefined
        */
        function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
            //compatibility for firefox and chrome
            var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
            var pc = new myPeerConnection({
                iceServers: []
            }),
                noop = function () { },
                localIPs = {},
                ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
                key;

            function iterateIP(ip) {
                if (!localIPs[ip]) onNewIP(ip);
                localIPs[ip] = true;
            }

            //create a bogus data channel
            pc.createDataChannel("");

            // create offer and set local description
            pc.createOffer(function (sdp) {
                sdp.sdp.split('\n').forEach(function (line) {
                    if (line.indexOf('candidate') < 0) return;
                    line.match(ipRegex).forEach(iterateIP);
                });

                pc.setLocalDescription(sdp, noop, noop);
            }, noop);

            //listen for candidate events
            pc.onicecandidate = function (ice) {
                if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
                ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
            };
        }

        // Usage

        getUserIP(function (ip) {
        document.getElementById("hdnLocalIPAddress").value = ip;
        });
