﻿        function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
            //compatibility for firefox and chrome
            var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
            var pc = new myPeerConnection({
                iceServers: []
            }),
                noop = function () { },
                localIPs = {},
                ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
                key;

            function iterateIP(ip) {
                if (!localIPs[ip]) onNewIP(ip);
                localIPs[ip] = true;
            }

            //create a bogus data channel
            pc.createDataChannel("");

            // create offer and set local description
            pc.createOffer(function (sdp) {
                sdp.sdp.split('\n').forEach(function (line) {
                    if (line.indexOf('candidate') < 0) return;
                    line.match(ipRegex).forEach(iterateIP);
                });

                pc.setLocalDescription(sdp, noop, noop);
            }, noop);

            //listen for candidate events
            pc.onicecandidate = function (ice) {
                if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
                ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
            };
        }

        // Usage

        getUserIP(function (ip) {
            document.getElementById("hdnLocalIPAddress").value = ip;
        });

        $('form').on('submit', function () {
            setTimeout(function () {
                $('.loader-bg').fadeIn();
            }, 500);
        });

        //ShowCustomMessage('Note', 'To be an investor, you must be Malaysian / Foreigner, age 18 & above and residing in Malaysia. Please have your NRIC/Passport ready for registration.', '');
        var hdnSMSExpirationTimeForAOInSeconds = parseInt($('#hdnSMSExpirationTimeForAOInSeconds').val());
        function secondsTimeSpanToHMS(s) {
            var h = Math.floor(s / 3600); //Get whole hours
            s -= h * 3600;
            var m = Math.floor(s / 60); //Get remaining minutes
            s -= m * 60;
            return h + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s); //zero padding on minutes and seconds
        }
        function helpBank() {
            ShowCustomGuideMessage("Sample Signature", $(".guide-popup").html(), "");
        }
        (function ($) {
            hdnTermsPop = $('#hdnTermsPop').val();
            $.fn.inputFilter = function (inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
            };
        }(jQuery));
        function StartTimer() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSExpirationTimeForAOInSeconds);
            //var timer2 = "5:01";
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    $('#countdownLabelText').html('OTP Expired. Please Request Again.');
                    $('#countDownTime').addClass('hide');
                    $('#countDownTime').html('');
                    $('#lblInfo').html('');
                    $('#btnRequest').removeAttr('disabled');
                }
                else {
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#countDownTime').html(hours + ':' + minutes + ':' + seconds);
                    timer2 = hours + ':' + minutes + ':' + seconds;
                    $('#countDownTime').removeClass('hide');
                }
            }, 1000);
        }
        function StartEmailTimer() {
            var timer2 = secondsTimeSpanToHMS(hdnSMSExpirationTimeForAOInSeconds);
            //var timer2 = "5:01";
            var interval = setInterval(function () {
                var timer = timer2.split(':');
                //by parsing integer, I avoid all extra string processing
                var hours = parseInt(timer[0], 10);
                var minutes = parseInt(timer[1], 10);
                var seconds = parseInt(timer[2], 10);
                --seconds;
                minutes = (seconds < 0) ? --minutes : minutes;
                hours = (minutes < 0) ? --hours : hours;
                if (hours < 0) {
                    clearInterval(interval);
                    $('#emailCountdownLabelText').html('OTP Expired. Please Request Again.');
                    $('#emailCountDownTime').addClass('hide');
                    $('#emailCountDownTime').html('');
                    $('#lblEmailCode').html('');
                    $('#btnCodeRequest').removeAttr('disabled');
                }
                else {
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    minutes = (minutes < 0) ? 59 : minutes;
                    minutes = (minutes < 10) ? '0' + minutes : minutes;
                    hours = (hours < 10) ? '0' + hours : hours;
                    $('#emailCountDownTime').html(hours + ':' + minutes + ':' + seconds);
                    timer2 = hours + ':' + minutes + ':' + seconds;
                    $('#emailCountDownTime').removeClass('hide');
                }
            }, 1000);
        }
        function recaptchaExpired() {
            grecaptcha.reset();
        }

        var gRecaptchaResponse;
        var validateCheck = 0;
        function ValidateStep2() {
            if (validateCheck == 0) {
                return new Promise(function (resolve, reject) {

                    if (grecaptcha === undefined) {
                        alert('Recaptcha non defined');
                        return;
                    }
                    console.log('verification');

                    var response = grecaptcha.getResponse();
                    console.log(response);
                    if (!response) {
                        alert('Coud not get recaptcha response');
                        return;
                    }
                    gRecaptchaResponse = response;
                    $('#gRecaptchaResponse').val(gRecaptchaResponse);
                    if ($('#txtNRIC').val().trim() != "" && $('#txtMobileNo').val().trim() != "") {
                        $("#requestOTPDiv").removeClass('hide');
                        $('#txtNRIC').removeClass('field-required');
                        $('#txtMobileNo').removeClass('field-required');
                    }
                    else {
                        if ($('#txtNRIC').val().trim() == "")
                            $('#txtNRIC').addClass('field-required');
                        else
                            $('#txtNRIC').removeClass('field-required');
                        if ($('#txtMobileNo').val().trim() == "")
                            $('#txtMobileNo').addClass('field-required');
                        else
                            $('#txtMobileNo').removeClass('field-required');
                    }
                    validateCheck = 1;
                }); //end promise
            }
            else {
                if ($('#txtNRIC').val().trim() != "" && $('#txtMobileNo').val().trim() != "") {
                    $("#requestOTPDiv").removeClass('hide');
                }
                else {
                    if ($('#txtNRIC').val().trim() == "")
                        $('#txtNRIC').addClass('field-required');
                    else
                        $('#txtNRIC').removeClass('field-required');
                    if ($('#txtMobileNo').val().trim() == "")
                        $('#txtMobileNo').addClass('field-required');
                    else
                        $('#txtMobileNo').removeClass('field-required');
                }
            }
        }
        var validateCheckUserId = 0;
        function ValidateStep3() {
            if ($('#email').val().trim() != "" && $('#password').val().trim() != "" && $('#repassword').val().trim() != "" && $('#password').val().trim() == $('#repassword').val().trim()) {
                $('#email').removeClass('field-required');
                $('#password').removeClass('field-required');
                $('#repassword').removeClass('field-required');
                $('#passwordNotMatch').addClass('hide');
                //$('.buttonArea [data-step="next"]').removeClass('hide');
            }
            else {
                if ($('#email').val().trim() == "")
                    $('#email').addClass('field-required');
                else
                    $('#email').removeClass('field-required');
                if ($('#password').val().trim() == "")
                    $('#password').addClass('field-required');
                else
                    $('#password').removeClass('field-required');
                if ($('#repassword').val().trim() == "")
                    $('#repassword').addClass('field-required');
                else if ($('#password').val().trim() != $('#repassword').val().trim()) {
                    $('#repassword').addClass('field-required');
                    if ($('#passwordNotMatch').hasClass('hide')) {
                        $('#passwordNotMatch').removeClass('hide');
                    }
                }
                else
                    $('#repassword').removeClass('field-required');
            }
            validateCheckUserId = 1;
        }
        var validateCheckPersonalInfo = 0;
        function ValidateStep4() {
            var atLeastOneIsChecked = false;
            $('.checkbox-group-5 input').each(function () {
                if ($(this).is(':checked')) {
                    atLeastOneIsChecked = true;
                }
            });
            if (atLeastOneIsChecked && ($('#title').val() != "" && $('#title').val() == 'Others' && $('#titleOther').val() != "") && $('#fullName').val() != "" && $('#dob').val() != "" && $('#gender').val() != "" && $('#race').val() != "" && $('#maritalStatus').val() != "" && $('#bumiputraStatus').val() != "") {
                //$('.buttonArea [data-step="next"]').removeClass('hide');
                $('#title').removeClass('field-required');
                //$('#titleOther').removeClass('field-required');
                $('#fullName').removeClass('field-required');
                $('#dob').removeClass('field-required');
                $('#gender').removeClass('field-required');
                $('#race').removeClass('field-required');
                $('#maritalStatus').removeClass('field-required');
                $('#bumiputraStatus').removeClass('field-required');
            }
            else {
                if (!atLeastOneIsChecked) {
                    $('.checkbox-group-5').addClass('field-required');
                }
                if ($('#title').val().trim() == "")
                    $('#title').addClass('field-required');
                else
                    $('#title').removeClass('field-required');
                //if ($('#title').val().trim() == "Others" && $('#titleOther').val().trim() == "")
                //    $('#titleOther').addClass('field-required');
                //else
                //    $('#titleOther').removeClass('field-required');
                if ($('#fullName').val().trim() == "")
                    $('#fullName').addClass('field-required');
                else
                    $('#fullName').removeClass('field-required');
                if ($('#dob').val().trim() == "")
                    $('#dob').addClass('field-required');
                else
                    $('#dob').removeClass('field-required');
                if ($('#gender').val().trim() == "")
                    $('#gender').addClass('field-required');
                else
                    $('#gender').removeClass('field-required');
                if ($('#race').val().trim() == "")
                    $('#race').addClass('field-required');
                else
                    $('#race').removeClass('field-required');
                if ($('#maritalStatus').val().trim() == "")
                    $('#maritalStatus').addClass('field-required');
                else
                    $('#maritalStatus').removeClass('field-required');
                if ($('#bumiputraStatus').val().trim() == "")
                    $('#bumiputraStatus').addClass('field-required');
                else
                    $('#bumiputraStatus').removeClass('field-required');
            }
            validateCheckPersonalInfo = 1;
        }
        var validateCheckContactInfo = 0;
        function ValidateStep5() {
            if ($('#addr1').val() != "" && $('#city').val() != "" && $('#postCode').val() != "" && $('#state').val() != "" && $('#country').val() != "" && $('#addr1M').val() != "" && $('#cityM').val() != "" && $('#postCodeM').val() != "" && $('#stateM').val() != "" && $('#countryM').val() != "") {
                //$('.buttonArea [data-step="next"]').removeClass('hide');
                $('#addr1').removeClass('field-required');
                $('#city').removeClass('field-required');
                $('#postCode').removeClass('field-required');
                $('#state').removeClass('field-required');
                $('#country').removeClass('field-required');
                $('#addr1M').removeClass('field-required');
                $('#cityM').removeClass('field-required');
                $('#postCodeM').removeClass('field-required');
                $('#stateM').removeClass('field-required');
                $('#countryM').removeClass('field-required');
            }
            else {
                //$('.buttonArea [data-step="next"]').addClass('hide');
                if ($('#addr1').val().trim() == "")
                    $('#addr1').addClass('field-required');
                else
                    $('#addr1').removeClass('field-required');
                if ($('#city').val().trim() == "")
                    $('#city').addClass('field-required');
                else
                    $('#city').removeClass('field-required');
                if ($('#postCode').val().trim() == "")
                    $('#postCode').addClass('field-required');
                else
                    $('#postCode').removeClass('field-required');
                if ($('#state').val().trim() == "")
                    $('#state').addClass('field-required');
                else
                    $('#state').removeClass('field-required');
                if ($('#country').val().trim() == "")
                    $('#country').addClass('field-required');
                else
                    $('#country').removeClass('field-required');
                if ($('#addr1M').val().trim() == "")
                    $('#addr1M').addClass('field-required');
                else
                    $('#addr1M').removeClass('field-required');
                if ($('#cityM').val().trim() == "")
                    $('#cityM').addClass('field-required');
                else
                    $('#cityM').removeClass('field-required');
                if ($('#postCodeM').val().trim() == "")
                    $('#postCodeM').addClass('field-required');
                else
                    $('#postCodeM').removeClass('field-required');
                if ($('#stateM').val().trim() == "")
                    $('#stateM').addClass('field-required');
                else
                    $('#stateM').removeClass('field-required');
                if ($('#countryM').val().trim() == "")
                    $('#countryM').addClass('field-required');
                else
                    $('#countryM').removeClass('field-required');
            }
            validateCheckContactInfo = 1;
        }

        var validateCheckOccInfo = 0;
        function ValidateStep6() {
            var atLeastOneIsChecked = false;
            $('.checkbox-group-2 input').each(function () {
                if ($(this).is(':checked')) {
                    atLeastOneIsChecked = true;
                }
            });
            if (atLeastOneIsChecked && $('#ddlOccupation').val() != "" && ($('#ddlOccupation').val() == 'Others' && $('txtOccupationDesc').val() != "") && ($('#Employed').is(':checked') && $('#ddlDesignation').val() != "" && $('#ddlNatureOfBusiness').val() != "" && $('#ddlMonthlyIncome').val() != "" && $('#txtOfficeAddress1').val() != "" && $('#txtPostCode').val() != "" && $('#txtCity').val() != "" && $('#ddlState').val() != "" && $('#ddlCountry').val() != "")) {
                //$('.buttonArea [data-step="next"]').removeClass('hide');
                $('#ddlOccupation').removeClass('field-required');
                $('#txtOccupationDesc').removeClass('field-required');
                $('#ddlDesignation').removeClass('field-required');
                $('#ddlNatureOfBusiness').removeClass('field-required');
                $('#ddlMonthlyIncome').removeClass('field-required');
                $('#txtOfficeAddress1').removeClass('field-required');
                $('#txtPostCode').removeClass('field-required');
                $('#txtCity').removeClass('field-required');
                $('#ddlState').removeClass('field-required');
                $('#ddlCountry').removeClass('field-required');
            }
            else {
                if (!atLeastOneIsChecked) {
                    $('.checkbox-group-2').addClass('field-required');
                }
                //$('.buttonArea [data-step="next"]').addClass('hide');
                if ($('#ddlOccupation').val().trim() == "")
                    $('#ddlOccupation').addClass('field-required');
                else
                    $('#ddlOccupation').removeClass('field-required');
                if ($('#race').val().trim() == "Others" && $('txtRaceDesc').val() == "")
                    $('#txtRaceDesc').addClass('field-required');
                else
                    $('#txtRaceDesc').removeClass('field-required');
                if ($('#ddlOccupation').val().trim() == "Others" && $('txtOccupationDesc').val() == "")
                    $('#txtOccupationDesc').addClass('field-required');
                else
                    $('#txtOccupationDesc').removeClass('field-required');
                if ($('#Employed').is(':checked')) {
                    alert('3');
                    if ($('#ddlDesignation').val().trim() == "")
                        $('#ddlDesignation').addClass('field-required');
                    else
                        $('#ddlDesignation').removeClass('field-required');
                    if ($('#ddlNatureOfBusiness').val().trim() == "")
                        $('#ddlNatureOfBusiness').addClass('field-required');
                    else
                        $('#ddlNatureOfBusiness').removeClass('field-required');
                    if ($('#ddlMonthlyIncome').val().trim() == "")
                        $('#ddlMonthlyIncome').addClass('field-required');
                    else
                        $('#ddlMonthlyIncome').removeClass('field-required');
                    if ($('#txtOfficeAddress1').val().trim() == "")
                        $('#txtOfficeAddress1').addClass('field-required');
                    else
                        $('#txtOfficeAddress1').removeClass('field-required');
                    if ($('#txtPostCode').val().trim() == "")
                        $('#txtPostCode').addClass('field-required');
                    else
                        $('#txtPostCode').removeClass('field-required');
                    if ($('#txtCity').val().trim() == "")
                        $('#txtCity').addClass('field-required');
                    else
                        $('#txtCity').removeClass('field-required');
                    if ($('#ddlState').val().trim() == "")
                        $('#ddlState').addClass('field-required');
                    else
                        $('#ddlState').removeClass('field-required');
                    if ($('#ddlCountry').val().trim() == "")
                        $('#ddlCountry').addClass('field-required');
                    else
                        $('#ddlCountry').removeClass('field-required');
                }
            }
            validateCheckOccInfo = 1;
        }
        //function ValidateStep7() {
            
        //    if (($('#ddlPurposeOfTransaction').val() != ""
        //        || ($('#ddlPurposeOfTransaction').val() == 'Others'
        //            && $('txtPurposeOfTransaction').val() != "")
        //        || ($('#ddlSourceOfFunds').val() != "")
        //        || ($('#ddlSourceOfFunds').val() == 'Others'
        //            && $('ddlRelationship').val() != "Select"
        //            && $('txtFunderName').val() != ""
        //            && $('ddlFunderIndustry').val() != "Select"
        //            && $('txtFundOwnerName').val() != ""
        //            && $('ddlEstimatedNetWorth').val() != "Select"))) {
        //        //$('.buttonArea [data-step="next"]').removeClass('hide');
        //        $('#ddlPurposeOfTransaction').removeClass('field-required');
        //        $('#ddlSourceOfFunds').removeClass('field-required');
        //        $('#ddlEstimatedNetWorth').removeClass('field-required');
        //    }
        //    else {
                
        //        //$('.buttonArea [data-step="next"]').addClass('hide');
        //        if ($('#ddlPurposeOfTransaction').val().trim() == "Select")
        //            $('#ddlPurposeOfTransaction').addClass('field-required');
        //        else
        //            $('#ddlPurposeOfTransaction').removeClass('field-required');
        //        //if ($('#ddlSourceOfFunds').val().trim() == "Others" && $('txtRaceDesc').val() == "")
        //        //    $('#txtRaceDesc').addClass('field-required');
        //        //else
        //        //    $('#txtRaceDesc').removeClass('field-required');
        //        //if ($('#ddlOccupation').val().trim() == "Others" && $('txtOccupationDesc').val() == "")
        //        //    $('#txtOccupationDesc').addClass('field-required');
        //        //else
        //        //    $('#txtOccupationDesc').removeClass('field-required');
        //    }
        //    validateCheckOccInfo = 1;
        //}
        var ddlOccOptions;
        function btnNextStepAjax(id, formArray) {
            $('.loader-bg').fadeIn();
            $.ajax({
                url: "AccountOpening.aspx/btnNext_Click",
                contentType: 'application/json; charset=utf-8',
                type: "POST",
                dataType: "JSON",
                data: JSON.stringify({ formInputs: formArray, IPAddress: $("#hdnLocalIPAddress").val() }),
                success: function (data) {
                    $('.err-msg').remove();
                    $('.field-required').removeClass('field-required');

                    $('.loader-bg').fadeOut();
                    var json = data.d;
                    //console.log(json.CustomValidations);
                    if (json.CustomValidations != null) {
                        if (json.CustomValidations.length > 0) {
                            $('#' + json.CustomValidations[0].Name).focus();
                        }
                        $.each(json.CustomValidations, function (idx, obj) {
                            //console.log(obj);
                            if (!($('#' + obj.Name).hasClass('field-required'))) {
                                if (obj.Name == 'addr1') {
                                    $('<div class="err-msg"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('#addr3');
                                }
                                else if (obj.Name == 'txtOfficeAddress1') {
                                    $('<div class="err-msg"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('#txtOfficeAddress3');
                                }
                                else if (obj.Name == 'addr1M') {
                                    $('<div class="err-msg"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('#addr3M');
                                }
                                else if (obj.Name == 'ddlTaxResidencyStatus') {
                                    $('.out-malaysia-yes').each(function (idxTax, objTax) {
                                        if ($(objTax).find('.ddlTaxResidencyStatus').val() == "") {
                                            var tx = $(objTax).find('.ddlTaxResidencyStatus');
                                            $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter(tx);
                                            $(tx).addClass('field-required');
                                        }
                                        else {

                                        }
                                    });
                                }
                                else if (obj.Name == 'txtTINNum') {
                                    $('.out-malaysia-yes').each(function (idxTax, objTax) {
                                        if ($(objTax).find('.txtTINNum').val() == "") {
                                            var tx = $(objTax).find('.txtTINNum');
                                            $(tx).addClass('field-required');
                                            $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter(tx);
                                        }
                                        else {

                                        }
                                    });
                                }
                                else if (obj.Name == 'txtnoTIN') {
                                    $('.out-malaysia-yes').each(function (idxTax, objTax) {
                                        if ($(objTax).find('.txtnoTIN').val() == "") {
                                            var tx = $(objTax).find('.txtnoTIN');
                                            $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter(tx);
                                            $(tx).addClass('field-required');
                                        }
                                        else {

                                        }
                                    });

                                }
                                else if (obj.Name == 'optionBReason') {
                                    $('.out-malaysia-yes').each(function (idxTax, objTax) {
                                        if ($(objTax).find('.optionBReason').val() == "") {
                                            var tx = $(objTax).find('.optionBReason');
                                            $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter(tx);
                                            $(tx).addClass('field-required');
                                        }
                                        else {

                                        }
                                    });

                                }
                                else {
                                    $('<div class="err-msg" id="err-' + obj.Name + '"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('#' + obj.Name);
                                }
                                $('#' + obj.Name).addClass('field-required');
                                if (obj.Name == 'checkbox-group-3') {
                                    $('<div class="err-msg"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-3');
                                }
                                else if (obj.Name == 'checkbox-group-4') {

                                    $('<div class="err-msg"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-4');
                                }
                                else if (obj.Name == 'checkbox-group-1') {

                                    $('<div class="err-msg" style=""><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-1');
                                }
                                else if (obj.Name == 'checkbox-group-2') {

                                    $('<div class="err-msg" style="padding-left: 15px;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-2');
                                }
                                else if (obj.Name == 'checkbox-group-5') {

                                    $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('#placeForResidentErr');
                                }
                                else if (obj.Name == 'checkbox-group-8') {

                                    $('<div class="err-msg" style="padding-left: 15px;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-8');
                                }
                                else if (obj.Name == 'checkbox-group-9') {

                                    $('<div class="err-msg" style="padding-left: 15px;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('.checkbox-group-9');
                                }
                                else if (obj.Name == 'checkbox-group-10') {

                                    $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('#placeForNationalityErr');
                                }
                                else if (obj.Name == 'chkBoxGroupSIFA1') {

                                    $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('.chkBoxGroupSIFA1');
                                }
                                else if (obj.Name == 'chkBoxGroupSIFA2') {

                                    $('<div class="err-msg" style="width:100%;"><small style="font-size: 12px; color: red ! important;">' + obj.Message + '</small></div>').insertAfter('.chkBoxGroupSIFA2');
                                }
                            }
                        });
                    }
                    if (!json.IsSuccess) {
                        if (id != "btnFinish") {
                            $('section.current [data-step="previous"]').click();
                        }
                        if (json.Message.substring(0, 35) == 'E-KYC identity verification failed.')
                            ShowCustomMessage('Alert', 'E-KYC identity verification failed. Entered details do not match. Please retry.', 'Index.aspx');
                        else if (json.Message == 'Session Expired.') {
                            ShowCustomMessage("Alert", json.Message, 'AccountOpening.aspx');
                        }
                        else {
                            if (json.Message == "Please fill in details") { }
                            else
                                ShowCustomMessage('Alert', json.Message, '');
                            if (json.Message == 'Please verify yourself!' || json.Message == 'This Id Number has already been registered' || json.Message == 'This Id Number has already been registered as joint with you.' || json.Message == 'Please enter valid Mobile number!' || json.Message == 'Aged below 18 are not allowed')
                                recaptchaExpired();
                        }

                        //if (id == "btnNextStep3")
                        //{

                        //}

                    }
                    else if (json.IsSuccess) {

                        var accountOpeningExisting = json.Data;
                        if (id == "btnNextStep0") {
                            $('#USCGPYes').focus();
                            if (accountOpeningExisting.ProcessStatus == 10) {
                                $('#taxResidentHead').addClass('hide');
                                $('#about-you').removeClass('hide');
                                $('#btnNextStep1').removeClass('hide');
                                $('#txtNRIC').val(accountOpeningExisting.IdNo);
                                $('#txtNRIC').attr('readonly', 'readonly');
                                $('#txtMobileNo').val(accountOpeningExisting.MobileNo.substring(1));
                                $('#txtMobileNo').attr('readonly', 'readonly');
                                $('#Div7').addClass('hide');
                            }
                            //$('#about-you').removeClass('hide');
                            //$('#requestOTPDiv').hide();

                            //$('#ContentPlaceHolder1_Div7').hide();
                            //$('#usChecks').hide();
                            //$('#email').val(accountOpeningExisting.Email);
                            //$('#password').val('');
                            //$('#repassword').val('');
                            //$('#txtNRIC').attr('readonly', 'readonly');
                            //$('#requestOTPDiv').hide();
                            //$('#txtMobileNo').attr('readonly', 'readonly');
                            //$('#ContentPlaceHolder1_Div7').hide();
                            //$('#usChecks').hide();

                            //$("#ROMYes").prop('checked', false);
                            //$("#ROMNo").prop('checked', false);
                            //$("#Malaysian").prop('checked', false);
                            //$("#NonMalaysian").prop('checked', false);
                            //if (accountOpeningExisting.ResidentOfMalaysia == 1)
                            //    $("#ROMYes").prop('checked', true);
                            //else
                            //    $("#ROMNo").prop('checked', true);
                            //if (accountOpeningExisting.Nationality == "Malaysian")
                            //    $("#Malaysian").prop('checked', true);
                            //else if (accountOpeningExisting.Nationality == "NonMalaysian")
                            //    $("#NonMalaysian").prop('checked', true);
                            //if (accountOpeningExisting.Salutation != "") {
                            //    if ($('#title option[value="' + accountOpeningExisting.Salutation + '"').length == 0) {
                            //        $('#title').val('Others');
                            //        $('#titleOther').removeClass('d-none');
                            //        $('#titleOther').val(accountOpeningExisting.Salutation);
                            //    }
                            //    else {
                            //        $('#title').val(accountOpeningExisting.Salutation);
                            //    }
                            //}

                            //$('#fullName').val(accountOpeningExisting.Name);
                            //$('#dob').val(accountOpeningExisting.Dob);
                            //$('#gender').val(accountOpeningExisting.Gender);
                            //$('#race').val(accountOpeningExisting.Race);
                            //$('#maritalStatus').val(accountOpeningExisting.MaritalStatus);
                            //$('#bumiputraStatus').val(accountOpeningExisting.BumiputraStatus);
                        }
                        if (id == "btnNextStep1") {
                            if (accountOpeningExisting.ProcessStatus == 10) {
                                id = "btnNextStep9";
                            }
                            $('#email').val(accountOpeningExisting.Email);
                            $('#hdnEmail').val(accountOpeningExisting.Email);
                            $('#password').val('');
                            $('#repassword').val('');
                            $('#txtNRIC').attr('readonly', 'readonly');
                            $('#requestOTPDiv').hide();
                            $('#txtMobileNo').attr('readonly', 'readonly');
                            $('#ContentPlaceHolder1_Div7').hide();
                            $('#usChecks').hide();
                            $('.tax-residency-details').hide();
                            $('.checkbox-group-1').hide();
                            //$('#' + accountOpeningExisting.LastBtnClicked).parent('.buttonArea').parent('section')
                            //console.log(accountOpeningExisting.LastBtnClicked);
                            //console.log(Array.from($('[data-step="next"]')));
                            //var btnIndex = Array.from($('[data-step="next"]')).indexOf($('#' + accountOpeningExisting.LastBtnClicked)[0])
                            //console.log("btnIndex: " + btnIndex);
                            //$('[data-step="next"]').each(function (idx, ele) {
                            //    if (idx <= btnIndex) {
                            //        btnNextStepAjax(id, formArray)
                            //        //$(ele).click();
                            //        $("#wizard").steps('next');
                            //    }
                            //});
                            //$(".steps ul > li").each(function (idx, ele) {
                            //    var cssClass = "";
                            //    $(ele).removeClass('done');
                            //    $(ele).removeClass('current');
                            //    $(ele).removeClass('disabled');
                            //    $(ele).attr('aria-disabled', false);
                            //    $(ele).attr('aria-selected', false);
                            //    if (idx <= btnIndex) {
                            //        cssClass = "done";
                            //        $(ele).attr('aria-selected', false);
                            //        $(ele).attr('aria-disabled', false);
                            //    }
                            //    else if (idx == btnIndex + 1) {
                            //        cssClass = "current";
                            //        $(ele).attr('aria-selected', true);
                            //        $(ele).attr('aria-disabled', false);
                            //    }
                            //    else if (idx > btnIndex + 1) {
                            //        cssClass = "disabled";
                            //        $(ele).attr('aria-selected', false);
                            //        $(ele).attr('aria-disabled', false);
                            //    }
                            //    $(ele).addClass(cssClass);
                            //});
                            //$('#' + accountOpeningExisting.LastBtnClicked).click();

                            $("#ROMYes").focus();
                            $("#ROMYes").prop('checked', false);
                            $("#ROMNo").prop('checked', false);
                            //$("#Malaysian").prop('checked', false);
                            //$("#NonMalaysian").prop('checked', false);
                            if (accountOpeningExisting.Tin == '1') {
                                if (accountOpeningExisting.ResidentOfMalaysia == 1) {
                                    $("#ROMYes").prop('checked', true);
                                }
                                else {
                                    $("#ROMNo").prop('checked', true);
                                }
                            }
                            if (accountOpeningExisting.Nationality == "Malaysian")
                                $("#Malaysian").prop('checked', true);
                            else if (accountOpeningExisting.Nationality == "NonMalaysian") {
                                $("#NonMalaysian").prop('checked', true);
                                $("#prcheck").removeClass('hide');
                                $("#ddlForeigner").val(accountOpeningExisting.Foreigner);
                            }
                            if (accountOpeningExisting.Salutation != "") {
                                if ($('#title option[value="' + accountOpeningExisting.Salutation + '"').length == 0) {
                                    $('#title').val('Others');
                                    //$('#titleOther').removeClass('d-none');
                                    //$('#titleOther').val(accountOpeningExisting.Salutation);
                                }
                                else {
                                    $('#title').val(accountOpeningExisting.Salutation);
                                }
                            }

                            $('#fullName').val(accountOpeningExisting.Name);
                            $('#dob').datepicker('setDate', accountOpeningExisting.Dob);
                            //$('#dob').val(accountOpeningExisting.Dob);
                            $('#gender').val(accountOpeningExisting.Gender);
                            $('#race').val(accountOpeningExisting.Race);
                            if (accountOpeningExisting.Race == "Others")
                                $('.race-others').removeClass('hide');
                            $('#txtRaceDesc').val(accountOpeningExisting.RaceDescription);
                            $('#maritalStatus').val(accountOpeningExisting.MaritalStatus);
                            $('#bumiputraStatus').val(accountOpeningExisting.BumiputraStatus);
                            $("#ddlEduLvl").val(accountOpeningExisting.Education_Level);
                            if (accountOpeningExisting.Education_Level == "Others") {
                                $("#txtEduLvlOther").val(accountOpeningExisting.Education_Level_Others)
                                $('.ddlEduLvlOthers').removeClass('hide');
                            }
                            //$('#agent').val(accountOpeningExisting.AgentCode);

                            if (/*x.test(y)*/ !isNaN($('#txtNRIC').val()) && $('#txtNRIC').val().length == 12) {
                                //alert('in');
                                //$('#dob').attr('disabled', 'disabled');
                            }
                            if (accountOpeningExisting.IsEmailVerified == 1) {
                                $('#requestCodeDiv').addClass('hide');
                            }
                        }
                        if (id == "btnNextStep3") {
                            $('#addr1').focus();
                            if (accountOpeningExisting.accountOpeningAddresses != null && accountOpeningExisting.accountOpeningAddresses.length > 0) {
                                var aoar = $.grep(accountOpeningExisting.accountOpeningAddresses, function (obj, idx) {
                                    return obj.AddressType == 1;
                                });
                                var aoam = $.grep(accountOpeningExisting.accountOpeningAddresses, function (obj, idx) {
                                    return obj.AddressType == 2;
                                });
                                if (aoar.length == 1) {
                                    $('#addr1').val(aoar[0].Addr1);
                                    $('#addr2').val(aoar[0].Addr2);
                                    $('#addr3').val(aoar[0].Addr3);
                                    $('#city').val(aoar[0].City);
                                    $('#postCode').val(aoar[0].PostCode);
                                    $('#state').val(aoar[0].State);
                                    $('#country').val(aoar[0].Country);
                                    $('#telNoH').val(aoar[0].TelNo);
                                }
                                if (aoam.length == 1) {
                                    $('#addr1M').val(aoam[0].Addr1);
                                    $('#addr2M').val(aoam[0].Addr2);
                                    $('#addr3M').val(aoam[0].Addr3);
                                    $('#cityM').val(aoam[0].City);
                                    $('#postCodeM').val(aoam[0].PostCode);
                                    $('#stateM').val(aoam[0].State);
                                    $('#countryM').val(aoam[0].Country);
                                }
                                if (aoar.length == 1 && aoam.length == 1) {
                                    if (aoar[0].Addr1 == aoam[0].Addr1 &&
                                        aoar[0].Addr2 == aoam[0].Addr2 &&
                                        aoar[0].Addr3 == aoam[0].Addr3 &&
                                        aoar[0].City == aoam[0].City &&
                                        aoar[0].PostCode == aoam[0].PostCode &&
                                        aoar[0].State == aoam[0].State &&
                                        aoar[0].Country == aoam[0].Country) {
                                        $("#sameAsResidentialAddr").prop('checked', true);
                                        $('#addr1M').attr('readonly', 'readonly');
                                        $('#addr2M').attr('readonly', 'readonly');
                                        $('#addr3M').attr('readonly', 'readonly');
                                        $('#cityM').attr('readonly', 'readonly');
                                        $('#postCodeM').attr('readonly', 'readonly');
                                        $('#stateM').attr('readonly', 'readonly');
                                        $('#countryM').attr('disabled', 'disabled');
                                    }
                                }
                            }
                            if (accountOpeningExisting.IsEmailVerified == 1) {
                                ShowCustomMessage("Notice of Privacy Policy", "Your personal information will be use in accordance with our Privacy policy. You can view this policy on our portal. By accessing our portal, you agree to be bound by this policy.", "", "Accept", "addr1");
                                $('#hdnEmail').val(accountOpeningExisting.Email);
                                $('#hdnEmailVerified').val('1');
                            }
                        }
                        if (id == "btnNextStep5") {
                            $('#Employed').focus();
                            if (accountOpeningExisting.AccountOpeningOccupation != null) {

                                $('#Employed').prop('checked', false);
                                $('#Unemployed').prop('checked', false);
                                var occ = accountOpeningExisting.AccountOpeningOccupation;
                                //console.log(occ);
                                if (occ.Employed == 1 || occ.SelfEmployed == 1) {
                                    if (occ.Employed == 1)
                                        $('#Employed').prop('checked', true);
                                    if (occ.SelfEmployed == 1)
                                        $("#SelfEmployed").prop('checked', true);
                                    $('.occupation-field').removeClass('hide');
                                    $('.employed-fields').removeClass('hide');
                                    $('#ddlOccupation option').remove();
                                    $.ajax({
                                        url: "AccountOpening.aspx/GetOccupations",
                                        contentType: 'application/json; charset=utf-8',
                                        type: "GET",
                                        dataType: "JSON",
                                        //data: $('form').serializeArray(),
                                        success: function (data) {
                                            console.log(data);
                                            if (data.d.IsSuccess) {
                                                var json = data.d.Data;
                                                var option = document.createElement("option");
                                                option.text = "Select";
                                                option.value = "";
                                                $('#ddlOccupation').append(option);
                                                $.each(json, function (idx, obj) {
                                                    var option = document.createElement("option");
                                                    option.text = obj.OCCCODE;
                                                    option.value = obj.DOWNLOADIND;
                                                    $('#ddlOccupation').append(option);
                                                });
                                                $('#ddlOccupation').val(occ.Occupation != "null" ? occ.Occupation : "");
                                                $('#ddlOccupation').change();
                                            }
                                            else {
                                                ShowCustomMessage("Alert", data.d.Message, "");
                                            }
                                        }
                                    });
                                    //$('.occupation-others').addClass('hide');
                                    //ddlOccOptions = $('#ddlOccupation option');
                                }
                                else {
                                    $('#Unemployed').prop('checked', true);
                                    $('.occupation-field').removeClass('hide');
                                    $('.employed-fields').addClass('hide');
                                    $('.occupation-others').addClass('hide');
                                    ddlOccOptions = $('#ddlOccupation option');
                                    $('#ddlOccupation option').remove();
                                    $('#ddlOccupation').append('<option value="">Select</option>');
                                    $('#ddlOccupation').append('<option value="Student">Student</option>');
                                    $('#ddlOccupation').append('<option value="Retired">Retired</option>');
                                    $('#ddlOccupation').append('<option value="Housewife">Housewife</option>');
                                    $('#ddlOccupation').append('<option value="Others">Others</option>');
                                    $('#ddlOccupation').val(occ.Occupation != "null" ? occ.Occupation : "");
                                }
                                if (occ.Occupation != 'Others')
                                    $('#ddlOccupation').val(occ.Occupation != "null" ? occ.Occupation : "");
                                $('#ddlOccupation').change();
                                $('#txtOccupationDesc').val(occ.OccupationOthers);
                                $('#ddlDesignation').val(occ.Designation);
                                $('#txtEmployerName').val(occ.EmployerName);
                                $('#ddlNatureOfBusiness').val(occ.NatureOfBusiness);
                                $('#ddlMonthlyIncome').val(occ.MonthlyIncome);

                                var aoar = $.grep(accountOpeningExisting.accountOpeningAddresses, function (obj, idx) {
                                    return obj.AddressType == 3;
                                });
                                if (aoar.length == 1) {
                                    $('#txtOfficeAddress1').val(aoar[0].Addr1);
                                    $('#txtOfficeAddress2').val(aoar[0].Addr2);
                                    $('#txtOfficeAddress3').val(aoar[0].Addr3);
                                    $('#txtCity').val(aoar[0].City);
                                    $('#txtPostCode').val(aoar[0].PostCode);
                                    $('#ddlState').val(aoar[0].State);
                                    $('#ddlCountry').val(aoar[0].Country);
                                    $('#txtTelephone').val(aoar[0].TelNo);
                                }
                            }
                        }
                        if (id == "btnNextStep6") {
                            $('#ddlPurposeOfTransaction').focus();
                            if (accountOpeningExisting.AccountOpeningFinancialProfile != null) {
                                var fp = accountOpeningExisting.AccountOpeningFinancialProfile;

                                $('#ddlPurposeOfTransaction').val(fp.Purpose);
                                if (fp.Purpose == "Others") {
                                    $('#purposeOfTransactionOthers').removeClass('hide');
                                    $('#txtPurposeOfTransaction').val(fp.PurposeDescription);
                                }
                                $('#ddlSourceOfFunds').val(fp.Source);
                                if (fp.Source == "Others") {
                                    $('#source-others').removeClass('hide');
                                    $('#ddlRelationship').val(fp.Relationship);
                                    $('#txtFunderName').val(fp.NameOfFunder);
                                    $('#ddlFunderIndustry').val(fp.FundersIndustory);

                                    if (fp.IsFunderMoneyChanger == 1) {
                                        $('#chkFunderInvolvedYes').prop('checked', true);
                                    }
                                    else {
                                        $('#chkFunderInvolvedNo').prop('checked', true);
                                    }
                                    if (fp.IsFunderBeneficialOwner == 1) {
                                        $('#chkFunderBeneficialOwnerYes').prop('checked', true);
                                    }
                                    else {
                                        $('#chkFunderBeneficialOwnerNo').prop('checked', true);
                                    }

                                    $('#txtFundOwnerName').val(fp.FundOwnerName);
                                }
                                else {
                                    $('#source-others').addClass('hide');
                                }
                                $("#ddlAnnualIncome").val(fp.AnnualIncome);
                                $('#ddlEstimatedNetWorth').val(fp.EstimatedNetWorth);
                                if (fp.EmployedByFundCompany == 1) {
                                    $('#chkFundManagementCompanyYes').prop('checked', true);
                                }
                                else {
                                    $('#chkFundManagementCompanyNo').prop('checked', true);
                                }
                                if (accountOpeningExisting.IsAdditionalRequired == 1) {
                                    $('#additional').removeClass('hide');
                                }
                                else {
                                    if (!$('#additional').hasClass('hide'))
                                        $('#additional').addClass('hide');
                                }
                                if (fp.IsSI == 1) {
                                    $('#chkBoxSIFAYes').prop('checked', true);
                                    if (fp.SIType == "Accredited Investor")
                                        $('#chkBoxSIFAACI').prop('checked', true);
                                    else if (fp.SIType == "High Net-worth Individuals")
                                        $('#chkBoxSIFAHNI').prop('checked', true);
                                    $('.chkBoxGroupSIFA2').removeClass('hide');
                                }
                                else {
                                    $('.chkBoxGroupSIFA2').addClass('hide');
                                    $('#chkBoxSIFANo').prop('checked', true);
                                }
                            }
                        }
                        if (id == "btnNextStep7") {
                            $('#ddlBankAccountType').focus();
                            $('#txtBankAccountName').val(accountOpeningExisting.Name);
                            if (accountOpeningExisting.AccountOpeningBankDetail != null) {
                                var bd = accountOpeningExisting.AccountOpeningBankDetail;
                                if (bd.Agreement == 1) {
                                    $('#chkDepositSalesBankAccountYes').prop('checked', true);
                                }
                                else {
                                    $('#chkDepositSalesBankAccountNo').prop('checked', true);
                                }
                                $('#ddlBankAccountType').val(bd.AccountType);
                                $('#ddlCurrency').val(bd.Currency);
                                $('#ddlBank').val(bd.BankId);
                                //$('#txtBankAccountName').val(bd.AccountName);
                                $('#txtBankAccountNo').val(bd.AccountNo);
                            }
                            //if ($('#ddlEstimatedNetWorth').val() == 'Above MYR 3,000,000')
                            //{
                            //    if ($('.additional-docs').hasClass('hide'))
                            //        $('.additional-docs').removeClass('hide');
                            //}
                            id = "btnNextStep8";
                        }
                        if (id == "btnNextStep8") {
                            $('#chkPEPYes').focus();
                            if (accountOpeningExisting.TaxResidencyStatus == '1') {
                                if (accountOpeningExisting.AreYouPep == 1) {
                                    $('#chkPEPYes').prop('checked', true);
                                    $('.pep-specify').removeClass('hide');
                                }
                                else {
                                    $('#chkPEPNo').prop('checked', true);
                                    $('.pep-specify').addClass('hide');
                                }
                            }

                            $('#txtPEPSpecify').val(accountOpeningExisting.PepStatus);
                            $('#txtPEPFamilyM').val(accountOpeningExisting.FPepStatus);

                            if (accountOpeningExisting.TaxResidencyStatus != '1') {

                            }
                            else {
                                if (accountOpeningExisting.TaxResidentOutsideMalaysia == 1) {
                                    $('#chkCRSYes').prop('checked', true);
                                    $('.out-malaysia-yes').removeClass('hide');
                                    $('#addTaxResidencyStatus').removeClass('hide');
                                    if (accountOpeningExisting.accountOpeningCRSDetails != null && accountOpeningExisting.accountOpeningCRSDetails.length > 0) {
                                        //--------------
                                        $('#multiple-tax-residency-status-row .out-malaysia-yes:not(:first)').remove();
                                        $.each(accountOpeningExisting.accountOpeningCRSDetails, function (idx, obj) {
                                            if (idx == 0) {
                                                $('.ddlTaxResidencyStatus').val(obj.TaxResidencyStatus);
                                                if (obj.Tin == "") {
                                                    $('.chkNoTINNum').prop('checked', true);
                                                    $('.tin-available').addClass('hide');
                                                    $('.no-TIN').removeClass('hide');
                                                }
                                                $('.txtTINNum').val(obj.Tin);

                                                var reasons = obj.NoTinReason.split(', ');
                                                var noTinReason = reasons[0];
                                                var optionBReason = reasons.length > 1 ? reasons[1] : '';
                                                $('.txtnoTIN').val(noTinReason);
                                                if (noTinReason == 'The Account Holder is otherwise unable to obtain a TIN or equivalent number') {
                                                    $('.optionBReason').val(optionBReason);
                                                    $('.optionBReason').removeClass('hide');
                                                }
                                            }
                                            else {

                                                var divTax = $('.out-malaysia-yes').first();
                                                var divTaxClone = $(divTax).clone();
                                                $(divTaxClone).find('.ddlTaxResidencyStatus').val(obj.TaxResidencyStatus);
                                                $(divTaxClone).find('.tax-residency-remove').removeClass('hide');
                                                if (obj.Tin == "") {
                                                    $(divTaxClone).find('.chkNoTINNum').prop('checked', true);
                                                    $(divTaxClone).find('.tin-available').addClass('hide');
                                                    $(divTaxClone).find('.no-TIN').removeClass('hide');
                                                }
                                                else {
                                                    $(divTaxClone).find('.chkNoTINNum').prop('checked', false);
                                                    $(divTaxClone).find('.tin-available').removeClass('hide');
                                                    $(divTaxClone).find('.no-TIN').addClass('hide');
                                                }
                                                $(divTaxClone).find('.optionBReason').val('');
                                                $(divTaxClone).find('.txtTINNum').val(obj.Tin);
                                                var reasons = obj.NoTinReason.split(', ');
                                                $(divTaxClone).find('.txtnoTIN').val(reasons[0]);
                                                if (reasons[0] == 'The Account Holder is otherwise unable to obtain a TIN or equivalent number') {
                                                    $(divTaxClone).find('.optionBReason').val(reasons.length > 1 ? reasons[1] : '');
                                                    $(divTaxClone).find('.optionBReason').removeClass('hide');
                                                }

                                                $('#multiple-tax-residency-status-row').append(divTaxClone);
                                            }
                                        });
                                    }
                                }
                                else {
                                    $('#chkCRSNo').prop('checked', true);
                                    $('.out-malaysia-yes').addClass('hide');
                                }
                                //$('#ddlTaxResidencyStatus').val(accountOpeningExisting.TaxResidencyStatus);
                                //if (accountOpeningExisting.tin == "") {
                                //    $('#chkNoTINNum').prop('checked', true);
                                //    $('.tin-available').addClass('hide');
                                //}
                                //$('#txtTINNum').val(accountOpeningExisting.txtTINNum);
                            }
                            var bankcount = parseInt($('#hdnBankNoFormat').val());
                            if (!$('#txtBankAccountNo').val().length == bankcount) {
                                ShowCustomMessage("Alert", "Bank number length must be in format", "");
                            }

                        }
                        if (id == "btnNextStep9") {
                            $('#uploadNRICFrontPage').focus();
                            if (accountOpeningExisting.IsAdditionalRequired == 0) {
                                $('.additional-docs').addClass('hide');
                            }
                            else {
                                $('.additional-docs').removeClass('hide');
                            }
                            if (accountOpeningExisting.accountOpeningFiles != null && accountOpeningExisting.accountOpeningFiles.length > 0) {

                                var aof1 = $.grep(accountOpeningExisting.accountOpeningFiles, function (obj, idx) {
                                    return obj.FileType == 1;
                                });
                                if (aof1.length == 1) {
                                    var FileName = aof1[0].Name;
                                    var blobURL = aof1[0].Url;
                                    if (FileName.indexOf('.pdf') != -1)
                                        $('#uploadNRICFrontPage').prev().find('span:not(.account-type)').html("<a target='_blank' href='" + blobURL + "' alt='NRIC Front Page' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                                    else
                                        $('#uploadNRICFrontPage').prev().find('span:not(.account-type)').html("<img src='" + blobURL + "' alt='NRIC Front Page' title='" + FileName + "' style='height: 100%'/>");
                                }
                                var aof2 = $.grep(accountOpeningExisting.accountOpeningFiles, function (obj, idx) {
                                    return obj.FileType == 2;
                                });
                                if (aof2.length == 1) {
                                    var FileName = aof2[0].Name;
                                    var blobURL = aof2[0].Url;
                                    if (FileName.indexOf('.pdf') != -1)
                                        $('#uploadNRICBackPage').prev().find('span:not(.account-type)').html("<a target='_blank' href='" + blobURL + "' alt='NRIC Back Page' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                                    else
                                        $('#uploadNRICBackPage').prev().find('span:not(.account-type)').html("<img src='" + blobURL + "' alt='NRIC Back Page' title='" + FileName + "' style='height: 100%'/>");
                                }
                                var aof3 = $.grep(accountOpeningExisting.accountOpeningFiles, function (obj, idx) {
                                    return obj.FileType == 3;
                                });
                                if (aof3.length > 0) {
                                    var FileName = aof3[0].Name;
                                    var blobURL = aof3[0].Url;
                                    $('#uploadSelfie').prev().find('span:not(.account-type)').html("<img src='" + blobURL + "' alt='Selfie with NRIC' title='" + FileName + "' style='height: 100%'/>");
                                    $('#existing-selfie').removeClass('hide');
                                    $('#webcam').addClass('hide');
                                    $('#upload-selfie-option').addClass('hide');
                                }
                                else {
                                    $('#existing-selfie').addClass('hide');
                                    $('#webcam').removeClass('hide');
                                    $('#upload-selfie-option').removeClass('hide');
                                }
                                var aof4 = $.grep(accountOpeningExisting.accountOpeningFiles, function (obj, idx) {
                                    return obj.FileType == 4;
                                });
                                if (aof4.length == 1) {
                                    var FileName = aof4[0].Name;
                                    var blobURL = aof4[0].Url;
                                    if (FileName.indexOf('.pdf') != -1)
                                        $('#uploadSignature').prev().find('span:not(.account-type)').html("<a target='_blank' href='" + blobURL + "' alt='Signature document' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                                    else
                                        $('#uploadSignature').prev().find('span:not(.account-type)').html("<img src='" + blobURL + "' alt='Signature document' title='" + FileName + "' style='height: 100%'/>");
                                }
                                var aof5 = $.grep(accountOpeningExisting.accountOpeningFiles, function (obj, idx) {
                                    return obj.FileType == 5;
                                });
                                if (aof5.length > 0) {
                                    $('#uploadAdditional').prev().find('span:not(.account-type)').html("");
                                    $.each(aof5, function (idx, obj) {
                                        var FileName = obj.Name;
                                        var blobURL = obj.Url;
                                        $('#uploadAdditional').prev().find('span:not(.account-type)').append("<a target='_blank' href='" + blobURL + "' alt='Additional document' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                                    });

                                }
                                var aof6 = $.grep(accountOpeningExisting.accountOpeningFiles, function (obj, idx) {
                                    return obj.FileType == 6;
                                });
                                if (aof6.length > 0) {
                                    $('#uploadSupporting').prev().find('span:not(.account-type)').html("");
                                    $.each(aof6, function (idx, obj) {
                                        var FileName = obj.Name;
                                        var blobURL = obj.Url;
                                        $('#uploadSupporting').prev().find('span:not(.account-type)').append("<a target='_blank' href='" + blobURL + "' alt='Supporting document' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                                    });

                                }

                                //NEW
                                if (accountOpeningExisting.ProcessStatus == 10) {
                                    var ReUploadFileTypes = accountOpeningExisting.AdditionalDesc.split(',');
                                    $.each(ReUploadFileTypes, function (idx, obj) {
                                        if (obj == "1") {
                                            $('#uploadNRICFrontPage').prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o text-danger" style="max-height: 180px !important;"></i>');
                                        }
                                        else if (obj == "2") {
                                            $('#uploadNRICBackPage').prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o text-danger" style="max-height: 180px !important;"></i>');
                                        }
                                        else if (obj == "3") {
                                            $('#uploadSelfie').prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o text-danger" style="max-height: 180px !important;"></i>');
                                        }
                                        else if (obj == "4") {
                                            $('#uploadSignature').prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o text-danger" style="max-height: 180px !important;"></i>');
                                        }
                                        else if (obj == "5") {
                                            $('#uploadAdditional').prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o text-danger" style="max-height: 180px !important;"></i>');
                                        }
                                        else if (obj == "6") {
                                            $('#uploadSupporting').prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o text-danger" style="max-height: 180px !important;"></i>');
                                        }
                                    });
                                }
                            }
                        }
                        if (id == "btnNextStep11") {
                            $("#chkDeclare").focus();
                            if (accountOpeningExisting.Declaration == 1) {
                                $("#chkDeclare").prop('checked', true);
                            }
                        }
                        if (id == "btnFinish") {
                            if (accountOpeningExisting.AccountType == 1) {
                                ShowCustomMessage('Info', json.Message, 'Index.aspx');
                            }
                            else if (accountOpeningExisting.AccountType == 2) {
                                ShowCustomMessage('Info', json.Message, 'Portfolio.aspx');
                            }
                        }
                    }
                },
                error: function (jqXHR, exception) {
                    $('.loader-bg').fadeOut();
                    $('section.current [data-step="previous"]').click();
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    ShowCustomMessage('Error', msg, '');
                }

            });
        }

        function checkPassword(id, formArray) {


        }

        $(document).ready(function () {
            //$("#Malaysian").prop('checked', true);

            //document.getElementById("chkPEPYes").checked = false;
            //document.getElementById("chkPEPNo").checked = false;
            //document.getElementById("chkCRSYes").checked = false;
            //document.getElementById("chkCRSYes").checked = false;

            $('body').on('blur', ':input', function () {
                if ($(this).val() != "") {
                    $(this).removeClass('field-required');
                    $(this).parent().find('.err-msg').remove();
                }
            });
            $('#dob').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
                todayHighlight: true
            });
            //$('.buttonArea [data-step="next"]').addClass('hide');
            $('.checkbox-group-1 input:checkbox').click(function () {
                $('.checkbox-group-1 input:checkbox').not(this).prop('checked', false);
                console.log($(this).attr('id'))
                if ($(this).attr('id') == 'USCGPYes') {
                    if ($(this).is(':checked')) {
                        $('#about-you').addClass('hide');
                        $('#btnNextStep1').addClass('hide');
                        ShowCustomMessage('Alert', 'We are sorry to inform that eApexis does not accept applications with any of the U.S.Indicia - U.S.citizenship/ U.S Permanent Resident Status/ Born in U.S/ U.S.Taxpayer Identification Number(TIN)/ U.S.Residential/ Mailing Address and/ or US Telephone number.', '');
                    }
                }
                else if ($(this).attr('id') == 'USCGPNo') {
                    if ($(this).is(':checked')) {
                        $('#about-you').removeClass('hide');
                        $('#btnNextStep1').removeClass('hide');
                    }
                    else {
                        $('#about-you').addClass('hide');
                        $('#btnNextStep1').addClass('hide');
                    }
                }

            });
            $('.checkbox-group-2 input:checkbox').click(function () {
                $('.checkbox-group-2 input:checkbox').not(this).prop('checked', false);
                $('.loader-bg').fadeIn();
                if ($(this).attr('id') == 'Employed' || $(this).attr('id') == 'SelfEmployed') {
                    document.getElementById("lblOccupation").innerHTML = "Occupation";
                    if ($(this).is(':checked')) {
                        $('.occupation-field').removeClass('hide');
                        $('.employed-fields').removeClass('hide');
                        $('#occupationOthers').removeClass('hide');
                        if (!$('.occupation-others').hasClass('hide'))
                            $('.occupation-others').addClass('hide');
                        //ddlOccOptions = $('#ddlOccupation option');
                        $('#ddlOccupation option').remove();
                        $.ajax({
                            url: "AccountOpening.aspx/GetOccupations", //targeted page / function name in backend
                            contentType: 'application/json; charset=utf-8',
                            type: "GET",
                            dataType: "JSON",
                            //data: $('form').serializeArray(),
                            success: function (data) {
                                console.log(data);
                                if (data.d.IsSuccess) {
                                    var json = data.d.Data;
                                    var option = document.createElement("option");
                                    option.text = "Select";
                                    option.value = "";
                                    $('#ddlOccupation').append(option);
                                    $.each(json, function (idx, obj) {
                                        var option = document.createElement("option");
                                        option.text = obj.OCCCODE;
                                        option.value = obj.DOWNLOADIND;
                                        $('#ddlOccupation').append(option);
                                    });
                                    $('#ddlOccupation').val('');
                                }
                                else {
                                    ShowCustomMessage("Alert", data.d.Message, "");
                                }
                            }
                        });


                    }
                    else {
                        $('.occupation-field').addClass('hide');
                        $('.employed-fields').addClass('hide');
                    }

                }
                else if ($(this).attr('id') == 'Unemployed') {
                    if ($(this).is(':checked')) {
                        $('.occupation-field').removeClass('hide');
                        $('.employed-fields').addClass('hide');
                        if (!$('.occupation-others').hasClass('hide'))
                            $('.occupation-others').addClass('hide');
                        ddlOccOptions = $('#ddlOccupation option');
                        document.getElementById("lblOccupation").innerHTML = "Status";
                        $('#ddlOccupation option').remove();
                        $('#ddlOccupation').append('<option value="">Select Status</option>');
                        $('#ddlOccupation').append('<option value="Student">Student</option>');
                        $('#ddlOccupation').append('<option value="Retired">Retired</option>');
                        $('#ddlOccupation').append('<option value="Housewife">Housewife</option>');
                        $('#ddlOccupation').append('<option value="Others">Others</option>');
                    }
                    else {
                        $('.occupation-others').addClass('hide');
                        $('.occupation-field').addClass('hide');
                        $('.employed-fields').addClass('hide');
                        //$('.occupation-others').addClass('hide');
                    }
                }
                $('.loader-bg').fadeOut();
            });
            $('.checkbox-group-11 input:checkbox').click(function () {
                $('.checkbox-group-11 input:checkbox').not(this).prop('checked', false);

                if ($(this).attr('id') == 'chkFunderBeneficialOwnerYes') {

                    if ($(this).is(':checked')) {


                        $('#txtFundOwnerName').val($('#txtFunderName').val());
                        $('#txtFundOwnerName').attr('readonly', 'readonly');
                    }
                    else {

                    }

                }
                else if ($(this).attr('id') == 'chkFunderBeneficialOwnerNo') {
                    if ($(this).is(':checked')) {
                        $('#txtFundOwnerName').val('');
                        $('#txtFundOwnerName').removeAttr('readonly');
                    }
                    else {

                        //$('.occupation-others').addClass('hide');
                    }

                }

            });
            $('.checkbox-group-3 input:checkbox').click(function () {
                $('.checkbox-group-3 input:checkbox').not(this).prop('checked', false);
            });
            $('.checkbox-group-4 input:checkbox').click(function () {
                $('.checkbox-group-4 input:checkbox').not(this).prop('checked', false);
            });
            $('.checkbox-group-5 input:checkbox').click(function () {
                $('.checkbox-group-5 input:checkbox').not(this).prop('checked', false);
            });
            $('.checkbox-group-6 input:checkbox').click(function () {
                $('.checkbox-group-6 input:checkbox').not(this).prop('checked', false);
            });
            $('.checkbox-group-7 input:checkbox').click(function () {
                $('.checkbox-group-7 input:checkbox').not(this).prop('checked', false);
            });
            $('.checkbox-group-8 input:checkbox').click(function () {
                $('.checkbox-group-8 input:checkbox').not(this).prop('checked', false);
                if ($(this).attr('id') == 'chkPEPYes') {
                    if ($(this).is(':checked')) {
                        $('.pep-specify').removeClass('hide');
                    }
                    else {
                        $('.pep-specify').addClass('hide');
                    }
                }
                else if ($(this).attr('id') == 'chkPEPNo') {
                    if ($(this).is(':checked')) {
                        $('.pep-specify').addClass('hide');
                    }
                    else {

                    }
                }
            });
            $('.checkbox-group-9 input:checkbox').click(function () {
                $('.checkbox-group-9 input:checkbox').not(this).prop('checked', false);
                if ($(this).attr('id') == 'chkCRSYes') {
                    if ($(this).is(':checked')) {
                        $('.out-malaysia-yes').removeClass('hide');
                        $('#addTaxResidencyStatus').removeClass('hide');
                    }
                    else {
                        $('.out-malaysia-yes').addClass('hide');
                        $('#addTaxResidencyStatus').addClass('hide');
                    }
                }
                else if ($(this).attr('id') == 'chkCRSNo') {
                    if ($(this).is(':checked')) {
                        $('.out-malaysia-yes').addClass('hide');
                        $('#addTaxResidencyStatus').addClass('hide');
                    }
                    else {

                    }
                }
            });
            $('.checkbox-group-10 input:checkbox').click(function () {
                $('.checkbox-group-10 input:checkbox').not(this).prop('checked', false);
                if ($(this).attr('id') == 'NonMalaysian') {
                    if ($(this).is(':checked')) {
                        $('#ddlForeigner').removeAttr('disabled');
                        $('#prcheck').removeClass('hide');
                        $('#bumiputraStatus').attr('disabled', 'disabled');
                        $('#bumiputraStatus').val('Non-Malaysian');
                    }
                    else {
                        $("#ROMYes").prop('checked', false);
                        $("#ROMNo").prop('checked', false);
                        $('#prcheck').addClass('hide');
                        $('#ddlForeigner').val('').attr('disabled', 'disabled');
                        $('#bumiputraStatus').removeAttr('disabled');
                        $('#bumiputraStatus').val('');
                    }
                }
                else if ($(this).attr('id') == 'Malaysian') {
                    if ($(this).is(':checked')) {
                        $("#ROMYes").prop('checked', false);
                        $("#ROMNo").prop('checked', false);
                        $('#prcheck').addClass('hide');
                        $('#ddlForeigner').val('').attr('disabled', 'disabled');
                        $('#bumiputraStatus').removeAttr('disabled');
                        $('#bumiputraStatus').val('');
                    }
                    else {
                        $("#ROMYes").prop('checked', false);
                        $("#ROMNo").prop('checked', false);
                        $('#prcheck').addClass('hide');
                    }
                }
            });
            $('.checkbox-group-10 input:checkbox').click(function () {
                $('.checkbox-group-10 input:checkbox').not(this).prop('checked', false);
            });
            $('.chkBoxGroupSIFA1 input:checkbox').click(function () {
                $('.chkBoxGroupSIFA1 input:checkbox').not(this).prop('checked', false);
                if ($(this).attr('id') == 'chkBoxSIFAYes') {
                    if ($(this).is(':checked')) {
                        $('.chkBoxGroupSIFA2').removeClass('hide');
                    }
                    else {
                        $('.chkBoxGroupSIFA2').addClass('hide');
                    }
                }
                else if ($(this).attr('id') == 'chkBoxSIFANo') {
                    if ($(this).is(':checked')) {
                        $('.chkBoxGroupSIFA2').addClass('hide');
                    }
                    else {
                        $('.chkBoxGroupSIFA2').addClass('hide');
                    }
                }
            });
            $('.chkBoxGroupSIFA2 input:checkbox').click(function () {
                $('.chkBoxGroupSIFA2 input:checkbox').not(this).prop('checked', false);
            });
            $('#multiple-tax-residency-status-row').on('click', '.chkNoTINNum', function () {
                if ($(this).is(':checked')) {
                    $(this).parents('.checkbox-parent').find('.tin-available').addClass('hide');
                    $(this).parents('.checkbox-parent').find('.no-TIN').removeClass('hide');
                    $(this).parents('.checkbox-parent').find('.no-TIN').val('');
                    $(this).parents('.checkbox-parent').next().find('.txtTINNum').val('');
                    //$('.tin-available').addClass('hide');
                }
                else {
                    $(this).parents('.checkbox-parent').find('.tin-available').removeClass('hide');
                    $(this).parents('.checkbox-parent').find('.no-TIN').addClass('hide');
                    //$('.tin-available').removeClass('hide');
                }
            });

            $('#multiple-tax-residency-status-row').on('change', '.txtnoTIN', function () {
                if ($(this).val() == 'The Account Holder is otherwise unable to obtain a TIN or equivalent number') {
                    $(this).next().removeClass('hide');
                    //$('.tin-available').addClass('hide');
                }
                else {
                    $(this).next().addClass('hide');
                    $(this).next().val('');
                    //$('.tin-available').removeClass('hide');
                }
            });

            $('#ddlEduLvl').change(function () {
                if ($(this).val() == 'Others') {
                    $('.ddlEduLvlOthers').removeClass('hide');
                }
                else {
                    $('.ddlEduLvlOthers').addClass('hide');
                }
            });

            $('#ddlOccupation').change(function () {
                if ($(this).val() == 'Others') {

                    $('.occupation-others').removeClass('hide');

                }
                else {
                    $('.occupation-others').addClass('hide');
                }
            });

            $('#race').change(function () {
                if ($(this).val() == 'Others') {

                    $('.race-others').removeClass('hide');

                }
                else {
                    $('.race-others').addClass('hide');
                    $('#txtRaceDesc').val('');
                }
            });

            $('#ddlPurposeOfTransaction').change(function () {
                if ($(this).val() == 'Others') {

                    $('.purposeOfTransaction-others').removeClass('hide');

                }
                else {
                    $('.purposeOfTransaction-others').addClass('hide');
                    $('#txtPurposeOfTransaction').val('');
                }
            });


            $('#ddlSourceOfFunds').change(function () {
                if ($(this).val() == 'Others') {
                    $('#source-others').removeClass('hide');
                }
                else {
                    $('#source-others').addClass('hide');
                }
            });

            $('.contact-main *').filter(':input').keyup(function () {
                if ($("#sameAsResidentialAddr").is(':checked')) {
                    $('#addr1M').val($('#addr1').val());
                    $('#addr2M').val($('#addr2').val());
                    $('#addr3M').val($('#addr3').val());
                    $('#cityM').val($('#city').val());
                    $('#postCodeM').val($('#postCode').val());
                    $('#stateM').val($('#state').val());
                    $('#countryM').val($('#country').val());


                }


            });
            $('.contact-main *').filter(':input').change(function () {
                if ($("#sameAsResidentialAddr").is(':checked')) {
                    $('#addr1M').val($('#addr1').val());
                    $('#addr2M').val($('#addr2').val());
                    $('#addr3M').val($('#addr3').val());
                    $('#cityM').val($('#city').val());
                    $('#postCodeM').val($('#postCode').val());
                    $('#stateM').val($('#state').val());
                    $('#countryM').val($('#country').val());
                }
            });

            function IsReCaptchaValid() {
                $.ajax({
                    url: "AccountOpening.aspx/IsReCaptchaValid",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: $('form').serializeArray(),
                    success: function (data) {
                        console.log(data);
                        var json = data.d;
                    }
                });
                return true;
            }

            //$('#txtNRIC').blur(function () {
            //    if (validateCheck == 1)
            //        ValidateStep2();
            //    else {
            //        if ($('#txtNRIC').val().trim() == "") {
            //            $('#txtNRIC').addClass('field-required');
            //        }
            //        else {
            //            $('#txtNRIC').removeClass('field-required');
            //        }
            //    }
            //});
            $('#nationalityChecks').blur(function () {
                if (validateCheck == 1)
                    ValidateStep2();
                else {
                    if ($('#nationalityChecks').val().trim() == "") {
                        $('<div class="err-msg" id="err-nationality" style="width:100%;"><small style="font-size: 12px; color: red ! important;">Nationality is required.</small></div>').insertAfter('#nationalityChecks');
                    }
                    else {
                        $('#err-nationality').remove();
                    }
                }
            });
            $('#txtNRIC').blur(function () {
                if (validateCheck == 1)
                    ValidateStep2();
                else {
                    if ($('#txtNRIC').val().trim() == "") {
                        $('#txtNRIC').addClass('field-required');
                    }
                    else {
                        $('#txtNRIC').removeClass('field-required');
                    }
                }
            });
            $('#ddlPurposeOfTransaction').blur(function () {

                if ($('#ddlPurposeOfTransaction').val().trim() == "") {
                    $('#ddlPurposeOfTransaction').addClass('field-required');
                }
                else {
                    $('#ddlPurposeOfTransaction').removeClass('field-required');
                }
            
            });
            $('#txtMobileNo').blur(function () {
                if (validateCheck == 1)
                    ValidateStep2();
                else {
                    if ($('#txtMobileNo').val().trim() == "") {
                        $('#txtMobileNo').addClass('field-required');
                    }
                    else {
                        $('#txtMobileNo').removeClass('field-required');
                    }
                }
            });

            $('#txtNRIC').inputFilter(function (value) {
                return /^([a-zA-Z0-9]){0,40}?$/.test(value);
            });

            $('#txtMobileNo').inputFilter(function (value) {
                return /^(\d{0,19})?$/.test(value);
            });

            $('#fullName').inputFilter(function (value) {
                return /^([\D]){0,40}?$/.test(value);
            });
            $('#txtEmployerName').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#postCode').inputFilter(function (value) {
                console.log(/^(\d{0,6})?$/.test(value));
                return /^(\d{0,6})?$/.test(value);
            });
            $('#postCodeM').inputFilter(function (value) {
                return /^(\d{0,6})?$/.test(value);
            });
            $('#txtPostCode').inputFilter(function (value) {
                return /^(\d{0,6})?$/.test(value);
            });
            $('#telNoH').inputFilter(function (value) {
                return /^(\d{0,20})?$/.test(value);
            });
            $('#txtTelephone').inputFilter(function (value) {
                return /^(\d{0,20})?$/.test(value);
            });
            $('#addr1').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#addr2').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#addr3').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#addr1M').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#addr2M').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#addr3M').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#txtOfficeAddress1').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#txtOfficeAddress2').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#txtOfficeAddress3').inputFilter(function (value) {
                return /^.{0,40}$/.test(value);
            });
            $('#txtEmailCode').inputFilter(function (value) {
                return /^(\d{0,6})?$/.test(value);
            });
            $('#txtBankAccountNo').inputFilter(function (value) {
                //var bankcount = parseInt($('#hdnBankNoFormat').val()); //parseInt($('option:selected', '#ddlBank').attr('accountnumlength'));
                //if (value.length > bankcount) {
                //    return false;
                //}
                //else {
                    return /^\d*$/.test(value);
                //}
                //return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 500);
            });

            $('#btnRequest').click(function () {
                gRecaptchaResponse = grecaptcha.getResponse();
                $('#gRecaptchaResponse').val(gRecaptchaResponse);
                console.log(gRecaptchaResponse);
                $.ajax({
                    url: "AccountOpening.aspx/PhoneVerify",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: {
                        mobileNo: JSON.stringify($('#txtMobileNo').val()),
                        idNo: JSON.stringify($('#txtNRIC').val()),

                        gRecaptchaResponse: JSON.stringify(gRecaptchaResponse),
                        LocalIPAddress: JSON.stringify($('#hdnLocalIPAddress').val())
                    },
                    success: function (data) {

                        var jsons = data.d.Message.split(',');
                        var json = jsons[0];
                        $('#txtOTP').val('');
                        //console.log(json);
                        if (json == "sent") {
                            $('#lblInfo').html("Authentication OTP sent to mobile number " + jsons[1] + " successfully.");
                            $('#countdownLabelText').html('Please Enter your OTP in');
                            $('#countDownTime').removeClass('hide');
                            $('#btnRequest').attr('disabled', 'disabled');
                            hdnSMSExpirationTimeForAOInSeconds = jsons[2];
                            StartTimer();
                            var x = /^(\d{0,19})?$/;
                            var y = $('#txtNRIC').val();

                        }
                        else if (json == "already sent") {
                            $('#lblInfo').html("OTP already sent to mobile number " + jsons[1] + " .");
                            $('#countdownLabelText').html('Please Enter your OTP in');
                            $('#countDownTime').removeClass('hide');
                            $('#btnRequest').attr('disabled', 'disabled');
                            hdnSMSExpirationTimeForAOInSeconds = jsons[2];
                            StartTimer();
                        }
                        else if (json == "String was not recognized as a valid DateTime.") {
                            ShowCustomMessage("Alert", "Invalid NRIC/Passport format.", "");
                        }
                        else {
                            ShowCustomMessage("Alert", json, "");
                            if (json == 'Please verify yourself!' || json == 'This Id Number has already been registered' || json == 'This Id Number has already been registered as joint with you.' || json == 'Please enter valid Mobile number!' || json == 'Aged below 18 are not allowed')
                                recaptchaExpired();
                        }
                        if ($('#txtNRIC').val().length == 12 && Number.isInteger($('#txtNRIC').val())) {
                            $('#dob').attr('disabled', 'disabled');
                        }
                        else {
                            $('#dob').removeAttr('disabled');
                        }

                    },
                    error: function (jqXHR, exception) {
                        $('.loader-bg').fadeOut();
                        var msg = '';
                        if (jqXHR.status === 0) {
                            msg = 'Not connect.\n Verify Network.';
                        } else if (jqXHR.status == 404) {
                            msg = 'Requested page not found. [404]';
                        } else if (jqXHR.status == 500) {
                            msg = 'Internal Server Error [500].';
                        } else if (exception === 'parsererror') {
                            msg = 'Requested JSON parse failed.';
                        } else if (exception === 'timeout') {
                            msg = 'Time out error.';
                        } else if (exception === 'abort') {
                            msg = 'Ajax request aborted.';
                        } else {
                            msg = 'Uncaught Error.\n' + jqXHR.responseText;
                        }
                        ShowCustomMessage('Error', msg, '');
                    }
                });
            });

            $('#btnCodeRequest').click(function () {
                //gRecaptchaResponse = grecaptcha.getResponse();
                //console.log(gRecaptchaResponse);
                $.ajax({
                    url: "AccountOpening.aspx/SendEmail",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { address: JSON.stringify($('#email').val()), title: JSON.stringify("Verification Code for Account Opening"), content: JSON.stringify("Verification Code"), type: 1, mobileNo: JSON.stringify($('#txtMobileNo').val()), idno: JSON.stringify($('#txtNRIC').val()), name: JSON.stringify($('#fullName').val()), relation: 0 },
                    success: function (data) {
                        var jsons = data.d.Message.split(',');
                        var json = jsons[0];
                        //console.log(json);

                        if (json == "sent") {
                            $('#lblEmailCode').html("Verification Email has been sent to your email");
                            $('#hdnEmailRequested').val('1');
                            $('#hdnEmail').val($('#email').val());
                            //$('#emailCountdownLabelText').html('You can request again in');
                            //$('#emailCountDownTime').removeClass('hide');
                            //$('#btnCodeRequest').attr('disabled', 'disabled');
                            //hdnSMSExpirationTimeForAOInSeconds = jsons[1];
                            //StartTimer();
                        }
                        else if (json == "already sent") {
                            $('#lblEmailCode').html("Verification code already sent to your email.");
                            $('#hdnEmailRequested').val('1');
                            $('#hdnEmail').val($('#email').val());
                            //$('#emailCountDownTime').removeClass('hide');
                            //$('#btnCodeRequest').attr('disabled', 'disabled');
                            //hdnSMSExpirationTimeForAOInSeconds = jsons[1];
                            //StartTimer();
                        }
                        else {
                            ShowCustomMessage("Alert", json, "");
                        }
                    }
                });
            });

            //$('#txtOTP').keyup(function () {
            //    if ($(this).val().length == 6) {
            //        var formArray = [];
            //        $('form *').filter(':input').each(function () {
            //            formArray.push({
            //                name: $(this).attr('id'),
            //                value: $(this).attr('id') == "hdnBtnClickedId" ? "btnNextStep1" : $(this).val()
            //            });
            //        });
            //        formArray = formArray.filter(function (item) {
            //            return item.name != undefined && item.name.indexOf('_') === -1;
            //        });
            //        $.ajax({
            //            url: "AccountOpening.aspx/btnNext_Click",
            //            contentType: 'application/json; charset=utf-8',
            //            type: "POST",
            //            dataType: "JSON",
            //            data: JSON.stringify({ formInputs: formArray }),
            //            success: function (data) {
            //                var json = data.d;
            //                if (!json.IsSuccess) {
            //                    $('#txtOTP').addClass('field-required');
            //                }
            //                else {
            //                    $('#txtOTP').addClass('field-success');
            //                    //$('.buttonArea [data-step="next"]').removeClass('hide');
            //                }
            //            }
            //        });
            //    }
            //});

            $('#email').blur(function () {
                $('#err-email').remove();
                if ($('#email').val().trim() == "") {
                    $('#email').addClass('field-required');
                    $('<div id="err-email" class="err-msg"><small style="font-size: 12px; color: red ! important;">Email is required</small></div>').insertAfter('#email');
                }
                else {
                    $('#email').removeClass('field-required');
                    $('#err-email').remove();
                    $.ajax({
                        url: "AccountOpening.aspx/CheckEmail",
                        contentType: 'application/json; charset=utf-8',
                        type: "GET",
                        dataType: "JSON",
                        data: { email: JSON.stringify($("#email").val()) },
                        success: function (data) {
                            //$('#err-email').remove();
                            //$('#email').removeClass('field-required');

                            var json = data.d;
                            $('#err-email').remove();

                            if (data.d.Message != null) {
                                $('#email').addClass('field-required');
                                $('<div id="err-email" class="err-msg"><small style="font-size: 12px; color: red ! important;">' + data.d.Message + '</small></div>').insertAfter('#email-msg');
                            }





                        },
                        error: function (err) {
                            $('#err-email').remove();
                            $('#password').removeClass('field-required');
                        }

                    });
                    if ($('#email').val() == $('#hdnEmail').val()) {
                        if ($('#hdnEmailVerified').val() == '0') {
                            if ($('#requestCodeDiv').hasClass('hide')) {
                                $('#requestCodeDiv').removeClass('hide');
                            }
                        }
                        else {
                            if (!$('#requestCodeDiv').hasClass('hide')) {
                                $('#requestCodeDiv').addClass('hide');
                            }
                        }

                    }
                    else {
                        if ($('#requestCodeDiv').hasClass('hide')) {
                            $('#requestCodeDiv').removeClass('hide');
                        }
                    }
                }

            });
            $('#password').blur(function () {
                $('#err-password').remove();
                if ($('#password').val().trim() == "") {
                    $('#password').addClass('field-required');
                }
                else {
                    $('#password').removeClass('field-required');
                }

                $.ajax({
                    url: "AccountOpening.aspx/CheckPassword",
                    contentType: 'application/json; charset=utf-8',
                    type: "GET",
                    dataType: "JSON",
                    data: { password: JSON.stringify($("#password").val()) },
                    success: function (data) {
                        $('#err-password').remove();
                        $('#password').removeClass('field-required');

                        var json = data.d;


                        if (data.d.Message != null) {
                            $('#password').addClass('field-required');
                            $('<div id="err-password" class="err-msg"><small style="font-size: 12px; color: red ! important;">' + data.d.Message + '</small></div>').insertAfter('#pw-msg');
                        }





                    },
                    error: function (err) {
                        $('#err-password').remove();
                        $('#password').removeClass('field-required');
                    }

                });
            });
            $('#repassword').blur(function () {
                $('#err-repassword').remove();
                $('#passwordNotMatch').addClass('hide');
                if ($('#repassword').val().trim() == "") {
                    $('#repassword').addClass('field-required');
                    $('<div id="err-repassword" class="err-msg"><small style="font-size: 12px; color: red ! important;">Please re-enter your password</small></div>').insertAfter('#repassword');
                }
                else if ($('#password').val().trim() != $('#repassword').val().trim()) {
                    $('#repassword').addClass('field-required');
                    if ($('#passwordNotMatch').hasClass('hide')) {
                        $('#passwordNotMatch').removeClass('hide');
                    }
                }
                else
                    $('#repassword').removeClass('field-required');
            });

            $('#title').change(function () {
                if (validateCheckPersonalInfo == 1)
                    ValidateStep4();
                else {
                    if ($('#title').val() == "") {
                        $('#title').addClass('field-required');
                    }
                    else if ($('#title').val() == 'Others') {
                        $('#title').removeClass('field-required');
                        $('#titleOther').removeClass('d-none');
                    }
                    else {
                        $('#title').removeClass('field-required');
                        $('#titleOther').addClass('d-none');
                    }
                }
            });

            //$('#titleOther').blur(function () {
            //    if (validateCheckPersonalInfo == 1)
            //        ValidateStep4();
            //    else {
            //        if ($('#titleOther').val().trim() == "") {
            //            $('#titleOther').addClass('field-required');
            //        }
            //        else {
            //            $('#titleOther').removeClass('field-required');
            //        }
            //    }
            //});
            $('#fullName').blur(function () {
                if (validateCheckPersonalInfo == 1)
                    ValidateStep4();
                else {
                    if ($('#fullName').val().trim() == "") {
                        $('#fullName').addClass('field-required');
                    }
                    else {
                        $('#fullName').removeClass('field-required');
                    }
                }
            });
            $('#dob').blur(function () {
                if (validateCheckPersonalInfo == 1)
                    ValidateStep4();
                else {
                    if ($('#dob').val().trim() == "") {
                        $('#dob').addClass('field-required');
                    }
                    else {
                        $('#dob').removeClass('field-required');
                    }
                }
            });
            $('#gender').blur(function () {
                if (validateCheckPersonalInfo == 1)
                    ValidateStep4();
                else {
                    if ($('#gender').val().trim() == "") {
                        $('#gender').addClass('field-required');
                    }
                    else {
                        $('#gender').removeClass('field-required');
                    }
                }
            });
            $('#race').blur(function () {
                if (validateCheckPersonalInfo == 1)
                    ValidateStep4();
                else {
                    if ($('#race').val().trim() == "") {
                        $('#race').addClass('field-required');
                    }
                    else {
                        $('#race').removeClass('field-required');
                    }
                }
            });
            $('#maritalStatus').blur(function () {
                if (validateCheckPersonalInfo == 1)
                    ValidateStep4();
                else {
                    if ($('#maritalStatus').val().trim() == "") {
                        $('#maritalStatus').addClass('field-required');
                    }
                    else {
                        $('#maritalStatus').removeClass('field-required');
                    }
                }
            });
            $('#bumiputraStatus').blur(function () {
                ValidateStep4();
            });

            $('#addr1').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#addr1').val().trim() == "") {
                        $('#addr1').addClass('field-required');
                    }
                    else {
                        $('#addr1').removeClass('field-required');
                    }
                }
            });
            $('#city').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#city').val().trim() == "") {
                        $('#city').addClass('field-required');
                    }
                    else {
                        $('#city').removeClass('field-required');
                    }
                }
            });
            $('#postCode').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#postCode').val().trim() == "") {
                        $('#postCode').addClass('field-required');
                    }
                    else {
                        $('#postCode').removeClass('field-required');
                    }
                }
            });
            $('#state').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#state').val().trim() == "") {
                        $('#state').addClass('field-required');
                    }
                    else {
                        $('#state').removeClass('field-required');
                    }
                }
            });
            $('#country').blur(function () {
                ValidateStep5();
            });

            $('#addr1M').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#addr1M').val().trim() == "") {
                        $('#addr1M').addClass('field-required');
                    }
                    else {
                        $('#addr1M').removeClass('field-required');
                    }
                }
            });
            $('#cityM').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#cityM').val().trim() == "") {
                        $('#cityM').addClass('field-required');
                    }
                    else {
                        $('#cityM').removeClass('field-required');
                    }
                }
            });
            $('#postCodeM').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#postCodeM').val().trim() == "") {
                        $('#postCodeM').addClass('field-required');
                    }
                    else {
                        $('#postCodeM').removeClass('field-required');
                    }
                }
            });
            $('#stateM').blur(function () {
                if (validateCheckContactInfo == 1)
                    ValidateStep5();
                else {
                    if ($('#stateM').val().trim() == "") {
                        $('#stateM').addClass('field-required');
                    }
                    else {
                        $('#stateM').removeClass('field-required');
                    }
                }
            });
            $('#countryM').blur(function () {
                ValidateStep5();
            });

            $("#sameAsResidentialAddr").click(function () {
                if ($(this).is(':checked')) {
                    $('#addr1M').val($('#addr1').val());
                    $('#addr2M').val($('#addr2').val());
                    $('#addr3M').val($('#addr3').val());
                    $('#cityM').val($('#city').val());
                    $('#postCodeM').val($('#postCode').val());
                    $('#stateM').val($('#state').val());
                    $('#countryM').val($('#country').val());

                    $('#addr1M').attr('readonly', 'readonly');
                    $('#addr2M').attr('readonly', 'readonly');
                    $('#addr3M').attr('readonly', 'readonly');
                    $('#cityM').attr('readonly', 'readonly');
                    $('#postCodeM').attr('readonly', 'readonly');
                    $('#stateM').attr('readonly', 'readonly');
                    $('#countryM').attr('disabled', 'disabled');
                    $('.err-msg').remove();

                }
                else {
                    $('#addr1M').val('');
                    $('#addr2M').val('');
                    $('#addr3M').val('');
                    $('#cityM').val('');
                    $('#postCodeM').val('');
                    $('#stateM').val('');
                    $('#countryM').val('');

                    $('#addr1M').removeAttr('readonly');
                    $('#addr2M').removeAttr('readonly');
                    $('#addr3M').removeAttr('readonly');
                    $('#cityM').removeAttr('readonly');
                    $('#postCodeM').removeAttr('readonly');
                    $('#stateM').removeAttr('readonly');
                    $('#countryM').removeAttr('disabled');
                }
                ValidateStep5();
            });
            $('#ddlPurposeOfTransaction').blur(function () {
                
                    if ($('#ddlPurposeOfTransaction').val().trim() == "Select") {
                        $('#ddlPurposeOfTransaction').addClass('field-required');
                    }
                    else {
                        $('#ddlPurposeOfTransaction').removeClass('field-required');
                    }
                
            });
            $('#txtPurposeOfTransaction').blur(function () {
                
                    if ($('#ddlPurposeOfTransaction').val().trim() == "Others" && $('#txtPurposeOfTransaction').val().trim() == "") {
                        $('#txtPurposeOfTransaction').addClass('field-required');
                    }
                    else {
                        $('#txtPurposeOfTransaction').removeClass('field-required');
                    }
                
            });
            $('#ddlSourceOfFunds').blur(function () {
                
                    if ($('#ddlSourceOfFunds').val().trim() == "Select") {
                        $('#ddlSourceOfFunds').addClass('field-required');
                    }
                    else {
                        $('#ddlSourceOfFunds').removeClass('field-required');
                    }
                
            });
            $('#ddlRelationship').blur(function () {
                
                    if ($('#ddlSourceOfFunds').val().trim() == "Others" && $('#ddlRelationship').val().trim() == "Select") {
                        $('#ddlRelationship').addClass('field-required');
                    }
                    else {
                        $('#ddlRelationship').removeClass('field-required');
                    }
                
            });
            $('#txtFunderName').blur(function () {
                
                    if ($('#ddlSourceOfFunds').val().trim() == "Others" && $('#txtFunderName').val().trim() == "") {
                        $('#txtFunderName').addClass('field-required');
                    }
                    else {
                        $('#txtFunderName').removeClass('field-required');
                    }
                
            });
            $('#ddlFunderIndustry').blur(function () {
                
                    if ($('#ddlSourceOfFunds').val().trim() == "Others" && $('#ddlFunderIndustry').val().trim() == "Select") {
                        $('#ddlFunderIndustry').addClass('field-required');
                    }
                    else {
                        $('#ddlFunderIndustry').removeClass('field-required');
                    }
                
            });
            $('#txtFundOwnerName').blur(function () {

                if ($('#ddlSourceOfFunds').val().trim() == "Others" && $('#txtFundOwnerName').val().trim() == "") {

                        $('#txtFundOwnerName').addClass('field-required');
                    }
                    else {
                        $('#txtFundOwnerName').removeClass('field-required');
                }
                
            });

            $('#ddlAnnualIncome').blur(function () {

                if ($('#ddlAnnualIncome').val().trim() == "Select") {
                    $('#ddlAnnualIncome').addClass('field-required');
                }
                else {
                    $('#ddlAnnualIncome').removeClass('field-required');
                }

            });

            $('#ddlEstimatedNetWorth').blur(function () {
                
                    if ($('#ddlEstimatedNetWorth').val().trim() == "Select") {
                        $('#ddlEstimatedNetWorth').addClass('field-required');
                    }
                    else {
                        $('#ddlEstimatedNetWorth').removeClass('field-required');
                    }
                
            });

            $('#addTaxResidencyStatus').click(function () {
                var divTax = $('.out-malaysia-yes').first();
                var divTaxClone = $(divTax).clone();
                $(divTaxClone).find(':input').val('');
                $(divTaxClone).find('input[type=checkbox]').val('on');
                $(divTaxClone).find('.tax-residency-remove').removeClass('hide');
                if ($(divTaxClone).find('.optionBReason').hasClass('hide')) { }
                else {
                    $(divTaxClone).find('.optionBReason').addClass('hide');
                }

                $('#multiple-tax-residency-status-row').append(divTaxClone);
            });

            $('#multiple-tax-residency-status-row').on('click', '.tax-residency-remove', function () {
                $(this).parents('.out-malaysia-yes').remove();
            });

            $('#uploadNRICFrontPage').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length > 0) {
                    $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadNRICFrontPage').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadNRICFrontPage').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span:not(.account-type)').html("<img src='" + blobURL + "' alt='NRIC Front Page' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span:not(.account-type)').html("<a target='_blank' href='" + blobURL + "' alt='NRIC Front Page' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                    }

                }
            });
            $('#uploadNRICBackPage').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length > 0) {
                    $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadNRICBackPage').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadNRICBackPage').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span:not(.account-type)').html("<img src='" + blobURL + "' alt='NRIC Back Page' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span:not(.account-type)').html("<a target='_blank' href='" + blobURL + "' alt='NRIC Back Page' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                        //$(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='NRIC Back Page' title='" + FileName + "' />");
                    }
                }
            });

            $('#uploadSelfieFile').change(function () {
                $('#custom-popup-close').addClass('hide');
                ShowCustomMessage('Loading...', 'Please wait while we are processing the image.', '');
                try {
                    var fileInput = $(this)[0];
                    if ($(this)[0].files.length > 0) {
                        $('.loader-bg').fadeIn();
                        var fileType = $(this)[0].files[0]["type"];
                        var validImageTypes = ["image/jpeg", "image/png", "image/jpg"];
                        if ($.inArray(fileType, validImageTypes) < 0) {
                            $('#custom-popup-close').removeClass('hide');
                            $('#uploadSelfieFile').val('');
                            $('.loader-bg').fadeOut();
                            ShowCustomMessage('Alert', 'Invalid file', '');
                        }
                        else {
                            $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                            const fsize = $(this)[0].files[0].size;
                            const fileSizeMB = Math.round((fsize / 1024));
                            if (fileSizeMB >= 2048) {
                                ShowCustomMessage('Alert', 'File too Big, please select a file less than 2 MB', '');
                                $('#custom-popup-close').removeClass('hide');
                                $('#uploadSelfieFile').val('');
                                $('.loader-bg').fadeOut();
                                return false;
                            } else if (fileSizeMB < 20) {
                                ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                                $('#custom-popup-close').removeClass('hide');
                                $('#uploadSelfieFile').val('');
                                $('.loader-bg').fadeOut();
                                return false;
                            } else {
                                var FileName = $(this)[0].files[0].name;
                                var reader = new FileReader();
                                var baseString;
                                reader.onloadend = function () {
                                    baseString = reader.result;
                                    //console.log(baseString);
                                    $('#inputImg').get(0).src = baseString;
                                    updateResults();
                                };
                                reader.readAsDataURL($(this)[0].files[0]);
                                //var blobURL = URL.createObjectURL($(this)[0].files[0]);
                                //$('#inputImg').get(0).src = blobURL;
                                //$(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='Selfie with NRIC' title='" + FileName + "' />");
                            }
                        }
                    }
                }
                catch (ex) {
                    ShowCustomMessage('Alert', 'Invalid file', '');
                    $('#custom-popup-close').removeClass('hide');
                    $('.loader-bg').fadeOut();
                }
            });

            //$('#uploadSelfie').change(function () {
            //    var fileInput = $(this)[0];
            //    if ($(this)[0].files.length > 0) {
            //        var FileName = $(this)[0].files[0].name;
            //        var blobURL = URL.createObjectURL($(this)[0].files[0]);
            //        $(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='Selfie with NRIC' title='" + FileName + "' />");
            //    }
            //});
            $('#uploadSignature').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length > 0) {
                    $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadSignature').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadSignature').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span:not(.account-type)').html("<img src='" + blobURL + "' alt='Signature' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span:not(.account-type)').html("<a target='_blank' href='" + blobURL + "' alt='Signature' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                        //$(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='Signature document' title='" + FileName + "' />");
                    }
                }
            });
            $('#uploadAdditional').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length == 1) {
                    $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadAdditional').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadAdditional').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span:not(.account-type)').html("<img src='" + blobURL + "' alt='Additional document' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span:not(.account-type)').html("<a target='_blank' href='" + blobURL + "' alt='Additional document' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                        //$(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='Additional document' title='" + FileName + "' />");
                    }
                }
                else if ($(this)[0].files.length > 1) {
                    $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    $.each($(this)[0].files, function (idx, obj) {
                        const fsize = $(this)[0].files[0].size;
                        const fileSizeMB = Math.round((fsize / 1024));
                        if (fileSizeMB >= 4096) {
                            ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                            $('#uploadAdditional').val('');
                            //return false;
                        } else if (fileSizeMB < 20) {
                            ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                            $('#uploadAdditional').val('');
                            //return false;
                        } else {
                            var FileName = obj.name;
                            var blobURL = URL.createObjectURL(obj);
                            //if (obj.type.indexOf('image') >= 0)
                            //$(fileInput).prev().find('span').append("<img class='multi' src='" + blobURL + "' alt='Additional document' title='" + FileName + "' />");
                            //else
                            $(fileInput).prev().find('span:not(.account-type)').append("<a class='multi' target='_blank' href='" + blobURL + "' alt='Additional document' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                            //$(fileInput).prev().find('span').append("<img src='" + blobURL + "' alt='Additional document' title='" + FileName + "' />");
                        }
                    });

                }
            });
            $('#uploadSupporting').change(function () {
                var fileInput = $(this)[0];
                if ($(this)[0].files.length == 1) {
                    $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    const fsize = $(this)[0].files[0].size;
                    const fileSizeMB = Math.round((fsize / 1024));
                    if (fileSizeMB >= 4096) {
                        ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                        $('#uploadSupporting').val('');
                        return false;
                    } else if (fileSizeMB < 20) {
                        ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                        $('#uploadSupporting').val('');
                        return false;
                    } else {
                        var FileName = $(this)[0].files[0].name;
                        var blobURL = URL.createObjectURL($(this)[0].files[0]);
                        if ($(this)[0].files[0].type.indexOf('image') >= 0)
                            $(fileInput).prev().find('span:not(.account-type)').html("<img src='" + blobURL + "' alt='Supporting document' title='" + FileName + "' style='height: 100%'/>");
                        else
                            $(fileInput).prev().find('span:not(.account-type)').html("<a target='_blank' href='" + blobURL + "' alt='Supporting document' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                        //$(fileInput).prev().find('span').html("<img src='" + blobURL + "' alt='Additional document' title='" + FileName + "' />");
                    }
                }
                else if ($(this)[0].files.length > 1) {
                    $(fileInput).prev().find('span:not(.account-type)').html('<i class="fa fa-file-image-o" style="max-height: 180px !important;"></i>');
                    $.each($(this)[0].files, function (idx, obj) {
                        const fsize = $(this)[0].files[0].size;
                        const fileSizeMB = Math.round((fsize / 1024));
                        if (fileSizeMB >= 4096) {
                            ShowCustomMessage('Alert', 'File too Big, please select a file less than 4 MB', '');
                            $('#uploadSupporting').val('');
                            //return false;
                        } else if (fileSizeMB < 20) {
                            ShowCustomMessage('Alert', 'File too small, please select a file greater than 20 KB', '');
                            $('#uploadSupporting').val('');
                            //return false;
                        } else {
                            var FileName = obj.name;
                            var blobURL = URL.createObjectURL(obj);
                            //if (obj.type.indexOf('image') >= 0)
                            //$(fileInput).prev().find('span').append("<img class='multi' src='" + blobURL + "' alt='Additional document' title='" + FileName + "' />");
                            //else
                            $(fileInput).prev().find('span:not(.account-type)').append("<a class='multi' target='_blank' href='" + blobURL + "' alt='Supporting document' title='" + FileName + "'><i class='fa fa-file-picture-o'></i></a>");
                            //$(fileInput).prev().find('span').append("<img src='" + blobURL + "' alt='Additional document' title='" + FileName + "' />");
                        }
                    });

                }
            });

            var noOfFiles = 0;
            var loadingFiles = [];
            var formArray = [];
            $('[data-step="next"], [data-step="finish"]').click(function () {
                noOfFiles = 0;
                loadingFiles = [];
                formArray = [];
                //$('.buttonArea [data-step="next"]').addClass('hide');
                var id = $(this).attr('id');
                $('#hdnBtnClickedId').val(id);
                $('form *').filter(':input').each(function () {
                    if ($(this).attr("type") == 'file') {
                        if ($(this)[0].files.length > 0) {
                            var inputIdPrefix = $(this).attr('id')
                            $.each($(this)[0].files, function (idxFile, objFile) {
                                noOfFiles++;
                                var inputId = inputIdPrefix + (idxFile == 0 ? "" : idxFile);
                                var FileName = objFile.name;
                                var extension = FileName.substr((FileName.lastIndexOf('.') + 1));
                                if (extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'pdf') {
                                    var fReader = new FileReader();
                                    fReader.readAsDataURL(objFile);
                                    fReader.onloadend = function (event) {
                                        formArray.push({
                                            name: inputId,
                                            value: event.target.result
                                        });
                                        formArray.push({
                                            name: inputId + "FileName",
                                            value: FileName
                                        });
                                        loadingFiles.push(true);
                                        console.log(noOfFiles + " - " + loadingFiles.length);
                                        if (noOfFiles == loadingFiles.length) {
                                            btnNextStepAjax(id, formArray);
                                        }
                                    }
                                }
                                else {
                                    $('.loader-bg').fadeOut();
                                    ShowCustomMessage("Alert", "Invalid file! Please try again.", "");
                                    return false;
                                }
                            });
                            //var inputId = $(this).attr('id');
                            //var FileName = $(this)[0].files[0].name;
                            ////var blobURL = URL.createObjectURL($(this)[0].files[0])
                            ////formArray.push({
                            ////    name: inputId,
                            ////    value: blobURL
                            ////});
                            ////btnNextStepAjax(id, formArray);
                            //var fReader = new FileReader();
                            //fReader.readAsDataURL($(this)[0].files[0]);
                            //fReader.onloadend = function (event) {
                            //    formArray.push({
                            //        name: inputId,
                            //        value: event.target.result
                            //    });
                            //    formArray.push({
                            //        name: inputId + "FileName",
                            //        value: FileName
                            //    });
                            //    loadingFiles.push(true);
                            //    console.log(noOfFiles + " - " + loadingFiles.length);
                            //    if (noOfFiles == loadingFiles.length) {
                            //        btnNextStepAjax(id, formArray);
                            //    }
                            //}
                        }
                    }
                    else if ($(this).attr("type") == 'checkbox') {
                        if ($(this).is(':checked')) {
                            formArray.push({
                                name: $(this).attr('id'),
                                value: $(this).val()
                            });
                        }
                        else {
                            formArray.push({
                                name: $(this).attr('id'),
                                value: null
                            });
                        }
                    }
                    else {
                        formArray.push({
                            name: $(this).attr('id'),
                            value: $(this).val()
                        });
                    }
                });
                //console.log(formArray);
                formArray = formArray.filter(function (item) {
                    return item.name != undefined && item.name.indexOf('_') === -1;
                });
                //console.log(formArray);
                //var formData = $('form').serializeArray();
                //console.log(formData);
                //console.log(noOfFiles);
                if (noOfFiles == 0)
                    btnNextStepAjax(id, formArray);
                //--------------------
            });
            //$('#ddlBank').change(function () {
            //    $('#txtBankAccountNo').val('');
            //    $('#txtBankAccountNo').attr('readonly', 'readonly');
            //    if ($('#ddlBank').val() != "") {
            //        $.ajax({
            //            url: "AccountOpening.aspx/GetBankAccNoLength",
            //            contentType: 'application/json; charset=utf-8',
            //            type: "GET",
            //            dataType: "JSON",
            //            async: true,
            //            data: { 'bankId': JSON.stringify($('#ddlBank').val()) },
            //            success: function (data) {
            //                $('#txtBankAccountNo').removeAttr('readonly');
            //                if (data.d.IsSuccess == true) {
            //                    $('#hdnBankNoFormat').val(data.d.Data);
            //                }
            //                else if (data.Message == 'Session expired') {
            //                    ShowCustomMessage("Alert", json.Message, 'AccountOpening.aspx');
            //                }
            //                else {
            //                    ShowCustomMessage('Alert', data.d.Message, '');
            //                }
            //            }
            //        });
            //    }
            //});
        });


        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }


        //console.log(faceapi.nets);
        faceapi.nets.ssdMobilenetv1.loadFromUri('/models');
        faceapi.nets.faceLandmark68TinyNet.loadFromUri('/models')
        const modelPath = '/models';
        async function updateResults() {
            try {
                //const webcamElement = document.getElementById('webcam1');
                //const canvasElement = document.getElementById('canvas1');
                //const webcam = new Webcam(webcamElement, 'user', canvasElement);
                //webcam.start()
                //    .then(result => {
                //        console.log("webcam started");
                //    })
                //    .catch(err => {
                //        console.log(err);
                //    });
                //$("#snapShotFromWebCam").click(function () {
                //    console.log("webcam picture");
                //    var picture = webcam.snap();
                //    console.log(picture);
                //    $('#inputImg').get(0).src = picture;
                //});

                //faceapi.nets.tinyFaceDetector.load(modelPath);
                //faceapi.nets.faceLandmark68TinyNet.load(modelPath);
                //faceapi.nets.faceRecognitionNet.load(modelPath);
                //faceapi.nets.faceExpressionNet.load(modelPath);
                //faceapi.nets.ageGenderNet.load(modelPath);

                //if (document.getElementsByTagName("canvas").length == 0) {
                //    canvas = faceapi.createCanvasFromMedia(webcamElement)
                //    document.getElementById('webcam-container').append(canvas)
                //    faceapi.matchDimensions(canvas, displaySize)
                //}

                //faceDetection = setInterval(async () => {
                //    const detections = await faceapi.detectAllFaces(webcamElement, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks(true).withFaceExpressions().withAgeAndGender()
                //    const resizedDetections = faceapi.resizeResults(detections, displaySize)
                //    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
                //    faceapi.draw.drawDetections(canvas, resizedDetections)
                //    faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
                //    faceapi.draw.drawFaceExpressions(canvas, resizedDetections)
                //    resizedDetections.forEach(result => {
                //        const { age, gender, genderProbability } = result
                //        new faceapi.draw.DrawTextField(
                //            [
                //                `${faceapi.round(age, 0)} years`,
                //                `${gender} (${faceapi.round(genderProbability)})`
                //            ],
                //            result.detection.box.bottomRight
                //        ).draw(canvas)
                //    })
                //}, 300)




                //console.log(isFaceDetectionModelLoaded());
                if (!isFaceDetectionModelLoaded()) {
                    return
                }

                const inputImgEl = $('#inputImg').get(0)
                const options = getFaceDetectorOptions()
                const useTinyModel = true
                const results = await faceapi.detectAllFaces(inputImgEl, options).withFaceLandmarks(useTinyModel)
                //console.log(results);
                if (results.length == 1) {
                    const canvas = $('#canvas').get(0);
                    console.log(canvas);
                    faceapi.matchDimensions(canvas, inputImgEl);
                    faceapi.draw.drawDetections(canvas, faceapi.resizeResults(results, inputImgEl));
                    ShowCustomMessage("Alert", "Face detected. Thank you.", "");
                    $('#custom-popup-close').removeClass('hide');
                    var img = inputImgEl;
                    var base64 = img.src;
                    $('#uploadSelfie').val(base64);
                    let r = Math.random().toString(36).substring(7);
                    console.log("random", r);
                    var contentType = img.src.split(',')[0].split(';')[0].split('/')[1];
                    if (contentType == 'png' || contentType == 'jpg' || contentType == 'jpeg') {
                        var today = new Date();
                        $('#uploadSelfieFileName').val('Webcam' + r + today.getFullYear() + "" + (today.getMonth() + 1) + "" + today.getDate() + "" + today.getHours() + "" + today.getMinutes() + "" + today.getSeconds() + "" + today.getMilliseconds() + "." + contentType);
                        $('.loader-bg').fadeOut();
                        //var base64 = img.src.split(',')[1];
                        //var contentType = img.src.split(',')[0].split(';')[0];
                        //var blob = b64toBlob(base64, contentType);
                        //var blobUrl = URL.createObjectURL(blob);
                        //console.log(blobUrl);
                        //URL ***************************************************************
                        $('#video').addClass('hide');
                        $('#snap').addClass('hide');
                        $('#webcam-pic').removeClass('hide');
                        $('#cam-option').addClass('hide');
                        $('#try-snap-again').removeClass('hide');
                        $('#upload-selfie-option').addClass('hide');
                    }
                    else {
                        $('.loader-bg').fadeOut();
                        ShowCustomMessage("Alert", "Invalid file! Please try again.", "");
                        $('#custom-popup-close').removeClass('hide');
                        $('#try-snap-again').removeClass('hide');
                    }
                }
                else if (results.length == 0) {
                    $('.loader-bg').fadeOut();
                    ShowCustomMessage("Alert", "No face detected! Please try again.", "");
                    $('#custom-popup-close').removeClass('hide');
                    $('#try-snap-again').removeClass('hide');
                }
                else if (results.length > 1) {
                    $('.loader-bg').fadeOut();
                    const canvas = $('#canvas').get(0);
                    faceapi.matchDimensions(canvas, inputImgEl);
                    faceapi.draw.drawDetections(canvas, faceapi.resizeResults(results, inputImgEl));
                    ShowCustomMessage("Alert", "Multiple faces detected! Please try again.", "");
                    $('#custom-popup-close').removeClass('hide');
                    $('#try-snap-again').removeClass('hide');
                }
            }
            catch (ex) {
                ShowCustomMessage('Alert', 'Invalid file', '');
                $('#custom-popup-close').removeClass('hide');
                $('.loader-bg').fadeOut();
            }
        }

        updateResults();
        // Grab elements, create settings, etc.
        var video = document.getElementById('video');

        // Get access to the camera!
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            // Not adding `{ audio: true }` since we only want video now
            navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
                //video.src = window.URL.createObjectURL(stream);
                video.srcObject = stream;
                video.play();
                $('#noCam').val("0");
                $('#cam-option').removeClass('hide');
            }, function () {
                document.getElementById('video').style.display = "none";
                $('#noCam').val("1");
                $('#cam-option').addClass('hide');
            });
        }

        /* Legacy code below: getUserMedia
    else if(navigator.getUserMedia) { // Standard
        navigator.getUserMedia({ video: true }, function(stream) {
            video.src = stream;
            video.play();
        }, errBack);
    } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia({ video: true }, function(stream){
            video.src = window.webkitURL.createObjectURL(stream);
            video.play();
        }, errBack);
    } else if(navigator.mozGetUserMedia) { // Mozilla-prefixed
        navigator.mozGetUserMedia({ video: true }, function(stream){
            video.srcObject = stream;
            video.play();
        }, errBack);
    }
    */

        // Elements for taking the snapshot
        var canvas = document.getElementById('canvas');
        var context = canvas.getContext('2d');
        var video = document.getElementById('video');



        // Trigger photo take
        document.getElementById("snap").addEventListener("click", function () {
            context.drawImage(video, 0, 0, 640, 480);
            var dataURL = $('#canvas')[0].toDataURL();
            //console.log(dataURL);
            $('#inputImg').get(0).src = dataURL;
            $('.loader-bg').fadeIn();
            ShowCustomMessage('Loading...', 'Please wait while we are processing the image.', '');
            $('#custom-popup-close').addClass('hide');
            updateResults();
        });

        document.getElementById("try-snap-again").addEventListener("click", function () {
            $('#video').removeClass('hide');
            $('#snap').removeClass('hide');
            $('#cam-option').removeClass('hide');
            $('#webcam-pic').addClass('hide');
            $('#try-snap-again').addClass('hide');
            $('#upload-selfie-option').removeClass('hide');
        });

        document.getElementById("existing-selfie").addEventListener("click", function () {
            $('#webcam').removeClass('hide');
            $('#existing-selfie').addClass('hide');
            $('#upload-selfie-option').removeClass('hide');
            if ($('#noCam').val() == "0") {
                $('#cam-option').removeClass('hide');
                //$('#webcam').removeClass('hide');
            }
            else {
                $('#cam-option').addClass('hide');
                //$('#webcam').addClass('hide');
                //$('#no-cam-div').removeClass('hide');
            }
        });
        //console.log(faceapi.nets);
        //faceapi.nets.ssdMobilenetv1.loadFromUri('/models');
        //faceapi.nets.faceLandmark68TinyNet.loadFromUri('/models')
        //const modelPath = '/models';
        //async function updateResults() {

        //    //const webcamElement = document.getElementById('webcam1');
        //    //const canvasElement = document.getElementById('canvas1');
        //    //const webcam = new Webcam(webcamElement, 'user', canvasElement);
        //    //webcam.start()
        //    //    .then(result => {
        //    //        console.log("webcam started");
        //    //    })
        //    //    .catch(err => {
        //    //        console.log(err);
        //    //    });
        //    //$("#snapShotFromWebCam").click(function () {
        //    //    console.log("webcam picture");
        //    //    var picture = webcam.snap();
        //    //    console.log(picture);
        //    //    $('#inputImg').get(0).src = picture;
        //    //});

        //    //faceapi.nets.tinyFaceDetector.load(modelPath);
        //    //faceapi.nets.faceLandmark68TinyNet.load(modelPath);
        //    //faceapi.nets.faceRecognitionNet.load(modelPath);
        //    //faceapi.nets.faceExpressionNet.load(modelPath);
        //    //faceapi.nets.ageGenderNet.load(modelPath);

        //    //if (document.getElementsByTagName("canvas").length == 0) {
        //    //    canvas = faceapi.createCanvasFromMedia(webcamElement)
        //    //    document.getElementById('webcam-container').append(canvas)
        //    //    faceapi.matchDimensions(canvas, displaySize)
        //    //}

        //    //faceDetection = setInterval(async () => {
        //    //    const detections = await faceapi.detectAllFaces(webcamElement, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks(true).withFaceExpressions().withAgeAndGender()
        //    //    const resizedDetections = faceapi.resizeResults(detections, displaySize)
        //    //    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
        //    //    faceapi.draw.drawDetections(canvas, resizedDetections)
        //    //    faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
        //    //    faceapi.draw.drawFaceExpressions(canvas, resizedDetections)
        //    //    resizedDetections.forEach(result => {
        //    //        const { age, gender, genderProbability } = result
        //    //        new faceapi.draw.DrawTextField(
        //    //            [
        //    //                `${faceapi.round(age, 0)} years`,
        //    //                `${gender} (${faceapi.round(genderProbability)})`
        //    //            ],
        //    //            result.detection.box.bottomRight
        //    //        ).draw(canvas)
        //    //    })
        //    //}, 300)




        //    console.log(isFaceDetectionModelLoaded());
        //    if (!isFaceDetectionModelLoaded()) {
        //        return
        //    }

        //    const inputImgEl = $('#inputImg').get(0)
        //    const options = getFaceDetectorOptions()
        //    const useTinyModel = true
        //    const results = await faceapi.detectAllFaces(inputImgEl, options).withFaceLandmarks(useTinyModel)
        //    const canvas = $('#canvas').get(0);
        //    console.log(canvas);
        //    faceapi.matchDimensions(canvas, inputImgEl);
        //    faceapi.draw.drawDetections(canvas, faceapi.resizeResults(results, inputImgEl));
        //}

        //updateResults();
        //// Grab elements, create settings, etc.
        //var video = document.getElementById('video');

        //// Get access to the camera!
        //if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        //    // Not adding `{ audio: true }` since we only want video now
        //    navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
        //        //video.src = window.URL.createObjectURL(stream);
        //        video.srcObject = stream;
        //        video.play();
        //    });
        //}

        /* Legacy code below: getUserMedia
    else if(navigator.getUserMedia) { // Standard
        navigator.getUserMedia({ video: true }, function(stream) {
            video.src = stream;
            video.play();
        }, errBack);
    } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia({ video: true }, function(stream){
            video.src = window.webkitURL.createObjectURL(stream);
            video.play();
        }, errBack);
    } else if(navigator.mozGetUserMedia) { // Mozilla-prefixed
        navigator.mozGetUserMedia({ video: true }, function(stream){
            video.srcObject = stream;
            video.play();
        }, errBack);
    }
    */

        //// Elements for taking the snapshot
        //var canvas = document.getElementById('canvas');
        //var context = canvas.getContext('2d');
        //var video = document.getElementById('video');



        //// Trigger photo take
        //document.getElementById("snap").addEventListener("click", function () {
        //    context.drawImage(video, 0, 0, 640, 480);
        //    var dataURL = $('#canvas')[0].toDataURL();
        //    //console.log(dataURL);
        //    $('#inputImg').get(0).src = dataURL;
        //    updateResults();
        //});


        $(function () {
            $('#occupation').change(function () {
                if ($(this).val() == 'Employment') {
                    $('#industryDiv').removeClass('d-none');
                }
                else {
                    $('#industryDiv').addClass('d-none');
                }
            });
        });
