﻿using DiOTP.Service;
using DiOTP.Service.IService;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.WebApp.ServiceCalls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static DiOTP.Utility.CustomClasses.CustomStatus;

namespace DiOTP.WebApp
{
    public partial class RSPList : System.Web.UI.Page
    {
        private static readonly Lazy<IUserOrderService> lazyUserOrderServiceObj = new Lazy<IUserOrderService>(() => new UserOrderService());

        public static IUserOrderService IUserOrderService { get { return lazyUserOrderServiceObj.Value; } }

        private static readonly Lazy<IUserAccountService> lazyUserAccountObj = new Lazy<IUserAccountService>(() => new UserAccountService());

        public static IUserAccountService IUserAccountService { get { return lazyUserAccountObj.Value; } }

        private static readonly Lazy<IUtmcFundInformationService> lazyUserAccountObj2 = new Lazy<IUtmcFundInformationService>(() => new UtmcFundInformationService());

        public static IUtmcFundInformationService IUtmcFundInformationService { get { return lazyUserAccountObj2.Value; } }

        private static readonly Lazy<IUserOrderFileService> lazyUserOrderFileObj = new Lazy<IUserOrderFileService>(() => new UserOrderFileService());

        public static IUserOrderFileService IUserOrderFileService { get { return lazyUserOrderFileObj.Value; } }

        public int UserChoosetype;
        public List<UserAccount> userAccounts { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Login.aspx');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Login to Proceed', 'Index.aspx');", true);
                    }
                }
                if (Session["isVerified"] == null)
                {
                    if (Request.Browser.IsMobileDevice)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Warning", "ShowCustomMessage('Alert', 'Please Verify to Proceed', 'Portfolio.aspx');", true);
                }
                if (Session["user"] != null && Session["isVerified"] != null)
                {

                    if (!IsPostBack)
                    {
                        User user = (User)Session["user"];

                        Response responseUAList = IUserAccountService.GetDataByFilter(" user_id = '" + user.Id + "' and status='1' ", 0, 0, false);
                        if (responseUAList.IsSuccess)
                        {

                            userAccounts = (List<UserAccount>)responseUAList.Data;

                            foreach (UserAccount x in userAccounts)
                            {
                                if (CustomValues.GetAccounPlan(x.HolderClass) != "EPF")
                                    ddlUserAccountId.Items.Add(new ListItem(CustomValues.GetAccounPlan(x.HolderClass) + " - " + x.AccountNo.ToString(), "'" + x.AccountNo + "'"));
                            }
                            ddlUserAccountId.Items.Insert(0, new ListItem("All Accounts", String.Join(",", userAccounts.Select(x => "'" + x.AccountNo + "'").ToList())));

                            string accNo = Request.QueryString["accNo"];
                            string typeString = Request.QueryString["type"];
                            if (!String.IsNullOrEmpty(accNo))
                            {
                                ddlUserAccountId.SelectedValue = "'" + accNo + "'";
                            }
                            if (!String.IsNullOrEmpty(typeString))
                            {
                                ddlOrderType.SelectedValue = typeString;
                            }
                            BindOrderList();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(this.Page.AppRelativeVirtualPath + " Page load: " + ex.Message);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Exception', \"" + ex.Message + "\", '');", true);
            }
        }

        public void BindOrderList()
        {
            if (Session["user"] != null && Session["isVerified"] != null && ddlUserAccountId.SelectedValue != "")
            {
                Boolean IsImpersonated = ServicesManager.IsImpersonated(Context);
                User user = (User)Session["user"];
                string accountNos = ddlUserAccountId.SelectedValue;
                string orderStatus = ddlOrderStatus.SelectedValue;
                string transPeriod = ddlTransactionPeriod.SelectedValue;
                if (orderStatus == "0")
                {
                    orderStatus = "('1')";
                }
                else if (orderStatus == "1")
                {
                    orderStatus = "('2','22')";
                }
                else if (orderStatus == "2")
                {
                    orderStatus = "('3')";
                }
                else if (orderStatus == "3")
                {
                    orderStatus = "('29','39')";
                }
                if (transPeriod == "-3")
                {
                    transPeriod = "TIMESTAMPDIFF(MONTH, uo.created_date, NOW()) < 3";
                }
                else if (transPeriod == "-6")
                {
                    transPeriod = "TIMESTAMPDIFF(MONTH, uo.created_date, NOW()) < 6";
                }
                else if (transPeriod == "-12")
                {
                    transPeriod = "TIMESTAMPDIFF(MONTH, uo.created_date, NOW()) < 12";
                }
                StringBuilder filter = new StringBuilder();
                filter.Append(@"SELECT uo.ID, uo.user_id, uo.user_account_id, ua.account_no, uo.order_no, uo.order_type, uo.created_date, uo.amount, uo.units, ufi.Fund_Name, ufi2.Fund_Name as Fund_Name2,
                            uo.payment_method, uo.payment_date, uo.settlement_date, uo.order_status, uo.fpx_transaction_id, uo.fpx_bank_code, uo.reject_reason FROM user_orders uo
                            left join user_accounts ua on ua.ID = uo.user_account_id
                            left join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            where uo.user_id='" + user.Id + "' and uo.order_type = '6' ");
                if (orderStatus != "")
                {
                    filter.Append(" and uo.order_status in " + orderStatus + " ");
                }
                else
                {
                    filter.Append(" and uo.order_status not in ('18', '19', '29', '39') ");
                }
                if (accountNos != "")
                {
                    filter.Append(" and ua.account_no in (" + accountNos + ") ");
                }
                if (transPeriod != "")
                {
                    filter.Append(" and " + transPeriod + " ");
                }

                filter.Append(" group by uo.order_no");
                filter.Append(" order by FIELD(uo.order_status, '1', '22', '2', '3'), uo.confirmation_date desc, uo.created_date desc ");
                Response responseUserOrderList = GenericService.GetDataByQuery(filter.ToString(), 0, 50, false, null, false, null, true);
                if (responseUserOrderList.IsSuccess)
                {
                    var rawDynData = responseUserOrderList.Data;
                    var responseJSON = JsonConvert.SerializeObject(rawDynData);
                    List<OrderListDTO> UserOrderList = JsonConvert.DeserializeObject<List<OrderListDTO>>(responseJSON);

                    StringBuilder tableBuilder = new StringBuilder();
                    int index = 1;
                    UserOrderList.ForEach(x =>
                    {
                        Response responseMO = GenericService.GetDataByQuery(@"SELECT uo.ID, uo.user_id, uo.user_account_id, ua.account_no, uo.order_no, uo.order_type, uo.created_date, uo.amount, uo.units, ufi.Fund_Name, ufi2.Fund_Name as Fund_Name2, uo.payment_method, uo.payment_date, uo.settlement_date, uo.order_status, uo.reject_reason FROM user_orders uo
                            left join user_accounts ua on ua.ID = uo.user_account_id
                            left join utmc_fund_information ufi on ufi.ID = uo.fund_id
                            left join utmc_fund_information ufi2 on ufi2.ID = uo.to_fund_id 
                            where uo.order_no='" + x.order_no + @"' ", 0, 0, false, null, false, null, true);

                        if (responseMO.IsSuccess)
                        {
                            var rawDynMOData = responseMO.Data;
                            var responseMOJSON = JsonConvert.SerializeObject(rawDynMOData);
                            List<OrderListDTO> UserOrderMOList = JsonConvert.DeserializeObject<List<OrderListDTO>>(responseMOJSON);
                            string totalAmt = UserOrderMOList.Sum(y => y.amount).ToString("N2", new CultureInfo("en-US"));
                            string totalUnits = UserOrderMOList.Sum(y => y.units).ToString("N4", new CultureInfo("en-US"));
                            decimal totalAmtEst = 0;
                            decimal totalUnitsEst = 0;

                            string ordertype = "";
                            if (x.order_type == (int)OrderType.Buy)
                                ordertype = "BUY";
                            else if (x.order_type == (int)OrderType.Sell)
                                ordertype = "SELL";
                            else if (x.order_type == (int)OrderType.SwitchIn || x.order_type == (int)OrderType.SwitchOut)
                                ordertype = "SWITCH";
                            else if (x.order_type == (int)OrderType.RSP)
                                ordertype = "RSP";

                            StringBuilder mos = new StringBuilder();
                            string actionsHTML = "-";

                            if (x.order_status == 1 && !IsImpersonated)
                            {
                                actionsHTML = "";
                                actionsHTML += "<div class='btn-group'>";
                                actionsHTML += "<a class='btn btn-info btn-sm mb-5' href='Show-Cart.aspx?UON=" + x.order_no + "'>" + (x.order_type == 1 ? "Pay" : (x.order_type == 2 || x.order_type == 3 ? "Request" : (x.order_type == 6 ? "Verify" : "-"))) + "</a>";
                                actionsHTML += "<a class='btn btn-default btn-sm cancel-order mb-5' data-orderno='" + x.order_no + "' href='javascript:;'>Cancel</a>";
                                actionsHTML += "</div>";
                            }
                            string commonActionRow = "<td rowspan='" + (x.order_type == 1 || x.order_type == 2 ? UserOrderMOList.Count : (UserOrderMOList.Count * 2)) + @"' class='text-center'>" + actionsHTML + @"</td>";
                            Decimal totalUnitsValueMYR = 0;
                            string totalUnitsInValue = "";

                            //int idx = 0;
                            string status = "";
                            int indexsub = 0;
                            UserOrderMOList.ForEach(y =>
                            {
                                string transno = y.trans_no == "" ? "-" : y.trans_no;
                                OrderListDTO uo = y;

                                if (uo.order_status == 1)
                                {
                                    status = "Pending";// Order Placed but not yet proceed to FPX/ not yet verified by user
                                }
                                else if (uo.order_status == 2 && (uo.order_type == (int)OrderType.RSP))
                                {
                                    status = "Processing";//Pending: FPX verified
                                }
                                else if (uo.order_status == 2 && (uo.order_type == (int)OrderType.Sell || uo.order_type == (int)OrderType.SwitchIn || uo.order_type == (int)OrderType.SwitchOut))
                                    status = "Processing";// Pending for order approval
                                else if (uo.order_status == 18)
                                    status = "Cancelled";
                                else if (uo.order_status == 22 && uo.payment_method == "1")
                                    status = "Processing";//Pending: for FPX Payment
                                else if (uo.order_status == 22 && uo.payment_method == "2")
                                    status = "Processing";//Pending: For payment approval
                                else if (uo.order_status == 29 && uo.payment_method == "1")
                                    status = "Failed";
                                else if (uo.order_status == 29 && uo.payment_method == "2")
                                    status = "Failed";
                                else if (uo.order_status == 3)
                                    status = "Completed";
                                else if (uo.order_status == 39)
                                    status = "Failed";
                                else if (uo.order_status == 2)
                                    status = "Processing";//pending: Payment success

                                string url = "";
                                Response responseUOFList = IUserOrderFileService.GetDataByPropertyName(nameof(UserOrderFile.OrderNo), uo.order_no, true, 0, 0, true);
                                if (responseUOFList.IsSuccess)
                                {
                                    UserOrderFile uof = ((List<UserOrderFile>)responseUOFList.Data).FirstOrDefault();
                                    if (uof != null)
                                        url = uof.Url;
                                    else
                                        url = "FPX PAYMENT";
                                }

                                commonActionRow = "<td rowspan='" + (UserOrderMOList.Count) + @"' class='text-center'>" + actionsHTML + @"</td>";
                                mos.Append(@"<tr>
                                            <td>" + y.Fund_Name.Capitalize() + @"</td>
                                            <td>" + (x.settlement_date == default(DateTime) ? "-" : x.settlement_date.ToString("dd/MM/yyyy HH:mm:ss")) + @"</td>
                                            <td>" + (y.order_status == 3 ? y.reject_reason : "-") + @"</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class='text-right'>" + y.amount.ToString("N2", new CultureInfo("en-US")) + @"</td>
                                            " + (y.order_status == 1 ? (indexsub == 0 ? commonActionRow : "") : "") + @"
                                        </tr>");
                                indexsub++;
                            });

                            tableBuilder.Append(@"<tr>
                                                <td><a href='javascript:;' class='show-extended-row'><i class='fa fa-plus'></i></a></td>
                                                <td>" + index + @"</td>
                                                <td>" + x.account_no + @"</td>
                                                <td>" + x.order_no + @"</td>
                                                <td>" + x.created_date + @"</td>
                                                <td>" + (x.fpx_bank_code == "0" || x.fpx_bank_code == null || x.fpx_bank_code == "" ? "-" : x.fpx_bank_code) + @"</td>
                                                <td>" + (x.fpx_transaction_id == "0" || x.fpx_transaction_id == null || x.fpx_transaction_id == "" ? "-" : x.fpx_transaction_id) + @"</td>
                                                <td>" + status + @"</td>
                                                <td class='text-right'>" + (ordertype == "BUY" || ordertype == "RSP" ? totalAmt : (ordertype == "SELL" || ordertype == "SWITCH" ? totalUnits : "")) + @"</td>
                                            </tr>");

                            tableBuilder.Append(@"<tr class='hide'>
                                        <td colspan='9' style='padding: 0 !important;'>
                                            <div class='extended-row' style='display:none;'>
                                                <table class='table' style='margin-bottom: 0px;'>
                                                    <thead> 
                                                        <tr>
                                                            <th>Fund Name</th>
                                                            <th>Transaction date</th>
                                                            <th>e-Mandate Reference no</th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th class='text-right'>Monthly Subscription Amount (MYR)</th>
                                                             "+(x.order_status == 1 ? "<th width = '150' class='text-center'>Action</th>" : "") + @"
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        " + mos.ToString() + @"
                                                        <tr>
                                                            
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td class='text-right'>Total (MYR)</td>
                                                            <td class='text-right'>" + totalAmt + @"</td>
                                                         "+(x.order_status == 1 ?" <td></td>":"")+ @" 
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                        <td class='hide'></td>
                                    </tr>");
                            index++;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseMO.Message + "\", '');", true);
                        }
                    });
                    tbodyUserOrders.InnerHtml = tableBuilder.ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Alert', \"" + responseUserOrderList.Message + "\", '');", true);
                }
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindOrderList();
        }
        protected void CancelOrder_Click(object sender, EventArgs e)
        {
            User user = (User)Session["user"];
            string orderNo = hdnCancelOrderNo.Value;
            Response responseUOList = IUserOrderService.GetDataByFilter(" order_no = '" + orderNo + "' and user_id=" + user.Id + " ", 0, 0, false);
            if (responseUOList.IsSuccess)
            {
                List<UserOrder> userOrders = (List<UserOrder>)responseUOList.Data;
                List<UserOrder> multipleOrders = new List<UserOrder>();

                userOrders.ForEach(x =>
                {
                    Response response = IUserOrderService.GetDataByPropertyName(nameof(UserOrder.OrderNo), x.OrderNo, true, 0, 0, false);
                    if (response.IsSuccess)
                    {
                        List<UserOrder> ol = (List<UserOrder>)response.Data;
                        if (ol.Count > 0)
                        {
                            multipleOrders.AddRange(ol);
                        }
                    }
                });

                multipleOrders.ForEach(x =>
                {
                    x.OrderStatus = 18;
                });
                IUserOrderService.UpdateBulkData(multipleOrders);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "failure", "ShowCustomMessage('Info', 'Order no: " + orderNo + " cancelled.', 'RSPList.aspx');", true);
            }
            // terminated RSP fund

        }
        public static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType("DiOTP.Utility.CustomClasses." + typeName);
                if (type != null)
                    return type;
            }
            return null;
        }
    }
}