﻿using DiOTP.OracleData;
using DiOTP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DiOTP.WebApi.Controllers
{
    public class UtmcCompositionalTransactionApiController : ApiController
    {
        // GET api/<controller>/5
        public List<UtmcCompositionalTransaction> Get(string holderNo)
        {
            Console.WriteLine("API Holder: " + holderNo);
            return UtmcCompositionalTransactionLiveData.GetDataByHolderNo(holderNo);
        }

        public List<UtmcCompositionalTransaction> Get(string holderNo, string startDate, string endDate)
        {
            Console.WriteLine("API Holder: " + holderNo);
            return UtmcCompositionalTransactionLiveData.GetDataByHolderNo(holderNo, startDate, endDate);
        }
    }
}
