﻿using DiOTP.OracleData;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.OracleDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DiOTP.WebApi.Controllers
{
    public class AddInfoApiController : ApiController
    {
        // GET api/<controller>
        public Response Get(string holderNo)
        {
            Response response = new Response();
            response = NewRepoClass.GetDataByFilter<AddInfo>(" info_number='" + holderNo + "' ", 0, 0, false, true, null, false);
            //List<HolderInv> holderInvs = (List<HolderInv>)response.Data;
            return response;
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
