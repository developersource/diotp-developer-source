﻿using DiOTP.Utility;
using DiOTP.OracleData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.WebApi.Controllers
{
    public class HolderLedgerApiController : ApiController
    {

        //GET api/<controller>
        public Response Get(string filter)
        {
            Response response = new Response();
            if (filter == "check")
            {
                response.IsSuccess = true;
                response.IsApiAvailable = true;
                Response responseQuery = GenericRepo.CheckConnection();
                response.IsDBAvailable = responseQuery.IsDBAvailable;
                response.IsSuccess = responseQuery.IsSuccess;
                return response;
            }
            else
            {
                //response = GenericRepo.PullData<HolderLedger>(filter, 0, 0, false, null);
                //string query = "SELECT * FROM (select * from uts.holder_ledger where "+ filter + ") suppliers2 WHERE rownum <= 3 ORDER BY rownum DESC";
                response = GenericRepo.GetDataByQuery(filter, 0, 0, false, null, false, null, false);
            }
            return response;
        }
    }
}
