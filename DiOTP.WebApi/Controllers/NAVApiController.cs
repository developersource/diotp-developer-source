﻿using DiOTP.OracleData;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using DiOTP.Utility.OracleDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DiOTP.WebApi.Controllers
{
    public class NAVApiController : ApiController
    {
        // GET api/<controller>
        public Response Get(string fundIdsString)
        {
            Response response = new Response();
            string query = "Select Emp.FUND_ID, Emp.TRANS_DT, Emp.SEQ_NO, Emp.PR_STATUS, Emp.UP_SELL, Emp.UNIT_IN_ISSUE, (Emp.UP_SELL * Emp.UNIT_IN_ISSUE) as UNIT_IN_ISSUE_PRICE From UTS.PRICE_HISTORY Emp INNER Join (Select FUND_ID, MAX(TRANS_DT) As latest_effective_date From UTS.PRICE_HISTORY Where FUND_ID In (" + fundIdsString + ") And PR_STATUS = 'C' Group By FUND_ID ) MaxEmp On Emp.FUND_ID = MaxEmp.FUND_ID And Emp.TRANS_DT = MaxEmp.latest_effective_date Where Emp.PR_STATUS = 'C' and rownum > 0 Order By Emp.FUND_ID";
            response = NewRepoClass.GetDataByQuery(query, 0, 0, false, null, false, null, true);
            //List<HolderInv> holderInvs = (List<HolderInv>)response.Data;
            return response;
        }
        public Response GetNAVByFundAndDate(string fundIdsString, string dateString)
        {
            Response response = new Response();
            string query = "Select Emp.FUND_ID, Emp.TRANS_DT, Emp.SEQ_NO, Emp.PR_STATUS, Emp.UP_SELL From UTS.PRICE_HISTORY Emp Where Emp.FUND_ID In (" + fundIdsString + ") And Emp.PR_STATUS = 'C' And Emp.TRANS_DT = to_date('" + dateString + "', 'yyyy/mm/dd') ";
            if (dateString.Contains("prev"))
            {
                dateString = dateString.Replace("prev", "");
                query = @"Select Emp.FUND_ID, Emp.TRANS_DT, Emp.SEQ_NO, Emp.PR_STATUS, Emp.UP_SELL From UTS.PRICE_HISTORY Emp Where Emp.FUND_ID In (" + fundIdsString + @") And Emp.PR_STATUS = 'C' And Emp.TRANS_DT = (Select MAX(TRANS_DT) from UTS.PRICE_HISTORY where FUND_ID In (" + fundIdsString + @") and PR_STATUS = 'C' and TRANS_DT < to_date('" + dateString + @"', 'yyyy/mm/dd')) ";
            }
            response = NewRepoClass.GetDataByQuery(query, 0, 0, false, null, false, null, true);
            Logger.WriteLog("GetNAVByFundAndDate response.IsSuccess: " + response.IsSuccess);
            return response;
        }

        public Response GetNAVHistory(string fundIdsString, int noOfMonths)
        {
            Response response = new Response();
            DateTime threeMonthsBack = DateTime.Now.AddMonths(-noOfMonths);
            string query = "Select Emp.FUND_ID, Emp.TRANS_DT, Emp.SEQ_NO, Emp.PR_STATUS, Emp.UP_SELL From UTS.PRICE_HISTORY Emp Where Emp.FUND_ID In (" + fundIdsString + ") And Emp.PR_STATUS = 'C' And Emp.TRANS_DT >= to_date('" + threeMonthsBack.ToString("yyyy-MM-dd") + "', 'yyyy/mm/dd') ";
            if (noOfMonths == 0)
            {
                query = @"SELECT A.FUND_ID, A.TRANS_DT, A.UP_SELL, A.UNIT_IN_ISSUE, A.UP_SELL * A.UNIT_IN_ISSUE a
                            FROM UTS.PRICE_HISTORY A INNER JOIN 
                            (SELECT FUND_ID, TRANS_DT, MAX(SEQ_NO)AS MaxRecord FROM UTS.PRICE_HISTORY GROUP BY FUND_ID, TRANS_DT) B 
                            ON A.FUND_ID = B.FUND_ID AND A.TRANS_DT = B.TRANS_DT AND A.SEQ_NO = B.MaxRecord 
                            WHERE A.FUND_ID IN (" + fundIdsString + @") AND A.PR_STATUS = 'C' 
                            ORDER BY A.FUND_ID, A.TRANS_DT ASC";
            }
            response = NewRepoClass.GetDataByQuery(query, 0, 0, false, null, false, null, true);
            return response;
        }

    }
}
