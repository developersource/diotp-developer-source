using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class UtmcFundDetailRepo : IUtmcFundDetailRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(UtmcFundDetail obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcFundDetail>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UtmcFundDetail> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcFundDetail>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UtmcFundDetail obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcFundDetail>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UtmcFundDetail> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcFundDetail>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UtmcFundDetail DeleteData(Int32 Id)
        {
            Response responseUFD = GetSingle(Id);
            if (responseUFD.IsSuccess)
            {
                UtmcFundDetail obj = (UtmcFundDetail)responseUFD.Data;
                try
                {
                    string query = obj.ObjectToQuery<UtmcFundDetail>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                    Response responseUFD = GetSingle(Id);
                    if (responseUFD.IsSuccess)
                    {
                        UtmcFundDetail obj = (UtmcFundDetail)responseUFD.Data;
                        query += obj.ObjectToQuery<UtmcFundDetail>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UtmcFundDetail obj = new UtmcFundDetail();
            try
            {
                string query = "select * from utmc_fund_details where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new UtmcFundDetail
                       {
                           Id = x.Field<Int32>("id"),
                           UtmcFundInformationId = x.Field<Int32>("utmc_fund_information_id"),
                           LaunchDate = x.Field<DateTime>("launch_date"),
                           RelaunchDate = x.Field<DateTime?>("relaunch_date"),
                           LaunchPrice = x.Field<Decimal>("launch_price"),
                           PricingBasis = x.Field<String>("pricing_basis"),
                           InvestmentObjective = x.Field<String>("investment_objective"),
                           InvestmentStrategyAndPolicy = x.Field<String>("investment_strategy_and_policy"),
                           LatestNavPrice = x.Field<Decimal>("latest_nav_price"),
                           LatestNavDate = x.Field<DateTime?>("latest_nav_date"),
                           HistoricalIncomeDistribution = x.Field<Int32>("historical_income_distribution"),
                           IsEpfApproved = x.Field<Int32>("is_epf_approved"),
                           ShariahCompliant = x.Field<Int32>("shariah_compliant"),
                           RiskRating = x.Field<String>("risk_rating"),
                           FundSizeRm = x.Field<String>("fund_size_rm"),
                           MinInitialInvestmentCash = x.Field<Int32>("min_initial_investment_cash"),
                           MinInitialInvestmentEpf = x.Field<Int32>("min_initial_investment_epf"),
                           MinSubsequentInvestmentCash = x.Field<Int32>("min_subsequent_investment_cash"),
                           MinSubsequentInvestmentEpf = x.Field<Int32>("min_subsequent_investment_epf"),
                           MinRspInvestmentInitialRm = x.Field<Int32>("min_rsp_investment_initial_rm"),
                           MinRspInvestmentAdditionalRm = x.Field<Int32>("min_rsp_investment_additional_rm"),
                           MinRedAmountUnits = x.Field<Int32>("min_red_amount_units"),
                           MinHoldingUnits = x.Field<Int32>("min_holding_units"),
                           CollingOffPeriod = x.Field<String>("colling_off_period"),
                           DistributionPolicy = x.Field<String>("distribution_policy"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundDetail> objs = new List<UtmcFundDetail>();
            try
            {
                string query = "select * from utmc_fund_details";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcFundDetail
                        {
                            Id = x.Field<Int32>("id"),
                            UtmcFundInformationId = x.Field<Int32>("utmc_fund_information_id"),
                            LaunchDate = x.Field<DateTime>("launch_date"),
                            RelaunchDate = x.Field<DateTime?>("relaunch_date"),
                            LaunchPrice = x.Field<Decimal>("launch_price"),
                            PricingBasis = x.Field<String>("pricing_basis"),
                            InvestmentObjective = x.Field<String>("investment_objective"),
                            InvestmentStrategyAndPolicy = x.Field<String>("investment_strategy_and_policy"),
                            LatestNavPrice = x.Field<Decimal>("latest_nav_price"),
                           LatestNavDate = x.Field<DateTime?>("latest_nav_date"),
                            HistoricalIncomeDistribution = x.Field<Int32>("historical_income_distribution"),
                            IsEpfApproved = x.Field<Int32>("is_epf_approved"),
                            ShariahCompliant = x.Field<Int32>("shariah_compliant"),
                            RiskRating = x.Field<String>("risk_rating"),
                            FundSizeRm = x.Field<String>("fund_size_rm"),
                            MinInitialInvestmentCash = x.Field<Int32>("min_initial_investment_cash"),
                            MinInitialInvestmentEpf = x.Field<Int32>("min_initial_investment_epf"),
                            MinSubsequentInvestmentCash = x.Field<Int32>("min_subsequent_investment_cash"),
                            MinSubsequentInvestmentEpf = x.Field<Int32>("min_subsequent_investment_epf"),
                            MinRspInvestmentInitialRm = x.Field<Int32>("min_rsp_investment_initial_rm"),
                            MinRspInvestmentAdditionalRm = x.Field<Int32>("min_rsp_investment_additional_rm"),
                            MinRedAmountUnits = x.Field<Int32>("min_red_amount_units"),
                            MinHoldingUnits = x.Field<Int32>("min_holding_units"),
                            CollingOffPeriod = x.Field<String>("colling_off_period"),
                            DistributionPolicy = x.Field<String>("distribution_policy"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_fund_details";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<UtmcFundDetail>(propertyName);
            List<UtmcFundDetail> objs = new List<UtmcFundDetail>();
            try
            {
                string query = "select * from utmc_fund_details where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from utmc_fund_details where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcFundDetail
                        {
                            Id = x.Field<Int32>("id"),
                            UtmcFundInformationId = x.Field<Int32>("utmc_fund_information_id"),
                            LaunchDate = x.Field<DateTime>("launch_date"),
                            RelaunchDate = x.Field<DateTime?>("relaunch_date"),
                            LaunchPrice = x.Field<Decimal>("launch_price"),
                            PricingBasis = x.Field<String>("pricing_basis"),
                            InvestmentObjective = x.Field<String>("investment_objective"),
                            InvestmentStrategyAndPolicy = x.Field<String>("investment_strategy_and_policy"),
                            LatestNavPrice = x.Field<Decimal>("latest_nav_price"),
                           LatestNavDate = x.Field<DateTime?>("latest_nav_date"),
                            HistoricalIncomeDistribution = x.Field<Int32>("historical_income_distribution"),
                            IsEpfApproved = x.Field<Int32>("is_epf_approved"),
                            ShariahCompliant = x.Field<Int32>("shariah_compliant"),
                            RiskRating = x.Field<String>("risk_rating"),
                            FundSizeRm = x.Field<String>("fund_size_rm"),
                            MinInitialInvestmentCash = x.Field<Int32>("min_initial_investment_cash"),
                            MinInitialInvestmentEpf = x.Field<Int32>("min_initial_investment_epf"),
                            MinSubsequentInvestmentCash = x.Field<Int32>("min_subsequent_investment_cash"),
                            MinSubsequentInvestmentEpf = x.Field<Int32>("min_subsequent_investment_epf"),
                            MinRspInvestmentInitialRm = x.Field<Int32>("min_rsp_investment_initial_rm"),
                            MinRspInvestmentAdditionalRm = x.Field<Int32>("min_rsp_investment_additional_rm"),
                            MinRedAmountUnits = x.Field<Int32>("min_red_amount_units"),
                            MinHoldingUnits = x.Field<Int32>("min_holding_units"),
                            CollingOffPeriod = x.Field<String>("colling_off_period"),
                            DistributionPolicy = x.Field<String>("distribution_policy"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<UtmcFundDetail>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_fund_details where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from utmc_fund_details where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundDetail> objs = new List<UtmcFundDetail>();
            try
            {
                string query = "select * from utmc_fund_details where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcFundDetail
                        {
                            Id = x.Field<Int32>("id"),
                            UtmcFundInformationId = x.Field<Int32>("utmc_fund_information_id"),
                            LaunchDate = x.Field<DateTime>("launch_date"),
                            RelaunchDate = x.Field<DateTime?>("relaunch_date"),
                            LaunchPrice = x.Field<Decimal>("launch_price"),
                            PricingBasis = x.Field<String>("pricing_basis"),
                            InvestmentObjective = x.Field<String>("investment_objective"),
                            InvestmentStrategyAndPolicy = x.Field<String>("investment_strategy_and_policy"),
                            LatestNavPrice = x.Field<Decimal>("latest_nav_price"),
                           LatestNavDate = x.Field<DateTime?>("latest_nav_date"),
                            HistoricalIncomeDistribution = x.Field<Int32>("historical_income_distribution"),
                            IsEpfApproved = x.Field<Int32>("is_epf_approved"),
                            ShariahCompliant = x.Field<Int32>("shariah_compliant"),
                            RiskRating = x.Field<String>("risk_rating"),
                            FundSizeRm = x.Field<String>("fund_size_rm"),
                            MinInitialInvestmentCash = x.Field<Int32>("min_initial_investment_cash"),
                            MinInitialInvestmentEpf = x.Field<Int32>("min_initial_investment_epf"),
                            MinSubsequentInvestmentCash = x.Field<Int32>("min_subsequent_investment_cash"),
                            MinSubsequentInvestmentEpf = x.Field<Int32>("min_subsequent_investment_epf"),
                            MinRspInvestmentInitialRm = x.Field<Int32>("min_rsp_investment_initial_rm"),
                            MinRspInvestmentAdditionalRm = x.Field<Int32>("min_rsp_investment_additional_rm"),
                            MinRedAmountUnits = x.Field<Int32>("min_red_amount_units"),
                            MinHoldingUnits = x.Field<Int32>("min_holding_units"),
                            CollingOffPeriod = x.Field<String>("colling_off_period"),
                            DistributionPolicy = x.Field<String>("distribution_policy"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_fund_details where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
