﻿using DiOTP.Data.IRepo;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Data
{
    public class AccountOpeningRepo : IAccountOpeningRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(AccountOpening obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<AccountOpening>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                obj.Id = Convert.ToInt32(cmd.LastInsertedId);
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<AccountOpening> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<AccountOpening>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(AccountOpening obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<AccountOpening>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<AccountOpening> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<AccountOpening>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public AccountOpening DeleteData(Int32 Id)
        {
            Response responseULM = GetSingle(Id);
            if (responseULM.IsSuccess)
            {
                AccountOpening obj = (AccountOpening)responseULM.Data;
                try
                {
                    string query = obj.ObjectToQuery<AccountOpening>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                    Response responseULM = GetSingle(Id);
                    if (responseULM.IsSuccess)
                    {
                        AccountOpening obj = (AccountOpening)responseULM.Data;
                        query += obj.ObjectToQuery<AccountOpening>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            AccountOpening obj = new AccountOpening();
            try
            {
                string query = "select * from account_openings where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new AccountOpening
                       {
                           Id = x.Field<Int32>("id"),
                           AccountType = x.Field<Int32>("account_type"),
                           PrincipleId = x.Field<String>("principle_id"),
                           PrincipleUserId = x.Field<Int32>("principle_user_id"),
                           JointRelationship = x.Field<String>("joint_relationship"),
                           UsCitizen = x.Field<Int32>("us_citizen"),
                           IdNo = x.Field<String>("id_no"),
                           MobileNo = x.Field<String>("mobile_no"),
                           IsMobileVerified = x.Field<Int32>("is_mobile_verified"),
                           IsOtpRequested = x.Field<Int32>("is_otp_requested"),
                           OtpAttempts = x.Field<Int32>("otp_attempts"),
                           OtpCode = x.Field<Int32>("otp_code"),
                           IsOtpExpired = x.Field<Int32>("is_otp_expired"),
                           Email = x.Field<String>("email"),
                           Password = x.Field<String>("password"),
                           EmailVerificationCode = x.Field<Int32>("email_verification_code"),
                           IsEmailRequested = x.Field<Int32>("is_email_requested"),
                           IsEmailVerified = x.Field<Int32>("is_email_verified"),
                           ResidentOfMalaysia = x.Field<Int32>("resident_of_malaysia"),
                           Nationality = x.Field<String>("nationality"),
                           Name = x.Field<String>("name"),
                           Dob = x.Field<String>("dob"),
                           Race = x.Field<String>("race"),
                           RaceDescription = x.Field<String>("race_description"),
                           BumiputraStatus = x.Field<String>("bumiputra_status"),
                           MaritalStatus = x.Field<String>("marital_status"),
                           Gender = x.Field<String>("gender"),
                           Salutation = x.Field<String>("salutation"),
                           AreYouPep = x.Field<Int32>("are_you_pep"),
                           PepStatus = x.Field<String>("pep_status"),
                           FPepStatus = x.Field<String>("f_pep_status"),
                           TaxResidentOutsideMalaysia = x.Field<Int32>("tax_resident_outside_malaysia"),
                           TaxResidencyStatus = x.Field<String>("tax_residency_status"),
                           Tin = x.Field<String>("tin"),
                           Declaration = x.Field<Int32>("declaration"),
                           StartedDate = x.Field<DateTime>("started_date"),
                           LastUpdatedDate = x.Field<DateTime?>("last_updated_date"),
                           SubmittedDate = x.Field<DateTime?>("submitted_date"),
                           IsAdditionalRequired = x.Field<Int32>("is_additional_required"),
                           IsAdditionalRequired2 = x.Field<Int32>("is_additional_required_2"),
                           SettlementDate = x.Field<DateTime?>("settlement_date"),
                           ProcessStatus = x.Field<Int32>("process_status"),
                           Remarks = x.Field<String>("remarks"),
                           IsIdentityVerified = x.Field<Int32>("is_identity_verified"),
                           IsDocumentsVerified = x.Field<Int32>("is_documents_verified"),
                           IdentityStatus = x.Field<String>("identity_status"),
                           DocumentsStatus = x.Field<String>("documents_status"),
                           IsAdditionalAdmin = x.Field<Int32>("is_additional_admin"),
                           AdditionalDesc = x.Field<String>("additional_desc"),
                           Status = x.Field<Int32>("status"),
                           LastBtnClicked = x.Field<String>("last_btn_clicked"),
                           RemaindTimes = x.Field<Int32>("remaind_times"),
                           RegisterIPAddress = x.Field<String>("register_ip_address"),
                           AgentCode = x.Field<String>("agent_code"),
                           EmailCodeSentDate = x.Field<DateTime?>("email_code_sent_date"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<AccountOpening> objs = new List<AccountOpening>();
            try
            {
                string query = "select * from account_openings";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new AccountOpening
                        {
                            Id = x.Field<Int32>("id"),
                            AccountType = x.Field<Int32>("account_type"),
                            PrincipleId = x.Field<String>("principle_id"),
                            PrincipleUserId = x.Field<Int32>("principle_user_id"),
                            JointRelationship = x.Field<String>("joint_relationship"),
                            UsCitizen = x.Field<Int32>("us_citizen"),
                            IdNo = x.Field<String>("id_no"),
                            MobileNo = x.Field<String>("mobile_no"),
                            IsMobileVerified = x.Field<Int32>("is_mobile_verified"),
                            IsOtpRequested = x.Field<Int32>("is_otp_requested"),
                            OtpAttempts = x.Field<Int32>("otp_attempts"),
                            OtpCode = x.Field<Int32>("otp_code"),
                            IsOtpExpired = x.Field<Int32>("is_otp_expired"),
                            Email = x.Field<String>("email"),
                            Password = x.Field<String>("password"),
                            EmailVerificationCode = x.Field<Int32>("email_verification_code"),
                            IsEmailRequested = x.Field<Int32>("is_email_requested"),
                            IsEmailVerified = x.Field<Int32>("is_email_verified"),
                            ResidentOfMalaysia = x.Field<Int32>("resident_of_malaysia"),
                            Nationality = x.Field<String>("nationality"),
                            Name = x.Field<String>("name"),
                            Dob = x.Field<String>("dob"),
                            Race = x.Field<String>("race"),
                            RaceDescription = x.Field<String>("race_description"),
                            BumiputraStatus = x.Field<String>("bumiputra_status"),
                            MaritalStatus = x.Field<String>("marital_status"),
                            Gender = x.Field<String>("gender"),
                            Salutation = x.Field<String>("salutation"),
                            AreYouPep = x.Field<Int32>("are_you_pep"),
                            PepStatus = x.Field<String>("pep_status"),
                            FPepStatus = x.Field<String>("f_pep_status"),
                            TaxResidentOutsideMalaysia = x.Field<Int32>("tax_resident_outside_malaysia"),
                            TaxResidencyStatus = x.Field<String>("tax_residency_status"),
                            Tin = x.Field<String>("tin"),
                            Declaration = x.Field<Int32>("declaration"),
                            StartedDate = x.Field<DateTime>("started_date"),
                            LastUpdatedDate = x.Field<DateTime?>("last_updated_date"),
                            SubmittedDate = x.Field<DateTime?>("submitted_date"),
                            IsAdditionalRequired = x.Field<Int32>("is_additional_required"),
                            IsAdditionalRequired2 = x.Field<Int32>("is_additional_required_2"),
                            SettlementDate = x.Field<DateTime?>("settlement_date"),
                            ProcessStatus = x.Field<Int32>("process_status"),
                            Remarks = x.Field<String>("remarks"),
                            IsIdentityVerified = x.Field<Int32>("is_identity_verified"),
                            IsDocumentsVerified = x.Field<Int32>("is_documents_verified"),
                            IdentityStatus = x.Field<String>("identity_status"),
                            DocumentsStatus = x.Field<String>("documents_status"),
                            IsAdditionalAdmin = x.Field<Int32>("is_additional_admin"),
                            AdditionalDesc = x.Field<String>("additional_desc"),
                            Status = x.Field<Int32>("status"),
                            LastBtnClicked = x.Field<String>("last_btn_clicked"),
                            RemaindTimes = x.Field<Int32>("remaind_times"),
                            RegisterIPAddress = x.Field<String>("register_ip_address"),
                            AgentCode = x.Field<String>("agent_code"),
                            EmailCodeSentDate = x.Field<DateTime?>("email_code_sent_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from account_openings";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<AccountOpening>(propertyName);
            List<AccountOpening> objs = new List<AccountOpening>();
            try
            {
                string query = "select * from account_openings where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from account_openings where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new AccountOpening
                        {
                            Id = x.Field<Int32>("id"),
                            AccountType = x.Field<Int32>("account_type"),
                            PrincipleId = x.Field<String>("principle_id"),
                            PrincipleUserId = x.Field<Int32>("principle_user_id"),
                            JointRelationship = x.Field<String>("joint_relationship"),
                            UsCitizen = x.Field<Int32>("us_citizen"),
                            IdNo = x.Field<String>("id_no"),
                            MobileNo = x.Field<String>("mobile_no"),
                            IsMobileVerified = x.Field<Int32>("is_mobile_verified"),
                            IsOtpRequested = x.Field<Int32>("is_otp_requested"),
                            OtpAttempts = x.Field<Int32>("otp_attempts"),
                            OtpCode = x.Field<Int32>("otp_code"),
                            IsOtpExpired = x.Field<Int32>("is_otp_expired"),
                            Email = x.Field<String>("email"),
                            Password = x.Field<String>("password"),
                            EmailVerificationCode = x.Field<Int32>("email_verification_code"),
                            IsEmailRequested = x.Field<Int32>("is_email_requested"),
                            IsEmailVerified = x.Field<Int32>("is_email_verified"),
                            ResidentOfMalaysia = x.Field<Int32>("resident_of_malaysia"),
                            Nationality = x.Field<String>("nationality"),
                            Name = x.Field<String>("name"),
                            Dob = x.Field<String>("dob"),
                            Race = x.Field<String>("race"),
                            RaceDescription = x.Field<String>("race_description"),
                            BumiputraStatus = x.Field<String>("bumiputra_status"),
                            MaritalStatus = x.Field<String>("marital_status"),
                            Gender = x.Field<String>("gender"),
                            Salutation = x.Field<String>("salutation"),
                            AreYouPep = x.Field<Int32>("are_you_pep"),
                            PepStatus = x.Field<String>("pep_status"),
                            FPepStatus = x.Field<String>("f_pep_status"),
                            TaxResidentOutsideMalaysia = x.Field<Int32>("tax_resident_outside_malaysia"),
                            TaxResidencyStatus = x.Field<String>("tax_residency_status"),
                            Tin = x.Field<String>("tin"),
                            Declaration = x.Field<Int32>("declaration"),
                            StartedDate = x.Field<DateTime>("started_date"),
                            LastUpdatedDate = x.Field<DateTime?>("last_updated_date"),
                            SubmittedDate = x.Field<DateTime?>("submitted_date"),
                            IsAdditionalRequired = x.Field<Int32>("is_additional_required"),
                            IsAdditionalRequired2 = x.Field<Int32>("is_additional_required_2"),
                            SettlementDate = x.Field<DateTime?>("settlement_date"),
                            ProcessStatus = x.Field<Int32>("process_status"),
                            Remarks = x.Field<String>("remarks"),
                            IsIdentityVerified = x.Field<Int32>("is_identity_verified"),
                            IsDocumentsVerified = x.Field<Int32>("is_documents_verified"),
                            IdentityStatus = x.Field<String>("identity_status"),
                            DocumentsStatus = x.Field<String>("documents_status"),
                            IsAdditionalAdmin = x.Field<Int32>("is_additional_admin"),
                            AdditionalDesc = x.Field<String>("additional_desc"),
                            Status = x.Field<Int32>("status"),
                            LastBtnClicked = x.Field<String>("last_btn_clicked"),
                            RemaindTimes = x.Field<Int32>("remaind_times"),
                            RegisterIPAddress = x.Field<String>("register_ip_address"),
                            AgentCode = x.Field<String>("agent_code"),
                            EmailCodeSentDate = x.Field<DateTime?>("email_code_sent_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<AccountOpening>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from account_openings where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from account_openings where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<AccountOpening> objs = new List<AccountOpening>();
            try
            {
                string query = "select * from account_openings where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new AccountOpening
                        {
                            Id = x.Field<Int32>("id"),
                            AccountType = x.Field<Int32>("account_type"),
                            PrincipleId = x.Field<String>("principle_id"),
                            PrincipleUserId = x.Field<Int32>("principle_user_id"),
                            JointRelationship = x.Field<String>("joint_relationship"),
                            UsCitizen = x.Field<Int32>("us_citizen"),
                            IdNo = x.Field<String>("id_no"),
                            MobileNo = x.Field<String>("mobile_no"),
                            IsMobileVerified = x.Field<Int32>("is_mobile_verified"),
                            IsOtpRequested = x.Field<Int32>("is_otp_requested"),
                            OtpAttempts = x.Field<Int32>("otp_attempts"),
                            OtpCode = x.Field<Int32>("otp_code"),
                            IsOtpExpired = x.Field<Int32>("is_otp_expired"),
                            Email = x.Field<String>("email"),
                            Password = x.Field<String>("password"),
                            EmailVerificationCode = x.Field<Int32>("email_verification_code"),
                            IsEmailRequested = x.Field<Int32>("is_email_requested"),
                            IsEmailVerified = x.Field<Int32>("is_email_verified"),
                            ResidentOfMalaysia = x.Field<Int32>("resident_of_malaysia"),
                            Nationality = x.Field<String>("nationality"),
                            Name = x.Field<String>("name"),
                            Dob = x.Field<String>("dob"),
                            Race = x.Field<String>("race"),
                            RaceDescription = x.Field<String>("race_description"),
                            BumiputraStatus = x.Field<String>("bumiputra_status"),
                            MaritalStatus = x.Field<String>("marital_status"),
                            Gender = x.Field<String>("gender"),
                            Salutation = x.Field<String>("salutation"),
                            AreYouPep = x.Field<Int32>("are_you_pep"),
                            PepStatus = x.Field<String>("pep_status"),
                            FPepStatus = x.Field<String>("f_pep_status"),
                            TaxResidentOutsideMalaysia = x.Field<Int32>("tax_resident_outside_malaysia"),
                            TaxResidencyStatus = x.Field<String>("tax_residency_status"),
                            Tin = x.Field<String>("tin"),
                            Declaration = x.Field<Int32>("declaration"),
                            StartedDate = x.Field<DateTime>("started_date"),
                            LastUpdatedDate = x.Field<DateTime?>("last_updated_date"),
                            SubmittedDate = x.Field<DateTime?>("submitted_date"),
                            IsAdditionalRequired = x.Field<Int32>("is_additional_required"),
                            IsAdditionalRequired2 = x.Field<Int32>("is_additional_required_2"),
                            SettlementDate = x.Field<DateTime?>("settlement_date"),
                            ProcessStatus = x.Field<Int32>("process_status"),
                            Remarks = x.Field<String>("remarks"),
                            IsIdentityVerified = x.Field<Int32>("is_identity_verified"),
                            IsDocumentsVerified = x.Field<Int32>("is_documents_verified"),
                            IdentityStatus = x.Field<String>("identity_status"),
                            DocumentsStatus = x.Field<String>("documents_status"),
                            IsAdditionalAdmin = x.Field<Int32>("is_additional_admin"),
                            AdditionalDesc = x.Field<String>("additional_desc"),
                            Status = x.Field<Int32>("status"),
                            LastBtnClicked = x.Field<String>("last_btn_clicked"),
                            RemaindTimes = x.Field<Int32>("remaind_times"),
                            RegisterIPAddress = x.Field<String>("register_ip_address"),
                            AgentCode = x.Field<String>("agent_code"),
                            EmailCodeSentDate = x.Field<DateTime?>("email_code_sent_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from account_openings where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}

