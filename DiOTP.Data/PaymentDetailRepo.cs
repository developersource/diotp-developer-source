﻿using DiOTP.Data.IRepo;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Data
{
    public class PaymentDetailRepo : IPaymentDetailRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public int DeleteBulkData(List<int> Ids)
        {
            throw new NotImplementedException();
        }

        public PaymentDetail DeleteData(int Id)
        {
            throw new NotImplementedException();
        }

        public int GetCount()
        {
            throw new NotImplementedException();
        }

        public int GetCountByFilter(string filter)
        {
            throw new NotImplementedException();
        }

        public int GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            throw new NotImplementedException();
        }

        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            throw new NotImplementedException();
        }

        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            throw new NotImplementedException();
        }

        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            throw new NotImplementedException();
        }

        public Response GetSingle(int Id)
        {
            throw new NotImplementedException();
        }

        public int PostBulkData(List<PaymentDetail> objs)
        {
            throw new NotImplementedException();
        }

        public Response PostData(PaymentDetail obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<PaymentDetail>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public int UpdateBulkData(List<PaymentDetail> objs)
        {
            throw new NotImplementedException();
        }

        public Response UpdateData(PaymentDetail obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<PaymentDetail>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
    }
}
