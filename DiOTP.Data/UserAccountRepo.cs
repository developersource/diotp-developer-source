using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class UserAccountRepo : IUserAccountRepo
    {
        //IMaHolderRegRepo IMaHolderRegRepo = new MaHolderRegRepo();

        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(UserAccount obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UserAccount>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                obj.Id = Convert.ToInt32(cmd.LastInsertedId);
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UserAccount> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UserAccount>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UserAccount obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UserAccount>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UserAccount> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UserAccount>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UserAccount DeleteData(Int32 Id)
        {
            Response responseUA = new Response();
            if (responseUA.IsSuccess)
            {
                UserAccount obj = (UserAccount)responseUA.Data;
                try
                {
                    string query = obj.ObjectToQuery<UserAccount>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseUA = new Response();
                    if (responseUA.IsSuccess)
                    {
                        UserAccount obj = (UserAccount)responseUA.Data;
                        query += obj.ObjectToQuery<UserAccount>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UserAccount obj = new UserAccount();
            try
            {
                string query = "select * from user_accounts where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new UserAccount
                       {
                           Id = x.Field<Int32>("id"),
                           UserId = x.Field<Int32>("user_id"),
                           IsPrimary = x.Field<Int32>("is_primary"),
                           MaHolderRegId = x.Field<Int32>("ma_holder_reg_id"),
                           AccountNo = x.Field<String>("account_no"),
                           IdNo = x.Field<String>("id_no"),
                           IsVerified = x.Field<Int32>("is_verified"),
                           VerificationCode = x.Field<Int32?>("verification_code") == null ? 0 : x.Field<Int32>("verification_code"),
                           VerifyExpired = x.Field<Int32>("verify_expired"),
                           IsSatChecked = x.Field<Int32>("is_sat_checked"),
                           SatUpdatedDate = x.Field<DateTime?>("sat_updated_date"),
                           SatScore = x.Field<Int32>("sat_score"),
                           IsHardcopy = x.Field<Int32>("is_hardcopy"),
                           HardcopyUpdatedDate = x.Field<DateTime?>("hardcopy_updated_date"),
                           CreatedBy = x.Field<Int32>("created_by"),
                           CreatedDate = x.Field<DateTime>("created_date"),
                           ModifiedBy = x.Field<Int32?>("modified_by") == null ? 0 : x.Field<Int32>("modified_by"),
                           ModifiedDate = x.Field<DateTime?>("modified_date"),
                           Status = x.Field<Int32>("status"),
                           HolderClass = x.Field<String>("holder_class"),
                           IsPrinciple = x.Field<Int32>("is_principle"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
                //obj.MaHolderRegIdMaHolderReg = IMaHolderRegRepo.GetSingle(obj.MaHolderRegId);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserAccount> objs = new List<UserAccount>();
            try
            {
                string query = "select * from user_accounts";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UserAccount
                        {
                            Id = x.Field<Int32>("id"),
                            UserId = x.Field<Int32>("user_id"),
                            IsPrimary = x.Field<Int32>("is_primary"),
                            MaHolderRegId = x.Field<Int32>("ma_holder_reg_id"),
                            AccountNo = x.Field<String>("account_no"),
                           IdNo = x.Field<String>("id_no"),
                            IsVerified = x.Field<Int32>("is_verified"),
                            VerificationCode = x.Field<Int32?>("verification_code") == null ? 0 : x.Field<Int32>("verification_code"),
                           VerifyExpired = x.Field<Int32>("verify_expired"),
                            IsSatChecked = x.Field<Int32>("is_sat_checked"),
                            SatUpdatedDate = x.Field<DateTime?>("sat_updated_date"),
                           SatScore = x.Field<Int32>("sat_score"),
                            IsHardcopy = x.Field<Int32>("is_hardcopy"),
                            HardcopyUpdatedDate = x.Field<DateTime?>("hardcopy_updated_date"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            ModifiedBy = x.Field<Int32?>("modified_by") == null ? 0 : x.Field<Int32>("modified_by"),
                            ModifiedDate = x.Field<DateTime?>("modified_date"),
                            Status = x.Field<Int32>("status"),
                            HolderClass = x.Field<String>("holder_class"),
                            IsPrinciple = x.Field<Int32>("is_principle"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from user_accounts";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<UserAccount>(propertyName);
            List<UserAccount> objs = new List<UserAccount>();
            try
            {
                string query = "select * from user_accounts where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from user_accounts where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UserAccount
                        {
                            Id = x.Field<Int32>("id"),
                            UserId = x.Field<Int32>("user_id"),
                            IsPrimary = x.Field<Int32>("is_primary"),
                            MaHolderRegId = x.Field<Int32>("ma_holder_reg_id"),
                            AccountNo = x.Field<String>("account_no"),
                           IdNo = x.Field<String>("id_no"),
                            IsVerified = x.Field<Int32>("is_verified"),
                            VerificationCode = x.Field<Int32?>("verification_code") == null ? 0 : x.Field<Int32>("verification_code"),
                           VerifyExpired = x.Field<Int32>("verify_expired"),
                            IsSatChecked = x.Field<Int32>("is_sat_checked"),
                            SatUpdatedDate = x.Field<DateTime?>("sat_updated_date"),
                           SatScore = x.Field<Int32>("sat_score"),
                            IsHardcopy = x.Field<Int32>("is_hardcopy"),
                            HardcopyUpdatedDate = x.Field<DateTime?>("hardcopy_updated_date"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            ModifiedBy = x.Field<Int32?>("modified_by") == null ? 0 : x.Field<Int32>("modified_by"),
                            ModifiedDate = x.Field<DateTime?>("modified_date"),
                            Status = x.Field<Int32>("status"),
                            HolderClass = x.Field<String>("holder_class"),
                            IsPrinciple = x.Field<Int32>("is_principle"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<UserAccount>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from user_accounts where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from user_accounts where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserAccount> objs = new List<UserAccount>();
            try
            {
                string query = "select * from user_accounts where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UserAccount
                        {
                            Id = x.Field<Int32>("id"),
                            UserId = x.Field<Int32>("user_id"),
                            IsPrimary = x.Field<Int32>("is_primary"),
                            MaHolderRegId = x.Field<Int32>("ma_holder_reg_id"),
                            AccountNo = x.Field<String>("account_no"),
                           IdNo = x.Field<String>("id_no"),
                            IsVerified = x.Field<Int32>("is_verified"),
                            VerificationCode = x.Field<Int32?>("verification_code") == null ? 0 : x.Field<Int32>("verification_code"),
                           VerifyExpired = x.Field<Int32>("verify_expired"),
                            IsSatChecked = x.Field<Int32>("is_sat_checked"),
                            SatUpdatedDate = x.Field<DateTime?>("sat_updated_date"),
                           SatScore = x.Field<Int32>("sat_score"),
                            IsHardcopy = x.Field<Int32>("is_hardcopy"),
                            HardcopyUpdatedDate = x.Field<DateTime?>("hardcopy_updated_date"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            ModifiedBy = x.Field<Int32?>("modified_by") == null ? 0 : x.Field<Int32>("modified_by"),
                            ModifiedDate = x.Field<DateTime?>("modified_date"),
                            Status = x.Field<Int32>("status"),
                            HolderClass = x.Field<String>("holder_class"),
                            IsPrinciple = x.Field<Int32>("is_principle"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from user_accounts where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        

        public static UserAccount GetByid(string id)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string query = @"select ID, user_id from user_accounts where account_no = @no";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@no", id);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader();
            UserAccount ua = new UserAccount();
            if (mdr.Read())
            {
                ua.Id = Convert.ToInt32(mdr["ID"].ToString());
                ua.UserId = Convert.ToInt32(mdr["user_id"].ToString());
            }
            conn.Close();
            return ua;
        }

        public static List<UserAccount> GetAll()
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string query = @"select * from user_accounts where is_verified=1 and status=1";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            conn.Open();
            MySqlDataReader mdr = cmd.ExecuteReader();
            List<UserAccount> ualist = new List<UserAccount>();

            while (mdr.Read())
            {
                UserAccount ua = new UserAccount();

                ua.Id = Convert.ToInt32(mdr["ID"].ToString());
                ua.UserId = Convert.ToInt32(mdr["user_id"].ToString());
                ua.AccountNo = mdr["account_no"].ToString();
                ualist.Add(ua);
            }
            conn.Close();
            return ualist;
        }
    }
}
