﻿using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class UtmcCompositionalInvestmentRepo : IUtmcCompositionalInvestmentRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(UtmcCompositionalInvestment obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcCompositionalInvestment>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UtmcCompositionalInvestment> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcCompositionalInvestment>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UtmcCompositionalInvestment obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcCompositionalInvestment>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UtmcCompositionalInvestment> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcCompositionalInvestment>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UtmcCompositionalInvestment DeleteData(Int32 Id)
        {
            Response responseUCI = GetSingle(Id);
            if (responseUCI.IsSuccess)
            {
                UtmcCompositionalInvestment obj = (UtmcCompositionalInvestment)responseUCI.Data;
                try
                {
                    string query = obj.ObjectToQuery<UtmcCompositionalInvestment>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseUCI = GetSingle(Id);
                    if (responseUCI.IsSuccess)
                    {
                        UtmcCompositionalInvestment obj = (UtmcCompositionalInvestment)responseUCI.Data;
                        query += obj.ObjectToQuery<UtmcCompositionalInvestment>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UtmcCompositionalInvestment obj = new UtmcCompositionalInvestment();
            try
            {
                string query = "select * from utmc_compositional_investment where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new UtmcCompositionalInvestment
                       {
                           EpfIpdCode = x.Field<String>("epf_ipd_code"),
                           IpdFundCode = x.Field<String>("ipd_fund_code"),
                           IpdMemberAccNo = x.Field<String>("ipd_member_acc_no"),
                           EffectiveDate = x.Field<DateTime?>("effective_date"),
                           OpeningBalanceUnits = x.Field<Decimal?>("opening_balance_units") == null ? 0 : x.Field<Decimal?>("opening_balance_units"),
                           OpeningBalanceCostRm = x.Field<Decimal?>("opening_balance_cost_rm") == null ? 0 : x.Field<Decimal?>("opening_balance_cost_rm"),                           
                           OpeningBalanceDate = x.Field<DateTime>("opening_balance_date"),
                           NetCumulativeClosingBalanceUnits = x.Field<Decimal?>("net_cumulative_closing_balance_units") == null ? 0 : x.Field<Decimal?>("net_cumulative_closing_balance_units"),
                           NetCumulativeClosingBalanceCostRm = x.Field<Decimal?>("net_cumulative_closing_balance_cost_rm") == null ? 0 : x.Field<Decimal?>("net_cumulative_closing_balance_cost_rm"),
                           NetCumulativeClosingBalanceDate = x.Field<DateTime?>("net_cumulative_closing_balance_date"),
                           MarketPriceNav = x.Field<Decimal?>("market_price_nav") == null ? 0 : x.Field<Decimal?>("market_price_nav"),
                           MarketPriceEffectiveDate = x.Field<DateTime?>("market_price_effective_date"),
                           UnrealisedGainLossRm = x.Field<Decimal?>("unrealised_gain_loss_rm") == null ? 0 : x.Field<Decimal?>("unrealised_gain_loss_rm"),
                           ReportDate = x.Field<DateTime?>("report_date"),
                           ReportKey = x.Field<String>("report_key"),
                           Isactive = x.Field<String>("isactive"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcCompositionalInvestment> objs = new List<UtmcCompositionalInvestment>();
            try
            {
                string query = "select * from utmc_compositional_investment";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcCompositionalInvestment
                        {
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            IpdMemberAccNo = x.Field<String>("ipd_member_acc_no"),
                            EffectiveDate = x.Field<DateTime?>("effective_date"),
                            OpeningBalanceUnits = x.Field<Decimal?>("opening_balance_units") == null ? 0 : x.Field<Decimal?>("opening_balance_units"),
                            OpeningBalanceCostRm = x.Field<Decimal?>("opening_balance_cost_rm") == null ? 0 : x.Field<Decimal?>("opening_balance_cost_rm"),
                            OpeningBalanceDate = x.Field<DateTime>("opening_balance_date"),
                            NetCumulativeClosingBalanceUnits = x.Field<Decimal?>("net_cumulative_closing_balance_units") == null ? 0 : x.Field<Decimal?>("net_cumulative_closing_balance_units"),
                            NetCumulativeClosingBalanceCostRm = x.Field<Decimal?>("net_cumulative_closing_balance_cost_rm") == null ? 0 : x.Field<Decimal?>("net_cumulative_closing_balance_cost_rm"),
                            NetCumulativeClosingBalanceDate = x.Field<DateTime?>("net_cumulative_closing_balance_date"),
                            MarketPriceNav = x.Field<Decimal?>("market_price_nav") == null ? 0 : x.Field<Decimal?>("market_price_nav"),
                            MarketPriceEffectiveDate = x.Field<DateTime?>("market_price_effective_date"),
                            UnrealisedGainLossRm = x.Field<Decimal?>("unrealised_gain_loss_rm") == null ? 0 : x.Field<Decimal?>("unrealised_gain_loss_rm"),
                            ReportDate = x.Field<DateTime?>("report_date"),
                            ReportKey = x.Field<String>("report_key"),
                            Isactive = x.Field<String>("isactive"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_compositional_investment";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<UtmcCompositionalInvestment>(propertyName);
            List<UtmcCompositionalInvestment> objs = new List<UtmcCompositionalInvestment>();
            try
            {
                string query = "select * from utmc_compositional_investment where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from utmc_compositional_investment where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcCompositionalInvestment
                        {
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            IpdMemberAccNo = x.Field<String>("ipd_member_acc_no"),
                            EffectiveDate = x.Field<DateTime?>("effective_date"),
                            OpeningBalanceUnits = x.Field<Decimal?>("opening_balance_units") == null ? 0 : x.Field<Decimal?>("opening_balance_units"),
                            OpeningBalanceCostRm = x.Field<Decimal?>("opening_balance_cost_rm") == null ? 0 : x.Field<Decimal?>("opening_balance_cost_rm"),
                            OpeningBalanceDate = x.Field<DateTime>("opening_balance_date"),
                            NetCumulativeClosingBalanceUnits = x.Field<Decimal?>("net_cumulative_closing_balance_units") == null ? 0 : x.Field<Decimal?>("net_cumulative_closing_balance_units"),
                            NetCumulativeClosingBalanceCostRm = x.Field<Decimal?>("net_cumulative_closing_balance_cost_rm") == null ? 0 : x.Field<Decimal?>("net_cumulative_closing_balance_cost_rm"),
                            NetCumulativeClosingBalanceDate = x.Field<DateTime?>("net_cumulative_closing_balance_date"),
                            MarketPriceNav = x.Field<Decimal?>("market_price_nav") == null ? 0 : x.Field<Decimal?>("market_price_nav"),
                            MarketPriceEffectiveDate = x.Field<DateTime?>("market_price_effective_date"),
                            UnrealisedGainLossRm = x.Field<Decimal?>("unrealised_gain_loss_rm") == null ? 0 : x.Field<Decimal?>("unrealised_gain_loss_rm"),
                            ReportDate = x.Field<DateTime?>("report_date"),
                            ReportKey = x.Field<String>("report_key"),
                            Isactive = x.Field<String>("isactive"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<UtmcCompositionalInvestment>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_compositional_investment where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from utmc_compositional_investment where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcCompositionalInvestment> objs = new List<UtmcCompositionalInvestment>();
            try
            {
                string query = "select * from utmc_compositional_investment where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcCompositionalInvestment
                        {
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            IpdMemberAccNo = x.Field<String>("ipd_member_acc_no"),
                            EffectiveDate = x.Field<DateTime?>("effective_date"),
                            OpeningBalanceUnits = x.Field<Decimal?>("opening_balance_units") == null ? 0 : x.Field<Decimal?>("opening_balance_units"),
                            OpeningBalanceCostRm = x.Field<Decimal?>("opening_balance_cost_rm") == null ? 0 : x.Field<Decimal?>("opening_balance_cost_rm"),
                            OpeningBalanceDate = x.Field<DateTime>("opening_balance_date"),
                            NetCumulativeClosingBalanceUnits = x.Field<Decimal?>("net_cumulative_closing_balance_units") == null ? 0 : x.Field<Decimal?>("net_cumulative_closing_balance_units"),
                            NetCumulativeClosingBalanceCostRm = x.Field<Decimal?>("net_cumulative_closing_balance_cost_rm") == null ? 0 : x.Field<Decimal?>("net_cumulative_closing_balance_cost_rm"),
                            NetCumulativeClosingBalanceDate = x.Field<DateTime?>("net_cumulative_closing_balance_date"),
                            MarketPriceNav = x.Field<Decimal?>("market_price_nav") == null ? 0 : x.Field<Decimal?>("market_price_nav"),
                            MarketPriceEffectiveDate = x.Field<DateTime?>("market_price_effective_date"),
                            UnrealisedGainLossRm = x.Field<Decimal?>("unrealised_gain_loss_rm") == null ? 0 : x.Field<Decimal?>("unrealised_gain_loss_rm"),
                            ReportDate = x.Field<DateTime?>("report_date"),
                            ReportKey = x.Field<String>("report_key"),
                            Isactive = x.Field<String>("isactive"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_compositional_investment where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
