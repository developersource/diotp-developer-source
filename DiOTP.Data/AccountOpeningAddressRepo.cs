﻿using DiOTP.Data.IRepo;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Data
{
    public class AccountOpeningAddressRepo : IAccountOpeningAddressRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(AccountOpeningAddress obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<AccountOpeningAddress>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                obj.Id = Convert.ToInt32(cmd.LastInsertedId);
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<AccountOpeningAddress> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<AccountOpeningAddress>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(AccountOpeningAddress obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<AccountOpeningAddress>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<AccountOpeningAddress> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<AccountOpeningAddress>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public AccountOpeningAddress DeleteData(Int32 Id)
        {
            Response responseULM = GetSingle(Id);
            if (responseULM.IsSuccess)
            {
                AccountOpeningAddress obj = (AccountOpeningAddress)responseULM.Data;
                try
                {
                    string query = obj.ObjectToQuery<AccountOpeningAddress>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                    Response responseULM = GetSingle(Id);
                    if (responseULM.IsSuccess)
                    {
                        AccountOpeningAddress obj = (AccountOpeningAddress)responseULM.Data;
                        query += obj.ObjectToQuery<AccountOpeningAddress>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            AccountOpeningAddress obj = new AccountOpeningAddress();
            try
            {
                string query = "select * from ao_addresses where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new AccountOpeningAddress
                       {
                           Id = x.Field<Int32>("id"),
                           AddressType = x.Field<Int32>("address_type"),
                           AccountOpeningId = x.Field<Int32>("account_opening_id"),
                           Addr1 = x.Field<String>("addr_1"),
                           Addr2 = x.Field<String>("addr_2"),
                           Addr3 = x.Field<String>("addr_3"),
                           City = x.Field<String>("city"),
                           PostCode = x.Field<String>("post_code"),
                           State = x.Field<String>("state"),
                           Country = x.Field<String>("country"),
                           TelNo = x.Field<String>("tel_no"),
                           Status = x.Field<Int32>("status")
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<AccountOpeningAddress> objs = new List<AccountOpeningAddress>();
            try
            {
                string query = "select * from ao_addresses";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new AccountOpeningAddress
                        {
                            Id = x.Field<Int32>("id"),
                            AddressType = x.Field<Int32>("address_type"),
                            AccountOpeningId = x.Field<Int32>("account_opening_id"),
                            Addr1 = x.Field<String>("addr_1"),
                            Addr2 = x.Field<String>("addr_2"),
                            Addr3 = x.Field<String>("addr_3"),
                            City = x.Field<String>("city"),
                            PostCode = x.Field<String>("post_code"),
                            State = x.Field<String>("state"),
                            Country = x.Field<String>("country"),
                            TelNo = x.Field<String>("tel_no"),
                            Status = x.Field<Int32>("status")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from ao_addresses";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<AccountOpeningAddress>(propertyName);
            List<AccountOpeningAddress> objs = new List<AccountOpeningAddress>();
            try
            {
                string query = "select * from ao_addresses where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from ao_addresses where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new AccountOpeningAddress
                        {
                            Id = x.Field<Int32>("id"),
                            AddressType = x.Field<Int32>("address_type"),
                            AccountOpeningId = x.Field<Int32>("account_opening_id"),
                            Addr1 = x.Field<String>("addr_1"),
                            Addr2 = x.Field<String>("addr_2"),
                            Addr3 = x.Field<String>("addr_3"),
                            City = x.Field<String>("city"),
                            PostCode = x.Field<String>("post_code"),
                            State = x.Field<String>("state"),
                            Country = x.Field<String>("country"),
                            TelNo = x.Field<String>("tel_no"),
                            Status = x.Field<Int32>("status")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<AccountOpeningAddress>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from ao_addresses where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from ao_addresses where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<AccountOpeningAddress> objs = new List<AccountOpeningAddress>();
            try
            {
                string query = "select * from ao_addresses where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new AccountOpeningAddress
                        {
                            Id = x.Field<Int32>("id"),
                            AddressType = x.Field<Int32>("address_type"),
                            AccountOpeningId = x.Field<Int32>("account_opening_id"),
                            Addr1 = x.Field<String>("addr_1"),
                            Addr2 = x.Field<String>("addr_2"),
                            Addr3 = x.Field<String>("addr_3"),
                            City = x.Field<String>("city"),
                            PostCode = x.Field<String>("post_code"),
                            State = x.Field<String>("state"),
                            Country = x.Field<String>("country"),
                            TelNo = x.Field<String>("tel_no"),
                            Status = x.Field<Int32>("status")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from ao_addresses where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
