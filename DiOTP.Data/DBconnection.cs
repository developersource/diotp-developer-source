﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Data
{
    class DBconnection
    {
        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["DiOTPConnection"].ConnectionString;
        }

        public static MySqlConnection GetConnection()
        {
            return new MySqlConnection(GetConnectionString());
        }
    }
}
