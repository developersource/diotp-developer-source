using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class FundInfoRepo : IFundInfoRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(FundInfo obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<FundInfo>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<FundInfo> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<FundInfo>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(FundInfo obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<FundInfo>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<FundInfo> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<FundInfo>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public FundInfo DeleteData(Int32 Id)
        {
            Response responseFI = GetSingle(Id);
            if (responseFI.IsSuccess)
            {
                FundInfo obj = (FundInfo)responseFI.Data;
                try
                {
                    string query = obj.ObjectToQuery<FundInfo>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseFI = GetSingle(Id);
                    if (responseFI.IsSuccess)
                    {
                        FundInfo obj = (FundInfo)responseFI.Data;
                        query += obj.ObjectToQuery<FundInfo>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            FundInfo obj = new FundInfo();
            try
            {
                string query = "select * from fund_info where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new FundInfo
                       {
                           Id = x.Field<Int32>("id"),
                           FundCode = x.Field<String>("fund_code"),
                           CurrentNavDate = x.Field<DateTime>("current_nav_date"),
                           CurrentUnitPrice = x.Field<Decimal>("current_unit_price"),
                           ChangePrice = x.Field<Decimal>("change_price"),
                           ChangePer = x.Field<Decimal>("change_per"),
                           UpdatedDate = x.Field<DateTime>("updated_date"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FundInfo> objs = new List<FundInfo>();
            try
            {
                string query = "select * from fund_info";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FundInfo
                        {
                            Id = x.Field<Int32>("id"),
                            FundCode = x.Field<String>("fund_code"),
                            CurrentNavDate = x.Field<DateTime>("current_nav_date"),
                            CurrentUnitPrice = x.Field<Decimal>("current_unit_price"),
                            ChangePrice = x.Field<Decimal>("change_price"),
                            ChangePer = x.Field<Decimal>("change_per"),
                            UpdatedDate = x.Field<DateTime>("updated_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from fund_info";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<FundInfo>(propertyName);
            List<FundInfo> objs = new List<FundInfo>();
            try
            {
                string query = "select * from fund_info where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from fund_info where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FundInfo
                        {
                            Id = x.Field<Int32>("id"),
                            FundCode = x.Field<String>("fund_code"),
                            CurrentNavDate = x.Field<DateTime>("current_nav_date"),
                            CurrentUnitPrice = x.Field<Decimal>("current_unit_price"),
                            ChangePrice = x.Field<Decimal>("change_price"),
                            ChangePer = x.Field<Decimal>("change_per"),
                            UpdatedDate = x.Field<DateTime>("updated_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<FundInfo>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from fund_info where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from fund_info where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FundInfo> objs = new List<FundInfo>();
            try
            {
                string query = "select * from fund_info where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FundInfo
                        {
                            Id = x.Field<Int32>("id"),
                            FundCode = x.Field<String>("fund_code"),
                            CurrentNavDate = x.Field<DateTime>("current_nav_date"),
                            CurrentUnitPrice = x.Field<Decimal>("current_unit_price"),
                            ChangePrice = x.Field<Decimal>("change_price"),
                            ChangePer = x.Field<Decimal>("change_per"),
                            UpdatedDate = x.Field<DateTime>("updated_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from fund_info where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        
    }
}
