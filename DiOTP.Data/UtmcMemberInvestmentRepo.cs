using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class UtmcMemberInvestmentRepo : IUtmcMemberInvestmentRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(UtmcMemberInvestment obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcMemberInvestment>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UtmcMemberInvestment> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcMemberInvestment>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UtmcMemberInvestment obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcMemberInvestment>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UtmcMemberInvestment> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcMemberInvestment>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UtmcMemberInvestment DeleteData(Int32 Id)
        {
            Response responseUMI = GetSingle(Id);
            if (responseUMI.IsSuccess)
            {
                UtmcMemberInvestment obj = (UtmcMemberInvestment)responseUMI.Data;
                try
                {
                    string query = obj.ObjectToQuery<UtmcMemberInvestment>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseUMI = GetSingle(Id);
                    if (responseUMI.IsSuccess)
                    {
                        UtmcMemberInvestment obj = (UtmcMemberInvestment)responseUMI.Data;
                        query += obj.ObjectToQuery<UtmcMemberInvestment>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UtmcMemberInvestment obj = new UtmcMemberInvestment();
            try
            {
                string query = "select * from utmc_member_investment where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new UtmcMemberInvestment
                       {
                           Id = x.Field<Int32>("id"),
                           MemberEpfNo = x.Field<String>("member_epf_no"),
                           EpfIpdCode = x.Field<String>("epf_ipd_code"),
                           IpdFundCode = x.Field<String>("ipd_fund_code"),
                           IpdMemberAccNo = x.Field<String>("ipd_member_acc_no"),
                           ActualTransferredFromEpfRm = x.Field<Decimal>("actual_transferred_from_epf_rm"),
                           ActualCost = x.Field<Decimal>("actual_cost"),
                           Units = x.Field<Decimal>("units"),
                           BookValue = x.Field<Decimal>("book_value"),
                           MarketValue = x.Field<Decimal>("market_value"),
                           EffectiveDate = x.Field<DateTime>("effective_date"),
                           ReportDate = x.Field<DateTime>("report_date"),
                           RedemptionCost = x.Field<Decimal?>("redemption_cost") == null ? 0 : x.Field<Decimal?>("redemption_cost"),
                           Isactive = x.Field<String>("isactive"),
                           CurrentUnitHoldingValue = x.Field<Decimal?>("Current_Unit_Holding_Value") == null ? 0 : x.Field<Decimal?>("Current_Unit_Holding_Value")
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcMemberInvestment> objs = new List<UtmcMemberInvestment>();
            try
            {
                string query = "select * from utmc_member_investment";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcMemberInvestment
                        {
                            Id = x.Field<Int32>("id"),
                            MemberEpfNo = x.Field<String>("member_epf_no"),
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            IpdMemberAccNo = x.Field<String>("ipd_member_acc_no"),
                            ActualTransferredFromEpfRm = x.Field<Decimal>("actual_transferred_from_epf_rm"),
                            ActualCost = x.Field<Decimal>("actual_cost"),
                            Units = x.Field<Decimal>("units"),
                            BookValue = x.Field<Decimal>("book_value"),
                            MarketValue = x.Field<Decimal>("market_value"),
                            EffectiveDate = x.Field<DateTime>("effective_date"),
                            ReportDate = x.Field<DateTime>("report_date"),
                            RedemptionCost = x.Field<Decimal?>("redemption_cost") == null ? 0 : x.Field<Decimal?>("redemption_cost"),
                            Isactive = x.Field<String>("isactive"),
                           CurrentUnitHoldingValue = x.Field<Decimal?>("Current_Unit_Holding_Value") == null ? 0 : x.Field<Decimal?>("Current_Unit_Holding_Value")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_member_investment";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<UtmcMemberInvestment>(propertyName);
            List<UtmcMemberInvestment> objs = new List<UtmcMemberInvestment>();
            try
            {
                string query = "select * from utmc_member_investment where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from utmc_member_investment where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcMemberInvestment
                        {
                            Id = x.Field<Int32>("id"),
                            MemberEpfNo = x.Field<String>("member_epf_no"),
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            IpdMemberAccNo = x.Field<String>("ipd_member_acc_no"),
                            ActualTransferredFromEpfRm = x.Field<Decimal>("actual_transferred_from_epf_rm"),
                            ActualCost = x.Field<Decimal>("actual_cost"),
                            Units = x.Field<Decimal>("units"),
                            BookValue = x.Field<Decimal>("book_value"),
                            MarketValue = x.Field<Decimal>("market_value"),
                            EffectiveDate = x.Field<DateTime>("effective_date"),
                            ReportDate = x.Field<DateTime>("report_date"),
                            RedemptionCost = x.Field<Decimal?>("redemption_cost") == null ? 0 : x.Field<Decimal?>("redemption_cost"),
                            Isactive = x.Field<String>("isactive"),
                           CurrentUnitHoldingValue = x.Field<Decimal?>("Current_Unit_Holding_Value") == null ? 0 : x.Field<Decimal?>("Current_Unit_Holding_Value")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<UtmcMemberInvestment>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_member_investment where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from utmc_member_investment where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcMemberInvestment> objs = new List<UtmcMemberInvestment>();
            try
            {
                string query = "select * from utmc_member_investment where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcMemberInvestment
                        {
                            Id = x.Field<Int32>("id"),
                            MemberEpfNo = x.Field<String>("member_epf_no"),
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            IpdMemberAccNo = x.Field<String>("ipd_member_acc_no"),
                            ActualTransferredFromEpfRm = x.Field<Decimal>("actual_transferred_from_epf_rm"),
                            ActualCost = x.Field<Decimal>("actual_cost"),
                            Units = x.Field<Decimal>("units"),
                            BookValue = x.Field<Decimal>("book_value"),
                            MarketValue = x.Field<Decimal>("market_value"),
                            EffectiveDate = x.Field<DateTime>("effective_date"),
                            ReportDate = x.Field<DateTime>("report_date"),
                            RedemptionCost = x.Field<Decimal?>("redemption_cost") == null ? 0 : x.Field<Decimal?>("redemption_cost"),
                            Isactive = x.Field<String>("isactive"),
                           CurrentUnitHoldingValue = x.Field<Decimal?>("Current_Unit_Holding_Value") == null ? 0 : x.Field<Decimal?>("Current_Unit_Holding_Value")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_member_investment where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
