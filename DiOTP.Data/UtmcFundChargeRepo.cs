using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class UtmcFundChargeRepo : IUtmcFundChargeRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(UtmcFundCharge obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcFundCharge>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UtmcFundCharge> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcFundCharge>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UtmcFundCharge obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcFundCharge>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UtmcFundCharge> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcFundCharge>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UtmcFundCharge DeleteData(Int32 Id)
        {
            Response responseUFC = GetSingle(Id);
            if (responseUFC.IsSuccess)
            {
                UtmcFundCharge obj = (UtmcFundCharge)responseUFC.Data;
                try
                {
                    string query = obj.ObjectToQuery<UtmcFundCharge>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseUFC = GetSingle(Id);
                    if (responseUFC.IsSuccess)
                    {
                        UtmcFundCharge obj = (UtmcFundCharge)responseUFC.Data;
                        query += obj.ObjectToQuery<UtmcFundCharge>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UtmcFundCharge obj = new UtmcFundCharge();
            try
            {
                string query = "select * from utmc_fund_charges where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new UtmcFundCharge
                       {
                           Id = x.Field<Int32>("id"),
                           UtmcFundInformationId = x.Field<Int32>("utmc_fund_information_id"),
                           InitialSalesChargesPercent = x.Field<Decimal>("initial_sales_charges_percent"),
                           EpfSalesChargesPercent = x.Field<Decimal>("epf_sales_charges_percent"),
                           AnnualManagementChargePercent = x.Field<Decimal>("annual_management_charge_percent"),
                           TrusteeFeePercent = x.Field<Decimal>("trustee_fee_percent"),
                           SwitchingFeePercent = x.Field<Decimal>("switching_fee_percent"),
                           RedemptionFeePercent = x.Field<Decimal>("redemption_fee_percent"),
                           TransferFeeRm = x.Field<Decimal>("transfer_fee_rm"),
                           OtherSignificantFeeRm = x.Field<Decimal>("other_significant_fee_rm"),
                           GstPercent = x.Field<Decimal>("gst_percent"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundCharge> objs = new List<UtmcFundCharge>();
            try
            {
                string query = "select * from utmc_fund_charges";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcFundCharge
                        {
                            Id = x.Field<Int32>("id"),
                            UtmcFundInformationId = x.Field<Int32>("utmc_fund_information_id"),
                            InitialSalesChargesPercent = x.Field<Decimal>("initial_sales_charges_percent"),
                            EpfSalesChargesPercent = x.Field<Decimal>("epf_sales_charges_percent"),
                            AnnualManagementChargePercent = x.Field<Decimal>("annual_management_charge_percent"),
                            TrusteeFeePercent = x.Field<Decimal>("trustee_fee_percent"),
                            SwitchingFeePercent = x.Field<Decimal>("switching_fee_percent"),
                            RedemptionFeePercent = x.Field<Decimal>("redemption_fee_percent"),
                            TransferFeeRm = x.Field<Decimal>("transfer_fee_rm"),
                            OtherSignificantFeeRm = x.Field<Decimal>("other_significant_fee_rm"),
                            GstPercent = x.Field<Decimal>("gst_percent"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_fund_charges";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<UtmcFundCharge>(propertyName);
            List<UtmcFundCharge> objs = new List<UtmcFundCharge>();
            try
            {
                string query = "select * from utmc_fund_charges where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from utmc_fund_charges where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcFundCharge
                        {
                            Id = x.Field<Int32>("id"),
                            UtmcFundInformationId = x.Field<Int32>("utmc_fund_information_id"),
                            InitialSalesChargesPercent = x.Field<Decimal>("initial_sales_charges_percent"),
                            EpfSalesChargesPercent = x.Field<Decimal>("epf_sales_charges_percent"),
                            AnnualManagementChargePercent = x.Field<Decimal>("annual_management_charge_percent"),
                            TrusteeFeePercent = x.Field<Decimal>("trustee_fee_percent"),
                            SwitchingFeePercent = x.Field<Decimal>("switching_fee_percent"),
                            RedemptionFeePercent = x.Field<Decimal>("redemption_fee_percent"),
                            TransferFeeRm = x.Field<Decimal>("transfer_fee_rm"),
                            OtherSignificantFeeRm = x.Field<Decimal>("other_significant_fee_rm"),
                            GstPercent = x.Field<Decimal>("gst_percent"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<UtmcFundCharge>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_fund_charges where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from utmc_fund_charges where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundCharge> objs = new List<UtmcFundCharge>();
            try
            {
                string query = "select * from utmc_fund_charges where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcFundCharge
                        {
                            Id = x.Field<Int32>("id"),
                            UtmcFundInformationId = x.Field<Int32>("utmc_fund_information_id"),
                            InitialSalesChargesPercent = x.Field<Decimal>("initial_sales_charges_percent"),
                            EpfSalesChargesPercent = x.Field<Decimal>("epf_sales_charges_percent"),
                            AnnualManagementChargePercent = x.Field<Decimal>("annual_management_charge_percent"),
                            TrusteeFeePercent = x.Field<Decimal>("trustee_fee_percent"),
                            SwitchingFeePercent = x.Field<Decimal>("switching_fee_percent"),
                            RedemptionFeePercent = x.Field<Decimal>("redemption_fee_percent"),
                            TransferFeeRm = x.Field<Decimal>("transfer_fee_rm"),
                            OtherSignificantFeeRm = x.Field<Decimal>("other_significant_fee_rm"),
                            GstPercent = x.Field<Decimal>("gst_percent"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_fund_charges where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
