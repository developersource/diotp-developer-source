using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class UtmcDailyNavFundRepo : IUtmcDailyNavFundRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(UtmcDailyNavFund obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcDailyNavFund>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UtmcDailyNavFund> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcDailyNavFund>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UtmcDailyNavFund obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcDailyNavFund>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UtmcDailyNavFund> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcDailyNavFund>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UtmcDailyNavFund DeleteData(Int32 Id)
        {
            Response responseUDNF = GetSingle(Id);
            if (responseUDNF.IsSuccess)
            {
                UtmcDailyNavFund obj = (UtmcDailyNavFund)responseUDNF.Data;
                try
                {
                    string query = obj.ObjectToQuery<UtmcDailyNavFund>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseUDNF = GetSingle(Id);
                    if (responseUDNF.IsSuccess)
                    {
                        UtmcDailyNavFund obj = (UtmcDailyNavFund)responseUDNF.Data;
                        query += obj.ObjectToQuery<UtmcDailyNavFund>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UtmcDailyNavFund obj = new UtmcDailyNavFund();
            try
            {
                string query = "select * from utmc_daily_nav_fund where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new UtmcDailyNavFund
                       {
                           EpfIpdCode = x.Field<String>("epf_ipd_code"),
                           IpdFundCode = x.Field<String>("ipd_fund_code"),
                           DailyNavDate = x.Field<DateTime>("daily_nav_date"),
                           DailyNav = x.Field<Decimal>("daily_nav"),
                           ReportDate = x.Field<DateTime>("report_date"),
                           DailyUnitCreated = x.Field<Decimal>("daily_unit_created"),
                           DailyNavEpf = x.Field<Decimal>("daily_nav_epf"),
                           DailyUnitCreatedEpf = x.Field<Decimal>("daily_unit_created_epf"),
                           DailyUnitPrice = x.Field<Decimal>("daily_unit_price"),
                           DailyUnitCreatedEpfAdj = x.Field<Decimal>("daily_unit_created_epf_adj"),
                           Id = x.Field<Int32>("id"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcDailyNavFund> objs = new List<UtmcDailyNavFund>();
            try
            {
                string query = "select * from utmc_daily_nav_fund";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcDailyNavFund
                        {
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            DailyNavDate = x.Field<DateTime>("daily_nav_date"),
                            DailyNav = x.Field<Decimal>("daily_nav"),
                            ReportDate = x.Field<DateTime>("report_date"),
                            DailyUnitCreated = x.Field<Decimal>("daily_unit_created"),
                            DailyNavEpf = x.Field<Decimal>("daily_nav_epf"),
                            DailyUnitCreatedEpf = x.Field<Decimal>("daily_unit_created_epf"),
                            DailyUnitPrice = x.Field<Decimal>("daily_unit_price"),
                            DailyUnitCreatedEpfAdj = x.Field<Decimal>("daily_unit_created_epf_adj"),
                            Id = x.Field<Int32>("id"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_daily_nav_fund";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<UtmcDailyNavFund>(propertyName);
            List<UtmcDailyNavFund> objs = new List<UtmcDailyNavFund>();
            try
            {
                string query = "select * from utmc_daily_nav_fund where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from utmc_daily_nav_fund where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcDailyNavFund
                        {
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            DailyNavDate = x.Field<DateTime>("daily_nav_date"),
                            DailyNav = x.Field<Decimal>("daily_nav"),
                            ReportDate = x.Field<DateTime>("report_date"),
                            DailyUnitCreated = x.Field<Decimal>("daily_unit_created"),
                            DailyNavEpf = x.Field<Decimal>("daily_nav_epf"),
                            DailyUnitCreatedEpf = x.Field<Decimal>("daily_unit_created_epf"),
                            DailyUnitPrice = x.Field<Decimal>("daily_unit_price"),
                            DailyUnitCreatedEpfAdj = x.Field<Decimal>("daily_unit_created_epf_adj"),
                            Id = x.Field<Int32>("id"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<UtmcDailyNavFund>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_daily_nav_fund where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from utmc_daily_nav_fund where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcDailyNavFund> objs = new List<UtmcDailyNavFund>();
            try
            {
                string query = "select * from utmc_daily_nav_fund where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcDailyNavFund
                        {
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            DailyNavDate = x.Field<DateTime>("daily_nav_date"),
                            DailyNav = x.Field<Decimal>("daily_nav"),
                            ReportDate = x.Field<DateTime>("report_date"),
                            DailyUnitCreated = x.Field<Decimal>("daily_unit_created"),
                            DailyNavEpf = x.Field<Decimal>("daily_nav_epf"),
                            DailyUnitCreatedEpf = x.Field<Decimal>("daily_unit_created_epf"),
                            DailyUnitPrice = x.Field<Decimal>("daily_unit_price"),
                            DailyUnitCreatedEpfAdj = x.Field<Decimal>("daily_unit_created_epf_adj"),
                            Id = x.Field<Int32>("id"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_daily_nav_fund where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        
    }
}
