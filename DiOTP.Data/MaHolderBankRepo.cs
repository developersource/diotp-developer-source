using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class MaHolderBankRepo : IMaHolderBankRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(MaHolderBank obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<MaHolderBank>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                obj.Id = Convert.ToInt32(cmd.LastInsertedId);
                // Update previous rec status
                //string queryforlastId = "select Id from ma_holder_bank where user_id="+obj.UserId+" order by Id desc LIMIT 1;";
                //cmd = new MySqlCommand(queryforlastId, mySQLDBConnect.connection);
                //long id = Convert.ToInt32(cmd.ExecuteScalar());
                //if(id != 0)
                //{
                //query = "update ma_holder_bank set Status=0 where user_id="+obj.UserId+" and Id !=" + id;
                //cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                //cmd.ExecuteNonQuery();
                //}
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<MaHolderBank> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<MaHolderBank>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(MaHolderBank obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<MaHolderBank>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<MaHolderBank> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<MaHolderBank>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public MaHolderBank DeleteData(Int32 Id)
        {
            Response responseMHB = GetSingle(Id);
            if (responseMHB.IsSuccess)
            {
                MaHolderBank obj = (MaHolderBank)responseMHB.Data;
                try
                {
                    string query = obj.ObjectToQuery<MaHolderBank>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseMHB = GetSingle(Id);
                    if (responseMHB.IsSuccess)
                    {
                        MaHolderBank obj = (MaHolderBank)responseMHB.Data;
                        query += obj.ObjectToQuery<MaHolderBank>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            MaHolderBank obj = new MaHolderBank();
            try
            {
                string query = "select * from ma_holder_bank where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new MaHolderBank
                       {
                           Id = x.Field<Int32>("id"),
                           UserId = x.Field<Int32>("user_id"),
                           IsPrimary = x.Field<Int32>("is_primary"),
                           BankDefId = x.Field<Int32>("bank_def_id"),
                           AccountName = x.Field<String>("account_name"),
                           BankAccountNo = x.Field<String>("bank_account_no"),
                           Remarks = x.Field<String>("remarks"),
                           CreatedDate = x.Field<DateTime>("created_date"),
                           CreatedBy = x.Field<Int32>("created_by"),
                           UpdatedDate = x.Field<DateTime?>("updated_date"),
                           UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                           Version = x.Field<Int32>("version"),
                           Status = x.Field<Int32>("status"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<MaHolderBank> objs = new List<MaHolderBank>();
            try
            {
                string query = "select * from ma_holder_bank";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new MaHolderBank
                        {
                            Id = x.Field<Int32>("id"),
                            UserId = x.Field<Int32>("user_id"),
                            IsPrimary = x.Field<Int32>("is_primary"),
                            BankDefId = x.Field<Int32>("bank_def_id"),
                            AccountName = x.Field<String>("account_name"),
                            BankAccountNo = x.Field<String>("bank_account_no"),
                            Remarks = x.Field<String>("remarks"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            UpdatedDate = x.Field<DateTime?>("updated_date"),
                            UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                            Version = x.Field<Int32>("version"),
                            Status = x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from ma_holder_bank";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<MaHolderBank>(propertyName);
            List<MaHolderBank> objs = new List<MaHolderBank>();
            try
            {
                // string query = "select * from ma_holder_bank where " + columnName + " = @propertyValue ";
                string query = "select * from ma_holder_bank where " + columnName + " = @propertyValue  ";
                if (!isEqual)
                    query = "select * from ma_holder_bank where " + columnName + " != @propertyValue  ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new MaHolderBank
                        {
                            Id = x.Field<Int32>("id"),
                            UserId = x.Field<Int32>("user_id"),
                            IsPrimary = x.Field<Int32>("is_primary"),
                            BankDefId = x.Field<Int32>("bank_def_id"),
                            AccountName = x.Field<String>("account_name"),
                            BankAccountNo = x.Field<String>("bank_account_no"),
                            Remarks = x.Field<String>("remarks"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            UpdatedDate = x.Field<DateTime?>("updated_date"),
                            UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                            Version = x.Field<Int32>("version"),
                            Status = x.Field<Int32>("status"),
                            Image = x.Field<string>("image")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<MaHolderBank>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from ma_holder_bank where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from ma_holder_bank where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<MaHolderBank> objs = new List<MaHolderBank>();
            try
            {
                string query = "select * from ma_holder_bank where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new MaHolderBank
                        {
                            Id = x.Field<Int32>("id"),
                            UserId = x.Field<Int32>("user_id"),
                            IsPrimary = x.Field<Int32>("is_primary"),
                            BankDefId = x.Field<Int32>("bank_def_id"),
                            AccountName = x.Field<String>("account_name"),
                            BankAccountNo = x.Field<String>("bank_account_no"),
                            Remarks = x.Field<String>("remarks"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            CreatedBy = x.Field<Int32>("created_by"),
                            UpdatedDate = x.Field<DateTime?>("updated_date"),
                            UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                            Version = x.Field<Int32>("version"),
                            Status = x.Field<Int32>("status"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            Image = x.Field<String>("image")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from ma_holder_bank where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        
    }
}
