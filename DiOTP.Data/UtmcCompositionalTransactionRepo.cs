using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class UtmcCompositionalTransactionRepo : IUtmcCompositionalTransactionRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(UtmcCompositionalTransaction obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcCompositionalTransaction>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UtmcCompositionalTransaction> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcCompositionalTransaction>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UtmcCompositionalTransaction obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcCompositionalTransaction>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UtmcCompositionalTransaction> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcCompositionalTransaction>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UtmcCompositionalTransaction DeleteData(Int32 Id)
        {
            Response responseUCT = GetSingle(Id);
            if (responseUCT.IsSuccess)
            {
                UtmcCompositionalTransaction obj = (UtmcCompositionalTransaction)responseUCT.Data;
                try
                {
                    string query = obj.ObjectToQuery<UtmcCompositionalTransaction>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseUCT = GetSingle(Id);
                    if (responseUCT.IsSuccess)
                    {
                        UtmcCompositionalTransaction obj = (UtmcCompositionalTransaction)responseUCT.Data;
                        query += obj.ObjectToQuery<UtmcCompositionalTransaction>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UtmcCompositionalTransaction obj = new UtmcCompositionalTransaction();
            try
            {
                string query = "select * from utmc_compositional_transactions where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new UtmcCompositionalTransaction
                       {
                           Id = x.Field<Int32>("id"),
                           EpfIpdCode = x.Field<String>("epf_ipd_code"),
                           IpdFundCode = x.Field<String>("ipd_fund_code"),
                           MemberEpfNo = x.Field<String>("member_epf_no"),
                           IpdMemberAccNo = x.Field<String>("ipd_member_acc_no"),
                           EffectiveDate = x.Field<DateTime?>("effective_date"),
                           DateOfTransaction = x.Field<DateTime?>("date_of_transaction"),
                           DateOfSettlement = x.Field<DateTime?>("date_of_settlement"),
                           IpdUniqueTransactionId = x.Field<String>("ipd_unique_transaction_id"),
                           TransactionCode = x.Field<String>("transaction_code"),
                           ReversedTransactionId = x.Field<String>("reversed_transaction_id"),
                           Units = x.Field<Decimal?>("units") == null ? 0 : x.Field<Decimal?>("units"),
                           GrossAmountRm = x.Field<Decimal?>("gross_amount_rm") == null ? 0 : x.Field<Decimal?>("gross_amount_rm"),
                           NetAmountRm = x.Field<Decimal?>("net_amount_rm") == null ? 0 : x.Field<Decimal?>("net_amount_rm"),
                           FeesRm = x.Field<Decimal?>("fees_rm") == null ? 0 : x.Field<Decimal?>("fees_rm"),
                           GstRm = x.Field<Decimal?>("gst_rm") == null ? 0 : x.Field<Decimal?>("gst_rm"),
                           CostRm = x.Field<Decimal?>("cost_rm") == null ? 0 : x.Field<Decimal?>("cost_rm"),
                           ProceedsRm = x.Field<Decimal?>("proceeds_rm") == null ? 0 : x.Field<Decimal?>("proceeds_rm"),
                           RealisedGainLoss = x.Field<Decimal?>("realised_gain_loss") == null ? 0 : x.Field<Decimal?>("realised_gain_loss"),
                           ReportDate = x.Field<DateTime?>("report_date"),
                           Isactive = x.Field<String>("isactive"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcCompositionalTransaction> objs = new List<UtmcCompositionalTransaction>();
            try
            {
                string query = "select * from utmc_compositional_transactions";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcCompositionalTransaction
                        {
                            Id = x.Field<Int32>("id"),
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            MemberEpfNo = x.Field<String>("member_epf_no"),
                            IpdMemberAccNo = x.Field<String>("ipd_member_acc_no"),
                            EffectiveDate = x.Field<DateTime?>("effective_date"),
                            DateOfTransaction = x.Field<DateTime?>("date_of_transaction"),
                            DateOfSettlement = x.Field<DateTime?>("date_of_settlement"),
                            IpdUniqueTransactionId = x.Field<String>("ipd_unique_transaction_id"),
                            TransactionCode = x.Field<String>("transaction_code"),
                            ReversedTransactionId = x.Field<String>("reversed_transaction_id"),
                            Units = x.Field<Decimal?>("units") == null ? 0 : x.Field<Decimal?>("units"),
                            GrossAmountRm = x.Field<Decimal?>("gross_amount_rm") == null ? 0 : x.Field<Decimal?>("gross_amount_rm"),
                            NetAmountRm = x.Field<Decimal?>("net_amount_rm") == null ? 0 : x.Field<Decimal?>("net_amount_rm"),
                            FeesRm = x.Field<Decimal?>("fees_rm") == null ? 0 : x.Field<Decimal?>("fees_rm"),
                            GstRm = x.Field<Decimal?>("gst_rm") == null ? 0 : x.Field<Decimal?>("gst_rm"),
                            CostRm = x.Field<Decimal?>("cost_rm") == null ? 0 : x.Field<Decimal?>("cost_rm"),
                            ProceedsRm = x.Field<Decimal?>("proceeds_rm") == null ? 0 : x.Field<Decimal?>("proceeds_rm"),
                            RealisedGainLoss = x.Field<Decimal?>("realised_gain_loss") == null ? 0 : x.Field<Decimal?>("realised_gain_loss"),
                            ReportDate = x.Field<DateTime?>("report_date"),
                            Isactive = x.Field<String>("isactive"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_compositional_transactions";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<UtmcCompositionalTransaction>(propertyName);
            List<UtmcCompositionalTransaction> objs = new List<UtmcCompositionalTransaction>();
            try
            {
                string query = "select * from utmc_compositional_transactions where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from utmc_compositional_transactions where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcCompositionalTransaction
                        {
                            Id = x.Field<Int32>("id"),
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            MemberEpfNo = x.Field<String>("member_epf_no"),
                            IpdMemberAccNo = x.Field<String>("ipd_member_acc_no"),
                            EffectiveDate = x.Field<DateTime?>("effective_date"),
                            DateOfTransaction = x.Field<DateTime?>("date_of_transaction"),
                            DateOfSettlement = x.Field<DateTime?>("date_of_settlement"),
                            IpdUniqueTransactionId = x.Field<String>("ipd_unique_transaction_id"),
                            TransactionCode = x.Field<String>("transaction_code"),
                            ReversedTransactionId = x.Field<String>("reversed_transaction_id"),
                            Units = x.Field<Decimal?>("units") == null ? 0 : x.Field<Decimal?>("units"),
                            GrossAmountRm = x.Field<Decimal?>("gross_amount_rm") == null ? 0 : x.Field<Decimal?>("gross_amount_rm"),
                            NetAmountRm = x.Field<Decimal?>("net_amount_rm") == null ? 0 : x.Field<Decimal?>("net_amount_rm"),
                            FeesRm = x.Field<Decimal?>("fees_rm") == null ? 0 : x.Field<Decimal?>("fees_rm"),
                            GstRm = x.Field<Decimal?>("gst_rm") == null ? 0 : x.Field<Decimal?>("gst_rm"),
                            CostRm = x.Field<Decimal?>("cost_rm") == null ? 0 : x.Field<Decimal?>("cost_rm"),
                            ProceedsRm = x.Field<Decimal?>("proceeds_rm") == null ? 0 : x.Field<Decimal?>("proceeds_rm"),
                            RealisedGainLoss = x.Field<Decimal?>("realised_gain_loss") == null ? 0 : x.Field<Decimal?>("realised_gain_loss"),
                            ReportDate = x.Field<DateTime?>("report_date"),
                            Isactive = x.Field<String>("isactive"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<UtmcCompositionalTransaction>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_compositional_transactions where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from utmc_compositional_transactions where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcCompositionalTransaction> objs = new List<UtmcCompositionalTransaction>();
            try
            {
                string query = "select uct.*,tc.name as transaction_code from utmc_compositional_transactions uct join transaction_codes tc on (tc.code = uct.Transaction_Code) where " + filter + "";
                if (isOrderByDesc)
                    query += " order by uct.ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcCompositionalTransaction
                        {
                            Id = x.Field<Int32>("id"),
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            MemberEpfNo = x.Field<String>("member_epf_no"),
                            IpdMemberAccNo = x.Field<String>("ipd_member_acc_no"),
                            EffectiveDate = x.Field<DateTime?>("effective_date"),
                            DateOfTransaction = x.Field<DateTime?>("date_of_transaction"),
                            DateOfSettlement = x.Field<DateTime?>("date_of_settlement"),
                            IpdUniqueTransactionId = x.Field<String>("ipd_unique_transaction_id"),
                            TransactionCode = x.Field<String>("transaction_code1"),
                            ReversedTransactionId = x.Field<String>("reversed_transaction_id"),
                            Units = x.Field<Decimal?>("units") == null ? 0 : x.Field<Decimal?>("units"),
                            GrossAmountRm = x.Field<Decimal?>("gross_amount_rm") == null ? 0 : x.Field<Decimal?>("gross_amount_rm"),
                            NetAmountRm = x.Field<Decimal?>("net_amount_rm") == null ? 0 : x.Field<Decimal?>("net_amount_rm"),
                            FeesRm = x.Field<Decimal?>("fees_rm") == null ? 0 : x.Field<Decimal?>("fees_rm"),
                            GstRm = x.Field<Decimal?>("gst_rm") == null ? 0 : x.Field<Decimal?>("gst_rm"),
                            CostRm = x.Field<Decimal?>("cost_rm") == null ? 0 : x.Field<Decimal?>("cost_rm"),
                            ProceedsRm = x.Field<Decimal?>("proceeds_rm") == null ? 0 : x.Field<Decimal?>("proceeds_rm"),
                            RealisedGainLoss = x.Field<Decimal?>("realised_gain_loss") == null ? 0 : x.Field<Decimal?>("realised_gain_loss"),
                            ReportDate = x.Field<DateTime?>("report_date"),
                            Isactive = x.Field<String>("isactive"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_compositional_transactions where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
