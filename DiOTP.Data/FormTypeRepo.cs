using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class FormTypeRepo : IFormTypeRepo
    {
        IFormDataFieldRepo IFormDataFieldRepo = new FormDataFieldRepo();

        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(FormType obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<FormType>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<FormType> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<FormType>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(FormType obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<FormType>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<FormType> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<FormType>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public FormType DeleteData(Int32 Id)
        {
            Response responseFT = GetSingle(Id);
            if (responseFT.IsSuccess)
            {
                FormType obj = (FormType)responseFT.Data;
                try
                {
                    string query = obj.ObjectToQuery<FormType>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseFT = GetSingle(Id);
                    if (responseFT.IsSuccess)
                    {
                        FormType obj = (FormType)responseFT.Data;
                        query += obj.ObjectToQuery<FormType>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            FormType obj = new FormType();
            try
            {
                string query = "select * from form_types where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new FormType
                       {
                           Id = x.Field<Int32>("id"),
                           Code = x.Field<String>("code"),
                           Name = x.Field<String>("name"),
                           Status = x.Field<Int32>("status"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                string propertyName = nameof(FormDataField.FormTypeId);
                Response responseFDFList = IFormDataFieldRepo.GetDataByPropertyName(propertyName, obj.Id.ToString(), true, 0, 0, false);
                if (responseFDFList.IsSuccess)
                {
                    obj.FormTypeIdFormDataFields = (List<FormDataField>)responseFDFList.Data;
                }
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FormType> objs = new List<FormType>();
            try
            {
                string query = "select * from form_types";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FormType
                        {
                            Id = x.Field<Int32>("id"),
                            Code = x.Field<String>("code"),
                            Name = x.Field<String>("name"),
                            Status = x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                objs.ForEach(formDataType => {
                    //formDataField.FormTypeIdFormType = IFormTypeRepo.GetSingle(formDataField.FormTypeId);
                    string propertyName = nameof(FormDataField.FormTypeId);
                    Response responseFDFList = IFormDataFieldRepo.GetDataByPropertyName(propertyName, formDataType.Id.ToString(), true, 0, 0, false);
                    if (responseFDFList.IsSuccess)
                    {
                        formDataType.FormTypeIdFormDataFields = (List<FormDataField>)responseFDFList.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from form_types";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<FormType>(propertyName);
            List<FormType> objs = new List<FormType>();
            try
            {
                string query = "select * from form_types where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from form_types where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FormType
                        {
                            Id = x.Field<Int32>("id"),
                            Code = x.Field<String>("code"),
                            Name = x.Field<String>("name"),
                            Status = x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                objs.ForEach(formDataType => {
                    //formDataField.FormTypeIdFormType = IFormTypeRepo.GetSingle(formDataField.FormTypeId);
                    string propertyName1 = nameof(FormDataField.FormTypeId);
                    Response responseFDFList = IFormDataFieldRepo.GetDataByPropertyName(propertyName1, formDataType.Id.ToString(), true, 0, 0, false);
                    if (responseFDFList.IsSuccess)
                    {
                        formDataType.FormTypeIdFormDataFields = (List<FormDataField>)responseFDFList.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<FormType>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from form_types where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from form_types where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FormType> objs = new List<FormType>();
            try
            {
                string query = "select * from form_types where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FormType
                        {
                            Id = x.Field<Int32>("id"),
                            Code = x.Field<String>("code"),
                            Name = x.Field<String>("name"),
                            Status = x.Field<Int32>("status"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                objs.ForEach(formDataType => {
                    //formDataField.FormTypeIdFormType = IFormTypeRepo.GetSingle(formDataField.FormTypeId);
                    string propertyName = nameof(FormDataField.FormTypeId);
                    Response responseFDFList = IFormDataFieldRepo.GetDataByPropertyName(propertyName, formDataType.Id.ToString(), true, 0, 0, false);
                    if (responseFDFList.IsSuccess)
                    {
                        formDataType.FormTypeIdFormDataFields = (List<FormDataField>)responseFDFList.Data;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from form_types where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        
    }
}
