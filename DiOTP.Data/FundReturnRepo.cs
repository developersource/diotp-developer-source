using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class FundReturnRepo : IFundReturnRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(FundReturn obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<FundReturn>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<FundReturn> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<FundReturn>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(FundReturn obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<FundReturn>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<FundReturn> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<FundReturn>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public FundReturn DeleteData(Int32 Id)
        {
            Response responseFR = GetSingle(Id);
            if (responseFR.IsSuccess)
            {
                FundReturn obj = (FundReturn)responseFR.Data;
                try
                {
                    string query = obj.ObjectToQuery<FundReturn>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseFR = GetSingle(Id);
                    if (responseFR.IsSuccess)
                    {
                        FundReturn obj = (FundReturn)responseFR.Data;
                        query += obj.ObjectToQuery<FundReturn>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            FundReturn obj = new FundReturn();
            try
            {
                string query = "select * from fund_returns where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new FundReturn
                       {
                           Id = x.Field<Int32>("id"),
                           FundCode = x.Field<String>("fund_code"),
                           OneWeek = x.Field<Decimal>("one_week"),
                           OneMonth = x.Field<Decimal>("one_month"),
                           ThreeMonth = x.Field<Decimal>("three_month"),
                           SixMonth = x.Field<Decimal>("six_month"),
                           OneYear = x.Field<Decimal>("one_year"),
                           TwoYear = x.Field<Decimal>("two_year"),
                           ThreeYear = x.Field<Decimal>("three_year"),
                           FiveYear = x.Field<Decimal>("five_year"),
                           TenYear = x.Field<Decimal>("ten_year"),
                           UpdatedDate = x.Field<DateTime>("updated_date"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FundReturn> objs = new List<FundReturn>();
            try
            {
                string query = "select * from fund_returns";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FundReturn
                        {
                            Id = x.Field<Int32>("id"),
                            FundCode = x.Field<String>("fund_code"),
                            OneWeek = x.Field<Decimal>("one_week"),
                            OneMonth = x.Field<Decimal>("one_month"),
                            ThreeMonth = x.Field<Decimal>("three_month"),
                            SixMonth = x.Field<Decimal>("six_month"),
                            OneYear = x.Field<Decimal>("one_year"),
                            TwoYear = x.Field<Decimal>("two_year"),
                            ThreeYear = x.Field<Decimal>("three_year"),
                            FiveYear = x.Field<Decimal>("five_year"),
                            TenYear = x.Field<Decimal>("ten_year"),
                            UpdatedDate = x.Field<DateTime>("updated_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from fund_returns";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<FundReturn>(propertyName);
            List<FundReturn> objs = new List<FundReturn>();
            try
            {
                string query = "select * from fund_returns where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from fund_returns where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FundReturn
                        {
                            Id = x.Field<Int32>("id"),
                            FundCode = x.Field<String>("fund_code"),
                            OneWeek = x.Field<Decimal>("one_week"),
                            OneMonth = x.Field<Decimal>("one_month"),
                            ThreeMonth = x.Field<Decimal>("three_month"),
                            SixMonth = x.Field<Decimal>("six_month"),
                            OneYear = x.Field<Decimal>("one_year"),
                            TwoYear = x.Field<Decimal>("two_year"),
                            ThreeYear = x.Field<Decimal>("three_year"),
                            FiveYear = x.Field<Decimal>("five_year"),
                            TenYear = x.Field<Decimal>("ten_year"),
                            UpdatedDate = x.Field<DateTime>("updated_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<FundReturn>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from fund_returns where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from fund_returns where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<FundReturn> objs = new List<FundReturn>();
            try
            {
                string query = "select * from fund_returns where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new FundReturn
                        {
                            Id = x.Field<Int32>("id"),
                            FundCode = x.Field<String>("fund_code"),
                            OneWeek = x.Field<Decimal>("one_week"),
                            OneMonth = x.Field<Decimal>("one_month"),
                            ThreeMonth = x.Field<Decimal>("three_month"),
                            SixMonth = x.Field<Decimal>("six_month"),
                            OneYear = x.Field<Decimal>("one_year"),
                            TwoYear = x.Field<Decimal>("two_year"),
                            ThreeYear = x.Field<Decimal>("three_year"),
                            FiveYear = x.Field<Decimal>("five_year"),
                            TenYear = x.Field<Decimal>("ten_year"),
                            UpdatedDate = x.Field<DateTime>("updated_date"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from fund_returns where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        
    }
}
