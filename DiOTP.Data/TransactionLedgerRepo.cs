using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class TransactionLedgerRepo : ITransactionLedgerRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(TransactionLedger obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<TransactionLedger>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<TransactionLedger> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<TransactionLedger>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(TransactionLedger obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<TransactionLedger>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<TransactionLedger> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<TransactionLedger>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public TransactionLedger DeleteData(Int32 Id)
        {
            Response responseTL = GetSingle(Id);
            if (responseTL.IsSuccess)
            {
                TransactionLedger obj = (TransactionLedger)responseTL.Data;
                try
                {
                    string query = obj.ObjectToQuery<TransactionLedger>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseTL = GetSingle(Id);
                    if (responseTL.IsSuccess)
                    {
                        TransactionLedger obj = (TransactionLedger)responseTL.Data;
                        query += obj.ObjectToQuery<TransactionLedger>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            TransactionLedger obj = new TransactionLedger();
            try
            {
                string query = "select * from transaction_ledger where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new TransactionLedger
                       {
                           Id = x.Field<Int32>("id"),
                           UserId = x.Field<Int32>("user_id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                           OrderNo = x.Field<String>("order_no"),
                           TransNo = x.Field<String>("trans_no"),
                           TransactionCodeId = x.Field<Int32>("transaction_code_id"),
                           Amount = x.Field<Decimal>("amount"),
                           Units = x.Field<Decimal>("units"),
                           PaymentMethod = x.Field<Int32>("payment_method"),
                           OrderStatus = x.Field<Int32>("order_status"),
                           IsPrinted = x.Field<Int32>("is_printed"),
                           IsDownloaded = x.Field<Int32>("is_downloaded"),
                           Version = x.Field<Int32>("version"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<TransactionLedger> objs = new List<TransactionLedger>();
            try
            {
                string query = "select * from transaction_ledger";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new TransactionLedger
                        {
                            Id = x.Field<Int32>("id"),
                            UserId = x.Field<Int32>("user_id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            OrderNo = x.Field<String>("order_no"),
                            TransNo = x.Field<String>("trans_no"),
                            TransactionCodeId = x.Field<Int32>("transaction_code_id"),
                            Amount = x.Field<Decimal>("amount"),
                            Units = x.Field<Decimal>("units"),
                            PaymentMethod = x.Field<Int32>("payment_method"),
                            OrderStatus = x.Field<Int32>("order_status"),
                            IsPrinted = x.Field<Int32>("is_printed"),
                            IsDownloaded = x.Field<Int32>("is_downloaded"),
                            Version = x.Field<Int32>("version"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from transaction_ledger";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<TransactionLedger>(propertyName);
            List<TransactionLedger> objs = new List<TransactionLedger>();
            try
            {
                string query = "select * from transaction_ledger where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from transaction_ledger where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new TransactionLedger
                        {
                            Id = x.Field<Int32>("id"),
                            UserId = x.Field<Int32>("user_id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            OrderNo = x.Field<String>("order_no"),
                            TransNo = x.Field<String>("trans_no"),
                            TransactionCodeId = x.Field<Int32>("transaction_code_id"),
                            Amount = x.Field<Decimal>("amount"),
                            Units = x.Field<Decimal>("units"),
                            PaymentMethod = x.Field<Int32>("payment_method"),
                            OrderStatus = x.Field<Int32>("order_status"),
                            IsPrinted = x.Field<Int32>("is_printed"),
                            IsDownloaded = x.Field<Int32>("is_downloaded"),
                            Version = x.Field<Int32>("version"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<TransactionLedger>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from transaction_ledger where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from transaction_ledger where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<TransactionLedger> objs = new List<TransactionLedger>();
            try
            {
                string query = "select * from transaction_ledger where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new TransactionLedger
                        {
                            Id = x.Field<Int32>("id"),
                            UserId = x.Field<Int32>("user_id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            OrderNo = x.Field<String>("order_no"),
                            TransNo = x.Field<String>("trans_no"),
                            TransactionCodeId = x.Field<Int32>("transaction_code_id"),
                            Amount = x.Field<Decimal>("amount"),
                            Units = x.Field<Decimal>("units"),
                            PaymentMethod = x.Field<Int32>("payment_method"),
                            OrderStatus = x.Field<Int32>("order_status"),
                            IsPrinted = x.Field<Int32>("is_printed"),
                            IsDownloaded = x.Field<Int32>("is_downloaded"),
                            Version = x.Field<Int32>("version"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from transaction_ledger where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        
    }
}
