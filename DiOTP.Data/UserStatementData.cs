﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility;

namespace DiOTP.Data
{
    public class UserStatementData
    {
        public static UserStatements Insert(UserStatements us)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string sql = @"INSERT INTO user_statements(
                            ID,
                            user_id,
                            user_account_id,
                            user_statement_category_id,
                            name,
                            url,
                            created_date, 
                            statement_date, 
                            status) VALUES(
                            @ID,
                            @user_id,
                            @user_account_id,
                            @user_statement_category_id,
                            @name,
                            @url,
                            @created_date, 
                            @statement_date, 
                            @status)";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@ID", us.id);
            cmd.Parameters.AddWithValue("@user_id", us.user_id);
            cmd.Parameters.AddWithValue("@user_account_id", us.user_account_id);
            cmd.Parameters.AddWithValue("@user_statement_category_id", us.user_statment_category_id);
            cmd.Parameters.AddWithValue("@name", us.name);
            cmd.Parameters.AddWithValue("@url", us.url);
            cmd.Parameters.AddWithValue("@created_date", us.createdate);
            cmd.Parameters.AddWithValue("@statement_date", us.statementdate);
            cmd.Parameters.AddWithValue("@status", us.status);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return us;
        }

        public static List<UserStatements> USGetAll()
        {
            MySqlConnection con = DBconnection.GetConnection();

            con.Open();
            string query = @"select * from user_statements";
            MySqlCommand cmd = new MySqlCommand(query, con);
            MySqlDataReader mdr = cmd.ExecuteReader();

            List<UserStatements> uslist = new List<UserStatements>();

            while (mdr.Read())
            {
                UserStatements us = new UserStatements()
                {
                    id = Convert.ToInt32(mdr["ID"].ToString()),
                    user_id = Convert.ToInt32(mdr["user_id"].ToString()),
                    user_account_id = Convert.ToInt32(mdr["user_account_id"].ToString()),
                    user_statment_category_id = Convert.ToInt32(mdr["user_statement_category_id"].ToString()),
                    name = mdr["name"].ToString(),
                    url = mdr["url"].ToString(),
                    createdate = Convert.ToDateTime(mdr["created_date"].ToString()),
                    statementdate = Convert.ToDateTime(mdr["statement_date"].ToString()),
                    status = Convert.ToInt32(mdr["status"].ToString())
                };
                uslist.Add(us);
            }
            con.Close();
            return uslist;
        }

        public static UserStatements GetByid(string id)
        {
            MySqlConnection conn = DBconnection.GetConnection();

            string query = @"select * from user_statements where account_no = @no";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@no", id);
            conn.Open();

            MySqlDataReader mdr = cmd.ExecuteReader();
            UserStatements us = new UserStatements();

            if (mdr.Read())
            {
                us.id = Convert.ToInt32(mdr["ID"].ToString());
                us.user_id = Convert.ToInt32(mdr["user_id"].ToString());
                us.user_account_id = Convert.ToInt32(mdr["user_account_id"].ToString());
                us.user_statment_category_id = Convert.ToInt32(mdr["user_statment_category_id"].ToString());
                us.name = mdr["name"].ToString();
                us.url = mdr["url"].ToString();
                us.createdate = Convert.ToDateTime(mdr["created_date"].ToString());
                us.statementdate = Convert.ToDateTime(mdr["statement_date"].ToString());
                us.status = Convert.ToInt32(mdr["status"].ToString());
            }
            conn.Close();
            return us;
        }
        
    }
}
