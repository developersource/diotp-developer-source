using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class UtmcFundCorporateActionRepo : IUtmcFundCorporateActionRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(UtmcFundCorporateAction obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcFundCorporateAction>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UtmcFundCorporateAction> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcFundCorporateAction>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UtmcFundCorporateAction obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcFundCorporateAction>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UtmcFundCorporateAction> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcFundCorporateAction>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UtmcFundCorporateAction DeleteData(Int32 Id)
        {
            Response responseUFCA = GetSingle(Id);
            if (responseUFCA.IsSuccess)
            {
                UtmcFundCorporateAction obj = (UtmcFundCorporateAction)responseUFCA.Data;
                try
                {
                    string query = obj.ObjectToQuery<UtmcFundCorporateAction>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                    Response responseUFCA = GetSingle(Id);
                    if (responseUFCA.IsSuccess)
                    {
                        UtmcFundCorporateAction obj = (UtmcFundCorporateAction)responseUFCA.Data;
                        query += obj.ObjectToQuery<UtmcFundCorporateAction>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UtmcFundCorporateAction obj = new UtmcFundCorporateAction();
            try
            {
                string query = "select * from utmc_fund_corporate_actions where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new UtmcFundCorporateAction
                       {
                           Id = x.Field<Int32>("id"),
                           EpfIpdCode = x.Field<String>("epf_ipd_code"),
                           IpdFundCode = x.Field<String>("ipd_fund_code"),
                           CorporateActionDate = x.Field<DateTime>("corporate_action_date"),
                           Distributions = x.Field<Decimal>("distributions"),
                           UnitSplits = x.Field<Decimal?>("unit_splits") == null ? 0 : x.Field<Decimal?>("unit_splits"),
                           ReportDate = x.Field<DateTime>("report_date"),
                           NetDistribution = x.Field<Decimal>("net_distribution"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundCorporateAction> objs = new List<UtmcFundCorporateAction>();
            try
            {
                string query = "select * from utmc_fund_corporate_actions";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcFundCorporateAction
                        {
                            Id = x.Field<Int32>("id"),
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            CorporateActionDate = x.Field<DateTime>("corporate_action_date"),
                            Distributions = x.Field<Decimal>("distributions"),
                            UnitSplits = x.Field<Decimal?>("unit_splits") == null ? 0 : x.Field<Decimal?>("unit_splits"),
                            ReportDate = x.Field<DateTime>("report_date"),
                            NetDistribution = x.Field<Decimal>("net_distribution"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_fund_corporate_actions";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<UtmcFundCorporateAction>(propertyName);
            List<UtmcFundCorporateAction> objs = new List<UtmcFundCorporateAction>();
            try
            {
                string query = "select * from utmc_fund_corporate_actions where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from utmc_fund_corporate_actions where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcFundCorporateAction
                        {
                            Id = x.Field<Int32>("id"),
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            CorporateActionDate = x.Field<DateTime>("corporate_action_date"),
                            Distributions = x.Field<Decimal>("distributions"),
                            UnitSplits = x.Field<Decimal?>("unit_splits") == null ? 0 : x.Field<Decimal?>("unit_splits"),
                            ReportDate = x.Field<DateTime>("report_date"),
                            NetDistribution = x.Field<Decimal>("net_distribution"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<UtmcFundCorporateAction>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_fund_corporate_actions where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from utmc_fund_corporate_actions where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundCorporateAction> objs = new List<UtmcFundCorporateAction>();
            try
            {
                string query = "select * from utmc_fund_corporate_actions where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcFundCorporateAction
                        {
                            Id = x.Field<Int32>("id"),
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            CorporateActionDate = x.Field<DateTime>("corporate_action_date"),
                            Distributions = x.Field<Decimal>("distributions"),
                            UnitSplits = x.Field<Decimal?>("unit_splits") == null ? 0 : x.Field<Decimal?>("unit_splits"),
                            ReportDate = x.Field<DateTime>("report_date"),
                            NetDistribution = x.Field<Decimal>("net_distribution"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_fund_corporate_actions where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
    }
}
