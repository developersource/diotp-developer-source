using DiOTP.Utility.Helper;
using DiOTP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Data.IRepo
{
    public interface IDvDistributionIntructionRepo : IDefaultInterface<DvDistributionIntruction>
    {
    }
}
