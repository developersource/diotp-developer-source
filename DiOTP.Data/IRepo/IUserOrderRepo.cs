using DiOTP.Utility.Helper;
using DiOTP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Data.IRepo
{
    public interface IUserOrderRepo : IDefaultInterface<UserOrder>
    {
        Int32 GetCountByColumnGroup(string propertyName);
        Int32 GetCountByColumnGroup(string filter, string groupByColumnName);
    }
}
