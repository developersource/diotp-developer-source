using DiOTP.Utility.Helper;
using DiOTP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data.IRepo
{
    public interface IUserRepo : IDefaultInterface<User>
    {
        Response GetDataByMA(string filter, int skip, int take, bool isOrderByDesc);
        Int32 GetCountByMA(string filter);
        Response GetDataByUT(string filter, int skip, int take, bool isOrderByDesc, string jfilter, string jtable);
        Int32 GetCountByUT(string filter, string jfilter, string jtable);
    }
}
