using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class UserOrderCartRepo : IUserOrderCartRepo
    {
        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(UserOrderCart obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UserOrderCart>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                obj.Id = Convert.ToInt32(cmd.LastInsertedId);
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UserOrderCart> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UserOrderCart>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UserOrderCart obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UserOrderCart>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UserOrderCart> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UserOrderCart>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UserOrderCart DeleteData(Int32 Id)
        {
            Response responseUOC = GetSingle(Id);
            if (responseUOC.IsSuccess)
            {
                UserOrderCart obj = (UserOrderCart)responseUOC.Data;
                try
                {
                    string query = obj.ObjectToQuery<UserOrderCart>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                Response responseUOC = GetSingle(Id);
                    if (responseUOC.IsSuccess)
                    {
                        UserOrderCart obj = (UserOrderCart)responseUOC.Data;
                        query += obj.ObjectToQuery<UserOrderCart>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UserOrderCart obj = new UserOrderCart();
            try
            {
                string query = "select * from user_order_carts where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new UserOrderCart
                       {
                           Id = x.Field<Int32>("id"),
                           RefNo = x.Field<String>("ref_no"),
                           OrderType = x.Field<Int32>("order_type"),
                           FundId = x.Field<Int32>("fund_id"),
                           ToFundId = x.Field<Int32>("to_fund_id"),
                           ToAccountId = x.Field<Int32>("to_account_id"),
                           UserId = x.Field<Int32>("user_id"),
                           UserAccountId = x.Field<Int32>("user_account_id"),
                           OrderNo = x.Field<String>("order_no"),
                           PaymentMethod = x.Field<String>("payment_method"),
                           Amount = x.Field<Decimal>("amount"),
                           Units = x.Field<Decimal>("units"),
                           TransId = x.Field<Int32>("trans_id"),
                           TransNo = x.Field<String>("trans_no"),
                           CreatedBy = x.Field<Int32?>("created_by") == null ? 0 : x.Field<Int32>("created_by"),
                           CreatedDate = x.Field<DateTime>("created_date"),
                           UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                           UpdatedDate = x.Field<DateTime?>("updated_date"),
                           Status = x.Field<Int32>("status"),
                           ConsultantId = x.Field<String>("consultant_id"),
                           DistributionId = x.Field<Int32>("distribution_id"),
                           DistributionBankId = x.Field<Int32>("distribution_bank_id"),
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserOrderCart> objs = new List<UserOrderCart>();
            try
            {
                string query = "select * from user_order_carts";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UserOrderCart
                        {
                            Id = x.Field<Int32>("id"),
                            RefNo = x.Field<String>("ref_no"),
                            OrderType = x.Field<Int32>("order_type"),
                            FundId = x.Field<Int32>("fund_id"),
                            ToFundId = x.Field<Int32>("to_fund_id"),
                            ToAccountId = x.Field<Int32>("to_account_id"),
                            UserId = x.Field<Int32>("user_id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            OrderNo = x.Field<String>("order_no"),
                            PaymentMethod = x.Field<String>("payment_method"),
                            Amount = x.Field<Decimal>("amount"),
                            Units = x.Field<Decimal>("units"),
                            TransId = x.Field<Int32>("trans_id"),
                            TransNo = x.Field<String>("trans_no"),
                            CreatedBy = x.Field<Int32?>("created_by") == null ? 0 : x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                            UpdatedDate = x.Field<DateTime?>("updated_date"),
                            Status = x.Field<Int32>("status"),
                            ConsultantId = x.Field<String>("consultant_id"),
                            DistributionId = x.Field<Int32>("distribution_id"),
                           DistributionBankId = x.Field<Int32>("distribution_bank_id"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from user_order_carts";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<UserOrderCart>(propertyName);
            List<UserOrderCart> objs = new List<UserOrderCart>();
            try
            {
                string query = "select * from user_order_carts where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from user_order_carts where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UserOrderCart
                        {
                            Id = x.Field<Int32>("id"),
                            RefNo = x.Field<String>("ref_no"),
                            OrderType = x.Field<Int32>("order_type"),
                            FundId = x.Field<Int32>("fund_id"),
                            ToFundId = x.Field<Int32>("to_fund_id"),
                            ToAccountId = x.Field<Int32>("to_account_id"),
                            UserId = x.Field<Int32>("user_id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            OrderNo = x.Field<String>("order_no"),
                            PaymentMethod = x.Field<String>("payment_method"),
                            Amount = x.Field<Decimal>("amount"),
                            Units = x.Field<Decimal>("units"),
                            TransId = x.Field<Int32>("trans_id"),
                            TransNo = x.Field<String>("trans_no"),
                            CreatedBy = x.Field<Int32?>("created_by") == null ? 0 : x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                            UpdatedDate = x.Field<DateTime?>("updated_date"),
                            Status = x.Field<Int32>("status"),
                            ConsultantId = x.Field<String>("consultant_id"),
                            DistributionId = x.Field<Int32>("distribution_id"),
                           DistributionBankId = x.Field<Int32>("distribution_bank_id"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<UserOrderCart>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from user_order_carts where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from user_order_carts where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UserOrderCart> objs = new List<UserOrderCart>();
            try
            {
                string query = "select * from user_order_carts where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UserOrderCart
                        {
                            Id = x.Field<Int32>("id"),
                            RefNo = x.Field<String>("ref_no"),
                            OrderType = x.Field<Int32>("order_type"),
                            FundId = x.Field<Int32>("fund_id"),
                            ToFundId = x.Field<Int32>("to_fund_id"),
                            ToAccountId = x.Field<Int32>("to_account_id"),
                            UserId = x.Field<Int32>("user_id"),
                            UserAccountId = x.Field<Int32>("user_account_id"),
                            OrderNo = x.Field<String>("order_no"),
                            PaymentMethod = x.Field<String>("payment_method"),
                            Amount = x.Field<Decimal>("amount"),
                            Units = x.Field<Decimal>("units"),
                            TransId = x.Field<Int32>("trans_id"),
                            TransNo = x.Field<String>("trans_no"),
                            CreatedBy = x.Field<Int32?>("created_by") == null ? 0 : x.Field<Int32>("created_by"),
                            CreatedDate = x.Field<DateTime>("created_date"),
                            UpdatedBy = x.Field<Int32?>("updated_by") == null ? 0 : x.Field<Int32>("updated_by"),
                            UpdatedDate = x.Field<DateTime?>("updated_date"),
                            Status = x.Field<Int32>("status"),
                            ConsultantId = x.Field<String>("consultant_id"),
                            DistributionId = x.Field<Int32>("distribution_id"),
                           DistributionBankId = x.Field<Int32>("distribution_bank_id"),
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from user_order_carts where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        

        public Boolean DeleteDataDB(int Id)
        {
            bool isDel = false;
            try
            {
                string query = "Delete from user_order_carts where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                isDel = true;
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return isDel;
        }
    }
}
