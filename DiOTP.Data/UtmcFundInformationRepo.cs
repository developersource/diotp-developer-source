using DiOTP.Data.IRepo;
using DiOTP.Utility;
using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data
{
    public class UtmcFundInformationRepo : IUtmcFundInformationRepo
    {
        IUtmcFundDetailRepo IUtmcFundDetailRepo = new UtmcFundDetailRepo();
        IUtmcFundChargeRepo IUtmcFundChargeRepo = new UtmcFundChargeRepo();
        IUtmcFundFileRepo IUtmcFundFileRepo = new UtmcFundFileRepo();
        IUtmcFundCategoriesDefRepo IUtmcFundCategoriesDefRepo = new UtmcFundCategoriesDefRepo();

        MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        public Response PostData(UtmcFundInformation obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcFundInformation>("insert");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                obj.Id = Convert.ToInt32(cmd.LastInsertedId);
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 PostBulkData(List<UtmcFundInformation> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcFundInformation>("insert");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response UpdateData(UtmcFundInformation obj)
        {
            Response response = new Response();
            try
            {
                string query = obj.ObjectToQuery<UtmcFundInformation>("update");
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 UpdateBulkData(List<UtmcFundInformation> objs)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                objs.ForEach(obj =>
                {
                    query += obj.ObjectToQuery<UtmcFundInformation>("update");
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public UtmcFundInformation DeleteData(Int32 Id)
        {
            Response responseUFI = GetSingle(Id);
            if (responseUFI.IsSuccess)
            {
                UtmcFundInformation obj = (UtmcFundInformation)responseUFI.Data;
                try
                {
                    string query = obj.ObjectToQuery<UtmcFundInformation>("update");
                    MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                    mySQLDBConnect.OpenConnection();
                    cmd.ExecuteNonQuery();
                    mySQLDBConnect.CloseConnection();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return obj;
            }
            return null;
        }
        public Int32 DeleteBulkData(List<Int32> Ids)
        {
            Int32 result = 0;
            try
            {
                string query = "";
                Ids.ForEach(Id =>
                {
                    Response responseUFI = GetSingle(Id);
                    if (responseUFI.IsSuccess)
                    {
                        UtmcFundInformation obj = (UtmcFundInformation)responseUFI.Data;
                        query += obj.ObjectToQuery<UtmcFundInformation>("update");
                    }
                });
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                result = cmd.ExecuteNonQuery();
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return result;
        }
        public Response GetSingle(Int32 Id)
        {
            Response response = new Response();
            UtmcFundInformation obj = new UtmcFundInformation();
            try
            {
                string query = "select * from utmc_fund_information where ID = @Id ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@Id", Id);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = (from x in dt.AsEnumerable()
                       select new UtmcFundInformation
                       {
                           Id = x.Field<Int32>("id"),
                           EpfIpdCode = x.Field<String>("epf_ipd_code"),
                           IpdFundCode = x.Field<String>("ipd_fund_code"),
                           FundCode = x.Field<String>("fund_code"),
                           FundName = x.Field<String>("fund_name"),
                           UtmcFundCategoriesDefId = x.Field<Int32>("utmc_fund_categories_def_id"),
                           EffectiveDate = x.Field<DateTime>("effective_date"),
                           LipperCategoryOfFund = x.Field<String>("lipper_category_of_fund"),
                           Conventional = x.Field<String>("conventional"),
                           Status = x.Field<String>("status"),
                           ForeignFund = x.Field<String>("foreign_fund"),
                           ReportDate = x.Field<DateTime>("report_date"),
                           FundBaseCurrency = x.Field<String>("fund_base_currency"),
                           IsEmis = x.Field<Int32?>("is_emis") == null ? 0 : x.Field<Int32>("is_emis"),
                           FundCls = x.Field<String>("fund_cls"),
                           IsRetail = x.Field<Int32?>("is_retail") == null ? 0 : x.Field<Int32>("is_retail"),
                           SatGroup = x.Field<String>("sat_group"),
                           IsRsp = x.Field<Int32?>("is_rsp") == null ? 0 : x.Field<Int32>("is_rsp")
                       }).ToList().FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                string propertyName = nameof(UtmcFundDetail.UtmcFundInformationId);
                Response responseUFDList = IUtmcFundDetailRepo.GetDataByPropertyName(propertyName, obj.Id.ToString(), true, 0, 0, false);
                if (responseUFDList.IsSuccess)
                {
                    obj.UtmcFundInformationIdUtmcFundDetails = (List<UtmcFundDetail>)responseUFDList.Data;
                }
                else
                {
                    response = responseUFDList;
                    return response;
                }
                Response responseUFCList = IUtmcFundChargeRepo.GetDataByPropertyName(propertyName, obj.Id.ToString(), true, 0, 0, false);
                if (responseUFCList.IsSuccess)
                {
                    obj.UtmcFundInformationIdUtmcFundCharges = (List<UtmcFundCharge>)responseUFCList.Data;
                }
                else
                {
                    response = responseUFCList;
                    return response;
                }
                Response responseUFFList = IUtmcFundFileRepo.GetDataByPropertyName(propertyName, obj.Id.ToString(), true, 0, 0, false);
                if (responseUFFList.IsSuccess)
                {
                    obj.UtmcFundInformationIdUtmcFundFiles = (List<UtmcFundFile>)responseUFFList.Data;
                }
                else
                {
                    response = responseUFFList;
                    return response;
                }
                Response responseFCD = IUtmcFundCategoriesDefRepo.GetSingle(obj.UtmcFundCategoriesDefId);
                if (responseFCD.IsSuccess)
                {
                    obj.UtmcFundCategoriesDefIdUtmcFundCategoriesDef = (UtmcFundCategoriesDef)responseFCD.Data;
                }
                else
                {
                    response = responseFCD;
                    return response;
                }
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Response GetData(int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundInformation> objs = new List<UtmcFundInformation>();
            try
            {
                string query = "select * from utmc_fund_information";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcFundInformation
                        {
                            Id = x.Field<Int32>("id"),
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            FundCode = x.Field<String>("fund_code"),
                            FundName = x.Field<String>("fund_name"),
                            UtmcFundCategoriesDefId = x.Field<Int32>("utmc_fund_categories_def_id"),
                            EffectiveDate = x.Field<DateTime>("effective_date"),
                            LipperCategoryOfFund = x.Field<String>("lipper_category_of_fund"),
                            Conventional = x.Field<String>("conventional"),
                            Status = x.Field<String>("status"),
                            ForeignFund = x.Field<String>("foreign_fund"),
                            ReportDate = x.Field<DateTime>("report_date"),
                            FundBaseCurrency = x.Field<String>("fund_base_currency"),
                            IsEmis = x.Field<Int32?>("is_emis") == null ? 0 : x.Field<Int32>("is_emis"),
                            FundCls = x.Field<String>("fund_cls"),
                            IsRetail = x.Field<Int32?>("is_retail") == null ? 0 : x.Field<Int32>("is_retail"),
                            SatGroup = x.Field<String>("sat_group"),
                            IsRsp = x.Field<Int32?>("is_rsp") == null ? 0 : x.Field<Int32>("is_rsp")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                objs.ForEach(utmcFundInfo =>
                {
                    string propertyName = nameof(UtmcFundDetail.UtmcFundInformationId);
                    Response responseUFDList = IUtmcFundDetailRepo.GetDataByPropertyName(propertyName, utmcFundInfo.Id.ToString(), true, 0, 0, false);
                    if (responseUFDList.IsSuccess)
                    {
                        utmcFundInfo.UtmcFundInformationIdUtmcFundDetails = (List<UtmcFundDetail>)responseUFDList.Data;
                    }
                    else
                    {
                        response = responseUFDList;
                    }
                    Response responseUFCList = IUtmcFundChargeRepo.GetDataByPropertyName(propertyName, utmcFundInfo.Id.ToString(), true, 0, 0, false);
                    if (responseUFCList.IsSuccess)
                    {
                        utmcFundInfo.UtmcFundInformationIdUtmcFundCharges = (List<UtmcFundCharge>)responseUFCList.Data;
                    }
                    else
                    {
                        response = responseUFCList;
                    }
                    Response responseUFFList = IUtmcFundFileRepo.GetDataByPropertyName(propertyName, utmcFundInfo.Id.ToString(), true, 0, 0, false);
                    if (responseUFFList.IsSuccess)
                    {
                        utmcFundInfo.UtmcFundInformationIdUtmcFundFiles = (List<UtmcFundFile>)responseUFFList.Data;
                    }
                    else
                    {
                        response = responseUFFList;
                    }
                    Response responseFCD = IUtmcFundCategoriesDefRepo.GetSingle(utmcFundInfo.UtmcFundCategoriesDefId);
                    if (responseFCD.IsSuccess)
                    {
                        utmcFundInfo.UtmcFundCategoriesDefIdUtmcFundCategoriesDef = (UtmcFundCategoriesDef)responseFCD.Data;
                    }
                    else
                    {
                        response = responseFCD;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCount()
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_fund_information";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByPropertyName(string propertyName, string propertyValue, bool isEqual, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            string columnName = Converter.GetColumnNameByPropertyName<UtmcFundInformation>(propertyName);
            List<UtmcFundInformation> objs = new List<UtmcFundInformation>();
            try
            {
                string query = "select * from utmc_fund_information where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select * from utmc_fund_information where " + columnName + " != @propertyValue ";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcFundInformation
                        {
                            Id = x.Field<Int32>("id"),
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            FundCode = x.Field<String>("fund_code"),
                            FundName = x.Field<String>("fund_name"),
                            UtmcFundCategoriesDefId = x.Field<Int32>("utmc_fund_categories_def_id"),
                            EffectiveDate = x.Field<DateTime>("effective_date"),
                            LipperCategoryOfFund = x.Field<String>("lipper_category_of_fund"),
                            Conventional = x.Field<String>("conventional"),
                            Status = x.Field<String>("status"),
                            ForeignFund = x.Field<String>("foreign_fund"),
                            ReportDate = x.Field<DateTime>("report_date"),
                            FundBaseCurrency = x.Field<String>("fund_base_currency"),
                            IsEmis = x.Field<Int32?>("is_emis") == null ? 0 : x.Field<Int32>("is_emis"),
                            FundCls = x.Field<String>("fund_cls"),
                            IsRetail = x.Field<Int32?>("is_retail") == null ? 0 : x.Field<Int32>("is_retail"),
                            SatGroup = x.Field<String>("sat_group"),
                            IsRsp = x.Field<Int32?>("is_rsp") == null ? 0 : x.Field<Int32>("is_rsp")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                objs.ForEach(utmcFundInfo =>
                {
                    string propertyName1 = nameof(UtmcFundDetail.UtmcFundInformationId);
                    Response responseUFDList = IUtmcFundDetailRepo.GetDataByPropertyName(propertyName1, utmcFundInfo.Id.ToString(), true, 0, 0, false);
                    if (responseUFDList.IsSuccess)
                    {
                        utmcFundInfo.UtmcFundInformationIdUtmcFundDetails = (List<UtmcFundDetail>)responseUFDList.Data;
                    }
                    else
                    {
                        response = responseUFDList;
                    }
                    Response responseUFCList = IUtmcFundChargeRepo.GetDataByPropertyName(propertyName1, utmcFundInfo.Id.ToString(), true, 0, 0, false);
                    if (responseUFCList.IsSuccess)
                    {
                        utmcFundInfo.UtmcFundInformationIdUtmcFundCharges = (List<UtmcFundCharge>)responseUFCList.Data;
                    }
                    else
                    {
                        response = responseUFCList;
                    }
                    Response responseUFFList = IUtmcFundFileRepo.GetDataByPropertyName(propertyName1, utmcFundInfo.Id.ToString(), true, 0, 0, false);
                    if (responseUFFList.IsSuccess)
                    {
                        utmcFundInfo.UtmcFundInformationIdUtmcFundFiles = (List<UtmcFundFile>)responseUFFList.Data;
                    }
                    else
                    {
                        response = responseUFFList;
                    }
                    Response responseFCD = IUtmcFundCategoriesDefRepo.GetSingle(utmcFundInfo.UtmcFundCategoriesDefId);
                    if (responseFCD.IsSuccess)
                    {
                        utmcFundInfo.UtmcFundCategoriesDefIdUtmcFundCategoriesDef = (UtmcFundCategoriesDef)responseFCD.Data;
                    }
                    else
                    {
                        response = responseFCD;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByPropertyName(string propertyName, string propertyValue, bool isEqual)
        {
            string columnName = Converter.GetColumnNameByPropertyName<UtmcFundInformation>(propertyName);
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_fund_information where " + columnName + " = @propertyValue ";
                if (!isEqual)
                    query = "select count(*) from utmc_fund_information where " + columnName + " != @propertyValue ";
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                cmd.Parameters.AddWithValue("@propertyValue", propertyValue);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }
        public Response GetDataByFilter(string filter, int skip, int take, bool isOrderByDesc)
        {
            Response response = new Response();
            List<UtmcFundInformation> objs = new List<UtmcFundInformation>();
            try
            {
                string query = "select * from utmc_fund_information where " + filter + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = (from x in dt.AsEnumerable()
                        select new UtmcFundInformation
                        {
                            Id = x.Field<Int32>("id"),
                            EpfIpdCode = x.Field<String>("epf_ipd_code"),
                            IpdFundCode = x.Field<String>("ipd_fund_code"),
                            FundCode = x.Field<String>("fund_code"),
                            FundName = x.Field<String>("fund_name"),
                            UtmcFundCategoriesDefId = x.Field<Int32>("utmc_fund_categories_def_id"),
                            EffectiveDate = x.Field<DateTime>("effective_date"),
                            LipperCategoryOfFund = x.Field<String>("lipper_category_of_fund"),
                            Conventional = x.Field<String>("conventional"),
                            Status = x.Field<String>("status"),
                            ForeignFund = x.Field<String>("foreign_fund"),
                            ReportDate = x.Field<DateTime>("report_date"),
                            FundBaseCurrency = x.Field<String>("fund_base_currency"),
                            IsEmis = x.Field<Int32?>("is_emis") == null ? 0 : x.Field<Int32>("is_emis"),
                            FundCls = x.Field<String>("fund_cls"),
                            IsRetail = x.Field<Int32?>("is_retail") == null ? 0 : x.Field<Int32>("is_retail"),
                            SatGroup = x.Field<String>("sat_group"),
                            IsRsp = x.Field<Int32?>("is_rsp") == null ? 0 : x.Field<Int32>("is_rsp")
                        }).ToList();
                mySQLDBConnect.CloseConnection();
                objs.ForEach(utmcFundInfo =>
                {
                    string propertyName = nameof(UtmcFundDetail.UtmcFundInformationId);
                    Response responseUFDList = IUtmcFundDetailRepo.GetDataByPropertyName(propertyName, utmcFundInfo.Id.ToString(), true, 0, 0, false);
                    if (responseUFDList.IsSuccess)
                    {
                        utmcFundInfo.UtmcFundInformationIdUtmcFundDetails = (List<UtmcFundDetail>)responseUFDList.Data;
                    }
                    else
                    {
                        response = responseUFDList;
                    }
                    Response responseUFCList = IUtmcFundChargeRepo.GetDataByPropertyName(propertyName, utmcFundInfo.Id.ToString(), true, 0, 0, false);
                    if (responseUFCList.IsSuccess)
                    {
                        utmcFundInfo.UtmcFundInformationIdUtmcFundCharges = (List<UtmcFundCharge>)responseUFCList.Data;
                    }
                    else
                    {
                        response = responseUFCList;
                    }
                    Response responseUFFList = IUtmcFundFileRepo.GetDataByPropertyName(propertyName, utmcFundInfo.Id.ToString(), true, 0, 0, false);
                    if (responseUFFList.IsSuccess)
                    {
                        utmcFundInfo.UtmcFundInformationIdUtmcFundFiles = (List<UtmcFundFile>)responseUFFList.Data;
                    }
                    else
                    {
                        response = responseUFFList;
                    }
                    Response responseFCD = IUtmcFundCategoriesDefRepo.GetSingle(utmcFundInfo.UtmcFundCategoriesDefId);
                    if (responseFCD.IsSuccess)
                    {
                        utmcFundInfo.UtmcFundCategoriesDefIdUtmcFundCategoriesDef = (UtmcFundCategoriesDef)responseFCD.Data;
                    }
                    else
                    {
                        response = responseFCD;
                    }
                });
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
        public Int32 GetCountByFilter(string filter)
        {
            Int32 count = 0;
            try
            {
                string query = "select count(*) from utmc_fund_information where " + filter;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                count = Convert.ToInt32(cmd.ExecuteScalar());
                mySQLDBConnect.CloseConnection();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return count;
        }

    }
}
