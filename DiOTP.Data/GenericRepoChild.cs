﻿using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.CustomClasses;
using DiOTP.Utility.Helper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Data
{
    public class GenericRepoChild
    {
        static MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();
        static Response response = new Response();

        public static Response GetSingle(Type type, Int32 Id)
        {
            dynamic obj = Activator.CreateInstance(type);
            string databaseTableName = Converter.GetDatabaseName(type);
            try
            {
                string query = "select * from " + databaseTableName + " where ID=" + Id;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                obj = dt.ToDynamicObject(type).FirstOrDefault();
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Response GetData(Type type, int skip, int take, bool isOrderByDesc)
        {
            dynamic objs;
            string databaseTableName = Converter.GetDatabaseName(type);
            try
            {
                string query = "select * from " + databaseTableName + "";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = dt.ToDynamicObject(type);
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }

        public static Response GetDataByProperty(Type type, string propName, string value, int skip, int take, bool isOrderByDesc)
        {
            dynamic objs;
            string databaseTableName = Converter.GetDatabaseName(type);

            string columnName = Converter.GetColumnNameByPropertyName(type, propName);
            try
            {
                string query = "select * from " + databaseTableName + " where " + columnName + "='" + value + "'";
                if (isOrderByDesc)
                    query += " order by ID desc";
                if (take != 0)
                    query += " limit " + skip + ", " + take;
                MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
                mySQLDBConnect.OpenConnection();
                DataTable dt = new DataTable();
                MySqlDataAdapter dA = new MySqlDataAdapter(cmd);
                dA.Fill(dt);
                objs = dt.ToDynamicObject(type);
                mySQLDBConnect.CloseConnection();
                response.IsSuccess = true;
                response.Data = objs;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                Console.Write(ex.ToString());
            }
            return response;
        }
    }
}
