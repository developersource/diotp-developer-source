﻿using DiOTP.Data.MySQLDBRef;
using DiOTP.Utility.CustomClasses;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiOTP.Data
{
    public class GenerateStatementRepo
    {
        public static List<SummaryPortfolio> GetAll(DateTime startDate, DateTime endDate, string accountNo)
        {
            MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();

            string date1 = startDate.ToString("yyyy-MM-dd");
            string date2 = endDate.ToString("yyyy-MM-dd");

            string query = @"SELECT B.Fund_Name,
                                    B.Fund_Code,
                                    A.Net_Cumulative_Closing_Balance_Units,
                                    A.Market_Price_NAV,
                                    (A.Net_Cumulative_Closing_Balance_Units* A.Market_Price_NAV) AS Current_Market_Value
                                    FROM utmc_compositional_investment A
                                    inner join utmc_fund_information B
                                    on A.IPD_Fund_Code=B.IPD_Fund_Code where 
                                    A.Report_Date>='" + date1 + "' and A.Report_Date<='" + date2 + "' AND A.IPD_Member_Acc_No='" + accountNo + "' group by B.Fund_Name";
            //A.Report_Date>='2017-08-31' and  A.Report_Date <= '2017-08-31'

            MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
            mySQLDBConnect.OpenConnection();
            MySqlDataReader mdr = cmd.ExecuteReader();
            List<SummaryPortfolio> summaryPortfolioList = new List<SummaryPortfolio>();
            while (mdr.Read())
            {
                SummaryPortfolio summaryPortfolio = new SummaryPortfolio();
                summaryPortfolio.Fund_Name = mdr["Fund_Name"].ToString();
                summaryPortfolio.Fund_Code = mdr["Fund_Code"].ToString();
                summaryPortfolio.Net_Cumulative_Closing_Balance_Units = Convert.ToDecimal(mdr["Net_Cumulative_Closing_Balance_Units"].ToString());
                summaryPortfolio.Market_Price_NAV = Convert.ToDecimal(mdr["Market_Price_NAV"].ToString());
                summaryPortfolio.Current_Market_Value = Convert.ToDecimal(mdr["Current_Market_Value"].ToString());

                summaryPortfolioList.Add(summaryPortfolio);
            }
            mySQLDBConnect.CloseConnection();
            return summaryPortfolioList;
        }

        public static List<TransactionByDate> GetAllTransaction(DateTime startDate, DateTime endDate, string accoutNo)
        {
            MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();

            string date1 = startDate.ToString("yyyy-MM-dd");
            string date2 = endDate.ToString("yyyy-MM-dd");

            string query = @"SELECT A.Date_Of_Transaction,
                                    A.Date_Of_Settlement,
                                    B.Fund_Code,
                                    B.Fund_Name,
                                    A.Transaction_Code,
                                    T.Name as Transaction_Name,
                                    A.IPD_Unique_Transaction_ID,
                                    A.units,
                                    A.units/A.Gross_Amount_RM As Unit_Cost_RM,
                                    A.Gross_Amount_RM  FROM 
                                    utmc_compositional_transactions A
                                    inner join utmc_fund_information B
                                    on A.IPD_Fund_Code = B.IPD_Fund_Code 
                                    inner join transaction_codes T 
                                    on T.Code = A.Transaction_Code
                                    where
                                    A.Date_Of_Transaction >= '" + date1 + "' and A.Date_Of_Transaction <= '" + date2 + "' AND IPD_Member_Acc_No='" +
                                    accoutNo + "';";
            //A.Date_Of_Transaction >= '2017-08-01' and A.Date_Of_Transaction <= '2017-08-03

            MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
            mySQLDBConnect.OpenConnection();
            MySqlDataReader mdr = cmd.ExecuteReader();
            List<TransactionByDate> transactionByDateList = new List<TransactionByDate>();
            while (mdr.Read())
            {
                TransactionByDate transactionByDate = new TransactionByDate();
                transactionByDate.Date_Of_Transaction = Convert.ToDateTime(mdr["Date_Of_Transaction"].ToString());
                transactionByDate.Date_Of_Settlement = Convert.ToDateTime(mdr["Date_Of_Settlement"].ToString());
                transactionByDate.Fund_Code = mdr["Fund_Code"].ToString();
                transactionByDate.Fund_Name = mdr["Fund_Name"].ToString();
                transactionByDate.Transaction_Code = mdr["Transaction_Code"].ToString();
                transactionByDate.Transaction_Name = mdr["Transaction_Name"].ToString();
                transactionByDate.IPD_Unique_Transaction_ID = mdr["IPD_Unique_Transaction_ID"].ToString();
                transactionByDate.Units = Convert.ToDecimal(mdr["units"].ToString());
                transactionByDate.Unit_Cost_RM = Convert.ToDecimal(mdr["Unit_Cost_RM"].ToString());
                transactionByDate.Gross_Amount_RM = Convert.ToDecimal(mdr["Gross_Amount_RM"].ToString());
                transactionByDateList.Add(transactionByDate);
            }
            mySQLDBConnect.CloseConnection();
            return transactionByDateList;
        }

        public static List<TransactionByDate> GetLastMonthSum(DateTime lastDate, string accoutNo)
        {
            MySQLDBConnect mySQLDBConnect = new MySQLDBConnect();

            //get the last date last month
            //var previousMonth = new DateTime(date.Year, date.Month, 1);
            lastDate = lastDate.AddDays(-1);

            string query = @"select A.Report_Date,B.Fund_Code, sum(Net_Cumulative_Closing_Balance_Units) As Sum_Units 
                            from utmc_compositional_investment A
                            left join utmc_fund_information B
                            on A.IPD_Fund_Code=B.IPD_Fund_Code
                            where A.Report_Date='" + lastDate.ToString("yyyy-MM-dd") + "' and A.IPD_Member_Acc_No='" + accoutNo + "' group by B.Fund_Code;";
            // A.Report_Date='2016-12-31' and A.IPD_Member_Acc_No='4531' group by B.Fund_Code;

            MySqlCommand cmd = new MySqlCommand(query, mySQLDBConnect.connection);
            mySQLDBConnect.OpenConnection();
            MySqlDataReader mdr = cmd.ExecuteReader();
            List<TransactionByDate> transactionByDateList = new List<TransactionByDate>();
            while (mdr.Read())
            {
                TransactionByDate transactionByDate = new TransactionByDate();
                transactionByDate.Report_Date = Convert.ToDateTime(mdr["Report_Date"].ToString());
                transactionByDate.Fund_Code = mdr["Fund_Code"].ToString();
                transactionByDate.Sum_Units = Convert.ToDecimal(mdr["Sum_Units"].ToString());
                transactionByDateList.Add(transactionByDate);
            }
            mySQLDBConnect.CloseConnection();
            return transactionByDateList;
        }

        public static List<SummaryPortfolio> GetAll(DateTime startDate, string accountNo)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string date1 = startDate.ToString("yyyy-MM-dd");
            //string date2 = endDate.ToString("yyyy-MM-dd");

            string query = @"SELECT B.Fund_Name,
                                    B.Fund_Code,
                                    A.Net_Cumulative_Closing_Balance_Units,
                                    A.Market_Price_NAV,
                                    (A.Net_Cumulative_Closing_Balance_Units* A.Market_Price_NAV) AS Current_Market_Value
                                    FROM utmc_compositional_investment A
                                    inner join utmc_fund_information B
                                    on A.IPD_Fund_Code=B.IPD_Fund_Code where 
                                    A.Report_Date>='" + date1 + "' AND A.IPD_Member_Acc_No='" + accountNo + "' group by B.Fund_Name";
            //A.Report_Date>='2017-08-31' and  A.Report_Date <= '2017-08-31'

            MySqlCommand cmd = new MySqlCommand(query, con);
            con.Open();
            MySqlDataReader mdr = cmd.ExecuteReader();
            List<SummaryPortfolio> summaryPortfolioList = new List<SummaryPortfolio>();
            while (mdr.Read())
            {
                SummaryPortfolio summaryPortfolio = new SummaryPortfolio();
                summaryPortfolio.Fund_Name = mdr["Fund_Name"].ToString();
                summaryPortfolio.Fund_Code = mdr["Fund_Code"].ToString();
                summaryPortfolio.Net_Cumulative_Closing_Balance_Units = Convert.ToDecimal(mdr["Net_Cumulative_Closing_Balance_Units"].ToString());
                summaryPortfolio.Market_Price_NAV = Convert.ToDecimal(mdr["Market_Price_NAV"].ToString());
                summaryPortfolio.Current_Market_Value = Convert.ToDecimal(mdr["Current_Market_Value"].ToString());

                summaryPortfolioList.Add(summaryPortfolio);
            }
            con.Close();
            return summaryPortfolioList;
        }

    }
}
