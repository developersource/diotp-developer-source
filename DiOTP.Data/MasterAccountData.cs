﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiOTP.Utility;
using MySql.Data.MySqlClient;

namespace DiOTP.Data
{
    class MasterAccountData
    {
        public static MasterAccount GetMasterAccount(string accountNo)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"select A.IPD_Member_Acc_No, B.HOLDER_CLS from utmc_compositional_investment A 
                            inner join ma_holder_reg B
                            where A.IPD_Member_Acc_No = B.HOLDER_NO AND
                            A.IPD_Member_Acc_No='" + accountNo + "' group by B.HOLDER_CLS;";


            MySqlCommand cmd = new MySqlCommand(query, con);
            con.Open();
            MySqlDataReader mdr = cmd.ExecuteReader();
            MasterAccount masterAccount = new MasterAccount();
            while (mdr.Read())
            {
                masterAccount.IPD_Member_Acc_No = mdr["IPD_Member_Acc_No"].ToString();
                masterAccount.HOLDER_CLS = mdr["HOLDER_CLS"].ToString();
            }
            con.Close();
            return masterAccount;
        }

        public static MasterAccount GetUserDetail(string accountNo)
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"select NAME_1, ADDR_1, ADDR_2, ADDR_3, ADDR_4 
                            from ma_holder_reg where HOLDER_NO='" + accountNo + "';";

            MySqlCommand cmd = new MySqlCommand(query, con);
            con.Open();
            MySqlDataReader mdr = cmd.ExecuteReader();
            MasterAccount masterAccount = new MasterAccount();
            while (mdr.Read())
            {
                masterAccount.NAME_1 = mdr["NAME_1"].ToString();
                masterAccount.ADDR_1 = mdr["ADDR_1"].ToString();
                masterAccount.ADDR_2 = mdr["ADDR_2"].ToString();
                masterAccount.ADDR_3 = mdr["ADDR_3"].ToString();
                masterAccount.ADDR_4 = mdr["ADDR_4"].ToString();
            }
            con.Close();
            return masterAccount;
        }

        public static List<MasterAccount> GetAllAccount()
        {
            MySqlConnection con = DBconnection.GetConnection();

            string query = @"select IPD_Member_Acc_No from utmc_compositional_investment
                            group by IPD_Member_Acc_No;";

            MySqlCommand cmd = new MySqlCommand(query, con);
            con.Open();
            MySqlDataReader mdr = cmd.ExecuteReader();
            List<MasterAccount> masterAccountList = new List<MasterAccount>();
            while (mdr.Read())
            {
                MasterAccount masterAccount = new MasterAccount();
                masterAccount.IPD_Member_Acc_No = mdr["IPD_Member_Acc_No"].ToString();
                masterAccountList.Add(masterAccount);
            }
            con.Close();
            return masterAccountList;

        }
    }
}
