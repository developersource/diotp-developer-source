using MySql.Data.MySqlClient;
using System.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DiOTP.Utility.Helper;
using DiOTP.Utility.CustomClasses;

namespace DiOTP.Data.MySQLDBRef
{
    public class MySQLDBConnect
    {
        public MySqlConnection connection;
        public string connectionString = ConfigurationManager.ConnectionStrings["DiOTPConnection"].ConnectionString;
        public MySQLDBConnect()
        {
            Initialize();
        }
        public void Initialize()
        {
            connection = new MySqlConnection(connectionString);
        }
        public bool OpenConnection()
        {
            try
            {
                if (connection.State == ConnectionState.Open)
                    Initialize();
                connection.Open();
                return true;
            }
            catch(MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        //MessageBox.Show("Cannot connect to server.Contact administrator");
                        break;
                    case 1045:
                        //MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        public Response CheckOpenConnection()
        {
            Response response = new Response();
            try
            {
                if (connection.State == ConnectionState.Open)
                    Initialize();
                connection.Open();
                response.IsSuccess = true;
                return response;
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        //MessageBox.Show("Cannot connect to server.Contact administrator");
                        break;
                    case 1045:
                        //MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response;
            }
        }
        public bool CloseConnection()
        {
            try
            {
                connection.Close();
                connection.Dispose();
                return true;
            }
            catch(MySqlException ex)
            {
                Logger.WriteLog(ex.Message);
                //MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
